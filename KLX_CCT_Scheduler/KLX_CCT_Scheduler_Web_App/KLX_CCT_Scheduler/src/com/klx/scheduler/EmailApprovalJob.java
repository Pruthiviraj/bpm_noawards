package com.klx.scheduler;

import com.klx.common.constants.QueryConstants;
import com.klx.common.logger.KLXLogger;

import java.io.BufferedReader;

import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.net.URLConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.HashMap;
import java.util.logging.Level;

import javax.naming.InitialContext;

import javax.sql.DataSource;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class EmailApprovalJob implements Job {
    private static KLXLogger logger = KLXLogger.getLogger();
    URL emailApprovalServlet = null;

    public EmailApprovalJob() {
        super();
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.log(Level.INFO, EmailApprovalJob.class.getSimpleName(), "execute", "Start of Email Approval Servlet");

        try {
            emailApprovalServlet = new URL(KlxSchedulerServlet.emailApprovalURL);
            URLConnection servletConnection = (URLConnection) emailApprovalServlet.openConnection();
            servletConnection.connect();
            BufferedReader in = new BufferedReader(new InputStreamReader(servletConnection.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null)
            logger.log(Level.INFO, EmailApprovalJob.class.getSimpleName(), "execute",
                           "InputLine:" + inputLine);
            in.close();

            logger.log(Level.INFO, EmailApprovalJob.class.getSimpleName(), "execute",
                       "servletConnection.getURL():" + servletConnection.getURL());

        } catch (Exception e) {
            logger.log(Level.SEVERE, EmailApprovalJob.class.getSimpleName(), "execute",
                       "Exception in EmailApprovalJob " + e);
            e.printStackTrace();
        }

        logger.log(Level.INFO, EmailApprovalJob.class.getSimpleName(), "execute", "End of Email Approval Servlet");
    }

}
