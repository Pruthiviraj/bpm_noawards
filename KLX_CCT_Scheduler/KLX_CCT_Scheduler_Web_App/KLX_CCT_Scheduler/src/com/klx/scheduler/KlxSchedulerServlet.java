package com.klx.scheduler;

import com.klx.common.constants.QueryConstants;
import com.klx.common.logger.KLXLogger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;

import javax.naming.InitialContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import javax.sql.DataSource;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

public class KlxSchedulerServlet extends HttpServlet {
    public static final String DS_NAME = "jdbc/XxKLXDataSource";
    Properties properties = new Properties();
    public static HashMap propertiesMap = new HashMap();
    private static KLXLogger logger = KLXLogger.getLogger();
    private StdSchedulerFactory schedulerFactory;
    private static final String KLX_SCHEDULER_GROUP_NAME = "KlxSchedulerGroup";
    public static final String EMAIL_APPROVAL_JOB_NAME = "EmailApprovalJob";
    public static final String REMINDER_EMAIL_JOB_NAME = "ReminderEmailJob";
    public static final String EMAIL_APPROVAL_TRIGGER_NAME = "KlxEmailApprovalTrigger";
    public static final String REMINDER_EMAIL_TRIGGER_NAME = "KlxReminderEmailTrigger";
    public static final String EMAIL_APPROVAL_URL = "EMAIL_APPROVAL_URL";
    public static final String REMINDER_EMAIL_URL = "REMINDER_EMAIL_URL";
    public static final String CRON_EXPR_EMAIL_APPROVAL = "CRON_EXPR_EMAIL_APPROVAL";
    public static final String CRON_EXPR_REMINDER_EMAIL = "CRON_EXPR_REMINDER_EMAIL";
    private Scheduler scheduler;
    public static String emailApprovalURL;
    public static String reminderEmailURL;
    public static String cronExpressionEmailApproval;
    public static String cronExpressionReminderEmail;

    public KlxSchedulerServlet() {
        super();
    }

    /**
     * @see Servlet#init(ServletConfig)
     */
    public void init(ServletConfig config) throws ServletException {
        logger.log(Level.INFO, KlxSchedulerServlet.class.getSimpleName(), "init",
                   "Entering init method");
        super.init(config);
        getSchedulerProperties();
        emailApprovalURL = (String) propertiesMap.get(EMAIL_APPROVAL_URL);
        reminderEmailURL = (String) propertiesMap.get(REMINDER_EMAIL_URL);
        cronExpressionEmailApproval = (String) propertiesMap.get(CRON_EXPR_EMAIL_APPROVAL);
        cronExpressionReminderEmail = (String) propertiesMap.get(CRON_EXPR_REMINDER_EMAIL);
        try {
            logger.log(Level.INFO, KlxSchedulerServlet.class.getSimpleName(), "init",
                       "Initializing StdSchedulerFactory and Scheduler");

            // Initiate a Schedule Factory
            schedulerFactory = new StdSchedulerFactory("Klx_Scheduler.properties");
            // Retrieve a scheduler from schedule factory
            scheduler = this.schedulerFactory.getScheduler();

            // Initialize EmailApproval Job
            logger.log(Level.INFO, KlxSchedulerServlet.class.getSimpleName(), "init", "Initializing EmailApproval Job");

            JobDetail jdAlliantIntegration =
                new JobDetail(KLX_SCHEDULER_GROUP_NAME, EMAIL_APPROVAL_JOB_NAME, EmailApprovalJob.class);

            logger.log(Level.INFO, KlxSchedulerServlet.class.getSimpleName(), "init",
                       "Initializing EmailApproval Job Trigger");

            CronTrigger cronTriggerAlliant = new CronTrigger(EMAIL_APPROVAL_TRIGGER_NAME, KLX_SCHEDULER_GROUP_NAME);
            cronTriggerAlliant.setCronExpression(cronExpressionEmailApproval);

            scheduler.scheduleJob(jdAlliantIntegration, cronTriggerAlliant);

            logger.log(Level.INFO, KlxSchedulerServlet.class.getSimpleName(), "init", "Email Approval Job Scheduled");

            // Initialize Reminder Email Job
            logger.log(Level.INFO, KlxSchedulerServlet.class.getSimpleName(), "init",
                       "Initializing Reminder Email Job");

            jdAlliantIntegration =
                new JobDetail(KLX_SCHEDULER_GROUP_NAME, REMINDER_EMAIL_JOB_NAME, ReminderEmailJob.class);

            logger.log(Level.INFO, KlxSchedulerServlet.class.getSimpleName(), "init",
                       "Initializing Reminder Email Job Trigger");

            cronTriggerAlliant = new CronTrigger(REMINDER_EMAIL_TRIGGER_NAME, KLX_SCHEDULER_GROUP_NAME);

            cronTriggerAlliant.setCronExpression(cronExpressionReminderEmail);

            scheduler.scheduleJob(jdAlliantIntegration, cronTriggerAlliant);

            logger.log(Level.INFO, KlxSchedulerServlet.class.getSimpleName(), "init", "Reminder Email Job Scheduled");

            scheduler.start();

            logger.log(Level.INFO, KlxSchedulerServlet.class.getSimpleName(), "init", "KLX Scheduler Started");
        } catch (Exception e) {
            logger.log(Level.SEVERE, KlxSchedulerServlet.class.getSimpleName(), "init",
                       "Exception in Scheduling Job(s): " + e);
        }
        logger.log(Level.INFO, KlxSchedulerServlet.class.getSimpleName(), "init",
                   "Exiting init method");
    }

    public void destroy() {
        logger.log(Level.INFO, KlxSchedulerServlet.class.getSimpleName(), "init",
                   "Entering destroy method");
        if (scheduler != null) {
            try {

                scheduler.unscheduleJob(EMAIL_APPROVAL_TRIGGER_NAME, KLX_SCHEDULER_GROUP_NAME);

                logger.log(Level.INFO, KlxSchedulerServlet.class.getSimpleName(), "destroy",
                           "Email Approval Job Unscheduled");

                scheduler.unscheduleJob(REMINDER_EMAIL_TRIGGER_NAME, KLX_SCHEDULER_GROUP_NAME);

                logger.log(Level.INFO, KlxSchedulerServlet.class.getSimpleName(), "destroy",
                           "Reminder Email Job Unscheduled");

                scheduler.shutdown();

                logger.log(Level.INFO, KlxSchedulerServlet.class.getSimpleName(), "destroy",
                           "KLX Scheduler was shutdown successfully");
            } catch (Exception e) {
                logger.log(Level.SEVERE, KlxSchedulerServlet.class.getSimpleName(), "destroy",
                           "Exception in Unscheduling Job(s) and shutting down Scheduler: " + e);
            }
        }
        logger.log(Level.INFO, KlxSchedulerServlet.class.getSimpleName(), "init",
                   "Exiting destroy method");
    }

    public void getSchedulerProperties() {
        logger.log(Level.INFO, getClass(), "KlxSchedulerServlet", "Entering getSchedulerProperties method");
        ResultSet resultset = null;
        Connection connection = null;
        Statement statement = null;

        try {
            connection = createDBConnection();
            statement = connection.createStatement();

            // fetch all external configuration properties from DB
            resultset = statement.executeQuery(QueryConstants.PROPERTIES_QUERY);

            while (resultset.next()) {
                propertiesMap.put(resultset.getString("KEY"), resultset.getString("VALUE"));
            }

            resultset.close();
            statement.close();

        } catch (Exception exception) {
            logger.log(Level.SEVERE, getClass(), "KlxSchedulerServlet",
                       "Exception in getSchedulerProperties:" + exception);
            exception.printStackTrace();

        } finally {
            try {
                if (resultset != null) {
                    resultset.close();
                }
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException sqlException) {
                logger.log(Level.SEVERE, getClass(), "KlxSchedulerServlet",
                           "Exception in getSchedulerProperties:" + sqlException);
                sqlException.printStackTrace();

            }
        }

        logger.log(Level.INFO, getClass(), "KlxSchedulerServlet", "Exiting getSchedulerProperties method");
    }

    public Connection createDBConnection() throws Exception {
        logger.log(Level.INFO, getClass(), "KlxSchedulerServlet", "Entering createDBConnection method");
        Connection connection = null;
        try {
            InitialContext context = new InitialContext();
            Object obj = context.lookup(DS_NAME);
            DataSource ds = (DataSource) obj;
            connection = ds.getConnection();
        } catch (SQLException sqlException) {
            logger.log(Level.SEVERE, getClass(), "KlxSchedulerServlet",
                       "in createDBConnection - " + sqlException.getMessage());
            sqlException.printStackTrace();
        }
        logger.log(Level.INFO, getClass(), "KlxSchedulerServlet", "Exiting createDBConnection method");
        return connection;
    }
}
