package com.klx.reminder;

import com.klx.common.logger.KLXLogger;
import com.klx.services.entities.Approval;
import com.klx.services.entities.Opportunity;
import com.klx.services.entities.Tasks;
import com.klx.services.wrapper.EmailUtilityWrapper;
import com.klx.services.wrapper.TaskQueryServiceWrapper;

import java.io.IOException;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.List;

import java.util.logging.Level;

import javax.naming.InitialContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

public class ReminderEmail implements ServletContextListener,
                                             javax.servlet.Servlet {
    private static KLXLogger logger = KLXLogger.getLogger();
    private ServletContext context = null;
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    public static final String DS_NAME = "jdbc/XxKLXDataSource";
    public static final String procStmt="begin ? := cct_notification_pkg.load_notification_temp; end;";
    public static final String fetchNotifTempQuery =
                "select OPPORTUNITY_NUMBER, NOTIF_TEMPLATE_ID from CCT_NOTIFICATION_TEMP " +
                "order by NOTIF_TEMPLATE_ID, OPPORTUNITY_NUMBER asc";
    public static final String source = "SCHEDULED";
    Connection conn;
    Statement stmt;
    CallableStatement cstmt;
    ResultSet rs;
    TaskQueryServiceWrapper taskObj;
    EmailUtilityWrapper emailObj;
    /**List<Opportunity> Notif3OptyList, Notif4OptyList, ProjFolderOptyList;
    List<Tasks> Notif3TasksList;
    int totalCustDueDateTasks;**/
    List<Opportunity> ApprOptyList;
    List<Approval> ApprTasksList;
    int totalApprovalTasks;

    public void contextInitialized(ServletContextEvent event) {
        context = event.getServletContext();
    }

    public void contextDestroyed(ServletContextEvent event) {
        context = event.getServletContext();
    }
    
    public Connection createDBConnection() throws Exception {
        logger.log(Level.INFO, getClass(), "ReminderEmail", "Entering createDBConnection method");
       Connection connection = null;
       try {
           logger.log(Level.INFO, getClass(),"createDBConnection", "In createDBConnection - start");
           InitialContext context = new InitialContext();
           Object obj = context.lookup(DS_NAME);
           logger.log(Level.INFO, getClass(),"createDBConnection","In createDBConnection - after data source look up");
           DataSource ds = (DataSource)obj;                
           connection = ds.getConnection();
           logger.log(Level.INFO, getClass(), "createDBConnection","In createDBConnection - end");
       } 
       catch (SQLException sqlException) {
           logger.log(Level.SEVERE, getClass(), "createDBConnection","in sql exception - " + sqlException.getMessage());
           sqlException.printStackTrace();
       }
        logger.log(Level.INFO, getClass(), "ReminderEmail", "Exiting createDBConnection method");
       return connection;
    }
    
    public void populateList(ServletResponse servletResponse){
        logger.log(Level.INFO, getClass(), "ReminderEmail", "Entering populateList method");
        try {
            logger.log(Level.INFO, getClass(), "populateList","In populate Array List-Start");
            conn = createDBConnection();            
            stmt = conn.createStatement();
            cstmt = conn.prepareCall(procStmt);
            cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
            cstmt.executeUpdate();
            String status = cstmt.getString(1);
            //System.out.println(status);
            servletResponse.getOutputStream().write("POPULATE LIST - REMINDER EMAIL CLIENT".getBytes());
            rs = stmt.executeQuery(fetchNotifTempQuery);   
            /**DISABLED
            Notif3OptyList = new ArrayList<Opportunity>();
            Notif4OptyList = new ArrayList<Opportunity>();**/
            ApprOptyList = new ArrayList<Opportunity>();
            /**DISABLED
            ProjFolderOptyList = new ArrayList<Opportunity>();
            totalCustDueDateTasks = 0;**/
            totalApprovalTasks = 0; 
            while (rs.next()) {
                logger.log(Level.INFO, getClass(),"populateList","OptyNumber-"+rs.getString("OPPORTUNITY_NUMBER"));              
                logger.log(Level.INFO, getClass(),"populateList","TemplateId-"+rs.getString("NOTIF_TEMPLATE_ID"));                
                //Put all opty and template details from DB into List
                int notifTemplateId = rs.getInt(2);
                Opportunity optyObj = new Opportunity();
                /**DISABLED if (notifTemplateId == 3) {
                    System.out.println("RESPECTIVE PRODUCT LINE TEAMS");  
                    optyObj.setOpportunityNumber(rs.getString(1));
                    //optyObj.setOpportunityId(rs.getString(2));
                    Notif3OptyList.add(optyObj);
                } 
                else if (notifTemplateId == 4){
                    System.out.println("SALES");
                    optyObj.setOpportunityNumber(rs.getString(1));
                    //optyObj.setOpportunityId(rs.getString(2));
                    Notif4OptyList.add(optyObj);
                }
                else **/
                if (notifTemplateId == 10){
                    logger.log(Level.INFO, getClass(),"populateList","APPROVALS");
                    optyObj.setOpportunityNumber(rs.getString(1));
                    //optyObj.setOpportunityId(rs.getString(2));
                    ApprOptyList.add(optyObj);
                }
                /**DISABLED else if (notifTemplateId == 16){
                    System.out.println("PROJECT FOLDER");
                    optyObj.setOpportunityNumber(rs.getString(1));
                    //optyObj.setOpportunityId(rs.getString(2));
                    ProjFolderOptyList.add(optyObj);
                }**/
            }
            //servletResponse.getOutputStream().write((Integer.toString(Notif4OptyList.size()) + " Notif4OptyList values").getBytes()); 
            servletResponse.getOutputStream().write((Integer.toString(ApprOptyList.size()) + " ApprOptyList values").getBytes()); 
            rs.close();
            stmt.close();
        } 
        catch (Exception exception) {
            logger.log(Level.INFO, getClass(), "populateList","in exception -" + exception.getMessage());
            exception.printStackTrace();
        } 
        finally {
            try {                
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } 
            catch (SQLException sqlException) {
                logger.log(Level.INFO, getClass(), "populateList","in sql exception -" + sqlException.getMessage());
                sqlException.printStackTrace();
            } 
        }
        logger.log(Level.INFO, getClass(), "ReminderEmail", "Exiting populateList method");
    }
    
    public void initializeTaskQueryService(ServletResponse servletResponse) {
        try{
            logger.log(Level.INFO, getClass(), "ReminderEmail", "Entering initializeTaskQueryService method");
            taskObj = new TaskQueryServiceWrapper();
            emailObj =  new EmailUtilityWrapper();
            //servletResponse.getOutputStream().write("initializeTaskQueryService - REMINDER EMAIL CLIENT".getBytes());
            logger.log(Level.INFO, getClass(), "initializeTaskQueryService","In initializeTaskQueryService Array List-Start");
            /** DISABLED 
            //Notification 4 - Customer due date is in 2 days
            System.out.println("In initializeTaskQueryService Notif4OptyList SIZE - " + Notif4OptyList.size());
            System.out.println("Before Looping Notif4OptyList");
            servletResponse.getOutputStream().write("Starting of Notification 4 <br />".getBytes());
            int count=0;
            for (Opportunity opty : Notif4OptyList){
                System.out.println(opty.getOpportunityNumber());
                //System.out.println(opty.getOpportunityId());
                System.out.println("EMAIL STATUS - " + emailObj.sendCustDueDateNotification(source,opty.getOpportunityNumber(),"4","scCCT_CSM"));
                count++;
            }
            servletResponse.getOutputStream().write((Integer.toString(count) + " - COUNT of Notification 4<br />").getBytes()); 
            servletResponse.getOutputStream().write("Ending of Notification 4 <br />".getBytes());
            System.out.println("After Looping Notif4OptyList");
            
            //Notification 3 - Customer due date is in 5 days
            Notif3TasksList = new ArrayList<Tasks>();
            System.out.println("In initializeTaskQueryService Notif3OptyList SIZE - " + Notif3OptyList.size());
            System.out.println("Before Looping Notif3OptyList");
            for (Opportunity opty : Notif3OptyList){
                Notif3TasksList = taskObj.fetchCustDueDateNotifTaskList(opty.getOpportunityNumber());
                System.out.println("In initializeTaskQueryService Notif3TasksList SIZE - " + Notif3TasksList.size());
                System.out.println("After Looping Notif3TasksList");
            }**/
            //Notification 10 - Approval Reminder
            ApprTasksList = new ArrayList<Approval>();
            logger.log(Level.INFO, getClass(), "initializeTaskQueryService","In initializeTaskQueryService ApprOptyList SIZE - " + ApprOptyList.size());
            servletResponse.getOutputStream().write("Starting of Notification 10 <br />".getBytes());
            int count1=0;
            for (Opportunity opty : ApprOptyList){
                ApprTasksList = taskObj.fetchApprovalReminderNotifTaskList(opty.getOpportunityNumber());
                logger.log(Level.INFO, getClass(), "initializeTaskQueryService","In initializeTaskQueryService ApprTasksList SIZE - " + ApprTasksList.size());
                totalApprovalTasks = totalApprovalTasks + ApprTasksList.size();
                for (Approval app : ApprTasksList){
                    logger.log(Level.INFO, getClass(), "initializeTaskQueryService","EMAIL STATUS - " + emailObj.sendApprovalNotification(source,opty.getOpportunityNumber(),"10",app.getApproverId(),app.getApproverRole(), app.getTaskNumber()));
                    servletResponse.getOutputStream().write("Status - <br />".getBytes());
                    count1++;
                }
             }
            servletResponse.getOutputStream().write((Integer.toString(count1) + " - COUNT of Notification 10 <br />").getBytes()); 
            servletResponse.getOutputStream().write("Ending of Notification 10 <br />".getBytes());
	    /**DISABLED 
            //Notification 16 - No file in Project Folder
            System.out.println("In initializeTaskQueryService ProjFolderOptyList SIZE - " + ProjFolderOptyList.size());
            System.out.println("Before Looping ProjFolderOptyList");
            for (Opportunity opty : ProjFolderOptyList){
                System.out.println("EMAIL STATUS - " + emailObj.sendEmailNotification(source,opty.getOpportunityNumber(),"","16"));
            }
            System.out.println("After Looping ProjFolderOptyList");**/
           }
        catch(Exception e){
            logger.log(Level.INFO, getClass(), "initializeTaskQueryService","Exception"+ e.getStackTrace());
            logger.log(Level.INFO, getClass(), "initializeTaskQueryService","Exception"+e.getSuppressed());
        }
        logger.log(Level.INFO, getClass(), "ReminderEmail", "Exiting initializeTaskQueryService method");
    }
    
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) {
        logger.log(Level.INFO, getClass(), "ReminderEmail", "Entering service method");
        try {
        servletResponse.getOutputStream().write("Starting of RemainderEmailClient Servlet <br />".getBytes());
        populateList(servletResponse);   
        initializeTaskQueryService(servletResponse);
        /** DISABLED servletResponse.getOutputStream().write((Integer.toString(Notif3OptyList.size()) + " Notif3OptyList values").getBytes());    **/
        //servletResponse.getOutputStream().write((Integer.toString(Notif4OptyList.size()) + " Notif4OptyList values").getBytes()); 
        //servletResponse.getOutputStream().write((Integer.toString(ApprOptyList.size()) + " ApprOptyList values").getBytes()); 
        /** DISABLED servletResponse.getOutputStream().write((Integer.toString(ProjFolderOptyList.size()) + " ProjFolderOptyList values").getBytes()); 
        servletResponse.getOutputStream().write((Integer.toString(totalCustDueDateTasks) + " Notif3TasksList values").getBytes()); **/
        servletResponse.getOutputStream().write((Integer.toString(totalApprovalTasks) + " ApprTasksList values").getBytes()); 
        servletResponse.getOutputStream().write("Completion of RemainderEmailClient Servlet <br />".getBytes());    
        } 
        catch (Exception exception) {
            logger.log(Level.SEVERE, getClass(), "service","in exception service -" + exception.getMessage());
            exception.printStackTrace();
        }  
        logger.log(Level.INFO, getClass(), "ReminderEmail", "Exiting service method");
    }

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        // TODO Implement this method
        populateList(null);   
        initializeTaskQueryService(null);
    }

    @Override
    public ServletConfig getServletConfig() {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getServletInfo() {
        // TODO Implement this method
        return null;
    }

    @Override
    public void destroy() {
        // TODO Implement this method
    }
    
}
