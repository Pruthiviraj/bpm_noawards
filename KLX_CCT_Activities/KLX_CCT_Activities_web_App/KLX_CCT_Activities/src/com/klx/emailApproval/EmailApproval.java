package com.klx.emailApproval;

import com.klx.cct.ad.user.v1.User;
import com.klx.common.logger.KLXLogger;

import com.klx.services.wrapper.ActiveDirectoryServiceWrapper;

import com.klx.services.wrapper.TaskQueryServiceWrapper;
import com.klx.services.wrapper.TaskUpdateServiceWrapper;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.logging.Level;

import javax.mail.Flags;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.search.FlagTerm;

import javax.servlet.ServletContextListener;

import oracle.bpel.services.workflow.task.model.Task;


public class EmailApproval implements ServletContextListener, javax.servlet.Servlet {
    //String senderMailId,String taskNumber, String outcome,String comments
    private static KLXLogger logger = KLXLogger.getLogger();
    public static final String ACTION_APPROVE = "APPROVE";
    public static final String ACTION_REJECT = "REJECT";
    public static final String ACTION_CONFLICT = "CONFLICT";
    public static final String TASK_FAILURE = "FAILURE";
    public static final String MAIL_FROM_ID = "fromEmailId";
    public static final String USER_ID = "userId";
    public static final String MAIL_CONTENT = "content";
    public static final String MAIL_SUBJECT_CRFNO = "CRFNo";
    public static final String MAIL_SUBJECT_TASKNUMBER = "TaskNumber";
    public static final String MAIL_SUBJECT_ACTION = "Action";
    public static final String MAIL_SUBJECT_GROUP = "Group";
    public static final String ACTION_SOURCE = "Email";
    ActiveDirectoryServiceWrapper adsWrapper = null;
    TaskUpdateServiceWrapper taskUpdateWrapper = null;
    TaskQueryServiceWrapper taskQueryWrapper = null;
    /***  **/
    @SuppressWarnings("unchecked")
    public void taskOperationsThroughEmail(ServletResponse servletResponse) {
        //public void taskOperationsThroughEmail(){
        logger.log(Level.INFO, getClass(), "EmailApproval", "Entering taskOperationsThroughEmail method");
        String fromEmailId = null;
        String content = null;
        try {
            //below three classes are used to get ActiveDirectory Details
            adsWrapper = new ActiveDirectoryServiceWrapper();
            taskUpdateWrapper = new TaskUpdateServiceWrapper();
            taskQueryWrapper = new TaskQueryServiceWrapper();
            
            MailAccess mailAccess = new MailAccess();

            Folder fldInbox = mailAccess.getInbox();
            Message messages[] = fldInbox.search(new FlagTerm(new Flags(Flag.SEEN), false));

            int messageLength = messages.length;

            for (int i = 0; i < messageLength; i++) {
                Message message = messages[i];
                //Below map should be used to store all the parameters of Subject, from mail id and the content of the mail
                HashMap<String, String> emailSubjectParamsMap = processMessageSubject(message.getSubject());
                // Typecasted the address to InternetAddress as messag.from[0] was returning both username and email id.
                InternetAddress[] from = (InternetAddress[]) message.getFrom();

                fromEmailId = from[0].getAddress();

                //This method is the get the complete content of the email

                content = mailAccess.processMessageBody(message);
                emailSubjectParamsMap.put(EmailApproval.MAIL_FROM_ID, fromEmailId);
                emailSubjectParamsMap.put(EmailApproval.MAIL_CONTENT, content);
                String status = approveOrRejectTask(emailSubjectParamsMap);


                //move email to different folder and also mark it as read

                boolean isMessageMoved = mailAccess.moveMessages(message, status);


            }
            mailAccess.closeInbox(fldInbox);

        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "EmailApproval", "Exception in taskOperationsThroughEmail method" + e);
            e.printStackTrace();
        }
        logger.log(Level.INFO, getClass(), "EmailApproval", "Exiting taskOperationsThroughEmail method");

    }

    public HashMap<String, String> processMessageSubject(String subject) {
        logger.log(Level.INFO, getClass(), "EmailApproval", "Entering processMessageSubject method");

        //Email Subject is exected in below format
        //CRFNo=crf1024:TaskNumber=1234:Action=Approve:Group=scCCT_SALES

        HashMap<String, String> subjectParams = new HashMap<String, String>();
        try {

            if (null == subject || "".equals(subject)) {
                logger.log(Level.INFO, getClass(), "EmailApproval", "No values in the subject:" + subject + ":");

            } else {
                //Creating String tokens for : values
                StringTokenizer strTokenSubject = new StringTokenizer(subject, ":");
                StringTokenizer strTokenParam = null;
                String strParam = null;
                String strValue = null;
                while (strTokenSubject.hasMoreTokens()) {
                    //Creating String tokens for = values example like CRFNo=crf1024
                    strTokenParam = new StringTokenizer(strTokenSubject.nextToken(), "=");
                    if (strTokenParam.hasMoreTokens()) {
                        strParam = strTokenParam.nextToken().trim();
                        if (strTokenParam.hasMoreTokens()) {
                            strValue = strTokenParam.nextToken().trim();
                            subjectParams.put(strParam, strValue);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "EmailApproval", "Exception while parsing the email subject:" + e);
            e.printStackTrace();
        }

        logger.log(Level.INFO, getClass(), "EmailApproval", "Exiting processMessageSubject method");
        return subjectParams;
    }

    public String approveOrRejectTask(HashMap emailParams) {
        logger.log(Level.INFO, getClass(), "EmailApproval", "Entering approveOrRejectTask method");
        String status = ""; //This returns the final action performed on the task

        User user = null;
        String userId = "";
        boolean isValidContent = false;

        try {

            String strCRFNo = (String) emailParams.get(EmailApproval.MAIL_SUBJECT_CRFNO);
            String strTaskNumber = (String) emailParams.get(EmailApproval.MAIL_SUBJECT_TASKNUMBER);
            String strAction = (String) emailParams.get(EmailApproval.MAIL_SUBJECT_ACTION);
            String strGroup = (String) emailParams.get(EmailApproval.MAIL_SUBJECT_GROUP);
            //Check for mandatory elements in HashMap to proceed further for Approval or Rejection of Opportunity/Task
            if (null == strCRFNo && null == strTaskNumber && null == strAction && null == strGroup){
                logger.log(Level.INFO, getClass(), "EmailApproval",
                           "Email Subject not for approve or reject");
            } 
            else  if (null == strCRFNo || null == strTaskNumber || null == strAction || null == strGroup) {
                //!IMP Condition: No processing of email if any of the above four params are not available
                status = EmailApproval.ACTION_CONFLICT;
                  logger.log(Level.INFO, getClass(), "EmailApproval",
                             "One of the mandatory parameter is not available in the subject" + emailParams);
              } 
            else {
                String strFromMailId = (String) emailParams.get(EmailApproval.MAIL_FROM_ID);
                String strContent = (String) emailParams.get(EmailApproval.MAIL_CONTENT);
                //Call service layer method to get user Id
                
                user = adsWrapper.fetchUserDetailsByEmailId(strFromMailId);
                logger.log(Level.INFO, getClass(), "EmailApproval",
                           "User ="+user);
                if(null != user)
                    userId = user.getUserId();
                //UserId is used for validation of task owner
                emailParams.put(EmailApproval.USER_ID, userId);
                
                logger.log(Level.INFO, getClass(), "EmailApproval",
                           "Key =" + emailParams.keySet().toString() + ":Values =" + emailParams.values().toString());

                //call validation function, if validation is false set strAction as conflict
                isValidContent = validateEmailContent(emailParams);

                if (!isValidContent) {
                    status = EmailApproval.ACTION_CONFLICT;
                } else {
                    //call service update wrapper to approve or reject class
                    String taskClosureStatus = null;
                    // call only if action performed is approve or reject
                    
                        taskClosureStatus =
                            taskUpdateWrapper.approveOrRejectTask(userId, strTaskNumber, strAction.toUpperCase(),
                                                                  strContent, EmailApproval.ACTION_SOURCE);
                   
                    if (EmailApproval.TASK_FAILURE.equals(taskClosureStatus)) {
                        status = EmailApproval.TASK_FAILURE;
                        logger.log(Level.SEVERE, getClass(), "EmailApproval",
                                   "Unable to perform action on the task" + status);
                    } else {
                        status = strAction; // value will be either Approve or Reject
                    }
                }
                logger.log(Level.INFO, getClass(),"EmailApproval","Email approval action is successfully completed with the details," +
                      "Task Number: "+strTaskNumber+" Action: "+strAction.toUpperCase()+" Status: "+status+" Approver mail id: "+strFromMailId);
                 
            }

        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "EmailApproval", "Error in email approval" + e);
            e.printStackTrace();
        }

        return status;
    }

    public boolean validateEmailContent(HashMap emailDetails) {
        logger.log(Level.INFO, getClass(), "EmailApproval", "Entering approveOrRejectTask method");
        boolean isValid = false;
        //validate if authorized user has sent request
        try{        
        boolean isUserIdvalid = validateUserId(emailDetails);
        boolean isUserTaskOwner = validateTaskDetails(emailDetails);
        if (isUserIdvalid == true && isUserTaskOwner == true)
            isValid = true;
        }catch(Exception e){
            logger.log(Level.SEVERE, getClass(), "EmailApproval", "Exception in validate EmailContent method"+e); 
            e.printStackTrace();
        }
        logger.log(Level.INFO, getClass(), "EmailApproval", "Entering approveOrRejectTask method");
        return isValid;

    }
    
    public boolean validateUserId(HashMap emailDetails){
        logger.log(Level.INFO, getClass(), "EmailApproval", "Entering validateUserId method");
        boolean isValidUser = false;        
        String userId = (String) emailDetails.get(EmailApproval.USER_ID);
        if(null != userId){
            isValidUser = true;
        }else
            logger.log(Level.INFO, getClass(), "EmailApproval", "Unauthorized user has sent a request for approval of task :"+emailDetails.get(EmailApproval.MAIL_FROM_ID));
        
        logger.log(Level.INFO, getClass(), "EmailApproval", "Exiting validateUserId method");
        return isValidUser;
    }
    
    public boolean validateTaskDetails(HashMap emailDetails){
        logger.log(Level.INFO, getClass(), "EmailApproval", "Entering validateTaskDetails method");
        boolean isUserTaskOwner = false;
        
        String taskNumber = (String) emailDetails.get(EmailApproval.MAIL_SUBJECT_TASKNUMBER);
        String userId = (String) emailDetails.get(EmailApproval.USER_ID);
        String groupName = (String) emailDetails.get(EmailApproval.MAIL_SUBJECT_GROUP);
        String crfNum = (String) emailDetails.get(EmailApproval.MAIL_SUBJECT_CRFNO);
        
        Task task = taskQueryWrapper.fetchTaskDetailsByNumber(taskNumber);
        String taskOwnerUser = task.getOwnerUser();
        String taskUserGroup = task.getSystemMessageAttributes().getTextAttribute4();
        String taskCrfNum = task.getSystemMessageAttributes().getTextAttribute1();
        
        if (null !=taskOwnerUser && null != taskUserGroup && null != taskCrfNum){
        if(userId.equalsIgnoreCase(taskOwnerUser) &&(groupName.equalsIgnoreCase(taskUserGroup))&&(crfNum.equalsIgnoreCase(taskCrfNum))) {
                isUserTaskOwner = true;
        }
            else
                    if(!userId.equalsIgnoreCase(taskOwnerUser)){
                        logger.log(Level.INFO, getClass(), "EmailApproval", "User who has sent request for approval of task is not the " +
                              "owner of the task.Sender Email id: "+emailDetails.get(EmailApproval.MAIL_FROM_ID)+" taskNumber: "+taskNumber+" taskOwner: "+taskOwnerUser);
                    }
                    if(!taskUserGroup.equalsIgnoreCase(groupName) || !(crfNum.equals(taskCrfNum))){
                        logger.log(Level.INFO, getClass(), "EmailApproval", "One or two parameters in the email subject are incorrect " +
                              "Sender Email id: "+emailDetails.get(EmailApproval.MAIL_FROM_ID)+" taskNumber: "+taskNumber+" taskOwner: "+taskOwnerUser+
                                   " Owner user Group: "+taskUserGroup+" Opportunity Number: "+taskCrfNum);
                        }
        }
        else {
            logger.log(Level.INFO, getClass(), "EmailApproval", "One or more email parameters value in the task are null,taskNumber: "+taskNumber );                  
        }
        logger.log(Level.INFO, getClass(), "EmailApproval", "Entering validateTaskDetails method");
        return isUserTaskOwner;
    }
    
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        // TODO Implement this method
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        // TODO Implement this method
    }

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        // TODO Implement this method
    }

    @Override
    public ServletConfig getServletConfig() {
        // TODO Implement this method
        return null;
    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException,
                                                                                               IOException {
       servletResponse.getOutputStream().write("Starting of EmailApproval Servlet <br />".getBytes());
       taskOperationsThroughEmail(servletResponse);
        servletResponse.getOutputStream().write("Completion of EmailApproval Servlet <br />".getBytes());        
}

    @Override
    public String getServletInfo() {
        // TODO Implement this method
        return null;
    }

    @Override
    public void destroy() {
        // TODO Implement this method
    }

   /*public static void main(String[] args){
        EmailApproval e = new EmailApproval();
        e.taskOperationsThroughEmail();
    }*/
}
