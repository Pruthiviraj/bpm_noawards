package com.klx.emailApproval;

import com.klx.common.constants.QueryConstants;
import com.klx.common.logger.KLXLogger;

import java.sql.Connection;
import java.sql.SQLException;

import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;

import java.util.Properties;

import java.io.IOException;

import java.sql.ResultSet;
import java.sql.Statement;

import java.util.ArrayList;

import java.util.Collection;
import java.util.Collections;

import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.logging.Level;

import javax.mail.Address;
import javax.mail.internet.InternetAddress;
import javax.mail.BodyPart;
import javax.mail.Flags.Flag;
import javax.mail.Flags;

import javax.naming.InitialContext;

import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;

import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;

import javax.sql.DataSource;


public class MailAccess {
    public static final String DS_NAME = "jdbc/XxKLXDataSource";
    Properties properties = new Properties();
    private Session session = null;
    private Store store = null;
    private static KLXLogger logger = KLXLogger.getLogger();
    public static HashMap propertiesMap = new HashMap();
    public static final String MAIL_STORE_PROTOCOL = "MAIL_STORE_PROTOCOL";
    public static final String MAIL_IMAPS_STARTTLS_ENABLE = "MAIL_IMAPS_STARTTLS_ENABLE";
    public static final String MAIL_IMAPS_HOST = "MAIL_IMAPS_HOST";
    public static final String MAIL_IMAPS_PORT = "MAIL_IMAPS_PORT";
    public static final String MAIL_USERID = "MAIL_USERID";
    public static final String MAIL_PASSWORD = "MAIL_PASSWORD";
    public static final String CRF_APPROVED_FOLDER = "Inbox/CRF Approved";
    public static final String CRF_REJECTED_FOLDER = "Inbox/CRF Rejected";
    public static final String CRF_CONFLICT_FOLDER = "Inbox/CRF Conflict";
    public static final String CRF_FAILURE_FOLDER = "Inbox/CRF Failure";
    private Folder fdrInbox = null;
    private Folder crfApprovedFolder = null;
    private Folder crfRejectedFolder = null;
    private Folder crfConflictFolder = null;
    private Folder crfFailureFolder = null;

    public MailAccess() {
        super();
    }


    public Connection createDBConnection() throws Exception {
        logger.log(Level.INFO, getClass(), "MailAccess", "Entering createDBConnection method");
        Connection connection = null;
        try {
            InitialContext context = new InitialContext();
            Object obj = context.lookup(DS_NAME);
            DataSource ds = (DataSource) obj;
            connection = ds.getConnection();
        } catch (SQLException sqlException) {
            logger.log(Level.SEVERE, getClass(), "MailAccess", "in createDBConnection - " + sqlException.getMessage());
            sqlException.printStackTrace();
        }
        logger.log(Level.INFO, getClass(), "MailAccess", "Exiting createDBConnection method");
        return connection;
    }

    public Folder getInbox() {
        logger.log(Level.INFO, getClass(), "MailAccess", "Entering getInbox method");

        getMailProperties();
        properties.setProperty("mail.store.protocol", (String) propertiesMap.get(MAIL_STORE_PROTOCOL));
        properties.setProperty("mail.imaps.starttls.enable", (String) propertiesMap.get(MAIL_IMAPS_STARTTLS_ENABLE));
        properties.setProperty("mail.imaps.host", (String) propertiesMap.get(MAIL_IMAPS_HOST));
        properties.setProperty("mail.imaps.port", (String) propertiesMap.get(MAIL_IMAPS_PORT));

        Session emailSession = Session.getDefaultInstance(properties);
        try {
            store = emailSession.getStore("imaps");
            store.connect((String) propertiesMap.get(MAIL_USERID), (String) propertiesMap.get(MAIL_PASSWORD));

            fdrInbox = store.getFolder("INBOX");
            fdrInbox.open(Folder.READ_WRITE);

            crfApprovedFolder = store.getFolder(MailAccess.CRF_APPROVED_FOLDER);
            crfRejectedFolder = store.getFolder(MailAccess.CRF_REJECTED_FOLDER);
            crfConflictFolder = store.getFolder(MailAccess.CRF_CONFLICT_FOLDER);
            crfFailureFolder = store.getFolder(MailAccess.CRF_FAILURE_FOLDER);

            if (!crfApprovedFolder.exists())
                crfApprovedFolder.create(Folder.HOLDS_MESSAGES);
            if (!crfRejectedFolder.exists())
                crfRejectedFolder.create(Folder.HOLDS_MESSAGES);
            if (!crfConflictFolder.exists())
                crfConflictFolder.create(Folder.HOLDS_MESSAGES);
            if (!crfFailureFolder.exists())
                crfFailureFolder.create(Folder.HOLDS_MESSAGES);

        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "MailAccess", "Exception in getInbox method" + e);
            e.printStackTrace();
        }
        logger.log(Level.INFO, getClass(), "MailAccess", "Exiting getInbox method");
        return fdrInbox;
    }

    public void closeInbox(Folder inbox) {
        logger.log(Level.INFO, getClass(), "MailAccess", "Entering closeInbox method");
        try {
            inbox.close(true);
            store.close();
        } catch (MessagingException e) {
            logger.log(Level.SEVERE, getClass(), "MailAccess", "Exception while closing mailbox:" + e);
            e.printStackTrace();
        }
        logger.log(Level.INFO, getClass(), "MailAccess", "Exiting closeInbox method");

    }

    public void getMailProperties() {
        logger.log(Level.INFO, getClass(), "MailAccess", "Entering getMailProperties method");
        ResultSet resultset = null;
        Connection connection = null;
        Statement statement = null;

        try {
            connection = createDBConnection();
            statement = connection.createStatement();

            // fetch all external configuration properties from DB
            resultset = statement.executeQuery(QueryConstants.PROPERTIES_QUERY);

            while (resultset.next()) {
                propertiesMap.put(resultset.getString("KEY"), resultset.getString("VALUE"));
            }

            resultset.close();
            statement.close();

        } catch (Exception exception) {
            logger.log(Level.SEVERE, getClass(), "MailAccess", "Exception in getMailProperties:" + exception);
            exception.printStackTrace();

        } finally {
            try {
                if (resultset != null) {
                    resultset.close();
                }
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException sqlException) {
                logger.log(Level.SEVERE, getClass(), "MailAccess", "Exception in getMailProperties:" + sqlException);
                sqlException.printStackTrace();

            }
        }

        logger.log(Level.INFO, getClass(), "MailAccess", "Exiting getMailProperties method");
    }

    public String processMessageBody(Message message) {
        logger.log(Level.INFO, getClass(), "MailAccess", "Entering processMessageBody method");
        String result = "";
        try {

            if (message.isMimeType("text/plain")) {
                result = message.getContent().toString();

            } else if (message.isMimeType("text/html")) {

                String html = (String) message.getContent();
                result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
                //For Approval email html content is not expected, hence not implemented.
            } else if (message.isMimeType("multipart/*")) {
                MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
                result = getTextFromMimeMultipart(mimeMultipart);

            }

        }

        catch (Exception ae) {
            logger.log(Level.SEVERE, getClass(), "MailAccess", "Exception while closing mailbox:" + ae);
            ae.printStackTrace();

        }
        logger.log(Level.INFO, getClass(), "MailAccess", "Result in process message body =" + result);
        logger.log(Level.INFO, getClass(), "MailAccess", "Exiting processMessageBody method");
        return result;
    }


    private String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws MessagingException, IOException {
        logger.log(Level.INFO, getClass(), "MailAccess", "Entering getTextFromMimeMultipart method");
        String result = "";

        int count = mimeMultipart.getCount();

        for (int i = 0; i < count; i++) {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            if (bodyPart.isMimeType("text/plain")) {
                result = result + "\n" + bodyPart.getContent();
                break;
            } else if (bodyPart.isMimeType("text/html")) {
                String html = (String) bodyPart.getContent();
                result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
                //For Approval email html content is not expected, hence not implemented.
            } else if (bodyPart.getContent() instanceof MimeMultipart) {
                result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
            }
        }
        logger.log(Level.INFO, getClass(), "MailAccess", "Exiting getTextFromMimeMultipart method");
        return result;

    }

    public boolean moveMessages(Message message, String status) throws MessagingException {
        logger.log(Level.INFO, getClass(), "MailAccess", "Entering moveMessages method");
        boolean isMessageMoved = false;

        Message[] arrMessage = { message };
        arrMessage[0] = message;


        switch (status.toUpperCase()) {
            //Move all the approved mails to CRF Approve folder
        case EmailApproval.ACTION_APPROVE:
            //Mark mail as read
            fdrInbox.setFlags(arrMessage, new Flags(Flags.Flag.SEEN), true);
            //Copy message to CRF Approve folder
            fdrInbox.copyMessages(arrMessage, crfApprovedFolder);
            //Delete message from Inbox
            fdrInbox.setFlags(arrMessage, new Flags(Flags.Flag.DELETED), true);
            isMessageMoved = true;
            break;

        case EmailApproval.ACTION_REJECT:
            //Mark mail as read
            fdrInbox.setFlags(arrMessage, new Flags(Flags.Flag.SEEN), true);
            //Copy message to CRF Reject folder
            fdrInbox.copyMessages(arrMessage, crfRejectedFolder);
            //Delete message from Inbox
            fdrInbox.setFlags(arrMessage, new Flags(Flags.Flag.DELETED), true);
            isMessageMoved = true;
            break;

        case EmailApproval.ACTION_CONFLICT:
            //Mark mail as read
            fdrInbox.setFlags(arrMessage, new Flags(Flags.Flag.SEEN), true);
            //Copy message to CRF Conflict folder
            fdrInbox.copyMessages(arrMessage, crfConflictFolder);
            //Delete message from Inbox
            fdrInbox.setFlags(arrMessage, new Flags(Flags.Flag.DELETED), true);
            isMessageMoved = true;
            break;
        case EmailApproval.TASK_FAILURE:
            //Mark mail as read
            fdrInbox.setFlags(arrMessage, new Flags(Flags.Flag.SEEN), true);
            //Copy message to CRF Conflict folder
            fdrInbox.copyMessages(arrMessage, crfFailureFolder);
            //Delete message from Inbox
            fdrInbox.setFlags(arrMessage, new Flags(Flags.Flag.DELETED), true);
            isMessageMoved = true;
            break;
        default:
            //Mark mail as read
            fdrInbox.setFlags(arrMessage, new Flags(Flags.Flag.SEEN), true);
        }

        logger.log(Level.INFO, getClass(), "MailAccess", "Exiting moveMessages method");
        return isMessageMoved;

    }
}
