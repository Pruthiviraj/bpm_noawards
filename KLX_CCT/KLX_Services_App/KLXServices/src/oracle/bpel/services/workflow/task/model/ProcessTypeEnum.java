
package oracle.bpel.services.workflow.task.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for processTypeEnum.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="processTypeEnum"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="BPEL"/&gt;
 *     &lt;enumeration value="OWF"/&gt;
 *     &lt;enumeration value="OTHER"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "processTypeEnum")
@XmlEnum
public enum ProcessTypeEnum {

    BPEL,
    OWF,
    OTHER;

    public String value() {
        return name();
    }

    public static ProcessTypeEnum fromValue(String v) {
        return valueOf(v);
    }

}
