
package oracle.bpel.services.workflow.task.command.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for commandResultType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="commandResultType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="SUCCESS"/&gt;
 *     &lt;enumeration value="NONSTATECHANGE_FAILED"/&gt;
 *     &lt;enumeration value="STATECHANGE_FAILED"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "commandResultType")
@XmlEnum
public enum CommandResultType {

    SUCCESS,
    NONSTATECHANGE_FAILED,
    STATECHANGE_FAILED;

    public String value() {
        return name();
    }

    public static CommandResultType fromValue(String v) {
        return valueOf(v);
    }

}
