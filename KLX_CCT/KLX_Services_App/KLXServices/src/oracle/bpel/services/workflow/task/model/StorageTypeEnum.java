
package oracle.bpel.services.workflow.task.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for storageTypeEnum.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="storageTypeEnum"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="TASK"/&gt;
 *     &lt;enumeration value="UCM"/&gt;
 *     &lt;enumeration value="URL"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "storageTypeEnum")
@XmlEnum
public enum StorageTypeEnum {

    TASK,
    UCM,
    URL;

    public String value() {
        return name();
    }

    public static StorageTypeEnum fromValue(String v) {
        return valueOf(v);
    }

}
