
package oracle.bpel.services.workflow.evidence.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WFTypeEnumType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="WFTypeEnumType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="OWF"/&gt;
 *     &lt;enumeration value="BPELWF"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "WFTypeEnumType")
@XmlEnum
public enum WFTypeEnumType {

    OWF,
    BPELWF;

    public String value() {
        return name();
    }

    public static WFTypeEnumType fromValue(String v) {
        return valueOf(v);
    }

}
