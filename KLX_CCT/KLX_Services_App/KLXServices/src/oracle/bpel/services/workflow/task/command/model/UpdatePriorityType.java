
package oracle.bpel.services.workflow.task.command.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updatePriorityType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="updatePriorityType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="UPDATE"/&gt;
 *     &lt;enumeration value="INCREMENT"/&gt;
 *     &lt;enumeration value="DECREMENT"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "updatePriorityType")
@XmlEnum
public enum UpdatePriorityType {

    UPDATE,
    INCREMENT,
    DECREMENT;

    public String value() {
        return name();
    }

    public static UpdatePriorityType fromValue(String v) {
        return valueOf(v);
    }

}
