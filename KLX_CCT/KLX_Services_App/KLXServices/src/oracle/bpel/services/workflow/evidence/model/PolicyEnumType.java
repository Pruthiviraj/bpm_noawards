
package oracle.bpel.services.workflow.evidence.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PolicyEnumType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PolicyEnumType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="PASSWORD"/&gt;
 *     &lt;enumeration value="CERTIFICATE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "PolicyEnumType")
@XmlEnum
public enum PolicyEnumType {

    PASSWORD,
    CERTIFICATE;

    public String value() {
        return name();
    }

    public static PolicyEnumType fromValue(String v) {
        return valueOf(v);
    }

}
