
package oracle.bpel.services.workflow.user.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for actionTypeEnum.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="actionTypeEnum"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NOOPERATION"/&gt;
 *     &lt;enumeration value="REASSIGN"/&gt;
 *     &lt;enumeration value="DELEGATE"/&gt;
 *     &lt;enumeration value="SETOUTCOME"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "actionTypeEnum")
@XmlEnum
public enum ActionTypeEnum {

    NOOPERATION,
    REASSIGN,
    DELEGATE,
    SETOUTCOME;

    public String value() {
        return name();
    }

    public static ActionTypeEnum fromValue(String v) {
        return valueOf(v);
    }

}
