
package oracle.bpel.services.workflow.metadata.routingslip.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for relativeDateType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="relativeDateType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ASSIGNED"/&gt;
 *     &lt;enumeration value="EXPIRATION"/&gt;
 *     &lt;enumeration value="DUE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "relativeDateType")
@XmlEnum
public enum RelativeDateType {

    ASSIGNED,
    EXPIRATION,
    DUE;

    public String value() {
        return name();
    }

    public static RelativeDateType fromValue(String v) {
        return valueOf(v);
    }

}
