
package com.oracle.xmlns.bpel.workflow.taskservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import oracle.bpel.services.workflow.metadata.routingslip.model.ParticipantsType;


/**
 * <p>Java class for routeTaskType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="routeTaskType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/bpel/workflow/taskService}taskServiceContextTaskTaskIdBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://xmlns.oracle.com/bpel/workflow/routingSlip}participants"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "routeTaskType", propOrder = { "participants" })
public class RouteTaskType extends TaskServiceContextTaskTaskIdBaseType {

    @XmlElement(namespace = "http://xmlns.oracle.com/bpel/workflow/routingSlip", required = true)
    protected ParticipantsType participants;

    /**
     * Gets the value of the participants property.
     *
     * @return
     *     possible object is
     *     {@link ParticipantsType }
     *
     */
    public ParticipantsType getParticipants() {
        return participants;
    }

    /**
     * Sets the value of the participants property.
     *
     * @param value
     *     allowed object is
     *     {@link ParticipantsType }
     *
     */
    public void setParticipants(ParticipantsType value) {
        this.participants = value;
    }

}
