package com.klx.constants;


public class ServiceConstants {
    public static final String FAILURE = "FAILURE";
    public static final String PROPOSAL_MANAGER = "Proposal Manager";
    public static final String PRICING_MANAGER = "Pricing Manager";
    public static final String QUALITY = "Quality";
    public static final String LEGAL = "Legal";
    public static final String OPERATIONS = "Operations";
    public static final String COMMODITY = "Commodity";
    public static final String ADVANCED_SOURCING = "Advanced Sourcing";
    public static final String ACCOUNTING_DIRECTOR = "Director Of Acconting";
    public static final String SALES_VP = "VP Sales";
    public static final String FINANCE_VP = "VP Finance";
    public static final String OPERATION_VP = "VP Operations";
    public static final String STATE = "state";
    public static final String MANAGE_APPROVAL = "ManageApproval";
    public static final String TASK_NUMBER = "TASKNUMBER";
    public static final String TASKID = "TASKID";
    public static final String taskID = "taskId";
    public static final String TITLE = "TITLE";
    public static final String OUTCOME = "OUTCOME";
    public static final String TEXTATTRIBUTE1 = "TEXTATTRIBUTE1";
    public static final String TEXTATTRIBUTE2 = "TEXTATTRIBUTE2";
    public static final String TEXTATTRIBUTE3 = "TEXTATTRIBUTE3";
    public static final String TEXTATTRIBUTE4 = "TEXTATTRIBUTE4";
    public static final String TEXTATTRIBUTE5 = "TEXTATTRIBUTE5";
    public static final String TEXTATTRIBUTE6 = "TEXTATTRIBUTE6";
    public static final String TEXTATTRIBUTE7 = "TEXTATTRIBUTE7";
    public static final String TEXTATTRIBUTE8 = "TEXTATTRIBUTE8";
    public static final String TEXTATTRIBUTE9 = "TEXTATTRIBUTE9";
    public static final String TEXTATTRIBUTE10 = "TEXTATTRIBUTE10";

    public static final String NUMBERATTRIBUTE1 = "NUMBERATTRIBUTE1";
    public static final String NUMBERATTRIBUTE2 = "NUMBERATTRIBUTE2";
    public static final String NUMBERATTRIBUTE3 = "NUMBERATTRIBUTE3";
    public static final String OWNERUSER = "ownerUser";
    
    public static final String SUCCESS = "Success";
    public static final String[] approvalTasks = {"APPROVE", "REJECT", "PASS", "CANCEL", "PENDING"};


}
