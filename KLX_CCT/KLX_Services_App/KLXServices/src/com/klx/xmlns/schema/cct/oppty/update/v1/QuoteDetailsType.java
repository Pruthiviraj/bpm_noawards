
package com.klx.xmlns.schema.cct.oppty.update.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QuoteDetailsType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="QuoteDetailsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Details" type="{http://xmlns.klx.com/schema/cct/oppty/update/V1.0}DetailsType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuoteDetailsType", propOrder = { "details" })
public class QuoteDetailsType {

    @XmlElement(name = "Details", required = true)
    protected DetailsType details;

    /**
     * Gets the value of the details property.
     *
     * @return
     *     possible object is
     *     {@link DetailsType }
     *
     */
    public DetailsType getDetails() {
        return details;
    }

    /**
     * Sets the value of the details property.
     *
     * @param value
     *     allowed object is
     *     {@link DetailsType }
     *
     */
    public void setDetails(DetailsType value) {
        this.details = value;
    }

}
