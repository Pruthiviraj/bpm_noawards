
package com.klx.xmlns.schema.opportunity.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for OpportunityHeaderInfoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="OpportunityHeaderInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Source" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OpportunityNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OpportunityId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="MessageId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OpportunityName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CustomerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TriggerTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OpportunityType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CustomerDueDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="ProjectNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpportunityHeaderInfoType",
         propOrder =
         { "source", "opportunityNumber", "opportunityId", "messageId", "opportunityName", "customerName",
           "triggerTimestamp", "status", "opportunityType", "customerDueDate", "projectNumber", "opportunityStatus",
           "isCustomerDueDateChanged", "oldCustomerDueDate", "contractType"
    })
public class OpportunityHeaderInfoType {

    @XmlElement(name = "Source", required = true)
    protected String source;
    @XmlElement(name = "OpportunityNumber", required = true)
    protected String opportunityNumber;
    @XmlElement(name = "OpportunityId", required = true)
    protected String opportunityId;
    @XmlElement(name = "MessageId")
    protected String messageId;
    @XmlElement(name = "OpportunityName")
    protected String opportunityName;
    @XmlElement(name = "CustomerName")
    protected String customerName;
    @XmlSchemaType(name = "dateTime")
    @XmlElement(name = "TriggerTimestamp", required = true)
    protected XMLGregorianCalendar triggerTimestamp;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "OpportunityType", required = true)
    protected String opportunityType;
    @XmlElement(name = "CustomerDueDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar customerDueDate;
    @XmlElement(name = "ProjectNumber")
    protected String projectNumber;
    @XmlElement(name = "IsCustomerDueDateChanged")
    protected Boolean isCustomerDueDateChanged;
    @XmlElement(name = "OldCustomerDueDate")
    protected String oldCustomerDueDate;
    @XmlElement(name = "OpportunityStatus")
    protected String opportunityStatus;
    @XmlElement(name = "ContractType")
    protected String contractType;

    /**
     * Gets the value of the source property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSource(String value) {
        this.source = value;
    }

    /**
     * Gets the value of the opportunityNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOpportunityNumber() {
        return opportunityNumber;
    }

    /**
     * Sets the value of the opportunityNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOpportunityNumber(String value) {
        this.opportunityNumber = value;
    }

    /**
     * Gets the value of the opportunityId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOpportunityId() {
        return opportunityId;
    }

    /**
     * Sets the value of the opportunityId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOpportunityId(String value) {
        this.opportunityId = value;
    }

    /**
     * Gets the value of the messageId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMessageId() {
        return messageId;
    }

    /**
     * Sets the value of the messageId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMessageId(String value) {
        this.messageId = value;
    }

    /**
     * Gets the value of the opportunityName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOpportunityName() {
        return opportunityName;
    }

    /**
     * Sets the value of the opportunityName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOpportunityName(String value) {
        this.opportunityName = value;
    }

    /**
     * Gets the value of the customerName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * Sets the value of the customerName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCustomerName(String value) {
        this.customerName = value;
    }

    /**
     * Gets the value of the triggerTimestamp property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getTriggerTimestamp() {
        return triggerTimestamp;
    }

    /**
     * Sets the value of the triggerTimestamp property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setTriggerTimestamp(XMLGregorianCalendar value) {
        this.triggerTimestamp = value;
    }

    /**
     * Gets the value of the status property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the opportunityType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOpportunityType() {
        return opportunityType;
    }

    /**
     * Sets the value of the opportunityType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOpportunityType(String value) {
        this.opportunityType = value;
    }

    /**
     * Gets the value of the customerDueDate property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getCustomerDueDate() {
        return customerDueDate;
    }

    /**
     * Sets the value of the customerDueDate property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setCustomerDueDate(XMLGregorianCalendar value) {
        this.customerDueDate = value;
    }

    /**
     * Gets the value of the projectNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getProjectNumber() {
        return projectNumber;
    }

    /**
     * Sets the value of the projectNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setProjectNumber(String value) {
        this.projectNumber = value;
    }

    /**
     * Gets the value of the oldCustomerDueDate property.
     *
     * @return
     * possible object is
     * {@link XMLGregorianCalendar}
     *
     */
    public String getOldCustomerDueDate() {
        return oldCustomerDueDate;
    }

    /**
     * Gets the value of the opportunityStatus property.
     *
     * @return
     * possible object is
     * {@link String}
     *
     */
    public String getOpportunityStatus() {
        return opportunityStatus;
    }

    /**
     * Gets the value of the isCustomerDueDateChanged property.
     *
     * @return
     * possible object is
     * {@link Boolean}
     *
     */
    public Boolean isIsCustomerDueDateChanged() {
        return isCustomerDueDateChanged;
    }

    /**
     * Sets the value of the isCustomerDueDateChanged property.
     *
     * @param value
     * allowed object is
     * {@link Boolean}
     *
     */
    public void setIsCustomerDueDateChanged(Boolean value) {
        this.isCustomerDueDateChanged = value;
    }

    /**
     * Sets the value of the oldCustomerDueDate property.
     *
     * @param value
     * allowed object is
     * {@link XMLGregorianCalendar}
     *
     */
    public void setOldCustomerDueDate(String value) {
        this.oldCustomerDueDate = value;
    }

    /**
     * Sets the value of the opportunityStatus property.
     *
     * @param value
     * allowed object is
     * {@link String}
     *
     */
    public void setOpportunityStatus(String value) {
        this.opportunityStatus = value;
    }

    /**
     * Gets the value of the contractType property.
     *
     * @return
     * possible object is
     * {@link String}
     *
     */
    public String getContractType() {
        return contractType;
    }

    /**
     * Sets the value of the contractType property.
     *
     * @param value
     * allowed object is
     * {@link String}
     *
     */
    public void setContractType(String value) {
        this.contractType = value;
    }

}
