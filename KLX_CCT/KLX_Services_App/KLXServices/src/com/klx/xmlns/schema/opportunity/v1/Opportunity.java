
package com.klx.xmlns.schema.opportunity.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OpportunityHeaderInfo" type="{http://xmlns.klx.com/schema/Opportunity/V1.0}OpportunityHeaderInfoType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "opportunityHeaderInfo" })
@XmlRootElement(name = "Opportunity")
public class Opportunity {

    @XmlElement(name = "OpportunityHeaderInfo", required = true)
    protected OpportunityHeaderInfoType opportunityHeaderInfo;

    /**
     * Gets the value of the opportunityHeaderInfo property.
     *
     * @return
     *     possible object is
     *     {@link OpportunityHeaderInfoType }
     *
     */
    public OpportunityHeaderInfoType getOpportunityHeaderInfo() {
        return opportunityHeaderInfo;
    }

    /**
     * Sets the value of the opportunityHeaderInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link OpportunityHeaderInfoType }
     *
     */
    public void setOpportunityHeaderInfo(OpportunityHeaderInfoType value) {
        this.opportunityHeaderInfo = value;
    }

}
