
package com.klx.xmlns.schema.cct.oppty.update.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.klx.xmlns.schema.cct.oppty.update.v1 package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpptyQuote_QNAME =
        new QName("http://xmlns.klx.com/schema/cct/oppty/update/V1.0", "OpptyQuote");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.klx.xmlns.schema.cct.oppty.update.v1
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link QuoteDetailsType }
     *
     */
    public QuoteDetailsType createQuoteDetailsType() {
        return new QuoteDetailsType();
    }

    /**
     * Create an instance of {@link DetailsType }
     *
     */
    public DetailsType createDetailsType() {
        return new DetailsType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuoteDetailsType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://xmlns.klx.com/schema/cct/oppty/update/V1.0", name = "OpptyQuote")
    public JAXBElement<QuoteDetailsType> createOpptyQuote(QuoteDetailsType value) {
        return new JAXBElement<QuoteDetailsType>(_OpptyQuote_QNAME, QuoteDetailsType.class, null, value);
    }

}
