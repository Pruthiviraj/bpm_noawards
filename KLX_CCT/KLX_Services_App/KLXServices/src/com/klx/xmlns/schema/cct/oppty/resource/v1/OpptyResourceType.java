
package com.klx.xmlns.schema.cct.oppty.resource.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OpptyResourceType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="OpptyResourceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Resource" type="{http://xmlns.klx.com/schema/cct/oppty/resource/V1.0}ResourceType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpptyResourceType", propOrder = { "resource" })
public class OpptyResourceType {

    @XmlElement(name = "Resource", required = true)
    protected ResourceType resource;

    /**
     * Gets the value of the resource property.
     *
     * @return
     *     possible object is
     *     {@link ResourceType }
     *
     */
    public ResourceType getResource() {
        return resource;
    }

    /**
     * Sets the value of the resource property.
     *
     * @param value
     *     allowed object is
     *     {@link ResourceType }
     *
     */
    public void setResource(ResourceType value) {
        this.resource = value;
    }

}
