
package com.klx.xmlns.schema.cct.notification.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CCTNotificationHeaderInfoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CCTNotificationHeaderInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Source" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OpportunityId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OpportunityNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="NotificationTemplateId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="MessageId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="isAttachmentApplicable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OpptyClosureStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Role" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Action" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="To" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Cc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Bcc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EmailBody" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EmailSubject" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ApprovalUserId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ApprovalUserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ApprovalUserEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Attribute1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Attribute2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Attribute3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Attribute4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CCTNotificationHeaderInfoType",
         propOrder =
         { "source", "opportunityId", "opportunityNumber", "notificationTemplateId", "messageId",
           "isAttachmentApplicable", "opptyClosureStatus", "role", "action", "to", "cc", "bcc", "emailBody",
           "emailSubject", "approvalUserId", "approvalUserName", "approvalUserEmail", "attribute1", "attribute2",
           "attribute3", "attribute4"
    })
public class CCTNotificationHeaderInfoType {

    @XmlElement(name = "Source", required = true)
    protected String source;
    @XmlElement(name = "OpportunityId", required = true)
    protected String opportunityId;
    @XmlElement(name = "OpportunityNumber", required = true)
    protected String opportunityNumber;
    @XmlElement(name = "NotificationTemplateId", required = true)
    protected String notificationTemplateId;
    @XmlElement(name = "MessageId")
    protected String messageId;
    protected String isAttachmentApplicable;
    @XmlElement(name = "OpptyClosureStatus")
    protected String opptyClosureStatus;
    @XmlElement(name = "Role")
    protected String role;
    @XmlElement(name = "Action")
    protected String action;
    @XmlElement(name = "To")
    protected String to;
    @XmlElement(name = "Cc")
    protected String cc;
    @XmlElement(name = "Bcc")
    protected String bcc;
    @XmlElement(name = "EmailBody")
    protected String emailBody;
    @XmlElement(name = "EmailSubject")
    protected String emailSubject;
    @XmlElement(name = "ApprovalUserId")
    protected String approvalUserId;
    @XmlElement(name = "ApprovalUserName")
    protected String approvalUserName;
    @XmlElement(name = "ApprovalUserEmail")
    protected String approvalUserEmail;
    @XmlElement(name = "Attribute1")
    protected String attribute1;
    @XmlElement(name = "Attribute2")
    protected String attribute2;
    @XmlElement(name = "Attribute3")
    protected String attribute3;
    @XmlElement(name = "Attribute4")
    protected String attribute4;

    /**
     * Gets the value of the source property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSource(String value) {
        this.source = value;
    }

    /**
     * Gets the value of the opportunityId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOpportunityId() {
        return opportunityId;
    }

    /**
     * Sets the value of the opportunityId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOpportunityId(String value) {
        this.opportunityId = value;
    }

    /**
     * Gets the value of the opportunityNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOpportunityNumber() {
        return opportunityNumber;
    }

    /**
     * Sets the value of the opportunityNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOpportunityNumber(String value) {
        this.opportunityNumber = value;
    }

    /**
     * Gets the value of the notificationTemplateId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNotificationTemplateId() {
        return notificationTemplateId;
    }

    /**
     * Sets the value of the notificationTemplateId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNotificationTemplateId(String value) {
        this.notificationTemplateId = value;
    }

    /**
     * Gets the value of the messageId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMessageId() {
        return messageId;
    }

    /**
     * Sets the value of the messageId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMessageId(String value) {
        this.messageId = value;
    }

    /**
     * Gets the value of the isAttachmentApplicable property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getIsAttachmentApplicable() {
        return isAttachmentApplicable;
    }

    /**
     * Sets the value of the isAttachmentApplicable property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIsAttachmentApplicable(String value) {
        this.isAttachmentApplicable = value;
    }

    /**
     * Gets the value of the opptyClosureStatus property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOpptyClosureStatus() {
        return opptyClosureStatus;
    }

    /**
     * Sets the value of the opptyClosureStatus property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOpptyClosureStatus(String value) {
        this.opptyClosureStatus = value;
    }

    /**
     * Gets the value of the role property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getRole() {
        return role;
    }

    /**
     * Sets the value of the role property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setRole(String value) {
        this.role = value;
    }

    /**
     * Gets the value of the action property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAction(String value) {
        this.action = value;
    }

    /**
     * Gets the value of the to property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTo() {
        return to;
    }

    /**
     * Sets the value of the to property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTo(String value) {
        this.to = value;
    }

    /**
     * Gets the value of the cc property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCc() {
        return cc;
    }

    /**
     * Sets the value of the cc property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCc(String value) {
        this.cc = value;
    }

    /**
     * Gets the value of the bcc property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getBcc() {
        return bcc;
    }

    /**
     * Sets the value of the bcc property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setBcc(String value) {
        this.bcc = value;
    }

    /**
     * Gets the value of the emailBody property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEmailBody() {
        return emailBody;
    }

    /**
     * Sets the value of the emailBody property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEmailBody(String value) {
        this.emailBody = value;
    }

    /**
     * Gets the value of the emailSubject property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEmailSubject() {
        return emailSubject;
    }

    /**
     * Sets the value of the emailSubject property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEmailSubject(String value) {
        this.emailSubject = value;
    }

    /**
     * Gets the value of the approvalUserId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getApprovalUserId() {
        return approvalUserId;
    }

    /**
     * Sets the value of the approvalUserId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setApprovalUserId(String value) {
        this.approvalUserId = value;
    }

    /**
     * Gets the value of the approvalUserName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getApprovalUserName() {
        return approvalUserName;
    }

    /**
     * Sets the value of the approvalUserName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setApprovalUserName(String value) {
        this.approvalUserName = value;
    }

    /**
     * Gets the value of the approvalUserEmail property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getApprovalUserEmail() {
        return approvalUserEmail;
    }

    /**
     * Sets the value of the approvalUserEmail property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setApprovalUserEmail(String value) {
        this.approvalUserEmail = value;
    }

    /**
     * Gets the value of the attribute1 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAttribute1() {
        return attribute1;
    }

    /**
     * Sets the value of the attribute1 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAttribute1(String value) {
        this.attribute1 = value;
    }

    /**
     * Gets the value of the attribute2 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAttribute2() {
        return attribute2;
    }

    /**
     * Sets the value of the attribute2 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAttribute2(String value) {
        this.attribute2 = value;
    }

    /**
     * Gets the value of the attribute3 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAttribute3() {
        return attribute3;
    }

    /**
     * Sets the value of the attribute3 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAttribute3(String value) {
        this.attribute3 = value;
    }

    /**
     * Gets the value of the attribute4 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAttribute4() {
        return attribute4;
    }

    /**
     * Sets the value of the attribute4 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAttribute4(String value) {
        this.attribute4 = value;
    }

}
