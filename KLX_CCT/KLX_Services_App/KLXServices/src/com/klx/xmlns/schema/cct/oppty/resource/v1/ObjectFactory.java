
package com.klx.xmlns.schema.cct.oppty.resource.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.klx.xmlns.schema.cct.oppty.resource.v1 package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpptyResource_QNAME =
        new QName("http://xmlns.klx.com/schema/cct/oppty/resource/V1.0", "OpptyResource");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.klx.xmlns.schema.cct.oppty.resource.v1
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpptyResourceType }
     *
     */
    public OpptyResourceType createOpptyResourceType() {
        return new OpptyResourceType();
    }

    /**
     * Create an instance of {@link ResourceType }
     *
     */
    public ResourceType createResourceType() {
        return new ResourceType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpptyResourceType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://xmlns.klx.com/schema/cct/oppty/resource/V1.0", name = "OpptyResource")
    public JAXBElement<OpptyResourceType> createOpptyResource(OpptyResourceType value) {
        return new JAXBElement<OpptyResourceType>(_OpptyResource_QNAME, OpptyResourceType.class, null, value);
    }

}
