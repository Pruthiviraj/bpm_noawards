
package com.klx.xmlns.schema.cct.oppty.update.v1;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DetailsType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="DetailsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OpptyId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OpptyNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="QuoteId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="QuoteRevision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Comment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="NetMargin" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="DocumentReadyToSubmit_c" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DetailsType",
         propOrder =
         { "opptyId", "opptyNumber", "quoteId", "quoteRevision", "comment", "amount", "netMargin",
           "documentReadyToSubmitC"
    })
public class DetailsType {

    @XmlElement(name = "OpptyId", required = true)
    protected String opptyId;
    @XmlElement(name = "OpptyNumber", required = true)
    protected String opptyNumber;
    @XmlElement(name = "QuoteId")
    protected String quoteId;
    @XmlElement(name = "QuoteRevision")
    protected String quoteRevision;
    @XmlElement(name = "Comment")
    protected String comment;
    @XmlElement(name = "Amount")
    protected BigDecimal amount;
    @XmlElement(name = "NetMargin")
    protected BigDecimal netMargin;
    @XmlElement(name = "DocumentReadyToSubmit_c")
    protected Boolean documentReadyToSubmitC;

    /**
     * Gets the value of the opptyId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOpptyId() {
        return opptyId;
    }

    /**
     * Sets the value of the opptyId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOpptyId(String value) {
        this.opptyId = value;
    }

    /**
     * Gets the value of the opptyNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOpptyNumber() {
        return opptyNumber;
    }

    /**
     * Sets the value of the opptyNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOpptyNumber(String value) {
        this.opptyNumber = value;
    }

    /**
     * Gets the value of the quoteId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getQuoteId() {
        return quoteId;
    }

    /**
     * Sets the value of the quoteId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setQuoteId(String value) {
        this.quoteId = value;
    }

    /**
     * Gets the value of the quoteRevision property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getQuoteRevision() {
        return quoteRevision;
    }

    /**
     * Sets the value of the quoteRevision property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setQuoteRevision(String value) {
        this.quoteRevision = value;
    }

    /**
     * Gets the value of the comment property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setComment(String value) {
        this.comment = value;
    }

    /**
     * Gets the value of the amount property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public BigDecimal getAmount() {
        /*return amount;*/

        return amount;
    }

    /**
     * Sets the value of the amount property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the netMargin property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public BigDecimal getNetMargin() {
        /*return netMargin;*/

        return netMargin;
    }

    /**
     * Sets the value of the netMargin property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setNetMargin(BigDecimal value) {
        this.netMargin = value;
    }

    /**
     * Gets the value of the documentReadyToSubmitC property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    public Boolean isDocumentReadyToSubmitC() {
        return documentReadyToSubmitC;
    }

    /**
     * Sets the value of the documentReadyToSubmitC property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    public void setDocumentReadyToSubmitC(Boolean value) {
        this.documentReadyToSubmitC = value;
    }

}
