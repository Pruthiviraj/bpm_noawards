
package com.klx.xmlns.schema.cct.oppty.resource.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ResourceType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ResourceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UserId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OpptyStatus" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OpptyId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ResourceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UserCreationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="EmailId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="GroupId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Manager" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CreatedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LastUpdateDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="RoleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResourceType",
         propOrder =
         { "userId", "opptyStatus", "opptyId", "resourceId", "fullName", "userCreationDate", "emailId", "groupId",
           "manager", "createdBy", "lastUpdateDate", "roleName"
    })
public class ResourceType {

    @XmlElement(name = "UserId", required = true)
    protected String userId;
    @XmlElement(name = "OpptyStatus", required = true)
    protected String opptyStatus;
    @XmlElement(name = "OpptyId", required = true)
    protected String opptyId;
    @XmlElement(name = "ResourceId")
    protected String resourceId;
    @XmlElement(name = "FullName")
    protected String fullName;
    @XmlElement(name = "UserCreationDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar userCreationDate;
    @XmlElement(name = "EmailId")
    protected String emailId;
    @XmlElement(name = "GroupId")
    protected String groupId;
    @XmlElement(name = "Manager")
    protected String manager;
    @XmlElement(name = "CreatedBy")
    protected String createdBy;
    @XmlElement(name = "LastUpdateDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar lastUpdateDate;
    @XmlElement(name = "RoleName")
    protected String roleName;

    /**
     * Gets the value of the userId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the opptyStatus property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOpptyStatus() {
        return opptyStatus;
    }

    /**
     * Sets the value of the opptyStatus property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOpptyStatus(String value) {
        this.opptyStatus = value;
    }

    /**
     * Gets the value of the opptyId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOpptyId() {
        return opptyId;
    }

    /**
     * Sets the value of the opptyId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOpptyId(String value) {
        this.opptyId = value;
    }

    /**
     * Gets the value of the resourceId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getResourceId() {
        return resourceId;
    }

    /**
     * Sets the value of the resourceId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setResourceId(String value) {
        this.resourceId = value;
    }

    /**
     * Gets the value of the fullName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Sets the value of the fullName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFullName(String value) {
        this.fullName = value;
    }

    /**
     * Gets the value of the userCreationDate property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getUserCreationDate() {
        return userCreationDate;
    }

    /**
     * Sets the value of the userCreationDate property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setUserCreationDate(XMLGregorianCalendar value) {
        this.userCreationDate = value;
    }

    /**
     * Gets the value of the emailId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * Sets the value of the emailId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEmailId(String value) {
        this.emailId = value;
    }

    /**
     * Gets the value of the groupId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Sets the value of the groupId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Gets the value of the manager property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getManager() {
        return manager;
    }

    /**
     * Sets the value of the manager property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setManager(String value) {
        this.manager = value;
    }

    /**
     * Gets the value of the createdBy property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the value of the createdBy property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCreatedBy(String value) {
        this.createdBy = value;
    }

    /**
     * Gets the value of the lastUpdateDate property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getLastUpdateDate() {
        return lastUpdateDate;
    }

    /**
     * Sets the value of the lastUpdateDate property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setLastUpdateDate(XMLGregorianCalendar value) {
        this.lastUpdateDate = value;
    }

    /**
     * Gets the value of the roleName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * Sets the value of the roleName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setRoleName(String value) {
        this.roleName = value;
    }

}
