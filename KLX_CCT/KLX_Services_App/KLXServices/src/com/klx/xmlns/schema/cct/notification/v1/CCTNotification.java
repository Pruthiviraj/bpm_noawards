
package com.klx.xmlns.schema.cct.notification.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CCTNotificationHeader" type="{http://xmlns.klx.com/schema/cct/notification/V1.0}CCTNotificationHeaderInfoType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "cctNotificationHeader" })
@XmlRootElement(name = "CCTNotification")
public class CCTNotification {

    @XmlElement(name = "CCTNotificationHeader", required = true)
    protected CCTNotificationHeaderInfoType cctNotificationHeader;

    /**
     * Gets the value of the cctNotificationHeader property.
     *
     * @return
     *     possible object is
     *     {@link CCTNotificationHeaderInfoType }
     *
     */
    public CCTNotificationHeaderInfoType getCCTNotificationHeader() {
        return cctNotificationHeader;
    }

    /**
     * Sets the value of the cctNotificationHeader property.
     *
     * @param value
     *     allowed object is
     *     {@link CCTNotificationHeaderInfoType }
     *
     */
    public void setCCTNotificationHeader(CCTNotificationHeaderInfoType value) {
        this.cctNotificationHeader = value;
    }

}
