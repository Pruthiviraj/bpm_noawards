
package com.klx.db.fault.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for DBFaultType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="DBFaultType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ComponentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ComponenetID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ErrorId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ErrorMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ErrorType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MessageId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TimeStamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="MessagePaylod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Attribute1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Attribute2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Attribute3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Attribute4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Attribute5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DBFaultType",
         propOrder =
         { "componentName", "componenetID", "errorId", "errorMessage", "errorType", "messageId", "timeStamp",
           "messagePaylod", "attribute1", "attribute2", "attribute3", "attribute4", "attribute5"
    })
public class DBFaultType {

    @XmlElement(name = "ComponentName")
    protected String componentName;
    @XmlElement(name = "ComponenetID")
    protected String componenetID;
    @XmlElement(name = "ErrorId")
    protected String errorId;
    @XmlElement(name = "ErrorMessage")
    protected String errorMessage;
    @XmlElement(name = "ErrorType")
    protected String errorType;
    @XmlElement(name = "MessageId")
    protected String messageId;
    @XmlElement(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlElement(name = "MessagePaylod")
    protected String messagePaylod;
    @XmlElement(name = "Attribute1")
    protected String attribute1;
    @XmlElement(name = "Attribute2")
    protected String attribute2;
    @XmlElement(name = "Attribute3")
    protected String attribute3;
    @XmlElement(name = "Attribute4")
    protected String attribute4;
    @XmlElement(name = "Attribute5")
    protected String attribute5;

    /**
     * Gets the value of the componentName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getComponentName() {
        return componentName;
    }

    /**
     * Sets the value of the componentName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setComponentName(String value) {
        this.componentName = value;
    }

    /**
     * Gets the value of the componenetID property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getComponenetID() {
        return componenetID;
    }

    /**
     * Sets the value of the componenetID property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setComponenetID(String value) {
        this.componenetID = value;
    }

    /**
     * Gets the value of the errorId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getErrorId() {
        return errorId;
    }

    /**
     * Sets the value of the errorId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setErrorId(String value) {
        this.errorId = value;
    }

    /**
     * Gets the value of the errorMessage property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the value of the errorMessage property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setErrorMessage(String value) {
        this.errorMessage = value;
    }

    /**
     * Gets the value of the errorType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getErrorType() {
        return errorType;
    }

    /**
     * Sets the value of the errorType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setErrorType(String value) {
        this.errorType = value;
    }

    /**
     * Gets the value of the messageId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMessageId() {
        return messageId;
    }

    /**
     * Sets the value of the messageId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMessageId(String value) {
        this.messageId = value;
    }

    /**
     * Gets the value of the timeStamp property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Sets the value of the timeStamp property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Gets the value of the messagePaylod property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMessagePaylod() {
        return messagePaylod;
    }

    /**
     * Sets the value of the messagePaylod property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMessagePaylod(String value) {
        this.messagePaylod = value;
    }

    /**
     * Gets the value of the attribute1 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAttribute1() {
        return attribute1;
    }

    /**
     * Sets the value of the attribute1 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAttribute1(String value) {
        this.attribute1 = value;
    }

    /**
     * Gets the value of the attribute2 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAttribute2() {
        return attribute2;
    }

    /**
     * Sets the value of the attribute2 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAttribute2(String value) {
        this.attribute2 = value;
    }

    /**
     * Gets the value of the attribute3 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAttribute3() {
        return attribute3;
    }

    /**
     * Sets the value of the attribute3 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAttribute3(String value) {
        this.attribute3 = value;
    }

    /**
     * Gets the value of the attribute4 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAttribute4() {
        return attribute4;
    }

    /**
     * Sets the value of the attribute4 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAttribute4(String value) {
        this.attribute4 = value;
    }

    /**
     * Gets the value of the attribute5 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAttribute5() {
        return attribute5;
    }

    /**
     * Sets the value of the attribute5 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAttribute5(String value) {
        this.attribute5 = value;
    }

}
