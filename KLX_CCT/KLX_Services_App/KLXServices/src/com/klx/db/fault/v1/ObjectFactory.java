
package com.klx.db.fault.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.klx.db.fault.v1 package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DBFault_QNAME = new QName("http://www.klx.com/db/fault/V1.0", "DBFault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.klx.db.fault.v1
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DBFaultType }
     *
     */
    public DBFaultType createDBFaultType() {
        return new DBFaultType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DBFaultType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/db/fault/V1.0", name = "DBFault")
    public JAXBElement<DBFaultType> createDBFault(DBFaultType value) {
        return new JAXBElement<DBFaultType>(_DBFault_QNAME, DBFaultType.class, null, value);
    }

}
