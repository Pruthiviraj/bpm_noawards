package com.klx.services.entities;

import java.io.Serializable;

import java.util.*;
public class Tasks implements Serializable{
    @SuppressWarnings("compatibility:5989714023806529499")
    private static final long serialVersionUID = 1L;

    public Tasks() {
        super();
    }
    
    private String taskId;
    private String taskNumber;
    private String title;  
    private String opportunityId;
    private String opportunityNumber;
    private String oscOppId;
    private String quoteid;    
    private String group;
    private String groupName;
    private String assignee;
    private String assigneeName;
    private String status;
    private String customerName;
    private String salesRep;
    private String pricingManagerRep;
    private String proposalManagerRep;
    private String salesMethod;
    private String state;
    private String subState;
    private String customerNo;    
    private List<Users> assigneeList;       
    private Date customerDueDate;
    private String dealType;
    private String dealStructure;
    private String teamNumber;
    private Date taskDueDate;
    private String taskDescription;
    private Date taskAssignmentDate;
    private Date taskCompletionDate;
    private String taskCompletedBy;
    private Date updatedDate;
    private String taskType;
    private String projectType;
    private String contractType;
    private String customerNoCrf;
    private String ownerUser;
    private String approvalComments;
    private String outcome;
    private boolean inactiveUser;

    public void setInactiveUser(boolean inactiveUser) {
        this.inactiveUser = inactiveUser;
    }

    public boolean isInactiveUser() {
        return inactiveUser;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setApprovalComments(String approvalComments) {
        this.approvalComments = approvalComments;
    }

    public String getApprovalComments() {
        return approvalComments;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }
    private Date creationDate;

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public String getTaskDescription() {
        return taskDescription;
    }


    public void setTaskDueDate(Date taskDueDate) {
        this.taskDueDate = taskDueDate;
    }

    public Date getTaskDueDate() {
        return taskDueDate;
    }


    public void setOscOppId(String oscOppId) {
        this.oscOppId = oscOppId;
    }

    public String getOscOppId() {
        return oscOppId;
    }

    public void setTeamNumber(String teamNumber) {
        this.teamNumber = teamNumber;
    }

    public String getTeamNumber() {
        return teamNumber;
    }

    public void setCustomerDueDate(Date customerDueDate) {
        this.customerDueDate = customerDueDate;
    }

    public Date getCustomerDueDate() {
        return customerDueDate;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealStructure(String dealStructure) {
        this.dealStructure = dealStructure;
    }

    public String getDealStructure() {
        return dealStructure;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setAssigneeName(String assigneeName) {
        this.assigneeName = assigneeName;
    }

    public String getAssigneeName() {
        return assigneeName;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskNumber(String taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getTaskNumber() {
        return taskNumber;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setOpportunityId(String opportunityId) {
        this.opportunityId = opportunityId;
    }

    public String getOpportunityId() {
        return opportunityId;
    }

    public void setOpportunityNumber(String opportunityNumber) {
        this.opportunityNumber = opportunityNumber;
    }

    public String getOpportunityNumber() {
        return opportunityNumber;
    }

    public void setQuoteid(String quoteid) {
        this.quoteid = quoteid;
    }

    public String getQuoteid() {
        return quoteid;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGroup() {
        return group;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setSalesRep(String salesRep) {
        this.salesRep = salesRep;
    }

    public String getSalesRep() {
        return salesRep;
    }

    public void setPricingManagerRep(String pricingManagerRep) {
        this.pricingManagerRep = pricingManagerRep;
    }

    public String getPricingManagerRep() {
        return pricingManagerRep;
    }

    public void setProposalManagerRep(String proposalManagerRep) {
        this.proposalManagerRep = proposalManagerRep;
    }

    public String getProposalManagerRep() {
        return proposalManagerRep;
    }

    public void setSalesMethod(String salesMethod) {
        this.salesMethod = salesMethod;
    }

    public String getSalesMethod() {
        return salesMethod;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setSubState(String subState) {
        this.subState = subState;
    }

    public String getSubState() {
        return subState;
    }

    public void setTaskAssignmentDate(Date taskAssignmentDate) {
        this.taskAssignmentDate = taskAssignmentDate;
    }

    public Date getTaskAssignmentDate() {
        return taskAssignmentDate;
    }

    public void setTaskCompletionDate(Date taskCompletionDate) {
        this.taskCompletionDate = taskCompletionDate;
    }

    public Date getTaskCompletionDate() {
        return taskCompletionDate;
    }

    public void setTaskCompletedBy(String taskCompletedBy) {
        this.taskCompletedBy = taskCompletedBy;
    }

    public String getTaskCompletedBy() {
        return taskCompletedBy;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }


    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }

    public String getProjectType() {
        return projectType;
    }

	public void setOwnerUser(String ownerUser) {
        this.ownerUser = ownerUser;
    }

    public String getOwnerUser() {
        return ownerUser;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getContractType() {
        return contractType;
    }

    public void setCustomerNoCrf(String customerNoCrf) {
        this.customerNoCrf = customerNoCrf;
    }

    public String getCustomerNoCrf() {
        return customerNoCrf;
    }    

    public void setAssigneeList(List<Users> assigneeList) {
        this.assigneeList = assigneeList;
    }

    public List<Users> getAssigneeList() {
        return assigneeList;
    }
}
