package com.klx.services.entities;

public class Approval {
    public Approval() {
        super();
    }
    
    private String opportunityNumber;
    private String opportunityId;
    private String snapshotId;
    private String approvalStatus;
    private String approvalNote;
    private String approvalDate;
    private String approverId;
    private String approverRole;
    private String taskNumber;

    public void setTaskNumber(String taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getTaskNumber() {
        return taskNumber;
    }

    public void setOpportunityNumber(String opportunityNumber) {
        this.opportunityNumber = opportunityNumber;
    }

    public String getOpportunityNumber() {
        return opportunityNumber;
    }

    public void setOpportunityId(String opportunityId) {
        this.opportunityId = opportunityId;
    }

    public String getOpportunityId() {
        return opportunityId;
    }

    public void setSnapshotId(String snapshotId) {
        this.snapshotId = snapshotId;
    }

    public String getSnapshotId() {
        return snapshotId;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalNote(String approvalNote) {
        this.approvalNote = approvalNote;
    }

    public String getApprovalNote() {
        return approvalNote;
    }

    public void setApprovalDate(String approvalDate) {
        this.approvalDate = approvalDate;
    }

    public String getApprovalDate() {
        return approvalDate;
    }


    public void setApproverId(String approverId) {
        this.approverId = approverId;
    }

    public String getApproverId() {
        return approverId;
    }


    public void setApproverRole(String approverRole) {
        this.approverRole = approverRole;
    }

    public String getApproverRole() {
        return approverRole;
    }
}
