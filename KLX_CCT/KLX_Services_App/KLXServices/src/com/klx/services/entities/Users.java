package com.klx.services.entities;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;

public class Users implements Serializable{
    @SuppressWarnings("compatibility:-6567984429144605502")
    private static final long serialVersionUID = 1L;

    public Users() {
        super();
    }
    
    private String userName;
    private String userID;
    private String userGroup;
    private String userRole;
    private String userEmail;
    private List<String> userGroups = new ArrayList<String>();
    

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserID() {
        return userID;
    }


    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }

    public String getUserGroup() {
        return userGroup;
    }

    public void setUserGroups(List<String> userGroups) {
        this.userGroups = userGroups;
    }

    public List<String> getUserGroups() {
        return userGroups;
    }
}
