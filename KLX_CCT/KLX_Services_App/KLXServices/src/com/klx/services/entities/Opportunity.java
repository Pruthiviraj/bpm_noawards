package com.klx.services.entities;

import java.util.Date;

public class Opportunity {
    
    private String opportunityNumber;
    private String opportunityName;
    private String opportunityId;
    private String customerName;
    private String salesRep;
    private String pricingManagerRep;
    private String proposalManagerRep;
    private String salesMethod;
    private String customerNo;
    private Date customerDueDate;
    private String dealType;
    private String dealStructure;
    private String teamNumber;
    private String projectType;
    private String contractType;
    private String customerNoCrf;

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setSalesRep(String salesRep) {
        this.salesRep = salesRep;
    }

    public String getSalesRep() {
        return salesRep;
    }

    public void setPricingManagerRep(String pricingManagerRep) {
        this.pricingManagerRep = pricingManagerRep;
    }

    public String getPricingManagerRep() {
        return pricingManagerRep;
    }

    public void setProposalManagerRep(String proposalManagerRep) {
        this.proposalManagerRep = proposalManagerRep;
    }

    public String getProposalManagerRep() {
        return proposalManagerRep;
    }

    public void setSalesMethod(String salesMethod) {
        this.salesMethod = salesMethod;
    }

    public String getSalesMethod() {
        return salesMethod;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public Opportunity() {
        super();
    }


    public void setOpportunityNumber(String opportunityNumber) {
        this.opportunityNumber = opportunityNumber;
    }

    public String getOpportunityNumber() {
        return opportunityNumber;
    }

    public void setOpportunityName(String opportunityName) {
        this.opportunityName = opportunityName;
    }

    public String getOpportunityName() {
        return opportunityName;
    }

    public void setOpportunityId(String opportunityId) {
        this.opportunityId = opportunityId;
    }

    public String getOpportunityId() {
        return opportunityId;
    }

    public void setCustomerDueDate(Date customerDueDate) {
        this.customerDueDate = customerDueDate;
    }

    public Date getCustomerDueDate() {
        return customerDueDate;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealStructure(String dealStructure) {
        this.dealStructure = dealStructure;
    }

    public String getDealStructure() {
        return dealStructure;
    }

    public void setTeamNumber(String teamNumber) {
        this.teamNumber = teamNumber;
    }

    public String getTeamNumber() {
        return teamNumber;
    }

    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }

    public String getProjectType() {
        return projectType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getContractType() {
        return contractType;
    }

    public void setCustomerNoCrf(String customerNoCrf) {
        this.customerNoCrf = customerNoCrf;
    }

    public String getCustomerNoCrf() {
        return customerNoCrf;
    }
}
