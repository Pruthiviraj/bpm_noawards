package com.klx.services.entities;

import java.io.Serializable;

import java.math.BigDecimal;

public class Quote implements Serializable{
    @SuppressWarnings("compatibility:-8897732459616018680")
    private static final long serialVersionUID = 1L;

    public Quote() {
        super();
    }
    
    private String quoteId;
    private String quoteNumber;
    private String quoteRevisionId;
    private String quoteRevision;

    public void setQuoteRevision(String quoteRevision) {
        this.quoteRevision = quoteRevision;
    }

    public String getQuoteRevision() {
        return quoteRevision;
    }

    public void setQuoteNumber(String quoteNumber) {
        this.quoteNumber = quoteNumber;
    }

    public String getQuoteNumber() {
        return quoteNumber;
    }

    public void setQuoteId(String quoteId) {
        this.quoteId = quoteId;
    }

    public String getQuoteId() {
        return quoteId;
    }

    public void setQuoteRevisionId(String quoteRevisionId) {
        this.quoteRevisionId = quoteRevisionId;
    }

    public String getQuoteRevisionId() {
        return quoteRevisionId;
    }

}
