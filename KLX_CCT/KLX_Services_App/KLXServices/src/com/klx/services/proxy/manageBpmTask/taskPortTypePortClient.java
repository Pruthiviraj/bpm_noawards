package com.klx.services.proxy.manageBpmTask;

import com.klx.cct.task.v1.RoleDetailsType;
import com.klx.cct.task.v1.RoleType;
import com.klx.cct.task.v1.TaskT;
import com.klx.services.wrapper.ManageBPMTaskServiceWrapper;
import com.klx.common.cache.ParamCache;

import oracle.adf.share.ADFContext;
// This source file is generated by Oracle tools.
// Contents may be subject to change.
// For reporting problems, use the following:
// Generated by Oracle JDeveloper 12c 12.2.1.2.0.1648
public class taskPortTypePortClient {
    public static void main(String[] args) {
        TaskPortType_Service taskPortType_Service = new TaskPortType_Service();
        TaskPortType taskPortType = taskPortType_Service.getTaskPortTypePort();
        // Add your code to call the desired methods.
        TaskT task=new TaskT();
        task.setOpportunityId("300000018799304");
        task.setOpportunityNumber("67892");
        task.setTaskTitle("Manage");
        task.setTaskType("NEW");
            RoleDetailsType roleDetails=new RoleDetailsType();
            RoleType role=new RoleType();
            role.setIsTaskCreationRequired(true);
            role.setRoleName((String)ParamCache.userRoleMap.get("CCT_PROPOSAL_MANAGER"));
            roleDetails.getRole().add(role);
            task.setRoleDetails(roleDetails);
            task.setCreatedBy("psal");
        ManageBPMTaskServiceWrapper wrapper=new ManageBPMTaskServiceWrapper();
        wrapper.createTask(task);
    }
}
