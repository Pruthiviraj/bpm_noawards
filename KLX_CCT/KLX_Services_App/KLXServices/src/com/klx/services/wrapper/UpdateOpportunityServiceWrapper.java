package com.klx.services.wrapper;


import com.klx.common.logger.KLXLogger;
import com.klx.constants.ServiceConstants;
import com.klx.services.proxy.taskProviderService.OpportunityPortTypePortClient;
import com.klx.services.proxy.taskProviderService.OpportunityPortType;
import com.klx.services.proxy.taskProviderService.OpportunityPortType_Service;
import com.klx.services.proxy.updateOpportunity.UpdateOpportunityPort;
import com.klx.services.proxy.updateOpportunity.UpdateOpportunityService;
import com.klx.xmlns.schema.cct.oppty.resource.v1.OpptyResourceType;
import com.klx.xmlns.schema.cct.oppty.resource.v1.ResourceType;
import com.klx.xmlns.schema.cct.oppty.update.v1.DetailsType;
import com.klx.xmlns.schema.cct.oppty.update.v1.QuoteDetailsType;

import com.klx.xmlns.schema.opportunity.v1.Opportunity;
import com.klx.xmlns.schema.opportunity.v1.OpportunityHeaderInfoType;
import com.klx.xmlns.schema.opportunity.v1.Response;
import java.util.Date;
import java.math.BigDecimal;

import java.text.SimpleDateFormat;

import java.util.GregorianCalendar;
import java.util.logging.Level;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.WebServiceRef;

public class UpdateOpportunityServiceWrapper {
    private static KLXLogger logger = KLXLogger.getLogger();    
    public UpdateOpportunityServiceWrapper() {
        super();
    }
    @WebServiceRef
    
    private static UpdateOpportunityService updateOpportunityService;
    private UpdateOpportunityPort updateOpportunityPort;
    
    public String updateOpportunityResource(String userId, String oscOppId,  String role){
        logger.log(Level.INFO,getClass(),"updateOpportunityResource","Entering updateOpportunityResource");
        UpdateOpportunityService updateOpportunityService=new UpdateOpportunityService();
        try{
        updateOpportunityPort=updateOpportunityService.getUpdateOpportunityPort();
        OpptyResourceType request=new OpptyResourceType();
        ResourceType resource=new ResourceType();
        resource.setUserId(userId);
        resource.setOpptyId(oscOppId);
        resource.setRoleName(role);
        request.setResource(resource);
        updateOpportunityPort.updateOppResource(request);
        }
        catch(Exception e){
            logger.log(Level.SEVERE,getClass(),"Exception Caught : ",e.getMessage());
        }
        logger.log(Level.INFO,getClass(),"updateOpportunityResource","Exiting updateOpportunityResource");
        return ServiceConstants.SUCCESS;
    }
    
    public String updateOpportunityData(String oscOppId,BigDecimal amount, BigDecimal netMargin, Boolean documentReadyToSubmit ){
        logger.log(Level.INFO,getClass(),"updateOpportunityData","Entering updateOpportunityData");
        UpdateOpportunityService updateOpportunityService=new UpdateOpportunityService();
        try{
        updateOpportunityPort=updateOpportunityService.getUpdateOpportunityPort();
        QuoteDetailsType request=new QuoteDetailsType();
        DetailsType details=new DetailsType();
        details.setOpptyId(oscOppId);
        details.setAmount(amount);
        details.setNetMargin(netMargin);
        details.setDocumentReadyToSubmitC(documentReadyToSubmit);    
        request.setDetails(details);
        updateOpportunityPort.updateOppData(request);
        }
        catch(Exception e){
            logger.log(Level.SEVERE,getClass(),"Exception Caught : ",e.getMessage());  
        }
        logger.log(Level.INFO,getClass(),"updateOpportunityData","Exiting updateOpportunityData");
        return ServiceConstants.SUCCESS;
    }
    
    public String createOpportunityRevision(String oppNumber, String oppID){
        logger.log(Level.INFO,getClass(),"createOpportunityRevision","Entering createOpportunityRevision");
        String status = "FAILURE";
        try{
        OpportunityPortType_Service opportunityPortType_Service = new OpportunityPortType_Service();
        OpportunityPortType opportunityPortType = opportunityPortType_Service.getOpportunityPortTypePort();
        Opportunity opportunity = new Opportunity();
        OpportunityHeaderInfoType headerInfo = new OpportunityHeaderInfoType();
        headerInfo.setOpportunityId(oppID);
        headerInfo.setOpportunityNumber(oppNumber);
        headerInfo.setSource("ADFUI");
        headerInfo.setOpportunityType("REVISE");
        opportunity.setOpportunityHeaderInfo(headerInfo);
        Response createOpportunityRevision = opportunityPortType.createOpportunity(opportunity);
        //Response createOpportunity = syncOpportunityDetailsToDbBPELProcess.syncOpportunity(opportunity);
        status = createOpportunityRevision.getStatus();
        }
        catch(Exception ae){
            logger.log(Level.SEVERE,getClass(),"Exception Caught : ",ae.getMessage());  
            ae.printStackTrace();
        }
        logger.log(Level.INFO,getClass(),"createOpportunityRevision","Exiting createOpportunityRevision");
        return status;
    }
    
    public String customerDueDateChange(String oppNumber, String oppID, Date oldDuedate){
        logger.log(Level.INFO,getClass(),"customerDueDateChange","Entering customerDueDateChange");
        String status = "FAILURE", oldDate = "";
        try{
                if (null != oldDate) {
                    SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                    oldDate = formatter.format(oldDuedate);
                }
//            GregorianCalendar gregorianCalendar = new GregorianCalendar();
//            gregorianCalendar.setTime(oldDuedate);
//            XMLGregorianCalendar xmlGrogerianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
//            
            OpportunityPortType_Service opportunityPortType_Service = new OpportunityPortType_Service();
            OpportunityPortType opportunityPortType = opportunityPortType_Service.getOpportunityPortTypePort();
            Opportunity opportunity = new Opportunity();
            OpportunityHeaderInfoType headerInfo = new OpportunityHeaderInfoType();
            headerInfo.setOpportunityId(oppID);
            headerInfo.setOpportunityNumber(oppNumber);
            headerInfo.setSource("ADFUI");
            headerInfo.setOpportunityType("UPDATE");
            headerInfo.setIsCustomerDueDateChanged(true);
            headerInfo.setOldCustomerDueDate(oldDate);
            opportunity.setOpportunityHeaderInfo(headerInfo);
            Response createOpportunityRevision = opportunityPortType.createOpportunity(opportunity);
            //Response createOpportunity = syncOpportunityDetailsToDbBPELProcess.syncOpportunity(opportunity);
            status = createOpportunityRevision.getStatus();
        }
        catch(Exception ae){
            logger.log(Level.SEVERE,getClass(),"Exception Caught : ",ae.getMessage());  
            ae.printStackTrace();
        }
        logger.log(Level.INFO,getClass(),"customerDueDateChange","Exiting customerDueDateChange");
        return status;
    }
    
    public String createAwardTask(String oppNumber, String oppID, String opptyOutcome){
        logger.log(Level.INFO,getClass(),"createAwardTask","Entering createAwardTask");
        String status = "FAILURE";
        OpportunityPortType_Service opportunityPortType_Service = new OpportunityPortType_Service();
        OpportunityPortType opportunityPortType = opportunityPortType_Service.getOpportunityPortTypePort();
        try{
    
            Opportunity opportunity = new Opportunity();
            OpportunityHeaderInfoType headerInfo = new OpportunityHeaderInfoType();
            headerInfo.setOpportunityId(oppID);
            headerInfo.setOpportunityNumber(oppNumber);
            headerInfo.setSource("ADFUI");
            headerInfo.setOpportunityType("UPDATE");
            headerInfo.setOpportunityStatus(opptyOutcome);
            opportunity.setOpportunityHeaderInfo(headerInfo);
            Response createOpportunityRevision = opportunityPortType.createOpportunity(opportunity);
            //Response createOpportunity = syncOpportunityDetailsToDbBPELProcess.syncOpportunity(opportunity);
            status = createOpportunityRevision.getStatus();
        }
        catch(Exception e){
            logger.log(Level.SEVERE,getClass(),"Exception Caught : ",e.getMessage());  
        }
        logger.log(Level.INFO,getClass(),"createAwardTask","Exiting createAwardTask");
        return status;
    }
}
