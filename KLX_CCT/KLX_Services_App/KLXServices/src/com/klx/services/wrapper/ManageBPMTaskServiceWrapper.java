package com.klx.services.wrapper;

import com.klx.cct.task.v1.StatusT;
import com.klx.cct.task.v1.TaskT;
import com.klx.common.logger.KLXLogger;
import com.klx.constants.ServiceConstants;
import com.klx.services.proxy.manageBpmTask.TaskPortType;
import com.klx.services.proxy.manageBpmTask.TaskPortType_Service;

import java.util.logging.Level;

import javax.xml.ws.WebServiceRef;

public class ManageBPMTaskServiceWrapper {
    private static KLXLogger logger = KLXLogger.getLogger();    
    public ManageBPMTaskServiceWrapper() {
        super();
    }
    
    @WebServiceRef
    
    private static TaskPortType_Service manageBPMTaskProcessService;
    private TaskPortType manageBPMTaskProcessPortType;
    
    public String createTask(TaskT task){
        logger.log(Level.INFO,getClass(),"createTask","Entering createTask");
        TaskPortType_Service manageBPMTaskProcessService=new TaskPortType_Service();
        manageBPMTaskProcessPortType=manageBPMTaskProcessService.getTaskPortTypePort();
        String result=ServiceConstants.FAILURE;
        try{
            StatusT status=manageBPMTaskProcessPortType.createTask(task);
            
            if(null!=status){
                result=status.getStatus();
            }
        }        
        catch(Exception e){
            logger.log(Level.INFO,getClass(),"createTask","Inside Exception");
        }
        logger.log(Level.INFO,getClass(),"createTask","Exiting createTask");
        return result;
    }
}
