package com.klx.services.wrapper;

import com.klx.cct.ad.activedirectoryservice.v1.GetUserDetailsByEmailRequest;
import com.klx.cct.ad.activedirectoryservice.v1.GetUserDetailsByEmailResponse;
import com.klx.cct.ad.activedirectoryservice.v1.GetUserDetailsRequest;
import com.klx.cct.ad.activedirectoryservice.v1.GetUserDetailsResponse;
import com.klx.cct.ad.activedirectoryservice.v1.GetUsersByRoleRequest;
import com.klx.cct.ad.activedirectoryservice.v1.GetUsersByRoleResponse;
import com.klx.cct.ad.user.v1.User;
import com.klx.cct.ad.user.v1.UsersList;
import com.klx.common.logger.KLXLogger;
import com.klx.services.entities.Users;
import com.klx.services.proxy.activeDirectory.ActiveDirectoryService;

import com.klx.services.proxy.activeDirectory.ActiveDirectoryServicePort;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.logging.Level;

import javax.xml.ws.WebServiceRef;

public class ActiveDirectoryServiceWrapper {
    public ActiveDirectoryServiceWrapper() {
        super();
    }
    private static KLXLogger logger = KLXLogger.getLogger();
    
    @WebServiceRef    
    private static ActiveDirectoryService activeDirectoryService;
    private ActiveDirectoryServicePort activeDirectoryServicePort;
    
    public User fetchUserDetailsById(String userId){        
        ActiveDirectoryService activeDirectoryService=new ActiveDirectoryService();
        GetUserDetailsResponse response=new GetUserDetailsResponse();
        User user = null;
        try{
            activeDirectoryServicePort=activeDirectoryService.getActiveDirectoryPortBinding();
            GetUserDetailsRequest request=new GetUserDetailsRequest();
            request.setUserId(userId);
            response=activeDirectoryServicePort.getUserDetailsById(request);
            if(null != response)
                user = response.getUser();
            
        }
        catch(Exception we){
            logger.log(Level.SEVERE,getClass(),"Exception Caught : ",we.getMessage());  
        }
        return user;
    }

    public User fetchUserDetailsByEmailId(String emailId){
        
        ActiveDirectoryService activeDirectoryService=new ActiveDirectoryService();
        GetUserDetailsByEmailResponse response=new GetUserDetailsByEmailResponse();
        User user = null;
        try{
            activeDirectoryServicePort=activeDirectoryService.getActiveDirectoryPortBinding();
            GetUserDetailsByEmailRequest request=new GetUserDetailsByEmailRequest();
            request.setEmailId(emailId);
            response=activeDirectoryServicePort.getUserDetailsByEmail(request);
            user = response.getUser();
        }
        catch(Exception we){
            logger.log(Level.SEVERE,getClass(),"Exception Caught : ",we.getMessage());  
        }
        return user;
    }


    public List<Users> fetchUsersForRole(String role){
    
        ActiveDirectoryService activeDirectoryService=new ActiveDirectoryService();
        GetUsersByRoleResponse response=new GetUsersByRoleResponse();
        List<Users> usersList=new ArrayList<Users>();
        try{
            activeDirectoryServicePort=activeDirectoryService.getActiveDirectoryPortBinding();
            GetUsersByRoleRequest request=new GetUsersByRoleRequest();
            request.setRole(role);
            response=activeDirectoryServicePort.getUsersByRole(request); 
            
            for(User user:response.getUsersList().getUser()){
                Users users=new Users();
                users.setUserID(user.getUserId());
                users.setUserName(user.getName());
                usersList.add(users);
            }
            usersList.sort(Comparator.comparing(Users::getUserName));
        }
        catch(Exception we){
            logger.log(Level.SEVERE,getClass(),"Exception Caught : ",we.getMessage()); 
        }
        return usersList;
    }
    
         
}
