package com.klx.services.wrapper;

import com.klx.bpm.cct.tqs.utility.v1.TaskQueryInputT;
import com.klx.bpm.cct.tqs.utility.v1.TaskQueryOutputT;
import com.klx.common.cache.ParamCache;
import com.klx.common.logger.KLXLogger;
import com.klx.constants.ServiceConstants;
import com.klx.services.proxy.taskQueryUtility.Port;
import com.klx.services.proxy.taskQueryUtility.Port_Service;
import com.klx.services.proxy.taskUpdate.StaleObjectFaultMessage;
import com.klx.services.proxy.taskUpdate.TaskService;
import com.klx.services.proxy.taskUpdate.TaskService_Service;
import com.klx.services.proxy.taskUpdate.WorkflowErrorMessage;

import com.oracle.xmlns.bpel.workflow.taskservice.TaskAssigneeType;
import com.oracle.xmlns.bpel.workflow.taskservice.TaskAssigneesType;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.logging.Level;

import javax.xml.ws.WebServiceRef;

import oracle.bpel.services.workflow.common.model.CredentialType;
import oracle.bpel.services.workflow.common.model.WorkflowContextType;
import oracle.bpel.services.workflow.task.model.Task;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class TaskUpdateServiceWrapper {
    private static KLXLogger logger = KLXLogger.getLogger();    
    public TaskUpdateServiceWrapper() {
        super();
    }
    
    @WebServiceRef
    
    private static TaskService_Service taskService_Service;
    private TaskService taskService;
    
    public String reassignTaskToUser(String username, String taskId){
        logger.log(Level.INFO,getClass(),"reassignTaskToUser","Entering reassignTaskToUser");  
    //      Get the service client port
        TaskService_Service taskService_Service=new TaskService_Service();
        taskService=taskService_Service.getTaskServicePort();
           
        try{                
    //      Creating credentials object for WorkflowContext
        CredentialType credential=new CredentialType();
            credential.setLogin(ParamCache.propertiesMap.get("BPM_ADMIN_USER").toString());
            credential.setPassword(ParamCache.propertiesMap.get("BPM_ADMIN_PASSWORD").toString());       
        
    //      Creating WorkflowContext object
        WorkflowContextType workflowContext=new WorkflowContextType();
        workflowContext.setCredential(credential);
        
        
    //      Creating TaskAssigneesType object
        TaskAssigneesType assignees=new TaskAssigneesType();
        TaskAssigneeType taskAssignee=new TaskAssigneeType();
        taskAssignee.setIsGroup(false);
        taskAssignee.setValue(username);
        assignees.getTaskAssignee().add(taskAssignee);   
   

    //      Calling the reassignTask method passing request object
        taskService.reassignTask(workflowContext, null, taskId, null, assignees, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        }
        catch(WorkflowErrorMessage we){
            logger.log(Level.SEVERE,getClass(),"Exception Caught : ",we.getMessage());
            return ServiceConstants.FAILURE; 
        }
        catch(StaleObjectFaultMessage so){
            logger.log(Level.SEVERE,getClass(),"Exception Caught : ",so.getMessage());
            return ServiceConstants.FAILURE; 
        }
        logger.log(Level.INFO,getClass(),"reassignTaskToUser","Exiting reassignTaskToUser");
        return ServiceConstants.SUCCESS;
    }
    
    public String assignTaskToUser(String username, String taskId){
    //      Get the service client port
        TaskService_Service taskService_Service=new TaskService_Service();
        taskService=taskService_Service.getTaskServicePort();
                   
        try{                
    //      Creating credentials object for WorkflowContext
        CredentialType credential=new CredentialType();
        credential.setLogin(ParamCache.propertiesMap.get("BPM_ADMIN_USER").toString());
        credential.setPassword(ParamCache.propertiesMap.get("BPM_ADMIN_PASSWORD").toString());       
        credential.setOnBehalfOfUser(username);
    //      Creating WorkflowContext object
        WorkflowContextType workflowContext=new WorkflowContextType();
        workflowContext.setCredential(credential);
                                 
    //      Calling the reassignTask method passing request object
        taskService.acquireTask(workflowContext, null, taskId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        }
        catch(WorkflowErrorMessage we){
            return ServiceConstants.FAILURE; 
        }
        catch(StaleObjectFaultMessage so){
            return ServiceConstants.FAILURE; 
        }
        return ServiceConstants.SUCCESS;
    }
    
    public String releaseTask(String username, String taskId){
    //      Get the service client port
        TaskService_Service taskService_Service=new TaskService_Service();
        taskService=taskService_Service.getTaskServicePort();
                   
        try{                
    //      Creating credentials object for WorkflowContext
        CredentialType credential=new CredentialType();
            credential.setLogin(ParamCache.propertiesMap.get("BPM_ADMIN_USER").toString());
            credential.setPassword(ParamCache.propertiesMap.get("BPM_ADMIN_PASSWORD").toString());      
        credential.setOnBehalfOfUser(username);
    //      Creating WorkflowContext object
        WorkflowContextType workflowContext=new WorkflowContextType();
        workflowContext.setCredential(credential);
                                    
    //      Calling the reassignTask method passing request object
        taskService.releaseTask(workflowContext, null, taskId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        }
        catch(WorkflowErrorMessage we){
            return ServiceConstants.FAILURE; 
        }
        catch(StaleObjectFaultMessage so){
            return ServiceConstants.FAILURE; 
        }
        return ServiceConstants.SUCCESS;
    }
    
    public String closeTask(String username, String taskId, String outcome,String comments,String assignedUser){
        String status=ServiceConstants.FAILURE;
        try{        
            Port_Service taskQueryUtility_Service=new Port_Service();
            Port taskQueryUtilityPort=taskQueryUtility_Service.getPortPort();
            TaskQueryInputT taskQueryInputT=new TaskQueryInputT();
            taskQueryInputT.setTaskId(taskId);
            taskQueryInputT.setUserComments(comments);
            taskQueryInputT.setAction(outcome);
           
            
            taskQueryInputT.setUserId(null == assignedUser ? "" : assignedUser);
            //always Logged in username should be passed to close the task
            taskQueryInputT.setLoggedUserId(username);
            TaskQueryOutputT outputT = taskQueryUtilityPort.updateTaskOutcome(taskQueryInputT);
            status = outputT.getStatus();
        }catch(Exception ex){
            ex.printStackTrace();
        }
//        TaskService_Service taskService_Service=new TaskService_Service();
//        taskService=taskService_Service.getTaskServicePort();
//        logger.log(Level.INFO,getClass(),"closeTask: TaskId: "+taskId+" :Outcome: "+outcome+" : Username:"+username,"Exiting reassignTaskToUser");          
//        try{                
//        //      Creating credentials object for WorkflowContext
//        CredentialType credential=new CredentialType();
//            credential.setLogin(ParamCache.propertiesMap.get("BPM_ADMIN_USER").toString());
//            credential.setPassword(ParamCache.propertiesMap.get("BPM_ADMIN_PASSWORD").toString());       
//        credential.setOnBehalfOfUser(username);
//        //      Creating WorkflowContext object
//        WorkflowContextType workflowContext=new WorkflowContextType();
//        workflowContext.setCredential(credential);
//            
////        CommentType commentType=new CommentType();
////        commentType.setComment(comments);   
//            
//        //      Calling the reassignTask method passing request object
//        taskService.updateTaskOutcome(workflowContext, null, taskId, null, outcome, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
//        }
//        catch(WorkflowErrorMessage we){
//            we.printStackTrace();
//        } catch (StaleObjectFaultMessage so) {
//            so.printStackTrace();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
        return status;
    }
    
    public String updateTaskDueDate(String taskId, Date newDueDate){
        TaskService_Service taskService_Service=new TaskService_Service();
        taskService=taskService_Service.getTaskServicePort();
        
        try{                
        //      Creating credentials object for WorkflowContext
        CredentialType credential=new CredentialType();
            credential.setLogin(ParamCache.propertiesMap.get("BPM_ADMIN_USER").toString());
            credential.setPassword(ParamCache.propertiesMap.get("BPM_ADMIN_PASSWORD").toString());       

        //      Creating WorkflowContext object
        WorkflowContextType workflowContext=new WorkflowContextType();
        workflowContext.setCredential(credential);
            
                                     
        //      Calling the fetchTaskDetailsById method passing taskId
        TaskQueryServiceWrapper query=new TaskQueryServiceWrapper();
        Task task=query.fetchTaskDetailsById(taskId);
        Element payload=(Element)task.getPayload();
        if(null!=payload){
            Node quoteNode=payload.getFirstChild();
            if(null!=quoteNode.getChildNodes().item(6)){
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String taskDueDate=df.format(newDueDate);
                quoteNode.getChildNodes().item(6).getFirstChild().setTextContent(taskDueDate);
            }
        }
        taskService.updateTask(workflowContext, task, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        }
        catch(WorkflowErrorMessage we){
            logger.log(Level.SEVERE,getClass(),"Exception Caught : ",we.getMessage());
            return ServiceConstants.SUCCESS; 
        }
        catch(StaleObjectFaultMessage so){
            logger.log(Level.SEVERE,getClass(),"Exception Caught : ",so.getMessage());
            return ServiceConstants.FAILURE; 
        }
        return ServiceConstants.SUCCESS;

        
    }
    
    public String approveOrRejectTask(String senderUserId, String taskNumber, String outcome,String comments, String approvalSource){
       String status = null;
        try{        
            Port_Service taskQueryUtility_Service=new Port_Service();
            Port taskQueryUtilityPort=taskQueryUtility_Service.getPortPort();
            TaskQueryInputT taskQueryInputT=new TaskQueryInputT();
            taskQueryInputT.setTaskNumber(taskNumber);
            taskQueryInputT.setUserComments(comments);
            taskQueryInputT.setAction(outcome);
            taskQueryInputT.setApprovalSource(approvalSource);
            //always Logged in username should be passed to close the task
            
            taskQueryInputT.setLoggedUserId(senderUserId);
            TaskQueryOutputT outputT = taskQueryUtilityPort.updateTaskOutcome(taskQueryInputT);
            status = outputT.getStatus();
        }catch(Exception ex){
            ex.printStackTrace();
        }
       return status;
    }
}
