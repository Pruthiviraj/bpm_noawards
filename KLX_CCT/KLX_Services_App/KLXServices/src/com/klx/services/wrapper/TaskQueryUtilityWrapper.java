package com.klx.services.wrapper;

import com.klx.bpm.cct.tqs.utility.v1.AssignedUsersForOpportunityReqT;
import com.klx.bpm.cct.tqs.utility.v1.AssignedUsersForOpportunityRspT;
import com.klx.bpm.cct.tqs.utility.v1.TaskQueryInputT;
import com.klx.bpm.cct.tqs.utility.v1.TaskQueryOutputT;
import com.klx.bpm.cct.tqs.utility.v1.UsersT;
import com.klx.cct.ad.user.v1.User;
import com.klx.common.logger.KLXLogger;
import com.klx.services.proxy.taskQueryUtility.Port_Service;
import com.klx.services.proxy.taskQueryUtility.Port;
import java.util.List;
import java.util.logging.Level;

import javax.xml.ws.WebServiceRef;

public class TaskQueryUtilityWrapper {
    public TaskQueryUtilityWrapper() {
        super();
    }
    
    private static KLXLogger logger = KLXLogger.getLogger();   
    @WebServiceRef
    
    private static Port_Service taskQueryUtility_Service;
    private Port taskQueryUtilityService;
    
    public User fetchUserForOpportunityRole(String opportunityNumber, String role){
        
        User user=new User();
        try{        
            Port_Service taskQueryUtility_Service=new Port_Service();
            taskQueryUtilityService=taskQueryUtility_Service.getPortPort();
            
            AssignedUsersForOpportunityReqT request=new AssignedUsersForOpportunityReqT();
            request.setOpptyNumber(opportunityNumber);
            request.setRole(role);
            
            AssignedUsersForOpportunityRspT resp=taskQueryUtilityService.getAssignedUsersOnOppty(request);
            if(null!=resp && null!=resp.getAssignedUsers()){
                List<UsersT> usersList=resp.getAssignedUsers().getUsers();
                if(null !=usersList){
                    UsersT userT=usersList.get(0);
                    if(null!=userT){
                        user.setUserId(userT.getUserId());
                        user.setName(userT.getName());
                        user.setEmail(userT.getEmail());

                    }
                }
            }
            
                        
        }
        catch(Exception e){
            logger.log(Level.SEVERE,getClass(),"Exception Caught : ",e.getMessage());
            }
        return user;  
    }
    public String reassignTask(String taskId, String reassignedUserId, String loggedInUser){
                   String status = "";
                   try {
                   Port_Service reassignTask_Service = new Port_Service();
                   taskQueryUtilityService = reassignTask_Service.getPortPort();
                   TaskQueryInputT request = new TaskQueryInputT();                  
                   request.setTaskId(taskId);
                   request.setLoggedUserId(loggedInUser);                   
                   request.setReAssignedUserId(reassignedUserId);                   
                   TaskQueryOutputT response = taskQueryUtilityService.reAssignTask(request);
                   status = response.getStatus();
                  
               } catch (Exception e) {
                   // TODO: Add catch code
                   e.printStackTrace();
               }
                   return status;
    }
    
}
