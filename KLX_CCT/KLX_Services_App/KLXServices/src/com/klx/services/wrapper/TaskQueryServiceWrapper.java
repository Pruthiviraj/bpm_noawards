package com.klx.services.wrapper;

import com.klx.cct.ad.user.v1.User;
import com.klx.cct.tqs.resp.wrapper.v1.TaskList;
import com.klx.cct.tqs.resp.wrapper.v1.TaskT;
import com.klx.common.cache.ParamCache;
import com.klx.common.logger.KLXLogger;
import com.klx.constants.ServiceConstants;
import com.klx.services.entities.Approval;
import com.klx.services.entities.Tasks;
import com.klx.services.proxy.taskQuery.TaskQueryService;
import com.klx.services.proxy.taskQuery.TaskQueryService_Service;
import com.klx.services.proxy.taskQuery.WorkflowErrorMessage;

import java.math.BigInteger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.xml.ws.WebServiceRef;

import oracle.bpel.services.workflow.common.model.CredentialType;
import oracle.bpel.services.workflow.common.model.WorkflowContextType;
import oracle.bpel.services.workflow.query.model.AssignmentFilterEnum;
import oracle.bpel.services.workflow.query.model.DisplayColumnType;
import oracle.bpel.services.workflow.query.model.PredicateClauseType;
import oracle.bpel.services.workflow.query.model.PredicateJoinOperatorEnum;
import oracle.bpel.services.workflow.query.model.PredicateOperationEnum;
import oracle.bpel.services.workflow.query.model.TaskDetailsByIdRequestType;
import oracle.bpel.services.workflow.query.model.TaskDetailsByNumberRequestType;
import oracle.bpel.services.workflow.query.model.TaskListRequestType;
import oracle.bpel.services.workflow.query.model.TaskOptionalInfoEnum;
import oracle.bpel.services.workflow.query.model.TaskOptionalInfoType;
import oracle.bpel.services.workflow.query.model.TaskPredicateQueryType;
import oracle.bpel.services.workflow.query.model.TaskPredicateType;
import oracle.bpel.services.workflow.query.model.ValueListType;
import oracle.bpel.services.workflow.task.model.StateEnum;
import oracle.bpel.services.workflow.task.model.SubstateEnum;
import oracle.bpel.services.workflow.task.model.Task;


public class TaskQueryServiceWrapper {
    public TaskQueryServiceWrapper() {
        super();
    }
    private static KLXLogger logger = KLXLogger.getLogger();
    @WebServiceRef

    private static TaskQueryService_Service taskQueryService_Service;
    private TaskQueryService taskQueryService;

    public List<Tasks> fetchMyTasksList(String username, List<String> userGroups) {
        List<Tasks> tasksList = new ArrayList<Tasks>();
        try {
            TaskQueryService_Service taskQueryService_Service = new TaskQueryService_Service();
            taskQueryService = taskQueryService_Service.getTaskQueryServicePort();
            // Creating Task Request Object to be passed as input to queryTasks
            TaskListRequestType taskRequest = new TaskListRequestType();
            CredentialType credential = new CredentialType();
            credential.setLogin(ParamCache.propertiesMap.get("BPM_ADMIN_USER").toString());
            credential.setPassword(ParamCache.propertiesMap.get("BPM_ADMIN_PASSWORD").toString());
            ValueListType userGroupList = new ValueListType();
            userGroupList.getValue().addAll(userGroups);
            //credential.setOnBehalfOfUser(username);
            WorkflowContextType workflowContext = new WorkflowContextType();
            workflowContext.setCredential(credential);
            TaskPredicateQueryType predicateQuery = new TaskPredicateQueryType();
            TaskPredicateType taskPredicate = new TaskPredicateType();
            BigInteger endRowValue = BigInteger.valueOf(5000);           
            predicateQuery.setEndRow(endRowValue);
            taskPredicate.setAssignmentFilter(AssignmentFilterEnum.ALL);
            PredicateClauseType clause = new PredicateClauseType();
            clause.setColumn(ServiceConstants.STATE);
            clause.setOperator(PredicateOperationEnum.EQ);
            clause.setValue(StateEnum.ASSIGNED.toString());            
            taskPredicate.getClause().add(clause);
            PredicateClauseType clause2 = new PredicateClauseType();
            clause2.setJoinOperator(PredicateJoinOperatorEnum.AND);
            clause2.setColumn(ServiceConstants.TEXTATTRIBUTE4);
            clause2.setOperator(PredicateOperationEnum.IN);
            clause2.setValueList(userGroupList);
           
            taskPredicate.getClause().add(clause2);
            PredicateClauseType clause3 = new PredicateClauseType();
            clause3.setJoinOperator(PredicateJoinOperatorEnum.OR);
            clause3.setColumn(ServiceConstants.STATE);
            clause3.setOperator(PredicateOperationEnum.EQ);
            clause3.setValue(StateEnum.ASSIGNED.toString());             
            taskPredicate.getClause().add(clause3);
            PredicateClauseType clause4 = new PredicateClauseType();
            clause4.setJoinOperator(PredicateJoinOperatorEnum.AND);
            clause4.setColumn(ServiceConstants.TEXTATTRIBUTE6);
            clause4.setOperator(PredicateOperationEnum.EQ);
            clause4.setValue(ServiceConstants.MANAGE_APPROVAL);            
            taskPredicate.getClause().add(clause4);
            predicateQuery.setPredicate(taskPredicate);

            DisplayColumnType displayColumns = new DisplayColumnType();
            displayColumns.getDisplayColumn().add(ServiceConstants.TASK_NUMBER);
            displayColumns.getDisplayColumn().add(ServiceConstants.TASKID);
            displayColumns.getDisplayColumn().add(ServiceConstants.TITLE);
            displayColumns.getDisplayColumn().add(ServiceConstants.OUTCOME);
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE1); //OppNo
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE2); //QuoteId
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE3); // Quote Rev
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE4); // Role
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE5); //Status
            displayColumns.getDisplayColumn().add(ServiceConstants.OWNERUSER);
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE6);//Manage Approval
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE7);
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE8);
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE9);
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE10);//approvalUserId
            displayColumns.getDisplayColumn().add(ServiceConstants.NUMBERATTRIBUTE1);
            displayColumns.getDisplayColumn().add(ServiceConstants.NUMBERATTRIBUTE2);
            displayColumns.getDisplayColumn().add(ServiceConstants.NUMBERATTRIBUTE3);
            TaskOptionalInfoType optionalInfo = new TaskOptionalInfoType();
            optionalInfo.getTaskOptionalInfo().add(TaskOptionalInfoEnum.COMMENTS);
            optionalInfo.getTaskOptionalInfo().add(TaskOptionalInfoEnum.PAYLOAD);

            predicateQuery.setDisplayColumnList(displayColumns);
            predicateQuery.setOptionalInfoList(optionalInfo);
            taskRequest.setWorkflowContext(workflowContext);
            taskRequest.setTaskPredicateQuery(predicateQuery);

            // Calling queryTasks method passing the request object
            TaskList taskResponse = taskQueryService.queryTasks(taskRequest);

            List<TaskT> bpmTaskList = taskResponse.getTask();
            for (TaskT task : bpmTaskList) {
                try {
                    Tasks tasks = new Tasks();
                    tasks.setTaskId(task.getTaskAttributes().getTaskId());
                    tasks.setTaskNumber(task.getTaskAttributes().getTaskNumber());
                    tasks.setTaskDescription(task.getPayload().getPMComments());
                    if(null != task.getPayload().getApproverComments())
                        tasks.setApprovalComments(task.getPayload().getApproverComments());
                    String dd = task.getPayload().getTaskDueDate();
                    if (null != dd && !"".equalsIgnoreCase(dd)) {
                        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        Date dueDate = (Date) formatter.parse(dd);
                        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                        String ddNew = df.format(dueDate);
                        Date taskDueDate = (Date) df.parse(ddNew);
                        tasks.setTaskDueDate(taskDueDate);
                    }
                    tasks.setOpportunityNumber(task.getPayload().getOpportunityNumber());
                    tasks.setOpportunityId(task.getPayload().getOpportunityId());
                    //tasks.setGroup(task.getPayload().getRoleName());
                    tasks.setGroup(task.getTaskAttributes().getTextAttribute4());
                    
                    //System.out.println("tasks group ===="+tasks.getGroup()+ ", Task number == "+tasks.getTaskNumber());
                       if(null != tasks.getGroup() && null != ParamCache.groupsMap
                                                 .get(tasks.getGroup())){
                    tasks.setGroupName(ParamCache.groupsMap
                                                 .get(tasks.getGroup())
                                                 .toString());
                    }
                    
                    tasks.setState(task.getTaskAttributes().getState());
                    tasks.setTaskType(task.getTaskAttributes().getTextAttribute6());
                    tasks.setOwnerUser(task.getTaskAttributes().getOwnerUser());
                    String createDate = task.getTaskAttributes().getCreatedDate();
                    if(null != task.getTaskAttributes().getOutcome()){
                        if(task.getTaskAttributes().getOutcome().equalsIgnoreCase(ServiceConstants.approvalTasks[0]))
                            tasks.setOutcome("APPROVED");
                        else if(task.getTaskAttributes().getOutcome().equalsIgnoreCase(ServiceConstants.approvalTasks[1]))
                            tasks.setOutcome("REJECTED");
                        else if(task.getTaskAttributes().getOutcome().equalsIgnoreCase(ServiceConstants.approvalTasks[2]))
                            tasks.setOutcome("APPROVED");
                        else if(task.getTaskAttributes().getOutcome().equalsIgnoreCase(ServiceConstants.approvalTasks[3]))
                            tasks.setOutcome("CANCELLED");
                        else
                            tasks.setOutcome(ServiceConstants.approvalTasks[4]);
                    }
                    if (null != createDate && !"".equalsIgnoreCase(createDate)) {
                        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        Date creationDate = (Date) format.parse(createDate);
                        DateFormat ddf = new SimpleDateFormat("MM/dd/yyyy");
                        String ddN = ddf.format(creationDate);
                        Date dateCreation = (Date) ddf.parse(ddN);
                        tasks.setCreationDate(dateCreation);
                    }
                    //       tasks.setSubState(task.getSystemAttributes().getSubstate().toString());
                    tasks.setAssignee(task.getTaskAttributes().getAssigneeUsersId());
                    if (null == tasks.getAssignee() || "".equalsIgnoreCase(tasks.getAssignee())) {
                        if (null != task.getTaskAttributes().getSubState() &&
                            !"".equalsIgnoreCase(task.getTaskAttributes().getSubState())) {
                            tasks.setSubState(task.getTaskAttributes().getSubState());
                            if (SubstateEnum.REASSIGNED
                                            .toString()
                                            .equalsIgnoreCase(tasks.getSubState())) {
                                tasks.setAssignee(task.getTaskAttributes().getAssigneeUsersId());
                            } else if (SubstateEnum.ACQUIRED
                                                   .toString()
                                                   .equalsIgnoreCase(tasks.getSubState())) {
                                tasks.setAssignee(task.getTaskAttributes().getAcquiredBy());
                            }
                        }
                    }
                    tasksList.add(tasks);
                } catch (ParseException pe) {
                    pe.printStackTrace();
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
            taskQueryService.destroyWorkflowContext(workflowContext);
        } catch (WorkflowErrorMessage we) {
            logger.log(Level.SEVERE, getClass(), "Exception Caught : ", we.getMessage());
        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "Exception Caught : ", e.getMessage());
        }
        logger.log(Level.INFO, getClass(), "fetchMyTasksList", "Exiting fetchMyTasksList");
        return tasksList;

    }

    public List<Tasks> fetchCompletedTasksList(String username) {
        logger.log(Level.INFO, getClass(), "fetchCompletedTasksList", "Entering fetchCompletedTasksList method");
        
        List<Tasks> tasksList = new ArrayList<Tasks>();
        try {
            TaskQueryService_Service taskQueryService_Service = new TaskQueryService_Service();
            taskQueryService = taskQueryService_Service.getTaskQueryServicePort();
            // Creating Task Request Object to be passed as input to queryTasks
            TaskListRequestType taskRequest = new TaskListRequestType();

            WorkflowContextType workflowContext = new WorkflowContextType();
            CredentialType credential = new CredentialType();
            credential.setLogin(ParamCache.propertiesMap
                                          .get("BPM_ADMIN_USER")
                                          .toString());
            credential.setPassword(ParamCache.propertiesMap
                                             .get("BPM_ADMIN_PASSWORD")
                                             .toString());
//            credential.setOnBehalfOfUser(username);
            workflowContext.setCredential(credential);

            TaskPredicateQueryType predicateQuery = new TaskPredicateQueryType();

            TaskPredicateType taskPredicate = new TaskPredicateType();
            taskPredicate.setAssignmentFilter(AssignmentFilterEnum.ALL);
//            BigInteger endRowValue = BigInteger.valueOf(5000);           
//            predicateQuery.setEndRow(endRowValue);
            PredicateClauseType clause = new PredicateClauseType();
            clause.setColumn(ServiceConstants.STATE);
            clause.setOperator(PredicateOperationEnum.EQ);
            clause.setValue(StateEnum.COMPLETED.toString());
            taskPredicate.getClause().add(clause);
            predicateQuery.setPredicate(taskPredicate);

            DisplayColumnType displayColumns = new DisplayColumnType();
            displayColumns.getDisplayColumn().add(ServiceConstants.TASK_NUMBER);
            displayColumns.getDisplayColumn().add(ServiceConstants.TASKID);
            displayColumns.getDisplayColumn().add(ServiceConstants.TITLE);
            displayColumns.getDisplayColumn().add(ServiceConstants.OUTCOME);
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE1); //OppNo
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE2); //QuoteId
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE3); // Quote Rev
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE4); // Role
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE5);
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE6);
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE7);
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE8);
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE9);
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE10);

            displayColumns.getDisplayColumn().add(ServiceConstants.NUMBERATTRIBUTE1);
            displayColumns.getDisplayColumn().add(ServiceConstants.NUMBERATTRIBUTE2);
            displayColumns.getDisplayColumn().add(ServiceConstants.NUMBERATTRIBUTE3);

            TaskOptionalInfoType optionalInfo = new TaskOptionalInfoType();
            optionalInfo.getTaskOptionalInfo().add(TaskOptionalInfoEnum.COMMENTS);
            optionalInfo.getTaskOptionalInfo().add(TaskOptionalInfoEnum.PAYLOAD);

            predicateQuery.setDisplayColumnList(displayColumns);
            predicateQuery.setOptionalInfoList(optionalInfo);
            taskRequest.setWorkflowContext(workflowContext);
            taskRequest.setTaskPredicateQuery(predicateQuery);

            // Calling queryTasks method passing the request object
            TaskList taskResponse = taskQueryService.queryTasks(taskRequest);

            List<TaskT> bpmTaskList = taskResponse.getTask();
            for (TaskT task : bpmTaskList) {
                Tasks tasks = new Tasks();
                tasks.setOpportunityNumber(task.getTaskAttributes().getTextAttribute1());
                tasks.setTaskNumber(task.getTaskAttributes().getTaskNumber());
                tasks.setTaskDescription(task.getPayload().getPMComments());
                if(null != task.getPayload().getApproverComments())
                    tasks.setApprovalComments(task.getPayload().getApproverComments());
                if(null != task.getTaskAttributes().getOutcome()){
                    if(task.getTaskAttributes().getOutcome().equalsIgnoreCase(ServiceConstants.approvalTasks[0]))
                        tasks.setOutcome("APPROVED");
                    else if(task.getTaskAttributes().getOutcome().equalsIgnoreCase(ServiceConstants.approvalTasks[1]))
                        tasks.setOutcome("REJECTED");
                    else if(task.getTaskAttributes().getOutcome().equalsIgnoreCase(ServiceConstants.approvalTasks[2]))
                        tasks.setOutcome("APPROVED");
                    else if(task.getTaskAttributes().getOutcome().equalsIgnoreCase(ServiceConstants.approvalTasks[3]))
                        tasks.setOutcome("CANCELLED");
                    else
                        tasks.setOutcome(ServiceConstants.approvalTasks[4]);
                }
                
                tasks.setTaskType(task.getTaskAttributes().getTextAttribute6());
                try {
                    tasks.setGroup((String) ParamCache.groupsMap.get(task.getPayload().getRoleName()));
                    tasks.setGroupName((String)ParamCache.groupsMap.get(task.getTaskAttributes().getTextAttribute4()));
                } catch (Exception e) {
                    e.printStackTrace();
                }// Role
//                tasks.setTaskCompletedBy(task.getTaskAttributes().getTextAttribute3());
                if (null != task.getTaskAttributes().getTextAttribute3()) {
                    ActiveDirectoryServiceWrapper impl = new ActiveDirectoryServiceWrapper();
                    try {
                        User taskClosedBy = impl.fetchUserDetailsById(task.getTaskAttributes().getTextAttribute3());
                        tasks.setTaskCompletedBy(taskClosedBy.getName());
                        // The below information needs to be changed for last owner -- 15-Jan
                        User updatedUser = impl.fetchUserDetailsById(task.getTaskAttributes().getUpdatedById());
                        tasks.setAssigneeName(updatedUser.getName());
                    } catch (Exception e) {
                        tasks.setTaskCompletedBy(null);
                    }

                } else {
                    tasks.setTaskCompletedBy(null);
                }
                tasks.setTaskAssignmentDate(dateFormatConverter(task.getTaskAttributes().getAssignedDate()));
                tasks.setTaskCompletionDate(dateFormatConverter(task.getTaskAttributes().getUpdatedDate()));
                tasks.setCreationDate(dateFormatConverter(task.getTaskAttributes().getCreatedDate()));
                tasksList.add(tasks);
            }
            taskQueryService.destroyWorkflowContext(workflowContext);
            logger.log(Level.INFO, getClass(), "fetchCompletedTasksList", "Exiting fetchCompletedTasksList method");
        }
        
        catch (WorkflowErrorMessage we) {
            logger.log(Level.SEVERE, getClass(), "Exception Caught : ", we.getMessage());
        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "Exception Caught : ", e.getMessage());
        }
        logger.log(Level.INFO, getClass(), "fetchCompletedTasksList", "Exiting fetchCompletedTasksList");
        return tasksList;

    }

    public List<Tasks> fetchTasksListByOppNo(String opportunityNumber, List<String> userGroup) {
        List<Tasks> tasksList = new ArrayList<Tasks>();
        try {
            TaskQueryService_Service taskQueryService_Service = new TaskQueryService_Service();
            taskQueryService = taskQueryService_Service.getTaskQueryServicePort();
            // Creating Task Request Object to be passed as input to queryTasks
            TaskListRequestType taskRequest = new TaskListRequestType();
            CredentialType credential = new CredentialType();

            credential.setLogin(ParamCache.propertiesMap
                                          .get("BPM_ADMIN_USER")
                                          .toString());
            credential.setPassword(ParamCache.propertiesMap
                                             .get("BPM_ADMIN_PASSWORD")
                                             .toString());

            WorkflowContextType workflowContext = new WorkflowContextType();
            workflowContext.setCredential(credential);

            TaskPredicateQueryType predicateQuery = new TaskPredicateQueryType();            
            BigInteger endRowValue = BigInteger.valueOf(5000);           
            predicateQuery.setEndRow(endRowValue);
            TaskPredicateType taskPredicate = new TaskPredicateType();
            taskPredicate.setAssignmentFilter(AssignmentFilterEnum.ALL);
            PredicateClauseType clause = new PredicateClauseType();
            clause.setColumn(ServiceConstants.TEXTATTRIBUTE1);
            clause.setOperator(PredicateOperationEnum.EQ);
            clause.setValue(opportunityNumber);

            PredicateClauseType clause1 = new PredicateClauseType();
            clause1.setJoinOperator(PredicateJoinOperatorEnum.AND);
            clause1.setColumn(ServiceConstants.STATE);
            clause1.setOperator(PredicateOperationEnum.NEQ);
            clause1.setValue(StateEnum.STALE.toString());

            taskPredicate.getClause().add(clause);
            taskPredicate.getClause().add(clause1);
            predicateQuery.setPredicate(taskPredicate);

            DisplayColumnType displayColumns = new DisplayColumnType();
            displayColumns.getDisplayColumn().add(ServiceConstants.TASK_NUMBER);
            displayColumns.getDisplayColumn().add(ServiceConstants.TASKID);
            displayColumns.getDisplayColumn().add(ServiceConstants.TITLE);
            displayColumns.getDisplayColumn().add(ServiceConstants.OUTCOME);
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE1); //OppNo
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE2); //QuoteId
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE3); // Quote Rev
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE4); // Role
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE5); //Status
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE6); //Manage Approval or Manage Opportunity or pricing
            TaskOptionalInfoType optionalInfo = new TaskOptionalInfoType();
            optionalInfo.getTaskOptionalInfo().add(TaskOptionalInfoEnum.COMMENTS);
            optionalInfo.getTaskOptionalInfo().add(TaskOptionalInfoEnum.PAYLOAD);

            predicateQuery.setDisplayColumnList(displayColumns);
            predicateQuery.setOptionalInfoList(optionalInfo);
            taskRequest.setWorkflowContext(workflowContext);
            taskRequest.setTaskPredicateQuery(predicateQuery);

            // Calling queryTasks method passing the request object
            TaskList taskResponse = taskQueryService.queryTasks(taskRequest);

            List<TaskT> bpmTaskList = taskResponse.getTask();
            for (int i = bpmTaskList.size() - 1; i >= 0; i--) {
                Tasks tasks = new Tasks();
                tasks.setTaskType(bpmTaskList.get(i).getTaskAttributes().getTextAttribute6());
                tasks.setTaskId(bpmTaskList.get(i)
                                           .getTaskAttributes()
                                           .getTaskId());
                tasks.setTaskNumber(bpmTaskList.get(i)
                                               .getTaskAttributes()
                                               .getTaskNumber());
                tasks.setTitle(bpmTaskList.get(i)
                                          .getTaskAttributes()
                                          .getTitle());
                //TODO
                tasks.setTaskDescription(bpmTaskList.get(i).getPayload().getPMComments());     //task description need to fetch
//                tasks.setGroup(bpmTaskList.get(i)
//                                          .getPayload()
//                                          .getRoleName()); 
                tasks.setGroup(bpmTaskList.get(i).getTaskAttributes().getTextAttribute4()); 
                tasks.setGroupName((String) ParamCache.groupsMap.get(tasks.getGroup()));
                String dd = bpmTaskList.get(i)
                                       .getPayload()
                                       .getTaskDueDate();
                if(null != bpmTaskList.get(i).getPayload().getApproverComments())
                    tasks.setApprovalComments(bpmTaskList.get(i).getPayload().getApproverComments());
                
                if(null != bpmTaskList.get(i).getTaskAttributes().getOutcome()){
                    if(bpmTaskList.get(i).getTaskAttributes().getOutcome().equalsIgnoreCase(ServiceConstants.approvalTasks[0]))
                        tasks.setOutcome("APPROVED");
                    else if(bpmTaskList.get(i).getTaskAttributes().getOutcome().equalsIgnoreCase(ServiceConstants.approvalTasks[1]))
                        tasks.setOutcome("REJECTED");
                    else if(bpmTaskList.get(i).getTaskAttributes().getOutcome().equalsIgnoreCase(ServiceConstants.approvalTasks[2]))
                        tasks.setOutcome("APPROVED");
                    else if(bpmTaskList.get(i).getTaskAttributes().getOutcome().equalsIgnoreCase(ServiceConstants.approvalTasks[3]))
                        tasks.setOutcome("CANCELLED");
                    else
                        tasks.setOutcome(ServiceConstants.approvalTasks[4]);
                }
                if (null != dd) {
                    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    Date dueDate = (Date) formatter.parse(dd);
                    DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                    String ddNew = df.format(dueDate);
                    Date taskDueDate = (Date) df.parse(ddNew);
                    tasks.setTaskDueDate(taskDueDate);
                }
                tasks.setOpportunityNumber(bpmTaskList.get(i)
                                                      .getPayload()
                                                      .getOpportunityNumber());
                tasks.setOpportunityId(bpmTaskList.get(i)
                                                  .getPayload()
                                                  .getOpportunityId());
                tasks.setGroup(bpmTaskList.get(i)
                                          .getPayload()
                                          .getRoleName());
                tasks.setStatus(bpmTaskList.get(i)
                                           .getTaskAttributes()
                                           .getState());
                tasks.setState(bpmTaskList.get(i)
                                          .getTaskAttributes()
                                          .getState());
                String createDate = bpmTaskList.get(i)
                                               .getTaskAttributes()
                                               .getCreatedDate();
                if (null != createDate) {
                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    Date creationDate = (Date) format.parse(createDate);
                    DateFormat ddf = new SimpleDateFormat("MM/dd/yyyy");
                    String ddN = ddf.format(creationDate);
                    Date dateCreation = (Date) ddf.parse(ddN);
                    tasks.setCreationDate(dateCreation);
                }
                if (null != bpmTaskList.get(i).getTaskAttributes().getTextAttribute3()) {
                    ActiveDirectoryServiceWrapper impl = new ActiveDirectoryServiceWrapper();
                    try {
                        User taskClosedBy = impl.fetchUserDetailsById(bpmTaskList.get(i).getTaskAttributes().getTextAttribute3());
                        tasks.setTaskCompletedBy(taskClosedBy.getName());
                        //User updatedUser = impl.fetchUserDetailsById(bpmTaskList.get(i).getTaskAttributes().getUpdatedById());
                        //tasks.setAssigneeName(updatedUser.getName());
                    } catch (Exception e) {
                        tasks.setTaskCompletedBy(null);
                    }

                }
                tasks.setTaskCompletionDate(dateFormatConverter(bpmTaskList.get(i).getTaskAttributes().getUpdatedDate()));
                //       tasks.setSubState(task.getSystemAttributes().getSubstate().toString());
                tasks.setAssignee(bpmTaskList.get(i)
                                             .getTaskAttributes()
                                             .getAssigneeUsersId());

                if (null != bpmTaskList.get(i)
                                       .getTaskAttributes()
                                       .getSubState()) {
                    tasks.setSubState(bpmTaskList.get(i)
                                                 .getTaskAttributes()
                                                 .getSubState());
                    if (SubstateEnum.REASSIGNED
                                    .toString()
                                    .equalsIgnoreCase(tasks.getSubState())) {
                        tasks.setAssignee(bpmTaskList.get(i)
                                                     .getTaskAttributes()
                                                     .getAssigneeUsersId());
                    } else if (SubstateEnum.ACQUIRED
                                           .toString()
                                           .equalsIgnoreCase(tasks.getSubState())) {
                        tasks.setAssignee(bpmTaskList.get(i)
                                                     .getTaskAttributes()
                                                     .getAcquiredBy());
                    }
                }
                if(StateEnum.COMPLETED.toString().equalsIgnoreCase(tasks.getState())){
                    tasks.setAssignee(null == bpmTaskList.get(i)
                                                 .getTaskAttributes()
                                                         .getTextAttribute2() ? "" : bpmTaskList.get(i)
                                                 .getTaskAttributes()
                                                         .getTextAttribute2());
                }
                
                tasksList.add(tasks);
                    
            }
            taskQueryService.destroyWorkflowContext(workflowContext);
        } catch (WorkflowErrorMessage we) {
            logger.log(Level.SEVERE, getClass(), "Exception Caught : ", we.getMessage());
        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "Exception Caught : ", e.getMessage());
        }
        logger.log(Level.INFO, getClass(), "fetchTasksListByOppNo", "Exiting fetchTasksListByOppNo");
        return tasksList;

    }

    public Task fetchTaskDetailsById(String taskId) {
        logger.log(Level.INFO, getClass(), "fetchTaskDetailsById", "Entering fetchTaskDetailsById");
        Task task = new Task();
        try {
            TaskQueryService_Service taskQueryService_Service = new TaskQueryService_Service();
            taskQueryService = taskQueryService_Service.getTaskQueryServicePort();

            CredentialType credential = new CredentialType();

            credential.setLogin(ParamCache.propertiesMap
                                          .get("BPM_ADMIN_USER")
                                          .toString());
            credential.setPassword(ParamCache.propertiesMap
                                             .get("BPM_ADMIN_PASSWORD")
                                             .toString());

            WorkflowContextType workflowContext = new WorkflowContextType();
            workflowContext.setCredential(credential);

            TaskDetailsByIdRequestType request = new TaskDetailsByIdRequestType();
            request.setWorkflowContext(workflowContext);
            request.setTaskId(taskId);
            task = taskQueryService.getTaskDetailsById(request);
        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "Exception Caught : ", e.getMessage());
        }
        logger.log(Level.INFO, getClass(), "fetchTaskDetailsById", "Exiting fetchTaskDetailsById");
        return task;
    }
    
    public Tasks fetchTaskDetailsByID(String opportunityNumber, String taskId){
        logger.log(Level.INFO, getClass(), "fetchTaskDetailsByID", "Entering fetchTaskDetailsByID");
        Tasks task = new Tasks();
        try{
            TaskQueryService_Service taskQueryService_Service = new TaskQueryService_Service();
            taskQueryService = taskQueryService_Service.getTaskQueryServicePort();
            // Creating Task Request Object to be passed as input to queryTasks
            TaskListRequestType taskRequest = new TaskListRequestType();
            CredentialType credential = new CredentialType();

            credential.setLogin(ParamCache.propertiesMap
                                          .get("BPM_ADMIN_USER")
                                          .toString());
            credential.setPassword(ParamCache.propertiesMap
                                             .get("BPM_ADMIN_PASSWORD")
                                             .toString());

            WorkflowContextType workflowContext = new WorkflowContextType();
            workflowContext.setCredential(credential);

            TaskPredicateQueryType predicateQuery = new TaskPredicateQueryType();
            TaskPredicateType taskPredicate = new TaskPredicateType();
            taskPredicate.setAssignmentFilter(AssignmentFilterEnum.ALL);
            PredicateClauseType clause = new PredicateClauseType();
            clause.setColumn(ServiceConstants.TEXTATTRIBUTE1);
            clause.setOperator(PredicateOperationEnum.EQ);
            clause.setValue(opportunityNumber);

            PredicateClauseType clause1 = new PredicateClauseType();
            clause1.setJoinOperator(PredicateJoinOperatorEnum.AND);
            clause1.setColumn(ServiceConstants.STATE);
            clause1.setOperator(PredicateOperationEnum.EQ);
            clause1.setValue(StateEnum.COMPLETED.toString());
            
            PredicateClauseType clause2 = new PredicateClauseType();
            clause2.setJoinOperator(PredicateJoinOperatorEnum.AND);
            clause2.setColumn(ServiceConstants.taskID);
            clause2.setOperator(PredicateOperationEnum.EQ);
            clause2.setValue(taskId);

            taskPredicate.getClause().add(clause);
            taskPredicate.getClause().add(clause1);
            taskPredicate.getClause().add(clause2);
            predicateQuery.setPredicate(taskPredicate);

            DisplayColumnType displayColumns = new DisplayColumnType();
            displayColumns.getDisplayColumn().add(ServiceConstants.TASK_NUMBER);
            displayColumns.getDisplayColumn().add(ServiceConstants.TASKID);
            displayColumns.getDisplayColumn().add(ServiceConstants.TITLE);
            displayColumns.getDisplayColumn().add(ServiceConstants.OUTCOME);
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE1); //OppNo
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE2); //QuoteId
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE3); // Quote Rev
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE4); // Role
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE5); //Status
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE6);
            TaskOptionalInfoType optionalInfo = new TaskOptionalInfoType();
            optionalInfo.getTaskOptionalInfo().add(TaskOptionalInfoEnum.COMMENTS);
            optionalInfo.getTaskOptionalInfo().add(TaskOptionalInfoEnum.PAYLOAD);

            predicateQuery.setDisplayColumnList(displayColumns);
            predicateQuery.setOptionalInfoList(optionalInfo);
            taskRequest.setWorkflowContext(workflowContext);
            taskRequest.setTaskPredicateQuery(predicateQuery);

            // Calling queryTasks method passing the request object
            TaskList taskResponse = taskQueryService.queryTasks(taskRequest);
            List<TaskT> bpmTaskList = taskResponse.getTask();            
            task.setTaskCompletionDate(dateFormatConverter(bpmTaskList.get(0).getTaskAttributes().getUpdatedDate()));
            if (null != bpmTaskList.get(0).getTaskAttributes().getTextAttribute3()) {
                ActiveDirectoryServiceWrapper impl = new ActiveDirectoryServiceWrapper();
                try {
                    User taskClosedBy = impl.fetchUserDetailsById(bpmTaskList.get(0).getTaskAttributes().getTextAttribute3());
                    task.setTaskCompletedBy(taskClosedBy.getName());
                    //User updatedUser = impl.fetchUserDetailsById(bpmTaskList.get(i).getTaskAttributes().getUpdatedById());
                    //tasks.setAssigneeName(updatedUser.getName());
                } catch (Exception e) {
                    task.setTaskCompletedBy(null);
                }

            }            
            taskQueryService.destroyWorkflowContext(workflowContext);
        }
        catch(Exception ae){
            ae.printStackTrace();
        }
        logger.log(Level.INFO, getClass(), "fetchTaskDetailsByID", "Exiting fetchTaskDetailsByID");
        return task;
    }

    public List<Approval> fetchApprovalTasksForSnapshot(String opportunityNumber, String snapshotId) {
        logger.log(Level.INFO, getClass(), "fetchApprovalTasksForSnapshot", "Entering fetchApprovalTasksForSnapshot");
        List<Approval> approvalList = new ArrayList<Approval>();
        try {
            TaskQueryService_Service taskQueryService_Service = new TaskQueryService_Service();
            taskQueryService = taskQueryService_Service.getTaskQueryServicePort();

            // Creating Task Request Object to be passed as input to queryTasks
            TaskListRequestType taskRequest = new TaskListRequestType();
            CredentialType credential = new CredentialType();

            credential.setLogin(ParamCache.propertiesMap
                                          .get("BPM_ADMIN_USER")
                                          .toString());
            credential.setPassword(ParamCache.propertiesMap
                                             .get("BPM_ADMIN_PASSWORD")
                                             .toString());

            WorkflowContextType workflowContext = new WorkflowContextType();
            workflowContext.setCredential(credential);

            TaskPredicateQueryType predicateQuery = new TaskPredicateQueryType();
            TaskPredicateType taskPredicate = new TaskPredicateType();
            taskPredicate.setAssignmentFilter(AssignmentFilterEnum.ALL);
            PredicateClauseType clause = new PredicateClauseType();
            clause.setColumn(ServiceConstants.TEXTATTRIBUTE1);
            clause.setOperator(PredicateOperationEnum.EQ);
            clause.setValue(opportunityNumber);

            PredicateClauseType clause1 = new PredicateClauseType();
            clause1.setJoinOperator(PredicateJoinOperatorEnum.AND);
            clause1.setColumn(ServiceConstants.STATE);
            clause1.setOperator(PredicateOperationEnum.NEQ);
            clause1.setValue(StateEnum.STALE.toString());

            PredicateClauseType clause2 = new PredicateClauseType();
            clause2.setJoinOperator(PredicateJoinOperatorEnum.AND);
            clause2.setColumn(ServiceConstants.TEXTATTRIBUTE6);
            clause2.setOperator(PredicateOperationEnum.EQ);
            clause2.setValue(ServiceConstants.MANAGE_APPROVAL);

            PredicateClauseType clause3 = new PredicateClauseType();
            clause3.setJoinOperator(PredicateJoinOperatorEnum.AND);
            clause3.setColumn(ServiceConstants.NUMBERATTRIBUTE2);
            clause3.setOperator(PredicateOperationEnum.EQ);
            clause3.setValue(snapshotId);

            taskPredicate.getClause().add(clause);
            taskPredicate.getClause().add(clause1);
            taskPredicate.getClause().add(clause2);
            taskPredicate.getClause().add(clause3);
            predicateQuery.setPredicate(taskPredicate);

            DisplayColumnType displayColumns = new DisplayColumnType();
            displayColumns.getDisplayColumn().add(ServiceConstants.TASK_NUMBER);
            displayColumns.getDisplayColumn().add(ServiceConstants.TASKID);
            displayColumns.getDisplayColumn().add(ServiceConstants.TITLE);
            displayColumns.getDisplayColumn().add(ServiceConstants.OUTCOME);
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE1); //OppNo
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE2); //QuoteId
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE3); // Quote Rev
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE4); // Role
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE5); //Status
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE6); //OppNo
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE7); //QuoteId
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE8); // Quote Rev
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE9); // Role
            displayColumns.getDisplayColumn().add(ServiceConstants.NUMBERATTRIBUTE1); //QuoteId
            displayColumns.getDisplayColumn().add(ServiceConstants.NUMBERATTRIBUTE2); // Quote Rev
            displayColumns.getDisplayColumn().add(ServiceConstants.NUMBERATTRIBUTE3); // Role

            TaskOptionalInfoType optionalInfo = new TaskOptionalInfoType();
            optionalInfo.getTaskOptionalInfo().add(TaskOptionalInfoEnum.COMMENTS);
            optionalInfo.getTaskOptionalInfo().add(TaskOptionalInfoEnum.PAYLOAD);

            predicateQuery.setDisplayColumnList(displayColumns);
            predicateQuery.setOptionalInfoList(optionalInfo);
            taskRequest.setWorkflowContext(workflowContext);
            taskRequest.setTaskPredicateQuery(predicateQuery);

            // Calling queryTasks method passing the request object
            TaskList taskResponse = taskQueryService.queryTasks(taskRequest);

            for (TaskT task : taskResponse.getTask()) {
                Approval approval = new Approval();
                approval.setOpportunityNumber(opportunityNumber);
                approval.setSnapshotId(snapshotId);
                approval.setApprovalStatus((null != task.getTaskAttributes().getOutcome() &&
                                            !"".equalsIgnoreCase(task.getTaskAttributes().getOutcome())) ?
                                           task.getTaskAttributes().getOutcome() : "Pending");
                if (null != approval.getApprovalStatus() &&
                    ("APPROVE".equalsIgnoreCase(approval.getApprovalStatus()) ||
                     "REJECT".equalsIgnoreCase(approval.getApprovalStatus())) ||
                    "PASS".equalsIgnoreCase(approval.getApprovalStatus()) ||
                    "CANCEL".equalsIgnoreCase(approval.getApprovalStatus())) {
                    approval.setApproverId(task.getTaskAttributes().getUpdatedById());
                } else {
                    approval.setApproverId(task.getTaskAttributes().getAssigneeUsersId());
                }
                approval.setApproverRole(task.getPayload().getRoleName());
                
                approval.setApprovalNote(task.getPayload().getApproverComments());  
                /* Date approvalDate = (Date)format.parse(task.getTaskAttributes().getUpdatedDate());
                DateFormat ddf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
                String ddN=ddf.format(creationDate);*/
                approval.setApprovalDate(dateFormatConverterStr(task.getTaskAttributes().getUpdatedDate())); 
                approvalList.add(approval);
            }


            taskQueryService.destroyWorkflowContext(workflowContext);
        } catch (WorkflowErrorMessage we) {
            logger.log(Level.SEVERE, getClass(), "Exception Caught : ", we.getMessage());
        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "Exception Caught : ", e.getMessage());
        }
        logger.log(Level.INFO, getClass(), "fetchApprovalTasksForSnapshot", "Exiting fetchApprovalTasksForSnapshot");
        return approvalList;
    }

    // Following method is used to fetch a task for an user who has recived an Email notification after Approval request is initiated
    public List<Tasks> fetchTasksListByEmailOppNo(String opportunityNumber, String userID, String groupId) {
        List<Tasks> tasksList = new ArrayList<Tasks>();
        try {
            TaskQueryService_Service taskQueryService_Service = new TaskQueryService_Service();
            taskQueryService = taskQueryService_Service.getTaskQueryServicePort();
            
            // Creating Task Request Object to be passed as input to queryTasks
            TaskListRequestType taskRequest = new TaskListRequestType();
            CredentialType credential = new CredentialType();

            credential.setLogin(ParamCache.propertiesMap
                                          .get("BPM_ADMIN_USER")
                                          .toString());
            credential.setPassword(ParamCache.propertiesMap
                                             .get("BPM_ADMIN_PASSWORD")
                                             .toString());

            WorkflowContextType workflowContext = new WorkflowContextType();
            workflowContext.setCredential(credential);

            TaskPredicateQueryType predicateQuery = new TaskPredicateQueryType();
            TaskPredicateType taskPredicate = new TaskPredicateType();
            taskPredicate.setAssignmentFilter(AssignmentFilterEnum.ALL);
            PredicateClauseType clause = new PredicateClauseType();
            clause.setColumn(ServiceConstants.TEXTATTRIBUTE1);
            clause.setOperator(PredicateOperationEnum.EQ);
            clause.setValue(opportunityNumber);

            PredicateClauseType clause1 = new PredicateClauseType();
            clause1.setJoinOperator(PredicateJoinOperatorEnum.AND);
            clause1.setColumn(ServiceConstants.STATE);
            clause1.setOperator(PredicateOperationEnum.EQ);
            clause1.setValue(StateEnum.ASSIGNED.toString());
            
            PredicateClauseType clause2 = new PredicateClauseType();
            clause2.setJoinOperator(PredicateJoinOperatorEnum.AND);
            clause2.setColumn(ServiceConstants.TEXTATTRIBUTE6);
            clause2.setOperator(PredicateOperationEnum.EQ);
            clause2.setValue(ServiceConstants.MANAGE_APPROVAL);
            
            PredicateClauseType clause3 = new PredicateClauseType();
            clause3.setJoinOperator(PredicateJoinOperatorEnum.AND);
            clause3.setColumn(ServiceConstants.TEXTATTRIBUTE4);
            clause3.setOperator(PredicateOperationEnum.EQ);
            clause3.setValue(groupId);
            
            PredicateClauseType clause4 = new PredicateClauseType();
            clause4.setJoinOperator(PredicateJoinOperatorEnum.AND);
            clause4.setColumn(ServiceConstants.OWNERUSER);
            clause4.setOperator(PredicateOperationEnum.EQ);
            clause4.setValue(userID);

            taskPredicate.getClause().add(clause);
            taskPredicate.getClause().add(clause1);
            taskPredicate.getClause().add(clause2);
            taskPredicate.getClause().add(clause3);
            taskPredicate.getClause().add(clause4);
            predicateQuery.setPredicate(taskPredicate);

            DisplayColumnType displayColumns = new DisplayColumnType();
            displayColumns.getDisplayColumn().add(ServiceConstants.TASK_NUMBER);
            displayColumns.getDisplayColumn().add(ServiceConstants.TASKID);
            displayColumns.getDisplayColumn().add(ServiceConstants.TITLE);
            displayColumns.getDisplayColumn().add(ServiceConstants.OUTCOME);
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE1); //OppNo
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE2); //QuoteId
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE3); // Quote Rev
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE4); // Role
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE5); //Status
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE6); 
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE7); 
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE8); 
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE9);             
            
            displayColumns.getDisplayColumn().add(ServiceConstants.NUMBERATTRIBUTE1);
            displayColumns.getDisplayColumn().add(ServiceConstants.NUMBERATTRIBUTE2);
            displayColumns.getDisplayColumn().add(ServiceConstants.NUMBERATTRIBUTE3);
            displayColumns.getDisplayColumn().add(ServiceConstants.OWNERUSER);
            TaskOptionalInfoType optionalInfo = new TaskOptionalInfoType();
            optionalInfo.getTaskOptionalInfo().add(TaskOptionalInfoEnum.COMMENTS);
            optionalInfo.getTaskOptionalInfo().add(TaskOptionalInfoEnum.PAYLOAD);

            predicateQuery.setDisplayColumnList(displayColumns);
            predicateQuery.setOptionalInfoList(optionalInfo);
            taskRequest.setWorkflowContext(workflowContext);
            taskRequest.setTaskPredicateQuery(predicateQuery);
            // Calling queryTasks method passing the request object
            TaskList taskResponse = taskQueryService.queryTasks(taskRequest);
            logger.log(Level.INFO, getClass(), "fetchMyTasksList", "BPM task List: " + taskResponse.getTask().size());
            List<TaskT> bpmTaskList = taskResponse.getTask();
            for (TaskT task : bpmTaskList) {
                Tasks tasks = new Tasks();
                tasks.setTaskId(task.getTaskAttributes().getTaskId());
                tasks.setTaskNumber(task.getTaskAttributes().getTaskNumber());
                tasks.setTaskDescription("");
                String dd = task.getPayload().getTaskDueDate();
                if (null != dd) {
                    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    Date dueDate = (Date) formatter.parse(dd);
                    DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                    String ddNew = df.format(dueDate);
                    Date taskDueDate = (Date) df.parse(ddNew);
                    tasks.setTaskDueDate(taskDueDate);
                }
                tasks.setOpportunityNumber(task.getPayload().getOpportunityNumber());
                tasks.setOpportunityId(task.getPayload().getOpportunityId());
                tasks.setGroup(task.getPayload().getRoleName());
                tasks.setGroupName(ParamCache.groupsMap
                                             .get(tasks.getGroup())
                                             .toString());
                tasks.setState(task.getTaskAttributes().getState());
                tasks.setTaskType(task.getTaskAttributes().getTextAttribute6());
                tasks.setOwnerUser(task.getTaskAttributes().getOwnerUser());
                String createDate = task.getTaskAttributes().getCreatedDate();
                if (null != createDate) {
                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    Date creationDate = (Date) format.parse(createDate);
                    DateFormat ddf = new SimpleDateFormat("MM/dd/yyyy");
                    String ddN = ddf.format(creationDate);
                    Date dateCreation = (Date) ddf.parse(ddN);
                    tasks.setCreationDate(dateCreation);
                }
                //       tasks.setSubState(task.getSystemAttributes().getSubstate().toString());
                tasks.setAssignee(task.getTaskAttributes().getAssigneeUsersId());

                if (null != task.getTaskAttributes().getSubState()) {
                    tasks.setSubState(task.getTaskAttributes().getSubState());
                    if (SubstateEnum.REASSIGNED
                                    .toString()
                                    .equalsIgnoreCase(tasks.getSubState())) {
                        tasks.setAssignee(task.getTaskAttributes().getAssigneeUsersId());
                    } else if (SubstateEnum.ACQUIRED
                                           .toString()
                                           .equalsIgnoreCase(tasks.getSubState())) {
                        tasks.setAssignee(task.getTaskAttributes().getAcquiredBy());
                    }
                }
                tasksList.add(tasks);
            }
            taskQueryService.destroyWorkflowContext(workflowContext);
        } catch (WorkflowErrorMessage we) {
            logger.log(Level.SEVERE, getClass(), "Exception Caught : ", we.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "Exception Caught : ", e.getMessage());
        }
        logger.log(Level.INFO, getClass(), "fetchMyTasksList", "tasksList contents ===" + tasksList.size());
        logger.log(Level.INFO, getClass(), "fetchTasksListByEmailOppNo", "Exiting fetchTasksListByEmailOppNo");
        return tasksList;
    }

    public List<Tasks> fetchCustDueDateNotifTaskList(String opportunityNumber) {
        logger.log(Level.INFO, getClass(), "fetchCustDueDateNotifTaskList", "Entering fetchCustDueDateNotifTaskList");
        List<Tasks> tasksList = new ArrayList<Tasks>();
        try {
            TaskQueryService_Service taskQueryService_Service = new TaskQueryService_Service();
            taskQueryService = taskQueryService_Service.getTaskQueryServicePort();
            // Creating Task Request Object to be passed as input to queryTasks
            TaskListRequestType taskRequest = new TaskListRequestType();
            CredentialType credential = new CredentialType();
            credential.setLogin(ParamCache.propertiesMap
                                          .get("BPM_ADMIN_USER")
                                          .toString());
            credential.setPassword(ParamCache.propertiesMap
                                             .get("BPM_ADMIN_PASSWORD")
                                             .toString());
    
            WorkflowContextType workflowContext = new WorkflowContextType();
            workflowContext.setCredential(credential);
    
            TaskPredicateQueryType predicateQuery = new TaskPredicateQueryType();
            TaskPredicateType taskPredicate = new TaskPredicateType();
            taskPredicate.setAssignmentFilter(AssignmentFilterEnum.ALL);
                        
            PredicateClauseType clause = new PredicateClauseType();
            clause.setColumn(ServiceConstants.TEXTATTRIBUTE1);
            clause.setOperator(PredicateOperationEnum.EQ);
            clause.setValue(opportunityNumber);
    
            PredicateClauseType clause1 = new PredicateClauseType();
            clause1.setJoinOperator(PredicateJoinOperatorEnum.AND);
            clause1.setColumn(ServiceConstants.STATE);
            clause1.setOperator(PredicateOperationEnum.NEQ);
            clause1.setValue(StateEnum.ASSIGNED.toString());
    
            PredicateClauseType clause2 = new PredicateClauseType();
            clause2.setJoinOperator(PredicateJoinOperatorEnum.AND);
            clause2.setColumn(ServiceConstants.TEXTATTRIBUTE6);
            clause2.setOperator(PredicateOperationEnum.EQ);
            clause2.setValue(ServiceConstants.PRICING_MANAGER);
                        
            taskPredicate.getClause().add(clause);
            taskPredicate.getClause().add(clause1);
            taskPredicate.getClause().add(clause2);
            predicateQuery.setPredicate(taskPredicate);
    
            DisplayColumnType displayColumns = new DisplayColumnType();
            displayColumns.getDisplayColumn().add(ServiceConstants.TASK_NUMBER);
            displayColumns.getDisplayColumn().add(ServiceConstants.TASKID);
            displayColumns.getDisplayColumn().add(ServiceConstants.TITLE);
            displayColumns.getDisplayColumn().add(ServiceConstants.OUTCOME);
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE1); //OppNo
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE2); //QuoteId
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE3); // Quote Rev
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE4); // Role
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE5); //Status
    
            TaskOptionalInfoType optionalInfo = new TaskOptionalInfoType();
            optionalInfo.getTaskOptionalInfo().add(TaskOptionalInfoEnum.COMMENTS);
            optionalInfo.getTaskOptionalInfo().add(TaskOptionalInfoEnum.PAYLOAD);
    
            predicateQuery.setDisplayColumnList(displayColumns);
            predicateQuery.setOptionalInfoList(optionalInfo);
            taskRequest.setWorkflowContext(workflowContext);
            taskRequest.setTaskPredicateQuery(predicateQuery);
    
            // Calling queryTasks method passing the request object
            TaskList taskResponse = taskQueryService.queryTasks(taskRequest);
    
            List<TaskT> bpmTaskList = taskResponse.getTask();
            for (int i = bpmTaskList.size() - 1; i >= 0; i--) {
                Tasks tasks = new Tasks();
                tasks.setOpportunityNumber(bpmTaskList.get(i).getTaskAttributes().getTextAttribute1());
                tasks.setGroup(bpmTaskList.get(i).getPayload().getRoleName()); 
                tasks.setGroupName((String) ParamCache.groupsMap.get(tasks.getGroup()));
                
                tasksList.add(tasks);     
            }
            logger.log(Level.INFO, getClass(), "fetchCustDueDateNotifTaskList", "BEFORE DESTROY SIZE IS - " + Integer.toString(tasksList.size()));
            taskQueryService.destroyWorkflowContext(workflowContext);
        } catch (WorkflowErrorMessage we) {
            logger.log(Level.SEVERE, getClass(), "Exception Caught : ", we.getMessage());
        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "Exception Caught : ", e.getMessage());
        }
        logger.log(Level.INFO, getClass(), "fetchCustDueDateNotifTaskList", "SIZE IS - " + Integer.toString(tasksList.size()));
        logger.log(Level.INFO, getClass(), "fetchCustDueDateNotifTaskList", "Exiting fetchCustDueDateNotifTaskList");
        return tasksList;
    }
    
    public List<Approval> fetchApprovalReminderNotifTaskList(String opportunityNumber){
        logger.log(Level.INFO, getClass(), "fetchApprovalReminderNotifTaskList", "Entering fetchApprovalReminderNotifTaskList");
        List<Approval> approvalList = new ArrayList<Approval>();
        try {
            TaskQueryService_Service taskQueryService_Service = new TaskQueryService_Service();
            taskQueryService = taskQueryService_Service.getTaskQueryServicePort();

            // Creating Task Request Object to be passed as input to queryTasks
            TaskListRequestType taskRequest = new TaskListRequestType();
            CredentialType credential = new CredentialType();

            credential.setLogin(ParamCache.propertiesMap
                                          .get("BPM_ADMIN_USER")
                                          .toString());
            credential.setPassword(ParamCache.propertiesMap
                                             .get("BPM_ADMIN_PASSWORD")
                                             .toString());

            WorkflowContextType workflowContext = new WorkflowContextType();
            workflowContext.setCredential(credential);

            TaskPredicateQueryType predicateQuery = new TaskPredicateQueryType();
            TaskPredicateType taskPredicate = new TaskPredicateType();
            taskPredicate.setAssignmentFilter(AssignmentFilterEnum.ALL);
            PredicateClauseType clause = new PredicateClauseType();
            clause.setColumn(ServiceConstants.TEXTATTRIBUTE1);
            clause.setOperator(PredicateOperationEnum.EQ);
            clause.setValue(opportunityNumber);

            PredicateClauseType clause1 = new PredicateClauseType();
            clause1.setJoinOperator(PredicateJoinOperatorEnum.AND);
            clause1.setColumn(ServiceConstants.STATE);
            clause1.setOperator(PredicateOperationEnum.EQ);
            clause1.setValue(StateEnum.ASSIGNED.toString());

            PredicateClauseType clause2 = new PredicateClauseType();
            clause2.setJoinOperator(PredicateJoinOperatorEnum.AND);
            clause2.setColumn(ServiceConstants.TEXTATTRIBUTE6);
            clause2.setOperator(PredicateOperationEnum.EQ);
            clause2.setValue(ServiceConstants.MANAGE_APPROVAL);
            
            taskPredicate.getClause().add(clause);
            taskPredicate.getClause().add(clause1);
            taskPredicate.getClause().add(clause2);
            predicateQuery.setPredicate(taskPredicate);

            DisplayColumnType displayColumns = new DisplayColumnType();
            displayColumns.getDisplayColumn().add(ServiceConstants.TASK_NUMBER);
            displayColumns.getDisplayColumn().add(ServiceConstants.TASKID);
            displayColumns.getDisplayColumn().add(ServiceConstants.TITLE);
            displayColumns.getDisplayColumn().add(ServiceConstants.OUTCOME);
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE1); //OppNo
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE2); //QuoteId
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE3); // Quote Rev
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE4); // Role
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE5); //Status
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE6); //OppNo
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE7); //QuoteId
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE8); // Quote Rev
            displayColumns.getDisplayColumn().add(ServiceConstants.TEXTATTRIBUTE9); // Role
            displayColumns.getDisplayColumn().add(ServiceConstants.NUMBERATTRIBUTE1); //QuoteId
            displayColumns.getDisplayColumn().add(ServiceConstants.NUMBERATTRIBUTE2); // Quote Rev
            displayColumns.getDisplayColumn().add(ServiceConstants.NUMBERATTRIBUTE3); // Role

            TaskOptionalInfoType optionalInfo = new TaskOptionalInfoType();
            optionalInfo.getTaskOptionalInfo().add(TaskOptionalInfoEnum.COMMENTS);

            predicateQuery.setDisplayColumnList(displayColumns);
            predicateQuery.setOptionalInfoList(optionalInfo);
            taskRequest.setWorkflowContext(workflowContext);
            taskRequest.setTaskPredicateQuery(predicateQuery);

            // Calling queryTasks method passing the request object
            TaskList taskResponse = taskQueryService.queryTasks(taskRequest);

            for (TaskT task : taskResponse.getTask()) {
                Approval approval = new Approval();
                approval.setOpportunityNumber(opportunityNumber);
                approval.setApprovalStatus((null != task.getTaskAttributes().getOutcome() &&
                                            !"".equalsIgnoreCase(task.getTaskAttributes().getOutcome())) ?
                                           task.getTaskAttributes().getOutcome() : "Pending");
                if (null != approval.getApprovalStatus() &&
                    ("APPROVE".equalsIgnoreCase(approval.getApprovalStatus()) ||
                     "REJECT".equalsIgnoreCase(approval.getApprovalStatus())) ||
                    "PASS".equalsIgnoreCase(approval.getApprovalStatus()) ||
                    "CANCEL".equalsIgnoreCase(approval.getApprovalStatus())) {
                    approval.setApproverId(task.getTaskAttributes().getUpdatedById());
                } else {
                    approval.setApproverId(task.getTaskAttributes().getAssigneeUsersId());
                }
                approval.setApproverRole(task.getTaskAttributes().getTextAttribute4());
                /* Date approvalDate = (Date)format.parse(task.getTaskAttributes().getUpdatedDate());
                DateFormat ddf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
                String ddN=ddf.format(creationDate);*/
                approval.setApprovalDate(task.getTaskAttributes().getUpdatedDate());
                approval.setTaskNumber(task.getTaskAttributes().getTaskNumber());
                if(approval.getApprovalStatus().equalsIgnoreCase("Pending")){
                    
                    approvalList.add(approval);
                }
            }
            taskQueryService.destroyWorkflowContext(workflowContext);
        } catch (WorkflowErrorMessage we) {
            logger.log(Level.SEVERE, getClass(), "Exception Caught : ", we.getMessage());
        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "Exception Caught : ", e.getMessage());
        }
        logger.log(Level.INFO, getClass(), "fetchApprovalReminderNotifTaskList", "Exiting fetchApprovalReminderNotifTaskList");
        return approvalList;
    }
    
    public Date dateFormatConverter(String dateString) {
        Date newDate = null;
        DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (null != dateString) {
                newDate = dateformat.parse(dateString);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newDate;
    }
    
    public String dateFormatConverterStr(String dateString) {
    Date newDate = null;
    String newStringDate="";
    DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
     try {
        if (null != dateString) {
     newDate = dateformat.parse(dateString);
     }
    newStringDate= dateformat.format(newDate);
     } catch (Exception e) {
     e.printStackTrace();
     }
     return newStringDate;
    }
    //    public static void main(String args[]){
    //           TaskQueryServiceWrapper obj = new TaskQueryServiceWrapper();
    //          
    //    }
    
    public Task fetchTaskDetailsByNumber(String taskNumber) {
        logger.log(Level.INFO, getClass(), "fetchTaskDetailsByNumber", "Entering fetchTaskDetailsByNumber");
        Task task = new Task();
        try {
            TaskQueryService_Service taskQueryService_Service = new TaskQueryService_Service();
            taskQueryService = taskQueryService_Service.getTaskQueryServicePort();

            CredentialType credential = new CredentialType();

            credential.setLogin(ParamCache.propertiesMap
                                          .get("BPM_ADMIN_USER")
                                          .toString());
            credential.setPassword(ParamCache.propertiesMap
                                             .get("BPM_ADMIN_PASSWORD")
                                             .toString());

            WorkflowContextType workflowContext = new WorkflowContextType();
            workflowContext.setCredential(credential);
            
            TaskDetailsByNumberRequestType request = new TaskDetailsByNumberRequestType();
            request.setWorkflowContext(workflowContext);
            request.setTaskNumber(new BigInteger(taskNumber));            
            task = taskQueryService.getTaskDetailsByNumber(request);    
        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "Exception Caught : ", e.getMessage());
        }
        logger.log(Level.INFO, getClass(), "fetchTaskDetailsById", "Exiting fetchTaskDetailsById");
        return task;
    }
}
