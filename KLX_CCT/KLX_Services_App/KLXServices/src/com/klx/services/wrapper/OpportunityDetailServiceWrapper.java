package com.klx.services.wrapper;

import com.klx.cct.osc.user.v1.GetOpptyRequestT;
import com.klx.cct.osc.user.v1.GetOpptyResponseT;
import com.klx.common.logger.KLXDMSLogger;
import com.klx.services.entities.Opportunity;
import com.klx.services.entities.Tasks;


import com.klx.services.proxy.opportunityDetail.OSCOpptyUtilityPortType;
import com.klx.services.proxy.opportunityDetail.OSCOpptyUtilityPortType_Service;


import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class OpportunityDetailServiceWrapper {
    public OpportunityDetailServiceWrapper() {
        super();
    }
    private static KLXDMSLogger logger = KLXDMSLogger.getLogger();
    
    public Opportunity getOpportunityDetails(String oscOpportunityId){ 
        logger.log(Level.INFO, getClass(), "getOpportunityDetails",  "Entering getOpportunityDetails method");
        Opportunity opportunity = new Opportunity(); 
        try{
        OSCOpptyUtilityPortType_Service oSCOpptyUtilityPortType_Service = new OSCOpptyUtilityPortType_Service();
        
        OSCOpptyUtilityPortType oSCOpptyUtilityPortType = oSCOpptyUtilityPortType_Service.getOSCOpptyUtilityPortTypePort();
        
        GetOpptyRequestT request = new GetOpptyRequestT();
        
        request.setOpportunityId(oscOpportunityId);
        GetOpptyResponseT response = oSCOpptyUtilityPortType.getOpptyDetailsOnId(request);
        
        logger.log(Level.INFO, getClass(), "getOpportunityDetails",  "Webservice Response-"+response+"-");
            
        if(null != response){
            opportunity.setOpportunityNumber(response.getOptyNumber());
            opportunity.setOpportunityName(response.getName());
            opportunity.setOpportunityId(response.getOpportunityId());
        }
        }catch(Exception e){
                e.printStackTrace();
        }
        
        logger.log(Level.INFO, getClass(), "getOpportunityDetails",  "Exiting getOpportunityDetails method");
        return opportunity;
    }
    
}
