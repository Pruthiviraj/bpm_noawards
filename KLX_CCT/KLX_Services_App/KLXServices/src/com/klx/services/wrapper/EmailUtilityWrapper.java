package com.klx.services.wrapper;

import com.klx.services.proxy.emailUtility.Cctnotificationporttype;
import com.klx.services.proxy.emailUtility.Cctnotificationporttypeservice;

import com.klx.xmlns.schema.cct.notification.v1.CCTNotification;

import com.klx.xmlns.schema.cct.notification.v1.CCTNotificationHeaderInfoType;

import com.klx.xmlns.schema.cct.notification.v1.Response;

import javax.xml.ws.WebServiceRef;

public class EmailUtilityWrapper {
    public EmailUtilityWrapper() {
        super();
    }
    @WebServiceRef    
    private static Cctnotificationporttypeservice cctNotificationService;
    private Cctnotificationporttype cctNotificationPort;
    
    public String sendEmailNotification(String source, String opptyNum, String oscId, String notfcnTemplateId){
            String status = "";
            try {
                Cctnotificationporttypeservice cctNotificationService = new Cctnotificationporttypeservice();
                cctNotificationPort = cctNotificationService.getCctnotificationporttypePort();

                CCTNotification payload = new CCTNotification();

                CCTNotificationHeaderInfoType cCTNotificationHeaderInfoType = new CCTNotificationHeaderInfoType();
                cCTNotificationHeaderInfoType.setSource(source);
                cCTNotificationHeaderInfoType.setOpportunityId(oscId);
                cCTNotificationHeaderInfoType.setOpportunityNumber(opptyNum);
                cCTNotificationHeaderInfoType.setNotificationTemplateId(notfcnTemplateId);
                payload.setCCTNotificationHeader(cCTNotificationHeaderInfoType);
                Response resp = cctNotificationPort.sendEmail(payload);
                status = resp.getStatus();
                
            } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
            }
            return status;

        }    
    
    public String sendCustDueDateNotification(String source, String opptyNum, String notfcnTemplateId, String role){
            String status = "";
            try {
                Cctnotificationporttypeservice cctNotificationService = new Cctnotificationporttypeservice();
                cctNotificationPort = cctNotificationService.getCctnotificationporttypePort();

                CCTNotification payload = new CCTNotification();

                CCTNotificationHeaderInfoType cCTNotificationHeaderInfoType = new CCTNotificationHeaderInfoType();
                cCTNotificationHeaderInfoType.setSource(source);
                cCTNotificationHeaderInfoType.setOpportunityNumber(opptyNum);
                cCTNotificationHeaderInfoType.setNotificationTemplateId(notfcnTemplateId);
                cCTNotificationHeaderInfoType.setRole(role);
                payload.setCCTNotificationHeader(cCTNotificationHeaderInfoType);
                Response resp = cctNotificationPort.sendEmail(payload);
                status = resp.getStatus();
                
            } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
            }
            return status;

    }
    
    public String sendApprovalNotification(String source, String opptyNum, String notfcnTemplateId, String apprUserId, String role, String taskNumber){
            String status = "";
            try {
                Cctnotificationporttypeservice cctNotificationService = new Cctnotificationporttypeservice();
                cctNotificationPort = cctNotificationService.getCctnotificationporttypePort();

                CCTNotification payload = new CCTNotification();

                CCTNotificationHeaderInfoType cCTNotificationHeaderInfoType = new CCTNotificationHeaderInfoType();
                cCTNotificationHeaderInfoType.setSource(source);
                cCTNotificationHeaderInfoType.setOpportunityNumber(opptyNum);
                cCTNotificationHeaderInfoType.setNotificationTemplateId(notfcnTemplateId);
                cCTNotificationHeaderInfoType.setApprovalUserId(apprUserId);
                cCTNotificationHeaderInfoType.setRole(role);
                cCTNotificationHeaderInfoType.setAttribute4(role);
                cCTNotificationHeaderInfoType.setAttribute3(taskNumber);
                payload.setCCTNotificationHeader(cCTNotificationHeaderInfoType);
                Response resp = cctNotificationPort.sendEmail(payload);
                status = resp.getStatus();
                
            } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
            }
            return status;

        }   
//    public static void main(String args[]){
//        EmailUtilityWrapper emailObj = new EmailUtilityWrapper();

//    }
}
