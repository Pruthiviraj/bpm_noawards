
package com.klx.bpm.cct.tqs.utility.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.klx.bpm.cct.tqs.utility.v1 package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TaskQueryInput_QNAME =
        new QName("http://www.klx.com/bpm/cct/tqs/utility/V1.0", "TaskQueryInput");
    private final static QName _TaskQueryOutput_QNAME =
        new QName("http://www.klx.com/bpm/cct/tqs/utility/V1.0", "TaskQueryOutput");
    private final static QName _AssignedUsersForOpportunityReq_QNAME =
        new QName("http://www.klx.com/bpm/cct/tqs/utility/V1.0", "AssignedUsersForOpportunityReq");
    private final static QName _AssignedUsersForOpportunityRsp_QNAME =
        new QName("http://www.klx.com/bpm/cct/tqs/utility/V1.0", "AssignedUsersForOpportunityRsp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.klx.bpm.cct.tqs.utility.v1
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TaskQueryInputT }
     *
     */
    public TaskQueryInputT createTaskQueryInputT() {
        return new TaskQueryInputT();
    }

    /**
     * Create an instance of {@link TaskQueryOutputT }
     *
     */
    public TaskQueryOutputT createTaskQueryOutputT() {
        return new TaskQueryOutputT();
    }

    /**
     * Create an instance of {@link AssignedUsersForOpportunityReqT }
     *
     */
    public AssignedUsersForOpportunityReqT createAssignedUsersForOpportunityReqT() {
        return new AssignedUsersForOpportunityReqT();
    }

    /**
     * Create an instance of {@link AssignedUsersForOpportunityRspT }
     *
     */
    public AssignedUsersForOpportunityRspT createAssignedUsersForOpportunityRspT() {
        return new AssignedUsersForOpportunityRspT();
    }

    /**
     * Create an instance of {@link AssignedUsersT }
     *
     */
    public AssignedUsersT createAssignedUsersT() {
        return new AssignedUsersT();
    }

    /**
     * Create an instance of {@link Roles }
     *
     */
    public Roles createRoles() {
        return new Roles();
    }

    /**
     * Create an instance of {@link UsersT }
     *
     */
    public UsersT createUsersT() {
        return new UsersT();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaskQueryInputT }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/bpm/cct/tqs/utility/V1.0", name = "TaskQueryInput")
    public JAXBElement<TaskQueryInputT> createTaskQueryInput(TaskQueryInputT value) {
        return new JAXBElement<TaskQueryInputT>(_TaskQueryInput_QNAME, TaskQueryInputT.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaskQueryOutputT }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/bpm/cct/tqs/utility/V1.0", name = "TaskQueryOutput")
    public JAXBElement<TaskQueryOutputT> createTaskQueryOutput(TaskQueryOutputT value) {
        return new JAXBElement<TaskQueryOutputT>(_TaskQueryOutput_QNAME, TaskQueryOutputT.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AssignedUsersForOpportunityReqT }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/bpm/cct/tqs/utility/V1.0", name = "AssignedUsersForOpportunityReq")
    public JAXBElement<AssignedUsersForOpportunityReqT> createAssignedUsersForOpportunityReq(AssignedUsersForOpportunityReqT value) {
        return new JAXBElement<AssignedUsersForOpportunityReqT>(_AssignedUsersForOpportunityReq_QNAME,
                                                                AssignedUsersForOpportunityReqT.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AssignedUsersForOpportunityRspT }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/bpm/cct/tqs/utility/V1.0", name = "AssignedUsersForOpportunityRsp")
    public JAXBElement<AssignedUsersForOpportunityRspT> createAssignedUsersForOpportunityRsp(AssignedUsersForOpportunityRspT value) {
        return new JAXBElement<AssignedUsersForOpportunityRspT>(_AssignedUsersForOpportunityRsp_QNAME,
                                                                AssignedUsersForOpportunityRspT.class, null, value);
    }

}
