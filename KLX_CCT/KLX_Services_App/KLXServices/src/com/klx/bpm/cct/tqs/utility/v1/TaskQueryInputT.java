
package com.klx.bpm.cct.tqs.utility.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for TaskQueryInputT complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="TaskQueryInputT"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TaskId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Action" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OSBMessageId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UserComments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Role" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UserId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SnapshotId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BatchId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OpportunityNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Tasktitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="textAttribute1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="textAttribute2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="textAttribute3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TaskDueDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="CustomerDueDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaskQueryInputT",
         propOrder =
         { "taskId", "taskNumber", "action", "osbMessageId", "userComments", "role", "userId", "loggedUserId",
           "taskState", "snapshotId", "batchId", "opportunityNumber", "tasktitle", "state", "textAttribute1",
           "textAttribute2", "textAttribute3", "taskDueDate", "customerDueDate", "reAssignedUserId",
           "reAssignedGroupId", "approvalSource"
    })
public class TaskQueryInputT {

    @XmlElement(name = "TaskId", required = true)
    protected String taskId;
    @XmlElement(name = "Action")
    protected String action;
    @XmlElement(name = "OSBMessageId")
    protected String osbMessageId;
    @XmlElement(name = "UserComments")
    protected String userComments;
    @XmlElement(name = "Role")
    protected String role;
    @XmlElement(name = "UserId")
    protected String userId;
    @XmlElement(name = "SnapshotId")
    protected String snapshotId;
    @XmlElement(name = "BatchId")
    protected String batchId;
    @XmlElement(name = "OpportunityNumber")
    protected String opportunityNumber;
    @XmlElement(name = "Tasktitle")
    protected String tasktitle;
    @XmlElement(name = "State")
    protected String state;
    protected String textAttribute1;
    protected String textAttribute2;
    protected String textAttribute3;
    @XmlElement(name = "TaskDueDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar taskDueDate;
    @XmlElement(name = "CustomerDueDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar customerDueDate;
    @XmlElement(name = "ApprovalSource")
    protected String approvalSource;
    @XmlElement(name = "LoggedUserId")
    protected String loggedUserId;
    @XmlElement(name = "ReAssignedGroupId")
    protected String reAssignedGroupId;
    @XmlElement(name = "ReAssignedUserId")
    protected String reAssignedUserId;
    @XmlElement(name = "TaskNumber")
    protected String taskNumber;
    @XmlElement(name = "TaskState")
    protected String taskState;

    /**
     * Gets the value of the taskId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * Sets the value of the taskId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTaskId(String value) {
        this.taskId = value;
    }

    /**
     * Gets the value of the action property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAction(String value) {
        this.action = value;
    }

    /**
     * Gets the value of the osbMessageId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOSBMessageId() {
        return osbMessageId;
    }

    /**
     * Sets the value of the osbMessageId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOSBMessageId(String value) {
        this.osbMessageId = value;
    }

    /**
     * Gets the value of the userComments property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getUserComments() {
        return userComments;
    }

    /**
     * Sets the value of the userComments property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setUserComments(String value) {
        this.userComments = value;
    }

    /**
     * Gets the value of the role property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getRole() {
        return role;
    }

    /**
     * Sets the value of the role property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setRole(String value) {
        this.role = value;
    }

    /**
     * Gets the value of the userId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the snapshotId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSnapshotId() {
        return snapshotId;
    }

    /**
     * Sets the value of the snapshotId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSnapshotId(String value) {
        this.snapshotId = value;
    }

    /**
     * Gets the value of the batchId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getBatchId() {
        return batchId;
    }

    /**
     * Sets the value of the batchId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setBatchId(String value) {
        this.batchId = value;
    }

    /**
     * Gets the value of the opportunityNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOpportunityNumber() {
        return opportunityNumber;
    }

    /**
     * Sets the value of the opportunityNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOpportunityNumber(String value) {
        this.opportunityNumber = value;
    }

    /**
     * Gets the value of the tasktitle property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTasktitle() {
        return tasktitle;
    }

    /**
     * Sets the value of the tasktitle property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTasktitle(String value) {
        this.tasktitle = value;
    }

    /**
     * Gets the value of the state property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the textAttribute1 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTextAttribute1() {
        return textAttribute1;
    }

    /**
     * Sets the value of the textAttribute1 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTextAttribute1(String value) {
        this.textAttribute1 = value;
    }

    /**
     * Gets the value of the textAttribute2 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTextAttribute2() {
        return textAttribute2;
    }

    /**
     * Sets the value of the textAttribute2 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTextAttribute2(String value) {
        this.textAttribute2 = value;
    }

    /**
     * Gets the value of the textAttribute3 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTextAttribute3() {
        return textAttribute3;
    }

    /**
     * Sets the value of the textAttribute3 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTextAttribute3(String value) {
        this.textAttribute3 = value;
    }

    /**
     * Gets the value of the taskDueDate property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getTaskDueDate() {
        return taskDueDate;
    }

    /**
     * Sets the value of the taskDueDate property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setTaskDueDate(XMLGregorianCalendar value) {
        this.taskDueDate = value;
    }

    /**
     * Gets the value of the customerDueDate property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getCustomerDueDate() {
        return customerDueDate;
    }

    /**
     * Sets the value of the customerDueDate property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setCustomerDueDate(XMLGregorianCalendar value) {
        this.customerDueDate = value;
    }

    /**
     * Gets the value of the approvalSource property.
     *
     * @return
     * possible object is
     * {@link String}
     *
     */
    public String getApprovalSource() {
        return approvalSource;
    }

    /**
     * Gets the value of the loggedUserId property.
     *
     * @return
     * possible object is
     * {@link String}
     *
     */
    public String getLoggedUserId() {
        return loggedUserId;
    }

    /**
     * Gets the value of the reAssignedGroupId property.
     *
     * @return
     * possible object is
     * {@link String}
     *
     */
    public String getReAssignedGroupId() {
        return reAssignedGroupId;
    }

    /**
     * Gets the value of the reAssignedUserId property.
     *
     * @return
     * possible object is
     * {@link String}
     *
     */
    public String getReAssignedUserId() {
        return reAssignedUserId;
    }

    /**
     * Gets the value of the taskNumber property.
     *
     * @return
     * possible object is
     * {@link String}
     *
     */
    public String getTaskNumber() {
        return taskNumber;
    }

    /**
     * Gets the value of the taskState property.
     *
     * @return
     * possible object is
     * {@link String}
     *
     */
    public String getTaskState() {
        return taskState;
    }

    /**
     * Sets the value of the approvalSource property.
     *
     * @param value
     * allowed object is
     * {@link String}
     *
     */
    public void setApprovalSource(String value) {
        this.approvalSource = value;
    }

    /**
     * Sets the value of the loggedUserId property.
     *
     * @param value
     * allowed object is
     * {@link String}
     *
     */
    public void setLoggedUserId(String value) {
        this.loggedUserId = value;
    }

    /**
     * Sets the value of the reAssignedGroupId property.
     *
     * @param value
     * allowed object is
     * {@link String}
     *
     */
    public void setReAssignedGroupId(String value) {
        this.reAssignedGroupId = value;
    }

    /**
     * Sets the value of the reAssignedUserId property.
     *
     * @param value
     * allowed object is
     * {@link String}
     *
     */
    public void setReAssignedUserId(String value) {
        this.reAssignedUserId = value;
    }

    /**
     * Sets the value of the taskNumber property.
     *
     * @param value
     * allowed object is
     * {@link String}
     *
     */
    public void setTaskNumber(String value) {
        this.taskNumber = value;
    }

    /**
     * Sets the value of the taskState property.
     *
     * @param value
     * allowed object is
     * {@link String}
     *
     */
    public void setTaskState(String value) {
        this.taskState = value;
    }

}
