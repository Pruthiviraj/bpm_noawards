
package com.klx.bpm.cct.tqs.utility.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssignedUsersForOpportunityRspT complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AssignedUsersForOpportunityRspT"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AssignedUsers" type="{http://www.klx.com/bpm/cct/tqs/utility/V1.0}AssignedUsersT" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AssignedUsersForOpportunityRspT", propOrder = { "assignedUsers" })
public class AssignedUsersForOpportunityRspT {

    @XmlElement(name = "AssignedUsers")
    protected AssignedUsersT assignedUsers;

    /**
     * Gets the value of the assignedUsers property.
     *
     * @return
     *     possible object is
     *     {@link AssignedUsersT }
     *
     */
    public AssignedUsersT getAssignedUsers() {
        return assignedUsers;
    }

    /**
     * Sets the value of the assignedUsers property.
     *
     * @param value
     *     allowed object is
     *     {@link AssignedUsersT }
     *
     */
    public void setAssignedUsers(AssignedUsersT value) {
        this.assignedUsers = value;
    }

}
