
package com.klx.bpm.cct.tqs.utility.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssignedUsersForOpportunityReqT complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AssignedUsersForOpportunityReqT"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OpptyNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Role" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Attribute1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Attribute2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Attribute3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AssignedUsersForOpportunityReqT",
         propOrder = { "opptyNumber", "role", "attribute1", "attribute2", "attribute3" })
public class AssignedUsersForOpportunityReqT {

    @XmlElement(name = "OpptyNumber", required = true)
    protected String opptyNumber;
    @XmlElement(name = "Role", required = true)
    protected String role;
    @XmlElement(name = "Attribute1")
    protected String attribute1;
    @XmlElement(name = "Attribute2")
    protected String attribute2;
    @XmlElement(name = "Attribute3")
    protected String attribute3;

    /**
     * Gets the value of the opptyNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOpptyNumber() {
        return opptyNumber;
    }

    /**
     * Sets the value of the opptyNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOpptyNumber(String value) {
        this.opptyNumber = value;
    }

    /**
     * Gets the value of the role property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getRole() {
        return role;
    }

    /**
     * Sets the value of the role property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setRole(String value) {
        this.role = value;
    }

    /**
     * Gets the value of the attribute1 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAttribute1() {
        return attribute1;
    }

    /**
     * Sets the value of the attribute1 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAttribute1(String value) {
        this.attribute1 = value;
    }

    /**
     * Gets the value of the attribute2 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAttribute2() {
        return attribute2;
    }

    /**
     * Sets the value of the attribute2 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAttribute2(String value) {
        this.attribute2 = value;
    }

    /**
     * Gets the value of the attribute3 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAttribute3() {
        return attribute3;
    }

    /**
     * Sets the value of the attribute3 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAttribute3(String value) {
        this.attribute3 = value;
    }

}
