
package com.klx.cct.osc.user.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.klx.cct.osc.user.v1 package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Result_QNAME = new QName("http://www.klx.com/cct/osc/user/V1.0", "Result");
    private final static QName _User_QNAME = new QName("http://www.klx.com/cct/osc/user/V1.0", "User");
    private final static QName _GetOpptyRequest_QNAME =
        new QName("http://www.klx.com/cct/osc/user/V1.0", "GetOpptyRequest");
    private final static QName _GetOpptyResponse_QNAME =
        new QName("http://www.klx.com/cct/osc/user/V1.0", "GetOpptyResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.klx.cct.osc.user.v1
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ResultT }
     *
     */
    public ResultT createResultT() {
        return new ResultT();
    }

    /**
     * Create an instance of {@link UserT }
     *
     */
    public UserT createUserT() {
        return new UserT();
    }

    /**
     * Create an instance of {@link GetOpptyRequestT }
     *
     */
    public GetOpptyRequestT createGetOpptyRequestT() {
        return new GetOpptyRequestT();
    }

    /**
     * Create an instance of {@link GetOpptyResponseT }
     *
     */
    public GetOpptyResponseT createGetOpptyResponseT() {
        return new GetOpptyResponseT();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResultT }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/cct/osc/user/V1.0", name = "Result")
    public JAXBElement<ResultT> createResult(ResultT value) {
        return new JAXBElement<ResultT>(_Result_QNAME, ResultT.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserT }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/cct/osc/user/V1.0", name = "User")
    public JAXBElement<UserT> createUser(UserT value) {
        return new JAXBElement<UserT>(_User_QNAME, UserT.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOpptyRequestT }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/cct/osc/user/V1.0", name = "GetOpptyRequest")
    public JAXBElement<GetOpptyRequestT> createGetOpptyRequest(GetOpptyRequestT value) {
        return new JAXBElement<GetOpptyRequestT>(_GetOpptyRequest_QNAME, GetOpptyRequestT.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOpptyResponseT }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/cct/osc/user/V1.0", name = "GetOpptyResponse")
    public JAXBElement<GetOpptyResponseT> createGetOpptyResponse(GetOpptyResponseT value) {
        return new JAXBElement<GetOpptyResponseT>(_GetOpptyResponse_QNAME, GetOpptyResponseT.class, null, value);
    }

}
