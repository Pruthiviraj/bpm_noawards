
package com.klx.cct.osc.user.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for GetOpptyRequestT complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="GetOpptyRequestT"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OpportunityId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Source" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RequestedDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="UniqueId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetOpptyRequestT", propOrder = { "opportunityId", "source", "requestedDateTime", "uniqueId" })
public class GetOpptyRequestT {

    @XmlElement(name = "OpportunityId", required = true)
    protected String opportunityId;
    @XmlElement(name = "Source", required = true)
    protected String source;
    @XmlElement(name = "RequestedDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar requestedDateTime;
    @XmlElement(name = "UniqueId")
    protected String uniqueId;

    /**
     * Gets the value of the opportunityId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOpportunityId() {
        return opportunityId;
    }

    /**
     * Sets the value of the opportunityId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOpportunityId(String value) {
        this.opportunityId = value;
    }

    /**
     * Gets the value of the source property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSource(String value) {
        this.source = value;
    }

    /**
     * Gets the value of the requestedDateTime property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getRequestedDateTime() {
        return requestedDateTime;
    }

    /**
     * Sets the value of the requestedDateTime property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setRequestedDateTime(XMLGregorianCalendar value) {
        this.requestedDateTime = value;
    }

    /**
     * Gets the value of the uniqueId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getUniqueId() {
        return uniqueId;
    }

    /**
     * Sets the value of the uniqueId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setUniqueId(String value) {
        this.uniqueId = value;
    }

}
