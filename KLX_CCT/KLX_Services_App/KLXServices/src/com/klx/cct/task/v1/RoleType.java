
package com.klx.cct.task.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="RoleType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RoleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsTaskCreationRequired" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoleType", propOrder = { "roleName", "isTaskCreationRequired" })
public class RoleType {

    @XmlElement(name = "RoleName", required = true)
    protected String roleName;
    @XmlElement(name = "IsTaskCreationRequired")
    protected boolean isTaskCreationRequired;

    /**
     * Gets the value of the roleName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * Sets the value of the roleName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setRoleName(String value) {
        this.roleName = value;
    }

    /**
     * Gets the value of the isTaskCreationRequired property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    public boolean isIsTaskCreationRequired() {
        /*return isTaskCreationRequired;*/

        return isTaskCreationRequired;
    }

    /**
     * Sets the value of the isTaskCreationRequired property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    public void setIsTaskCreationRequired(boolean value) {
        this.isTaskCreationRequired = value;
    }

}
