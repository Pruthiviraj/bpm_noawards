
package com.klx.cct.task.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.klx.cct.task.v1 package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Status_QNAME = new QName("http://www.klx.com/cct/task/V1.0", "Status");
    private final static QName _Task_QNAME = new QName("http://www.klx.com/cct/task/V1.0", "Task");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.klx.cct.task.v1
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link StatusT }
     *
     */
    public StatusT createStatusT() {
        return new StatusT();
    }

    /**
     * Create an instance of {@link TaskT }
     *
     */
    public TaskT createTaskT() {
        return new TaskT();
    }

    /**
     * Create an instance of {@link RoleDetailsType }
     *
     */
    public RoleDetailsType createRoleDetailsType() {
        return new RoleDetailsType();
    }

    /**
     * Create an instance of {@link RoleType }
     *
     */
    public RoleType createRoleType() {
        return new RoleType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusT }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/cct/task/V1.0", name = "Status")
    public JAXBElement<StatusT> createStatus(StatusT value) {
        return new JAXBElement<StatusT>(_Status_QNAME, StatusT.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaskT }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/cct/task/V1.0", name = "Task")
    public JAXBElement<TaskT> createTask(TaskT value) {
        return new JAXBElement<TaskT>(_Task_QNAME, TaskT.class, null, value);
    }

}
