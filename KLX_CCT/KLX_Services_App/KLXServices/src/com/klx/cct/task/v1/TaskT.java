
package com.klx.cct.task.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for TaskT complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="TaskT"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OpportunityNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OpportunityId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="QuoteId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="QuoteVersion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RoleDetails" type="{http://www.klx.com/cct/task/V1.0}RoleDetailsType"/&gt;
 *         &lt;element name="CreatedBy" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Attribute1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Attribute2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Attribute3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Attribute4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Attribute5" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="TaskType" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="NEW"/&gt;
 *               &lt;enumeration value="REVISION"/&gt;
 *               &lt;enumeration value="UPDATE"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="TaskTitle"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="ManageOpportunity"/&gt;
 *               &lt;enumeration value="ManagePricing"/&gt;
 *               &lt;enumeration value="ManageApproval"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaskT",
         propOrder =
         { "opportunityNumber", "opportunityId", "quoteId", "quoteVersion", "roleDetails", "createdBy", "taskDueDate",
           "comments", "attribute1", "attribute2", "attribute3", "attribute4", "attribute5", "snapshotId", "taskType",
           "taskTitle", "action"
    })
public class TaskT {

    @XmlElement(name = "OpportunityNumber", required = true)
    protected String opportunityNumber;
    @XmlElement(name = "OpportunityId", required = true)
    protected String opportunityId;
    @XmlElement(name = "QuoteId")
    protected String quoteId;
    @XmlElement(name = "QuoteVersion")
    protected String quoteVersion;
    @XmlElement(name = "RoleDetails")
    protected RoleDetailsType roleDetails;
    @XmlElement(name = "CreatedBy", required = true)
    protected String createdBy;
    @XmlElement(name = "Attribute1")
    protected String attribute1;
    @XmlElement(name = "Attribute2")
    protected String attribute2;
    @XmlElement(name = "Attribute3")
    protected String attribute3;
    @XmlElement(name = "Attribute4")
    protected String attribute4;
    @XmlElement(name = "Attribute5")
    protected Integer attribute5;
    @XmlElement(name = "TaskType", required = true)
    protected String taskType;
    @XmlElement(name = "TaskTitle", required = true)
    protected String taskTitle;
    @XmlElement(name = "Action", required = true)
    protected String action;
    @XmlElement(name = "Comments")
    protected String comments;
    @XmlElement(name = "SnapshotId")
    protected Integer snapshotId;
    @XmlElement(name = "TaskDueDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar taskDueDate;

    /**
     * Gets the value of the opportunityNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOpportunityNumber() {
        return opportunityNumber;
    }

    /**
     * Sets the value of the opportunityNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOpportunityNumber(String value) {
        this.opportunityNumber = value;
    }

    /**
     * Gets the value of the opportunityId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOpportunityId() {
        return opportunityId;
    }

    /**
     * Sets the value of the opportunityId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOpportunityId(String value) {
        this.opportunityId = value;
    }

    /**
     * Gets the value of the quoteId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getQuoteId() {
        return quoteId;
    }

    /**
     * Sets the value of the quoteId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setQuoteId(String value) {
        this.quoteId = value;
    }

    /**
     * Gets the value of the quoteVersion property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getQuoteVersion() {
        return quoteVersion;
    }

    /**
     * Sets the value of the quoteVersion property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setQuoteVersion(String value) {
        this.quoteVersion = value;
    }

    /**
     * Gets the value of the roleDetails property.
     *
     * @return
     *     possible object is
     *     {@link RoleDetailsType }
     *
     */
    public RoleDetailsType getRoleDetails() {
        return roleDetails;
    }

    /**
     * Sets the value of the roleDetails property.
     *
     * @param value
     *     allowed object is
     *     {@link RoleDetailsType }
     *
     */
    public void setRoleDetails(RoleDetailsType value) {
        this.roleDetails = value;
    }

    /**
     * Gets the value of the createdBy property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the value of the createdBy property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCreatedBy(String value) {
        this.createdBy = value;
    }

    /**
     * Gets the value of the attribute1 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAttribute1() {
        return attribute1;
    }

    /**
     * Sets the value of the attribute1 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAttribute1(String value) {
        this.attribute1 = value;
    }

    /**
     * Gets the value of the attribute2 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAttribute2() {
        return attribute2;
    }

    /**
     * Sets the value of the attribute2 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAttribute2(String value) {
        this.attribute2 = value;
    }

    /**
     * Gets the value of the attribute3 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAttribute3() {
        return attribute3;
    }

    /**
     * Sets the value of the attribute3 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAttribute3(String value) {
        this.attribute3 = value;
    }

    /**
     * Gets the value of the attribute4 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAttribute4() {
        return attribute4;
    }

    /**
     * Sets the value of the attribute4 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAttribute4(String value) {
        this.attribute4 = value;
    }

    /**
     * Gets the value of the attribute5 property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getAttribute5() {
        return attribute5;
    }

    /**
     * Sets the value of the attribute5 property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setAttribute5(Integer value) {
        this.attribute5 = value;
    }

    /**
     * Gets the value of the taskType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTaskType() {
        return taskType;
    }

    /**
     * Sets the value of the taskType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTaskType(String value) {
        this.taskType = value;
    }

    /**
     * Gets the value of the taskTitle property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTaskTitle() {
        return taskTitle;
    }

    /**
     * Sets the value of the taskTitle property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTaskTitle(String value) {
        this.taskTitle = value;
    }

    /**
     * Gets the value of the action property.
     *
     * @return
     * possible object is
     * {@link String}
     *
     */
    public String getAction() {
        return action;
    }

    /**
     * Gets the value of the comments property.
     *
     * @return
     * possible object is
     * {@link String}
     *
     */
    public String getComments() {
        return comments;
    }

    /**
     * Gets the value of the snapshotId property.
     *
     * @return
     * possible object is
     * {@link Integer}
     *
     */
    public Integer getSnapshotId() {
        return snapshotId;
    }

    /**
     * Gets the value of the taskDueDate property.
     *
     * @return
     * possible object is
     * {@link XMLGregorianCalendar}
     *
     */
    public XMLGregorianCalendar getTaskDueDate() {
        return taskDueDate;
    }

    /**
     * Sets the value of the action property.
     *
     * @param value
     * allowed object is
     * {@link String}
     *
     */
    public void setAction(String value) {
        this.action = value;
    }

    /**
     * Sets the value of the comments property.
     *
     * @param value
     * allowed object is
     * {@link String}
     *
     */
    public void setComments(String value) {
        this.comments = value;
    }

    /**
     * Sets the value of the snapshotId property.
     *
     * @param value
     * allowed object is
     * {@link Integer}
     *
     */
    public void setSnapshotId(Integer value) {
        this.snapshotId = value;
    }

    /**
     * Sets the value of the taskDueDate property.
     *
     * @param value
     * allowed object is
     * {@link XMLGregorianCalendar}
     *
     */
    public void setTaskDueDate(XMLGregorianCalendar value) {
        this.taskDueDate = value;
    }

}
