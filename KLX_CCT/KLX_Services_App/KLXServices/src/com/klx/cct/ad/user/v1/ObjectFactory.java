
package com.klx.cct.ad.user.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.klx.cct.ad.user.v1 package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _User_QNAME = new QName("http://www.klx.com/cct/ad/User/V1.0", "User");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.klx.cct.ad.user.v1
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Roles }
     *
     */
    public Roles createRoles() {
        return new Roles();
    }

    /**
     * Create an instance of {@link User }
     *
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link UsersList }
     *
     */
    public UsersList createUsersList() {
        return new UsersList();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/cct/ad/User/V1.0", name = "User")
    public JAXBElement<Object> createUser(Object value) {
        return new JAXBElement<Object>(_User_QNAME, Object.class, null, value);
    }

}
