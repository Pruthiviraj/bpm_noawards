
package com.klx.cct.ad.activedirectoryservice.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetUsersEmailsResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="GetUsersEmailsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="emails" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetUsersEmailsResponse", propOrder = { "emails" })
public class GetUsersEmailsResponse {

    @XmlElement(required = true)
    protected String emails;

    /**
     * Gets the value of the emails property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEmails() {
        return emails;
    }

    /**
     * Sets the value of the emails property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEmails(String value) {
        this.emails = value;
    }

}
