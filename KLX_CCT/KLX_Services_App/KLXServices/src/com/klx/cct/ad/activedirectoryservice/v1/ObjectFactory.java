
package com.klx.cct.ad.activedirectoryservice.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.klx.cct.ad.activedirectoryservice.v1 package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetUserDetailsRequest_QNAME =
        new QName("http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0", "GetUserDetailsRequest");
    private final static QName _GetUserDetailsResponse_QNAME =
        new QName("http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0", "GetUserDetailsResponse");
    private final static QName _GetUsersByRoleRequest_QNAME =
        new QName("http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0", "GetUsersByRoleRequest");
    private final static QName _GetUsersByRoleResponse_QNAME =
        new QName("http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0", "GetUsersByRoleResponse");
    private final static QName _GetMultipleUsersDetailsRequest_QNAME =
        new QName("http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0", "GetMultipleUsersDetailsRequest");
    private final static QName _GetMultipleUsersDetailsResponse_QNAME =
        new QName("http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0", "GetMultipleUsersDetailsResponse");
    private final static QName _GetUsersEmailsRequest_QNAME =
        new QName("http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0", "GetUsersEmailsRequest");
    private final static QName _GetUsersEmailsResponse_QNAME =
        new QName("http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0", "GetUsersEmailsResponse");
    private final static QName _GetUserDetailsByEmailRequest_QNAME =
        new QName("http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0", "GetUserDetailsByEmailRequest");
    private final static QName _GetUserDetailsByEmailResponse_QNAME =
        new QName("http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0", "GetUserDetailsByEmailResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.klx.cct.ad.activedirectoryservice.v1
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetUserDetailsRequest }
     *
     */
    public GetUserDetailsRequest createGetUserDetailsRequest() {
        return new GetUserDetailsRequest();
    }

    /**
     * Create an instance of {@link GetUserDetailsResponse }
     *
     */
    public GetUserDetailsResponse createGetUserDetailsResponse() {
        return new GetUserDetailsResponse();
    }

    /**
     * Create an instance of {@link GetUsersByRoleRequest }
     *
     */
    public GetUsersByRoleRequest createGetUsersByRoleRequest() {
        return new GetUsersByRoleRequest();
    }

    /**
     * Create an instance of {@link GetUsersByRoleResponse }
     *
     */
    public GetUsersByRoleResponse createGetUsersByRoleResponse() {
        return new GetUsersByRoleResponse();
    }

    /**
     * Create an instance of {@link GetMultipleUsersDetailsRequest }
     *
     */
    public GetMultipleUsersDetailsRequest createGetMultipleUsersDetailsRequest() {
        return new GetMultipleUsersDetailsRequest();
    }

    /**
     * Create an instance of {@link GetMultipleUsersDetailsResponse }
     *
     */
    public GetMultipleUsersDetailsResponse createGetMultipleUsersDetailsResponse() {
        return new GetMultipleUsersDetailsResponse();
    }

    /**
     * Create an instance of {@link GetUsersEmailsRequest }
     *
     */
    public GetUsersEmailsRequest createGetUsersEmailsRequest() {
        return new GetUsersEmailsRequest();
    }

    /**
     * Create an instance of {@link GetUsersEmailsResponse }
     *
     */
    public GetUsersEmailsResponse createGetUsersEmailsResponse() {
        return new GetUsersEmailsResponse();
    }

    /**
     * Create an instance of {@link GetUserDetailsByEmailRequest }
     *
     */
    public GetUserDetailsByEmailRequest createGetUserDetailsByEmailRequest() {
        return new GetUserDetailsByEmailRequest();
    }

    /**
     * Create an instance of {@link GetUserDetailsByEmailResponse }
     *
     */
    public GetUserDetailsByEmailResponse createGetUserDetailsByEmailResponse() {
        return new GetUserDetailsByEmailResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserDetailsRequest }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0", name = "GetUserDetailsRequest")
    public JAXBElement<GetUserDetailsRequest> createGetUserDetailsRequest(GetUserDetailsRequest value) {
        return new JAXBElement<GetUserDetailsRequest>(_GetUserDetailsRequest_QNAME, GetUserDetailsRequest.class, null,
                                                      value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserDetailsResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0",
                    name = "GetUserDetailsResponse")
    public JAXBElement<GetUserDetailsResponse> createGetUserDetailsResponse(GetUserDetailsResponse value) {
        return new JAXBElement<GetUserDetailsResponse>(_GetUserDetailsResponse_QNAME, GetUserDetailsResponse.class,
                                                       null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsersByRoleRequest }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0", name = "GetUsersByRoleRequest")
    public JAXBElement<GetUsersByRoleRequest> createGetUsersByRoleRequest(GetUsersByRoleRequest value) {
        return new JAXBElement<GetUsersByRoleRequest>(_GetUsersByRoleRequest_QNAME, GetUsersByRoleRequest.class, null,
                                                      value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsersByRoleResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0",
                    name = "GetUsersByRoleResponse")
    public JAXBElement<GetUsersByRoleResponse> createGetUsersByRoleResponse(GetUsersByRoleResponse value) {
        return new JAXBElement<GetUsersByRoleResponse>(_GetUsersByRoleResponse_QNAME, GetUsersByRoleResponse.class,
                                                       null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMultipleUsersDetailsRequest }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0",
                    name = "GetMultipleUsersDetailsRequest")
    public JAXBElement<GetMultipleUsersDetailsRequest> createGetMultipleUsersDetailsRequest(GetMultipleUsersDetailsRequest value) {
        return new JAXBElement<GetMultipleUsersDetailsRequest>(_GetMultipleUsersDetailsRequest_QNAME,
                                                               GetMultipleUsersDetailsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMultipleUsersDetailsResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0",
                    name = "GetMultipleUsersDetailsResponse")
    public JAXBElement<GetMultipleUsersDetailsResponse> createGetMultipleUsersDetailsResponse(GetMultipleUsersDetailsResponse value) {
        return new JAXBElement<GetMultipleUsersDetailsResponse>(_GetMultipleUsersDetailsResponse_QNAME,
                                                                GetMultipleUsersDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsersEmailsRequest }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0", name = "GetUsersEmailsRequest")
    public JAXBElement<GetUsersEmailsRequest> createGetUsersEmailsRequest(GetUsersEmailsRequest value) {
        return new JAXBElement<GetUsersEmailsRequest>(_GetUsersEmailsRequest_QNAME, GetUsersEmailsRequest.class, null,
                                                      value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsersEmailsResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0",
                    name = "GetUsersEmailsResponse")
    public JAXBElement<GetUsersEmailsResponse> createGetUsersEmailsResponse(GetUsersEmailsResponse value) {
        return new JAXBElement<GetUsersEmailsResponse>(_GetUsersEmailsResponse_QNAME, GetUsersEmailsResponse.class,
                                                       null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserDetailsByEmailRequest }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0",
                    name = "GetUserDetailsByEmailRequest")
    public JAXBElement<GetUserDetailsByEmailRequest> createGetUserDetailsByEmailRequest(GetUserDetailsByEmailRequest value) {
        return new JAXBElement<GetUserDetailsByEmailRequest>(_GetUserDetailsByEmailRequest_QNAME,
                                                             GetUserDetailsByEmailRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserDetailsByEmailResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0",
                    name = "GetUserDetailsByEmailResponse")
    public JAXBElement<GetUserDetailsByEmailResponse> createGetUserDetailsByEmailResponse(GetUserDetailsByEmailResponse value) {
        return new JAXBElement<GetUserDetailsByEmailResponse>(_GetUserDetailsByEmailResponse_QNAME,
                                                              GetUserDetailsByEmailResponse.class, null, value);
    }

}
