
package com.klx.cct.ad.activedirectoryservice.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.klx.cct.ad.user.v1.UsersList;


/**
 * <p>Java class for GetMultipleUsersDetailsResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="GetMultipleUsersDetailsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UsersList" type="{http://www.klx.com/cct/ad/User/V1.0}UsersList"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetMultipleUsersDetailsResponse", propOrder = { "usersList" })
public class GetMultipleUsersDetailsResponse {

    @XmlElement(name = "UsersList", required = true)
    protected UsersList usersList;

    /**
     * Gets the value of the usersList property.
     *
     * @return
     *     possible object is
     *     {@link UsersList }
     *
     */
    public UsersList getUsersList() {
        return usersList;
    }

    /**
     * Sets the value of the usersList property.
     *
     * @param value
     *     allowed object is
     *     {@link UsersList }
     *
     */
    public void setUsersList(UsersList value) {
        this.usersList = value;
    }

}
