
package com.klx.cct.ad.activedirectoryservice.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getUserDetailsByEmailRequest complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="getUserDetailsByEmailRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="emailId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getUserDetailsByEmailRequest", propOrder = { "emailId" })
public class GetUserDetailsByEmailRequest {

    @XmlElement(required = true)
    protected String emailId;

    /**
     * Gets the value of the emailId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * Sets the value of the emailId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEmailId(String value) {
        this.emailId = value;
    }

}
