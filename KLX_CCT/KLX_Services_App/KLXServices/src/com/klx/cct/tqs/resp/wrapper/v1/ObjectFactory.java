
package com.klx.cct.tqs.resp.wrapper.v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.klx.cct.tqs.resp.wrapper.v1 package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.klx.cct.tqs.resp.wrapper.v1
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TaskT }
     *
     */
    public TaskT createTaskT() {
        return new TaskT();
    }

    /**
     * Create an instance of {@link TaskList }
     *
     */
    public TaskList createTaskList() {
        return new TaskList();
    }

    /**
     * Create an instance of {@link TaskT.Payload }
     *
     */
    public TaskT.Payload createTaskTPayload() {
        return new TaskT.Payload();
    }

    /**
     * Create an instance of {@link TaskT.TaskAttributes }
     *
     */
    public TaskT.TaskAttributes createTaskTTaskAttributes() {
        return new TaskT.TaskAttributes();
    }

}
