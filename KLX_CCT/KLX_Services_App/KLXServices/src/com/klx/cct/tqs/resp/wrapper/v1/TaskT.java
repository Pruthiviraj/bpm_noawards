
package com.klx.cct.tqs.resp.wrapper.v1;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for taskT complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="taskT"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="payload"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="OpportunityNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="OpportunityId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="BatchId" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                   &lt;element name="SnapshotId" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                   &lt;element name="QuoteId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="QuoteVersion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CreatedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TaskDueDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="PMComments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ApproverComments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="Attribute1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="Attribute2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="Attribute3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="Attribute4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="Attribute5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RoleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DBRoleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="UserNames" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="UserFullNames" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="UserEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="PMAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="AssignmentCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="groupAssigned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="Role" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="QuoteNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="QuoteRevision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TaskTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="OpportunityType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="UIAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TaskClosureId" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="TaskAttributes"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ownerUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="priority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="identityContext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="processinstanceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="processId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="processName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="assignedDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="assigneeUsersId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="assigneeUsersDisplayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="assigneeUsersType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="createdDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="fromUserType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="taskId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="taskNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="subState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="acquiredBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="outcome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="updatedById" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="updatedByDisplayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="updatedByType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="updatedDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="activityName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="textAttribute1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="textAttribute2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="textAttribute3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="textAttribute4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="textAttribute5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="textAttribute6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="textAttribute7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="textAttribute8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="textAttribute9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="textAttribute10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="textAttribute11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="numberAttribute1" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                   &lt;element name="numberAttribute2" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                   &lt;element name="numberAttribute3" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                   &lt;element name="ownerUserDisplayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "taskT", propOrder = { "payload", "taskAttributes" })
public class TaskT {

    @XmlElement(required = true)
    protected TaskT.Payload payload;
    @XmlElement(name = "TaskAttributes", required = true)
    protected TaskT.TaskAttributes taskAttributes;

    /**
     * Gets the value of the payload property.
     *
     * @return
     *     possible object is
     *     {@link TaskT.Payload }
     *
     */
    public TaskT.Payload getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     *
     * @param value
     *     allowed object is
     *     {@link TaskT.Payload }
     *
     */
    public void setPayload(TaskT.Payload value) {
        this.payload = value;
    }

    /**
     * Gets the value of the taskAttributes property.
     *
     * @return
     *     possible object is
     *     {@link TaskT.TaskAttributes }
     *
     */
    public TaskT.TaskAttributes getTaskAttributes() {
        return taskAttributes;
    }

    /**
     * Sets the value of the taskAttributes property.
     *
     * @param value
     *     allowed object is
     *     {@link TaskT.TaskAttributes }
     *
     */
    public void setTaskAttributes(TaskT.TaskAttributes value) {
        this.taskAttributes = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="OpportunityNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="OpportunityId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="BatchId" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *         &lt;element name="SnapshotId" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *         &lt;element name="QuoteId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="QuoteVersion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CreatedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TaskDueDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="PMComments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ApproverComments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="Attribute1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="Attribute2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="Attribute3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="Attribute4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="Attribute5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RoleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DBRoleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="UserNames" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="UserFullNames" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="UserEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="PMAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="AssignmentCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="groupAssigned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="Role" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="QuoteNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="QuoteRevision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TaskTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="OpportunityType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="UIAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TaskClosureId" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "",
             propOrder =
             { "opportunityNumber", "opportunityId", "batchId", "snapshotId", "quoteId", "quoteVersion", "createdBy",
               "taskDueDate", "pmComments", "approverComments", "attribute1", "attribute2", "attribute3", "attribute4",
               "attribute5", "roleName", "dbRoleName", "userNames", "userFullNames", "userEmail", "pmAction",
               "assignmentCategory", "groupAssigned", "role", "quoteNo", "quoteRevision", "status", "taskTitle",
               "opportunityType", "uiAction", "taskClosureId"
        })
    public static class Payload {

        @XmlElement(name = "OpportunityNumber")
        protected String opportunityNumber;
        @XmlElement(name = "OpportunityId")
        protected String opportunityId;
        @XmlElement(name = "BatchId")
        protected BigDecimal batchId;
        @XmlElement(name = "SnapshotId")
        protected BigDecimal snapshotId;
        @XmlElement(name = "QuoteId")
        protected String quoteId;
        @XmlElement(name = "QuoteVersion")
        protected String quoteVersion;
        @XmlElement(name = "CreatedBy")
        protected String createdBy;
        @XmlElement(name = "TaskDueDate")
        protected String taskDueDate;
        @XmlElement(name = "PMComments")
        protected String pmComments;
        @XmlElement(name = "ApproverComments")
        protected String approverComments;
        @XmlElement(name = "Attribute1")
        protected String attribute1;
        @XmlElement(name = "Attribute2")
        protected String attribute2;
        @XmlElement(name = "Attribute3")
        protected String attribute3;
        @XmlElement(name = "Attribute4")
        protected String attribute4;
        @XmlElement(name = "Attribute5")
        protected String attribute5;
        @XmlElement(name = "RoleName")
        protected String roleName;
        @XmlElement(name = "DBRoleName")
        protected String dbRoleName;
        @XmlElement(name = "UserNames")
        protected String userNames;
        @XmlElement(name = "UserFullNames")
        protected String userFullNames;
        @XmlElement(name = "UserEmail")
        protected String userEmail;
        @XmlElement(name = "PMAction")
        protected String pmAction;
        @XmlElement(name = "AssignmentCategory")
        protected String assignmentCategory;
        protected String groupAssigned;
        @XmlElement(name = "Role")
        protected String role;
        @XmlElement(name = "QuoteNo")
        protected String quoteNo;
        @XmlElement(name = "QuoteRevision")
        protected String quoteRevision;
        @XmlElement(name = "Status")
        protected String status;
        @XmlElement(name = "TaskTitle")
        protected String taskTitle;
        @XmlElement(name = "OpportunityType")
        protected String opportunityType;
        @XmlElement(name = "UIAction")
        protected String uiAction;
        @XmlElement(name = "TaskClosureId")
        protected BigDecimal taskClosureId;

        /**
         * Gets the value of the opportunityNumber property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getOpportunityNumber() {
            return opportunityNumber;
        }

        /**
         * Sets the value of the opportunityNumber property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setOpportunityNumber(String value) {
            this.opportunityNumber = value;
        }

        /**
         * Gets the value of the opportunityId property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getOpportunityId() {
            return opportunityId;
        }

        /**
         * Sets the value of the opportunityId property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setOpportunityId(String value) {
            this.opportunityId = value;
        }

        /**
         * Gets the value of the batchId property.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getBatchId() {
            return batchId;
        }

        /**
         * Sets the value of the batchId property.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setBatchId(BigDecimal value) {
            this.batchId = value;
        }

        /**
         * Gets the value of the snapshotId property.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getSnapshotId() {
            return snapshotId;
        }

        /**
         * Sets the value of the snapshotId property.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setSnapshotId(BigDecimal value) {
            this.snapshotId = value;
        }

        /**
         * Gets the value of the quoteId property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getQuoteId() {
            return quoteId;
        }

        /**
         * Sets the value of the quoteId property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setQuoteId(String value) {
            this.quoteId = value;
        }

        /**
         * Gets the value of the quoteVersion property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getQuoteVersion() {
            return quoteVersion;
        }

        /**
         * Sets the value of the quoteVersion property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setQuoteVersion(String value) {
            this.quoteVersion = value;
        }

        /**
         * Gets the value of the createdBy property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCreatedBy() {
            return createdBy;
        }

        /**
         * Sets the value of the createdBy property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCreatedBy(String value) {
            this.createdBy = value;
        }

        /**
         * Gets the value of the taskDueDate property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTaskDueDate() {
            return taskDueDate;
        }

        /**
         * Sets the value of the taskDueDate property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTaskDueDate(String value) {
            this.taskDueDate = value;
        }

        /**
         * Gets the value of the pmComments property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getPMComments() {
            return pmComments;
        }

        /**
         * Sets the value of the pmComments property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setPMComments(String value) {
            this.pmComments = value;
        }

        /**
         * Gets the value of the approverComments property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getApproverComments() {
            return approverComments;
        }

        /**
         * Sets the value of the approverComments property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setApproverComments(String value) {
            this.approverComments = value;
        }

        /**
         * Gets the value of the attribute1 property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAttribute1() {
            return attribute1;
        }

        /**
         * Sets the value of the attribute1 property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAttribute1(String value) {
            this.attribute1 = value;
        }

        /**
         * Gets the value of the attribute2 property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAttribute2() {
            return attribute2;
        }

        /**
         * Sets the value of the attribute2 property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAttribute2(String value) {
            this.attribute2 = value;
        }

        /**
         * Gets the value of the attribute3 property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAttribute3() {
            return attribute3;
        }

        /**
         * Sets the value of the attribute3 property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAttribute3(String value) {
            this.attribute3 = value;
        }

        /**
         * Gets the value of the attribute4 property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAttribute4() {
            return attribute4;
        }

        /**
         * Sets the value of the attribute4 property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAttribute4(String value) {
            this.attribute4 = value;
        }

        /**
         * Gets the value of the attribute5 property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAttribute5() {
            return attribute5;
        }

        /**
         * Sets the value of the attribute5 property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAttribute5(String value) {
            this.attribute5 = value;
        }

        /**
         * Gets the value of the roleName property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getRoleName() {
            return roleName;
        }

        /**
         * Sets the value of the roleName property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setRoleName(String value) {
            this.roleName = value;
        }

        /**
         * Gets the value of the dbRoleName property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getDBRoleName() {
            return dbRoleName;
        }

        /**
         * Sets the value of the dbRoleName property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setDBRoleName(String value) {
            this.dbRoleName = value;
        }

        /**
         * Gets the value of the userNames property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getUserNames() {
            return userNames;
        }

        /**
         * Sets the value of the userNames property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setUserNames(String value) {
            this.userNames = value;
        }

        /**
         * Gets the value of the userFullNames property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getUserFullNames() {
            return userFullNames;
        }

        /**
         * Sets the value of the userFullNames property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setUserFullNames(String value) {
            this.userFullNames = value;
        }

        /**
         * Gets the value of the userEmail property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getUserEmail() {
            return userEmail;
        }

        /**
         * Sets the value of the userEmail property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setUserEmail(String value) {
            this.userEmail = value;
        }

        /**
         * Gets the value of the pmAction property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getPMAction() {
            return pmAction;
        }

        /**
         * Sets the value of the pmAction property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setPMAction(String value) {
            this.pmAction = value;
        }

        /**
         * Gets the value of the assignmentCategory property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAssignmentCategory() {
            return assignmentCategory;
        }

        /**
         * Sets the value of the assignmentCategory property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAssignmentCategory(String value) {
            this.assignmentCategory = value;
        }

        /**
         * Gets the value of the groupAssigned property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getGroupAssigned() {
            return groupAssigned;
        }

        /**
         * Sets the value of the groupAssigned property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setGroupAssigned(String value) {
            this.groupAssigned = value;
        }

        /**
         * Gets the value of the role property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getRole() {
            return role;
        }

        /**
         * Sets the value of the role property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setRole(String value) {
            this.role = value;
        }

        /**
         * Gets the value of the quoteNo property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getQuoteNo() {
            return quoteNo;
        }

        /**
         * Sets the value of the quoteNo property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setQuoteNo(String value) {
            this.quoteNo = value;
        }

        /**
         * Gets the value of the quoteRevision property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getQuoteRevision() {
            return quoteRevision;
        }

        /**
         * Sets the value of the quoteRevision property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setQuoteRevision(String value) {
            this.quoteRevision = value;
        }

        /**
         * Gets the value of the status property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getStatus() {
            return status;
        }

        /**
         * Sets the value of the status property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setStatus(String value) {
            this.status = value;
        }

        /**
         * Gets the value of the taskTitle property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTaskTitle() {
            return taskTitle;
        }

        /**
         * Sets the value of the taskTitle property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTaskTitle(String value) {
            this.taskTitle = value;
        }

        /**
         * Gets the value of the opportunityType property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getOpportunityType() {
            return opportunityType;
        }

        /**
         * Sets the value of the opportunityType property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setOpportunityType(String value) {
            this.opportunityType = value;
        }

        /**
         * Gets the value of the uiAction property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getUIAction() {
            return uiAction;
        }

        /**
         * Sets the value of the uiAction property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setUIAction(String value) {
            this.uiAction = value;
        }

        /**
         * Gets the value of the taskClosureId property.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getTaskClosureId() {
            return taskClosureId;
        }

        /**
         * Sets the value of the taskClosureId property.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setTaskClosureId(BigDecimal value) {
            this.taskClosureId = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ownerUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="priority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="identityContext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="processinstanceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="processId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="processName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="assignedDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="assigneeUsersId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="assigneeUsersDisplayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="assigneeUsersType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="createdDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="fromUserType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="taskId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="taskNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="subState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="acquiredBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="outcome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="updatedById" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="updatedByDisplayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="updatedByType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="updatedDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="activityName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="textAttribute1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="textAttribute2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="textAttribute3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="textAttribute4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="textAttribute5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="textAttribute6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="textAttribute7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="textAttribute8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="textAttribute9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="textAttribute10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="textAttribute11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="numberAttribute1" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *         &lt;element name="numberAttribute2" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *         &lt;element name="numberAttribute3" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *         &lt;element name="ownerUserDisplayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "",
             propOrder =
             { "title", "ownerUser", "priority", "identityContext", "processinstanceId", "processId", "processName",
               "assignedDate", "assigneeUsersId", "assigneeUsersDisplayName", "assigneeUsersType", "createdDate",
               "fromUserType", "taskId", "taskNumber", "state", "subState", "acquiredBy", "outcome", "updatedById",
               "updatedByDisplayName", "updatedByType", "updatedDate", "activityName", "textAttribute1",
               "textAttribute2", "textAttribute3", "textAttribute4", "textAttribute5", "textAttribute6",
               "textAttribute7", "textAttribute8", "textAttribute9", "textAttribute10", "textAttribute11",
               "numberAttribute1", "numberAttribute2", "numberAttribute3", "ownerUserDisplayName"
        })
    public static class TaskAttributes {

        protected String title;
        protected String ownerUser;
        protected String priority;
        protected String identityContext;
        protected String processinstanceId;
        protected String processId;
        protected String processName;
        protected String assignedDate;
        protected String assigneeUsersId;
        protected String assigneeUsersDisplayName;
        protected String assigneeUsersType;
        protected String createdDate;
        protected String fromUserType;
        protected String taskId;
        protected String taskNumber;
        protected String state;
        protected String subState;
        protected String acquiredBy;
        protected String outcome;
        protected String updatedById;
        protected String updatedByDisplayName;
        protected String updatedByType;
        protected String updatedDate;
        protected String activityName;
        protected String textAttribute1;
        protected String textAttribute2;
        protected String textAttribute3;
        protected String textAttribute4;
        protected String textAttribute5;
        protected String textAttribute6;
        protected String textAttribute7;
        protected String textAttribute8;
        protected String textAttribute9;
        protected String textAttribute10;
        protected String textAttribute11;
        protected BigDecimal numberAttribute1;
        protected BigDecimal numberAttribute2;
        protected BigDecimal numberAttribute3;
        protected String ownerUserDisplayName;

        /**
         * Gets the value of the title property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTitle() {
            return title;
        }

        /**
         * Sets the value of the title property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTitle(String value) {
            this.title = value;
        }

        /**
         * Gets the value of the ownerUser property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getOwnerUser() {
            return ownerUser;
        }

        /**
         * Sets the value of the ownerUser property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setOwnerUser(String value) {
            this.ownerUser = value;
        }

        /**
         * Gets the value of the priority property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getPriority() {
            return priority;
        }

        /**
         * Sets the value of the priority property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setPriority(String value) {
            this.priority = value;
        }

        /**
         * Gets the value of the identityContext property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getIdentityContext() {
            return identityContext;
        }

        /**
         * Sets the value of the identityContext property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setIdentityContext(String value) {
            this.identityContext = value;
        }

        /**
         * Gets the value of the processinstanceId property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getProcessinstanceId() {
            return processinstanceId;
        }

        /**
         * Sets the value of the processinstanceId property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setProcessinstanceId(String value) {
            this.processinstanceId = value;
        }

        /**
         * Gets the value of the processId property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getProcessId() {
            return processId;
        }

        /**
         * Sets the value of the processId property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setProcessId(String value) {
            this.processId = value;
        }

        /**
         * Gets the value of the processName property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getProcessName() {
            return processName;
        }

        /**
         * Sets the value of the processName property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setProcessName(String value) {
            this.processName = value;
        }

        /**
         * Gets the value of the assignedDate property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAssignedDate() {
            return assignedDate;
        }

        /**
         * Sets the value of the assignedDate property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAssignedDate(String value) {
            this.assignedDate = value;
        }

        /**
         * Gets the value of the assigneeUsersId property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAssigneeUsersId() {
            return assigneeUsersId;
        }

        /**
         * Sets the value of the assigneeUsersId property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAssigneeUsersId(String value) {
            this.assigneeUsersId = value;
        }

        /**
         * Gets the value of the assigneeUsersDisplayName property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAssigneeUsersDisplayName() {
            return assigneeUsersDisplayName;
        }

        /**
         * Sets the value of the assigneeUsersDisplayName property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAssigneeUsersDisplayName(String value) {
            this.assigneeUsersDisplayName = value;
        }

        /**
         * Gets the value of the assigneeUsersType property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAssigneeUsersType() {
            return assigneeUsersType;
        }

        /**
         * Sets the value of the assigneeUsersType property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAssigneeUsersType(String value) {
            this.assigneeUsersType = value;
        }

        /**
         * Gets the value of the createdDate property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCreatedDate() {
            return createdDate;
        }

        /**
         * Sets the value of the createdDate property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCreatedDate(String value) {
            this.createdDate = value;
        }

        /**
         * Gets the value of the fromUserType property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getFromUserType() {
            return fromUserType;
        }

        /**
         * Sets the value of the fromUserType property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setFromUserType(String value) {
            this.fromUserType = value;
        }

        /**
         * Gets the value of the taskId property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTaskId() {
            return taskId;
        }

        /**
         * Sets the value of the taskId property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTaskId(String value) {
            this.taskId = value;
        }

        /**
         * Gets the value of the taskNumber property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTaskNumber() {
            return taskNumber;
        }

        /**
         * Sets the value of the taskNumber property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTaskNumber(String value) {
            this.taskNumber = value;
        }

        /**
         * Gets the value of the state property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getState() {
            return state;
        }

        /**
         * Sets the value of the state property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setState(String value) {
            this.state = value;
        }

        /**
         * Gets the value of the subState property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getSubState() {
            return subState;
        }

        /**
         * Sets the value of the subState property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setSubState(String value) {
            this.subState = value;
        }

        /**
         * Gets the value of the acquiredBy property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAcquiredBy() {
            return acquiredBy;
        }

        /**
         * Sets the value of the acquiredBy property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAcquiredBy(String value) {
            this.acquiredBy = value;
        }

        /**
         * Gets the value of the outcome property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getOutcome() {
            return outcome;
        }

        /**
         * Sets the value of the outcome property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setOutcome(String value) {
            this.outcome = value;
        }

        /**
         * Gets the value of the updatedById property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getUpdatedById() {
            return updatedById;
        }

        /**
         * Sets the value of the updatedById property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setUpdatedById(String value) {
            this.updatedById = value;
        }

        /**
         * Gets the value of the updatedByDisplayName property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getUpdatedByDisplayName() {
            return updatedByDisplayName;
        }

        /**
         * Sets the value of the updatedByDisplayName property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setUpdatedByDisplayName(String value) {
            this.updatedByDisplayName = value;
        }

        /**
         * Gets the value of the updatedByType property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getUpdatedByType() {
            return updatedByType;
        }

        /**
         * Sets the value of the updatedByType property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setUpdatedByType(String value) {
            this.updatedByType = value;
        }

        /**
         * Gets the value of the updatedDate property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getUpdatedDate() {
            return updatedDate;
        }

        /**
         * Sets the value of the updatedDate property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setUpdatedDate(String value) {
            this.updatedDate = value;
        }

        /**
         * Gets the value of the activityName property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getActivityName() {
            return activityName;
        }

        /**
         * Sets the value of the activityName property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setActivityName(String value) {
            this.activityName = value;
        }

        /**
         * Gets the value of the textAttribute1 property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTextAttribute1() {
            return textAttribute1;
        }

        /**
         * Sets the value of the textAttribute1 property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTextAttribute1(String value) {
            this.textAttribute1 = value;
        }

        /**
         * Gets the value of the textAttribute2 property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTextAttribute2() {
            return textAttribute2;
        }

        /**
         * Sets the value of the textAttribute2 property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTextAttribute2(String value) {
            this.textAttribute2 = value;
        }

        /**
         * Gets the value of the textAttribute3 property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTextAttribute3() {
            return textAttribute3;
        }

        /**
         * Sets the value of the textAttribute3 property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTextAttribute3(String value) {
            this.textAttribute3 = value;
        }

        /**
         * Gets the value of the textAttribute4 property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTextAttribute4() {
            return textAttribute4;
        }

        /**
         * Sets the value of the textAttribute4 property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTextAttribute4(String value) {
            this.textAttribute4 = value;
        }

        /**
         * Gets the value of the textAttribute5 property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTextAttribute5() {
            return textAttribute5;
        }

        /**
         * Sets the value of the textAttribute5 property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTextAttribute5(String value) {
            this.textAttribute5 = value;
        }

        /**
         * Gets the value of the textAttribute6 property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTextAttribute6() {
            return textAttribute6;
        }

        /**
         * Sets the value of the textAttribute6 property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTextAttribute6(String value) {
            this.textAttribute6 = value;
        }

        /**
         * Gets the value of the textAttribute7 property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTextAttribute7() {
            return textAttribute7;
        }

        /**
         * Sets the value of the textAttribute7 property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTextAttribute7(String value) {
            this.textAttribute7 = value;
        }

        /**
         * Gets the value of the textAttribute8 property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTextAttribute8() {
            return textAttribute8;
        }

        /**
         * Sets the value of the textAttribute8 property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTextAttribute8(String value) {
            this.textAttribute8 = value;
        }

        /**
         * Gets the value of the textAttribute9 property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTextAttribute9() {
            return textAttribute9;
        }

        /**
         * Sets the value of the textAttribute9 property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTextAttribute9(String value) {
            this.textAttribute9 = value;
        }

        /**
         * Gets the value of the textAttribute10 property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTextAttribute10() {
            return textAttribute10;
        }

        /**
         * Sets the value of the textAttribute10 property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTextAttribute10(String value) {
            this.textAttribute10 = value;
        }

        /**
         * Gets the value of the textAttribute11 property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTextAttribute11() {
            return textAttribute11;
        }

        /**
         * Sets the value of the textAttribute11 property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTextAttribute11(String value) {
            this.textAttribute11 = value;
        }

        /**
         * Gets the value of the numberAttribute1 property.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getNumberAttribute1() {
            return numberAttribute1;
        }

        /**
         * Sets the value of the numberAttribute1 property.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setNumberAttribute1(BigDecimal value) {
            this.numberAttribute1 = value;
        }

        /**
         * Gets the value of the numberAttribute2 property.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getNumberAttribute2() {
            return numberAttribute2;
        }

        /**
         * Sets the value of the numberAttribute2 property.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setNumberAttribute2(BigDecimal value) {
            this.numberAttribute2 = value;
        }

        /**
         * Gets the value of the numberAttribute3 property.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getNumberAttribute3() {
            return numberAttribute3;
        }

        /**
         * Sets the value of the numberAttribute3 property.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setNumberAttribute3(BigDecimal value) {
            this.numberAttribute3 = value;
        }

        /**
         * Gets the value of the ownerUserDisplayName property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getOwnerUserDisplayName() {
            return ownerUserDisplayName;
        }

        /**
         * Sets the value of the ownerUserDisplayName property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setOwnerUserDisplayName(String value) {
            this.ownerUserDisplayName = value;
        }

    }

}
