ALTER TABLE CCT_OPPORTUNITY_SNAPSHOT ADD DATE_SUBMITTED_TO_SALES DATE
/
alter table cct_opportunity_snapshot add CUSTOMER_REBATE number
/
alter table cct_opportunity_snapshot add CUSTOMER_NO_CRF VARCHAR2(50 BYTE)
/
alter table cct_opportunity_snapshot add STOCKING_STRATEGY_CRF NUMBER
/
alter table cct_opportunity_snapshot add SERVICE_FEES_CRF CHAR(1 BYTE)
/
alter table cct_opportunity_snapshot add LINE_MIN_CRF NUMBER
/
alter table cct_opportunity_snapshot add CONTRACT_TYPE VARCHAR2(255 BYTE)
/
alter table cct_opportunity_snapshot add PROJECT_TYPE VARCHAR2(255 BYTE)
/

ALTER table CCT_OPPORTUNITY_SNAPSHOT modify SERVICE_FEES_CRF VARCHAR2(250)
/