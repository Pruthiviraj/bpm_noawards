alter table cct_quote_parts add suggested_TOTAL_QTY_LEN_CON number
/
alter table cct_quote_parts add PRICE_CONFIDENCE varchar2(50)
/
alter table cct_quote_parts add tech_support_source varchar2(50)
/
alter table cct_quote_parts add tech_support_alternate varchar2(50)
/
alter table cct_quote_parts add tech_support_comments varchar2(50)
/
alter table cct_quote_parts add PACKAGE_QTY number
/
alter table cct_quote_parts add pct_discount number
/
alter table cct_quote_parts add tot_sls_val_usd12mths_crdx_uni varchar2(50)
/
alter table cct_quote_parts add storage_group varchar2(50)
/
alter table cct_quote_parts add hazmat_jn varchar2(50)
/
alter table cct_quote_parts add shelf_life_months_general varchar2(50)
/
alter table cct_quote_parts add stock_location varchar2(50)
/
alter table cct_quote_parts add inco_term varchar2(50)
/
alter table cct_quote_parts add moq_cardex_unit varchar2(50)
/
alter table cct_quote_parts add selling_increment number
/
alter table cct_quote_parts add alternate_source varchar2(50)
/
alter table cct_quote_parts add vendor_location_country varchar2(50)
/
alter table cct_quote_parts add itl_internal_pn varchar2(50)
/
alter table cct_quote_parts add extended_annual_cost number
/
alter table cct_quote_parts add crdx_unit_size_info_iden_to_s4 varchar2(50)
/
alter table cct_quote_parts add comments_um_conversion varchar2(50)
/
alter table cct_quote_parts add cstmr_um_conv_fctr_cardex_unit varchar2(50)
/
alter table cct_quote_parts add customer_uom varchar2(50)
/
alter table cct_quote_parts add customer_container_size_data varchar2(50)
/
alter table cct_quote_parts add negotiated number
/
alter table cct_quote_parts add spreadsheet number
/
alter table cct_quote_parts add quote_number_from_supplier varchar2(50)
/
alter table cct_quote_parts add vendor_comment varchar2(50)
/
alter table cct_quote_parts add dfar_fee varchar2(50)
/
alter table cct_quote_parts add misc_fee varchar2(50)
/
alter table cct_quote_parts add tool_fee number
/
alter table cct_quote_parts add fair_fee number
/
alter table cct_quote_parts add test_reports varchar2(50)
/
alter table cct_quote_parts add manufacturer_certs varchar2(50)
/
alter table cct_quote_parts add global_quantities varchar2(50)
/
alter table cct_quote_parts add amortiz_infl_pct number
/
alter table cct_quote_parts add moq number
/
alter table cct_quote_parts add qty_break2 number
/
alter table cct_quote_parts add qty_break1 number
/
alter table cct_quote_parts add quoted_qty number
/
alter table cct_quote_parts add comment_category varchar2(50)
/
alter table cct_quote_parts add status varchar2(50)
/
alter table cct_quote_parts add sales_restriction varchar2(1000)
/
alter table cct_quote_parts add lta_full_qty_used number
/
alter table cct_quote_parts add lta_cost_cust number
/
alter table cct_quote_parts add external_notes varchar2(50)
/
alter table cct_quote_parts add internal_notes varchar2(50)
/
alter table cct_quote_parts add customer_final_leadtime varchar2(50)
/
alter table cct_quote_parts modify
(
    customer_pn  varchar2(500 BYTE),
    quoted_part_number  varchar2(500 BYTE)
)
/

alter TABLE CCT_QUOTE_PARTS MODIFY MISC_CUST_DATA VARCHAR2(2000)
/
alter TABLE CCT_QUOTE_PARTS MODIFY PART_DESC VARCHAR2(2000)
/
alter TABLE CCT_QUOTE_PARTS MODIFY INTERNAL_NOTES VARCHAR2(2000)
/
alter TABLE CCT_QUOTE_PARTS MODIFY EXTERNAL_NOTES VARCHAR2(2000)
/



alter TABLE CCT_QUOTE_PARTS MODIFY TECH_SUPPORT_COMMENTS VARCHAR2(2000)
/
alter TABLE CCT_QUOTE_PARTS MODIFY COMMENTS_UM_CONVERSION VARCHAR2(2000)
/
alter TABLE CCT_QUOTE_PARTS MODIFY VENDOR_COMMENT VARCHAR2(2000)
/
alter TABLE CCT_QUOTE_PARTS MODIFY COMMENT_CATEGORY VARCHAR2(2000)
/
