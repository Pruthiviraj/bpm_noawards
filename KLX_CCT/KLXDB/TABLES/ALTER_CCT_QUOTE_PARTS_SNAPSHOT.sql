alter table cct_quote_parts_snapshot add suggested_TOTAL_QTY_LEN_CON number
/
alter table cct_quote_parts_snapshot add PRICE_CONFIDENCE varchar2(50)
/
alter table cct_quote_parts_snapshot add tech_support_source varchar2(50)
/
alter table cct_quote_parts_snapshot add tech_support_alternate varchar2(50)
/
alter table cct_quote_parts_snapshot add tech_support_comments varchar2(50)
/
alter table cct_quote_parts_snapshot add PACKAGE_QTY number
/
alter table cct_quote_parts_snapshot add pct_discount number
/
alter table cct_quote_parts_snapshot add tot_sls_val_usd12mths_crdx_uni varchar2(50)
/
alter table cct_quote_parts_snapshot add storage_group varchar2(50)
/
alter table cct_quote_parts_snapshot add hazmat_jn varchar2(50)
/
alter table cct_quote_parts_snapshot add shelf_life_months_general varchar2(50)
/
alter table cct_quote_parts_snapshot add stock_location varchar2(50)
/
alter table cct_quote_parts_snapshot add inco_term varchar2(50)
/
alter table cct_quote_parts_snapshot add moq_cardex_unit varchar2(50)
/
alter table cct_quote_parts_snapshot add selling_increment number
/
alter table cct_quote_parts_snapshot add alternate_source varchar2(50)
/
alter table cct_quote_parts_snapshot add vendor_location_country varchar2(50)
/
alter table cct_quote_parts_snapshot add itl_internal_pn varchar2(50)
/
alter table cct_quote_parts_snapshot add extended_annual_cost number
/
alter table cct_quote_parts_snapshot add crdx_unit_size_info_iden_to_s4 varchar2(50)
/
alter table cct_quote_parts_snapshot add comments_um_conversion varchar2(50)
/
alter table cct_quote_parts_snapshot add cstmr_um_conv_fctr_cardex_unit varchar2(50)
/
alter table cct_quote_parts_snapshot add customer_uom varchar2(50)
/
alter table cct_quote_parts_snapshot add customer_container_size_data varchar2(50)
/
alter table cct_quote_parts_snapshot add negotiated number
/
alter table cct_quote_parts_snapshot add spreadsheet number
/
alter table cct_quote_parts_snapshot add quote_number_from_supplier varchar2(50)
/
alter table cct_quote_parts_snapshot add vendor_comment varchar2(50)
/
alter table cct_quote_parts_snapshot add dfar_fee varchar2(50)
/
alter table cct_quote_parts_snapshot add misc_fee varchar2(50)
/
alter table cct_quote_parts_snapshot add tool_fee number
/
alter table cct_quote_parts_snapshot add fair_fee number
/
alter table cct_quote_parts_snapshot add test_reports varchar2(50)
/
alter table cct_quote_parts_snapshot add manufacturer_certs varchar2(50)
/
alter table cct_quote_parts_snapshot add global_quantities varchar2(50)
/
alter table cct_quote_parts_snapshot add amortiz_infl_pct number
/
alter table cct_quote_parts_snapshot add moq number
/
alter table cct_quote_parts_snapshot add qty_break2 number
/
alter table cct_quote_parts_snapshot add qty_break1 number
/
alter table cct_quote_parts_snapshot add quoted_qty number
/
alter table cct_quote_parts_snapshot add comment_category varchar2(50)
/
alter table cct_quote_parts_snapshot add status varchar2(50)
/
alter table cct_quote_parts_snapshot add sales_restriction varchar2(1000)
/
alter table cct_quote_parts_snapshot add lta_full_qty_used number
/
alter table cct_quote_parts_snapshot add lta_cost_cust number
/
alter table cct_quote_parts_snapshot add external_notes varchar2(50)
/
alter table cct_quote_parts_snapshot add internal_notes varchar2(50)
/
alter table cct_quote_parts_snapshot add customer_final_leadtime varchar2(50)
/
alter table cct_quote_parts_snapshot modify
(
    customer_pn  varchar2(500 BYTE),
    quoted_part_number  varchar2(500 BYTE)
)
/
alter TABLE CCT_QUOTE_PARTS_SNAPSHOT MODIFY MISC_CUST_DATA VARCHAR2(2000)
/
alter TABLE CCT_QUOTE_PARTS_SNAPSHOT MODIFY PART_DESC VARCHAR2(2000)
/
alter TABLE CCT_QUOTE_PARTS_SNAPSHOT MODIFY INTERNAL_NOTES VARCHAR2(2000)
/
alter TABLE CCT_QUOTE_PARTS_SNAPSHOT MODIFY EXTERNAL_NOTES VARCHAR2(2000)
/


alter TABLE CCT_QUOTE_PARTS_SNAPSHOT MODIFY TECH_SUPPORT_COMMENTS VARCHAR2(2000)
/
alter TABLE CCT_QUOTE_PARTS_SNAPSHOT MODIFY COMMENTS_UM_CONVERSION VARCHAR2(2000)
/
alter TABLE CCT_QUOTE_PARTS_SNAPSHOT MODIFY VENDOR_COMMENT VARCHAR2(2000)
/
alter TABLE CCT_QUOTE_PARTS_SNAPSHOT MODIFY COMMENT_CATEGORY VARCHAR2(2000)
/
