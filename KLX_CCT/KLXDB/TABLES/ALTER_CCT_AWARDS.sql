alter table cct_awards modify EAU number
/
alter table cct_awards add quote_line_number number
/
alter table cct_awards add quote_Customer_PN varchar2(50)
/
alter table cct_awards add quote_Certified_PN varchar2(50)
/
alter table cct_awards add quote_Annual_Qty number
/
alter table cct_awards add quote_Price number
/
alter table cct_awards add quote_UOM varchar2(50)
/
alter table cct_awards add quote_Item_Classification varchar2(50)
/
alter table cct_awards add quote_Leadtime varchar2(50)
/
alter table cct_awards add quote_Comment varchar2(50)
/
alter table cct_awards add quote_KLX_Quote_number varchar2(50)
/
alter table cct_awards add quote_Prime varchar2(50)
/
alter table cct_awards add quote_Cost number
/
alter table cct_awards add quote_Cost_Source varchar2(50)
/
alter table cct_awards add quote_Manufacturer varchar2(50)
/
alter table cct_awards add quote_Work_Queue varchar2(50)
/
alter table cct_awards add quote_Part_Description varchar2(50)
/
alter table cct_awards modify awarded_part_number varchar2(500 BYTE)
/
alter TABLE cct_awards MODIFY QUOTE_PART_DESCRIPTION VARCHAR2(2000)
/
alter TABLE CCT_AWARDS MODIFY QUOTE_COMMENT VARCHAR2(2000)
/