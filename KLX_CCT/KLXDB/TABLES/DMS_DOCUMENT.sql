CREATE TABLE DMS_DOCUMENT (
DOCUMENT_ID NUMBER NOT NULL,
OPPORTUNITY_ID NUMBER NOT NULL,
FOLDER_ID NUMBER NOT NULL,
FILE_TITLE VARCHAR2(240 BYTE) NOT NULL,
FILE_NAME VARCHAR2(240 BYTE) NOT NULL,
FILE_DESCRIPTION VARCHAR2(500 BYTE),
FILE_CONTENT BLOB NOT NULL, 
FILE_CONTENT_TYPE	VARCHAR2(200 BYTE) NOT NULL,
FILE_TYPE	VARCHAR2(50 BYTE) NOT NULL,
SOURCE_SYSTEM	VARCHAR2(10 BYTE) NOT NULL,
IS_DELETED		CHAR(1) DEFAULT 'N',
FLEX_ATTR_1		VARCHAR2(100 BYTE),
FLEX_ATTR_2		VARCHAR2(100 BYTE),
FLEX_ATTR_3		VARCHAR2(100 BYTE),
FLEX_ATTR_4		VARCHAR2(100 BYTE),
FLEX_ATTR_5		VARCHAR2(100 BYTE),
CREATED_BY			VARCHAR2(50 BYTE),
CREATED_DATE		DATE,
UPDATED_BY			VARCHAR2(50 BYTE),
UPDATED_DATE		DATE,
CONSTRAINT PK_DOCUMENT_ID PRIMARY KEY (DOCUMENT_ID),
CONSTRAINT FK_FOLDER_ID FOREIGN KEY
(
  FOLDER_ID 
)
REFERENCES DMS_FOLDER
(
  FOLDER_ID 
),
CONSTRAINT FK_OPPORTUNITY_ID FOREIGN KEY
(
  OPPORTUNITY_ID 
)
REFERENCES DMS_OPPORTUNITY
(
  OPPORTUNITY_ID 
)
);