CREATE TABLE "XXCCT"."CCT_CRF_CUSTOMER_MASTER_STG" 
(	"CUST_ID" NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOT NULL ENABLE, 
	"CUST_CARDEX_ID" VARCHAR2(50 BYTE), 
	"CUSTOMER_NO" VARCHAR2(50 BYTE), 
	"CUSTOMER_NAME" VARCHAR2(255 BYTE), 
	"ACCOUNT_TYPE" VARCHAR2(500 BYTE), 
	"SALES_REP_ID" VARCHAR2(50 BYTE), 
	"SALES_OWNER" VARCHAR2(255 BYTE), 
	"SALES_TEAM" VARCHAR2(50 BYTE), 
	"SALES_TEAM_NUMBER" NUMBER, 
	"SALES_MANAGER" VARCHAR2(255 BYTE), 
	"SALES_DIRECTOR" VARCHAR2(255 BYTE), 
	"SALES_VP" VARCHAR2(255 BYTE), 
	"SALES_OUTSIDE" VARCHAR2(255 BYTE), 
	"IS_GT25" CHAR(1 BYTE), 
	"IS_ECOMM" CHAR(1 BYTE), 
	"CURRENCY_CODE" VARCHAR2(10 BYTE), 
	"PAYMENT_TERMS" VARCHAR2(50 BYTE), 
	"INCO_TERMS" VARCHAR2(50 BYTE), 
	"ULTIMTATE_DESTINATION" VARCHAR2(50 BYTE), 
	"EXISTING_CONTRACT" VARCHAR2(50 BYTE), 
	"EXISTING_CONTRACT_NAME" VARCHAR2(255 BYTE), 
	"EXISTING_CONTRACT_TYPE" VARCHAR2(50 BYTE), 
	"IS_DFAR" CHAR(1 BYTE), 
	"IS_ASL_APPLICABLE" CHAR(1 BYTE), 
	"LINE_MIN" NUMBER, 
	"ORDER_MIN" NUMBER, 
	"WAREHOUSE_INFO" VARCHAR2(50 BYTE), 
	 CONSTRAINT "PK_CRF_CUSTOMER_MASTER_STG" PRIMARY KEY ("CUST_ID")
);