CREATE TABLE CCT_AWARD_HEADERS 
(
  AWARD_HEADER_ID NUMBER NOT NULL 
, OPPORTUNITY_ID NUMBER NOT NULL 
, AWARD_FILENAME VARCHAR2(200 BYTE) 
, SOURCE_OPPORTUNITY_ID NUMBER 
, CONSTRAINT CCT_AWARDFILE_OPPTY_MAPPING_PK PRIMARY KEY 
  (
    AWARD_HEADER_ID 
  , OPPORTUNITY_ID 
  )
  ENABLE 
) 
/