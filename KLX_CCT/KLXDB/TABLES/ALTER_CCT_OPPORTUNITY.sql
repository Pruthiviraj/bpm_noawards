ALTER TABLE CCT_OPPORTUNITY ADD DATE_SUBMITTED_TO_SALES DATE
/
alter table cct_opportunity add CUSTOMER_REBATE number
/
alter table cct_opportunity add CUSTOMER_NO_CRF VARCHAR2(50 BYTE)
/
alter table cct_opportunity add STOCKING_STRATEGY_CRF NUMBER
/
alter table cct_opportunity add SERVICE_FEES_CRF CHAR(1 BYTE)
/
alter table cct_opportunity add LINE_MIN_CRF NUMBER
/
alter table cct_opportunity add CONTRACT_TYPE VARCHAR2(255 BYTE)
/
alter table cct_opportunity add PROJECT_TYPE VARCHAR2(255 BYTE)
/

ALTER table CCT_OPPORTUNITY modify SERVICE_FEES_CRF VARCHAR2(250)
/


