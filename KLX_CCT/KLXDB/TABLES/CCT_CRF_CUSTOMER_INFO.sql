CREATE TABLE "XXCCT"."CCT_CRF_CUSTOMER_INFO" 
(	"CCT_CRF_CUSTOMER_INFO_ID" NUMBER NOT NULL ENABLE, 
	"CRF_ID" NUMBER NOT NULL ENABLE, 
	"CUST_REVISION_NO" NUMBER NOT NULL ENABLE, 
	"CUSTOMER_NO" VARCHAR2(50 BYTE), 
	"CUSTOMER_NAME" VARCHAR2(255 BYTE), 
	"ACCOUNT_TYPE" VARCHAR2(50 BYTE), 
	"IS_GT25" CHAR(1 BYTE), 
	"IS_ECOMM" CHAR(1 BYTE), 
	"CURRENCY_CODE" VARCHAR2(10 BYTE), 
	"PAYMENT_TERMS" VARCHAR2(50 BYTE), 
	"INCO_TERMS" VARCHAR2(50 BYTE), 
	"ULTIMTATE_DESTINATION" VARCHAR2(50 BYTE), 
	"IS_DFAR" CHAR(1 BYTE), 
	"IS_ASL_APPLICABLE" CHAR(1 BYTE), 
	"LINE_MIN" NUMBER, 
	"ORDER_MIN" NUMBER, 
	"WAREHOUSE_INFO" VARCHAR2(50 BYTE), 
	"IS_ASL_APPLIES" CHAR(1 BYTE), 
	"ASL_DETAILS" VARCHAR2(255 BYTE), 
	"EXISTING_CONTRACT_NUMBER" NUMBER, 
	"EXISTING_CONTRACT_TYPE" VARCHAR2(255 BYTE), 
	"CREATED_BY" VARCHAR2(255 BYTE), 
	"CREATION_DATE" DATE, 
	"UPDATED_BY" VARCHAR2(45 BYTE), 
	"UPDATE_DATE" DATE, 
	"SALES_TEAM" VARCHAR2(50 BYTE), 
	"SALES_OUTSIDE" VARCHAR2(255 BYTE), 
	"SALES_OWNER" VARCHAR2(255 BYTE), 
	"SALES_TEAM_NUMBER" NUMBER, 
	"EXISTING_CONTRACT" VARCHAR2(500 BYTE), 
	 CONSTRAINT "CCT_CRF_CUSTOMER_INFO_PK" PRIMARY KEY ("CCT_CRF_CUSTOMER_INFO_ID"), 
	 CONSTRAINT "CCT_CRF_CUSTOMER_INFO_FK1" FOREIGN KEY ("CRF_ID")
	  REFERENCES "XXCCT"."CCT_CRF" ("CRF_ID") ENABLE
);