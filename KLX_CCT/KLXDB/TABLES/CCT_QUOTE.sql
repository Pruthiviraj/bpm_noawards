CREATE TABLE CCT_QUOTE 
(
  QUOTE_ID NUMBER NOT NULL 
, OPPORTUNITY_ID NUMBER 
, QUOTE_NUMBER VARCHAR2(50 BYTE) 
, QUOTE_STATUS VARCHAR2(50 BYTE) 
, CREATION_DATE DATE 
, CREATED_BY VARCHAR2(50 BYTE) 
, LAST_UPDATED_DATE DATE 
, LAST_UPDATED_BY VARCHAR2(50 BYTE) 
, CONSTRAINT PK_Q PRIMARY KEY 
  (
    QUOTE_ID 
  )
  ENABLE 
) 
/

ALTER TABLE CCT_QUOTE
ADD CONSTRAINT FK_Q FOREIGN KEY
(
  OPPORTUNITY_ID 
)
REFERENCES CCT_OPPORTUNITY
(
  OPPORTUNITY_ID 
)
ENABLE
/
