ALTER TABLE CCT_APPROVAL_RULES ADD WHERE_CLAUSE	VARCHAR2(200 BYTE)
/

UPDATE CCT_APPROVAL_RULES SET WHERE_CLAUSE = 'opportunity_id=:id'
/
COMMIT
/
UPDATE CCT_APPROVAL_RULES SET WHERE_CLAUSE = 'snapshot_id=:snap',  
TABLE_NAME='CCT_QUOTE_FORMULA_SS_FINAL_V' WHERE FIELD_NAME IN ('AMOUNT','PROD_GRS_MARGIN')
/

COMMIT
/
UPDATE CCT_APPROVAL_RULES SET FIELD_NAME='TOTAL_PRODUCT_RESALE_LOC' WHERE FIELD_NAME ='AMOUNT'
/
COMMIT
/
UPDATE CCT_APPROVAL_RULES SET FIELD_NAME='PRODUCT_GROSS_MARGIN' WHERE FIELD_NAME ='PROD_GRS_MARGIN'
/
COMMIT
/
ALTER TABLE CCT_APPROVAL_RULES MODIFY FIELD_NAME VARCHAR2(500 BYTE)
/
COMMIT
/
UPDATE CCT_APPROVAL_RULES SET FIELD_NAME='SUBSTR(PAYMENT_TERMS,INSTR(PAYMENT_TERMS,''NET '',1)+4,2)',WHERE_CLAUSE='opportunity_id=:id AND payment_terms like ''NET %''' WHERE FIELD_NAME ='PAYMENT_TERMS'
/
COMMIT
/