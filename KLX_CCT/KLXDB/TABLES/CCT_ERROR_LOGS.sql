CREATE TABLE CCT_ERROR_LOGS 
(
  ERROR_ID NUMBER 
, QUOTE_REVISION_ID NUMBER 
, OPPORTUNITY_ID NUMBER 
, QUOTE_NUMBER VARCHAR2(50 BYTE) 
, QUOTE_REVISION VARCHAR2(50 BYTE) 
, ERROR_MESSAGE VARCHAR2(1000 BYTE) 
, ERROR_TYPE VARCHAR2(50 BYTE) 
, MODULE_NAME VARCHAR2(500 BYTE) 
, TASK_ID VARCHAR2(50 BYTE) 
, INSTANCE_ID VARCHAR2(50 BYTE) 
, CREATION_DATE DATE 
, CREATED_BY VARCHAR2(50 BYTE) 
, LAST_UPDATED_DATE DATE 
, LAST_UPDATED_BY VARCHAR2(50 BYTE) 
) 
/

ALTER TABLE CCT_ERROR_LOGS
ADD CONSTRAINT CCT_ERROR_LOGS_FK1 FOREIGN KEY
(
  QUOTE_REVISION_ID 
)
REFERENCES CCT_QUOTE_REVISIONS
(
  REVISION_ID 
)
ENABLE
/
