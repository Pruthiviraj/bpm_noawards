
CREATE TABLE "XXCCT"."CCT_CRF_SALES_CONTACT_INFO" 
(	"CCT_CRF_SALES_CONTACT_INFO_ID" NUMBER NOT NULL ENABLE, 
	"CRF_ID" NUMBER NOT NULL ENABLE, 
	"SALES_REVISION_NO" NUMBER NOT NULL ENABLE, 
	"CREATED_BY" VARCHAR2(255 BYTE), 
	"SALES_OWNER" VARCHAR2(255 BYTE), 
	"SALES_TEAM" VARCHAR2(50 BYTE), 
	"SALES_MANAGER" VARCHAR2(255 BYTE), 
	"SALES_DIRECTOR" VARCHAR2(255 BYTE), 
	"SALES_VP" VARCHAR2(255 BYTE), 
	"SALES_OUTSIDE" VARCHAR2(255 BYTE), 
	 CONSTRAINT "CCT_CRF_SALES_CONTACT_INFO_PK" PRIMARY KEY ("CCT_CRF_SALES_CONTACT_INFO_ID"), 
	 CONSTRAINT "CCT_CRF_SALES_CONTACT_INFO_FK1" FOREIGN KEY ("CRF_ID")
	  REFERENCES "XXCCT"."CCT_CRF" ("CRF_ID") ENABLE
);