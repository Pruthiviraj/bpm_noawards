CREATE TABLE CCT_QUOTE_PARTS 
(
  QUOTE_REVISION_ID NUMBER 
, QUEUE_ID NUMBER 
, WHSE_ID NUMBER 
, PRODUCT_ID NUMBER 
, QUOTED_PART_NUMBER VARCHAR2(50 BYTE) NOT NULL 
, CUSTOMER_PN VARCHAR2(50 BYTE) 
, ANNUAL_QTY NUMBER 
, UOM VARCHAR2(50 BYTE) 
, APPROVED_SOURCE VARCHAR2(50 BYTE) 
, DISAPPROVED_SOURCE VARCHAR2(50 BYTE) 
, MISC_CUST_DATA VARCHAR2(50 BYTE) 
, TARGET_PRICE NUMBER 
, TARGET_MARGIN NUMBER 
, CERTIFIED_PART VARCHAR2(50 BYTE) 
, CONTRACT_PRICE NUMBER 
, LAST_PRICE_PAID NUMBER 
, HIGHEST_CONTRACT_PRICE NUMBER 
, LOWEST_CONTRACT_PRICE NUMBER 
, HIGH_TAPE_PRICE NUMBER 
, MIDDLE_TAPE_PRICE NUMBER 
, LOW_TAPE_PRICE NUMBER 
, TOTAL_QTY_LEN_CONTRACT NUMBER 
, AVERAGE_COST NUMBER 
, AVERAGE_COST_LBS NUMBER 
, LAST_COST NUMBER 
, ACTUAL_COST NUMBER 
, VENDOR_COST NUMBER 
, MARKET_COST NUMBER 
, LTA_COST NUMBER 
, POQ_COST NUMBER 
, POQ_QUANTITY NUMBER 
, DELIVERY_TIME_POQ VARCHAR2(50 BYTE) 
, CD_FROM_JDA VARCHAR2(50 BYTE) 
, KAPCO_PART_COST NUMBER 
, SYMPH_DROP_SHIPMENT VARCHAR2(50 BYTE) 
, HPP_PART_PRICE NUMBER 
, HPP_PART VARCHAR2(50 BYTE) 
, PRIME VARCHAR2(50 BYTE) 
, DUP_PRIME VARCHAR2(50 BYTE) 
, DUP_CUST_PART VARCHAR2(50 BYTE) 
, VENDOR_BACK_ORDERS NUMBER 
, CUST_BACK_ORDERS NUMBER 
, LTA_MOQ NUMBER 
, LTA_EXP_DATE DATE 
, LTA_MFR VARCHAR2(50 BYTE) 
, LTA_LEADTIME_WEEKS VARCHAR2(50 BYTE) 
, VENDOR_LEADTIME_WEEKS VARCHAR2(50 BYTE) 
, PROD_TYPE_ABC VARCHAR2(50 BYTE) 
, KAPCO_LEADTIME_WEEKS VARCHAR2(50 BYTE) 
, FIRST_PLAN_PURDAY DATE 
, JDA_FORECAST_12M NUMBER 
, CONTROL_NUMBER VARCHAR2(50 BYTE) 
, MANF_NUMBER NUMBER 
, MANF_NAME VARCHAR2(50 BYTE) 
, MFR_CAGE_CODE VARCHAR2(50 BYTE) 
, SHELF_LIFE VARCHAR2(50 BYTE) 
, PMA VARCHAR2(50 BYTE) 
, TSO VARCHAR2(50 BYTE) 
, CERTS VARCHAR2(50 BYTE) 
, C_AND_P VARCHAR2(50 BYTE) 
, REV VARCHAR2(50 BYTE) 
, PART_DESC VARCHAR2(50 BYTE) 
, TARIFF VARCHAR2(50 BYTE) 
, ITAR VARCHAR2(50 BYTE) 
, ECCN VARCHAR2(50 BYTE) 
, DFAR VARCHAR2(50 BYTE) 
, PPP NUMBER 
, STOCKING_UNIT_MEASURE VARCHAR2(50 BYTE) 
, COMMENTS VARCHAR2(4000 BYTE) 
, PRODUCT_NOTES VARCHAR2(4000 BYTE) 
, RESTRICTION_CODES VARCHAR2(4000 BYTE) 
, PROD_CLASS NUMBER 
, TOTAL_ONHAND_WHSES NUMBER 
, OTHER_CUST_PRICE NUMBER 
, CONTRACT_PART_NUMBER VARCHAR2(50 BYTE) 
, ALT_PART_NUMBER VARCHAR2(50 BYTE) 
, TOTAL_ALT_QTY_ONHAND NUMBER 
, TOTAL_VENDOR_BACK_ORDERS NUMBER 
, CREATION_DATE DATE 
, CREATED_BY VARCHAR2(50 BYTE) 
, LAST_UPDATED_DATE DATE 
, LAST_UPDATED_BY VARCHAR2(50 BYTE) 
, WHSE_QUANTITY NUMBER 
, LINE_IDENTIFIER NUMBER 
, QUOTE_PART_ID NUMBER NOT NULL 
, RESALE NUMBER 
, COST_USED_FOR_QUOTE NUMBER 
, CALC_ABC VARCHAR2(50 BYTE) 
, ALTERNATIVES_AVL VARCHAR2(2000 BYTE) 
, FINAL_COST_SOURCE VARCHAR2(500 BYTE) 
, EXTENDED_COST NUMBER 
, EXTENDED_RESALE NUMBER 
, FINAL_MKT_CLASS VARCHAR2(100 BYTE) 
, ITEM_TAG VARCHAR2(100 BYTE) 
, LAST_PRICE_OFFERED NUMBER 
, LTA_REVIEW_ITEM NUMBER 
, POTENTIAL_UOM_ISSUE VARCHAR2(100 BYTE) 
, REPLACEMENT_COST_USED VARCHAR2(20 BYTE) 
, SPECIALITY_PRODUCTS VARCHAR2(100 BYTE) 
, PIPELINE_QTY NUMBER 
, DYP_PRICE_BREAK_QTY NUMBER 
, ECOMMERCE_PRICE NUMBER 
, PRICE_TYPE VARCHAR2(100 BYTE) 
, MARGIN_VALIDATION VARCHAR2(10 BYTE) 
, CONSTRAINT CCT_QUOTE_PARTS_PK PRIMARY KEY 
  (
    QUOTE_PART_ID 
  )
  ENABLE 
) 
/

ALTER TABLE CCT_QUOTE_PARTS
ADD CONSTRAINT CCT_QUOTE_PARTS_FK1 FOREIGN KEY
(
  QUOTE_REVISION_ID 
)
REFERENCES CCT_QUOTE_REVISIONS
(
  REVISION_ID 
)
ENABLE
/

ALTER TABLE CCT_QUOTE_PARTS
ADD CONSTRAINT CCT_QUOTE_PARTS_FK2 FOREIGN KEY
(
  QUEUE_ID 
)
REFERENCES CCT_PARTS_QUEUE
(
  QUEUE_ID 
)
ENABLE
/

--lta exp date field change to varchar
ALTER TABLE cct_quote_parts ADD LTA_EXP_DATE_1 VARCHAR2(50)
/
UPDATE cct_quote_parts SET LTA_EXP_DATE_1=TO_CHAR(LTA_EXP_DATE,'DD-MON-YY')
/
UPDATE cct_quote_parts SET LTA_EXP_DATE=NULL
/
ALTER TABLE cct_quote_parts MODIFY LTA_EXP_DATE VARCHAR2(50)
/
UPDATE cct_quote_parts SET LTA_EXP_DATE=LTA_EXP_DATE_1
/
ALTER TABLE cct_quote_parts DROP COLUMN LTA_EXP_DATE_1
/
--addnal fields
ALTER TABLE cct_quote_parts ADD SUGGESTED_CUST_FINAL_LEADTIME VARCHAR2(50)
/
ALTER TABLE cct_quote_parts ADD SUGGESTED_QUOTED_PART_NUMBER VARCHAR2(50)
/
ALTER TABLE cct_quote_parts ADD SUGGESTED_ANNUAL_QTY NUMBER
/
ALTER TABLE cct_quote_parts ADD SUGGESTED_UOM VARCHAR2(50)
/
ALTER TABLE cct_quote_parts ADD SUGGESTED_PRIME VARCHAR2(50)
/
ALTER TABLE cct_quote_parts ADD SUGGESTED_CALC_ABC VARCHAR2(50)
/
ALTER TABLE cct_quote_parts ADD SUGGESTED_COST_USED_FOR_QUOTE NUMBER
/
ALTER TABLE cct_quote_parts ADD SUGGESTED_MANF_NAME VARCHAR2(50)
/
ALTER TABLE cct_quote_parts ADD SUGGESTED_MFR_CAGE_CODE VARCHAR2(50)
/
ALTER TABLE cct_quote_parts ADD SUGGESTED_RESALE NUMBER
/
ALTER TABLE cct_quote_parts ADD Queue_Assignment_date date
/
