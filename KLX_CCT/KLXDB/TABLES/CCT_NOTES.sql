CREATE TABLE CCT_NOTES 
(
  NOTE_ID NUMBER NOT NULL 
, NOTE_LEVEL VARCHAR2(50 BYTE) 
, NOTE_DESC VARCHAR2(4000 BYTE) 
, NOTE_QUOTE_PART_ID NUMBER 
, NOTE_TASK_NO NUMBER 
, CREATION_DATE TIMESTAMP(6) 
, CREATED_BY VARCHAR2(50 BYTE) 
, NOTE_OPP_NO VARCHAR2(20 BYTE) 
, USER_NAME VARCHAR2(100 BYTE) 
, CONSTRAINT XXCCT_NOTES_PK PRIMARY KEY 
  (
    NOTE_ID 
  )
  ENABLE 
)
/

