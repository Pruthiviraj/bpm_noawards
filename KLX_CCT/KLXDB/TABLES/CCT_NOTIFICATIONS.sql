CREATE TABLE CCT_NOTIFICATIONS 
(
  NOTIFICATION_ID NUMBER 
, QUOTE_REVISION_ID NUMBER 
, QUOTE_NUMBER VARCHAR2(50 BYTE) 
, QUOTE_REVISION VARCHAR2(50 BYTE) 
, NOTIF_MESSAGE VARCHAR2(4000 BYTE) 
, NOTIF_RECIPIENT VARCHAR2(50 BYTE) 
, NOTIF_STATUS VARCHAR2(50 BYTE) 
, NOTIF_DATE DATE 
, CREATION_DATE DATE 
, CREATED_BY VARCHAR2(50 BYTE) 
, LAST_UPDATED_DATE DATE 
, LAST_UPDATED_BY VARCHAR2(50 BYTE) 
) 
/

ALTER TABLE CCT_NOTIFICATIONS
ADD CONSTRAINT XXKLX_NOTIFICATIONS_FK1 FOREIGN KEY
(
  QUOTE_REVISION_ID 
)
REFERENCES CCT_QUOTE_REVISIONS
(
  REVISION_ID 
)
ENABLE
/
