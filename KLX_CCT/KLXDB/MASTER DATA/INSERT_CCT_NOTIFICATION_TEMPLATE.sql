REM INSERTING into CCT_NOTIFICATION_TEMPLATE
SET DEFINE OFF;
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (1,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (1,'Queue',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (1,'Customer Number',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (1,'Customer Name',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (1,'Customer Due Date',5);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (1,'Your Due Date',6);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (1,'Product Line Count',7);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (1,'Unknown Product Count',8);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (2,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (2,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (2,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (2,'Contract Term',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (2,'Decimal Places',5);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (2,'FAA (PMA/TSO) Applies',6);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (2,'DFAR Applies',7);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (2,'Customer Specific Warehouses',8);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (2,'Alternative Customer Pricing',9);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (2,'Customer Due Date',10);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (2,'Your Due Date',11);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (3,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (3,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (3,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (3,'Customer Due Date',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (3,'Product Team Due Date',5);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (3,'CSM Products',6);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (3,'Unknown Products',7);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (4,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (4,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (4,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (4,'Customer Due Date',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (4,'CSM Products',5);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (4,'Unknown Products',6);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (5,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (5,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (5,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (5,'Old Customer Due Date',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (5,'New Customer Due Date',5);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (5,'Team Due Date',6);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (5,'CSM Products',7);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (5,'Unknown Products',8);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (6,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (6,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (6,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (6,'Customer Due Date',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (6,'Queue',5);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (6,'Number of items in Queue',6);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (6,'Number of items with Cost',7);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (7,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (7,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (7,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (7,'Customer Due Date',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (8,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (8,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (8,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (8,'Customer Due Date',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Customer Due Date',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (10,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (10,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (10,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (10,'Customer Due Date',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (10,'Approval Status',5);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (11,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (11,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (11,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (11,'Customer Due Date',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (11,'Approver Name',5);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (11,'Approval Status',6);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (12,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (12,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (12,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (12,'Customer Due Date',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (13,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (13,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (13,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (13,'Customer Due Date',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (13,'Opportunity Status',5);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (14,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (14,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (14,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (14,'Customer Due Date',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (15,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (15,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (15,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (15,'Customer Due Date',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (16,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (16,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (16,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (16,'Customer Due Date',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (17,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (17,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (17,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (17,'Customer Due Date',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (17,'Documents',5);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (18,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (18,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (18,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (18,'Customer Due Date',4);
commit;
/
REM INSERTING into CCT_NOTIFICATION_TEMPLATE
SET DEFINE OFF;
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (19,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (19,'Contract Reference #',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (19,'Customer Number',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (19,'Customer Name',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (19,'Customer Due Date',5);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (19,'Description',6);
commit;
/

REM INSERTING into CCT_NOTIFICATION_TEMPLATE
SET DEFINE OFF;
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (21,'Revision Topic',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (21,'Pricing',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (21,'Legal',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (21,'Programs',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (21,'Proposals',5);
COMMIT;
/

delete from CCT_NOTIFICATION_TEMPLATE where NOTIF_TEMPLATE_ID=9;
commit;
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Snapshot Date',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'CRF Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Sales Team #',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Contract Ref #',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Due Date to Customer',5);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Customer Name',6);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'GT25',7);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Contract Type',8);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Project Type',9);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Length of Contract - years',10);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Program Type',11);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Total Product Cost',12);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Total Product Resale',13);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Gross Margin',14);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Total Adjustments',15);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Project Net Margin',16);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Rebate - LOC',17);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Total Program Costs',18);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Shipping Fees/Taxes',19);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Service Fees',20);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Payment Terms',21);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Incoterms',22);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'KLX or Customer Document',23);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Agent Commissions',24);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Line min',25);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Order min',26);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Effective LOL Protection',27);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Customer Liability On Investment',28);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Stocking Strategy',29);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Penalties/Liquidated Damages',30);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Unusual Title Transfer',31);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Rights of Return',32);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Incentives/Other Credits',33);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Cancellations Privileges',34);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Bill and Hold',35);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Buy Back',36);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Currency',37);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Total Parts Requested',38);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Total Parts Quoted',39);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class A Item Count',40);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class A Cost - LOC',41);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class A Resale - LOC',42);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class A Margin',43);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class A1A Item Count',44);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class A1A Cost - LOC',45);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class A1A Resale - LOC',46);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class A1A Margin',47);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class A1B Item Count',48);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class A1B Cost - LOC',49);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class A1B Resale - LOC',50);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class A1B Margin',51);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class B Item Count',52);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class B Cost - LOC',53);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class B Resale - LOC',54);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class B Margin',55);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class C Item Count',56);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class C Cost - LOC',57);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class C Resale - LOC',58);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class C Margin',59);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class D Item Count',60);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class D Cost - LOC',61);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class D Resale - LOC',62);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Product Class D Margin',63);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (9,'Proposal Manager Comments',64);
commit;
/
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (20,'CRF Number',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (20,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (20,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (20,'Customer Due Date',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (20,'Task Number',5);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (20,'Approval Status',6);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (20,'Group',7);
commit;
/


REM INSERTING into CCT_NOTIFICATION_TEMPLATE
SET DEFINE OFF;
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (22,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (22,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (22,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (23,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (23,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (23,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (23,'Customer Due Date',4);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (23,'Cardex Payment Terms',5);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (23,'Customer Requested Terms',6);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (23,'KLX Offered Terms',7);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (23,'Payment Terms Note',8);
commit;
/
update cct_notification_template set seq_order=seq_order+2 where NOTIF_TEMPLATE_ID=21;
commit;
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (21,'Customer Number',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (21,'Customer Name',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (24,'Opportunity',1);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (24,'Customer Number',2);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (24,'Customer Name',3);
Insert into CCT_NOTIFICATION_TEMPLATE (NOTIF_TEMPLATE_ID,TABLE_HEADER,SEQ_ORDER) values (24,'Customer Due Date',4);
commit;
