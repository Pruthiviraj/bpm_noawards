Insert into DMS_FOLDER (FOLDER_ID,FOLDER_NAME,FOLDER_DESCRIPTION,PARENT_FOLDER_ID,IS_DISABLED,CREATED_BY,CREATED_DATE,UPDATED_BY,UPDATED_DATE) values (1,'Customer Initial','Folder for Customer Initial',1,'N','admin',sysdate,null,null)
/

Insert into DMS_FOLDER (FOLDER_ID,FOLDER_NAME,FOLDER_DESCRIPTION,PARENT_FOLDER_ID,IS_DISABLED,CREATED_BY,CREATED_DATE,UPDATED_BY,UPDATED_DATE) values (2,'Working Quote Docs','Folder for Working Quote Docs',2,'N','admin',sysdate,null,null)
/

Insert into DMS_FOLDER (FOLDER_ID,FOLDER_NAME,FOLDER_DESCRIPTION,PARENT_FOLDER_ID,IS_DISABLED,CREATED_BY,CREATED_DATE,UPDATED_BY,UPDATED_DATE) values (3,'Customer Final','Folder for Customer Fina',3,'N','admin',sysdate,null,null)
/

Insert into DMS_FOLDER (FOLDER_ID,FOLDER_NAME,FOLDER_DESCRIPTION,PARENT_FOLDER_ID,IS_DISABLED,CREATED_BY,CREATED_DATE,UPDATED_BY,UPDATED_DATE) values (4,'Awards','folder for Awards',4,'N','admin',sysdate,null,null)
/

Insert into DMS_FOLDER (FOLDER_ID,FOLDER_NAME,FOLDER_DESCRIPTION,PARENT_FOLDER_ID,IS_DISABLED,CREATED_BY,CREATED_DATE,UPDATED_BY,UPDATED_DATE) values (5,'Migrated Docs','Folder for Migrated Docs',5,'N','admin',sysdate,null,null)

commit
/