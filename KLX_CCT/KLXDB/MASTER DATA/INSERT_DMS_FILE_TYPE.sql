Insert into DMS_FILE_TYPE (FILE_TYPE_ID,FILE_CONTENT_TYPE,FILE_TYPE,CREATED_BY,CREATED_DATE,UPDATED_BY,UPDATED_DATE) values (13,'application/vnd.openxmlformats-officedocument.presentationml.presentation','PPTX','admin',sysdate,null,null)
/
Insert into DMS_FILE_TYPE (FILE_TYPE_ID,FILE_CONTENT_TYPE,FILE_TYPE,CREATED_BY,CREATED_DATE,UPDATED_BY,UPDATED_DATE) values (14,'application/vnd.ms-excel','CSV','admin',sysdate,null,null)
/
Insert into DMS_FILE_TYPE (FILE_TYPE_ID,FILE_CONTENT_TYPE,FILE_TYPE,CREATED_BY,CREATED_DATE,UPDATED_BY,UPDATED_DATE) values (8,'application/vnd.ms-excel.sheet.macroenabled.12','XLSM','admin',sysdate,null,null)
/
Insert into DMS_FILE_TYPE (FILE_TYPE_ID,FILE_CONTENT_TYPE,FILE_TYPE,CREATED_BY,CREATED_DATE,UPDATED_BY,UPDATED_DATE) values (9,'application/vnd.ms-excel.sheet.binary.macroenabled.12','XLSB','admin',sysdate,null,null)
/
Insert into DMS_FILE_TYPE (FILE_TYPE_ID,FILE_CONTENT_TYPE,FILE_TYPE,CREATED_BY,CREATED_DATE,UPDATED_BY,UPDATED_DATE) values (10,'application/vnd.ms-powerpoint','PPT','admin',sysdate,null,null)
/
Insert into DMS_FILE_TYPE (FILE_TYPE_ID,FILE_CONTENT_TYPE,FILE_TYPE,CREATED_BY,CREATED_DATE,UPDATED_BY,UPDATED_DATE) values (11,'application/zip','ZIP','admin',sysdate,null,null)
/
Insert into DMS_FILE_TYPE (FILE_TYPE_ID,FILE_CONTENT_TYPE,FILE_TYPE,CREATED_BY,CREATED_DATE,UPDATED_BY,UPDATED_DATE) values (12,'text/plain','TXT','admin',sysdate,null,null)
/
Insert into DMS_FILE_TYPE (FILE_TYPE_ID,FILE_CONTENT_TYPE,FILE_TYPE,CREATED_BY,CREATED_DATE,UPDATED_BY,UPDATED_DATE) values (1,'application/pdf','PDF','admin',sysdate,null,null)
/
Insert into DMS_FILE_TYPE (FILE_TYPE_ID,FILE_CONTENT_TYPE,FILE_TYPE,CREATED_BY,CREATED_DATE,UPDATED_BY,UPDATED_DATE) values (2,'image/jpeg','JPEG','admin',sysdate,null,null)
/
Insert into DMS_FILE_TYPE (FILE_TYPE_ID,FILE_CONTENT_TYPE,FILE_TYPE,CREATED_BY,CREATED_DATE,UPDATED_BY,UPDATED_DATE) values (3,'application/msword','DOC','admin',sysdate,null,null)
/
Insert into DMS_FILE_TYPE (FILE_TYPE_ID,FILE_CONTENT_TYPE,FILE_TYPE,CREATED_BY,CREATED_DATE,UPDATED_BY,UPDATED_DATE) values (4,'application/vnd.openxmlformats-officedocument.wordprocessingml.document','DOCX','admin',sysdate,null,null)
/
Insert into DMS_FILE_TYPE (FILE_TYPE_ID,FILE_CONTENT_TYPE,FILE_TYPE,CREATED_BY,CREATED_DATE,UPDATED_BY,UPDATED_DATE) values (5,'application/x-zip-compressed','ZIP','admin',sysdate,null,null)
/
Insert into DMS_FILE_TYPE (FILE_TYPE_ID,FILE_CONTENT_TYPE,FILE_TYPE,CREATED_BY,CREATED_DATE,UPDATED_BY,UPDATED_DATE) values (6,'application/octet-stream','ZIP','admin',sysdate,null,null)
/
Insert into DMS_FILE_TYPE (FILE_TYPE_ID,FILE_CONTENT_TYPE,FILE_TYPE,CREATED_BY,CREATED_DATE,UPDATED_BY,UPDATED_DATE) values (7,'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','XLSX','admin',sysdate,null,null)
/
Insert into DMS_FILE_TYPE (FILE_TYPE_ID,FILE_CONTENT_TYPE,FILE_TYPE) values (15,'application/vnd.ms-excel','XLS')
/
Insert into DMS_FILE_TYPE (FILE_TYPE_ID,FILE_CONTENT_TYPE,FILE_TYPE) values (16,'image/png','PNG')
/
Insert into DMS_FILE_TYPE (FILE_TYPE_ID,FILE_CONTENT_TYPE,FILE_TYPE) values (17,'image/tiff','TIFF')
/
Insert into DMS_FILE_TYPE (FILE_TYPE_ID,FILE_CONTENT_TYPE,FILE_TYPE) values (18,'image/bmp','BPM')
/
Insert into DMS_FILE_TYPE (FILE_TYPE_ID,FILE_CONTENT_TYPE,FILE_TYPE) values (19,'text/html','HTML')
/
Insert into DMS_FILE_TYPE (FILE_TYPE_ID,FILE_CONTENT_TYPE,FILE_TYPE) values (20,'application/x-gzip','GZ')
/
commit
/
