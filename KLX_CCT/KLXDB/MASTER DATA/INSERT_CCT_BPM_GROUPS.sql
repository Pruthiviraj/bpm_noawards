Insert into CCT_BPM_GROUPS (GROUP_ID,GROUP_NAME) values ('scCCT_Award_Manager','Awards Manager')
/
Insert into CCT_BPM_GROUPS (GROUP_ID,GROUP_NAME) values ('scCCT_CSM','Chemical')
/
Insert into CCT_BPM_GROUPS (GROUP_ID,GROUP_NAME) values ('scCCT_Kitting','Kitting')
/
Insert into CCT_BPM_GROUPS (GROUP_ID,GROUP_NAME) values ('scCCT_Lighting_Product_Line','Lighting')
/
Insert into CCT_BPM_GROUPS (GROUP_ID,GROUP_NAME) values ('scCCT_Proposal_Manager','Proposal Manager')
/
Insert into CCT_BPM_GROUPS (GROUP_ID,GROUP_NAME) values ('scCCT_Strategic_Pricing','Pricing Manager')
/
Insert into CCT_BPM_GROUPS (GROUP_ID,GROUP_NAME) values ('scCCT_Advanced_Sourcing','Advanced Sourcing')
/
Insert into CCT_BPM_GROUPS (GROUP_ID,GROUP_NAME) values ('scCCT_Quality','Quality')
/
Insert into CCT_BPM_GROUPS (GROUP_ID,GROUP_NAME) values ('scCCT_Legal','Legal')
/
Insert into CCT_BPM_GROUPS (GROUP_ID,GROUP_NAME) values ('scCCT_Operations','Operations')
/

commit
/
