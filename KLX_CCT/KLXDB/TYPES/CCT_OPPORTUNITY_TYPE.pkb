create or replace TYPE BODY CCT_OPPORTUNITY_TYPE
AS
CONSTRUCTOR FUNCTION CCT_OPPORTUNITY_TYPE RETURN SELF AS RESULT
IS
BEGIN
        OSC_OPPORTUNITY_ID      	:= NULL ;
        OPPORTUNITY_NUMBER      	:= NULL ;
        OPPORTUNITY_NAME        	:= NULL ;
        CUSTOMER_NUMBER         	:= NULL ;
        CUSTOMER_NAME           	:= NULL ;
        CURRENCY                	:= NULL ;
        SALES_METHOD            	:= NULL ;
        STATUS                  	:= NULL ;
        CUSTOMER_DUE_DATE       	:= NULL ;
        CONTRACT_TERM           	:= NULL ;
        DECIMAL_TO_QUOTE        	:= NULL ;
        FAA_CERTIFICATION       	:= NULL ;
        DFAR_APPLY              	:= NULL ;
        CUSTOMER_WH             	:= NULL ;
        ALT_PRICING_CUST_NUM    	:= NULL ;
        CUST_APPR_SOURCES       	:= NULL ;
        PROJECT_NUMBER          	:= NULL ;
        SPECIAL_INSTRUCTION     	:= NULL ;
        QUOTE_BASIS             	:= NULL ;
        DEAL_TYPE               	:= NULL ;
        DEAL_STRUCTURE          	:= NULL ;
        SOURCE_PRODUCTS         	:= NULL ;
        REBATE                  	:= NULL ;
        PAYMENT_TERMS           	:= NULL ;
        INCOTERMS               	:= NULL ;
        AGENT_COMMISSIONS       	:= NULL ;
        LINE_MIN_PROVISION      	:= NULL ;
        LOL_PROTECTION          	:= NULL ;
        CUST_LIAB_PROTECTION    	:= NULL ;
        STOCKING_STRATEGY       	:= NULL ;
        PENALTIES               	:= NULL ;
        TITLE_TRANSFER          	:= NULL ;
        RIGHTS_OF_RETURN        	:= NULL ;
        INCENTIVES              	:= NULL ;
        CANCEL_PRIVILEGES       	:= NULL ;
        BILL_HOLD               	:= NULL ;
        BUY_BACK                	:= NULL ;
        GT25                    	:= NULL ;
        TOTAL_PROD_COST         	:= NULL ;
        TOTAL_PROD_RESALE       	:= NULL ;
        PROD_GRS_MARGIN         	:= NULL ;
        PROJ_NET_MARGIN         	:= NULL ;
        BITEM_LIABILITY         	:= NULL ;
        CUSTOMER_DOC            	:= NULL ;
        TOTAL_PROD_REQUESTED    	:= NULL ;
        TOTAL_PROD_QUOTED       	:= NULL ;
        TOTAL_ADJUSTMENTS       	:= NULL ;
        TOTAL_PROGRAM_COSTS     	:= NULL ;
        SHIPPING_FEES           	:= NULL ;
        SHIPPING_TAXES          	:= NULL ;
        SERVICE_FEES            	:= NULL ;
        LIQ_DAMAGES             	:= NULL ;
        TARGET_PERCENTAGE       	:= NULL ;
        TARGET_DATE             	:= NULL ;
        CONTRACT_TEAM           	:= NULL ;
        USER_NAME               	:= NULL ;
        ONAME                   	:= NULL ;
        EMAILID                 	:= NULL ;
        USER_GROUP              	:= NULL ;
        ROLE                    	:= NULL ;
        MANAGER                 	:= NULL ;
        SALES_REP               	:= NULL ;
        PROPOSAL_MANAGER        	:= NULL ;
        PRICING_MANAGER         	:= NULL ;
        CUST_LIAB_ON_INVEST     	:= NULL ;
        RET_FMT_PROPOSAL        	:= NULL ;
        AMOUNT                  	:= NULL ;
        PROGRAM_TYPE            	:= NULL ;
        EXPECTED_REVENUE        	:= NULL ;
        PRIMARY_CONTACT         	:= NULL ;
        PRIMARY_COMPETITOR      	:= NULL ;
        FWD_STOCK_LOCATION      	:= NULL ;
        LINE_MINIMUM            	:= NULL ;
        ULT_DESTINATION_OF_PARTS	:= NULL ;
        DESCRIPTION             	:= NULL ;
        RET_FMT_PRICING_FILE    	:= NULL ;
        CAN_USE_MIN_ORD_QTY     	:= NULL ;
        SALES_STAGE             	:= NULL ;
        RECOVER_SHIPPING_COST   	:= NULL ;
        TSO_PMA_CERTIFICATION   	:= NULL ;
        WIN_LOSS_REASON         	:= NULL ;
        PLATFORM_NAME           	:= NULL ;
        CUST_REF_NUMBER         	:= NULL ;
        WIN_PROBABILITY         	:= NULL ;
        ORDER_MINIMUM           	:= NULL ;
        TEAM_NUMBER             	:= NULL ;
        EXISTING_CONTRACT_NUM   	:= NULL ;
        FWD_STOCK_LOC_REQ       	:= NULL ;
        CLOSE_DATE              	:= NULL ;
        RECOVER_PROG_COST       	:= NULL ;
        CREATION_DATE           	:= NULL ;
        CREATED_BY              	:= NULL ;
        LAST_UPDATED_DATE       	:= NULL ;
        LAST_UPDATED_BY         	:= NULL ;
		OTHER_PAYMENT_TERMS	    	:= NULL ;
		OTHER_INCOTERMS	        	:= NULL ;
		OTHER_CUST_APPROVED_SOURCES	:= NULL ;
        TERMS_NOTE                  := NULL ;
        PROJECT_NUM_BLANK           := NULL ;
        SALES_PROJECT_NUM_BLANK     := NULL ;
        AWARD_FILE_UPLOAD           := NULL ;
        DOC_RDY_TO_SUBMIT           := NULL ;
        SALES_OWNER_EMAIL           := NULL ;
        IS_CUST_REBATE_REQUESTED	:= NULL ;
        REQUESTED_STOCKING_STRATEGY	:= NULL ;
        REBATE_CRF                  := NULL ;
        CUSTOMER_NO_CRF             := NULL ;
        STOCKING_STRATEGY_CRF       := NULL ;
        SERVICE_FEES_CRF            := NULL ;
        LINE_MIN_CRF                := NULL ;
        CONTRACT_TYPE	            := NULL ;
        PROJECT_TYPE	            := NULL ;
RETURN ;
END;
END;
/