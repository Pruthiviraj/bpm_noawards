CREATE SEQUENCE  "XXCCT"."CCT_CRF_SALES_TEAM_SEQ" 
       MINVALUE 1 
       MAXVALUE 999999999999999999999999999 
       INCREMENT BY 1 
       START WITH 1 
       CACHE 20 NOORDER  NOCYCLE  NOPARTITION ;