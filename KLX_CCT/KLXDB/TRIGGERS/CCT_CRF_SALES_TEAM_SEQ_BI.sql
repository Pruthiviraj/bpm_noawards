create or replace trigger CCT_CRF_SALES_TEAM_SEQ_BI
before insert
  on CCT_CRF_SALES_TEAM
  for each row
  begin
  select CCT_CRF_SALES_TEAM_SEQ.nextval into :new.SALES_TEAM_ID from dual;
  end;