create or replace TRIGGER CCT_OPPORTUNITY_SEQ_BI
BEFORE INSERT
  ON CCT_OPPORTUNITY
  FOR EACH ROW
  BEGIN
  	SELECT CCT_OPPORTUNITY_SEQ.NEXTVAL INTO :NEW.OPPORTUNITY_ID FROM DUAL;
  END;