create or replace trigger CCT_QUOTE_PARTS_FINAL_SEQ_BI
before insert
  on CCT_QUOTE_PARTS_FINAL
  for each row
  begin
  select CCT_QUOTE_PARTS_FINAL_SEQ.nextval into :new.QUOTE_PART_ID from dual;
  end;