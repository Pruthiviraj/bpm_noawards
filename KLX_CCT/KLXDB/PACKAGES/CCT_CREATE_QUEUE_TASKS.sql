CREATE OR REPLACE PACKAGE cct_create_queue_tasks AS
/**Handles creating queue tasks
  Modification History
  PERSON        DATE           COMMENTS
  ------------  -----------    ---------------------------------------------
  Sureka        07-Jul-2017    Created initialize proc
  Sureka        07-Jul-2017    Created create_parts_queue proc
  Ankit Bhatt   14-Jul-2017    Added create_queue_task proc
  Ankit Bhatt   20-Jul-2017    Added other roles for different Queue
  Ankit Bhatt   25-Jul-2017    Modified procedure create_parts_queue to fulfill
                               'Advance Sourcing' and 'Unknown' where conditions
  Ankit Bhatt   26-Jul-2017    Modified procedure create_queue_task
  Ankit Bhatt   28-Jul-2017    Modified create_queue_task procedure to add validation
                               for task creation
  Ankit Bhatt   31-Jul-2017    Added task creation validation
  Ankit Bhatt   21-Sep-2017    Call cct_create_opportunity.update_formula_fields
                               procedure to update net margin and amount
                               after queue assignment done.
  Ankit Bhatt   10-Oct-2017    Modified date format
  Ankit Bhatt   31-Oct-2017    Modified proc. create_queue_task to pass attribute4
                               as 'BeaconUploadProcess'
  Raje          29-NOV-2018     Added electrical queue
**/
    TYPE cct_queue_tbl_type IS
        TABLE OF VARCHAR2(50) INDEX BY PLS_INTEGER;
    g_queuetypeelem            CCT_QUEUE_TBL_TYPE;
    g_prodtypeelem             CCT_PROD_TYPE;
    g_chemicalarray            CCT_QUEUE_PROD_CLASS_TBL_TYPE;
    g_lightningarray           CCT_QUEUE_PROD_CLASS_TBL_TYPE;
    g_electricalarray          CCT_QUEUE_PROD_CLASS_TBL_TYPE;
    g_kittingarray             CCT_QUEUE_PROD_CLASS_TBL_TYPE;
    g_hpp_part                 CCT_QUOTE_PARTS.HPP_PART%TYPE;
    g_item_tag                 CCT_QUOTE_PARTS.ITEM_TAG%TYPE;
    --Queue Names
    g_queue_commodity          CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
    g_queue_advance_sourcing   CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
    g_queue_hpp                CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
    g_queue_lighting           CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
    g_queue_electrical         CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
    g_queue_kitting            CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
    g_queue_strategic_pricing  CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
    g_queue_chemical           CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
    g_queue_unknown            CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
    g_queue_no_bid             CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
    --Role Names
    g_role_advance_sourcing    CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
    g_role_hpp                 CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
    g_role_lighting            CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
    g_role_electrical          CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
    g_role_kitting             CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
    g_role_strategic_pricing   CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
    g_role_checmical           CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
    g_role_unknown             CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
    g_source_products_y        CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
    g_source_products_n        CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
    g_code                     VARCHAR2(50);
    g_msg                      VARCHAR2(2000);
    
    PROCEDURE initialize;

    PROCEDURE create_parts_queue (
        P_QUOTE_NUMBER     IN CCT_QUOTE.QUOTE_NUMBER%TYPE,
        P_QUOTE_REVISION   IN CCT_QUOTE_REVISIONS.QUOTE_REVISION%TYPE,
        P_RETURN_STATUS   OUT NOCOPY VARCHAR2
    );

    PROCEDURE create_queue_task (
        P_QUOTE_NUMBER     IN CCT_QUOTE.QUOTE_NUMBER%TYPE,
        P_QUOTE_REVISION   IN CCT_QUOTE_REVISIONS.QUOTE_REVISION%TYPE,
        P_RETURN_STATUS   OUT NOCOPY VARCHAR2
    );

END cct_create_queue_tasks;
/


CREATE OR REPLACE PACKAGE BODY cct_create_queue_tasks
AS
/**Handles creating queue tasks
  Modification History
  PERSON        DATE           COMMENTS
  ------------  -----------    ---------------------------------------------
  Sureka        07-Jul-2017    Created initialize proc
  Sureka        07-Jul-2017    Created create_parts_queue proc
  Ankit Bhatt   14-Jul-2017    Added create_queue_task proc
  Ankit Bhatt   20-Jul-2017    Added other roles for different Queue
  Ankit Bhatt   25-Jul-2017    Modified procedure create_parts_queue to fulfill
                               Advance Sourcing' and 'Unknown' where conditions
  Ankit Bhatt   26-Jul-2017    Modified procedure create_queue_task
  Ankit Bhatt   28-Jul-2017    Modified create_queue_task procedure to add validation
                               for task creation
  Ankit Bhatt   31-Jul-2017    Added task creation validation
  Ankit Bhatt   21-Sep-2017    Call cct_create_opportunity.update_formula_fields
                               procedure to update net margin and amount after 
                                queue assignment done.
  Ankit Bhatt   10-Oct-2017    Modified date format
  Ankit Bhatt   31-Oct-2017    Modified proc. create_queue_task to pass attribute4
                                as 'BeaconUploadProcess'
  Ankit Bhatt   06-Nov-2017    Modified procedure create_queue_task to send  notification
                               to scCCT_Technical_Support_Team group for Unknown queue
  Ankit Bhatt   09-Nov-2017    Modified create_parts_queue procedure to update QUEUE_ASSIGNMENT_DATE
                                with date and time
  Ankit Bhatt   15-Nov-2017   Added logs in create_queue_task procedure
  Ankit Bhatt   30-Nov-2017   Modified create_queue_task procedure procedure fetch groups(roles)
                              based on group id
  Ankit Bhatt   13-Dec-2017   Modified create_parts_queue procedure
  Ankit Bhatt   11-Oct-2018   Modified create_parts_queue proc. to improve performance
  Raje          29-NOV-2018   Added Electrical Queue
**/
    PROCEDURE initialize
    AS
    BEGIN
        g_chemicalarray           := cct_queue_prod_class_tbl_type( 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 19, 21, 23, 25, 26, 27, 28, 29, 30, 32, 50, 58, 61, 70, 71, 77, 82, 586, 587, 588, 589, 590, 591, 592, 593, 594, 595, 3,20,22,34,39,481,664,665,666,667,668,670,672,673 );
        g_lightningarray          := cct_queue_prod_class_tbl_type( 56, 59, 62, 63, 287, 288, 289, 290, 291, 292 );
        g_kittingarray            := cct_queue_prod_class_tbl_type( 192, 261, 370 );
        g_electricalarray         := cct_queue_prod_class_tbl_type( 57, 191, 295, 583, 604, 607, 617 );
        g_hpp_part                := 'Y';
        g_item_tag                := '5';
        g_source_products_y       := 'TRUE';
        g_source_products_n       := 'FALSE';
        g_queuetypeelem(1)        := 1;
        g_queuetypeelem(2)        := 2;
        g_queuetypeelem(3)        := 3;
        g_queuetypeelem(4)        := 4;
        g_queuetypeelem(5)        := 5;
        g_queuetypeelem(6)        := 6;
        g_queuetypeelem(7)        := 7;
        g_queuetypeelem(8)        := 8;
        g_queuetypeelem(9)        := 9;
        g_prodtypeelem            := cct_prod_type( 'C', 'R' );

        --Variable declaration and assignment for Queues
        g_queue_commodity         := upper('Commodity');
        g_queue_advance_sourcing  := upper('Advance Sourcing');
        g_queue_hpp               := upper('HPP');
        g_queue_lighting          := upper('Lighting');
        g_queue_electrical        := upper('Electrical');
        g_queue_kitting           := upper('Kitting');
        g_queue_strategic_pricing := upper('Strategic Pricing');
        g_queue_chemical          := upper('Chemical');
        g_queue_unknown           := upper('Unknown');
        g_queue_no_bid            := upper('No Bid');

        --Variable declaration and assignment for Groups(Roles)
        g_role_advance_sourcing   := upper('CCT_ADVANCED_SOURCING');
        g_role_hpp                := upper('CCT_HONEYWELL_PRODUCT_LINE');
        g_role_lighting           := upper('CCT_LIGHTING_PRODUCT_LINE');
        g_role_electrical         := upper('CCT_ELECTRICAL_PRODUCT_LINE');
        g_role_kitting            := upper('CCT_KITTING');
        g_role_strategic_pricing  := upper('CCT_STRATEGIC_PRICING');
        g_role_checmical          := upper('CCT_CSM');
        g_role_unknown            := upper('CCT_TECHNICAL_SUPPORT_TEAM');
    END initialize;

    PROCEDURE create_parts_queue (
        P_QUOTE_NUMBER     IN CCT_QUOTE.QUOTE_NUMBER%TYPE,
        P_QUOTE_REVISION   IN CCT_QUOTE_REVISIONS.QUOTE_REVISION%TYPE,
        P_RETURN_STATUS   OUT NOCOPY VARCHAR2)
    IS
        l_quote_number        CCT_QUOTE.QUOTE_NUMBER%TYPE;
        l_quote_revision      CCT_QUOTE_REVISIONS.QUOTE_REVISION%TYPE;
        l_opportunity_id      CCT_QUOTE.OPPORTUNITY_ID%TYPE;
        l_quote_revision_id   CCT_QUOTE_PARTS.QUOTE_REVISION_ID%TYPE;
        l_status              VARCHAR2(20);
        
        CURSOR part_cur 
        IS
           SELECT cqp.hpp_part,
                  cqp.quote_revision_id,
                  cqp.prod_type_abc,
                  cqp.prod_class,
                  co.opportunity_id,
                  co.source_products,
                  cqp.cost_used_for_quote,
                  cqp.item_tag, 
                  cqp.quote_part_id 
              FROM cct_quote_parts cqp,
                   cct_quote_revisions cqr,
                   cct_quote cq,
                   cct_opportunity co
             WHERE cqp.quote_revision_id = cqr.revision_id AND 
                   cqr.quote_id = cq.quote_id AND 
                   co.opportunity_id = cq.opportunity_id AND 
                   cqp.queue_id IS NULL AND 
                   cqr.quote_revision = P_QUOTE_REVISION AND 
                   cq.quote_number = P_QUOTE_NUMBER;

           part_rec part_cur%rowtype;                         
    BEGIN
        initialize;
        l_quote_number   := P_QUOTE_NUMBER;
        l_quote_revision := P_QUOTE_REVISION;     

        BEGIN
          SELECT DISTINCT co.opportunity_id, 
                 cqr.revision_id 
            INTO l_opportunity_id,
                 l_quote_revision_id   
            FROM cct_quote_revisions cqr,
                 cct_quote cq,
                 cct_opportunity co
           WHERE cqr.quote_id = cq.quote_id AND 
                 co.opportunity_id = cq.opportunity_id AND 
                 cqr.quote_revision = l_quote_revision AND 
                 cq.quote_number = l_quote_number;
        EXCEPTION
        WHEN OTHERS THEN
          g_code := SQLCODE;
          g_msg  := SUBSTR( sqlerrm, 1, 500 );
          cct_create_errors.record_error( 
            P_QUOTE_NUMBER => l_quote_number, 
            P_QUOTE_REVISION => l_quote_revision, 
            P_ERROR_MESSAGE => g_msg, 
            P_ERROR_TYPE => g_code, 
            P_MODULE_NAME => 'CCT_CREATE_QUEUE_TASKS.create_parts_queue', 
            P_CREATION_DATE => SYSDATE, 
            P_CREATED_BY => sys_context('USERENV','OS_USER'));
        END;
/*  modified to improve performance */        
            OPEN part_cur;
            LOOP
                FETCH part_cur INTO part_rec;
                EXIT WHEN part_cur%notfound;
                    UPDATE cct_quote_parts cqp
                        SET
                            queue_id = (
                                CASE
                                    WHEN (
                                        cqp.item_tag = g_item_tag
                                    ) THEN g_queuetypeelem(8)
                                    WHEN upper(cqp.hpp_part) = g_hpp_part THEN g_queuetypeelem(1)
                                    WHEN (
                                            upper(cqp.prod_type_abc) = g_prodtypeelem(1)
                                        OR
                                            upper(cqp.prod_type_abc) = g_prodtypeelem(2)
                                    ) AND (
                                        cqp.prod_class MEMBER OF g_chemicalarray
                                    ) THEN g_queuetypeelem(2)
                                    WHEN (
                                        cqp.prod_class MEMBER OF g_lightningarray
                                    ) THEN g_queuetypeelem(3)
                                    WHEN (
                                        cqp.cost_used_for_quote IS NOT NULL
                                    ) THEN g_queuetypeelem(4)
                                    WHEN (
                                            cqp.cost_used_for_quote IS NULL
                                        AND
                                            upper(part_rec.source_products) = g_source_products_y
                                    ) THEN g_queuetypeelem(5)
                                    WHEN (
                                            cqp.cost_used_for_quote IS NULL
                                        AND
                                            upper(part_rec.source_products) = g_source_products_n
                                    ) THEN g_queuetypeelem(6) 
                                                --WHEN cqp.prod_class MEMBER OF g_kittingarray THEN
                                                --g_queuetypeelem(9)
                                    ELSE g_queuetypeelem(6)
                                END
                            ),
                            last_updated_date = SYSDATE
                    WHERE
                            quote_revision_id = part_rec.quote_revision_id
                        AND
                            quote_part_id = part_rec.quote_part_id;
             END LOOP;
             COMMIT;
     /*  modified to improve performance */                     
     
             /*
        FOR rec IN (SELECT cqp.hpp_part,
                           cqp.quote_revision_id,
                           cqp.prod_type_abc,
                           cqp.prod_class,
                           co.opportunity_id,
                           co.source_products,
                           cqp.cost_used_for_quote,
                           cqp.item_tag, 
                           cqp.quote_part_id 
                      FROM cct_quote_parts cqp,
                           cct_quote_revisions cqr,
                           cct_quote cq,
                           cct_opportunity co
                     WHERE cqp.quote_revision_id = cqr.revision_id AND 
                           cqr.quote_id = cq.quote_id AND 
                           co.opportunity_id = cq.opportunity_id AND 
                           cqp.queue_id IS NULL AND 
                           cqr.quote_revision = l_quote_revision AND 
                           cq.quote_number = l_quote_number)
        LOOP
        UPDATE cct_quote_parts cqp 
           SET queue_id = (CASE WHEN (cqp.item_tag = g_item_tag) THEN 
                                g_queuetypeelem(8) 
                                WHEN upper(cqp.hpp_part) = g_hpp_part THEN 
                                g_queuetypeelem(1) 
                                WHEN (upper(cqp.prod_type_abc) = g_prodtypeelem(1) OR 
                                      upper(cqp.prod_type_abc) = g_prodtypeelem(2)) AND 
                                     (cqp.prod_class MEMBER OF g_chemicalarray) THEN 
                                g_queuetypeelem(2) 
                                WHEN (cqp.prod_class MEMBER OF g_lightningarray) THEN 
                                g_queuetypeelem(3) 
                                WHEN (cqp.cost_used_for_quote IS NOT NULL) THEN 
                                g_queuetypeelem(4) 
                                WHEN (cqp.cost_used_for_quote IS NULL AND 
                                      upper(rec.source_products)  = g_source_products_y) THEN 
                                g_queuetypeelem(5) 
                                WHEN (cqp.cost_used_for_quote IS NULL AND 
                                      upper(rec.source_products)  = g_source_products_n) THEN 
                                g_queuetypeelem(6) 
                                --WHEN cqp.prod_class MEMBER OF g_kittingarray THEN
                                --g_queuetypeelem(9)
                           ELSE g_queuetypeelem(6) 
                           END),
              last_updated_date = SYSDATE
        WHERE quote_revision_id = rec.quote_revision_id AND 
              quote_part_id = rec.quote_part_id;
        END LOOP;
        
        COMMIT;       
    */
        cct_create_opportunity.update_formula_fields(l_opportunity_id);
        create_queue_task( P_QUOTE_NUMBER, P_QUOTE_REVISION, l_status );

        IF l_status = 'SUCCESS' THEN
          P_RETURN_STATUS := 'SUCCESS';
        ELSE
          P_RETURN_STATUS := 'FAILURE';
        END IF;

    EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
        P_RETURN_STATUS := 'FAILURE';
        g_code          := SQLCODE;
        g_msg           := SUBSTR( sqlerrm, 1, 500 );
        cct_create_errors.record_error( 
            P_QUOTE_REVISION_ID => l_quote_revision_id, 
            P_OPPORTUNITY_ID => l_opportunity_id, 
            P_QUOTE_NUMBER => l_quote_number, 
            P_QUOTE_REVISION => l_quote_revision, 
            P_ERROR_MESSAGE => g_msg, 
            P_ERROR_TYPE => g_code, 
            P_MODULE_NAME => 'CCT_CREATE_QUEUE_TASKS.create_parts_queue', 
            P_CREATION_DATE => SYSDATE, 
            P_CREATED_BY => sys_context('USERENV','OS_USER'));
    END create_parts_queue;

    PROCEDURE create_queue_task(
        P_QUOTE_NUMBER     IN CCT_QUOTE.QUOTE_NUMBER%TYPE,
        P_QUOTE_REVISION   IN CCT_QUOTE_REVISIONS.QUOTE_REVISION%TYPE,
        P_RETURN_STATUS   OUT NOCOPY VARCHAR2)
    AS
        CURSOR cur_create_task
        IS
          SELECT DISTINCT co.opportunity_number,
                 co.osc_opportunity_id,
                 cq.quote_id,
                 cqr.quote_revision,
                 cpq.queue_name
            FROM cct_opportunity co,
                 cct_quote cq,
                 cct_quote_revisions cqr,
                 cct_parts_queue cpq,
                 cct_quote_parts cqp
           WHERE co.opportunity_id = cq.opportunity_id AND 
                 cq.quote_id = cqr.quote_id AND 
                 cqr.revision_id = cqp.quote_revision_id AND 
                 cpq.queue_id = cqp.queue_id AND 
                 cqp.queue_id IS NOT NULL AND 
                 cq.quote_number = P_QUOTE_NUMBER AND 
                 cqr.quote_revision  = P_QUOTE_REVISION;
        l_cur_val             cur_create_task%rowtype;
        l_url                 VARCHAR2(1000);
        l_method              VARCHAR2(20);
        l_http_version        VARCHAR2(20);
        l_content_type        VARCHAR2(20);
        l_soapaction          VARCHAR2(1000);
        l_soap_request        VARCHAR2(32767);
        l_soap_response       VARCHAR2(32767);
        l_http_request        utl_http.req;
        l_http_response       utl_http.resp;
        l_action              VARCHAR2(4000) := '';
        l_opp_number          CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE;
        l_opp_id              CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE;
        l_quote_id            CCT_QUOTE.QUOTE_ID%TYPE;
        l_quote_revision      CCT_QUOTE_REVISIONS.QUOTE_REVISION%TYPE;
        l_queue_name          CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
        l_created_by          VARCHAR2(50);
        l_role                VARCHAR2(50);
        l_task_creation_flag  VARCHAR2(10) := 'TRUE';
        l_task_type           VARCHAR2(10) := 'NEW';
        l_task_title          VARCHAR2(50) := 'MANAGEPRICING';
        l_task_count          NUMBER;
        l_task_status         VARCHAR2(50) := 'ASSIGNED';
        l_attribute_4         VARCHAR2(50) := 'BeaconUploadProcess';
        l_role_str_pricing    VARCHAR2(50) := 'scCCT_Strategic_Pricing'; -- To put the validation to avoid the task creation for group Strategic Pricing
        resp                  XMLTYPE;
        l_clob_val            VARCHAR2(500);
    BEGIN
        initialize;
        OPEN cur_create_task;
        LOOP
          FETCH cur_create_task INTO l_cur_val;
          EXIT WHEN cur_create_task%notfound;
          l_opp_number     := l_cur_val.opportunity_number;
          l_opp_id         := l_cur_val.osc_opportunity_id;
          l_quote_id       := l_cur_val.quote_id;
          l_quote_revision := l_cur_val.quote_revision;
          l_queue_name     := l_cur_val.queue_name;
          BEGIN
            IF upper(l_cur_val.queue_name) = g_queue_commodity THEN
              SELECT user_group
                INTO l_role
                FROM cct_role_properties
               WHERE upper(user_group_id) = g_role_advance_sourcing;
            ELSIF upper(l_cur_val.queue_name) = g_queue_advance_sourcing THEN
              SELECT user_group
                INTO l_role
                FROM cct_role_properties
               WHERE upper(user_group_id) = g_role_advance_sourcing;
            ELSIF upper(l_cur_val.queue_name) = g_queue_hpp THEN
              SELECT user_group
                INTO l_role
                FROM cct_role_properties
               WHERE upper(user_group_id) = g_role_hpp;
            ELSIF upper(l_cur_val.queue_name) = g_queue_lighting THEN
              SELECT user_group
                INTO l_role
                FROM cct_role_properties
               WHERE upper(user_group_id) = g_role_lighting;
            -- 12/3
            ELSIF upper(l_cur_val.queue_name) = g_queue_electrical THEN
              SELECT user_group
                INTO l_role
                FROM cct_role_properties
               WHERE upper(user_group_id) = g_role_electrical;
            ELSIF upper(l_cur_val.queue_name) = g_queue_kitting THEN
              SELECT user_group
                INTO l_role
                FROM cct_role_properties
               WHERE upper(user_group_id) = g_role_kitting;
            ELSIF upper(l_cur_val.queue_name) = g_queue_strategic_pricing THEN
              SELECT user_group
                INTO l_role
                FROM cct_role_properties
               WHERE upper(user_group_id) = g_role_strategic_pricing;
            ELSIF upper(l_cur_val.queue_name) = g_queue_chemical THEN
              SELECT user_group
                INTO l_role
                FROM cct_role_properties
               WHERE upper(user_group_id) = g_role_checmical;
            ELSIF upper(l_cur_val.queue_name) = g_queue_unknown THEN
              SELECT user_group
                INTO l_role
                FROM cct_role_properties
               WHERE upper(user_group_id) = g_role_unknown;
            ELSIF upper(l_cur_val.queue_name) = g_queue_no_bid THEN
              SELECT user_group
                INTO l_role
                FROM cct_role_properties
               WHERE upper(user_group_id) = g_role_strategic_pricing;
            END IF;
        EXCEPTION
        WHEN OTHERS THEN
            l_role := NULL;
        END;

        cct_create_errors.record_tasks( 
            P_QUOTE_NUMBER => P_QUOTE_NUMBER, 
            P_QUOTE_REVISION => l_quote_revision, 
            P_OPPORTUNITY_NUMBER => l_opp_number, 
            P_OPPORTUNITY_ID => l_opp_id, 
            P_QUOTE_ID => l_quote_id, 
            P_ROLE_NAME => l_role, 
            P_TASK_COMMENTS => 'Before calling web service', 
            P_TASK_STATUS => NULL, 
            P_TASK_TITLE => l_task_title, 
            P_CREATED_BY => sys_context('USERENV','OS_USER'));

        IF upper(l_role) <> upper(l_role_str_pricing) THEN
            IF (l_opp_number IS NOT NULL) AND (l_quote_id IS NOT NULL) AND (l_quote_revision IS NOT NULL) AND (l_queue_name IS NOT NULL) AND (l_role IS NOT NULL) AND (l_task_creation_flag IS NOT NULL) AND (l_task_type IS NOT NULL) AND (l_task_title IS NOT NULL) THEN
               -- dbms_output.put_line('HI');
                  cct_properties_pkg.get_soap_param( 'TASK_URL', l_url );
                  cct_properties_pkg.get_soap_param( 'TASK_METHOD', l_method );
                  cct_properties_pkg.get_soap_param( 'TASK_HTTP_VERSION', l_http_version );
                  l_http_request := utl_http.begin_request( l_url, l_method, l_http_version );
                  cct_properties_pkg.get_soap_param( 'TASK_SOAP_REQUEST', l_soap_request );
                  --update soap request with fetched values
                  l_soap_request := REPLACE( l_soap_request, '$OPPNO$', l_cur_val.opportunity_number );
                  l_soap_request := REPLACE( l_soap_request, '$OPPID$', l_cur_val.osc_opportunity_id );
                  l_soap_request := REPLACE( l_soap_request, '$QUOTE$', l_cur_val.quote_id );
                  l_soap_request := REPLACE( l_soap_request, '$QREV$', l_cur_val.quote_revision );
                  l_soap_request := REPLACE( l_soap_request, '$ROLE$', l_role );
                  l_soap_request := REPLACE( l_soap_request, '$FLAG$', l_task_creation_flag );
                  l_soap_request := REPLACE( l_soap_request, '$TYPE$', l_task_type );
                  l_soap_request := REPLACE( l_soap_request, '$TITLE$', l_task_title );
                  l_soap_request := REPLACE( l_soap_request, '$ATTRIBUTE4$', l_attribute_4 );
                  cct_properties_pkg.get_soap_param( 'TASK_CONTENT_TYPE', l_content_type );
                  cct_properties_pkg.get_soap_param( 'TASK_SOAP_ACTION', l_soapaction );
                  utl_http.set_header( l_http_request, 'Content-Type', l_content_type );
                  utl_http.set_header( l_http_request, 'Content-Length', LENGTH(l_soap_request) );
                  utl_http.set_header( l_http_request, 'SOAPAction', l_soapaction );
                  utl_http.write_text( l_http_request, l_soap_request );
                  l_http_response                    := utl_http.get_response(l_http_request);
                  IF upper(l_role) <> upper(l_role_str_pricing) THEN
                    cct_create_errors.record_tasks( 
                        P_QUOTE_NUMBER => P_QUOTE_NUMBER, 
                        P_QUOTE_REVISION => l_quote_revision, 
                        P_OPPORTUNITY_NUMBER => l_opp_number, 
                        P_OPPORTUNITY_ID => l_opp_id, 
                        P_QUOTE_ID => l_quote_id, 
                        P_ROLE_NAME => l_role, 
                        P_TASK_COMMENTS => 'Response Received', 
                        P_TASK_STATUS => NULL, 
                        P_TASK_TITLE => l_task_title, 
                        P_CREATED_BY => sys_context('USERENV','OS_USER') );
                  END IF;
                  BEGIN
                    utl_http.read_text(l_http_response,l_soap_response );
                    resp       := xmltype.createxml(l_soap_response);
                    resp       := resp.extract('/soap:Envelope/soap:Body/child::node()', 'xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"' );
                    resp       := resp.extract('n:Status/n:Status/text()', 'xmlns:n="http://www.klx.com/cct/task/V1.0"' );
                    l_clob_val := resp.getclobval ();
                  EXCEPTION
                  WHEN utl_http.too_many_requests THEN
                    utl_http.end_response(l_http_response);
                    g_msg  := SUBSTR(utl_http.get_detailed_sqlerrm, 1, 500 );
                    g_code := SQLCODE;
                    cct_create_errors.record_error( 
                        P_OPPORTUNITY_ID => l_opp_id, 
                        P_QUOTE_REVISION => l_quote_revision, 
                        P_QUOTE_NUMBER => P_QUOTE_NUMBER, 
                        P_ERROR_MESSAGE => g_msg, 
                        P_ERROR_TYPE => g_code, 
                        P_MODULE_NAME => 'CCT_CREATE_QUEUE_TASKS.create_queue_task', 
                        P_CREATED_BY => sys_context('USERENV','OS_USER'));
                  END;
                  utl_http.end_response(l_http_response);
            END IF;
        END IF;
        IF upper(l_role) <> upper(l_role_str_pricing) THEN
            cct_create_errors.record_tasks( 
                P_QUOTE_NUMBER => P_QUOTE_NUMBER, 
                P_QUOTE_REVISION => l_quote_revision, 
                P_OPPORTUNITY_NUMBER => l_opp_number, 
                P_OPPORTUNITY_ID => l_opp_id, 
                P_QUOTE_ID => l_quote_id, 
                P_ROLE_NAME => l_role, 
                P_TASK_COMMENTS => l_clob_val, 
                P_TASK_STATUS => NULL, 
                P_TASK_TITLE => l_task_title, 
                P_CREATED_BY => sys_context('USERENV','OS_USER'));
        END IF;
        IF l_clob_val = 'SUCCESS' THEN --AND upper(l_role) <> upper(g_role_strategic_pricing) THEN
            cct_create_errors.record_tasks( 
                P_QUOTE_NUMBER => P_QUOTE_NUMBER, 
                P_QUOTE_REVISION => l_quote_revision, 
                P_OPPORTUNITY_NUMBER => l_opp_number, 
                P_OPPORTUNITY_ID => l_opp_id, 
                P_QUOTE_ID => l_quote_id, 
                P_ROLE_NAME => l_role, 
                P_TASK_COMMENTS => ('Records uploaded from ADF: (Quote Number: ' || P_QUOTE_NUMBER || ' ,Quote Revision: ' || l_quote_revision || ' ,Opportunity Number: ' || l_opp_number || ' ,Opportunity ID: ' || l_opp_id || ' ,Quote ID: ' || l_quote_id || ' ,Role: ' || l_role || ' ,Task Title: ' || l_task_title || ' ,Created By: ' || sys_context('USERENV','OS_USER') || ' ,Creation Date: ' || TO_CHAR( SYSDATE, 'dd-mon-yyyy hh24:mi:ss' ) || ')'), 
                P_TASK_STATUS => 'SUCCESS', 
                P_TASK_TITLE => l_task_title, 
                P_CREATED_BY => sys_context('USERENV','OS_USER'));
        ELSIF l_clob_val = 'FAILURE' AND UPPER(l_role) <> UPPER(l_role_str_pricing) THEN
            cct_create_errors.record_tasks( 
                P_QUOTE_NUMBER => P_QUOTE_NUMBER, 
                P_QUOTE_REVISION => l_quote_revision, 
                P_OPPORTUNITY_NUMBER => l_opp_number, 
                P_OPPORTUNITY_ID => l_opp_id, 
                P_QUOTE_ID => l_quote_id, 
                P_ROLE_NAME => l_role, 
                P_TASK_COMMENTS => ('Records uploaded from ADF: (Quote Number: ' || P_QUOTE_NUMBER || ' ,Quote Revision: ' || l_quote_revision || ' ,Opportunity Number: ' || l_opp_number || ' ,Opportunity ID: ' || l_opp_id || ' ,Quote ID: ' || l_quote_id || ' ,Role: ' || l_role || ' ,Task Title: ' || l_task_title || ' ,Created By: ' || sys_context('USERENV','OS_USER') || ' ,Creation Date: ' || TO_CHAR( SYSDATE, 'dd-mon-yyyy hh24:mi:ss' ) || ')'), 
                P_TASK_STATUS => 'FAILURE', 
                P_TASK_TITLE => l_task_title, 
                P_CREATED_BY => sys_context('USERENV','OS_USER'));
        ELSIF l_clob_val IS NULL AND upper(l_role) <> upper(l_role_str_pricing) THEN
            cct_create_errors.record_tasks( 
                P_QUOTE_NUMBER => P_QUOTE_NUMBER, 
                P_QUOTE_REVISION => l_quote_revision, 
                P_OPPORTUNITY_NUMBER => l_opp_number, 
                P_OPPORTUNITY_ID => l_opp_id, 
                P_QUOTE_ID => l_quote_id, 
                P_ROLE_NAME => l_role, 
                P_TASK_COMMENTS => 'There is no response from web service', 
                P_TASK_STATUS => 'FAILURE', 
                P_TASK_TITLE => l_task_title, 
                P_CREATED_BY => sys_context('USERENV','OS_USER'));
        END IF;
        END LOOP;
        CLOSE cur_create_task;
    P_RETURN_STATUS := 'SUCCESS';
    EXCEPTION
    WHEN utl_http.request_failed THEN
        g_msg  := SUBSTR( sqlerrm, 1, 500 );
        g_code := SQLCODE;
        cct_create_errors.record_error( 
            P_OPPORTUNITY_ID => l_opp_id, 
            P_QUOTE_REVISION => l_quote_revision, 
            P_QUOTE_NUMBER => P_QUOTE_NUMBER, 
            P_ERROR_MESSAGE => g_msg, 
            P_ERROR_TYPE => g_code, 
            P_MODULE_NAME => 'CCT_CREATE_QUEUE_TASKS.create_queue_task', 
            P_CREATED_BY => sys_context('USERENV','OS_USER'));
    WHEN utl_http.too_many_requests THEN
        utl_http.end_response(l_http_response);
        P_RETURN_STATUS := 'FAILURE';
    END create_queue_task;

END cct_create_queue_tasks;
/
