create or replace PACKAGE cct_create_opportunity
AS
/**Handles CCT_OPPORTUNITY_TBL loading
  Modification History
  PERSON        DATE         COMMENTS
  ------------  -----------  ---------------------------------------------
  Sureka        06-Jul-2017  Created insert_opportunity_details proc
  Sureka        13-Jul-2017  Modified insert_opportunity_details proc
  Sureka        13-Jul-2017  Created update_opportunity_details proc
  Ankit Bhatt   28-Jul-2017  Added  quote_revision_id and quote_revision out parameters
                             in create_quote_revisions procedure.
  Ankit Bhatt   08-Aug-2017  Created new procedure calculate_quote_parameters
  Ankit Bhatt   16-Aug-2017  Added additional out paramters to calculate_quote_parameters procedure
  Sureka        22-Aug-2017  Modified insert_opportunity_details proc
  Sureka        22-Aug-2017  Modified update_opportunity_details proc
  Ankit Bhatt   01-Sep-2017  New procedure to update Net margin and Amount fields
  Ankit Bhatt   02-Sep-2017  Modified insert_opportunity_details for inserting the records into
                             cct_program_cost_header and cct_program_cost_lines
  Ankit Bhatt   27-Sep-2017  modified insert_opportunity_details procedure to insert category_subtype_id
                             into CCT_PROGRAM_COST_LINES table
  Ankit Bhatt   10-Oct-2017  Modified date format
  Sureka        17-Oct-2017  Modified insert_opportunity_details proc
  Sureka        17-Oct-2017  Modified update_opportunity_details proc
  Ankit Bhatt   17-Oct-2017  Modified insert_opportunity_details proc. to populate category_subtype_name
                             into CCT_PROGRAM_COST_LINES table
  Ankit Bhatt   06-Nov-2017  Modified procedure update_formula_fields with cct_quote_parts_final
  Sureka        17-Oct-2017  Modified insert_opportunity_details proc
  Sureka        07-Dec-2017  Created submit_to_sales
  Ankit         28-Dec-2017  Modified total adjustment calculations in procedure update_formula_fields
  Sureka        04-Jan-2018  Modified insert_opportunity_details proc
  Sureka        05-Jan-2018  Created copy_to_opportunity proc
  Sureka        16-Jan-2018  Created map_to_opportunity proc
  Sureka        16-Jan-2018  Created update_to_opportunity proc
  Sureka        14-Feb-2017  Modified update_opportunity_details proc for customer mismatch
**/
    g_osc_opp_id                    CCT_OPPORTUNITY.OSC_OPPORTUNITY_ID%TYPE;
    g_created_by                    CCT_OPPORTUNITY.CREATED_BY%TYPE;
    g_opportunity_number            CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE;
    g_opportunity_id                CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE;
    g_code                          VARCHAR2(50);
    g_msg                           VARCHAR2(500);
    g_count                         NUMBER;

    PROCEDURE insert_opportunity_details(
        P_OPP_OBJ                IN CCT_OPPORTUNITY_TYPE,
        P_OPPORTUNITY_TYPE       IN VARCHAR2,
        P_RETURN_STATUS         OUT NOCOPY VARCHAR2,
        P_CUST_DUE_DATE_FLAG    OUT NOCOPY VARCHAR2,
        P_OLD_CUST_DUE_DATE     OUT NOCOPY DATE);

    PROCEDURE update_opportunity_details(
        P_OPP_OBJ                IN CCT_OPPORTUNITY_TYPE,
        P_RETURN_STATUS         OUT NOCOPY VARCHAR2);

    PROCEDURE calculate_quote_parameters(
        P_OPP_NUMBER             IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE,
        P_NET_MARGIN            OUT VARCHAR2,
        P_AMOUNT                OUT CCT_OPPORTUNITY.AMOUNT%TYPE,
        P_PAYMENT_TERMS         OUT CCT_OPPORTUNITY.PAYMENT_TERMS%TYPE,
        P_OTHER_PAYMENT_TERMS   OUT CCT_OPPORTUNITY.OTHER_PAYMENT_TERMS%TYPE,
        P_DEAL_STRUCTURE        OUT CCT_OPPORTUNITY.DEAL_STRUCTURE%TYPE);

    PROCEDURE update_formula_fields(
        P_OPP_ID                 IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE);

    PROCEDURE submit_to_sales(
        P_OPP_ID                 IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE);

    PROCEDURE copy_to_opportunity(
        P_CRF_NO                 IN CCT_CRF.CRF_NO%TYPE);

    PROCEDURE map_to_opportunity(
        P_CRF_NO                 IN CCT_CRF.CRF_NO%TYPE,
        P_OPP_ID                 IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE,
        P_OPP_OBJ               OUT CCT_OPPORTUNITY_TYPE);

    PROCEDURE update_to_opportunity(
        P_CRF_NO                 IN CCT_CRF.CRF_NO%TYPE);

END cct_create_opportunity;