create or replace PACKAGE cct_quote_pricing AS
/**History 
   Author        Date         COMMENTS
   ------------  -----------  -----------------------------------------------
   Ankit Bhatt   14-Jul-2017  total_cost function created
   Ankit Bhatt   14-Jul-2017  total_resale function created
   Sureka        14-Sep-2017  class_margin function created
**/
/*FUNCTION total_cost(
        P_QUOTE_REVISION_ID   CCT_QUOTE_PARTS.QUOTE_REVISION_ID%type) 
    RETURN NUMBER;

    FUNCTION total_resale(
        P_QUOTE_REVISION_ID   CCT_QUOTE_PARTS.QUOTE_REVISION_ID%TYPE) 
    RETURN NUMBER;
 */
    FUNCTION class_margin (
        p_quote_revision_id   cct_quote_parts.quote_revision_id%TYPE,
        p_calc_abc            cct_quote_parts.calc_abc%TYPE
    ) RETURN NUMBER;

END cct_quote_pricing;