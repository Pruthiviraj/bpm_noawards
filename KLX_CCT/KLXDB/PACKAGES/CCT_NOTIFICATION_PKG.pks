create or replace PACKAGE CCT_NOTIFICATION_PKG AS
/**Handles Email Notification loading
   Modification History
   PERSON        DATE           COMMENTS
   ------------  -----------    ---------------------------------------------
   Sureka        27-Jun-2017    Created load_notification_temp
   Sureka        20-Jul-2017    Created get_mail_body
   Sureka        04-Aug-2017    Created get_mail_details
   Sureka        15-Aug-2017    Created send_daily_notifications
   Ankit Bhatt   10-Oct-2017    Modified date format
   Sureka        17-Oct-2017    Modified get_mail_body
   Sureka        01-Dec-2017    Created get_to_details
   Sureka        01-Dec-2017    Created get_cc_details
   Sureka        16-Feb-2018    Modified get_mail_details for Notification 19
**/
    g_code                      VARCHAR2(50);
    g_msg                       VARCHAR2(500);

    FUNCTION load_notification_temp
        RETURN VARCHAR2;

    PROCEDURE get_mail_details(
        P_OPP_NUM            IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE,
        P_NOTIF_TEMPLATE_ID  IN CCT_NOTIFICATION_TYPE.NOTIF_TEMPLATE_ID%TYPE,
        P_SOURCE             IN CCT_NOTIFICATION_REQUEST.SOURCE%TYPE,
        P_MESSAGE_ID         IN CCT_NOTIFICATION_REQUEST.MESSAGE_ID%TYPE,
        P_ATTACHMENT_FLAG    IN CCT_NOTIFICATION_REQUEST.ATTACHMENT_FLAG%TYPE,
        P_OPP_CLOSURE_STATUS IN CCT_NOTIFICATION_REQUEST.OPP_CLOSURE_STATUS%TYPE,
        P_ROLE_NAME          IN CCT_NOTIFICATION_REQUEST.ROLE_NAME%TYPE,
        P_ATTRIBUTE1         IN CCT_NOTIFICATION_REQUEST.ATTRIBUTE1%TYPE,
        P_ATTRIBUTE2         IN CCT_NOTIFICATION_REQUEST.ATTRIBUTE2%TYPE,
        P_ATTRIBUTE3         IN CCT_NOTIFICATION_REQUEST.ATTRIBUTE3%TYPE,
        P_ATTRIBUTE4         IN CCT_NOTIFICATION_REQUEST.ATTRIBUTE4%TYPE,
        P_ACTION             IN CCT_NOTIFICATION_REQUEST.ACTION%TYPE,
        P_SUBJECT        IN OUT CCT_NOTIFICATION_REQUEST.SUBJECT%TYPE,
        P_TO_LIST        IN OUT CCT_NOTIFICATION_REQUEST.TO_LIST%TYPE,
        P_CC_LIST        IN OUT CCT_NOTIFICATION_REQUEST.CC_LIST%TYPE,
        P_BCC_LIST       IN OUT CCT_NOTIFICATION_REQUEST.BCC_LIST%TYPE,
        P_TO_OSC_LIST    IN OUT CCT_NOTIFICATION_REQUEST.TO_OSC_LIST%TYPE,
        P_CC_OSC_LIST    IN OUT CCT_NOTIFICATION_REQUEST.CC_OSC_LIST%TYPE,
        P_BODY           IN OUT CCT_NOTIFICATION_REQUEST.BODY%TYPE,
        P_REQUEST_ID        OUT CCT_NOTIFICATION_REQUEST.NOTIFICATION_REQ_ID%TYPE,
        P_RETURN_STATUS     OUT VARCHAR2);

    PROCEDURE get_mail_body(
        P_REQUEST_ID         IN CCT_NOTIFICATION_REQUEST.NOTIFICATION_REQ_ID%TYPE,
        P_OPP_NUM            IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE,
        P_NOTIF_TEMPLATE_ID  IN CCT_NOTIFICATION_TYPE.NOTIF_TEMPLATE_ID%TYPE,
        P_SQL_QUERY          IN CLOB,
        P_BODY              OUT CLOB);

    PROCEDURE get_to_details(
        P_REQUEST_ID         IN CCT_NOTIFICATION_REQUEST.NOTIFICATION_REQ_ID%TYPE,
        P_NOTIF_TEMPLATE_ID  IN CCT_NOTIFICATION_TYPE.NOTIF_TEMPLATE_ID%TYPE,
        P_TO_LIST           OUT CLOB,
        P_TO_OSC_LIST       OUT CLOB);

    PROCEDURE get_cc_details(
        P_REQUEST_ID         IN CCT_NOTIFICATION_REQUEST.NOTIFICATION_REQ_ID%TYPE,
        P_NOTIF_TEMPLATE_ID  IN CCT_NOTIFICATION_TYPE.NOTIF_TEMPLATE_ID%TYPE,
        P_CC_LIST           OUT CLOB,
        P_CC_OSC_LIST       OUT CLOB);

END CCT_NOTIFICATION_PKG;