create or replace PACKAGE cct_approval_process
AS
/**Handles CCT_APPROVAL_RULES table data
   Modification History
   PERSON          DATE           COMMENTS
   ------------    -----------    ---------------------------------------------
   Sureka          14-Aug-2017    Created create_rule_version proc
   Sureka          14-Aug-2017    Created create_rule proc
   Sureka          15-Aug-2017    Created get_approval_user proc
   Sureka          25-Aug-2017    Created create_snapshot proc
   Sureka          27-Aug-2017    Modified get_approval_user proc
   Sureka          27-Aug-2017    Created insert_approval_user proc
   Sureka          28-Aug-2017    Created fetch_snapshots proc
   Sureka          31-Aug-2017    Created create_quote_approvals proc
   Sureka          31-Aug-2017    Created insert_approval_snapshots proc
   Sureka          31-Aug-2017    Modified fetch_snapshots proc
   Sureka          20-Sep-2017    Created insert_dummy_snapshot proc
   Sureka          26-Sep-2017    Created update_approval_status proc
   Sureka          28-Sep-2017    Modified create_snapshot proc
   Ankit Bhatt     10-Oct-2017    Modified date format
   Sureka          16-Oct-2017    Modified insert_snapshots proc
   Sureka          16-Oct-2017    Modified insert_approval_snapshots proc
   Sureka          17-Oct-2017    Modified get_approval_user proc
   Sureka          01-Dec-2017    Modified get_approval_user proc
   Sureka          01-Dec-2017    Modified fetch_snapshots proc
   Sureka          12-Jan-2018    Created update_snapshot_master proc
**/
    g_code                        VARCHAR2(50);
    g_msg                         VARCHAR2(500);
    g_approval_status             CCT_QUOTE_APPROVALS.APPROVAL_STATUS%TYPE := 'IN PROGRESS';
    --TYPE APPROVAL_CUR           IS REF CURSOR;

    PROCEDURE create_rule_version(
        P_START_DATE           IN CCT_APPROVAL_RULE_VERSIONS.START_DATE%TYPE,
        P_END_DATE             IN CCT_APPROVAL_RULE_VERSIONS.END_DATE%TYPE,
        P_RETURN_STATUS       OUT NVARCHAR2);

    PROCEDURE create_rule(
        P_RULE_VERSION         IN CCT_APPROVAL_RULES.RULE_VERSION%TYPE,
        P_RULE_SET_NAME        IN CCT_APPROVAL_RULES.RULE_SET_NAME%TYPE,
        P_GROUP_IDENTIFIER     IN CCT_APPROVAL_RULES.GROUP_IDENTIFIER%TYPE,
        P_RULE_NAME            IN CCT_APPROVAL_RULES.RULE_NAME%TYPE,
        P_TABLE_NAME           IN CCT_APPROVAL_RULES.TABLE_NAME%TYPE DEFAULT NULL,
        P_FIELD_NAME           IN CCT_APPROVAL_RULES.FIELD_NAME%TYPE,
        P_RULE_CONDITION       IN CCT_APPROVAL_RULES.RULE_CONDITION%TYPE,
        P_RULE_VALUE           IN CCT_APPROVAL_RULES.RULE_VALUE%TYPE,
        P_JOIN_CONDITION       IN CCT_APPROVAL_RULES.JOIN_CONDITION%TYPE DEFAULT NULL,
        P_APPROVAL_ROLE        IN CCT_APPROVAL_RULES.APPROVAL_ROLE%TYPE DEFAULT NULL,
        P_USER_NAME            IN CCT_APPROVAL_RULES.USER_NAME%TYPE DEFAULT NULL,
        P_RULE_STATUS          IN CCT_APPROVAL_RULES.RULE_STATUS%TYPE DEFAULT 'ACTIVE',
        P_RETURN_STATUS       OUT NVARCHAR2);

    PROCEDURE get_approval_user(
        P_APPROVAL_TYPE        IN CCT_APPROVAL_TYPE,
        P_APPROVAL_USER_CUR   OUT SYS_REFCURSOR,
        P_APPROVAL_ID         OUT CCT_APPROVAL_REQUEST.APPROVAL_ID%TYPE);

    PROCEDURE create_quote_approvals(
        P_OPP_ID               IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE,
        P_APPROVAL_ID          IN CCT_APPROVAL_REQUEST.APPROVAL_ID%TYPE,
        P_SNAPSHOT_ID          IN CCT_APPROVAL_REQUEST.SNAPSHOT_ID%TYPE,
        P_COMMENTS             IN CCT_APPROVAL_REQUEST.COMMENTS%TYPE);

    PROCEDURE insert_approval_user(
        P_APPROVAL_RULE        IN CCT_APPROVAL_RULES.RULE_NAME%TYPE,
        P_APPROVAL_USER        IN CCT_APPROVAL_RULES.USER_NAME%TYPE,
        P_APPROVAL_ROLE        IN CCT_APPROVAL_RULES.APPROVAL_ROLE%TYPE);

    PROCEDURE update_approval_status(
        P_APPROVAL_ID          IN CCT_QUOTE_APPROVALS.APPROVAL_ID%TYPE DEFAULT NULL,
        P_SNAPSHOT_ID          IN CCT_QUOTE_APPROVALS.SNAPSHOT_ID%TYPE,
        P_APPROVAL_STATUS      IN CCT_QUOTE_APPROVALS.APPROVAL_STATUS%TYPE);

    PROCEDURE create_snapshot(
        P_OPP_NUM              IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE,
        P_SNAPSHOT_TYPE        IN CCT_SNAPSHOT.SNAPSHOT_TYPE%TYPE);

    PROCEDURE fetch_snapshots(
        P_OPP_ID               IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE,
        P_ROLE_TYPE            IN NVARCHAR2 DEFAULT NULL,
        P_SNAPSHOT_CUR        OUT SYS_REFCURSOR);

    PROCEDURE insert_snapshots(
        P_OPP_ID               IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE);

    PROCEDURE insert_approval_snapshots(
        P_OPP_ID               IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE);

    PROCEDURE insert_dummy_snapshot(
        P_OPP_ID               IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE);

    PROCEDURE update_snapshot_master(
        P_OPP_ID               IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE);

END cct_approval_process;