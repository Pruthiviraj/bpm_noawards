create or replace PACKAGE BODY cct_cost_comparison 
AS 
/**Handles CCT_APPROVAL_RULES table data
   Modification History 
   PERSON        DATE           COMMENTS
   ------------  -----------    ---------------------------------------------
   Sureka        28-Sep-2017    Created insert_cost_header proc
   Sureka        28-Sep-2017    Created setup_snapshot_header proc
   Sureka        28-Sep-2017    Created setup_current_header proc
   Sureka        28-Sep-2017    Created calc_delta proc
   Sureka        29-Sep-2017    Created calc_additional_changes proc
   Sureka        30-Sep-2017    Created insert_cost_lines proc
   Sureka        06-Oct-2017    Modified insert_cost_lines proc
   Sureka        16-Oct-2017    Modified calc_delta proc
   Sureka        06-Nov-2017    Created insert_approvals_header
   Sureka        06-Nov-2017    Modified insert_cost_header
   Sureka        06-Nov-2017    Modified setup_snapshot_header
   Sureka        06-Nov-2017    Modified setup_current_header
   Sureka        06-Nov-2017    Modified calc_delta
   Sureka        20-Nov-2017    Modified calc_additional_changes to include 
                                suggested columns
   Ankit Bhatt   08-Dec-2017    Modified insert_cost_line proc. to handle 'MISMATCHING QUEUE'                             
   Ankit Bhatt   12-Dec-2017    Modified proc. calc_additional_changes,insert_cost_lines 
                                to add new field suggested Total Qty Len Contract
   Ankit Bhatt   28-Sep-2018    Call new proc. cct_fetch_fields.get_latest_approved_snapshot 
                                into insert_approvals_header proc.                                
**/

    PROCEDURE insert_cost_header(
        P_OPP_NUM            IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE)
    AS 
        l_opp_id                CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE;
        l_snapshot_id           CCT_SNAPSHOT.SNAPSHOT_ID%TYPE;
        l_quote_revision_id     CCT_QUOTE_REVISIONS.REVISION_ID%TYPE;
        l_snapshot_quote_rev    CCT_SNAPSHOT.QUOTE_REVISION_ID%TYPE;
        l_count                 NUMBER;
    BEGIN

        CCT_FETCH_FIELDS.get_opty_id(
                P_OPPORTUNITY_NUMBER => P_OPP_NUM,
                P_OPPORTUNITY_ID     => l_opp_id);
        -- dbms_output.put_line(l_opp_id);        
        CCT_FETCH_FIELDS.get_latest_snapshot(
                P_OPPORTUNITY_NUMBER => P_OPP_NUM,
                P_OPPORTUNITY_ID     => l_opp_id,
                P_SNAPSHOT_ID        => l_snapshot_id);
        -- dbms_output.put_line(l_snapshot_id);                
        CCT_FETCH_FIELDS.get_latest_revision( 
                P_OPPORTUNITY_NUMBER => P_OPP_NUM,
                P_OPPORTUNITY_ID     => l_opp_id,
                P_REVISION_ID        => l_quote_revision_id);
        -- dbms_output.put_line(l_quote_revision_id);                
        IF l_snapshot_id <> 0 THEN 
            SELECT quote_revision_id 
              INTO l_snapshot_quote_rev 
              FROM cct_snapshot 
             WHERE snapshot_id = l_snapshot_id;
        ELSE 
            l_snapshot_quote_rev := 0;
        END IF;

        SELECT COUNT(*) 
          INTO l_count 
          FROM cct_cost_compare_header 
         WHERE opportunity_id = l_opp_id; 

        IF l_count=0 THEN 
            setup_snapshot_header(l_opp_id,l_snapshot_id,l_snapshot_quote_rev,'COST');
            setup_current_header(l_opp_id,l_quote_revision_id,'COST');
            calc_delta(l_opp_id,'INSERT','COST');
        ELSE 
            setup_snapshot_header(l_opp_id,l_snapshot_id,l_snapshot_quote_rev,'COST');
            setup_current_header(l_opp_id,l_quote_revision_id,'COST');
            calc_delta(l_opp_id,'UPDATE','COST');
        END IF;
    EXCEPTION
    WHEN OTHERS THEN
        g_code                 := sqlcode;
        g_msg                  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            p_opportunity_id   => l_opp_id,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_COST_COMPARISON.insert_cost_header',
            p_created_by       => sys_context('USERENV','OS_USER')
        );
    END insert_cost_header;

    PROCEDURE insert_approvals_header(
        P_OPP_NUM            IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE)
    AS 
        l_opp_id                CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE;
        l_approved_snapshot_id  CCT_SNAPSHOT.SNAPSHOT_ID%TYPE;
        l_approval_snapshot_id  CCT_SNAPSHOT.SNAPSHOT_ID%TYPE;
        l_quote_revision_id     CCT_QUOTE_REVISIONS.REVISION_ID%TYPE;
        l_snapshot_all_rev      CCT_SNAPSHOT.QUOTE_REVISION_ID%TYPE;
        l_snapshot_app_rev      CCT_SNAPSHOT.QUOTE_REVISION_ID%TYPE;
        l_app_count             NUMBER;
        l_all_count             NUMBER;
    BEGIN

        CCT_FETCH_FIELDS.get_opty_id(
                P_OPPORTUNITY_NUMBER => P_OPP_NUM,
                P_OPPORTUNITY_ID     => l_opp_id);
        -- dbms_output.put_line(l_opp_id); 

       /*** calling new proc. cct_fetch_fields.get_latest_approved_snapshot ***/
          --GET approved snapshot
           CCT_FETCH_FIELDS.get_latest_approved_snapshot(
                P_OPPORTUNITY_NUMBER => P_OPP_NUM,
                P_OPPORTUNITY_ID     => l_opp_id,
                P_SNAPSHOT_TYPE      => 'APPROVED',
                P_SNAPSHOT_ID        => l_approved_snapshot_id); 
        --GET approved snapshot
/*        CCT_FETCH_FIELDS.get_latest_snapshot(
                P_OPPORTUNITY_NUMBER => P_OPP_NUM,
                P_OPPORTUNITY_ID     => l_opp_id,
                P_SNAPSHOT_TYPE      => 'APPROVED',
                P_SNAPSHOT_ID        => l_approved_snapshot_id);
       -- dbms_output.put_line('APPROVED ' || l_approved_snapshot_id);          
 */      
       /*** calling new proc. cct_fetch_fields.get_latest_approved_snapshot ***/
        IF l_approved_snapshot_id <> 0 THEN 
            SELECT quote_revision_id 
              INTO l_snapshot_app_rev 
              FROM cct_snapshot 
             WHERE snapshot_id = l_approved_snapshot_id;
        ELSE 
            l_snapshot_app_rev := 0;
        END IF;

        --GET approval snapshot
        CCT_FETCH_FIELDS.get_latest_snapshot(
                P_OPPORTUNITY_NUMBER => P_OPP_NUM,
                P_OPPORTUNITY_ID     => l_opp_id,
                P_SNAPSHOT_TYPE      => 'APPROVALS',
                P_SNAPSHOT_ID        => l_approval_snapshot_id);
        -- dbms_output.put_line('APPROVAL ' || l_approval_snapshot_id);  
        IF l_approval_snapshot_id <> 0 THEN 
            SELECT quote_revision_id 
              INTO l_snapshot_all_rev 
              FROM cct_snapshot 
             WHERE snapshot_id = l_approval_snapshot_id;
        ELSE 
            l_snapshot_all_rev := 0;
        END IF;

        CCT_FETCH_FIELDS.get_latest_revision( 
                P_OPPORTUNITY_NUMBER => P_OPP_NUM,
                P_OPPORTUNITY_ID     => l_opp_id,
                P_REVISION_ID        => l_quote_revision_id);
        -- dbms_output.put_line(l_quote_revision_id);                

        SELECT COUNT(*) 
          INTO l_app_count 
          FROM cct_approved_compare_header  
         WHERE opportunity_id = l_opp_id; 

        SELECT COUNT(*) 
          INTO l_all_count 
          FROM cct_approval_compare_header  
         WHERE opportunity_id = l_opp_id; 

        setup_snapshot_header(l_opp_id,l_approved_snapshot_id,l_snapshot_app_rev,'APPROVED');
        setup_current_header(l_opp_id,l_quote_revision_id,'APPROVED');
        setup_snapshot_header(l_opp_id,l_approval_snapshot_id,l_snapshot_all_rev,'APPROVAL');
        setup_current_header(l_opp_id,l_quote_revision_id,'APPROVAL');   

        IF l_app_count=0 THEN           
            calc_delta(l_opp_id,'INSERT','APPROVED');
        ELSIF l_app_count=3 THEN  
            calc_delta(l_opp_id,'UPDATE','APPROVED');
        END IF;

        IF l_all_count=0 THEN          
            calc_delta(l_opp_id,'INSERT','APPROVAL');
        ELSIF l_all_count=3 THEN  
            calc_delta(l_opp_id,'UPDATE','APPROVAL');
        END IF;

    EXCEPTION
    WHEN OTHERS THEN
        g_code                 := sqlcode;
        g_msg                  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            p_opportunity_id   => l_opp_id,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_COST_COMPARISON.insert_approvals_header',
            p_created_by       => sys_context('USERENV','OS_USER')
        );
    END insert_approvals_header;

    PROCEDURE setup_snapshot_header(
        P_OPP_ID             IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE,
        P_SNAPSHOT_ID        IN CCT_SNAPSHOT.SNAPSHOT_ID%TYPE,
        P_QUOTE_REVISION_ID  IN CCT_SNAPSHOT.QUOTE_REVISION_ID%TYPE,
        P_HEADER_TYPE        IN VARCHAR2)
    AS 
        l_count                 NUMBER;
        l_data_type             CCT_COST_COMPARE_HEADER.DATA_TYPE%TYPE;
        l_total_parts           CCT_COST_COMPARE_HEADER.TOTAL_PARTS%TYPE;
        l_parts_with_cost       CCT_COST_COMPARE_HEADER.PARTS_WITH_COST%TYPE;
        l_parts_quoted          CCT_COST_COMPARE_HEADER.PARTS_QUOTED%TYPE;
        l_parts_not_quoted      CCT_COST_COMPARE_HEADER.PARTS_NOT_QUOTED%TYPE;
        l_total_cost            CCT_COST_COMPARE_HEADER.TOTAL_COST%TYPE;
        l_perc_difference       CCT_COST_COMPARE_HEADER.PERC_DIFFERENCE%TYPE;
        l_additional_changes    CCT_COST_COMPARE_HEADER.ADDITIONAL_CHANGES%TYPE;
        l_total_resale          CCT_APPROVED_COMPARE_HEADER.TOTAL_RESALE%TYPE;
        l_part_margin           CCT_APPROVED_COMPARE_HEADER.PART_MARGIN%TYPE;
        l_net_margin            CCT_APPROVED_COMPARE_HEADER.NET_MARGIN%TYPE;
        l_snapshot_date         VARCHAR2(1000);
    BEGIN
        l_data_type := 'Snapshot';

        IF P_HEADER_TYPE = 'COST' THEN 

            l_additional_changes := 0;

            SELECT COUNT(*) 
              INTO l_count 
              FROM cct_cost_compare_header 
             WHERE opportunity_id = P_OPP_ID AND 
                   data_type like 'Snapshot%'; 

            IF P_SNAPSHOT_ID IS NULL or P_SNAPSHOT_ID = 0 THEN 
                l_total_parts      := 0; 
                l_parts_with_cost  := 0;
                l_parts_quoted     := 0;
                l_parts_not_quoted := 0; 
                l_total_cost       := 0;
                l_perc_difference  := 0;
                l_snapshot_date    := '';
            ELSE 
                SELECT NVL(total_no_of_parts_requested,0),  
                       NVL(total_no_of_parts_with_cost,0), 
                       NVL(total_no_of_parts_quoted,0), 
                       NVL(total_no_of_part_not_quoted,0), 
                       NVL(total_cost,0), 
                       0, 
                       CASE WHEN cs.created_date IS NULL THEN ' ' 
                        ELSE TO_CHAR(cs.created_date,'MM/DD/YYYY') 
                            || ' ' || TO_CHAR(cs.created_Date, 'HH24:MI:SS') 
                        END created_date 
                  INTO l_total_parts, 
                       l_parts_with_cost,
                       l_parts_quoted,
                       l_parts_not_quoted, 
                       l_total_cost, 
                       l_perc_difference, 
                       l_snapshot_date 
                  FROM cct_quote_pricing_ss_v cqv RIGHT JOIN  
                       cct_snapshot cs 
                    ON cs.quote_revision_id = cqv.quote_revision_id AND 
                       cs.snapshot_id = cqv.snapshot_id 
                 WHERE cs.snapshot_id = P_SNAPSHOT_ID;

                l_data_type := 'Snapshot' || ' ' || l_snapshot_date;
            END IF;

            IF l_count = 0 THEN
                INSERT INTO CCT_COST_COMPARE_HEADER(
                    OPPORTUNITY_ID,
                    TYPE_ID,
                    DATA_TYPE,
                    QUOTE_REVISION_ID,
                    TOTAL_PARTS,
                    PARTS_WITH_COST,
                    PARTS_QUOTED,
                    PARTS_NOT_QUOTED,
                    TOTAL_COST,
                    PERC_DIFFERENCE,
                    ADDITIONAL_CHANGES)
                VALUES(
                    P_OPP_ID,
                    1,
                    l_data_type,
                    P_QUOTE_REVISION_ID,
                    l_total_parts,
                    l_parts_with_cost,
                    l_parts_quoted,
                    l_parts_not_quoted,
                    l_total_cost,
                    l_perc_difference,
                    l_additional_changes);
                COMMIT;
            ELSE 
                UPDATE cct_cost_compare_header 
                   SET total_parts = l_total_parts, 
                       parts_with_cost = l_parts_with_cost, 
                       parts_quoted = l_parts_quoted, 
                       parts_not_quoted = l_parts_not_quoted,
                       total_cost = l_total_cost,
                       perc_difference = l_perc_difference, 
                       additional_changes = l_additional_changes, 
                       quote_revision_id = P_QUOTE_REVISION_ID, 
                       data_type = l_data_type 
                 WHERE opportunity_id = P_OPP_ID AND 
                       data_type like 'Snapshot%';  
                COMMIT;
            END IF;      
        ELSE
            IF P_SNAPSHOT_ID IS NULL or P_SNAPSHOT_ID = 0 THEN 
                l_total_parts       := 0; 
                l_parts_with_cost   := 0;
                l_total_resale      := 0; 
                l_total_cost        := 0;
                l_part_margin       := 0;
                l_net_margin        := 0;
                l_snapshot_date    := '';
            ELSE 
                SELECT NVL(total_no_of_parts_requested,0),  
                       NVL(total_no_of_parts_quoted,0), 
                       NVL(total_resale,0), 
                       NVL(total_cost,0), 
                       NVL(parts_margin,0), 
                       NVL(net_margin,0), 
                       CASE WHEN cs.created_date IS NULL THEN ' ' 
                        ELSE TO_CHAR(cs.created_date,'MM/DD/YYYY') 
                            || ' ' || TO_CHAR(cs.created_Date, 'HH24:MI:SS') 
                        END created_date 
                  INTO l_total_parts, 
                       l_parts_with_cost, 
                       l_total_resale, 
                       l_total_cost, 
                       l_part_margin,
                       l_net_margin, 
                       l_snapshot_date 
                  FROM cct_quote_pricing_ss_final_v cqv RIGHT JOIN  
                       cct_snapshot cs 
                    ON cs.quote_revision_id = cqv.quote_revision_id AND 
                       cs.snapshot_id = cqv.snapshot_id 
                 WHERE cs.snapshot_id = P_SNAPSHOT_ID;

                l_data_type := 'Snapshot' || ' ' || l_snapshot_date; 
            END IF;

            IF P_HEADER_TYPE = 'APPROVED' THEN    

                 SELECT COUNT(*) 
                  INTO l_count 
                  FROM cct_approved_compare_header  
                 WHERE opportunity_id = P_OPP_ID AND 
                       data_type like 'Snapshot%'; 

                IF l_count = 0 THEN
                    INSERT INTO CCT_APPROVED_COMPARE_HEADER(
                        OPPORTUNITY_ID, 
                        TYPE_ID,
                        DATA_TYPE,
                        QUOTE_REVISION_ID,
                        TOTAL_PARTS_REQUESTED,
                        TOTAL_PARTS_QUOTED,
                        TOTAL_COST,
                        TOTAL_RESALE,
                        PART_MARGIN,
                        NET_MARGIN)
                    VALUES(
                        P_OPP_ID,
                        1,
                        l_data_type,
                        P_QUOTE_REVISION_ID,
                        l_total_parts,
                        l_parts_with_cost,
                        l_total_cost,
                        l_total_resale,
                        l_part_margin,
                        l_net_margin);
                    COMMIT;
                ELSE 
                    UPDATE cct_approved_compare_header  
                       SET total_parts_requested = l_total_parts, 
                           total_parts_quoted = l_parts_with_cost, 
                           total_cost = l_total_cost,
                           total_resale = l_total_resale,
                           part_margin = l_part_margin, 
                           net_margin = l_net_margin, 
                           quote_revision_id = P_QUOTE_REVISION_ID, 
                           data_type = l_data_type 
                     WHERE opportunity_id = P_OPP_ID AND 
                           data_type like 'Snapshot%';  
                    COMMIT;
                END IF;      

            ELSIF P_HEADER_TYPE = 'APPROVAL' THEN   

                SELECT COUNT(*) 
                  INTO l_count 
                  FROM cct_approval_compare_header  
                 WHERE opportunity_id = P_OPP_ID AND 
                       data_type like 'Snapshot%'; 

               IF l_count = 0 THEN
                    INSERT INTO CCT_APPROVAL_COMPARE_HEADER(
                        OPPORTUNITY_ID, 
                        TYPE_ID,
                        DATA_TYPE,
                        QUOTE_REVISION_ID,
                        TOTAL_PARTS_REQUESTED,
                        TOTAL_PARTS_QUOTED,
                        TOTAL_COST,
                        TOTAL_RESALE,
                        PART_MARGIN,
                        NET_MARGIN)
                    VALUES(
                        P_OPP_ID,
                        1,
                        l_data_type,
                        P_QUOTE_REVISION_ID,
                        l_total_parts,
                        l_parts_with_cost,
                        l_total_cost,
                        l_total_resale,
                        l_part_margin,
                        l_net_margin);
                    COMMIT;
                ELSE 
                    UPDATE cct_approval_compare_header  
                       SET total_parts_requested = l_total_parts, 
                           total_parts_quoted = l_parts_with_cost, 
                           total_cost = l_total_cost,
                           total_resale = l_total_resale,
                           part_margin = l_part_margin, 
                           net_margin = l_net_margin, 
                           quote_revision_id = P_QUOTE_REVISION_ID, 
                           data_type = l_data_type 
                     WHERE opportunity_id = P_OPP_ID AND 
                           data_type like 'Snapshot%'; 
                    COMMIT;
                END IF;      
            END IF;       
        END IF;
    EXCEPTION
    WHEN OTHERS THEN
        g_code                 := sqlcode;
        g_msg                  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            p_opportunity_id   => P_OPP_ID,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_COST_COMPARISON.setup_snapshot_header',
            p_created_by       => sys_context('USERENV','OS_USER')
        );
    END setup_snapshot_header;

    PROCEDURE setup_current_header(
        P_OPP_ID             IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE,
        P_QUOTE_REVISION_ID  IN CCT_QUOTE_PARTS.QUOTE_REVISION_ID%TYPE,
        P_HEADER_TYPE        IN VARCHAR2)
    AS 
        l_count                 NUMBER;
        l_data_type             CCT_COST_COMPARE_HEADER.DATA_TYPE%TYPE;
        l_total_parts           CCT_COST_COMPARE_HEADER.TOTAL_PARTS%TYPE;
        l_parts_with_cost       CCT_COST_COMPARE_HEADER.PARTS_WITH_COST%TYPE;
        l_parts_quoted          CCT_COST_COMPARE_HEADER.PARTS_QUOTED%TYPE;
        l_parts_not_quoted      CCT_COST_COMPARE_HEADER.PARTS_NOT_QUOTED%TYPE;
        l_total_cost            CCT_COST_COMPARE_HEADER.TOTAL_COST%TYPE;
        l_perc_difference       CCT_COST_COMPARE_HEADER.PERC_DIFFERENCE%TYPE;
        l_additional_changes    CCT_COST_COMPARE_HEADER.ADDITIONAL_CHANGES%TYPE;
        l_total_resale          CCT_APPROVED_COMPARE_HEADER.TOTAL_RESALE%TYPE;
        l_part_margin           CCT_APPROVED_COMPARE_HEADER.PART_MARGIN%TYPE;
        l_net_margin            CCT_APPROVED_COMPARE_HEADER.NET_MARGIN%TYPE;
    BEGIN
        l_data_type := 'Current';

		IF P_HEADER_TYPE = 'COST' THEN 

			l_additional_changes := 0;

			SELECT COUNT(*) 
			  INTO l_count 
			  FROM cct_cost_compare_header 
			 WHERE opportunity_id = P_OPP_ID AND 
				   data_type = l_data_type; 

			IF P_QUOTE_REVISION_ID IS NULL OR P_QUOTE_REVISION_ID = 0 THEN 
				l_total_parts      := 0; 
				l_parts_with_cost  := 0;
                l_parts_quoted     := 0;
				l_parts_not_quoted := 0; 
				l_total_cost       := 0;
				l_perc_difference  := 0;
			ELSE             
				SELECT NVL(cqv.total_no_of_parts_requested,0),  
					   NVL(cqv.total_no_of_parts_with_cost,0), 
                       NVL(cqv.total_no_of_parts_quoted,0),
					   NVL(cqv.total_no_of_part_not_quoted,0), 
					   NVL(cqv.total_cost,0), 
					   0 
				  INTO l_total_parts, 
					   l_parts_with_cost,
                       l_parts_quoted,
					   l_parts_not_quoted, 
					   l_total_cost, 
					   l_perc_difference 
				  FROM cct_quote_pricing_v cqv RIGHT JOIN 
					   cct_quote_revisions cqr 
					ON cqv.quote_revision_id = cqr.revision_id 
				 WHERE cqr.revision_id = p_quote_revision_id;
			END IF;

			IF l_count = 0 THEN
				INSERT INTO CCT_COST_COMPARE_HEADER(
					OPPORTUNITY_ID,
					TYPE_ID,
					DATA_TYPE,
					QUOTE_REVISION_ID,
					TOTAL_PARTS,
					PARTS_WITH_COST,
                    PARTS_QUOTED,
					PARTS_NOT_QUOTED,
					TOTAL_COST,
					PERC_DIFFERENCE,
					ADDITIONAL_CHANGES)
				VALUES(
					P_OPP_ID,
					2,
					l_data_type,
					P_QUOTE_REVISION_ID,
					l_total_parts,
					l_parts_with_cost,
                    l_parts_quoted,
					l_parts_not_quoted,
					l_total_cost,
					l_perc_difference,
					l_additional_changes);
				COMMIT;
			ELSE 
				UPDATE cct_cost_compare_header 
				   SET total_parts = l_total_parts, 
					   parts_with_cost = l_parts_with_cost, 
                       parts_quoted = l_parts_quoted,
					   parts_not_quoted = l_parts_not_quoted,
					   total_cost = l_total_cost,
					   perc_difference = l_perc_difference, 
					   additional_changes = l_additional_changes, 
					   quote_revision_id = P_QUOTE_REVISION_ID 
				 WHERE opportunity_id = P_OPP_ID AND 
					   data_type = 'Current';  
				COMMIT;       
			END IF;

		ELSE

			IF P_QUOTE_REVISION_ID IS NULL OR P_QUOTE_REVISION_ID = 0 THEN 
				l_total_parts       := 0; 
                l_parts_with_cost   := 0;
                l_total_resale      := 0; 
                l_total_cost        := 0;
                l_part_margin       := 0;
                l_net_margin        := 0;
			ELSE             
				SELECT NVL(total_no_of_parts_requested,0),  
                       NVL(total_no_of_parts_quoted,0), 
                       NVL(total_resale,0), 
                       NVL(total_cost,0), 
                       NVL(parts_margin,0), 
                       NVL(net_margin,0) 
                  INTO l_total_parts, 
                       l_parts_with_cost, 
                       l_total_resale, 
                       l_total_cost, 
                       l_part_margin,
                       l_net_margin 
				  FROM cct_quote_pricing_final_v cqv RIGHT JOIN 
					   cct_quote_revisions cqr 
					ON cqv.quote_revision_id = cqr.revision_id 
				 WHERE cqr.revision_id = p_quote_revision_id;
			END IF;			

			IF P_HEADER_TYPE = 'APPROVED' THEN    

                 SELECT COUNT(*) 
                  INTO l_count 
                  FROM cct_approved_compare_header  
                 WHERE opportunity_id = P_OPP_ID AND 
                       data_type = l_data_type; 

                IF l_count = 0 THEN
                    INSERT INTO CCT_APPROVED_COMPARE_HEADER(
                        OPPORTUNITY_ID, 
                        TYPE_ID,
                        DATA_TYPE,
                        QUOTE_REVISION_ID,
                        TOTAL_PARTS_REQUESTED,
                        TOTAL_PARTS_QUOTED,
                        TOTAL_COST,
                        TOTAL_RESALE,
                        PART_MARGIN,
                        NET_MARGIN)
                    VALUES(
                        P_OPP_ID,
                        2,
                        l_data_type,
                        P_QUOTE_REVISION_ID,
                        l_total_parts,
                        l_parts_with_cost,
                        l_total_cost,
                        l_total_resale,
                        l_part_margin,
                        l_net_margin);
                    COMMIT;
                ELSE 
                    UPDATE cct_approved_compare_header  
                       SET total_parts_requested = l_total_parts, 
                           total_parts_quoted = l_parts_with_cost, 
                           total_cost = l_total_cost,
                           total_resale = l_total_resale,
                           part_margin = l_part_margin, 
                           net_margin = l_net_margin, 
                           quote_revision_id = P_QUOTE_REVISION_ID 
                     WHERE opportunity_id = P_OPP_ID AND 
                           data_type = l_data_type;  
                    COMMIT;
                END IF;      

            ELSIF P_HEADER_TYPE = 'APPROVAL' THEN   

                SELECT COUNT(*) 
                  INTO l_count 
                  FROM cct_approval_compare_header  
                 WHERE opportunity_id = P_OPP_ID AND 
                       data_type = l_data_type; 

				IF l_count = 0 THEN
                    INSERT INTO CCT_APPROVAL_COMPARE_HEADER(
                        OPPORTUNITY_ID, 
                        TYPE_ID,
                        DATA_TYPE,
                        QUOTE_REVISION_ID,
                        TOTAL_PARTS_REQUESTED,
                        TOTAL_PARTS_QUOTED,
                        TOTAL_COST,
                        TOTAL_RESALE,
                        PART_MARGIN,
                        NET_MARGIN)
                    VALUES(
                        P_OPP_ID,
                        2,
                        l_data_type,
                        P_QUOTE_REVISION_ID,
                        l_total_parts,
                        l_parts_with_cost,
                        l_total_cost,
                        l_total_resale,
                        l_part_margin,
                        l_net_margin);
                    COMMIT;
                ELSE 
                    UPDATE cct_approval_compare_header  
                       SET total_parts_requested = l_total_parts, 
                           total_parts_quoted = l_parts_with_cost, 
                           total_cost = l_total_cost,
                           total_resale = l_total_resale,
                           part_margin = l_part_margin, 
                           net_margin = l_net_margin, 
                           quote_revision_id = P_QUOTE_REVISION_ID 
                     WHERE opportunity_id = P_OPP_ID AND 
                           data_type = l_data_type;  
                    COMMIT;
                END IF;      
            END IF;  
		END IF;
    EXCEPTION
    WHEN OTHERS THEN
        g_code                 := sqlcode;
        g_msg                  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            p_opportunity_id   => P_OPP_ID,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_COST_COMPARISON.setup_current_header',
            p_created_by       => sys_context('USERENV','OS_USER')
        );
    END setup_current_header;

    PROCEDURE calc_delta(
        P_OPP_ID             IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE,
        P_DML_TYPE           IN VARCHAR2,
        P_DELTA_TYPE         IN VARCHAR2)
    AS 
        l_opportunity_id        CCT_COST_COMPARE_HEADER.OPPORTUNITY_ID%TYPE;
        l_data_type             CCT_COST_COMPARE_HEADER.DATA_TYPE%TYPE;
        l_total_parts           CCT_COST_COMPARE_HEADER.TOTAL_PARTS%TYPE;
        l_parts_with_cost       CCT_COST_COMPARE_HEADER.PARTS_WITH_COST%TYPE;
        l_parts_quoted          CCT_COST_COMPARE_HEADER.PARTS_QUOTED%TYPE;
        l_parts_not_quoted      CCT_COST_COMPARE_HEADER.PARTS_NOT_QUOTED%TYPE;
        l_total_cost            CCT_COST_COMPARE_HEADER.TOTAL_COST%TYPE;
        l_perc_difference       CCT_COST_COMPARE_HEADER.PERC_DIFFERENCE%TYPE;
        l_additional_changes    CCT_COST_COMPARE_HEADER.ADDITIONAL_CHANGES%TYPE;
        l_snap_revision_id      CCT_COST_COMPARE_HEADER.QUOTE_REVISION_ID%TYPE;
        l_curr_revision_id      CCT_COST_COMPARE_HEADER.QUOTE_REVISION_ID%TYPE;
        l_total_resale          CCT_APPROVED_COMPARE_HEADER.TOTAL_RESALE%TYPE;
        l_part_margin           CCT_APPROVED_COMPARE_HEADER.PART_MARGIN%TYPE;
        l_net_margin            CCT_APPROVED_COMPARE_HEADER.NET_MARGIN%TYPE;
    BEGIN
        IF P_DELTA_TYPE = 'COST' THEN 
            SELECT s.opportunity_id, 
                   'Delta',
                   s.quote_revision_id, 
                   c.quote_revision_id, 
                   (NVL(c.total_parts,0)-NVL(s.total_parts,0)), 
                   (NVL(c.parts_with_cost,0)-NVL(s.parts_with_cost,0)), 
                   (NVL(c.parts_quoted,0)-NVL(s.parts_quoted,0)), 
                   (NVL(c.parts_not_quoted,0)-NVL(s.parts_not_quoted,0)), 
                   (NVL(c.total_cost,0)-NVL(s.total_cost,0)), 
                   CASE WHEN s.total_cost <> 0 THEN 
                    (((NVL(c.total_cost,0)-NVL(s.total_cost,0))/NVL(s.total_cost,0))*100) 
                   ELSE 
                    0
                   END 
              INTO l_opportunity_id, 
                   l_data_type, 
                   l_snap_revision_id, 
                   l_curr_revision_id, 
                   l_total_parts, 
                   l_parts_with_cost, 
                   l_parts_quoted, 
                   l_parts_not_quoted, 
                   l_total_cost, 
                   l_perc_difference 
              FROM cct_cost_compare_header s, 
                   cct_cost_compare_header c 
             WHERE s.opportunity_id = c.opportunity_id AND 
                   s.opportunity_id = P_OPP_ID AND 
                   s.data_type like 'Snapshot%' AND 
                   c.data_type = 'Current';
            --calc_additional_changes(P_OPP_ID,l_additional_changes);
            IF P_DML_TYPE = 'INSERT' THEN    
                INSERT INTO CCT_COST_COMPARE_HEADER(
                    OPPORTUNITY_ID,
                    TYPE_ID,
                    DATA_TYPE,
                    TOTAL_PARTS,
                    PARTS_WITH_COST,
                    PARTS_QUOTED, 
                    PARTS_NOT_QUOTED,
                    TOTAL_COST,
                    PERC_DIFFERENCE,
                    ADDITIONAL_CHANGES)
                VALUES(
                    l_opportunity_id, 
                    3,
                    l_data_type, 
                    l_total_parts, 
                    l_parts_with_cost, 
                    l_parts_quoted, 
                    l_parts_not_quoted, 
                    l_total_cost, 
                    l_perc_difference, 
                    l_additional_changes);
            ELSE
                UPDATE cct_cost_compare_header 
                   SET total_parts = l_total_parts, 
                       parts_with_cost = l_parts_with_cost, 
                       parts_quoted = l_parts_quoted,
                       parts_not_quoted = l_parts_not_quoted,
                       total_cost = l_total_cost,
                       perc_difference = l_perc_difference, 
                       additional_changes = l_additional_changes  
                 WHERE opportunity_id = P_OPP_ID AND 
                       data_type = 'Delta';  
            END IF;
            COMMIT;
            insert_cost_lines(P_OPP_ID,l_snap_revision_id,l_curr_revision_id); 
        ELSIF P_DELTA_TYPE = 'APPROVAL' THEN 
            SELECT s.opportunity_id, 
                   'Delta',
                   s.quote_revision_id, 
                   c.quote_revision_id, 
                   (NVL(c.total_parts_requested,0)-NVL(s.total_parts_requested,0)), 
                   (NVL(c.total_parts_quoted,0)-NVL(s.total_parts_quoted,0)), 
                   (NVL(c.total_resale,0)-NVL(s.total_resale,0)), 
                   (NVL(c.total_cost,0)-NVL(s.total_cost,0)), 
                   (NVL(c.part_margin,0)-NVL(s.part_margin,0)),
                   (NVL(c.net_margin,0)-NVL(s.net_margin,0))  
              INTO l_opportunity_id, 
                   l_data_type, 
                   l_snap_revision_id, 
                   l_curr_revision_id, 
                   l_total_parts, 
                   l_parts_with_cost, 
                   l_total_resale, 
                   l_total_cost, 
                   l_part_margin,
                   l_net_margin 
              FROM cct_approval_compare_header s, 
                   cct_approval_compare_header c 
             WHERE s.opportunity_id = c.opportunity_id AND 
                   s.opportunity_id = P_OPP_ID AND 
                   s.data_type like 'Snapshot%' AND 
                   c.data_type = 'Current';

            IF P_DML_TYPE = 'INSERT' THEN    
                INSERT INTO CCT_APPROVAL_COMPARE_HEADER(
                    OPPORTUNITY_ID, 
                    TYPE_ID,
                    DATA_TYPE,
                    TOTAL_PARTS_REQUESTED,
                    TOTAL_PARTS_QUOTED,
                    TOTAL_COST,
                    TOTAL_RESALE,
                    PART_MARGIN,
                    NET_MARGIN) 
                VALUES(
                    l_opportunity_id, 
                    3,
                    l_data_type, 
                    l_total_parts, 
                    l_parts_with_cost, 
                    l_total_cost, 
                    l_total_resale, 
                    l_part_margin, 
                    l_net_margin);
            ELSE
                UPDATE cct_approval_compare_header 
                   SET total_parts_requested = l_total_parts, 
                       total_parts_quoted = l_parts_with_cost, 
                       total_resale = l_total_resale,
                       total_cost = l_total_cost,
                       part_margin = l_part_margin, 
                       net_margin = l_net_margin  
                 WHERE opportunity_id = P_OPP_ID AND 
                       data_type = 'Delta';  
            END IF;
            COMMIT;
        ELSIF P_DELTA_TYPE = 'APPROVED' THEN 
            SELECT s.opportunity_id, 
                   'Delta',
                   s.quote_revision_id, 
                   c.quote_revision_id, 
                   (NVL(c.total_parts_requested,0)-NVL(s.total_parts_requested,0)), 
                   (NVL(c.total_parts_quoted,0)-NVL(s.total_parts_quoted,0)), 
                   (NVL(c.total_resale,0)-NVL(s.total_resale,0)), 
                   (NVL(c.total_cost,0)-NVL(s.total_cost,0)), 
                   (NVL(c.part_margin,0)-NVL(s.part_margin,0)),
                   (NVL(c.net_margin,0)-NVL(s.net_margin,0))  
              INTO l_opportunity_id, 
                   l_data_type, 
                   l_snap_revision_id, 
                   l_curr_revision_id, 
                   l_total_parts, 
                   l_parts_with_cost, 
                   l_total_resale, 
                   l_total_cost, 
                   l_part_margin,
                   l_net_margin 
              FROM cct_approved_compare_header s, 
                   cct_approved_compare_header c 
             WHERE s.opportunity_id = c.opportunity_id AND 
                   s.opportunity_id = P_OPP_ID AND 
                   s.data_type like 'Snapshot%' AND 
                   c.data_type = 'Current';

            IF P_DML_TYPE = 'INSERT' THEN    
                INSERT INTO CCT_APPROVED_COMPARE_HEADER(
                    OPPORTUNITY_ID, 
                    TYPE_ID,
                    DATA_TYPE,
                    TOTAL_PARTS_REQUESTED,
                    TOTAL_PARTS_QUOTED,
                    TOTAL_COST,
                    TOTAL_RESALE,
                    PART_MARGIN,
                    NET_MARGIN) 
                VALUES(
                    l_opportunity_id, 
                    3,
                    l_data_type, 
                    l_total_parts, 
                    l_parts_with_cost, 
                    l_total_cost, 
                    l_total_resale, 
                    l_part_margin, 
                    l_net_margin);
            ELSE
                UPDATE cct_approved_compare_header 
                   SET total_parts_requested = l_total_parts, 
                       total_parts_quoted = l_parts_with_cost, 
                       total_resale = l_total_resale,
                       total_cost = l_total_cost,
                       part_margin = l_part_margin, 
                       net_margin = l_net_margin  
                 WHERE opportunity_id = P_OPP_ID AND 
                       data_type = 'Delta';  
            END IF;
            COMMIT;
        END IF;
    EXCEPTION
    WHEN OTHERS THEN
        g_code                 := sqlcode;
        g_msg                  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            p_opportunity_id   => P_OPP_ID,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_COST_COMPARISON.calc_delta',
            p_created_by       => sys_context('USERENV','OS_USER')
        );
    END calc_delta;

    PROCEDURE calc_additional_changes(
        P_OPP_ID             IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE,
        P_SNAPSHOT_REV_ID    IN CCT_SNAPSHOT.QUOTE_REVISION_ID%TYPE,
        P_CURRENT_REV_ID     IN CCT_QUOTE_PARTS.QUOTE_REVISION_ID%TYPE,
        P_QUOTE_PART         IN CCT_COST_COMPARE_LINES.QUOTED_PART_NUMBER%TYPE,
        P_CUSTOMER_PN        IN CCT_QUOTE_PARTS_SNAPSHOT.CUSTOMER_PN%TYPE,    
        P_LINE_IDENTIFIER    IN CCT_QUOTE_PARTS_SNAPSHOT.LINE_IDENTIFIER%TYPE, 
        P_FLAG              OUT VARCHAR2)
    AS 
        l_snapshot_id           CCT_QUOTE_PARTS_SNAPSHOT.SNAPSHOT_ID%TYPE;
        l_count                 NUMBER;
    BEGIN 

        SELECT MAX(snapshot_id) 
          INTO l_snapshot_id 
          FROM cct_snapshot 
         WHERE opportunity_id = P_OPP_ID AND 
               UPPER(snapshot_type) <> 'APPROVAL';  

        IF l_snapshot_id IS NULL THEN 
            l_snapshot_id := 0;
        END IF;
        l_count := 1;
        SELECT COUNT(*) 
          INTO l_count  
          FROM (
                SELECT DISTINCT queue_id, whse_id, product_id, quoted_part_number, 
                       annual_qty, uom, approved_source, disapproved_source, misc_cust_data,
                       target_price, target_margin, certified_part, contract_price, last_price_paid,
                       highest_contract_price, lowest_contract_price, high_tape_price, 
                       middle_tape_price, low_tape_price, nvl(suggested_TOTAL_QTY_LEN_CON,total_qty_len_contract) total_qty_len_contract, average_cost, 
                       average_cost_lbs, last_cost, actual_cost, vendor_cost, market_cost, lta_cost, 
                       poq_cost, poq_quantity, delivery_time_poq, cd_from_jda, kapco_part_cost, 
                       symph_drop_shipment, hpp_part_price, hpp_part, prime, dup_prime, dup_cust_part, 
                       vendor_back_orders, cust_back_orders, lta_moq, lta_exp_date, lta_mfr, 
                       lta_leadtime_weeks, vendor_leadtime_weeks, prod_type_abc, kapco_leadtime_weeks, 
                       first_plan_purday, jda_forecast_12m, control_number, manf_number, manf_name, 
                       mfr_cage_code, shelf_life, pma, tso, certs, c_and_p, rev, part_desc, tariff, 
                       itar, eccn, dfar, ppp, stocking_unit_measure, comments, product_notes, 
                       restriction_codes, prod_class, total_onhand_whses, other_cust_price, 
                       contract_part_number, alt_part_number, total_alt_qty_onhand, 
                       total_vendor_back_orders, whse_quantity, resale, calc_abc, 
                       alternatives_avl, final_cost_source, extended_cost, extended_resale, 
                       final_mkt_class, item_tag, last_price_offered, lta_review_item, 
                       potential_uom_issue, replacement_cost_used, speciality_products, pipeline_qty, 
                       dyp_price_break_qty, ecommerce_price, price_type, margin_validation, 
                       customer_final_leadtime, internal_notes, external_notes, lta_cost_cust, 
                       lta_full_qty_used, sales_restriction, status, comment_category, quoted_qty,
                       qty_break1, qty_break2, moq, amortiz_infl_pct, global_quantities, manufacturer_certs, 
                       test_reports, fair_fee, tool_fee, misc_fee, dfar_fee, vendor_comment, quote_number_from_supplier,
                       spreadsheet, price_confidence, negotiated, customer_container_size_data, customer_uom, 
                       cstmr_um_conv_fctr_cardex_unit, comments_um_conversion, crdx_unit_size_info_iden_to_s4, 
                       extended_annual_cost, itl_internal_pn, vendor_location_country, alternate_source,
                       selling_increment, moq_cardex_unit, inco_term, stock_location, shelf_life_months_general,
                       hazmat_jn, storage_group, tot_sls_val_usd12mths_crdx_uni, pct_discount, package_qty,
                       tech_support_comments, tech_support_alternate, tech_support_source, suggested_cust_final_leadtime, 
                       suggested_quoted_part_number, suggested_annual_qty, suggested_uom, suggested_prime, suggested_calc_abc,
                       suggested_manf_name, suggested_mfr_cage_code, suggested_resale  
                  FROM cct_quote_parts 
                 WHERE quote_revision_id = P_CURRENT_REV_ID AND 
                       quoted_part_number = P_QUOTE_PART AND 
                       customer_pn = P_CUSTOMER_PN AND 
                       line_identifier = P_LINE_IDENTIFIER 
                UNION  
                SELECT DISTINCT queue_id, whse_id, product_id, quoted_part_number, 
                       annual_qty, uom, approved_source, disapproved_source, misc_cust_data,
                       target_price, target_margin, certified_part, contract_price, last_price_paid,
                       highest_contract_price, lowest_contract_price, high_tape_price, 
                       middle_tape_price, low_tape_price, nvl(suggested_TOTAL_QTY_LEN_CON,total_qty_len_contract) total_qty_len_contract, average_cost, 
                       average_cost_lbs, last_cost, actual_cost, vendor_cost, market_cost, lta_cost, 
                       poq_cost, poq_quantity, delivery_time_poq, cd_from_jda, kapco_part_cost, 
                       symph_drop_shipment, hpp_part_price, hpp_part, prime, dup_prime, dup_cust_part, 
                       vendor_back_orders, cust_back_orders, lta_moq, lta_exp_date, lta_mfr, 
                       lta_leadtime_weeks, vendor_leadtime_weeks, prod_type_abc, kapco_leadtime_weeks, 
                       first_plan_purday, jda_forecast_12m, control_number, manf_number, manf_name, 
                       mfr_cage_code, shelf_life, pma, tso, certs, c_and_p, rev, part_desc, tariff, 
                       itar, eccn, dfar, ppp, stocking_unit_measure, comments, product_notes, 
                       restriction_codes, prod_class, total_onhand_whses, other_cust_price, 
                       contract_part_number, alt_part_number, total_alt_qty_onhand, 
                       total_vendor_back_orders, whse_quantity, resale, calc_abc, 
                       alternatives_avl, final_cost_source, extended_cost, extended_resale, 
                       final_mkt_class, item_tag, last_price_offered, lta_review_item, 
                       potential_uom_issue, replacement_cost_used, speciality_products, pipeline_qty, 
                       dyp_price_break_qty, ecommerce_price, price_type, margin_validation, 
                       customer_final_leadtime, internal_notes, external_notes, lta_cost_cust, 
                       lta_full_qty_used, sales_restriction, status, comment_category, quoted_qty,
                       qty_break1, qty_break2, moq, amortiz_infl_pct, global_quantities, manufacturer_certs, 
                       test_reports, fair_fee, tool_fee, misc_fee, dfar_fee, vendor_comment, quote_number_from_supplier,
                       spreadsheet, price_confidence, negotiated, customer_container_size_data, customer_uom, 
                       cstmr_um_conv_fctr_cardex_unit, comments_um_conversion, crdx_unit_size_info_iden_to_s4, 
                       extended_annual_cost, itl_internal_pn, vendor_location_country, alternate_source,
                       selling_increment, moq_cardex_unit, inco_term, stock_location, shelf_life_months_general,
                       hazmat_jn, storage_group, tot_sls_val_usd12mths_crdx_uni, pct_discount, package_qty,
                       tech_support_comments, tech_support_alternate, tech_support_source, suggested_cust_final_leadtime, 
                       suggested_quoted_part_number, suggested_annual_qty, suggested_uom, suggested_prime, suggested_calc_abc,
                       suggested_manf_name, suggested_mfr_cage_code, suggested_resale   
                  FROM cct_quote_parts_snapshot 
                 WHERE quote_revision_id = P_SNAPSHOT_REV_ID AND 
                       quoted_part_number = P_QUOTE_PART AND 
                       customer_pn = P_CUSTOMER_PN AND 
                       line_identifier = P_LINE_IDENTIFIER AND 
                       snapshot_id = l_snapshot_id);

        IF l_count > 1 THEN 
            P_FLAG := 'YES';
        ELSE 
            P_FLAG := 'NO';
        END IF;
    EXCEPTION
    WHEN OTHERS THEN
        g_code                 := sqlcode;
        g_msg                  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            p_opportunity_id   => P_OPP_ID,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_COST_COMPARISON.calc_additional_changes',
            p_created_by       => sys_context('USERENV','OS_USER')
        );
    END calc_additional_changes;

    PROCEDURE insert_cost_lines(
        P_OPP_ID             IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE,
        P_SNAPSHOT_REV_ID    IN CCT_SNAPSHOT.QUOTE_REVISION_ID%TYPE,
        P_CURRENT_REV_ID     IN CCT_QUOTE_PARTS.QUOTE_REVISION_ID%TYPE)
    AS 
        CURSOR cur_parts 
        IS 
            SELECT DISTINCT c.customer_pn, 
                   c.line_identifier, 
                   c.quoted_part_number, 
                   nvl(c.suggested_TOTAL_QTY_LEN_CON,c.total_qty_len_contract) total_qty_len_contract, 
                   c.suggested_cost_used_for_quote, 
                   c.queue_id,
                   s.customer_pn, 
                   s.line_identifier, 
                   s.quoted_part_number, 
                   nvl(s.suggested_TOTAL_QTY_LEN_CON,s.total_qty_len_contract) total_qty_len_contract, 
                   s.suggested_cost_used_for_quote, 
                   s.queue_id 
              FROM (SELECT * 
                      FROM cct_quote_parts 
                     WHERE quote_revision_id = P_CURRENT_REV_ID) C FULL JOIN 
                   (SELECT * 
                      FROM cct_quote_parts_snapshot 
                     WHERE snapshot_id = (SELECT NVL(MAX(snapshot_id),0)  
                                            FROM cct_snapshot 
                                           WHERE opportunity_id = P_OPP_ID AND 
                                                 UPPER(snapshot_type) <>'APPROVAL') AND 
                           quote_revision_id = P_SNAPSHOT_REV_ID) S 
                ON c.quoted_part_number = s.quoted_part_number and 
                   c.customer_pn = s.customer_pn and 
                   c.line_identifier = s.line_identifier 
             ORDER BY c.customer_pn ASC NULLS LAST;

        l_curr_cust_pn          CCT_QUOTE_PARTS.CUSTOMER_PN%TYPE;    
        l_curr_li               CCT_QUOTE_PARTS.LINE_IDENTIFIER%TYPE;    
        l_curr_part             CCT_QUOTE_PARTS.QUOTED_PART_NUMBER%TYPE;
        l_curr_qty              CCT_QUOTE_PARTS.TOTAL_QTY_LEN_CONTRACT%TYPE;
        l_curr_quote            CCT_QUOTE_PARTS.SUGGESTED_COST_USED_FOR_QUOTE%TYPE;
        l_curr_queue_id         CCT_QUOTE_PARTS.QUEUE_ID%TYPE;
        l_snap_cust_pn          CCT_QUOTE_PARTS_SNAPSHOT.CUSTOMER_PN%TYPE;    
        l_snap_li               CCT_QUOTE_PARTS_SNAPSHOT.LINE_IDENTIFIER%TYPE; 
        l_snap_part             CCT_QUOTE_PARTS_SNAPSHOT.QUOTED_PART_NUMBER%TYPE;
        l_snap_qty              CCT_QUOTE_PARTS_SNAPSHOT.TOTAL_QTY_LEN_CONTRACT%TYPE;
        l_snap_quote            CCT_QUOTE_PARTS_SNAPSHOT.SUGGESTED_COST_USED_FOR_QUOTE%TYPE;       
        l_snap_queue_id         CCT_QUOTE_PARTS_SNAPSHOT.QUEUE_ID%TYPE; 
        l_quote_part            CCT_COST_COMPARE_LINES.QUOTED_PART_NUMBER%TYPE;
        l_prev_cost             CCT_COST_COMPARE_LINES.PREVIOUS_EXT_COST%TYPE;
        l_new_cost              CCT_COST_COMPARE_LINES.NEW_EXT_COST%TYPE;
        l_delta                 CCT_COST_COMPARE_LINES.DELTA%TYPE;
        l_perc_diff             CCT_COST_COMPARE_LINES.PERC_DIFFERENCE%TYPE;
        l_perc_cost             CCT_COST_COMPARE_LINES.PERC_COST%TYPE;
        l_work_queue            CCT_COST_COMPARE_LINES.WORK_QUEUE%TYPE;
        l_change_type           CCT_COST_COMPARE_LINES.CHANGE_TYPE%TYPE;
        l_snap_cost             CCT_COST_COMPARE_HEADER.TOTAL_COST%TYPE;
        l_count                 NUMBER;
        l_flag                  VARCHAR2(10);
    BEGIN 

        DELETE FROM cct_cost_compare_lines  
         WHERE opportunity_id = P_OPP_ID;

        SELECT total_cost 
          INTO l_snap_cost 
          FROM cct_cost_compare_header 
         WHERE opportunity_id = P_OPP_ID AND 
               quote_revision_id = P_SNAPSHOT_REV_ID AND 
               data_type like 'Snapshot%'; 

        l_count := 0;               
        OPEN cur_parts;
        LOOP
            FETCH cur_parts INTO l_curr_cust_pn, l_curr_li, l_curr_part, 
                                 l_curr_qty, l_curr_quote, l_curr_queue_id, 
                                 l_snap_cust_pn, l_snap_li, l_snap_part, 
                                 l_snap_qty, l_snap_quote, l_snap_queue_id;
            EXIT WHEN cur_parts%NOTFOUND;
            l_curr_part     := NVL(l_curr_part,'N/A');
            l_curr_qty      := NVL(l_curr_qty,0);
            l_curr_quote    := NVL(l_curr_quote,0);
            l_curr_queue_id := NVL(l_curr_queue_id,0);
            l_snap_part     := NVL(l_snap_part,'N/A');
            l_snap_qty      := NVL(l_snap_qty,0);
            l_snap_quote    := NVL(l_snap_quote,0);
            l_snap_queue_id := NVL(l_snap_queue_id,0); 
            l_count := l_count + 1;
            /**dbms_output.put_line('COUNT' || l_count);
            dbms_output.put_line(l_curr_part);
            dbms_output.put_line(l_curr_qty);
            dbms_output.put_line(l_curr_quote);
            dbms_output.put_line(l_curr_queue_id);
            dbms_output.put_line(l_snap_part);
            dbms_output.put_line(l_snap_qty);
            dbms_output.put_line(l_snap_quote);
            dbms_output.put_line(l_snap_queue_id);**/
            IF l_snap_part='N/A' THEN 
                l_quote_part := l_curr_part;
                l_prev_cost := 0;
                l_new_cost := l_curr_qty * l_curr_quote;
                l_delta := l_new_cost - l_prev_cost;
                l_change_type := 'Cost Change';
            ELSIF l_curr_part='N/A' THEN 
                l_quote_part := l_snap_part;
                l_prev_cost := l_snap_qty * l_snap_quote;
                l_new_cost := 0;
                l_delta := l_new_cost - l_prev_cost;
                l_change_type := 'Cost Change';
            ELSE 
                l_quote_part := l_curr_part;
                l_prev_cost := l_snap_qty * l_snap_quote;
                l_new_cost := l_curr_qty * l_curr_quote;
                l_delta := l_new_cost - l_prev_cost;

                IF l_delta = 0 THEN 
                    calc_additional_changes(P_OPP_ID,P_SNAPSHOT_REV_ID,
                        P_CURRENT_REV_ID,l_quote_part,l_curr_cust_pn, l_curr_li,l_flag);
                    IF l_flag = 'YES' THEN 
                        l_change_type := 'Additional Change';
                    ELSE 
                        l_change_type := 'No Change';
                    END IF;
                ELSE 
                    l_change_type := 'Cost Change';
                END IF;
            END IF;

            IF l_prev_cost = 0 AND l_new_cost = 0 THEN 
                l_perc_diff := 0;
                l_perc_cost := 0;
            ELSIF l_prev_cost = 0 AND l_new_cost <> 0 THEN 
                l_perc_diff := 100;
                l_perc_cost := 0;
            ELSE 
                l_perc_diff := ((l_new_cost - l_prev_cost)/l_prev_cost) * 100;
                IF l_snap_cost = 0  THEN 
                    l_perc_cost := 0;
                ELSE
                    l_perc_cost := (l_prev_cost / l_snap_cost) * 100;
                END IF;
            END IF;

            IF l_curr_queue_id = 0 AND l_snap_queue_id = 0  THEN
                l_work_queue := 'N/A';
            ELSIF l_snap_queue_id = 0 THEN 
                SELECT queue_name 
                  INTO l_work_queue 
                  FROM cct_parts_queue 
                 WHERE queue_id = l_curr_queue_id;
            ELSIF l_curr_queue_id = 0  THEN 
                SELECT queue_name 
                  INTO l_work_queue 
                  FROM cct_parts_queue  
                 WHERE queue_id = l_snap_queue_id;
            ELSIF l_curr_queue_id = l_snap_queue_id THEN  
                SELECT queue_name 
                  INTO l_work_queue 
                  FROM cct_parts_queue 
                 WHERE queue_id = l_curr_queue_id;
            ELSIF l_curr_queue_id <> l_snap_queue_id THEN  
                SELECT queue_name 
                  INTO l_work_queue 
                  FROM cct_parts_queue 
                 WHERE queue_id = nvl(l_curr_queue_id,l_snap_queue_id);     
/*                 
            ELSE 
                 l_work_queue := 'MISMATCHING QUEUE';                    */
            END IF;

            INSERT INTO CCT_COST_COMPARE_LINES(
                OPPORTUNITY_ID,
                QUOTED_PART_NUMBER, 
                PREVIOUS_EXT_COST, 
                NEW_EXT_COST, 
                DELTA, 
                PERC_DIFFERENCE, 
                PERC_COST, 
                WORK_QUEUE,
                CHANGE_TYPE) 
            VALUES(
                P_OPP_ID, 
                l_quote_part,
                l_prev_cost,
                l_new_cost,
                l_delta,
                l_perc_diff,
                l_perc_cost,
                l_work_queue,
                l_change_type);
            COMMIT;
        END LOOP;
        CLOSE cur_parts;

        UPDATE cct_cost_compare_header 
           SET additional_changes = (SELECT COUNT(*) 
                                       FROM cct_cost_compare_lines 
                                      WHERE opportunity_id = P_OPP_ID AND 
                                            UPPER(change_type) = 'ADDITIONAL CHANGE') 
         WHERE opportunity_id = P_OPP_ID AND 
               UPPER(data_type) = 'DELTA';

        COMMIT;

        DELETE FROM cct_cost_compare_lines 
         WHERE opportunity_id = P_OPP_ID AND 
               UPPER(change_type)=UPPER('NO CHANGE');

        COMMIT;    
    EXCEPTION
    WHEN OTHERS THEN
        g_code                 := sqlcode;
        g_msg                  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            p_opportunity_id   => P_OPP_ID,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_COST_COMPARISON.insert_cost_lines',
            p_created_by       => sys_context('USERENV','OS_USER')
        );
    END insert_cost_lines;

END cct_cost_comparison;