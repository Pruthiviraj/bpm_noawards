create or replace PACKAGE BODY CCT_QUOTE_PRICING_FINAL
AS
/**History
    Author        Date         COMMENTS
    ------------  -----------  -----------------------------------------------
    Ankit Bhatt   02-Nov-2017  Created initial version
**/
/*
    FUNCTION total_cost(
        P_QUOTE_REVISION_ID     CCT_QUOTE_PARTS_FINAL.QUOTE_REVISION_ID%TYPE)
    RETURN NUMBER 
    AS 
        l_total_cost            NUMBER;
    BEGIN
        SELECT  SUM(CQP.COST_USED_FOR_QUOTE * CQP.TOTAL_QTY_LEN_CONTRACT) 
          INTO  l_total_cost 
          FROM  CCT_QUOTE_PARTS_FINAL CQP, 
                CCT_QUOTE_REVISIONS CQR 
         WHERE  CQR.REVISION_ID = CQP.QUOTE_REVISION_ID AND 
                CQP.QUOTE_REVISION_ID = P_QUOTE_REVISION_ID;                 
        RETURN  l_total_cost;
    END total_cost;

    FUNCTION total_resale(
        P_QUOTE_REVISION_ID     CCT_QUOTE_PARTS_FINAL.QUOTE_REVISION_ID%TYPE)
    RETURN NUMBER 
    AS 
        l_total_resale          NUMBER;
    BEGIN
        SELECT  SUM(CQP.RESALE * CQP.TOTAL_QTY_LEN_CONTRACT) 
          INTO  l_total_resale 
          FROM  CCT_QUOTE_PARTS_FINAL CQP,
                CCT_QUOTE_REVISIONS CQR 
         WHERE  CQR.REVISION_ID = CQP.QUOTE_REVISION_ID AND 
                CQP.QUOTE_REVISION_ID = P_QUOTE_REVISION_ID;
        RETURN  l_total_resale;
    END total_resale;
*/    
    FUNCTION class_margin(
        P_QUOTE_REVISION_ID     CCT_QUOTE_PARTS_FINAL.QUOTE_REVISION_ID%TYPE,
        P_CALC_ABC              CCT_QUOTE_PARTS_FINAL.CALC_ABC%TYPE)
    RETURN NUMBER 
    AS
        l_parts_margin          NUMBER;
    BEGIN
        SELECT  PARTS_MARGIN 
          INTO  l_parts_margin 
          FROM  CCT_QUOTE_CLASS_V
         WHERE  QUOTE_REVISION_ID = P_QUOTE_REVISION_ID AND 
                ITEM_CLASS = P_CALC_ABC;
        RETURN  l_parts_margin;
    END class_margin;

    FUNCTION class_cost_snapshot(
        P_SNAPSHOT_ID           CCT_SNAPSHOT.SNAPSHOT_ID%TYPE,
        P_QUOTE_REVISION_ID     CCT_QUOTE_PARTS_FINAL.QUOTE_REVISION_ID%TYPE,
        P_CALC_ABC              CCT_QUOTE_PARTS_FINAL.CALC_ABC%TYPE)
    RETURN NUMBER 
    AS
        l_class_cost            NUMBER := 0;
    BEGIN
        SELECT  TOTAL_COST 
          INTO  l_class_cost 
          FROM  CCT_QUOTE_CLASS_SS_FINAL_V  
         WHERE  SNAPSHOT_ID = P_SNAPSHOT_ID AND 
                QUOTE_REVISION_ID = P_QUOTE_REVISION_ID AND 
                ITEM_CLASS = P_CALC_ABC;
        RETURN  l_class_cost;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 0;
    END class_cost_snapshot;

    FUNCTION class_resale_snapshot(
        P_SNAPSHOT_ID           CCT_SNAPSHOT.SNAPSHOT_ID%TYPE,
        P_QUOTE_REVISION_ID     CCT_QUOTE_PARTS_FINAL.QUOTE_REVISION_ID%TYPE,
        P_CALC_ABC              CCT_QUOTE_PARTS_FINAL.CALC_ABC%TYPE)
    RETURN NUMBER 
    AS
        l_class_resale          NUMBER := 0;
    BEGIN
        SELECT  TOTAL_RESALE
          INTO  l_class_resale
          FROM  CCT_QUOTE_CLASS_SS_FINAL_V 
         WHERE  SNAPSHOT_ID = P_SNAPSHOT_ID AND 
                QUOTE_REVISION_ID = P_QUOTE_REVISION_ID AND 
                ITEM_CLASS = P_CALC_ABC;
        RETURN  l_class_resale;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 0;
    END class_resale_snapshot;

    FUNCTION class_margin_snapshot(
        P_SNAPSHOT_ID           CCT_SNAPSHOT.SNAPSHOT_ID%TYPE,
        P_QUOTE_REVISION_ID     CCT_QUOTE_PARTS_FINAL.QUOTE_REVISION_ID%TYPE,
        P_CALC_ABC              CCT_QUOTE_PARTS_FINAL.CALC_ABC%TYPE)
    RETURN NUMBER
    AS
        l_class_margin          NUMBER := 0;
    BEGIN
        SELECT  PARTS_MARGIN 
          INTO  l_class_margin 
          FROM  CCT_QUOTE_CLASS_SS_FINAL_V  
         WHERE  SNAPSHOT_ID = P_SNAPSHOT_ID AND 
                QUOTE_REVISION_ID = P_QUOTE_REVISION_ID AND 
                ITEM_CLASS = P_CALC_ABC;
        RETURN  l_class_margin;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 0;
    END class_margin_snapshot;

    FUNCTION class_count_snapshot(
        P_SNAPSHOT_ID           CCT_SNAPSHOT.SNAPSHOT_ID%TYPE,
        P_QUOTE_REVISION_ID     CCT_QUOTE_PARTS_FINAL.QUOTE_REVISION_ID%TYPE,
        P_CALC_ABC              CCT_QUOTE_PARTS_FINAL.CALC_ABC%TYPE)
    RETURN NUMBER 
    AS
        l_count                 NUMBER := 0;
    BEGIN
        SELECT  PN_QUOTED
          INTO  l_count
          FROM  CCT_QUOTE_CLASS_SS_FINAL_V 
         WHERE  SNAPSHOT_ID = P_SNAPSHOT_ID AND 
                QUOTE_REVISION_ID = P_QUOTE_REVISION_ID AND 
                ITEM_CLASS = P_CALC_ABC;
        RETURN  l_count;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 0;
    END class_count_snapshot;

END CCT_QUOTE_PRICING_FINAL;