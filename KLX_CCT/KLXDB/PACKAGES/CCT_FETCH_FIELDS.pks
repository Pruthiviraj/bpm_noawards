create or replace PACKAGE CCT_FETCH_FIELDS AS
/**Handles fetching fields
   Modification History
   PERSON          DATE           COMMENTS
   ------------    -----------    ---------------------------------------------
   Sureka          14-Sep-2017    Created get_opty_id proc
   Sureka          14-Sep-2017    Created get_osc_opty_id proc
   Sureka          14-Sep-2017    Created get_opty_num proc
   Sureka          14-Sep-2017    Created get_latest_revision proc
   Sureka          14-Sep-2017    Created get_unknown_pd_count func
   Sureka          14-Sep-2017    Created get_queue_pd_count func
   Sureka          28-Sep-2017    Created get_latest_snapshot proc
   Sureka          25-Oct-2017    Created get_queue_pd_count_cost proc
   Sureka          26-Oct-2017    Created get_latest_approval proc
   Sureka          14-Dec-2017    Created get_proposal_comments proc
   Sureka          30-Jan-2018    Created get_approver_name proc
   Sureka          07-Feb-2018    Created get_crf_id proc
   Sureka          04-Apr-2018    Created get_payment_notes func
   Ankit Bhatt     28-Sep-2018    Created get_latest_approved_snapshot proc
**/
    g_code                        VARCHAR2(50);
    g_msg                         VARCHAR2(500);

    PROCEDURE get_opty_id (
        P_OPPORTUNITY_NUMBER   IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE,
        P_OPPORTUNITY_ID      OUT CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE);

    PROCEDURE get_osc_opty_id (
        P_OPPORTUNITY_NUMBER   IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE,
        P_OSC_OPPORTUNITY_ID  OUT CCT_OPPORTUNITY.OSC_OPPORTUNITY_ID%TYPE);

    PROCEDURE get_opty_num (
        P_OPPORTUNITY_ID       IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE,
        P_OPPORTUNITY_NUMBER  OUT CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE);

    PROCEDURE get_latest_revision (
        P_OPPORTUNITY_NUMBER   IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE DEFAULT NULL,
        P_OPPORTUNITY_ID       IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE DEFAULT NULL,
        P_REVISION_ID         OUT CCT_QUOTE_REVISIONS.REVISION_ID%TYPE);

    PROCEDURE get_latest_snapshot (
        P_OPPORTUNITY_NUMBER   IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE DEFAULT NULL,
        P_OPPORTUNITY_ID       IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE DEFAULT NULL,
        P_SNAPSHOT_TYPE        IN CCT_SNAPSHOT.SNAPSHOT_TYPE%TYPE DEFAULT NULL,
        P_SNAPSHOT_ID         OUT CCT_SNAPSHOT.SNAPSHOT_ID%TYPE);
  
    PROCEDURE get_latest_approval (
        P_SNAPSHOT_ID          IN CCT_QUOTE_APPROVALS.SNAPSHOT_ID%TYPE,
        P_APPROVAL_ID         OUT CCT_QUOTE_APPROVALS.APPROVAL_ID%TYPE);

    PROCEDURE get_proposal_comments (
        P_OPPORTUNITY_ID       IN CCT_SNAPSHOT.OPPORTUNITY_ID%TYPE,
        P_SNAPSHOT_VERSION     IN CCT_SNAPSHOT.SNAPSHOT_VERSION%TYPE,
        P_PROPOSAL_COMMENTS   OUT CLOB);

    FUNCTION get_unknown_pd_count (
        P_REVISION_ID          IN CCT_QUOTE_REVISIONS.REVISION_ID%TYPE)
    RETURN NUMBER;

    FUNCTION get_queue_pd_count (
        P_REVISION_ID          IN CCT_QUOTE_REVISIONS.REVISION_ID%TYPE,
        P_ROLE_NAME            IN CCT_NOTIFICATION_REQUEST.ROLE_NAME%TYPE)
    RETURN NUMBER;

    FUNCTION get_queue_pd_count_cost (
        P_REVISION_ID          IN CCT_QUOTE_REVISIONS.REVISION_ID%TYPE,
        P_ROLE_NAME            IN CCT_NOTIFICATION_REQUEST.ROLE_NAME%TYPE)
    RETURN NUMBER;

    PROCEDURE get_approver_name(
        P_OPPORTUNITY_ID       IN CCT_SNAPSHOT.OPPORTUNITY_ID%TYPE,
        P_SNAPSHOT_VERSION     IN CCT_SNAPSHOT.SNAPSHOT_VERSION%TYPE,
        P_APPROVAL_ROLE_ID     IN CCT_APPROVAL_USERS.APPROVAL_ROLE_ID%TYPE,
        P_APPROVAL_USER_NAME  OUT CCT_APPROVAL_USERS.APPROVAL_USER_NAME%TYPE);

    PROCEDURE get_crf_id(
        P_OPPORTUNITY_NUMBER   IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE DEFAULT NULL,
        P_OPPORTUNITY_ID       IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE DEFAULT NULL,
        P_CRF_ID              OUT CCT_CRF.CRF_ID%TYPE);

    FUNCTION get_payment_notes(
        P_OPPORTUNITY_NUMBER   IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE) 
    RETURN VARCHAR2;
    
       PROCEDURE get_latest_approved_snapshot (
        P_OPPORTUNITY_NUMBER   IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE DEFAULT NULL,
        P_OPPORTUNITY_ID       IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE DEFAULT NULL,
        P_SNAPSHOT_TYPE        IN CCT_SNAPSHOT.SNAPSHOT_TYPE%TYPE DEFAULT NULL,
        P_SNAPSHOT_ID         OUT CCT_SNAPSHOT.SNAPSHOT_ID%TYPE);         


END CCT_FETCH_FIELDS;