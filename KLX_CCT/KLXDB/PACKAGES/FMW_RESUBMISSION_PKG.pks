CREATE OR REPLACE PACKAGE fmw_resubmission_pkg
AS
    PROCEDURE get_resubmission_payloads(
        I_RESUBMISSION_IDS          IN RESUBMISSION_IDS,
        O_RESUBMISSION_DETAILS_TBL OUT RESUBMISSION_DETAILS_TBL,
        O_STATUS                   OUT VARCHAR2,
        O_ERROR_MESSAGE            OUT VARCHAR2);
      
    PROCEDURE get_resubmission_ids(
        I_MESSAGE_FLOW_IDS          IN MESSAGE_FLOW_IDS,
        I_INTERFACE_ID              IN VARCHAR2,
        O_RESUBMISSION_IDS         OUT RESUBMISSION_IDS,
        O_STATUS                   OUT VARCHAR2,
        O_ERROR_MESSAGE            OUT VARCHAR2);
    
    PROCEDURE update_resubmission_status(
        I_RESUBMISSION_IDS          IN RESUBMISSION_IDS,
        O_STATUS                   OUT VARCHAR2,
        O_ERROR_MESSAGE            OUT VARCHAR2);

    PROCEDURE check_resubmission_required (
        IN_RESUBMISSION_ID          IN  FMW_RESUBMISSION_COUNT_TBL.RESUBMISSION_ID%TYPE,
        IN_MESSAGE_FLOW_ID          IN  FMW_RESUBMISSION_COUNT_TBL.MESSAGE_FLOW_ID%TYPE ,
        IN_MESSAGE_HASH             IN  FMW_RESUBMISSION_COUNT_TBL.MESSAGE_HASH%TYPE,
        IN_RETRY_COUNT              IN  NUMBER,
        IN_STATUS                   IN  FMW_RESUBMISSION_COUNT_TBL.STATUS%TYPE,
        IN_FLEX_ATTRIBUTE1          IN  FMW_RESUBMISSION_COUNT_TBL.FLEX_ATTRIBUTE1%TYPE,
        OUT_FLAG                    OUT VARCHAR2,
        OUT_MSG_HASH_COUNT          OUT NUMBER); 
    
END fmw_resubmission_pkg;