create or replace PACKAGE BODY                               cct_top50_parts_pkg AS
/* Modification History
  PERSON        DATE         COMMENTS
  ------------  -----------  ------------------------------------------------------
  Infosys       14-Aug-2018  Calculate and insert top50 queue and parts combination
 */

 /*Proc to get the top 50 parts*/

    PROCEDURE get_top50_parts ( p_in_quote_rev_id   IN cct_quote_parts.quote_revision_id%TYPE
                               ,p_out_num_ret_code OUT NUMBER
                               ,p_out_chr_err_msg OUT VARCHAR2
  )
   AS
   l_code VARCHAR2(50);
   l_msg VARCHAR2(500);	
   exc_exit_prog EXCEPTION;
   l_num_tot_sum_cost NUMBER;
  BEGIN	
  
  -- execute immediate 'alter session set max_dump_file_size=unlimited';
  -- execute immediate 'alter session set tracefile_identifier="XXCCT_PARTS"';
  -- execute immediate 'alter session set events ''10046 trace name context forever, level 12''';
  
    p_out_num_ret_code := 0;
    IF p_in_quote_rev_id is NULL
    THEN 
    	l_msg :='Quote Revision ID is null';
    	l_code := 'TOP50-001';
    	RAISE exc_exit_prog;
    END IF;
    BEGIN
    	DELETE 
    	  FROM xxcct.cct_top50_parts
         WHERE quote_revision_id = p_in_quote_rev_id;
         
        dbms_output.put_line('entered  procedure');


        SELECT ROUND(SUM((NVL(NVL(cqp.suggested_cost_used_for_quote,cqp.cost_used_for_quote),0) * NVL(NVL(suggested_total_qty_len_con,cqp.total_qty_len_contract),0)) ),2) tot_summary_cost
	  INTO l_num_tot_sum_cost
	  FROM cct_quote_parts cqp
	 WHERE quote_revision_id = p_in_quote_rev_id;

        INSERT 
          INTO xxcct.cct_top50_parts (
            quote_revision_id,
            quoted_part_number,
            customer_pn,
            total_cost,
            queue_name,
            queue_id,
	    line_identifier,
            rank,
            annual_qty,
	    total_qty_len_contract,
	    uom,
	    certified_part,
	    prime,
	    manf_name,
	    cost_used_for_quote,
	    EXTENDED_COST,
	    ind_perc_cost,
	    CUMULATIVE_PERC_COST,
	    internal_notes,
	    external_notes 
        ) SELECT
            quote_revision_id,
            quoted_part_number,
            customer_pn,
            NVL(total_cost,0),
            queue_name,
            queue_id,
	    line_identifier,
            cost_rank,
            annual_qty, /* new */
            NVL(total_qty_len_contract,0),
            uom,
            certified_part,
	    prime,
	    NVL(SUGGESTED_MANF_NAME,manf_name),
	    NVL(cost_used_for_quote,0),
            NVL(total_cost,0),
            ind_perc_cost,
            SUM(ind_perc_cost) over (PARTITION BY quote_revision_id order by cost_rank) as CUM_PERC_COST,
	    internal_notes,
	    external_notes            
        FROM
            (
                SELECT
                   a.*,
                    ROW_NUMBER() OVER(PARTITION BY
                        a.quote_revision_id
                        ORDER BY
                            a.total_cost
                        DESC,
                            a.line_identifier
                        ASC
                    ) AS cost_rank,
                     CASE WHEN NVL(l_num_tot_sum_cost,0) = 0 THEN 0
                    ELSE ROUND(100*(a.total_cost/l_num_tot_sum_cost),2) END as ind_perc_cost
                FROM
                    (
                        SELECT
                            cqp.quote_revision_id,
                            cqp.line_identifier,
                            cqp.quoted_part_number,
                            cqp.customer_pn,
                            cpq.queue_name,
                            cqp.queue_id,
                            ( nvl(
                                nvl(
                                    cqp.suggested_cost_used_for_quote,
                                    cqp.cost_used_for_quote
                                ),
                                0
                            ) * nvl(
                                nvl(
                                    suggested_total_qty_len_con,
                                    cqp.total_qty_len_contract
                                ),
                                0
                            ) ) AS total_cost,
                            cqp.annual_qty,
			    NVL(suggested_total_qty_len_con,cqp.total_qty_len_contract) as total_qty_len_contract,
                            cqp.uom,
			    cqp.certified_part,
			    cqp.prime,
			    cqp.manf_name,
                            cqp.SUGGESTED_MANF_NAME,
			    NVL(cqp.suggested_cost_used_for_quote,cqp.cost_used_for_quote) as cost_used_for_quote,
			    cqp.internal_notes,
			    cqp.external_notes
                        FROM
                            cct_quote_parts cqp,
                            cct_parts_queue cpq
                        WHERE
                                cqp.queue_id = cpq.queue_id
                            AND
                                cqp.quote_revision_id = p_in_quote_rev_id
                    ) a
            )
        WHERE
            cost_rank <= 50
        ORDER BY cost_rank;

       COMMIT;


       	/*call to procedure get_top50_prod_queue to get the top prod queue parts combination*/
       BEGIN
          get_top50_prod_queue( p_in_quote_rev_id );
       EXCEPTION
          WHEN OTHERS THEN
             l_code := sqlcode;
             p_out_num_ret_code := 2;
            l_msg := substr(
                sqlerrm,
                1,
                500
            );
           cct_create_errors.record_error(
                p_quote_revision_id   => p_in_quote_rev_id,
                p_error_message       => l_msg,
                p_error_type          => l_code,
                p_module_name         => 'CCT_TOP50_PROD_QUEUE_PKG.cct_get_top50_parts'
            );
         END;

      EXCEPTION
          WHEN OTHERS THEN
             ROLLBACK;
             p_out_num_ret_code := 2;
            l_code := sqlcode;
            l_msg := substr(
                sqlerrm,
                1,
                500
            );
           cct_create_errors.record_error(
                p_quote_revision_id   => p_in_quote_rev_id,
                p_error_message       => l_msg,
                p_error_type          => l_code,
                p_module_name         => 'CCT_TOP50_PROD_QUEUE_PKG.cct_get_top50_parts'
            );
         END;


    EXCEPTION
        WHEN EXC_EXIT_PROG THEN
                   p_out_num_ret_code := 2;
 	           cct_create_errors.record_error(
	              p_quote_revision_id   => p_in_quote_rev_id,
	               P_ERROR_MESSAGE    => l_msg,
	               P_ERROR_TYPE       => l_code,
	               P_MODULE_NAME      => 'CCT_TOP50_PROD_QUEUE_PKG.cct_get_top50_parts',
            P_CREATED_BY       => sys_context('USERENV','OS_USER'));
        WHEN OTHERS THEN
            l_code := sqlcode;
            l_msg := substr(
                sqlerrm,
                1,
                500
            );
           cct_create_errors.record_error(
                p_quote_revision_id   => p_in_quote_rev_id,
                p_error_message       => l_msg,
                p_error_type          => l_code,
                p_module_name         => 'CCT_TOP50_PROD_QUEUE_PKG.cct_get_top50_parts'
            );

    END get_top50_parts;

 /*Proc to get the top prod queue summary*/

    PROCEDURE get_top50_prod_queue ( p_in_quote_rev_id   IN cct_quote_parts.quote_revision_id%TYPE )
        AS
        l_code VARCHAR2(50);
        l_msg VARCHAR2(500);	
    BEGIN
 -- execute immediate 'alter session set max_dump_file_size=unlimited';
 -- execute immediate 'alter session set tracefile_identifier="XXCCT_QUEUE"';
 -- execute immediate 'alter session set events ''10046 trace name context forever, level 12''';
 

    BEGIN
	 DELETE
	   FROM xxcct.cct_top50_prod_queue
          WHERE quote_revision_id=p_in_quote_rev_id;

        INSERT INTO xxcct.cct_top50_prod_queue (
            quote_revision_id,
            queue_name,
            poc,
            count_pn_with_cost,
	    total_cost,
            pct_of_total_cost,
            queue_id
        ) SELECT
            p_in_quote_rev_id,
            queue_name,
            poc,
            count_pn_cost,
	    total_cost,
            pct_of_total_cost,
            queue_id
           FROM
             (
              SELECT
                 p_in_quote_rev_id,
                 a.queue_name,
                 a.queue_id,
                 NULL AS poc,
                 nvl(
                   b.count_pn_with_cost,
                   0
                    ) AS count_pn_cost,
                 nvl(
                     b.top50_queue_cost,
                     0
                    ) AS total_cost,
                CASE
                    WHEN nvl(
                        d.all_extended_cost,
                        0
                    ) = 0THEN 0
                    ELSE round(
                        100 * (nvl(
                            b.top50_queue_cost,
                            0
                        ) / d.all_extended_cost),
                        4
                    )
                END
            AS pct_of_total_cost
        FROM
            (
                SELECT  
                    cpq.queue_id,
                    cpq.queue_name
                FROM
                   cct_parts_queue cpq
                WHERE cpq.queue_id <> 9
            ) a,
            (
                SELECT
                    ctqp.queue_name,
                    SUM(ctqp.total_cost) AS top50_queue_cost,
                    COUNT(ctqp.customer_pn) count_pn_with_cost
                FROM
                    cct_top50_parts ctqp
                WHERE quote_revision_id = p_in_quote_rev_id
                GROUP BY
                    ctqp.queue_name
                ORDER BY
                   ctqp.queue_name
            ) b,
              (
                SELECT
                    SUM(NVL(cqp.SUGGESTED_COST_USED_FOR_QUOTE,cqp.COST_USED_FOR_QUOTE) * NVL(suggested_TOTAL_QTY_LEN_CON,cqp.total_qty_len_contract)) all_extended_cost
                FROM
                    cct_quote_parts cqp
                WHERE quote_revision_id = p_in_quote_rev_id
            ) d
        WHERE
              a.queue_name = b.queue_name (+)

         )
       order by queue_id;

       COMMIT;


       EXCEPTION
              WHEN OTHERS THEN
                 ROLLBACK;
                l_code := sqlcode;
                l_msg := substr(
                    sqlerrm,
                    1,
                    500
                );
               cct_create_errors.record_error(
                    p_quote_revision_id   => p_in_quote_rev_id,
                    p_error_message       => l_msg,
                    p_error_type          => l_code,
                    p_module_name         => 'CCT_TOP50_PROD_QUEUE_PKG.cct_get_top50_parts'
                );
         END;


    EXCEPTION
        WHEN OTHERS THEN
            l_code := sqlcode;
            l_msg := substr(
                sqlerrm,
                1,
                500
            );
           cct_create_errors.record_error(
                p_quote_revision_id   => p_in_quote_rev_id,
                p_error_message       => l_msg,
                p_error_type          => l_code,
                p_module_name         => 'CCT_TOP50_PROD_QUEUE_PKG.GET_TOP50_PROD_QUEUE'
            );

    END get_top50_prod_queue;

END cct_top50_parts_pkg;