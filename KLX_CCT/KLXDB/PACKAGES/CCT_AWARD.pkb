create or replace PACKAGE BODY cct_award
AS
/**Modification History
    PERSON        DATE         COMMENTS
    ------------  -----------  ---------------------------------------------
    Ankit Bhatt   26-Sep-2017  Created map_part_with_opportunity proc
    Sureka        04-Oct-2017  Created update_award_headers proc
    Ankit Bhatt   17-Oct-2017  Modified map_part_with_opportunity procdure with
                               addtional conditions
    Sureka        18-Oct-2017  Created update_awards_summary proc
	Ankit Bhatt   06-Nov-2017  Modified map_part_with_opportunity and update_awards_summary
                               procedures with cct_quote_parts_snapshot_final table.
                               Modified update_award_headers procedures to update opportunity id
                               correponding to opportunity number in cct_awards table
    Sureka        09-Nov-2017  Created delete_awards_details proc
**/

    PROCEDURE map_part_with_opportunity(
      P_AWARD_HEADER_ID     IN CCT_AWARD_HEADERS.AWARD_HEADER_ID%TYPE)
    AS
        CURSOR award_cur(
            P_AWARD_HEADER_ID   IN CCT_AWARD_HEADERS.AWARD_HEADER_ID%TYPE)
        IS
            SELECT DISTINCT a.invoice_part_number,
                   COUNT(b.opportunity_id) count_opportunity_id
              FROM (SELECT DISTINCT ca.invoice_part_number,
                           cs.snapshot_id,
                           cah.opportunity_id
                      FROM cct_award_headers cah,
                           cct_awards ca,
                           cct_quote_parts_snapshot_final cqps,
                           cct_snapshot cs
                     WHERE ca.invoice_part_number = cqps.quoted_part_number AND
                           cqps.snapshot_id = cs.snapshot_id AND
                           cs.opportunity_id = cah.opportunity_id AND
                           cah.award_header_id = p_award_header_id) a,
                   (SELECT DISTINCT cah.opportunity_id,
                           MAX(cqa.snapshot_id) AS snapshot_id
                      FROM cct_award_headers cah,
                           cct_quote_approvals cqa
                     WHERE cah.award_header_id = P_AWARD_HEADER_ID AND
                           ((upper(cqa.approval_status) = 'APPROVED') OR
                            (upper(cqa.approval_status) = 'PASSED BY PROPOSAL MANAGER')) AND
                           cqa.opportunity_id = cah.opportunity_id
                  GROUP BY cah.opportunity_id ) b
             WHERE a.opportunity_id = b.opportunity_id AND
                   a.snapshot_id = b.snapshot_id
          GROUP BY a.invoice_part_number;
        v_oppty_number   VARCHAR2(50);
        v_count          NUMBER;
    BEGIN
        FOR award_rec IN award_cur(p_award_header_id)
        LOOP
            IF award_rec.count_opportunity_id = 1 THEN
                SELECT DISTINCT co.opportunity_number
                  INTO v_oppty_number
                  FROM cct_opportunity co
                 WHERE co.opportunity_id = (SELECT DISTINCT source_opportunity_id
                                              FROM cct_award_headers
                                             WHERE award_header_id = P_AWARD_HEADER_ID);

                UPDATE cct_awards ca
                   SET ca.opportunity_id = (SELECT DISTINCT source_opportunity_id
                                              FROM cct_award_headers
                                             WHERE award_header_id = P_AWARD_HEADER_ID),
                       ca.opportunity_number = v_oppty_number,
                       ca.multi_opportunity_flag = NULL
                 WHERE ca.award_header_id IN (
                            SELECT cah.award_header_id
                              FROM cct_award_headers cah
                             WHERE ca.award_header_id = cah.award_header_id AND
                                   ca.award_header_id = P_AWARD_HEADER_ID) AND
                                   ca.invoice_part_number = award_rec.invoice_part_number;
            ELSIF award_rec.count_opportunity_id > 1 THEN
                SELECT LISTAGG(co.opportunity_number,',')
                        WITHIN GROUP(ORDER BY co.opportunity_number) AS opportunity_number
                  INTO v_oppty_number
                  FROM cct_opportunity co
                 WHERE co.opportunity_id IN (
                            SELECT DISTINCT opportunity_id
                              FROM cct_award_headers
                             WHERE award_header_id = P_AWARD_HEADER_ID );

                UPDATE cct_awards ca
                   SET ca.opportunity_id = NULL,
                       ca.opportunity_number = v_oppty_number,
                       ca.multi_opportunity_flag = 'Yes'
                 WHERE ca.award_header_id IN (
                            SELECT cah.award_header_id
                              FROM cct_award_headers cah
                             WHERE ca.award_header_id = cah.award_header_id AND
                                   ca.award_header_id = p_award_header_id) AND
                                   ca.invoice_part_number = award_rec.invoice_part_number;
            END IF;
        END LOOP;
        COMMIT;
        update_awards_summary(P_AWARD_HEADER_ID);
    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg  := substr(sqlerrm,1,500);
        cct_create_errors.record_error(
            p_error_message   => g_msg,
            p_error_type      => g_code,
            p_module_name     => 'cct_award.map_part_with_opportunity',
            p_created_by      => sys_context('USERENV','OS_USER')
        );
    END map_part_with_opportunity;

    PROCEDURE update_award_headers(
      P_OPPORTUNITY_ID      IN CCT_AWARD_HEADERS.OPPORTUNITY_ID%TYPE)
    AS
        CURSOR opp_list IS
            SELECT DISTINCT opportunity_number
              FROM cct_awards
             WHERE award_header_id IN (
                        SELECT DISTINCT award_header_id
                          FROM cct_award_headers
                         WHERE opportunity_id = P_OPPORTUNITY_ID) AND
                   opportunity_number IS NOT NULL;
        l_opp_num              CCT_AWARDS.OPPORTUNITY_NUMBER%TYPE;
        l_opp_id               CCT_AWARD_HEADERS.OPPORTUNITY_ID%TYPE;
        l_award_header_id      CCT_AWARD_HEADERS.AWARD_HEADER_ID%TYPE;
        l_award_filename       CCT_AWARD_HEADERS.AWARD_FILENAME%TYPE;
        l_source_opty_id       CCT_AWARD_HEADERS.SOURCE_OPPORTUNITY_ID%TYPE;
        l_count                NUMBER;
    BEGIN

        delete_awards_details(P_OPPORTUNITY_ID);

        SELECT DISTINCT award_header_id,
               award_filename,
               source_opportunity_id
          INTO l_award_header_id,
               l_award_filename,
               l_source_opty_id
          FROM cct_award_headers
         WHERE opportunity_id = P_OPPORTUNITY_ID;

        OPEN opp_list;
        LOOP
            FETCH opp_list INTO l_opp_num;
            EXIT WHEN opp_list%notfound;
            cct_fetch_fields.get_opty_id(
                p_opportunity_number   => l_opp_num,
                p_opportunity_id       => l_opp_id
            );

			--Update cct_awards table with corresponding opportunity id for the fetched opportunity number from the cursor
            UPDATE cct_awards
               SET opportunity_id = l_opp_id
             WHERE opportunity_number = l_opp_num AND
				   award_header_id = (SELECT award_header_id
										FROM cct_award_headers
									   WHERE opportunity_id = p_opportunity_id);

            SELECT COUNT(*)
              INTO l_count
              FROM cct_award_headers
             WHERE award_header_id = l_award_header_id AND
                   opportunity_id = l_opp_id;

            IF l_count = 0 THEN
                INSERT INTO cct_award_headers (
                    award_header_id,
                    opportunity_id,
                    award_filename,
                    source_opportunity_id
                ) VALUES (
                    l_award_header_id,
                    l_opp_id,
                    l_award_filename,
                    l_source_opty_id
                );
            END IF;
            COMMIT;

        END LOOP;
        CLOSE opp_list;
        update_awards_summary(l_award_header_id);
    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg  := substr(sqlerrm,1,500);
        cct_create_errors.record_error(
            p_error_message   => g_msg,
            p_error_type      => g_code,
            p_module_name     => 'cct_award.update_award_headers',
            p_created_by      => sys_context('USERENV','OS_USER')
        );
    END update_award_headers;

    PROCEDURE update_awards_summary(
        P_AWARD_HEADER_ID     IN CCT_AWARD_HEADERS.AWARD_HEADER_ID%TYPE)
    AS
        CURSOR cur_opportunity
        IS
            SELECT DISTINCT opportunity_number 
              FROM cct_awards
             WHERE award_header_id = P_AWARD_HEADER_ID AND 
                   opportunity_number IS NOT NULL;
        l_opp_number              CCT_AWARDS.OPPORTUNITY_NUMBER%TYPE;
        l_opp_id                  CCT_AWARDS_SUMMARY.OPPORTUNITY_ID%TYPE;
        l_total_parts             CCT_AWARDS_SUMMARY.TOTAL_PARTS%TYPE;
        l_total_quoted            CCT_AWARDS_SUMMARY.TOTAL_QUOTED%TYPE;
        l_quoted_and_awarded      CCT_AWARDS_SUMMARY.QUOTED_AND_AWARDED%TYPE;
        l_quoted_and_notawarded   CCT_AWARDS_SUMMARY.QUOTED_AND_NOTAWARDED%TYPE;
        l_notquoted_and_awarded   CCT_AWARDS_SUMMARY.NOTQUOTED_AND_AWARDED%TYPE;
        l_pct_win                 CCT_AWARDS_SUMMARY.PCT_WIN%TYPE;
        l_oppty_id_temp           CCT_AWARDS_SUMMARY.OPPORTUNITY_ID%TYPE;
    BEGIN

        BEGIN
           SELECT opportunity_id
             INTO l_oppty_id_temp
             FROM cct_award_headers
            WHERE award_header_id = P_AWARD_HEADER_ID
              AND rownum=1;
        EXCEPTION
        WHEN OTHERS THEN
            g_code := sqlcode;
            g_msg  := substr(sqlerrm,1,500);
            cct_create_errors.record_error(
                p_error_message   => g_msg,
                p_error_type      => g_code,
                p_module_name     => 'cct_award.update_awards_summary optyId',
                p_created_by      => sys_context('USERENV','OS_USER')
            );
        END;

        DELETE FROM cct_awards_summary
         WHERE award_header_id IN (SELECT award_header_id
                                     FROM cct_award_headers
                                    WHERE opportunity_id = l_oppty_id_temp AND
                                          rownum=1);
        COMMIT;
        OPEN cur_opportunity;
        LOOP
            FETCH cur_opportunity INTO l_opp_number;
            EXIT WHEN cur_opportunity%notfound;

            /**IF l_opp_number = 'Sales Quote' THEN
                SELECT NULL,
                       COUNT(invoice_part_number)
                  INTO l_opp_id,
                       l_total_parts
                  FROM cct_awards
                 WHERE award_header_id = P_AWARD_HEADER_ID AND
                       opportunity_number is NULL;
            ELSE
                SELECT DISTINCT opportunity_id,
                       COUNT(invoice_part_number)
                  INTO l_opp_id,
                       l_total_parts
                  FROM cct_awards
                 WHERE award_header_id = P_AWARD_HEADER_ID AND
                       opportunity_number = l_opp_number
                GROUP BY opportunity_id;
            END IF;**/

            SELECT DISTINCT opportunity_id
              INTO l_opp_id
              FROM cct_awards
             WHERE award_header_id = P_AWARD_HEADER_ID AND
                   opportunity_number = l_opp_number;

            l_total_quoted := 0;
            l_quoted_and_awarded := 0;
            l_quoted_and_notawarded := 0;
            l_notquoted_and_awarded := 0;
            l_pct_win := 0;
            l_total_parts := 0;

            IF INSTR(l_opp_number,',') = 0 THEN
            ---Awarded and Not Quoted---
            /**    SELECT COUNT(AwardsReviewEO.customer_reference)
                  INTO l_notquoted_and_awarded
                  FROM (SELECT DISTINCT ca.opportunity_id,
                               ca.awarded_part_number,
                               ca.customer_reference
                          FROM cct_awards ca
                         WHERE ca.award_header_id = P_AWARD_HEADER_ID AND
                               ca.opportunity_id = l_opp_id) AwardsReviewEO
                 WHERE NOT EXISTS
                       (SELECT DISTINCT *
                          FROM (SELECT DISTINCT cs.opportunity_id,
                                       cqps.quoted_part_number,
                                       cqps.customer_pn
                                  FROM cct_quote_parts_snapshot_final cqps,
                                       cct_snapshot cs,
                                       (SELECT DISTINCT cah.opportunity_id,
                                               MAX(cqa.snapshot_id) AS snapshot_id
                                          FROM cct_award_headers cah,
                                               cct_quote_approvals cqa
                                         WHERE cah.award_header_id = P_AWARD_HEADER_ID AND
                                               ((UPPER(cqa.approval_status) = 'APPROVED') OR
                                                (UPPER(cqa.approval_status) = 'PASSED BY PROPOSAL MANAGER')) AND
                                               cqa.opportunity_id = cah.opportunity_id AND
                                               cah.opportunity_id = l_opp_id
                                         GROUP BY cah.opportunity_id) co
                                 WHERE cs.snapshot_id = co.snapshot_id AND
                                       cs.snapshot_id = cqps.snapshot_id AND
                                       cs.opportunity_id = l_opp_id AND
                                       (cqps.cost_used_for_quote IS NOT NULL OR
                                       cqps.resale IS NOT NULL)) QuotePartsEO
                          where QuotePartsEO.opportunity_id = AwardsReviewEO.opportunity_id AND
                                QuotePartsEO.quoted_part_number = AwardsReviewEO.awarded_part_number AND
                                QuotePartsEO.customer_pn = AwardsReviewEO.customer_reference);**/
                SELECT count(*)
                  INTO l_notquoted_and_awarded
                  FROM (SELECT ca.awarded_part_number
                          FROM cct_awards ca
                         WHERE ca.award_header_id = P_AWARD_HEADER_ID AND 
                               ca.opportunity_number IS NULL);
            ---Quoted and Not Awarded---
                SELECT COUNT(QuotePartsEO.customer_pn)
                  INTO l_quoted_and_notawarded
                  FROM (SELECT cs.opportunity_id, 
                               cqps.quoted_part_number,
                               cqps.customer_pn
                          FROM cct_quote_parts_snapshot_final cqps,
                               cct_snapshot cs,
                               (SELECT DISTINCT cah.opportunity_id,
                                       MAX(cqa.snapshot_id) AS snapshot_id
                                  FROM cct_award_headers cah,
                                       cct_quote_approvals cqa
                                 WHERE cah.award_header_id = P_AWARD_HEADER_ID AND
                                       cah.opportunity_id = l_opp_id AND
                                       ((UPPER(cqa.approval_status) = 'APPROVED') OR
                                        (UPPER(cqa.approval_status) = 'PASSED BY PROPOSAL MANAGER')) AND
                                       cqa.opportunity_id = cah.opportunity_id
                                 GROUP BY cah.opportunity_id) co
                         WHERE cs.snapshot_id = co.snapshot_id AND
                               cs.snapshot_id = cqps.snapshot_id AND
                               cqps.cost_used_for_quote IS NOT NULL AND
                               cqps.resale IS NOT NULL) QuotePartsEO
                 WHERE NOT EXISTS
                       (SELECT DISTINCT opportunity_id,
                                awarded_part_number,
                                customer_reference
                           FROM cct_awards AwardsReviewEO
                          WHERE award_header_id = P_AWARD_HEADER_ID AND
                                opportunity_id = l_opp_id AND
                                QuotePartsEO.opportunity_id = AwardsReviewEO.opportunity_id AND
                                QuotePartsEO.quoted_part_number = AwardsReviewEO.awarded_part_number);

            ---Quoted and Awarded---
            /**    SELECT COUNT(AwardsReviewEO.customer_reference)
                  INTO l_quoted_and_awarded
                  FROM (SELECT DISTINCT ca.opportunity_id,
                               ca.awarded_part_number,
                               ca.customer_reference
                          FROM cct_awards ca
                         WHERE ca.award_header_id = P_AWARD_HEADER_ID AND
                               ca.opportunity_id = l_opp_id) AwardsReviewEO
                INNER JOIN
                       (SELECT DISTINCT *
                          FROM (SELECT DISTINCT cs.opportunity_id,
                                       cqps.quoted_part_number,
                                       cqps.customer_pn
                                  FROM cct_quote_parts_snapshot_final cqps,
                                       cct_snapshot cs,
                                       (SELECT DISTINCT cah.opportunity_id,
                                               MAX(cqa.snapshot_id) AS snapshot_id
                                          FROM cct_award_headers cah,
                                               cct_quote_approvals cqa
                                         WHERE cah.award_header_id = P_AWARD_HEADER_ID AND
                                               cah.opportunity_id = l_opp_id AND
                                               ((UPPER(cqa.approval_status) = 'APPROVED') OR
                                                (UPPER(cqa.approval_status) = 'PASSED BY PROPOSAL MANAGER')) AND
                                               cqa.opportunity_id = cah.opportunity_id
                                         GROUP BY cah.opportunity_id) co
                                 WHERE cs.snapshot_id = co.snapshot_id AND
                                       cs.snapshot_id = cqps.snapshot_id AND
                                       cqps.cost_used_for_quote IS NOT NULL AND
                                       cqps.resale IS NOT NULL)) QuotePartsEO
                          ON QuotePartsEO.opportunity_id = AwardsReviewEO.opportunity_id AND
                             QuotePartsEO.quoted_part_number = AwardsReviewEO.awarded_part_number AND
                             QuotePartsEO.customer_pn = AwardsReviewEO.customer_reference;**/
                SELECT count(*)
                  INTO l_quoted_and_awarded
                  FROM (SELECT ca.awarded_part_number
                          FROM cct_awards ca
                         WHERE ca.award_header_id = P_AWARD_HEADER_ID AND
                               ca.opportunity_id = l_opp_id);

            ---Quoted---
                SELECT COUNT(cqps.customer_pn)
                  INTO l_total_quoted
                  FROM cct_quote_parts_snapshot_final cqps,
                      (SELECT MAX(cqa.snapshot_id) AS snapshot_id
                         FROM cct_quote_approvals cqa
                        WHERE ((upper(cqa.approval_status) = 'APPROVED') OR
                               (upper(cqa.approval_status) = 'PASSED BY PROPOSAL MANAGER')) AND
                              cqa.opportunity_id = l_opp_id) co
                WHERE co.snapshot_id = cqps.snapshot_id AND
                      cqps.cost_used_for_quote IS NOT NULL AND
                      cqps.resale IS NOT NULL;

                SELECT COUNT(*)
                  INTO l_total_parts
                  FROM cct_quote_parts_snapshot_final cqps,
                      (SELECT MAX(cqa.snapshot_id) AS snapshot_id
                         FROM cct_quote_approvals cqa
                        WHERE ((upper(cqa.approval_status) = 'APPROVED') OR
                               (upper(cqa.approval_status) = 'PASSED BY PROPOSAL MANAGER')) AND
                              cqa.opportunity_id = l_opp_id) co
                WHERE co.snapshot_id = cqps.snapshot_id;
            END IF;

            IF l_total_quoted = 0 THEN
                l_pct_win := 0;
            ELSE
                l_pct_win := round(((NVL(l_quoted_and_awarded,0) / NVL(l_total_quoted,0)) * 100),2);
            END IF;

            INSERT INTO cct_awards_summary(
                    award_header_id,
                    opportunity_id,
                    opportunity_number,
                    total_parts,
                    total_quoted,
                    quoted_and_awarded,
                    quoted_and_notawarded,
                    notquoted_and_awarded,
                    pct_win)
            VALUES (P_AWARD_HEADER_ID,
                    l_opp_id,
                    l_opp_number,
                    l_total_parts,
                    l_total_quoted,
                    l_quoted_and_awarded,
                    l_quoted_and_notawarded,
                    l_notquoted_and_awarded,
                    l_pct_win);
            COMMIT;
        END LOOP;
        CLOSE cur_opportunity;

    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg  := substr(sqlerrm,1,500);
        cct_create_errors.record_error(
            p_error_message   => g_msg,
            p_error_type      => g_code,
            p_module_name     => 'cct_award.update_awards_summary',
            p_created_by      => sys_context('USERENV','OS_USER')
        );
    END update_awards_summary;

    PROCEDURE delete_awards_details(
        P_SOURCE_OPTY_ID    IN CCT_AWARD_HEADERS.SOURCE_OPPORTUNITY_ID%TYPE)
    AS
        l_award_header_id      CCT_AWARD_HEADERS.AWARD_HEADER_ID%TYPE;
    BEGIN

        SELECT NVL((SELECT DISTINCT award_header_id
                      FROM cct_award_headers
                     WHERE source_opportunity_id = P_SOURCE_OPTY_ID),0)
          INTO l_award_header_id
          FROM dual;

        DELETE FROM cct_award_headers
            WHERE award_header_id IN (SELECT award_header_id
                                        FROM cct_award_headers
                                       WHERE (OPPORTUNITY_ID = P_SOURCE_OPTY_ID OR
                                             SOURCE_OPPORTUNITY_ID = P_SOURCE_OPTY_ID) AND
                                             award_header_id <> l_award_header_id);
        COMMIT;

        DELETE FROM cct_awards
            WHERE award_header_id IN (SELECT award_header_id
                                        FROM cct_award_headers
                                       WHERE (OPPORTUNITY_ID = P_SOURCE_OPTY_ID OR
                                             SOURCE_OPPORTUNITY_ID = P_SOURCE_OPTY_ID) AND
                                             award_header_id <> l_award_header_id);
        COMMIT;

        DELETE FROM cct_awards_summary
            WHERE award_header_id IN (SELECT award_header_id
                                        FROM cct_award_headers
                                       WHERE (OPPORTUNITY_ID = P_SOURCE_OPTY_ID OR
                                             SOURCE_OPPORTUNITY_ID = P_SOURCE_OPTY_ID) AND
                                             award_header_id <> l_award_header_id);
        COMMIT;

    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg  := substr(sqlerrm,1,500);
        cct_create_errors.record_error(
            p_error_message   => g_msg,
            p_error_type      => g_code,
            p_module_name     => 'cct_award.delete_awards_details',
            p_created_by      => sys_context('USERENV','OS_USER')
        );
    END delete_awards_details;

END cct_award;