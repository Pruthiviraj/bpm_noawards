--------------------------------------------------------
--  DDL for Package CCT_QUOTE_CREATION
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "XXCCT"."CCT_QUOTE_CREATION" AS
/**Handles quote creation and revision and duplicating quote parts
   Modification History
   Author        Date         COMMENTS
   ------------  -----------  ----------------------------------------
   Ankit Bhatt   05-Jul-2017  Package for Quote Creation and Revisions
   Sureka        17-Jul-2017  copy_quote_parts procedure created
   Sureka        19-Jul-2017  modified out parameters of create_quote
   Ankit Bhatt   27-Jul-2017  created new procedure copy_quote_revision_with_parts
                              and added out parameter in create_quote_revisions procedure.
   Sureka        05-Mar-2018  modified copy_quote_parts procedure 
**/
    g_quote_status 			    VARCHAR2(50) := 'ACTIVE';
    g_new_revision_id 		    CCT_QUOTE_REVISIONS.REVISION_ID%TYPE;
    g_quote_id 				    CCT_QUOTE.QUOTE_ID%TYPE;
    g_max_quote_revision 	    CCT_QUOTE_REVISIONS.QUOTE_REVISION%TYPE;
    g_max_revision_id 		    CCT_QUOTE_REVISIONS.REVISION_ID%TYPE;
    g_initial_quote_rev 	    CCT_QUOTE_REVISIONS.QUOTE_REVISION%TYPE := '0.1';
    g_message 				    VARCHAR2(500);
    g_code 					    VARCHAR2(50);

    PROCEDURE create_quote (
        P_OPPORTUNITY_NUMBER IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE,
        P_QUOTE_NUMBER      OUT CCT_QUOTE.QUOTE_NUMBER%TYPE,
        P_QUOTE_REVISION    OUT CCT_QUOTE_REVISIONS.QUOTE_REVISION%TYPE,
        P_REVISION_ID       OUT CCT_QUOTE_REVISIONS.REVISION_ID%TYPE);

    PROCEDURE create_quote_revisions (
        P_OPPORTUNITY_NUMBER IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE,
        P_QUOTE_REVISION_ID OUT CCT_QUOTE_REVISIONS.REVISION_ID%TYPE,
        P_QUOTE_REV_NUM     OUT CCT_QUOTE_REVISIONS.QUOTE_REVISION%TYPE);

    PROCEDURE copy_quote_revision_with_parts (
        P_OPPORTUNITY_NUMBER IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE);

    PROCEDURE copy_quote_parts(
        P_OLD_REVISION_ID    IN CCT_QUOTE_REVISIONS.REVISION_ID%TYPE,
        P_NEW_REVISION_ID    IN CCT_QUOTE_REVISIONS.REVISION_ID%TYPE);

END cct_quote_creation;

/
