create or replace PACKAGE cct_create_task
AS
/**Handles CCT_TASK_STATUS loading
   Modification History
   PERSON        DATE         COMMENTS
   -----------  -----------  ---------------------------------------------
   Sureka       28-Jul-2017  Created insert_task_status proc
   Sureka       28-Jul-2017  Created update_task_status proc
   Sureka       10-Aug-2017  Modified insert_task_status to return due_date
   Ankit Bhatt  10-Oct-2017  Modified date format
   Ankit Bhatt  02-Nov-2017  Dropped procedure update_task_status
**/
 
    g_user_role              CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
    g_instance_id            CCT_ERROR_LOGS.INSTANCE_ID%TYPE;
    g_opp_number             CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE;
    g_quote_number           CCT_QUOTE.QUOTE_NUMBER%TYPE;
    g_quote_revision         CCT_QUOTE_REVISIONS.QUOTE_REVISION%TYPE;
    g_task_id                CCT_ERROR_LOGS.TASK_ID%TYPE;
    g_opp_id                 CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE;
    g_code                   VARCHAR2(50);
    g_msg                    VARCHAR2(500);
    
    PROCEDURE insert_task_status(
        P_TASK_OBJ        IN CCT_TASK_TYPE,
        P_TASK_SUBJECT   OUT CCT_ROLE_PROPERTIES.TASK_SUBJECT%TYPE,
        P_DUE_DATE       OUT DATE);
      
END cct_create_task;