create or replace PACKAGE BODY CCT_PROPERTIES_PKG
AS
/**Handles Properties table
   Modification History 
   PERSON        DATE         COMMENTS
   ------------  -----------  ---------------------------------------------
   Sureka        23-Aug-2017  Created insert_properties
   Sureka        23-Aug-2017  Created update_properties
   Sureka        23-Aug-2017  Created get_soap_param
**/

    PROCEDURE insert_properties(
        P_KEY            IN  CCT_PROPERTIES.KEY%TYPE,
        P_VALUE          IN  CCT_PROPERTIES.VALUE%TYPE DEFAULT NULL, 
        P_DESCRIPTION    IN  CCT_PROPERTIES.DESCRIPTION%TYPE DEFAULT NULL,
        P_CREATED_BY     IN  CCT_PROPERTIES.CREATED_BY%TYPE DEFAULT NULL,
        P_CREATED_DATE   IN  CCT_PROPERTIES.CREATED_DATE%TYPE DEFAULT NULL,
        P_RETURN_STATUS OUT  VARCHAR2)
    AS
    BEGIN   
        INSERT INTO CCT_PROPERTIES(
            KEY,
            VALUE,
            DESCRIPTION,
            CREATED_BY,
            CREATED_DATE)
        VALUES(
            P_KEY,
            P_VALUE,
            P_DESCRIPTION,
            NVL(P_CREATED_BY,sys_context('USERENV','OS_USER')),
            NVL(P_CREATED_DATE,sysdate));
        COMMIT;
        P_RETURN_STATUS := 'SUCCESS';
    EXCEPTION
    WHEN OTHERS THEN
        g_code          := SQLCODE;
        g_msg           := SUBSTR(SQLERRM, 1, 500);
        cct_create_errors.record_error(
			p_error_message    => g_msg,
			p_error_type       => g_code,
			p_module_name      => 'CCT_PROPERTIES_PKG.insert_properties',
			p_created_by       => sys_context('USERENV','OS_USER')
		);  
        P_RETURN_STATUS := 'FAILURE';
    END insert_properties;

    PROCEDURE update_properties(
        P_KEY            IN  CCT_PROPERTIES.KEY%TYPE,
        P_VALUE          IN  CCT_PROPERTIES.VALUE%TYPE DEFAULT NULL, 
        P_DESCRIPTION    IN  CCT_PROPERTIES.DESCRIPTION%TYPE DEFAULT NULL,
        P_MODIFIED_BY    IN  CCT_PROPERTIES.MODIFIED_BY%TYPE DEFAULT NULL,
        P_MODIFIED_DATE  IN  CCT_PROPERTIES.MODIFIED_DATE%TYPE DEFAULT NULL,
        P_RETURN_STATUS OUT  VARCHAR2)
    AS
    BEGIN
        UPDATE CCT_PROPERTIES
           SET VALUE = NVL(P_VALUE,VALUE), 
               DESCRIPTION =  NVL(P_DESCRIPTION,DESCRIPTION), 
               MODIFIED_BY = NVL(P_MODIFIED_BY,sys_context('USERENV','OS_USER')),  
               MODIFIED_DATE = NVL(P_MODIFIED_DATE,SYSDATE)  
         WHERE KEY = P_KEY;
        COMMIT;
        P_RETURN_STATUS := 'SUCCESS';
    EXCEPTION
    WHEN OTHERS THEN
        g_code          := SQLCODE;
        g_msg           := SUBSTR(SQLERRM, 1, 500);
        cct_create_errors.record_error(
			p_error_message    => g_msg,
			p_error_type       => g_code,
			p_module_name      => 'CCT_PROPERTIES_PKG.update_properties',
			p_created_by       => sys_context('USERENV','OS_USER')
		);  
        P_RETURN_STATUS := 'FAILURE';
    END update_properties;

    PROCEDURE get_soap_param(
        P_KEY            IN  CCT_PROPERTIES.KEY%TYPE,
        P_VALUE         OUT  CCT_PROPERTIES.VALUE%TYPE)
    AS
    BEGIN
        SELECT VALUE
          INTO P_VALUE
          FROM CCT_PROPERTIES 
         WHERE KEY = P_KEY;
    EXCEPTION
    WHEN OTHERS THEN
        g_code          := SQLCODE;
        g_msg           := SUBSTR(SQLERRM, 1, 500);
        cct_create_errors.record_error(
			p_error_message    => g_msg,
			p_error_type       => g_code,
			p_module_name      => 'CCT_PROPERTIES_PKG.get_soap_param',
			p_created_by       => sys_context('USERENV','OS_USER')
		);  
    END get_soap_param;

END CCT_PROPERTIES_PKG;