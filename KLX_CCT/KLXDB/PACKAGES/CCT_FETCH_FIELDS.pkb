create or replace PACKAGE BODY CCT_FETCH_FIELDS
AS
/**Handles fetching fields
   Modification History
   PERSON          DATE           COMMENTS
   ------------    -----------    ---------------------------------------------
   Sureka          14-Sep-2017    Created get_opty_id proc
   Sureka          14-Sep-2017    Created get_osc_opty_id proc
   Sureka          14-Sep-2017    Created get_opty_num proc
   Sureka          14-Sep-2017    Created get_latest_revision proc
   Sureka          14-Sep-2017    Created get_unknown_pd_count func
   Sureka          14-Sep-2017    Created get_queue_pd_count func
   Sureka          28-Sep-2017    Created get_latest_snapshot proc
   Sureka          25-Oct-2017    Created get_queue_pd_count_cost proc
   Sureka          26-Oct-2017    Created get_latest_approval proc
   Sureka          14-Dec-2017    Created get_proposal_comments proc
   Sureka          30-Jan-2018    Created get_approver_name proc
   Sureka          07-Feb-2018    Created get_crf_id proc
   Sureka          04-Apr-2018    Created get_payment_notes func
   Ankit Bhatt     28-Sep-2018    Created get_latest_approved_snapshot proc
**/

    PROCEDURE get_opty_id (
        P_OPPORTUNITY_NUMBER   IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE,
        P_OPPORTUNITY_ID      OUT CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE)
    AS
    BEGIN
        SELECT DISTINCT opportunity_id
          INTO P_OPPORTUNITY_ID
          FROM cct_opportunity
         WHERE opportunity_number = P_OPPORTUNITY_NUMBER;
    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            p_error_message   => g_msg,
            p_error_type      => g_code,
            p_module_name     => 'CCT_FETCH_FIELDS.get_opty_id',
            p_created_by      => sys_context('USERENV','OS_USER')
        );
    END get_opty_id;

    PROCEDURE get_osc_opty_id (
        P_OPPORTUNITY_NUMBER   IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE,
        P_OSC_OPPORTUNITY_ID  OUT CCT_OPPORTUNITY.OSC_OPPORTUNITY_ID%TYPE)
    AS
        l_opportunity_id          CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE;
    BEGIN
        get_opty_id(p_opportunity_number,l_opportunity_id);
        SELECT DISTINCT osc_opportunity_id
          INTO P_OSC_OPPORTUNITY_ID
          FROM cct_opportunity
         WHERE opportunity_number = P_OPPORTUNITY_NUMBER;
    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            p_opportunity_id   => l_opportunity_id,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_FETCH_FIELDS.get_osc_opty_id',
            p_created_by       => sys_context('USERENV','OS_USER')
        );
    END get_osc_opty_id;

    PROCEDURE get_opty_num (
        P_OPPORTUNITY_ID       IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE,
        P_OPPORTUNITY_NUMBER  OUT CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE)
    AS
    BEGIN
        SELECT DISTINCT opportunity_number
          INTO P_OPPORTUNITY_NUMBER
          FROM cct_opportunity
         WHERE opportunity_id = P_OPPORTUNITY_ID;
    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            p_opportunity_id   => P_OPPORTUNITY_ID,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_FETCH_FIELDS.get_opty_num',
            p_created_by       => sys_context('USERENV','OS_USER')
        );
    END get_opty_num;

    PROCEDURE get_latest_revision (
        P_OPPORTUNITY_NUMBER   IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE DEFAULT NULL,
        P_OPPORTUNITY_ID       IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE DEFAULT NULL,
        P_REVISION_ID         OUT CCT_QUOTE_REVISIONS.REVISION_ID%TYPE)
    AS
        l_opportunity_id          CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE;
    BEGIN
        IF P_OPPORTUNITY_ID IS NULL THEN
            get_opty_id(P_OPPORTUNITY_NUMBER,l_opportunity_id);
        ELSE
            l_opportunity_id := P_OPPORTUNITY_ID;
        END IF;

        SELECT MAX(cqr.revision_id)
          INTO P_REVISION_ID
          FROM cct_quote cq,
               cct_opportunity co,
               cct_quote_revisions cqr
         WHERE co.opportunity_id = cq.opportunity_id AND
               cq.quote_id = cqr.quote_id AND
               co.opportunity_id = l_opportunity_id;

        IF P_REVISION_ID is NULL THEN
            P_REVISION_ID := 0;
        END IF;

    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            p_opportunity_id   => l_opportunity_id,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_FETCH_FIELDS.get_latest_revision',
            p_created_by       => sys_context('USERENV','OS_USER')
        );
    END get_latest_revision;

    PROCEDURE get_latest_snapshot (
        P_OPPORTUNITY_NUMBER   IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE DEFAULT NULL,
        P_OPPORTUNITY_ID       IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE DEFAULT NULL,
        P_SNAPSHOT_TYPE        IN CCT_SNAPSHOT.SNAPSHOT_TYPE%TYPE DEFAULT NULL,
        P_SNAPSHOT_ID         OUT CCT_SNAPSHOT.SNAPSHOT_ID%TYPE)
    AS
        l_opportunity_id          CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE;
    BEGIN
        IF P_OPPORTUNITY_ID IS NULL THEN
            get_opty_id(P_OPPORTUNITY_NUMBER,l_opportunity_id);
        ELSE
            l_opportunity_id := P_OPPORTUNITY_ID;
        END IF;

        IF P_SNAPSHOT_TYPE = 'APPROVALS' THEN
            SELECT MAX(cs.snapshot_id)
              INTO P_SNAPSHOT_ID
              FROM cct_snapshot cs
             WHERE cs.opportunity_id = l_opportunity_id AND
                   UPPER(cs.snapshot_type) = 'APPROVAL';
        ELSIF P_SNAPSHOT_TYPE = 'APPROVED' THEN
            SELECT MAX(cqa.snapshot_id)
              INTO P_SNAPSHOT_ID
              FROM cct_quote_approvals cqa
             WHERE cqa.opportunity_id = l_opportunity_id AND
                   (UPPER(cqa.approval_status) = 'APPROVED' OR
                    UPPER(cqa.approval_status) = 'PASSED BY PROPOSAL MANAGER');
        ELSE
            SELECT MAX(cs.snapshot_id)
              INTO P_SNAPSHOT_ID
              FROM cct_snapshot cs
             WHERE cs.opportunity_id = l_opportunity_id AND
                   UPPER(cs.snapshot_type) <> 'APPROVAL';
        END IF;

        IF P_SNAPSHOT_ID is NULL THEN
            P_SNAPSHOT_ID := 0;
        END IF;

    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            p_opportunity_id   => l_opportunity_id,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_FETCH_FIELDS.get_latest_snapshot',
            p_created_by       => sys_context('USERENV','OS_USER')
        );
    END get_latest_snapshot;

    PROCEDURE get_latest_approval (
        P_SNAPSHOT_ID          IN CCT_QUOTE_APPROVALS.SNAPSHOT_ID%TYPE,
        P_APPROVAL_ID         OUT CCT_QUOTE_APPROVALS.APPROVAL_ID%TYPE)
    AS
    BEGIN
        SELECT  MAX(approval_id)
          INTO  P_APPROVAL_ID
          FROM  cct_quote_approvals
         WHERE  snapshot_id = P_SNAPSHOT_ID;
    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_FETCH_FIELDS.get_latest_approval',
            p_created_by       => sys_context('USERENV','OS_USER')
        );
    END get_latest_approval;

    PROCEDURE get_proposal_comments (
        P_OPPORTUNITY_ID       IN CCT_SNAPSHOT.OPPORTUNITY_ID%TYPE,
        P_SNAPSHOT_VERSION     IN CCT_SNAPSHOT.SNAPSHOT_VERSION%TYPE,
        P_PROPOSAL_COMMENTS   OUT CLOB)
    AS
        l_comments              CCT_APPROVAL_REQUEST.COMMENTS%TYPE;
        CURSOR cur_approvals_list
        IS
            SELECT car.comments
              FROM cct_approval_request car,
                   cct_snapshot cs
             WHERE car.snapshot_id = cs.snapshot_id AND
                   cs.opportunity_id = P_OPPORTUNITY_ID AND
                   cs.snapshot_version = P_SNAPSHOT_VERSION AND
                   car.comments IS NOT NULL
          ORDER BY car.approval_id ASC;
    BEGIN
        OPEN cur_approvals_list;
        LOOP
            FETCH cur_approvals_list INTO l_comments;
            EXIT WHEN cur_approvals_list%notfound;
            IF P_PROPOSAL_COMMENTS IS NULL THEN
                P_PROPOSAL_COMMENTS := l_comments;
            ELSE
                P_PROPOSAL_COMMENTS := P_PROPOSAL_COMMENTS || '. ' || l_comments ;
            END IF;
        END LOOP;
        CLOSE cur_approvals_list;
    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_FETCH_FIELDS.get_proposal_comments',
            p_created_by       => sys_context('USERENV','OS_USER')
        );
    END get_proposal_comments;

    FUNCTION get_unknown_pd_count (
        P_REVISION_ID          IN CCT_QUOTE_REVISIONS.REVISION_ID%TYPE)
    RETURN NUMBER
    AS
        l_queue_id   cct_role_properties.queue_id%TYPE;
        l_count      NUMBER;
    BEGIN
        SELECT queue_id
          INTO l_queue_id
          FROM cct_parts_queue
         WHERE upper(queue_name) = 'UNKNOWN';

        SELECT COUNT(*)
          INTO l_count
          FROM cct_quote_parts
         WHERE quote_revision_id = p_revision_id AND
               queue_id = l_queue_id;

        RETURN l_count;
    EXCEPTION
        WHEN OTHERS THEN
            g_code := sqlcode;
            g_msg := substr(sqlerrm,1,450);
            cct_create_errors.record_error(
                p_quote_revision_id   => p_revision_id,
                p_error_message       => g_msg,
                p_error_type          => g_code,
                p_module_name         => 'CCT_FETCH_FIELDS.get_unknown_pd_count',
                p_created_by          => sys_context('USERENV','OS_USER')
            );
    END get_unknown_pd_count;

    FUNCTION get_queue_pd_count (
        P_REVISION_ID          IN CCT_QUOTE_REVISIONS.REVISION_ID%TYPE,
        P_ROLE_NAME            IN CCT_NOTIFICATION_REQUEST.ROLE_NAME%TYPE)
    RETURN NUMBER
    AS
        l_queue_id   cct_role_properties.queue_id%TYPE;
        l_count      NUMBER;
    BEGIN
        SELECT queue_id
          INTO l_queue_id
          FROM cct_role_properties
         WHERE upper(user_group) = upper(p_role_name);

        SELECT COUNT(*)
          INTO l_count
          FROM cct_quote_parts
         WHERE quote_revision_id = p_revision_id AND
               queue_id = l_queue_id;

        RETURN l_count;
    EXCEPTION
        WHEN OTHERS THEN
            g_code := sqlcode;
            g_msg := substr(sqlerrm,1,450);
            cct_create_errors.record_error(
                p_quote_revision_id   => p_revision_id,
                p_error_message       => g_msg,
                p_error_type          => g_code,
                p_module_name         => 'CCT_FETCH_FIELDS.get_queue_pd_count',
                p_created_by          => sys_context('USERENV','OS_USER')
            );
    END get_queue_pd_count;

    FUNCTION get_queue_pd_count_cost (
        P_REVISION_ID          IN CCT_QUOTE_REVISIONS.REVISION_ID%TYPE,
        P_ROLE_NAME            IN CCT_NOTIFICATION_REQUEST.ROLE_NAME%TYPE)
    RETURN NUMBER
    AS
        l_queue_id   cct_role_properties.queue_id%TYPE;
        l_count      NUMBER;
    BEGIN
        SELECT queue_id
          INTO l_queue_id
          FROM cct_role_properties
         WHERE upper(user_group) = upper(p_role_name);

        SELECT COUNT(*)
          INTO l_count
          FROM cct_quote_parts
         WHERE quote_revision_id = p_revision_id AND
               queue_id = l_queue_id AND
               nvl(suggested_cost_used_for_quote,cost_used_for_quote) is not null;

        RETURN l_count;
    EXCEPTION
        WHEN OTHERS THEN
            g_code := sqlcode;
            g_msg := substr(sqlerrm,1,450);
            cct_create_errors.record_error(
                p_quote_revision_id   => p_revision_id,
                p_error_message       => g_msg,
                p_error_type          => g_code,
                p_module_name         => 'CCT_FETCH_FIELDS.get_queue_pd_count_cost',
                p_created_by          => sys_context('USERENV','OS_USER')
            );
    END get_queue_pd_count_cost;

    PROCEDURE get_approver_name(
        P_OPPORTUNITY_ID       IN CCT_SNAPSHOT.OPPORTUNITY_ID%TYPE,
        P_SNAPSHOT_VERSION     IN CCT_SNAPSHOT.SNAPSHOT_VERSION%TYPE,
        P_APPROVAL_ROLE_ID     IN CCT_APPROVAL_USERS.APPROVAL_ROLE_ID%TYPE,
        P_APPROVAL_USER_NAME  OUT CCT_APPROVAL_USERS.APPROVAL_USER_NAME%TYPE)
    AS
    BEGIN
        SELECT ' - ' || approval_user_name
          INTO P_APPROVAL_USER_NAME
          FROM cct_approval_users cau,
               cct_snapshot cs
         WHERE cs.snapshot_id = cau.snapshot_id AND
               cs.opportunity_id = P_OPPORTUNITY_ID AND
               cs.snapshot_version = P_SNAPSHOT_VERSION AND
               cau.approval_role_id = P_APPROVAL_ROLE_ID;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,450);
        P_APPROVAL_USER_NAME := ' ';
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_FETCH_FIELDS.get_approver_name',
            p_created_by       => sys_context('USERENV','OS_USER')
        );
    END get_approver_name;

    PROCEDURE get_crf_id(
        P_OPPORTUNITY_NUMBER   IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE DEFAULT NULL,
        P_OPPORTUNITY_ID       IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE DEFAULT NULL,
        P_CRF_ID              OUT CCT_CRF.CRF_ID%TYPE)
    AS
        l_opportunity_number      CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE;
    BEGIN
        IF P_OPPORTUNITY_NUMBER IS NULL THEN
            get_opty_num(P_OPPORTUNITY_ID,l_opportunity_number);
        ELSE
            l_opportunity_number := P_OPPORTUNITY_NUMBER;
        END IF;

        SELECT crf_id
          INTO P_CRF_ID
          FROM cct_crf
         WHERE crf_no = l_opportunity_number;
    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_FETCH_FIELDS.get_crf_id',
            p_created_by       => sys_context('USERENV','OS_USER')
        );
    END get_crf_id;

    FUNCTION get_payment_notes(
        P_OPPORTUNITY_NUMBER   IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE) 
    RETURN VARCHAR2 
    AS
        l_opportunity_id          CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE;
        l_note_desc               cct_notes.note_desc%TYPE;
        l_result                  VARCHAR2(2000);
        CURSOR cur_payment_notes 
        IS 
            SELECT note_desc 
              FROM cct_notes 
             WHERE note_opp_no=P_OPPORTUNITY_NUMBER AND 
                   UPPER(note_type)='PAYMENT TERMS' 
            ORDER BY note_id ASC;
    BEGIN
        get_opty_id(P_OPPORTUNITY_NUMBER,l_opportunity_id);
        OPEN cur_payment_notes;
        LOOP
            FETCH cur_payment_notes INTO l_note_desc;
            EXIT WHEN cur_payment_notes%notfound;
            IF l_note_desc IS NULL THEN
                l_result := l_note_desc;
            ELSE
                l_result := l_result || l_note_desc ;
            END IF;
        END LOOP;
        CLOSE cur_payment_notes;
        RETURN l_result;
    EXCEPTION
        WHEN OTHERS THEN
            g_code := sqlcode;
            g_msg := substr(sqlerrm,1,450);
            cct_create_errors.record_error(
                p_opportunity_id      => l_opportunity_id,
                p_error_message       => g_msg,
                p_error_type          => g_code,
                p_module_name         => 'CCT_FETCH_FIELDS.get_payment_notes',
                p_created_by          => sys_context('USERENV','OS_USER')
            );
    END get_payment_notes;
    
    PROCEDURE get_latest_approved_snapshot (
        P_OPPORTUNITY_NUMBER   IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE DEFAULT NULL,
        P_OPPORTUNITY_ID       IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE DEFAULT NULL,
        P_SNAPSHOT_TYPE        IN CCT_SNAPSHOT.SNAPSHOT_TYPE%TYPE DEFAULT NULL,
        P_SNAPSHOT_ID         OUT CCT_SNAPSHOT.SNAPSHOT_ID%TYPE)
    AS
        l_opportunity_id          CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE;
    BEGIN
        IF P_OPPORTUNITY_ID IS NULL THEN
            get_opty_id(P_OPPORTUNITY_NUMBER,l_opportunity_id);
        ELSE
            l_opportunity_id := P_OPPORTUNITY_ID;
        END IF;

        IF P_SNAPSHOT_TYPE = 'APPROVALS' THEN
            SELECT MAX(cs.snapshot_id)
              INTO P_SNAPSHOT_ID
              FROM cct_snapshot cs
             WHERE cs.opportunity_id = l_opportunity_id AND
                   UPPER(cs.snapshot_type) = 'APPROVAL';
        ELSIF P_SNAPSHOT_TYPE = 'APPROVED' THEN
            SELECT MAX(cqa.snapshot_id)
              INTO P_SNAPSHOT_ID
              FROM cct_quote_approvals cqa
             WHERE cqa.opportunity_id = l_opportunity_id AND
                   UPPER(cqa.approval_status) = 'APPROVED';
        ELSE
            SELECT MAX(cs.snapshot_id)
              INTO P_SNAPSHOT_ID
              FROM cct_snapshot cs
             WHERE cs.opportunity_id = l_opportunity_id AND
                   UPPER(cs.snapshot_type) <> 'APPROVAL';
        END IF;

        IF P_SNAPSHOT_ID is NULL THEN
            P_SNAPSHOT_ID := 0;
        END IF;

    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            p_opportunity_id   => l_opportunity_id,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_FETCH_FIELDS.get_latest_approved_snapshot',
            p_created_by       => sys_context('USERENV','OS_USER')
        );
    END get_latest_approved_snapshot;

END CCT_FETCH_FIELDS;