create or replace PACKAGE cct_create_queue_tasks AS
/**Handles creating queue tasks
  Modification History
  PERSON        DATE           COMMENTS
  ------------  -----------    ---------------------------------------------
  Sureka        07-Jul-2017    Created initialize proc
  Sureka        07-Jul-2017    Created create_parts_queue proc
  Ankit Bhatt   14-Jul-2017    Added create_queue_task proc
  Ankit Bhatt   20-Jul-2017    Added other roles for different Queue
  Ankit Bhatt   25-Jul-2017    Modified procedure create_parts_queue to fulfill
                               'Advance Sourcing' and 'Unknown' where conditions
  Ankit Bhatt   26-Jul-2017    Modified procedure create_queue_task
  Ankit Bhatt   28-Jul-2017    Modified create_queue_task procedure to add validation
                               for task creation
  Ankit Bhatt   31-Jul-2017    Added task creation validation
  Ankit Bhatt   21-Sep-2017    Call cct_create_opportunity.update_formula_fields
                               procedure to update net margin and amount
                               after queue assignment done.
  Ankit Bhatt   10-Oct-2017    Modified date format
  Ankit Bhatt   31-Oct-2017    Modified proc. create_queue_task to pass attribute4
                               as 'BeaconUploadProcess'
**/
    TYPE cct_queue_tbl_type IS
        TABLE OF VARCHAR2(50) INDEX BY PLS_INTEGER;
    g_queuetypeelem            CCT_QUEUE_TBL_TYPE;
    g_prodtypeelem             CCT_PROD_TYPE;
    g_chemicalarray            CCT_QUEUE_PROD_CLASS_TBL_TYPE;
    g_lightningarray           CCT_QUEUE_PROD_CLASS_TBL_TYPE;
    g_kittingarray             CCT_QUEUE_PROD_CLASS_TBL_TYPE;
    g_hpp_part                 CCT_QUOTE_PARTS.HPP_PART%TYPE;
    g_item_tag                 CCT_QUOTE_PARTS.ITEM_TAG%TYPE;
    --Queue Names
    g_queue_commodity          CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
    g_queue_advance_sourcing   CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
    g_queue_hpp                CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
    g_queue_lighting           CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
    g_queue_kitting            CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
    g_queue_strategic_pricing  CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
    g_queue_chemical           CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
    g_queue_unknown            CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
    g_queue_no_bid             CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
    --Role Names
    g_role_advance_sourcing    CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
    g_role_hpp                 CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
    g_role_lighting            CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
    g_role_kitting             CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
    g_role_strategic_pricing   CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
    g_role_checmical           CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
    g_role_unknown             CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
    g_source_products_y        CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
    g_source_products_n        CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
    g_code                     VARCHAR2(50);
    g_msg                      VARCHAR2(2000);
    
    PROCEDURE initialize;

    PROCEDURE create_parts_queue (
        P_QUOTE_NUMBER     IN CCT_QUOTE.QUOTE_NUMBER%TYPE,
        P_QUOTE_REVISION   IN CCT_QUOTE_REVISIONS.QUOTE_REVISION%TYPE,
        P_RETURN_STATUS   OUT NOCOPY VARCHAR2
    );

    PROCEDURE create_queue_task (
        P_QUOTE_NUMBER     IN CCT_QUOTE.QUOTE_NUMBER%TYPE,
        P_QUOTE_REVISION   IN CCT_QUOTE_REVISIONS.QUOTE_REVISION%TYPE,
        P_RETURN_STATUS   OUT NOCOPY VARCHAR2
    );

END cct_create_queue_tasks;