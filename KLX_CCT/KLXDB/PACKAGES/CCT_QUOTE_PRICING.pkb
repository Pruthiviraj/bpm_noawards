create or replace PACKAGE BODY cct_quote_pricing AS
/**History 
   Author        Date         COMMENTS
   ------------  -----------  -----------------------------------------------
   Ankit Bhatt   14-Jul-2017  total_cost function created
   Ankit Bhatt   14-Jul-2017  total_resale function created
   Sureka        14-Sep-2017  class_margin function created
**/

/*    FUNCTION total_cost(
        P_QUOTE_REVISION_ID   CCT_QUOTE_PARTS.QUOTE_REVISION_ID%type) 
    RETURN NUMBER 
    AS
        l_total_cost   NUMBER;
    BEGIN
        SELECT SUM(CQP.COST_USED_FOR_QUOTE * CQP.TOTAL_QTY_LEN_CONTRACT)
          INTO l_total_cost
          FROM CCT_QUOTE_PARTS CQP,
               CCT_QUOTE_REVISIONS CQR
         WHERE CQR.REVISION_ID = CQP.QUOTE_REVISION_ID AND 
               CQP.QUOTE_REVISION_ID = P_QUOTE_REVISION_ID;

        RETURN l_total_cost;
    END total_cost;

    FUNCTION total_resale(
        P_QUOTE_REVISION_ID   CCT_QUOTE_PARTS.QUOTE_REVISION_ID%type) 
    RETURN NUMBER 
    AS
        l_total_resale   NUMBER;
    BEGIN
        SELECT SUM(CQP.RESALE * CQP.TOTAL_QTY_LEN_CONTRACT)
          INTO l_total_resale
          FROM CCT_QUOTE_PARTS CQP,
               CCT_QUOTE_REVISIONS CQR
         WHERE CQR.REVISION_ID = CQP.QUOTE_REVISION_ID AND
               CQP.QUOTE_REVISION_ID = P_QUOTE_REVISION_ID;

        RETURN l_total_resale;
    END total_resale;
*/

    FUNCTION class_margin (
        P_QUOTE_REVISION_ID   CCT_QUOTE_PARTS.QUOTE_REVISION_ID%TYPE,
        P_CALC_ABC            CCT_QUOTE_PARTS.CALC_ABC%TYPE) 
    RETURN NUMBER AS 
        l_parts_margin   NUMBER;
    BEGIN
        SELECT parts_margin
          INTO l_parts_margin
          FROM cct_quote_class_v
         WHERE quote_revision_id = P_QUOTE_REVISION_ID AND
               item_class = P_CALC_ABC;
        RETURN l_parts_margin; 
    END class_margin;

END cct_quote_pricing;