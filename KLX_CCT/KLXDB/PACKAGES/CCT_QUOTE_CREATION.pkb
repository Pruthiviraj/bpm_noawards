--------------------------------------------------------
--  DDL for Package Body CCT_QUOTE_CREATION
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "XXCCT"."CCT_QUOTE_CREATION" AS
/**Handles quote creation and revision and duplicating quote parts
   Modification History
   Author        Date         COMMENTS
   ------------  -----------  ----------------------------------------
   Ankit Bhatt   05-Jul-2017  Package for Quote Creation and Revisions
   Sureka        17-Jul-2017  copy_quote_parts procedure created
   Sureka        19-Jul-2017  modified out parameters of create_quote
   Ankit Bhatt   27-Jul-2017  created new procedure copy_quote_revision_with_parts
                              and added out parameter in create_quote_revisions procedure.
   Sureka        05-Mar-2018  modified copy_quote_parts procedure                           
**/

    PROCEDURE create_quote (
        P_OPPORTUNITY_NUMBER IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE,
        P_QUOTE_NUMBER      OUT CCT_QUOTE.QUOTE_NUMBER%TYPE,
        P_QUOTE_REVISION    OUT CCT_QUOTE_REVISIONS.QUOTE_REVISION%TYPE,
        P_REVISION_ID       OUT CCT_QUOTE_REVISIONS.REVISION_ID%TYPE)
    AS
        l_opp_id                NUMBER;
        ex_quote                EXCEPTION;
        l_quote_count           NUMBER := 0;
    BEGIN
        g_quote_id := cct_quote_seq.nextval;
        cct_fetch_fields.get_opty_id(P_OPPORTUNITY_NUMBER,l_opp_id);

        SELECT COUNT(*)
          INTO l_quote_count
          FROM cct_quote
         WHERE opportunity_id = l_opp_id;

        IF l_quote_count = 0 THEN
            g_new_revision_id := cct_quote_revision_seq.nextval;
            P_QUOTE_NUMBER := 'Q0000000' || cct_quote_number_seq.nextval;
            P_REVISION_ID := g_new_revision_id;
            P_QUOTE_REVISION := g_initial_quote_rev;
            INSERT INTO cct_quote VALUES (
                g_quote_id,
                l_opp_id,
                P_QUOTE_NUMBER,
                g_quote_status,
                SYSDATE,
                sys_context('USERENV','OS_USER'),
                NULL,
                NULL
            );

            INSERT INTO cct_quote_revisions VALUES (
                g_new_revision_id,
                g_quote_id,
                g_initial_quote_rev,
                g_quote_status,
                SYSDATE,
                sys_context('USERENV','OS_USER'),
                NULL,
                NULL
            );

            COMMIT;
        ELSE
            SELECT  cq.quote_number,
                    MAX(cqr.quote_revision)
              INTO  P_QUOTE_NUMBER,
                    P_QUOTE_REVISION
              FROM  cct_quote cq,
                    cct_quote_revisions cqr
             WHERE  cq.opportunity_id = l_opp_id AND
                    cq.quote_id = cqr.quote_id
            GROUP BY cq.quote_number;

            SELECT cqr.revision_id
              INTO p_revision_id
              FROM cct_quote cq,
                   cct_quote_revisions cqr
             WHERE cq.quote_id = cqr.quote_id AND
                   cq.quote_number = P_QUOTE_NUMBER AND
                   quote_revision = P_QUOTE_REVISION;

            RAISE ex_quote;
        END IF;

    EXCEPTION
    WHEN ex_quote THEN
        cct_create_errors.record_error(
            p_opportunity_id      => l_opp_id,
            p_quote_revision_id   => p_revision_id,
            p_quote_number        => p_quote_number,
            p_quote_revision      => p_quote_revision,
            p_error_message       => 'ORA-00001 QUOTE ALREADY EXISTS FOR GIVEN OPP',
            p_error_type          => '1',
            p_module_name         => 'cct_quote_creation.create_quote',
            p_created_by          => sys_context('USERENV','OS_USER')
        );
    WHEN OTHERS THEN
        g_message := substr(sqlerrm,1,50);
        g_code := sqlcode;
        cct_create_errors.record_error(
            p_opportunity_id      => l_opp_id,
            p_quote_revision_id   => p_revision_id,
            p_quote_number        => p_quote_number,
            p_quote_revision      => p_quote_revision,
            p_error_message       => g_message,
            p_error_type          => g_code,
            p_module_name         => 'cct_quote_creation.create_quote',
            p_created_by          => sys_context('USERENV','OS_USER')
        );
    END create_quote;

    PROCEDURE create_quote_revisions (
        P_OPPORTUNITY_NUMBER IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE,
        P_QUOTE_REVISION_ID OUT CCT_QUOTE_REVISIONS.REVISION_ID%TYPE,
        P_QUOTE_REV_NUM     OUT CCT_QUOTE_REVISIONS.QUOTE_REVISION%TYPE)
    AS
        v_sales_stage                CCT_OPPORTUNITY.SALES_STAGE%TYPE;
		/* capture sales stage value based on opportunity_number */
        v_revision                   VARCHAR2(50) := '0.1';
		/* Adding with 0.1 for every revision of the quotes*/
        v_new_revision               VARCHAR2(50);
		/* New Revision before sent to the customer*/
        v_substr_quote_revision      VARCHAR2(50);
		/* Substring quote revision value */
        v_quote_sent_to_cust_version VARCHAR2(50);
		/* final version of the quote when its sent to the cutomer */
        v_sent_to_cust_version       VARCHAR2(50) := '1.0';
		/* Quote Revision value when its sent to customer */
        v_sales_stage_negotiation    CCT_OPPORTUNITY.SALES_STAGE%TYPE := 'Negotiation';
		/* sales stage value for sent to the customer */
        v_max_revision_id            CCT_QUOTE_REVISIONS.REVISION_ID%TYPE;
    BEGIN
        SELECT  cq.quote_id,
                MAX(cqr.revision_id) revision_id,
                MAX(cqr.quote_revision) quote_revision,
                substr( MAX(cqr.quote_revision),1,2) quote_revised,
                co.sales_stage
          INTO  g_quote_id,
                v_max_revision_id,
                g_max_quote_revision,
                v_substr_quote_revision,
                v_sales_stage
          FROM  cct_opportunity co,
                cct_quote cq,
                cct_quote_revisions cqr
         WHERE  co.opportunity_id = cq.opportunity_id AND
                cq.quote_id = cqr.quote_id AND
                co.opportunity_number = P_OPPORTUNITY_NUMBER
       GROUP BY cq.quote_id,
                co.opportunity_number,
                cqr.quote_id,
                co.sales_stage;

        g_new_revision_id := cct_quote_revision_seq.nextval;
        IF v_sales_stage <> v_sales_stage_negotiation OR
            v_sales_stage IS NULL THEN
            v_new_revision := g_max_quote_revision + v_revision;
            v_new_revision := TO_CHAR(v_new_revision,'FM90.0');
            INSERT INTO cct_quote_revisions VALUES (
                g_new_revision_id,
                g_quote_id,
                v_new_revision,
                g_quote_status,
                SYSDATE,
                sys_context('USERENV','OS_USER'),
                NULL,
                NULL
            );

        ELSIF v_sales_stage = v_sales_stage_negotiation THEN
            v_quote_sent_to_cust_version := v_substr_quote_revision + v_sent_to_cust_version;
            v_quote_sent_to_cust_version := TO_CHAR(v_quote_sent_to_cust_version,'FM90.0');
            INSERT INTO cct_quote_revisions VALUES (
                g_new_revision_id,
                g_quote_id,
                v_quote_sent_to_cust_version,
                g_quote_status,
                SYSDATE,
                sys_context('USERENV','OS_USER'),
                NULL,
                NULL
            );

            COMMIT;
        END IF;
        copy_quote_parts(P_OLD_REVISION_ID => v_max_revision_id,
                         P_NEW_REVISION_ID => g_new_revision_id);

        IF v_sales_stage <> v_sales_stage_negotiation OR
            v_sales_stage IS NULL THEN
            P_QUOTE_REV_NUM := v_new_revision;
            P_QUOTE_REVISION_ID := g_new_revision_id;
        ELSIF v_sales_stage = v_sales_stage_negotiation THEN
            P_QUOTE_REV_NUM := v_quote_sent_to_cust_version;
            P_QUOTE_REVISION_ID := g_new_revision_id;
        END IF;

    EXCEPTION
    WHEN OTHERS THEN
        g_message := substr(sqlerrm,1,50);
        g_code := sqlcode;
        cct_create_errors.record_error(
            p_error_message   => g_message,
            p_error_type      => g_code,
            p_module_name     => 'cct_quote_creation.create_quote_revisions - Insert into CCT_QUOTE_REVISIONS',
            p_created_by      => sys_context('USERENV','OS_USER')
        );
    END create_quote_revisions;

    PROCEDURE copy_quote_revision_with_parts (
        P_OPPORTUNITY_NUMBER IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE)
    AS
        v_sales_stage                CCT_OPPORTUNITY.SALES_STAGE%TYPE;
		/* capture sales stage value based on opportunity_number */
        v_revision                   VARCHAR2(50) := '0.1';
		/* Adding with 0.1 for every revision of the quotes*/
        v_new_revision               VARCHAR2(50);
		/* New Revision before sent to the customer*/
        v_substr_quote_revision      VARCHAR2(50);
		/* Substring quote revision value */
        v_quote_sent_to_cust_version VARCHAR2(50);
		/* final version of the quote when its sent to the cutomer */
        v_sent_to_cust_version       VARCHAR2(50) := '1.0';
		/* Quote Revision value when its sent to customer */
        v_sales_stage_negotiation    CCT_OPPORTUNITY.SALES_STAGE%TYPE := 'Negotiation';
		/* sales stage value for sent to the customer */
        v_max_revision_id            CCT_QUOTE_REVISIONS.REVISION_ID%TYPE;
    BEGIN
        SELECT  cq.quote_id,
                MAX(cqr.revision_id) revision_id,
                MAX(cqr.quote_revision) quote_revision,
                substr(MAX(cqr.quote_revision),1,2) quote_revised,
                co.sales_stage
          INTO  g_quote_id,
                v_max_revision_id,
                g_max_quote_revision,
                v_substr_quote_revision,
                v_sales_stage
          FROM  cct_opportunity co,
                cct_quote cq,
                cct_quote_revisions cqr
         WHERE  co.opportunity_id = cq.opportunity_id AND
                cq.quote_id = cqr.quote_id AND
                co.opportunity_number = p_opportunity_number
       GROUP BY cq.quote_id,
                co.opportunity_number,
                cqr.quote_id,
                co.sales_stage;

        g_new_revision_id := cct_quote_revision_seq.nextval;
        IF v_sales_stage <> v_sales_stage_negotiation OR
           v_sales_stage IS NULL THEN
            v_new_revision := g_max_quote_revision + v_revision;
            v_new_revision := TO_CHAR(v_new_revision,'FM90.0');
            INSERT INTO cct_quote_revisions VALUES (
                g_new_revision_id,
                g_quote_id,
                v_new_revision,
                g_quote_status,
                SYSDATE,
                sys_context('USERENV','OS_USER'),
                NULL,
                NULL
            );

        ELSIF v_sales_stage = v_sales_stage_negotiation THEN
            v_quote_sent_to_cust_version := v_substr_quote_revision + v_sent_to_cust_version;
            v_quote_sent_to_cust_version := TO_CHAR(v_quote_sent_to_cust_version,'FM90.0');
            INSERT INTO cct_quote_revisions VALUES (
                g_new_revision_id,
                g_quote_id,
                v_quote_sent_to_cust_version,
                g_quote_status,
                SYSDATE,
                sys_context('USERENV','OS_USER'),
                NULL,
                NULL
            );

            COMMIT;
        END IF;

        copy_quote_parts(P_OLD_REVISION_ID => v_max_revision_id,
                         P_NEW_REVISION_ID => g_new_revision_id);
    EXCEPTION
    WHEN OTHERS THEN
        g_message := substr(sqlerrm,1,50);
        g_code := sqlcode;
        cct_create_errors.record_error(
            p_error_message   => g_message,
            p_error_type      => g_code,
            p_module_name     => 'cct_quote_creation.copy_quote_revision_with_parts - Insert into CCT_QUOTE_REVISIONS',
            p_created_by      => sys_context('USERENV','OS_USER')
        );
    END copy_quote_revision_with_parts;

    PROCEDURE copy_quote_parts(
        P_OLD_REVISION_ID    IN CCT_QUOTE_REVISIONS.REVISION_ID%TYPE,
        P_NEW_REVISION_ID    IN CCT_QUOTE_REVISIONS.REVISION_ID%TYPE) 
    AS
    BEGIN

        INSERT INTO cct_quote_parts (quote_revision_id,
            queue_id,
            whse_id,
            product_id,
            quoted_part_number,
            customer_pn,
            annual_qty,
            uom,
            approved_source,
            disapproved_source,
            misc_cust_data,
            target_price,
            target_margin,
            certified_part,
            contract_price,
            last_price_paid,
            highest_contract_price,
            lowest_contract_price,
            high_tape_price,
            middle_tape_price,
            low_tape_price,
            total_qty_len_contract,
            average_cost,
            average_cost_lbs,
            last_cost,
            actual_cost,
            vendor_cost,
            market_cost,
            lta_cost,
            poq_cost,
            poq_quantity,
            delivery_time_poq,
            cd_from_jda,
            kapco_part_cost,
            symph_drop_shipment,
            hpp_part_price,
            hpp_part,
            prime,
            dup_prime,
            dup_cust_part,
            vendor_back_orders,
            cust_back_orders,
            lta_moq,
            lta_exp_date,
            lta_mfr,
            lta_leadtime_weeks,
            vendor_leadtime_weeks,
            prod_type_abc,
            kapco_leadtime_weeks,
            first_plan_purday,
            jda_forecast_12m,
            control_number,
            manf_number,
            manf_name,
            mfr_cage_code,
            shelf_life,
            pma,
            tso,
            certs,
            c_and_p,
            rev,
            part_desc,
            tariff,
            itar,
            eccn,
            dfar,
            ppp,
            stocking_unit_measure,
            comments,
            product_notes,
            restriction_codes,
            prod_class,
            total_onhand_whses,
            other_cust_price,
            contract_part_number,
            alt_part_number,
            total_alt_qty_onhand,
            total_vendor_back_orders,
            creation_date,
            created_by,
            whse_quantity,
            line_identifier,
            quote_part_id,
            resale,
            cost_used_for_quote,
            calc_abc,
            alternatives_avl,
            final_cost_source,
            extended_cost,
            extended_resale,
            final_mkt_class,
            item_tag,
            last_price_offered,
            lta_review_item,
            potential_uom_issue,
            replacement_cost_used,
            speciality_products,
            pipeline_qty,
            dyp_price_break_qty,
            ecommerce_price,
            price_type,
            margin_validation,
            suggested_cust_final_leadtime,
            suggested_quoted_part_number,
            suggested_annual_qty,
            suggested_uom,
            suggested_prime,
            suggested_calc_abc,
            suggested_cost_used_for_quote,
            suggested_manf_name,
            suggested_mfr_cage_code,
            suggested_resale,
            queue_assignment_date,
            suggested_total_qty_len_con,
            price_confidence,
            tech_support_source,
            tech_support_alternate,
            tech_support_comments,
            package_qty,
            pct_discount,
            tot_sls_val_usd12mths_crdx_uni,
            storage_group,
            hazmat_jn,
            shelf_life_months_general,
            stock_location,
            inco_term,
            moq_cardex_unit,
            selling_increment,
            alternate_source,
            vendor_location_country,
            itl_internal_pn,
            extended_annual_cost,
            crdx_unit_size_info_iden_to_s4,
            comments_um_conversion,
            cstmr_um_conv_fctr_cardex_unit,
            customer_uom,
            customer_container_size_data,
            negotiated,
            spreadsheet,
            quote_number_from_supplier,
            vendor_comment,
            dfar_fee,
            misc_fee,
            tool_fee,
            fair_fee,
            test_reports,
            manufacturer_certs,
            global_quantities,
            amortiz_infl_pct,
            moq,
            qty_break2,
            qty_break1,
            quoted_qty,
            comment_category,
            status,
            sales_restriction,
            lta_full_qty_used,
            lta_cost_cust,
            external_notes,
            internal_notes,
            customer_final_leadtime
        ) ( SELECT  P_NEW_REVISION_ID,
                    queue_id,
                    whse_id,
                    product_id,
                    quoted_part_number,
                    customer_pn,
                    annual_qty,
                    uom,
                    approved_source,
                    disapproved_source,
                    misc_cust_data,
                    target_price,
                    target_margin,
                    certified_part,
                    contract_price,
                    last_price_paid,
                    highest_contract_price,
                    lowest_contract_price,
                    high_tape_price,
                    middle_tape_price,
                    low_tape_price,
                    total_qty_len_contract,
                    average_cost,
                    average_cost_lbs,
                    last_cost,
                    actual_cost,
                    vendor_cost,
                    market_cost,
                    lta_cost,
                    poq_cost,
                    poq_quantity,
                    delivery_time_poq,
                    cd_from_jda,
                    kapco_part_cost,
                    symph_drop_shipment,
                    hpp_part_price,
                    hpp_part,
                    prime,
                    dup_prime,
                    dup_cust_part,
                    vendor_back_orders,
                    cust_back_orders,
                    lta_moq,
                    lta_exp_date,
                    lta_mfr,
                    lta_leadtime_weeks,
                    vendor_leadtime_weeks,
                    prod_type_abc,
                    kapco_leadtime_weeks,
                    first_plan_purday,
                    jda_forecast_12m,
                    control_number,
                    manf_number,
                    manf_name,
                    mfr_cage_code,
                    shelf_life,
                    pma,
                    tso,
                    certs,
                    c_and_p,
                    rev,
                    part_desc,
                    tariff,
                    itar,
                    eccn,
                    dfar,
                    ppp,
                    stocking_unit_measure,
                    comments,
                    product_notes,
                    restriction_codes,
                    prod_class,
                    total_onhand_whses,
                    other_cust_price,
                    contract_part_number,
                    alt_part_number,
                    total_alt_qty_onhand,
                    total_vendor_back_orders,
                    sysdate,
                    sys_context('USERENV','OS_USER'),
                    whse_quantity,
                    line_identifier,
                    quote_part_id,
                    resale,
                    cost_used_for_quote,
                    calc_abc,
                    alternatives_avl,
                    final_cost_source,
                    extended_cost,
                    extended_resale,
                    final_mkt_class,
                    item_tag,
                    last_price_offered,
                    lta_review_item,
                    potential_uom_issue,
                    replacement_cost_used,
                    speciality_products,
                    pipeline_qty,
                    dyp_price_break_qty,
                    ecommerce_price,
                    price_type,
                    margin_validation,
                    suggested_cust_final_leadtime,
                    suggested_quoted_part_number,
                    suggested_annual_qty,
                    suggested_uom,
                    suggested_prime,
                    suggested_calc_abc,
                    suggested_cost_used_for_quote,
                    suggested_manf_name,
                    suggested_mfr_cage_code,
                    suggested_resale,
                    queue_assignment_date,
                    suggested_total_qty_len_con,
                    price_confidence,
                    tech_support_source,
                    tech_support_alternate,
                    tech_support_comments,
                    package_qty,
                    pct_discount,
                    tot_sls_val_usd12mths_crdx_uni,
                    storage_group,
                    hazmat_jn,
                    shelf_life_months_general,
                    stock_location,
                    inco_term,
                    moq_cardex_unit,
                    selling_increment,
                    alternate_source,
                    vendor_location_country,
                    itl_internal_pn,
                    extended_annual_cost,
                    crdx_unit_size_info_iden_to_s4,
                    comments_um_conversion,
                    cstmr_um_conv_fctr_cardex_unit,
                    customer_uom,
                    customer_container_size_data,
                    negotiated,
                    spreadsheet,
                    quote_number_from_supplier,
                    vendor_comment,
                    dfar_fee,
                    misc_fee,
                    tool_fee,
                    fair_fee,
                    test_reports,
                    manufacturer_certs,
                    global_quantities,
                    amortiz_infl_pct,
                    moq,
                    qty_break2,
                    qty_break1,
                    quoted_qty,
                    comment_category,
                    status,
                    sales_restriction,
                    lta_full_qty_used,
                    lta_cost_cust,
                    external_notes,
                    internal_notes,
                    customer_final_leadtime 
              FROM  cct_quote_parts
             WHERE  quote_revision_id = P_OLD_REVISION_ID);
             
        INSERT INTO cct_quote_parts_final (quote_revision_id,
            queue_id,
            whse_id,
            product_id,
            quoted_part_number,
            customer_pn,
            annual_qty,
            uom,
            approved_source,
            disapproved_source,
            misc_cust_data,
            target_price,
            target_margin,
            certified_part,
            contract_price,
            last_price_paid,
            highest_contract_price,
            lowest_contract_price,
            high_tape_price,
            middle_tape_price,
            low_tape_price,
            total_qty_len_contract,
            average_cost,
            average_cost_lbs,
            last_cost,
            actual_cost,
            vendor_cost,
            market_cost,
            lta_cost,
            poq_cost,
            poq_quantity,
            delivery_time_poq,
            cd_from_jda,
            kapco_part_cost,
            symph_drop_shipment,
            hpp_part_price,
            hpp_part,
            prime,
            dup_prime,
            dup_cust_part,
            vendor_back_orders,
            cust_back_orders,
            lta_moq,
            lta_exp_date,
            lta_mfr,
            lta_leadtime_weeks,
            vendor_leadtime_weeks,
            prod_type_abc,
            kapco_leadtime_weeks,
            first_plan_purday,
            jda_forecast_12m,
            control_number,
            manf_number,
            manf_name,
            mfr_cage_code,
            shelf_life,
            pma,
            tso,
            certs,
            c_and_p,
            rev,
            part_desc,
            tariff,
            itar,
            eccn,
            dfar,
            ppp,
            stocking_unit_measure,
            comments,
            product_notes,
            restriction_codes,
            prod_class,
            total_onhand_whses,
            other_cust_price,
            contract_part_number,
            alt_part_number,
            total_alt_qty_onhand,
            total_vendor_back_orders,
            creation_date,
            created_by,
            whse_quantity,
            line_identifier,
            quote_part_id,
            resale,
            cost_used_for_quote,
            calc_abc,
            alternatives_avl,
            final_cost_source,
            extended_cost,
            extended_resale,
            final_mkt_class,
            item_tag,
            last_price_offered,
            lta_review_item,
            potential_uom_issue,
            replacement_cost_used,
            speciality_products,
            pipeline_qty,
            dyp_price_break_qty,
            ecommerce_price,
            price_type,
            margin_validation,
            suggested_cust_final_leadtime,
            suggested_quoted_part_number,
            suggested_annual_qty,
            suggested_uom,
            suggested_prime,
            suggested_calc_abc,
            suggested_cost_used_for_quote,
            suggested_manf_name,
            suggested_mfr_cage_code,
            suggested_resale,
            price_confidence,
            tech_support_source,
            tech_support_alternate,
            tech_support_comments,
            package_qty,
            pct_discount,
            tot_sls_val_usd12mths_crdx_uni,
            storage_group,
            hazmat_jn,
            shelf_life_months_general,
            stock_location,
            inco_term,
            moq_cardex_unit,
            selling_increment,
            alternate_source,
            vendor_location_country,
            itl_internal_pn,
            extended_annual_cost,
            crdx_unit_size_info_iden_to_s4,
            comments_um_conversion,
            cstmr_um_conv_fctr_cardex_unit,
            customer_uom,
            customer_container_size_data,
            negotiated,
            spreadsheet,
            quote_number_from_supplier,
            vendor_comment,
            dfar_fee,
            misc_fee,
            tool_fee,
            fair_fee,
            test_reports,
            manufacturer_certs,
            global_quantities,
            amortiz_infl_pct,
            moq,
            qty_break2,
            qty_break1,
            quoted_qty,
            comment_category,
            status,
            sales_restriction,
            lta_full_qty_used,
            lta_cost_cust,
            external_notes,
            internal_notes,
            customer_final_leadtime,
            ext_cost_value_rank
        ) ( SELECT  P_NEW_REVISION_ID,
                    queue_id,
                    whse_id,
                    product_id,
                    quoted_part_number,
                    customer_pn,
                    annual_qty,
                    uom,
                    approved_source,
                    disapproved_source,
                    misc_cust_data,
                    target_price,
                    target_margin,
                    certified_part,
                    contract_price,
                    last_price_paid,
                    highest_contract_price,
                    lowest_contract_price,
                    high_tape_price,
                    middle_tape_price,
                    low_tape_price,
                    total_qty_len_contract,
                    average_cost,
                    average_cost_lbs,
                    last_cost,
                    actual_cost,
                    vendor_cost,
                    market_cost,
                    lta_cost,
                    poq_cost,
                    poq_quantity,
                    delivery_time_poq,
                    cd_from_jda,
                    kapco_part_cost,
                    symph_drop_shipment,
                    hpp_part_price,
                    hpp_part,
                    prime,
                    dup_prime,
                    dup_cust_part,
                    vendor_back_orders,
                    cust_back_orders,
                    lta_moq,
                    lta_exp_date,
                    lta_mfr,
                    lta_leadtime_weeks,
                    vendor_leadtime_weeks,
                    prod_type_abc,
                    kapco_leadtime_weeks,
                    first_plan_purday,
                    jda_forecast_12m,
                    control_number,
                    manf_number,
                    manf_name,
                    mfr_cage_code,
                    shelf_life,
                    pma,
                    tso,
                    certs,
                    c_and_p,
                    rev,
                    part_desc,
                    tariff,
                    itar,
                    eccn,
                    dfar,
                    ppp,
                    stocking_unit_measure,
                    comments,
                    product_notes,
                    restriction_codes,
                    prod_class,
                    total_onhand_whses,
                    other_cust_price,
                    contract_part_number,
                    alt_part_number,
                    total_alt_qty_onhand,
                    total_vendor_back_orders,
                    sysdate,
                    sys_context('USERENV','OS_USER'),
                    whse_quantity,
                    line_identifier,
                    quote_part_id,
                    resale,
                    cost_used_for_quote,
                    calc_abc,
                    alternatives_avl,
                    final_cost_source,
                    extended_cost,
                    extended_resale,
                    final_mkt_class,
                    item_tag,
                    last_price_offered,
                    lta_review_item,
                    potential_uom_issue,
                    replacement_cost_used,
                    speciality_products,
                    pipeline_qty,
                    dyp_price_break_qty,
                    ecommerce_price,
                    price_type,
                    margin_validation,
                    suggested_cust_final_leadtime,
                    suggested_quoted_part_number,
                    suggested_annual_qty,
                    suggested_uom,
                    suggested_prime,
                    suggested_calc_abc,
                    suggested_cost_used_for_quote,
                    suggested_manf_name,
                    suggested_mfr_cage_code,
                    suggested_resale,
                    price_confidence,
                    tech_support_source,
                    tech_support_alternate,
                    tech_support_comments,
                    package_qty,
                    pct_discount,
                    tot_sls_val_usd12mths_crdx_uni,
                    storage_group,
                    hazmat_jn,
                    shelf_life_months_general,
                    stock_location,
                    inco_term,
                    moq_cardex_unit,
                    selling_increment,
                    alternate_source,
                    vendor_location_country,
                    itl_internal_pn,
                    extended_annual_cost,
                    crdx_unit_size_info_iden_to_s4,
                    comments_um_conversion,
                    cstmr_um_conv_fctr_cardex_unit,
                    customer_uom,
                    customer_container_size_data,
                    negotiated,
                    spreadsheet,
                    quote_number_from_supplier,
                    vendor_comment,
                    dfar_fee,
                    misc_fee,
                    tool_fee,
                    fair_fee,
                    test_reports,
                    manufacturer_certs,
                    global_quantities,
                    amortiz_infl_pct,
                    moq,
                    qty_break2,
                    qty_break1,
                    quoted_qty,
                    comment_category,
                    status,
                    sales_restriction,
                    lta_full_qty_used,
                    lta_cost_cust,
                    external_notes,
                    internal_notes,
                    customer_final_leadtime,
                    ext_cost_value_rank
              FROM  cct_quote_parts_final 
             WHERE  quote_revision_id = P_OLD_REVISION_ID);    

        COMMIT;
    EXCEPTION
    WHEN OTHERS THEN
        g_message := substr(sqlerrm,1,50);
        g_code := sqlcode;
        cct_create_errors.record_error(
            p_error_message   => g_message,
            p_error_type      => g_code,
            p_module_name     => 'cct_quote_creation.copy_quote_parts - Insert into CCT_QUOTE_PARTS',
            p_created_by      => sys_context('USERENV','OS_USER')
        );
    END copy_quote_parts;

END cct_quote_creation;

/
