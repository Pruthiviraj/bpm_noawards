create or replace PACKAGE BODY cct_create_task
AS
/**Handles CCT_TASK_STATUS loading
   Modification History
   PERSON        DATE         COMMENTS
   -----------  -----------  ---------------------------------------------
   Sureka       28-Jul-2017  Created insert_task_status proc
   Sureka       28-Jul-2017  Created update_task_status proc
   Sureka       10-Aug-2017  Modified insert_task_status to return due_date
   Ankit Bhatt  10-Oct-2017  Modified date format
   Ankit Bhatt  02-Nov-2017  Dropped procedure update_task_status
   Ramu Katakam 17-Jul-2019  CHG0041267
   Ramu Katakam 18-Aug-2019  RITM0038022-Task due date rules


**/

    PROCEDURE insert_task_status(
        P_TASK_OBJ        IN CCT_TASK_TYPE,
        P_TASK_SUBJECT   OUT CCT_ROLE_PROPERTIES.TASK_SUBJECT%TYPE,
        P_DUE_DATE       OUT DATE)
    AS
        ex_opp             EXCEPTION;
    BEGIN
        g_user_role  := UPPER(P_TASK_OBJ.USER_ROLE);
        g_opp_number := P_TASK_OBJ.OPPORTUNITY_NUMBER;
        cct_fetch_fields.get_opty_id(g_opp_number,g_opp_id);

        IF g_opp_id IS NULL THEN
            RAISE ex_opp;
        END IF;

        g_instance_id    := P_TASK_OBJ.INSTANCE_ID;
        g_task_id        := P_TASK_OBJ.TASK_ID;
        g_quote_number   := P_TASK_OBJ.QUOTE_NUMBER;
        g_quote_revision := P_TASK_OBJ.QUOTE_REVISION;

        /*l_count          := 0;
        BEGIN
        SELECT COUNT(*)
        INTO l_count
        FROM cct_task_status
        WHERE UPPER(user_role) = g_user_role AND
        instance_id = g_instance_id AND
        opportunity_number = g_opp_number;
        EXCEPTION
        WHEN OTHERS THEN
        g_code := SQLCODE;
        g_msg  := SUBSTR( sqlerrm, 1, 500 );
        cct_create_errors.record_error( p_task_id => g_task_id, p_instance_id => g_instance_id, p_quote_number => g_quote_number, p_quote_revision => g_quote_revision, p_error_message => g_msg, p_error_type => g_code, p_module_name => 'insert_task_status - REC TYPE exist on not', p_created_by => USER );
        raise_application_error( -20001, 'REC TYPE not valid' );
        END;
        IF l_count=0 THEN
        INSERT INTO CCT_TASK_STATUS(
        TASK_ID,
        USER_ROLE,
        CREATED_DATE,
        COMPLETION_DATE,
        TASK_STATUS,
        INSTANCE_ID,
        COMPLETION_METHOD,
        OPPORTUNITY_NUMBER,
        QUOTE_NUMBER,
        QUOTE_REVISION,
        CREATED_BY,
        LAST_UPDATED_DATE,
        LAST_UPDATED_BY,
        OPPORTUNITY_STATUS,
        TASK_TITLE,
        ATTRIBUTE1,
        ATTRIBUTE2,
        ATTRIBUTE3)
        VALUES(
        P_TASK_OBJ.TASK_ID,
        P_TASK_OBJ.USER_ROLE,
        TO_DATE(P_TASK_OBJ.CREATED_DATE,'DD-MM-RRRR HH24:MI:SS'),
        TO_DATE(P_TASK_OBJ.COMPLETION_DATE,'DD-MM-RRRR HH24:MI:SS'),
        P_TASK_OBJ.TASK_STATUS,
        P_TASK_OBJ.INSTANCE_ID,
        P_TASK_OBJ.COMPLETION_METHOD,
        P_TASK_OBJ.OPPORTUNITY_NUMBER,
        P_TASK_OBJ.QUOTE_NUMBER,
        P_TASK_OBJ.QUOTE_REVISION,
        P_TASK_OBJ.CREATED_BY,
        TO_DATE(P_TASK_OBJ.LAST_UPDATED_DATE,'DD-MM-RRRR HH24:MI:SS'),
        P_TASK_OBJ.LAST_UPDATED_BY,
        P_TASK_OBJ.OPPORTUNITY_STATUS,
        P_TASK_OBJ.TASK_TITLE,
        P_TASK_OBJ.ATTRIBUTE1,
        P_TASK_OBJ.ATTRIBUTE2,
        P_TASK_OBJ.ATTRIBUTE3);
        COMMIT;
        ELSE
        update_task_status(P_TASK_OBJ);
        END IF;**/
        BEGIN
        
            SELECT  r.task_subject,
                    CASE 
                    WHEN UPPER(g_user_role)='SCCCT_LEGAL' or UPPER(g_user_role)=UPPER('scCCT_Compliance') 
        or UPPER(g_user_role)= UPPER('scCCT_Payment_Terms') or UPPER(g_user_role)= UPPER('scCCT_Quality')
        or UPPER(g_user_role)= UPPER('scCCT_Contracts_Administrator') or UPPER(g_user_role)= UPPER('scCCT_Program_Solutions')
        or UPPER(g_user_role)= UPPER('scCCT_Strategic_Pricing')
        THEN 
                     (CASE WHEN (trim(TO_CHAR( TRUNC(o.customer_due_date), 'Day')) = 'Monday' or 
            trim(TO_CHAR( TRUNC(o.customer_due_date), 'Day')) = 'Tuesday') THEN
                        TRUNC(o.customer_due_date) - r.due_days - 2 
                        ELSE TRUNC(o.customer_due_date) - r.due_days END)
                    WHEN UPPER(g_user_role)= UPPER('scCCT_Operations') or UPPER(g_user_role)= UPPER('scCCT_Kitting')
                     or UPPER(g_user_role)= UPPER('scCCT_Advanced_Sourcing')
                      or UPPER(g_user_role)= UPPER('scCCT_CSM')
                       or UPPER(g_user_role)= UPPER('scCCT_Commodity')
                        or UPPER(g_user_role)= UPPER('scCCT_Electrical_Product_Line')
                         or UPPER(g_user_role)= UPPER('scCCT_Honeywell_Product_Line')
                          or UPPER(g_user_role)= UPPER('scCCT_Lighting_Product_Line')
                           or UPPER(g_user_role)= UPPER('scCCT_Technical_Support_Team')THEN 
                     (CASE WHEN (trim(TO_CHAR( TRUNC(o.customer_due_date), 'Day')) = 'Monday' or 
                     trim(TO_CHAR( TRUNC(o.customer_due_date), 'Day')) = 'Tuesday' or
                     trim(TO_CHAR( TRUNC(o.customer_due_date), 'Day')) = 'Wednesday' or 
                     trim(TO_CHAR( TRUNC(o.customer_due_date), 'Day')) = 'Thursday' )THEN
                        TRUNC(o.customer_due_date) - r.due_days - 2 
                        ELSE TRUNC(o.customer_due_date) - r.due_days END)
                    WHEN trim(TO_CHAR( TRUNC(o.customer_due_date) - r.due_days, 'Day')) = 'Sunday' THEN
                        TRUNC(o.customer_due_date) - r.due_days - 2
                    WHEN trim(TO_CHAR( TRUNC(o.customer_due_date) - r.due_days, 'Day')) = 'Saturday' THEN
                        TRUNC(o.customer_due_date) - r.due_days - 1
                    WHEN TRUNC(o.customer_due_date) IS NULL THEN
                        TRUNC(SYSDATE) + 60
                    ELSE TRUNC(o.customer_due_date) - r.due_days END
              INTO  P_TASK_SUBJECT,
                    P_DUE_DATE
              FROM  cct_role_properties r,
                    cct_opportunity o
             WHERE  UPPER(r.user_group)= UPPER(g_user_role) AND
                    o.opportunity_number = P_TASK_OBJ.OPPORTUNITY_NUMBER;
                   
        EXCEPTION
        WHEN OTHERS THEN
          g_code := SQLCODE;
          g_msg  := SUBSTR(sqlerrm,1,500 );
          cct_create_errors.record_error(
            p_error_message => g_msg,
            p_error_type => g_code,
            p_module_name => 'insert_task_status - USER GRP not valid',
            p_created_by => USER );
          raise_application_error( -20001, 'USER GRP not valid' );
        END;
    EXCEPTION
    WHEN ex_opp THEN
        g_code := SQLCODE;
        g_msg  := SUBSTR(sqlerrm, 1, 500);
        cct_create_errors.record_error(
            p_task_id => g_task_id,
            p_instance_id => g_instance_id,
            p_error_message => g_msg,
            p_error_type => g_code,
            p_quote_number => g_quote_number,
            p_quote_revision => g_quote_revision,
            p_module_name => 'insert_task_status - OPTY INVALID',
            p_created_by => USER);
        raise_application_error(-20002, 'OPTY not valid');
    WHEN OTHERS THEN
        P_TASK_SUBJECT := 'FAILURE';
        ROLLBACK;
        g_code := SQLCODE;
        g_msg  := SUBSTR(sqlerrm, 1, 500);
        cct_create_errors.record_error(
            p_error_message => g_msg,
            p_error_type => g_code,
            p_opportunity_id => g_opp_id,
            p_module_name => 'CCT_CREATE_TASK.insert_task_status',
            p_created_by => USER);
    END insert_task_status;

END cct_create_task;