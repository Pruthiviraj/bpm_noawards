CREATE OR REPLACE PACKAGE cct_award
AS
/**Modification History
    PERSON        DATE         COMMENTS
    ------------  -----------  ---------------------------------------------
    Ankit Bhatt   26-Sep-2017  Created map_part_with_opportunity proc
    Sureka        04-Oct-2017  Created update_award_headers proc
    Ankit Bhatt   17-Oct-2017  Modified map_part_with_opportunity procdure with
                               addtional conditions 
    Sureka        18-Oct-2017  Created update_awards_summary proc
	Ankit Bhatt   06-Nov-2017  Modified map_part_with_opportunity and update_awards_summary 
                               procedures with cct_quote_parts_snapshot_final table.
                               Modified update_award_headers procedures to update opportunity id
                               correponding to opportunity number in cct_awards table 
    Sureka        09-Nov-2017  Created delete_awards_details proc
**/
    g_code                     VARCHAR2(50);
    g_msg                      VARCHAR2(500);
    g_opportunity_id           NUMBER;
  
    PROCEDURE map_part_with_opportunity(
        P_AWARD_HEADER_ID   IN CCT_AWARD_HEADERS.AWARD_HEADER_ID%TYPE);
    
    PROCEDURE update_award_headers(
        P_OPPORTUNITY_ID    IN CCT_AWARD_HEADERS.OPPORTUNITY_ID%TYPE);
      
    PROCEDURE update_awards_summary(
        P_AWARD_HEADER_ID   IN CCT_AWARD_HEADERS.AWARD_HEADER_ID%TYPE);
      
    PROCEDURE delete_awards_details(  
        P_SOURCE_OPTY_ID    IN CCT_AWARD_HEADERS.SOURCE_OPPORTUNITY_ID%TYPE);
        
END cct_award;