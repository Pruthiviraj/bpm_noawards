create or replace PACKAGE CCT_PROPERTIES_PKG
AS
/**Handles Properties table
   Modification History 
   PERSON        DATE         COMMENTS
   ------------  -----------  ---------------------------------------------
   Sureka        23-Aug-2017  Created insert_properties
   Sureka        23-Aug-2017  Created update_properties
   Sureka        23-Aug-2017  Created get_soap_param
**/

    g_code                     VARCHAR2(50);
    g_msg                      VARCHAR2(500);
    
    PROCEDURE insert_properties(
        P_KEY            IN  CCT_PROPERTIES.KEY%TYPE,
        P_VALUE          IN  CCT_PROPERTIES.VALUE%TYPE DEFAULT NULL, 
        P_DESCRIPTION    IN  CCT_PROPERTIES.DESCRIPTION%TYPE DEFAULT NULL,
        P_CREATED_BY     IN  CCT_PROPERTIES.CREATED_BY%TYPE DEFAULT NULL,
        P_CREATED_DATE   IN  CCT_PROPERTIES.CREATED_DATE%TYPE DEFAULT NULL,
        P_RETURN_STATUS OUT  VARCHAR2);

    PROCEDURE update_properties(
        P_KEY            IN  CCT_PROPERTIES.KEY%TYPE,
        P_VALUE          IN  CCT_PROPERTIES.VALUE%TYPE DEFAULT NULL, 
        P_DESCRIPTION    IN  CCT_PROPERTIES.DESCRIPTION%TYPE DEFAULT NULL,
        P_MODIFIED_BY    IN  CCT_PROPERTIES.MODIFIED_BY%TYPE DEFAULT NULL,
        P_MODIFIED_DATE  IN  CCT_PROPERTIES.MODIFIED_DATE%TYPE DEFAULT NULL,
        P_RETURN_STATUS OUT  VARCHAR2);

    PROCEDURE get_soap_param(
        P_KEY            IN  CCT_PROPERTIES.KEY%TYPE,
        P_VALUE         OUT  CCT_PROPERTIES.VALUE%TYPE);

END CCT_PROPERTIES_PKG;