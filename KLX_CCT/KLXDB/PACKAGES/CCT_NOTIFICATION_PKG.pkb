create or replace PACKAGE BODY             CCT_NOTIFICATION_PKG AS
/**Handles Email Notification loading
  Modification History 
   PERSON        DATE           COMMENTS
   ------------  -----------    ---------------------------------------------
   Sureka        27-Jun-2017    Created load_notification_temp
   Sureka        20-Jul-2017    Created get_mail_body
   Sureka        04-Aug-2017    Created get_mail_details
   Sureka        15-Aug-2017    Created send_daily_notifications
   Ankit Bhatt   10-Oct-2017    Modified date format
   Sureka        17-Oct-2017    Modified get_mail_body 
   Sureka        01-Dec-2017    Created get_to_details
   Sureka        01-Dec-2017    Created get_cc_details
   Sureka        16-Feb-2018    Modified get_mail_details for Notification 19
   Sureka        03-Apr-2018    Modified get_mail_details for Notification 21
   Ankit Bhatt   24-Sep-2018    Modified get_mail_details proc. to handle 
                                CHG0035881 & CHG0035222.
   Ankit Bhatt   25-Sep-2018    Modified  get_mail_body to add reminder mail functionality 
   Infosys       19-Nov-2008    Fix for task number in approval reminder email
   Sudhansu      11-Jul-2019    Fix for Double Digit revision -Line PB 252
**/

    FUNCTION load_notification_temp 
    RETURN VARCHAR2 
    AS
        l_opp_number            CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE;
        l_cust_due_date         CCT_OPPORTUNITY.CUSTOMER_DUE_DATE%TYPE;
        l_opp_id                CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE;
        l_osc_opp_id            CCT_OPPORTUNITY.OSC_OPPORTUNITY_ID%TYPE;
        l_opp_status            CCT_OPPORTUNITY.STATUS%TYPE;
        l_notif_template_id     CCT_NOTIFICATION_TEMP.NOTIF_TEMPLATE_ID%TYPE;
        l_count                 NUMBER := 0;
        l_status                VARCHAR2(20);
--        CURSOR cur_cust_notif 
--        IS
--            SELECT  co.opportunity_number,
--                    co.customer_due_date,
--                    co.opportunity_id,
--                    co.osc_opportunity_id, 
--                    co.status
--              FROM  cct_opportunity co 
--            -- WHERE  (trunc(sysdate) BETWEEN trunc(co.customer_due_date)-5 AND trunc(co.customer_due_date)) AND 
--             WHERE  trunc(customer_due_date)-trunc(SYSDATE) <=2 AND 
--                    UPPER(co.status) = 'ACTIVE' 
--            ORDER BY co.opportunity_number;

        CURSOR cur_appr_notif 
        IS 
            SELECT  DISTINCT co.opportunity_id,
                    co.osc_opportunity_id 
              FROM  cct_quote_approvals cqa JOIN 
                    cct_opportunity co 
                ON  co.opportunity_id = cqa.opportunity_id 
             WHERE  (trunc(sysdate) >= trunc(cqa.creation_date)+1) AND 
                    UPPER(cqa.approval_status) = 'IN PROGRESS';

--        CURSOR cur_proj_file_notif 
--        IS 
--            SELECT co.opportunity_id, 
--                   co.osc_opportunity_id 
--              FROM cct_opportunity co  
--             WHERE UPPER(co.status) = 'ACTIVE' AND 
--                   co.opportunity_id NOT IN
--                       (SELECT opportunity_id 
--                          FROM xxdms.dms_document 
--                         WHERE folder_id= 1 
--                         GROUP BY opportunity_id 
--                        HAVING count(*)>0);
    BEGIN
        DELETE FROM cct_notification_temp;
        COMMIT;

--        OPEN cur_cust_notif;
--        LOOP
--            FETCH cur_cust_notif INTO l_opp_number,l_cust_due_date,l_opp_id,l_osc_opp_id,l_opp_status;
--            EXIT WHEN cur_cust_notif%notfound;
--            --insert into notif temp tbl              
----            l_notif_template_id := 3;
----            
----            INSERT INTO cct_notification_temp(
----                OPPORTUNITY_NUMBER,
----                OPPORTUNITY_ID,
----                CUSTOMER_DUE_DATE,
----                STATUS,
----                NOTIF_TEMPLATE_ID,
----                OSC_OPPORTUNITY_ID
----            ) 
----            VALUES(
----                l_opp_number,
----                NULL, 
----                l_cust_due_date,
----                UPPER(l_opp_status),
----                l_notif_template_id,
----                l_osc_opp_id);
----            COMMIT;
--            
--            IF (trunc(l_cust_due_date)-trunc(SYSDATE) <=2) THEN 
--                l_notif_template_id := 4;
--                INSERT INTO cct_notification_temp(
--                    OPPORTUNITY_NUMBER,
--                    OPPORTUNITY_ID,
--                    CUSTOMER_DUE_DATE,
--                    STATUS,
--                    NOTIF_TEMPLATE_ID,
--                    OSC_OPPORTUNITY_ID 
--                ) 
--                VALUES(
--                    l_opp_number,
--                    l_opp_id, 
--                    l_cust_due_date,
--                    UPPER(l_opp_status),
--                    l_notif_template_id,
--                    NULL);
--                COMMIT;
--            END IF; 
--        END LOOP;
--        CLOSE cur_cust_notif;

        OPEN cur_appr_notif;
        LOOP
            FETCH cur_appr_notif INTO l_opp_id,l_osc_opp_id;
            EXIT WHEN cur_appr_notif%notfound;

            l_notif_template_id := 10;
            cct_fetch_fields.get_opty_num(l_opp_id,l_opp_number);
            INSERT INTO cct_notification_temp(
                OPPORTUNITY_NUMBER,
                OPPORTUNITY_ID,
                NOTIF_TEMPLATE_ID,
                OSC_OPPORTUNITY_ID
            ) 
            VALUES(
                l_opp_number,
                l_opp_id, 
                l_notif_template_id,
                NULL);
            COMMIT;

        END LOOP;
        CLOSE cur_appr_notif;    

--        OPEN cur_proj_file_notif;
--        LOOP
--            FETCH cur_proj_file_notif INTO l_opp_id,l_osc_opp_id;
--            EXIT WHEN cur_proj_file_notif%notfound; 
--            
--            l_notif_template_id := 16;
--            cct_fetch_fields.get_opty_num(l_opp_id,l_opp_number);
--            INSERT INTO cct_notification_temp(
--                OPPORTUNITY_NUMBER,
--                OPPORTUNITY_ID,
--                NOTIF_TEMPLATE_ID,
--                OSC_OPPORTUNITY_ID
--            ) 
--            VALUES(
--                l_opp_number,
--                l_opp_id, 
--                l_notif_template_id,
--                l_osc_opp_id);
--            COMMIT;
--            
--        END LOOP;
--        CLOSE cur_proj_file_notif;
        l_status := 'SUCCESS'; 
        RETURN l_status;
    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,500);
        l_status := 'FAILURE';
        ROLLBACK;
        cct_create_errors.record_error(
            p_opportunity_id   => l_opp_id,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'cct_notification_pkg.load_notification_temp',
            p_created_by       => sys_context('USERENV','OS_USER')
        );
        RETURN l_status;
    END load_notification_temp;

    PROCEDURE get_mail_details(
        P_OPP_NUM            IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE,
        P_NOTIF_TEMPLATE_ID  IN CCT_NOTIFICATION_TYPE.NOTIF_TEMPLATE_ID%TYPE,
        P_SOURCE             IN CCT_NOTIFICATION_REQUEST.SOURCE%TYPE,
        P_MESSAGE_ID         IN CCT_NOTIFICATION_REQUEST.MESSAGE_ID%TYPE,
        P_ATTACHMENT_FLAG    IN CCT_NOTIFICATION_REQUEST.ATTACHMENT_FLAG%TYPE,
        P_OPP_CLOSURE_STATUS IN CCT_NOTIFICATION_REQUEST.OPP_CLOSURE_STATUS%TYPE,
        P_ROLE_NAME          IN CCT_NOTIFICATION_REQUEST.ROLE_NAME%TYPE,
        P_ATTRIBUTE1         IN CCT_NOTIFICATION_REQUEST.ATTRIBUTE1%TYPE,
        P_ATTRIBUTE2         IN CCT_NOTIFICATION_REQUEST.ATTRIBUTE2%TYPE,
        P_ATTRIBUTE3         IN CCT_NOTIFICATION_REQUEST.ATTRIBUTE3%TYPE,
        P_ATTRIBUTE4         IN CCT_NOTIFICATION_REQUEST.ATTRIBUTE4%TYPE,
        P_ACTION             IN CCT_NOTIFICATION_REQUEST.ACTION%TYPE,
        P_SUBJECT        IN OUT CCT_NOTIFICATION_REQUEST.SUBJECT%TYPE,
        P_TO_LIST        IN OUT CCT_NOTIFICATION_REQUEST.TO_LIST%TYPE,
        P_CC_LIST        IN OUT CCT_NOTIFICATION_REQUEST.CC_LIST%TYPE,
        P_BCC_LIST       IN OUT CCT_NOTIFICATION_REQUEST.BCC_LIST%TYPE,
        P_TO_OSC_LIST    IN OUT CCT_NOTIFICATION_REQUEST.TO_OSC_LIST%TYPE,
        P_CC_OSC_LIST    IN OUT CCT_NOTIFICATION_REQUEST.CC_OSC_LIST%TYPE,
        P_BODY           IN OUT CCT_NOTIFICATION_REQUEST.BODY%TYPE,
        P_REQUEST_ID        OUT CCT_NOTIFICATION_REQUEST.NOTIFICATION_REQ_ID%TYPE,
        P_RETURN_STATUS     OUT VARCHAR2) 
    AS
        l_opp_id                CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE;
        l_osc_opp_id            CCT_OPPORTUNITY.OSC_OPPORTUNITY_ID%TYPE;
        l_snap_ver              CCT_SNAPSHOT_APPROVAL.SNAPSHOT_VERSION%TYPE;
        l_subject               CCT_NOTIFICATION_TYPE.SUBJECT%TYPE;
        l_sql_query             CCT_NOTIFICATION_TYPE.SQL_QUERY%TYPE;
        l_revision_id           CCT_QUOTE_REVISIONS.REVISION_ID%TYPE;
        l_role_name             CCT_NOTIFICATION_REQUEST.ROLE_NAME%TYPE;
        l_queue_name            CCT_PARTS_QUEUE.QUEUE_NAME%TYPE;
        l_app_comments          VARCHAR2(2000);
        l_grp_name              VARCHAR2(100);
        role_exc                EXCEPTION;
        rev_exc                 EXCEPTION;
        app_exc                 EXCEPTION;
        tsk_exc                 EXCEPTION;
        cust_date_exc           EXCEPTION;
        grp_exc                 EXCEPTION;
    BEGIN
    -- execute immediate 'alter session set max_dump_file_size=unlimited';
    --execute immediate 'alter session set tracefile_identifier="KAMAL"';
    --  execute immediate 'alter session set events ''10046 trace name context forever, level 12''';
        P_REQUEST_ID   := cct_notification_req_seq.nextval;
        cct_fetch_fields.get_opty_id(P_OPP_NUM,l_opp_id);
        cct_fetch_fields.get_osc_opty_id(P_OPP_NUM,l_osc_opp_id);
        BEGIN
            SELECT  subject,
                    sql_query
              INTO  l_subject,
                    l_sql_query
              FROM  cct_notification_type
             WHERE  notif_template_id = P_NOTIF_TEMPLATE_ID;
        EXCEPTION
        WHEN OTHERS THEN
            g_code := sqlcode;
            g_msg := substr(sqlerrm,1,500);
            p_return_status := 'FAILURE';
            cct_create_errors.record_notif_error(
                p_notif_req_id    => P_REQUEST_ID,
                p_notif_message   => g_code || '-' || g_msg,
                p_notif_status    => P_RETURN_STATUS,
                p_notif_date      => SYSDATE
            );
        END;

        --DBMS_OUTPUT.PUT_LINE(l_sql_query);
        l_sql_query := REPLACE(l_sql_query,':num','''' || P_OPP_NUM || '''');
        l_sql_query := REPLACE(l_sql_query,':opp_id',l_opp_id);
        BEGIN
            SELECT MAX(to_number(snapshot_version))||'.0'
              INTO l_snap_ver
              FROM cct_snapshot_approval
             WHERE opportunity_id = l_opp_id AND
                   snapshot_version IS NOT NULL;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN 
           NULL;
        WHEN OTHERS THEN
            g_code := sqlcode;
            g_msg := substr(sqlerrm,1,500);
            p_return_status := 'FAILURE';
            cct_create_errors.record_notif_error(
                p_notif_req_id    => P_REQUEST_ID,
                p_notif_message   => g_code || '-' || g_msg,
                p_notif_status    => P_RETURN_STATUS,
                p_notif_date      => SYSDATE
            );
        END;
        l_sql_query := REPLACE(l_sql_query,':snap_ver','''' || l_snap_ver || '''');
        l_role_name := UPPER(P_ROLE_NAME);

        IF P_NOTIF_TEMPLATE_ID = 2 THEN 
            l_role_name := 'SCCCT_PROPOSAL_MANAGER'; 
        ELSIF P_NOTIF_TEMPLATE_ID = 5 THEN 
    /* modified role_name from SCCCT_HONEYWELL_PRODUCT_LINE to scCCT_CSM to handle CHG0035881 & CHG0035222 defects */ 
            --l_role_name := 'SCCCT_HONEYWELL_PRODUCT_LINE'; 
      l_role_name := upper('scCCT_CSM');      
    /* modified role_name from SCCCT_HONEYWELL_PRODUCT_LINE to scCCT_CSM to handle CHG0035881 & CHG0035222 defects */         
        END IF;

        IF INSTR(l_sql_query,':role') > 0 THEN
            IF l_role_name IS NULL THEN
                RAISE role_exc;
            ELSE
                l_sql_query := REPLACE(l_sql_query,':role','''' || l_role_name || '''');
            END IF;
        END IF;

        IF INSTR(l_sql_query,':app_name') > 0 THEN
            IF P_ATTRIBUTE1 IS NULL THEN
                RAISE app_exc;
            ELSE
                l_sql_query := REPLACE(l_sql_query,':app_name',
                    '''' || P_ATTRIBUTE1 || '''');
            END IF;
        END IF;

        IF INSTR(l_sql_query,':rev') > 0 THEN
            cct_fetch_fields.get_latest_revision(P_OPP_NUM,l_opp_id,l_revision_id);
            l_sql_query := REPLACE(l_sql_query,':rev',l_revision_id);
        END IF;

        IF INSTR(l_sql_query,':old_cust_date') > 0 THEN
            IF P_ATTRIBUTE3 IS NULL THEN
                RAISE cust_date_exc;
            ELSE
                l_sql_query := REPLACE(l_sql_query,':old_cust_date',
                    '''' || P_ATTRIBUTE3 || '''');
            END IF;
        END IF;

        IF INSTR(l_sql_query,':task_desc') > 0 THEN
            IF P_ATTRIBUTE2 IS NULL THEN
                l_sql_query := REPLACE(l_sql_query,':task_desc',
                    ''' ''');
            ELSE
                l_sql_query := REPLACE(l_sql_query,':task_desc',
                    '''' || P_ATTRIBUTE2 || '''');
            END IF;
        END IF;

        IF P_TO_LIST IS NULL THEN
        --get to list
            get_to_details(P_REQUEST_ID,P_NOTIF_TEMPLATE_ID,P_TO_LIST,P_TO_OSC_LIST);
        END IF;

        IF P_CC_LIST IS NULL THEN
        --get cc list
            get_cc_details(P_REQUEST_ID,P_NOTIF_TEMPLATE_ID,P_CC_LIST,P_CC_OSC_LIST);
        END IF;

        IF P_SUBJECT IS NULL THEN
        --get subject
            IF INSTR(l_subject,'OPTY NUMBER') > 0 THEN 
                l_subject := REPLACE(l_subject,'OPTY NUMBER',P_OPP_NUM);
            END IF;
            IF INSTR(l_subject,'APP STATUS') > 0 THEN 
                l_subject := REPLACE(l_subject,'APP STATUS',
                            (CASE WHEN UPPER(P_ATTRIBUTE2)='APPROVE' THEN 'Approval' ELSE 'Rejection' END)) ;
            END IF;
            IF INSTR(l_subject,'$QUEUE$') > 0 THEN
                SELECT INITCAP(queue_name) 
                  INTO l_queue_name 
                  FROM cct_parts_queue 
                 WHERE queue_id = (SELECT queue_id 
                                     FROM cct_role_properties 
                                    WHERE UPPER(USER_GROUP) = l_role_name);
                l_subject := REPLACE(l_subject,'$QUEUE$',l_queue_name);
            END IF;
            P_SUBJECT := l_subject;
        END IF;

        IF P_BODY IS NULL THEN
        --get html mail body
            get_mail_body(P_REQUEST_ID,P_OPP_NUM,P_NOTIF_TEMPLATE_ID,l_sql_query,P_BODY);
        END IF;

        IF P_NOTIF_TEMPLATE_ID = 2 THEN
            IF UPPER(P_ACTION) = 'REVISED' THEN
                P_SUBJECT := REPLACE(P_SUBJECT,'New','Revised');
            END IF;
        END IF;
        IF P_NOTIF_TEMPLATE_ID = 9 THEN
            BEGIN 
                SELECT crp.group_name || '$APPDETAILS$' || cau.approval_user_name 
                  INTO l_app_comments 
                  FROM cct_approval_users cau,
                       cct_snapshot_approval csa,
                       cct_snapshot cs, 
                       cct_role_properties crp 
                 WHERE cau.opportunity_id = csa.opportunity_id and 
                       cau.opportunity_id = cs.opportunity_id and 
                       csa.snapshot_version= cs.snapshot_version and 
                       crp.user_group_id = cau.approval_role_id and 
                       cau.opportunity_id = l_opp_id and 
                       csa.snapshot_version = l_snap_ver and 
                       crp.user_group = P_ATTRIBUTE4;
            EXCEPTION 
            WHEN OTHERS THEN
                g_code := sqlcode;
                g_msg := substr(sqlerrm,1,500);
                P_RETURN_STATUS := 'FAILURE';
                cct_create_errors.record_notif_error(
                    p_notif_req_id    => P_REQUEST_ID,
                    p_notif_message   => g_code || '-' || g_msg,
                    p_notif_status    => P_RETURN_STATUS,
                    p_notif_date      => SYSDATE
                );
            END;
            l_app_comments := REPLACE(l_app_comments,'$APPDETAILS$','</b></th><td>');
            l_app_comments := '<tr><th><b>' || l_app_comments || '</td></tr></table>';
            P_BODY :=  REPLACE(P_BODY,'</table>',l_app_comments);
        END IF;
        IF P_NOTIF_TEMPLATE_ID IN (9,10) THEN /* Added on 19-Nov-2008 */
           IF INSTR(P_BODY,'$TSKNO$') > 0 THEN
               IF P_ATTRIBUTE3 IS NULL THEN 
                   RAISE tsk_exc;
               ELSE 
                   P_BODY := REPLACE(P_BODY,'$TSKNO$',P_ATTRIBUTE3);
               END IF;
           END IF;
        END IF;



        IF INSTR(P_BODY,'$NOTES$') > 0 THEN
            IF P_ATTRIBUTE2 IS NULL THEN 
                P_BODY := REPLACE(P_BODY,'$NOTES$','');
            ELSE 
                P_BODY := REPLACE(P_BODY,'$NOTES$',P_ATTRIBUTE2);
            END IF;
        END IF;
        IF INSTR(P_BODY,'$QUEUE$') > 0 THEN 
            P_BODY := REPLACE(P_BODY,'$QUEUE$', '<br><br>' || l_queue_name );
        END IF;
        IF INSTR(P_BODY,'$GRP$') > 0 OR INSTR(P_BODY,'$GRP_NAME$') > 0 THEN
            IF P_ATTRIBUTE4 IS NULL THEN 
                RAISE grp_exc;
            ELSE 
                IF P_NOTIF_TEMPLATE_ID <> 20 THEN 
                    P_BODY := REPLACE(P_BODY,'$GRP$',P_ATTRIBUTE4);
                ELSE 
                    BEGIN 
                        SELECT group_name 
                          INTO l_grp_name 
                          FROM cct_role_properties 
                         WHERE user_group = P_ATTRIBUTE4;
                    EXCEPTION 
                        WHEN OTHERS THEN 
                        RAISE grp_exc;
                    END;
                    P_BODY := REPLACE(P_BODY,'$GRP_NAME$',l_grp_name);
                END IF;
            END IF;
        END IF;

        IF P_NOTIF_TEMPLATE_ID = 21 THEN 
            IF P_ATTRIBUTE1 IS NULL THEN 
                P_BODY := REPLACE(P_BODY,'ATTR1#',' ');
            ELSE 
                P_BODY := REPLACE(P_BODY,'ATTR1#',P_ATTRIBUTE1);
            END IF;
            IF P_ATTRIBUTE2 IS NULL THEN 
                P_BODY := REPLACE(P_BODY,'ATTR2#',' ');
            ELSE 
                P_BODY := REPLACE(P_BODY,'ATTR2#',P_ATTRIBUTE2);
            END IF;
            IF P_ATTRIBUTE3 IS NULL THEN 
                P_BODY := REPLACE(P_BODY,'ATTR3#',' ');
            ELSE 
                P_BODY := REPLACE(P_BODY,'ATTR3#',P_ATTRIBUTE3);
            END IF;
            IF P_ATTRIBUTE4 IS NULL THEN 
                P_BODY := REPLACE(P_BODY,'ATTR4#',' ');
            ELSE 
                P_BODY := REPLACE(P_BODY,'ATTR4#',P_ATTRIBUTE4);
            END IF;
        END IF;
        IF INSTR(P_BODY,'APP STATUS') > 0 THEN 
            P_BODY := REPLACE(P_BODY,'APP STATUS',(CASE WHEN UPPER(P_ATTRIBUTE2)='APPROVE' THEN 'Approved' ELSE 'Rejected' END));
        END IF;
        IF INSTR(P_BODY,'$COLSPAN$2$') > 0 THEN 
            P_BODY := REPLACE(P_BODY,'<td>$COLSPAN$2$','<td colspan="2">');
        END IF;
        INSERT INTO cct_notification_request(
            NOTIFICATION_REQ_ID,
            OPPORTUNITY_ID,
            NOTIF_TEMPLATE_ID,
            MESSAGE_ID,
            ATTACHMENT_FLAG,
            OPP_CLOSURE_STATUS,
            SOURCE,
            ROLE_NAME,
            ATTRIBUTE1,
            ATTRIBUTE2,
            ATTRIBUTE3,
            ATTRIBUTE4,
            ACTION,
            SUBJECT,
            BODY,
            TO_LIST,
            CC_LIST,
            BCC_LIST,
            TO_OSC_LIST,
            CC_OSC_LIST,
            CREATION_DATE,
            CREATED_BY
        ) 
        VALUES(
            P_REQUEST_ID,
            l_opp_id,
            P_NOTIF_TEMPLATE_ID,
            P_MESSAGE_ID,
            P_ATTACHMENT_FLAG,
            P_OPP_CLOSURE_STATUS,
            P_SOURCE,
            P_ROLE_NAME,
            P_ATTRIBUTE1,
            P_ATTRIBUTE2,
            P_ATTRIBUTE3,
            P_ATTRIBUTE4,
            P_ACTION,
            P_SUBJECT,
            P_BODY,
            P_TO_LIST,
            P_CC_LIST,
            P_BCC_LIST,
            P_TO_OSC_LIST,
            P_CC_OSC_LIST,
            SYSDATE,
            sys_context('USERENV','OS_USER'));
        COMMIT;
        P_RETURN_STATUS := 'SUCCESS';
    EXCEPTION
    WHEN role_exc THEN
        g_code := '-20001';
        g_msg := 'NO ROLE IN REQUEST';
        P_RETURN_STATUS := 'FAILURE';
        cct_create_errors.record_notif_error(
            p_notif_req_id    => P_REQUEST_ID,
            p_notif_message   => g_code || '-' || g_msg,
            p_notif_status    => P_RETURN_STATUS,
            p_notif_date      => SYSDATE
        );
    WHEN app_exc THEN
        g_code := '-20002';
        g_msg := 'NO APPROVER MENTIONED IN REQUEST';
        P_RETURN_STATUS := 'FAILURE';
        cct_create_errors.record_notif_error(
            p_notif_req_id    => P_REQUEST_ID,
            p_notif_message   => g_code || '-' || g_msg,
            p_notif_status    => P_RETURN_STATUS,
            p_notif_date      => SYSDATE
        );
    WHEN rev_exc THEN
        g_code := '-20003';
        g_msg := 'NO REVISION CREATED FOR GIVEN OPTY';
        P_RETURN_STATUS := 'FAILURE';
        cct_create_errors.record_notif_error(
            p_notif_req_id    => P_REQUEST_ID,
            p_notif_message   => g_code || '-' || g_msg,
            p_notif_status    => P_RETURN_STATUS,
            p_notif_date      => SYSDATE
        );
    WHEN tsk_exc THEN
        g_code := '-20004';
        g_msg := 'NO TASK NUMBER PRESENT IN REQUEST';
        P_RETURN_STATUS := 'FAILURE';
        cct_create_errors.record_notif_error(
            p_notif_req_id    => P_REQUEST_ID,
            p_notif_message   => g_code || '-' || g_msg,
            p_notif_status    => P_RETURN_STATUS,
            p_notif_date      => SYSDATE
        );
    WHEN cust_date_exc THEN
        g_code := '-20005';
        g_msg := 'NO OLD CUSTOMER DUE DATE ADDED FOR GIVEN OPTY';
        P_RETURN_STATUS := 'FAILURE';
        cct_create_errors.record_notif_error(
            p_notif_req_id    => P_REQUEST_ID,
            p_notif_message   => g_code || '-' || g_msg,
            p_notif_status    => P_RETURN_STATUS,
            p_notif_date      => SYSDATE
        ); 
    WHEN grp_exc THEN
        g_code := '-20006';
        g_msg := 'NO GROUP MENTIONED IN REQUEST';
        P_RETURN_STATUS := 'FAILURE';
        cct_create_errors.record_notif_error(
            p_notif_req_id    => P_REQUEST_ID,
            p_notif_message   => g_code || '-' || g_msg,
            p_notif_status    => P_RETURN_STATUS,
            p_notif_date      => SYSDATE
        ); 
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,500);
        P_RETURN_STATUS := 'FAILURE';
        cct_create_errors.record_notif_error(
            p_notif_req_id    => P_REQUEST_ID,
            p_notif_message   => g_code || '-' || g_msg,
            p_notif_status    => P_RETURN_STATUS,
            p_notif_date      => SYSDATE
        );
    END get_mail_details;

    PROCEDURE get_mail_body(
        P_REQUEST_ID         IN CCT_NOTIFICATION_REQUEST.NOTIFICATION_REQ_ID%TYPE,
        P_OPP_NUM            IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE,
        P_NOTIF_TEMPLATE_ID  IN CCT_NOTIFICATION_TYPE.NOTIF_TEMPLATE_ID%TYPE,
        P_SQL_QUERY          IN CLOB,
        P_BODY              OUT CLOB) 
    AS
        CURSOR cur_tbl_header 
        IS 
            SELECT  table_header
              FROM  cct_notification_template
             WHERE  notif_template_id = P_NOTIF_TEMPLATE_ID
            ORDER BY seq_order ASC;
        l_tbl_header            CLOB;
        TYPE tbldatacurtyp IS REF CURSOR;
        cur_tbl_data            tbldatacurtyp;
        field_value             VARCHAR2(500);
        field_name              VARCHAR2(500);
        tbl_header              CCT_NOTIFICATION_TEMPLATE.TABLE_HEADER%TYPE;
        l_osc_link              VARCHAR2(1000);
        l_doc_link              VARCHAR2(1000);
        l_opty_flag             CCT_NOTIFICATION_TYPE.OPTY_FLAG%TYPE;
        l_header_flag           CCT_NOTIFICATION_TYPE.HEADER_FLAG%TYPE;
        l_footer_flag           CCT_NOTIFICATION_TYPE.FOOTER_FLAG%TYPE;
        --l_hostname              VARCHAR2(500);
        l_body_text             CCT_NOTIFICATION_TYPE.BODY_TEXT%TYPE;
        l_cct_link              VARCHAR2(2000);
        l_row_data              VARCHAR2(1000);    
        l_opp_id                CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE;
        l_add_changes           NUMBER; 
        l_notes                 VARCHAR2(2000);
        l_header                VARCHAR2(2000);
        l_footer                VARCHAR2(2000);
                                l_appr_mailid           VARCHAR2(2000);
        l_app_link              VARCHAR2(2000);
        l_rej_link              VARCHAR2(2000);
        l_app_subject           VARCHAR2(2000);
    BEGIN
        BEGIN
            SELECT  body_text, 
                    opty_flag,
                    header_flag,
                    footer_flag 
              INTO  l_body_text,
                    l_opty_flag,
                    l_header_flag,
                    l_footer_flag 
              FROM  cct_notification_type
             WHERE  notif_template_id = P_NOTIF_TEMPLATE_ID;
        EXCEPTION
        WHEN OTHERS THEN
            g_code := sqlcode;
            g_msg := substr(sqlerrm,1,500);
            cct_create_errors.record_notif_error(
                p_notif_req_id    => P_REQUEST_ID,
                p_notif_message   => g_code || '-' || g_msg,
                p_notif_status    => 'FAILURE',
                p_notif_date      => SYSDATE
            );
        END;
        IF UPPER(l_header_flag) = 'Y' THEN 
            cct_properties_pkg.get_soap_param('NOTIF_HEADER',l_header); 
            l_header := REPLACE(l_header,'$HEADER$','<br><br>');
       ELSE 
            l_header := '<br>';
        END IF;
        IF P_NOTIF_TEMPLATE_ID = 9 THEN 
            cct_properties_pkg.get_soap_param('NOTIF_OSC_LINK_APPROVAL',l_osc_link);
            l_notes := '<br><br><b> Notes : </b> $NOTES$ <br><br>';
        ELSIF P_NOTIF_TEMPLATE_ID = 10 THEN  
            cct_properties_pkg.get_soap_param('NOTIF_OSC_LINK_APPROVAL',l_osc_link);
            l_notes := '<br>';
        ELSIF P_NOTIF_TEMPLATE_ID = 17 THEN 
            cct_properties_pkg.get_soap_param('NOTIF_OSC_LINK',l_osc_link);
            cct_properties_pkg.get_soap_param('NOTIF_DOC_LINK',l_doc_link);
            l_notes := '<br>';
            l_doc_link := '<a href="' || l_doc_link || '">';
            l_doc_link := REPLACE(l_doc_link,'$OPP$',P_OPP_NUM);
        ELSE 
            cct_properties_pkg.get_soap_param('NOTIF_OSC_LINK',l_osc_link);
            l_notes := '<br>';
        END IF;
        --link to OPTY#
        --l_osc_link := l_hostname || l_osc_link;
        l_osc_link := '<a href="' || l_osc_link || '">';
        l_osc_link := REPLACE(l_osc_link,'$OPP$',P_OPP_NUM);
        IF INSTR(l_body_text,'OPTY NUMBER') > 0 THEN 
                l_body_text := REPLACE(l_body_text,'OPTY NUMBER',l_osc_link || P_OPP_NUM || '</a>');
        END IF;
        --P_BODY := P_BODY || l_osc_link || field_value || '</a>';
        P_BODY := '<body style="font-family: Calibri"><br>' || l_body_text || l_header ;
        P_BODY := P_BODY || '<br><br><style>';
        P_BODY := P_BODY || 'th { background-color: #F5F5F5; text-align: left; font-family: Calibri; }';
        P_BODY := P_BODY || 'td { font-family: Calibri; } </style>';
        P_BODY := P_BODY || '<table border=''1'' cellpadding=''2'' cellspacing=''0''>';

        OPEN cur_tbl_header;
        LOOP
            FETCH cur_tbl_header INTO tbl_header;
            EXIT WHEN cur_tbl_header%notfound;
            OPEN cur_tbl_data FOR P_SQL_QUERY;
            LOOP
                FETCH cur_tbl_data INTO field_value, field_name;
                EXIT WHEN cur_tbl_data%notfound;
                IF UPPER(field_name) = UPPER(tbl_header) THEN
                    P_BODY := P_BODY || '<tr><th><b>' || tbl_header;
                    P_BODY := P_BODY || '</b></th><td>';
                    IF INSTR(field_value,'$$') > 0 THEN
                        field_value := REPLACE(field_value,'$$','</td><td>');
                    END IF;
                    IF (((UPPER(field_name) = 'OPPORTUNITY') OR 
                         (UPPER(field_name) = 'CRF NUMBER')) AND 
                        (UPPER(l_opty_flag) = 'Y')) THEN 
                        P_BODY := P_BODY || l_osc_link || field_value || '</a>';
                    ELSIF UPPER(field_name) = 'DOCUMENTS' THEN 
                        P_BODY := P_BODY || l_doc_link || field_value || '</a>';
                    ELSIF UPPER(field_name) = 'APPROVAL STATUS' THEN 
                        P_BODY := P_BODY || '<b>' || field_value || '</b>';
                                                                                ELSE
                        P_BODY := P_BODY || field_value;
                    END IF;
                    P_BODY := P_BODY || '</td></tr>';
                END IF;
            END LOOP;
            CLOSE cur_tbl_data;
        END LOOP;
        CLOSE cur_tbl_header;
        P_BODY := REPLACE(P_BODY,'<td></td>','<td>  </td>');
        P_BODY := P_BODY || '</table>';
        /*IF P_NOTIF_TEMPLATE_ID = 12 OR P_NOTIF_TEMPLATE_ID = 9 THEN 
           cct_fetch_fields.get_opty_id(P_OPP_NUM,l_opp_id);
           P_BODY := P_BODY || '<br><br>';
           P_BODY := P_BODY || '<table border=''1'' cellpadding=''2'' cellspacing=''0''>';
           P_BODY := P_BODY || '<tr><th> </th><th>Total Parts</th><th>Parts With Cost</th>';
           P_BODY := P_BODY || '<th>Parts Not Quoted</th><th>Total Cost</th><th>% Difference</th></tr>';
           WITH T  (data_type,total_parts,parts_With_Cost,parts_not_quoted,total_cost,perc_difference) AS  
            (SELECT * FROM (SELECT data_type,total_parts,parts_With_Cost,parts_not_quoted,TRIM(TO_CHAR(round(total_cost),'999,999,999,999,999')),perc_difference 
               FROM cct_cost_compare_header 
              WHERE opportunity_id=l_opp_id AND data_type='Snapshot' 
              UNION ALL 
             SELECT 'Snapshot', 0,0,0,'0',0 from dual) where rownum=1)  
             SELECT '<tr><td>'||data_type||'</td><td>'||total_parts ||'</td><td>'||parts_With_Cost||'</td><td>'||parts_not_quoted||'</td><td>'||total_cost||'</td><td>'||perc_difference||'</td></tr>'  
               INTO l_row_data   
               FROM t;        
           P_BODY := P_BODY || l_row_data;
           WITH T  (data_type,total_parts,parts_With_Cost,parts_not_quoted,total_cost,perc_difference) AS  
            (SELECT * FROM (SELECT data_type,total_parts,parts_With_Cost,parts_not_quoted,TRIM(TO_CHAR(round(total_cost),'999,999,999,999,999')),perc_difference 
               FROM cct_cost_compare_header 
              WHERE opportunity_id=l_opp_id AND data_type='Current' 
              UNION ALL 
             SELECT 'Current', 0,0,0,'0',0 from dual) where rownum=1)  
             SELECT '<tr><td>'||data_type||'</td><td>'||total_parts ||'</td><td>'||parts_With_Cost||'</td><td>'||parts_not_quoted||'</td><td>'||total_cost||'</td><td>'||perc_difference||'</td></tr>'  
               INTO l_row_data   
               FROM t;        
           P_BODY := P_BODY || l_row_data;
           WITH T  (data_type,total_parts,parts_With_Cost,parts_not_quoted,total_cost,perc_difference) AS  
            (SELECT * FROM (SELECT data_type,total_parts,parts_With_Cost,parts_not_quoted,TRIM(TO_CHAR(round(total_cost),'999,999,999,999,999')),perc_difference 
               FROM cct_cost_compare_header 
              WHERE opportunity_id=l_opp_id AND data_type='Delta' 
              UNION ALL 
             SELECT 'Delta', 0,0,0,'0',0 from dual) where rownum=1)  
             SELECT '<tr><td>'||data_type||'</td><td>'||total_parts ||'</td><td>'||parts_With_Cost||'</td><td>'||parts_not_quoted||'</td><td>'||total_cost||'</td><td>'||perc_difference||'</td></tr>'  
               INTO l_row_data   
               FROM t;        
           P_BODY := P_BODY || l_row_data;
           P_BODY := P_BODY || '</table>';
           P_BODY := P_BODY || '<br><br>';
           BEGIN
               SELECT additional_changes 
                 INTO l_add_changes 
                 FROM cct_cost_compare_header 
                WHERE opportunity_id=l_opp_id AND data_type='Delta';
           EXCEPTION 
           WHEN OTHERS THEN 
            l_add_changes := 0;
           END;
           P_BODY := P_BODY || '<table border=''1'' cellpadding=''2'' cellspacing=''0''>';
           P_BODY := P_BODY || '<tr><th>Additional Changes</th><td>' || l_add_changes || '</td></tr>';
           P_BODY := P_BODY || '</table>';           
        END IF; */
        P_BODY := P_BODY || l_notes;
        IF UPPER(l_footer_flag) = 'Y' THEN 
            --Add email footer
            cct_properties_pkg.get_soap_param('NOTIF_CCT',l_cct_link);
            --l_cct_link := l_hostname || l_cct_link;
            l_cct_link := '<a href="' || l_cct_link || '">';
            P_BODY := P_BODY || '<br><br>';
            cct_properties_pkg.get_soap_param('NOTIF_FOOTER',l_footer);
            l_footer := REPLACE(l_footer,'$CCTLINK$',l_cct_link);
            P_BODY := P_BODY || l_footer || '</a><br><br>';
                                ELSE 
            IF P_NOTIF_TEMPLATE_ID in (9,10) THEN   --added notif_template_ID = 10 for reminder mail functionality
                cct_properties_pkg.get_soap_param('MAIL_USERID',l_appr_mailid);
                l_app_subject := 'CRFNo=' || P_OPP_NUM || ':TaskNumber=';
                l_app_subject := l_app_subject || '$TSKNO$:Action=$APPSTAT$:';
                l_app_subject := l_app_subject || 'Group=$GRP$';
                --approval
                l_app_link := '<a href="mailto:' || l_appr_mailid || '?subject=' ;
                l_app_link := l_app_link || REPLACE(l_app_subject,'$APPSTAT$','APPROVE');
                l_app_link := l_app_link || '"> Click here to <b>Approve</b> ' ;
                l_app_link := l_app_link || P_OPP_NUM || ' through email';
                P_BODY := P_BODY || l_app_link || '</a><br><br>';
                l_rej_link := '<a href="mailto:' || l_appr_mailid || '?subject=' ;
                l_rej_link := l_rej_link || REPLACE(l_app_subject,'$APPSTAT$','REJECT');
                l_rej_link := l_rej_link || '"> Click here to <b>Reject</b> ' ;
                l_rej_link := l_rej_link || P_OPP_NUM || ' through email';
                P_BODY := P_BODY || l_rej_link || '</a><br><br>';
                P_BODY := P_BODY || l_osc_link;
                P_BODY := P_BODY || 'Click here to Approve/Reject OPTY NUMBER through the application.</a><br>';
                P_BODY := REPLACE(P_BODY ,'$OPP$',P_OPP_NUM);
            END IF;
        END IF;
        P_BODY := P_BODY || '</body>';
        IF INSTR(P_BODY,'OPTY NUMBER') > 0 THEN 
                P_BODY := REPLACE(P_BODY,'OPTY NUMBER',P_OPP_NUM);
        END IF;
        --dbms_output.put_line(P_BODY);
    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,500);
        cct_create_errors.record_notif_error(
            p_notif_req_id    => P_REQUEST_ID,
            p_notif_message   => 'get_mail_body' || g_code || '-' || g_msg,
            p_notif_status    => 'FAILURE',
            p_notif_date      => SYSDATE
        );
    END get_mail_body;

    PROCEDURE get_to_details(
        P_REQUEST_ID         IN CCT_NOTIFICATION_REQUEST.NOTIFICATION_REQ_ID%TYPE,
        P_NOTIF_TEMPLATE_ID  IN CCT_NOTIFICATION_TYPE.NOTIF_TEMPLATE_ID%TYPE,
        P_TO_LIST           OUT CLOB,
        P_TO_OSC_LIST       OUT CLOB) 
    AS 
        CURSOR cur_to_list 
        IS 
            SELECT  crp.USER_GROUP 
              FROM  cct_notification_role cnr, 
                    cct_role_properties crp 
             WHERE  notif_template_id = P_NOTIF_TEMPLATE_ID AND 
                    crp.USER_GROUP_ID = cnr.TO_USER_GROUP_ID; 
        CURSOR cur_to_osc_list 
        IS 
            SELECT  crp.OSC_GROUP 
              FROM  cct_notification_role cnr, 
                    cct_role_properties crp 
             WHERE  notif_template_id = P_NOTIF_TEMPLATE_ID AND 
                    crp.USER_GROUP_ID = cnr.TO_USER_GROUP_ID; 
        l_to_user_group         CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
        l_to_osc_group          CCT_ROLE_PROPERTIES.OSC_GROUP%TYPE;
    BEGIN 
        OPEN cur_to_list;
        LOOP
            FETCH cur_to_list INTO l_to_user_group;
            EXIT WHEN cur_to_list%notfound; 
            IF P_TO_LIST IS NULL THEN 
                P_TO_LIST := l_to_user_group || ',' ;
            ELSE 
                P_TO_LIST := P_TO_LIST || l_to_user_group || ',' ;
            END IF;
        END LOOP; 
        CLOSE cur_to_list;
        IF P_TO_LIST IS NOT NULL THEN 
            P_TO_LIST := SUBSTR(P_TO_LIST,1,length(P_TO_LIST)-1);
        END IF;

        OPEN cur_to_osc_list;
        LOOP
            FETCH cur_to_osc_list INTO l_to_osc_group;
            EXIT WHEN cur_to_osc_list%notfound; 

            IF l_to_osc_group IS NOT NULL THEN 
                IF P_TO_OSC_LIST IS NULL THEN 
                    P_TO_OSC_LIST := l_to_osc_group || ',' ;
                ELSE 
                    P_TO_OSC_LIST := P_TO_OSC_LIST || l_to_osc_group || ',' ;
                END IF;
            END IF;
        END LOOP; 
        CLOSE cur_to_osc_list;
        IF P_TO_OSC_LIST IS NOT NULL THEN 
            P_TO_OSC_LIST := SUBSTR(P_TO_OSC_LIST,1,length(P_TO_OSC_LIST)-1);
        END IF;
    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,500);
        cct_create_errors.record_notif_error(
            p_notif_req_id    => P_REQUEST_ID,
            p_notif_message   => 'get_to_details' || g_code || '-' || g_msg,
            p_notif_status    => 'FAILURE',
            p_notif_date      => SYSDATE
        );
    END get_to_details;

    PROCEDURE get_cc_details(
        P_REQUEST_ID         IN CCT_NOTIFICATION_REQUEST.NOTIFICATION_REQ_ID%TYPE,
        P_NOTIF_TEMPLATE_ID  IN CCT_NOTIFICATION_TYPE.NOTIF_TEMPLATE_ID%TYPE,
        P_CC_LIST           OUT CLOB,
        P_CC_OSC_LIST       OUT CLOB)  
    AS 
        CURSOR cur_cc_list 
        IS 
            SELECT  crp.USER_GROUP 
              FROM  cct_notification_role cnr, 
                    cct_role_properties crp 
             WHERE  notif_template_id = P_NOTIF_TEMPLATE_ID AND 
                    crp.USER_GROUP_ID = cnr.CC_USER_GROUP_ID;
        CURSOR cur_cc_osc_list 
        IS 
            SELECT  crp.OSC_GROUP 
              FROM  cct_notification_role cnr, 
                    cct_role_properties crp 
             WHERE  notif_template_id = P_NOTIF_TEMPLATE_ID AND 
                    crp.USER_GROUP_ID = cnr.CC_USER_GROUP_ID;

        l_cc_user_group         CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
        l_cc_osc_group          CCT_ROLE_PROPERTIES.OSC_GROUP%TYPE;
    BEGIN 
        OPEN cur_cc_list;
        LOOP
            FETCH cur_cc_list INTO l_cc_user_group;
            EXIT WHEN cur_cc_list%notfound; 
            IF P_CC_LIST IS NULL THEN 
                P_CC_LIST := l_cc_user_group || ',' ;
            ELSE 
                P_CC_LIST := P_CC_LIST || l_cc_user_group || ',' ;
            END IF;
        END LOOP; 
        CLOSE cur_cc_list;
        IF P_CC_LIST IS NOT NULL THEN 
            P_CC_LIST := SUBSTR(P_CC_LIST,1,length(P_CC_LIST)-1);
        END IF; 

        OPEN cur_cc_osc_list;
        LOOP
            FETCH cur_cc_osc_list INTO l_cc_osc_group;
            EXIT WHEN cur_cc_osc_list%notfound; 
            IF l_cc_osc_group IS NOT NULL THEN 
                IF P_CC_OSC_LIST IS NULL THEN 
                    P_CC_OSC_LIST := l_cc_osc_group || ',' ;
                ELSE 
                    P_CC_OSC_LIST := P_CC_OSC_LIST || l_cc_osc_group || ',' ;
                END IF;
           END IF;
        END LOOP; 
        CLOSE cur_cc_osc_list;
        IF P_CC_OSC_LIST IS NOT NULL THEN 
            P_CC_OSC_LIST := SUBSTR(P_CC_OSC_LIST,1,length(P_CC_OSC_LIST)-1);
        END IF;  
    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,500);
        cct_create_errors.record_notif_error(
            p_notif_req_id    => P_REQUEST_ID,
            p_notif_message   => 'get_cc_details' || g_code || '-' || g_msg,
            p_notif_status    => 'FAILURE',
            p_notif_date      => SYSDATE
        );
    END get_cc_details;

END CCT_NOTIFICATION_PKG;
