create or replace PACKAGE BODY CCT_APPROVAL_PROCESS
AS
/**Handles CCT_APPROVAL_RULES table data
   Modification History
   PERSON          DATE           COMMENTS
   ------------    -----------    ---------------------------------------------
   Sureka          14-Aug-2017    Created create_rule_version proc
   Sureka          14-Aug-2017    Created create_rule proc
   Sureka          15-Aug-2017    Created get_approval_user proc
   Sureka          25-Aug-2017    Created create_snapshot proc
   Sureka          27-Aug-2017    Modified get_approval_user proc
   Sureka          27-Aug-2017    Created insert_approval_user proc
   Sureka          28-Aug-2017    Created fetch_snapshots proc
   Sureka          31-Aug-2017    Created create_quote_approvals proc
   Sureka          31-Aug-2017    Created insert_approval_snapshots proc
   Sureka          31-Aug-2017    Modified fetch_snapshots proc
   Sureka          20-Sep-2017    Created insert_dummy_snapshot proc
   Sureka          26-Sep-2017    Created update_approval_status proc
   Sureka          28-Sep-2017    Modified create_snapshot proc
   Ankit Bhatt     10-Oct-2017    Modified date format
   Sureka          16-Oct-2017    Modified insert_snapshots proc
   Sureka          16-Oct-2017    Modified insert_approval_snapshots proc
   Sureka          17-Oct-2017    Modified get_approval_user proc
   Sureka          01-Dec-2017    Modified get_approval_user proc
   Sureka          01-Dec-2017    Modified fetch_snapshots proc
   Sureka          12-Jan-2018    Created update_snapshot_master proc
   Sureka          22-Feb-2018    Modified create_snapshot proc for D-50
   Sureka          22-Feb-2018    Modified create_snapshot proc for PerformanceIssues
   Sureka          07-Mar-2018    Modified insert_snapshots proc for approver names inclusion
   Sureka          07-Mar-2018    Modified insert_approval_snapshots proc for approver names inclusion
   Sureka          12-Mar-2018    Modified create_quote_approvals proc for offer_approval_sent_date
   Sureka          12-Mar-2018    Modified update_approval_status proc for offer_approval_recd_date
   Sureka          21-Mar-2018    Modified fetch_snapshots, insert_dummy_snapshot, insert_snapshots 
                                  and insert_approval_snapshots proc for APPROVERS_COMMENTS and SupplyChainVP
   Ramu            31-May-2019    Modified fetch_snapshots proc              
**/
    PROCEDURE create_rule_version(
        P_START_DATE           IN CCT_APPROVAL_RULE_VERSIONS.START_DATE%TYPE,
        P_END_DATE             IN CCT_APPROVAL_RULE_VERSIONS.END_DATE%TYPE,
        P_RETURN_STATUS       OUT NVARCHAR2)
    AS
    BEGIN
        INSERT INTO CCT_APPROVAL_RULE_VERSIONS(
            START_DATE,
            END_DATE)
        VALUES(
            P_START_DATE,
            P_END_DATE);
        COMMIT;
        P_RETURN_STATUS        := 'SUCCESS';
    EXCEPTION
    WHEN OTHERS THEN
        g_code                 := sqlcode;
        g_msg                  := substr(sqlerrm,1,450);
        P_RETURN_STATUS        := 'FAILURE';
        cct_create_errors.record_error(
            P_ERROR_MESSAGE    => g_msg,
            P_ERROR_TYPE       => g_code,
            P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.create_rule_version',
            P_CREATED_BY       => sys_context('USERENV','OS_USER'));
    END create_rule_version;

    PROCEDURE create_rule(
        P_RULE_VERSION         IN CCT_APPROVAL_RULES.RULE_VERSION%TYPE,
        P_RULE_SET_NAME        IN CCT_APPROVAL_RULES.RULE_SET_NAME%TYPE,
        P_GROUP_IDENTIFIER     IN CCT_APPROVAL_RULES.GROUP_IDENTIFIER%TYPE,
        P_RULE_NAME            IN CCT_APPROVAL_RULES.RULE_NAME%TYPE,
        P_TABLE_NAME           IN CCT_APPROVAL_RULES.TABLE_NAME%TYPE DEFAULT NULL,
        P_FIELD_NAME           IN CCT_APPROVAL_RULES.FIELD_NAME%TYPE,
        P_RULE_CONDITION       IN CCT_APPROVAL_RULES.RULE_CONDITION%TYPE,
        P_RULE_VALUE           IN CCT_APPROVAL_RULES.RULE_VALUE%TYPE,
        P_JOIN_CONDITION       IN CCT_APPROVAL_RULES.JOIN_CONDITION%TYPE DEFAULT NULL,
        P_APPROVAL_ROLE        IN CCT_APPROVAL_RULES.APPROVAL_ROLE%TYPE DEFAULT NULL,
        P_USER_NAME            IN CCT_APPROVAL_RULES.USER_NAME%TYPE DEFAULT NULL,
        P_RULE_STATUS          IN CCT_APPROVAL_RULES.RULE_STATUS%TYPE DEFAULT 'ACTIVE',
        P_RETURN_STATUS       OUT NVARCHAR2)
    AS
        l_rule_set_id	          CCT_APPROVAL_RULES.RULE_SET_ID%TYPE;
    BEGIN
        IF P_RULE_SET_NAME = 'LTA' THEN
            l_rule_set_id := 1;
        ELSIF P_RULE_SET_NAME = 'PROGRAM' THEN
            l_rule_set_id := 2;
        ELSE
            l_rule_set_id := 3;
        END IF;
        INSERT INTO CCT_APPROVAL_RULES(
            RULE_VERSION,
            RULE_SET_ID,
            RULE_SET_NAME,
            GROUP_IDENTIFIER,
            RULE_NAME,
            TABLE_NAME,
            FIELD_NAME,
            RULE_CONDITION,
            RULE_VALUE,
            JOIN_CONDITION,
            APPROVAL_ROLE,
            USER_NAME,
            RULE_STATUS,
            CREATION_DATE,
            CREATED_BY)
        VALUES(
            P_RULE_VERSION,
            l_rule_set_id,
            P_RULE_SET_NAME,
            P_GROUP_IDENTIFIER,
            P_RULE_NAME,
            P_TABLE_NAME,
            P_FIELD_NAME,
            P_RULE_CONDITION,
            P_RULE_VALUE,
            P_JOIN_CONDITION,
            P_APPROVAL_ROLE,
            P_USER_NAME,
            P_RULE_STATUS,
            SYSDATE,
            sys_context('USERENV','OS_USER'));
        COMMIT;
        P_RETURN_STATUS        := 'SUCCESS';
    EXCEPTION
    WHEN OTHERS THEN
        g_code                 := sqlcode;
        g_msg                  := substr(sqlerrm,1,450);
        P_RETURN_STATUS        := 'FAILURE';
        cct_create_errors.record_error(
            P_ERROR_MESSAGE    => g_msg,
            P_ERROR_TYPE       => g_code,
            P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.create_rule',
            P_CREATED_BY       => sys_context('USERENV','OS_USER'));
    END create_rule;

    PROCEDURE get_approval_user(
        P_APPROVAL_TYPE        IN CCT_APPROVAL_TYPE,
        P_APPROVAL_USER_CUR   OUT SYS_REFCURSOR,
        P_APPROVAL_ID         OUT CCT_APPROVAL_REQUEST.APPROVAL_ID%TYPE)
    AS
        l_opp_id                  CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE;
        l_deal_structure          CCT_OPPORTUNITY.DEAL_STRUCTURE%TYPE;
        l_rule_name               CCT_APPROVAL_RULES.RULE_NAME%TYPE;
        l_rule_set_name           CCT_APPROVAL_RULES.RULE_SET_NAME%TYPE;
        l_rule_set_id             CCT_APPROVAL_RULES.RULE_SET_ID%TYPE;
        l_group_identifier        CCT_APPROVAL_RULES.GROUP_IDENTIFIER%TYPE;
        l_rule_id                 CCT_APPROVAL_RULES.RULE_ID%TYPE;
        l_table_name              CCT_APPROVAL_RULES.TABLE_NAME%TYPE;
        l_field_name              CCT_APPROVAL_RULES.FIELD_NAME%TYPE;
        l_rule_condn              CCT_APPROVAL_RULES.RULE_CONDITION%TYPE;
        l_rule_value              CCT_APPROVAL_RULES.RULE_VALUE%TYPE;
        l_join_condn              CCT_APPROVAL_RULES.JOIN_CONDITION%TYPE;
        l_approval_rule           CCT_ROLE_PROPERTIES.GROUP_NAME%TYPE;
        l_approval_role_id        CCT_ROLE_PROPERTIES.USER_GROUP_ID%TYPE;
        l_approval_role           CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
        l_user_name               CCT_APPROVAL_RULES.USER_NAME%TYPE;
        l_rule_version            CCT_APPROVAL_RULE_VERSIONS.RULE_VERSION%TYPE;
        l_quote_number            CCT_QUOTE.QUOTE_NUMBER%TYPE;
        l_snap_id                 CCT_SNAPSHOT.SNAPSHOT_ID%TYPE;
        l_where_clause            CCT_APPROVAL_RULES.WHERE_CLAUSE%TYPE;
        l_main_query              VARCHAR2(1000);
        l_rule_query              VARCHAR2(1000);
        l_app_query               VARCHAR2(1000);
        l_sub_query               VARCHAR2(1000);
        l_sub_query1              VARCHAR2(1000);
        l_flag                    NUMBER := 0;
        l_flag1                   NUMBER := 0;
        l_approval_count          NUMBER := 0;
        TYPE curtyp IS REF CURSOR;
        cur_rules                 curtyp;
        cur_app_rules             curtyp;
        cur_ind_rules             curtyp;
        CURSOR cur_approval_users
        IS
            SELECT approval_role_id,
                   approval_role,
                   approval_user_id
              FROM cct_approval_users
             WHERE opportunity_id = (SELECT opportunity_id
                                       FROM cct_opportunity
                                      WHERE opportunity_number = P_APPROVAL_TYPE.OPPORTUNITY_NUMBER) AND
                   snapshot_id = P_APPROVAL_TYPE.SNAPSHOT_ID
            ORDER BY id ASC;
    BEGIN
        EXECUTE IMMEDIATE 'TRUNCATE TABLE CCT_APPROVAL_USERS_TEMP';
        cct_fetch_fields.get_opty_id(P_APPROVAL_TYPE.OPPORTUNITY_NUMBER,l_opp_id);
        --DETERMINE LTA OR PROGRAM
        BEGIN
            SELECT deal_structure
              INTO l_deal_structure
              FROM cct_opportunity
             WHERE opportunity_number = P_APPROVAL_TYPE.OPPORTUNITY_NUMBER;
        EXCEPTION
        WHEN OTHERS THEN
            g_code := sqlcode;
            g_msg  := substr(sqlerrm,1,450);
            cct_create_errors.record_error(
                P_ERROR_MESSAGE   => g_msg,
                P_ERROR_TYPE      => g_code,
                P_MODULE_NAME     => 'CCT_APPROVAL_PROCESS.get_approval_user GET DEAL_STRUCTURE and OPTYID',
                P_CREATED_BY      => sys_context('USERENV','OS_USER'));
        END;

        IF l_deal_structure = 'Products Only' THEN
            l_rule_set_name := 'LTA';
            l_rule_set_id := '1';
        ELSE
            l_rule_set_name := 'PROGRAM';
            l_rule_set_id := '2';
        END IF;

        --PICKUP THE CORRECT VERSION
        BEGIN
            SELECT rule_version
              INTO l_rule_version
              FROM cct_approval_rule_versions
             WHERE TO_DATE(SYSDATE,'DD/MM/YYYY') BETWEEN
                       TO_DATE(start_date,'DD/MM/YYYY') AND
                       TO_DATE(end_date,'DD/MM/YYYY');
        EXCEPTION
        WHEN OTHERS THEN
            g_code := sqlcode;
            g_msg  := substr(sqlerrm,1,450);
            cct_create_errors.record_error(
                P_OPPORTUNITY_ID   => l_opp_id,
                P_ERROR_MESSAGE    => g_msg,
                P_ERROR_TYPE       => g_code,
                P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.get_approval_user GET VERSION',
                P_CREATED_BY       => sys_context('USERENV','OS_USER'));
        END;

        --Log the request
        P_APPROVAL_ID := cct_approval_req_seq.nextval;
        l_snap_id := P_APPROVAL_TYPE.SNAPSHOT_ID;
        INSERT INTO cct_approval_request(
            APPROVAL_ID,
            OPPORTUNITY_ID,
            SOURCE,
            CREATED_BY,
            CREATION_DATE,
            NUM_OF_APPROVALS,
            MESSAGE_ID,
            ACTION,
            COMMENTS,
            SNAPSHOT_ID
        )
        VALUES(
            P_APPROVAL_ID,
            l_opp_id,
            P_APPROVAL_TYPE.SOURCE,
            P_APPROVAL_TYPE.CREATED_BY,
            SYSDATE,
            0,
            P_APPROVAL_TYPE.MESSAGE_ID,
            P_APPROVAL_TYPE.ACTION,
            P_APPROVAL_TYPE.COMMENTS || '- INSERT',
            l_snap_id);
        COMMIT;
        /**
        --Find rule name and loop under it
        l_rule_query := 'SELECT distinct RULE_NAME FROM CCT_APPROVAL_RULES WHERE ';
        l_rule_query := l_rule_query || 'RULE_VERSION=''' || l_rule_version;
        l_rule_query := l_rule_query || ''' AND RULE_SET_NAME=''' || l_rule_set_name;
        l_rule_query := l_rule_query || ''' AND RULE_STATUS=''ACTIVE'' ';
        l_rule_query := l_rule_query || 'ORDER BY RULE_NAME';
        OPEN cur_rules FOR l_rule_query;
        LOOP
            FETCH cur_rules INTO l_rule_name;
            EXIT WHEN ( cur_rules%notfound );
            --Find the group identifier and loop under it l_group_identifier,l_approval_role;
            l_app_query := l_rule_query;
            l_app_query := replace(l_app_query,'distinct RULE_NAME',
                'distinct GROUP_IDENTIFIER,APPROVAL_ROLE,USER_NAME');
            l_app_query := replace(l_app_query,'ORDER BY RULE_NAME',
                'ORDER BY GROUP_IDENTIFIER');
            l_app_query := replace(l_app_query,'''ACTIVE'' ',
                '''ACTIVE'' AND RULE_NAME=''' || l_rule_name || ''' ');
            l_flag1 := 0;
            OPEN cur_app_rules FOR l_app_query;
            LOOP
                FETCH cur_app_rules INTO l_group_identifier,l_approval_role_id,l_user_name;
                EXIT WHEN ( cur_app_rules%notfound OR l_flag1 = 1 );
                l_main_query := l_app_query;
                l_main_query := replace(l_main_query,
                    'distinct GROUP_IDENTIFIER,APPROVAL_ROLE,USER_NAME','*');
                l_main_query := replace(l_main_query,
                    ' ORDER BY GROUP_IDENTIFIER','');
                --loop through table names and identify if they fulfil the rules
                l_sub_query := 'SELECT distinct RULE_ID,TABLE_NAME,FIELD_NAME,RULE_CONDITION,';
                l_sub_query := l_sub_query || 'RULE_VALUE,JOIN_CONDITION,WHERE_CLAUSE FROM (';
                l_sub_query := l_sub_query || l_main_query || ' AND RULE_SET_ID=''';
                l_sub_query := l_sub_query || l_rule_set_id || ''' AND GROUP_IDENTIFIER=''';
                l_sub_query := l_sub_query || l_group_identifier || '''';
                l_sub_query := l_sub_query || ') ORDER BY RULE_ID ASC';
                OPEN cur_ind_rules FOR l_sub_query;
                LOOP
                    FETCH cur_ind_rules INTO l_rule_id,l_table_name,l_field_name,
                                             l_rule_condn,l_rule_value,l_join_condn,l_where_clause;
                    EXIT WHEN (cur_ind_rules%notfound OR (l_flag=0 AND l_join_condn='AND'));
                    l_sub_query1 := 'begin select count(*) into :flags from ' || l_table_name;
                    l_sub_query1 := l_sub_query1 || ' where ' || l_where_clause;
                    l_sub_query1 := l_sub_query1 || ' and ' || l_field_name || l_rule_condn;
                    l_sub_query1 := l_sub_query1 || '''' || l_rule_value || '''; end;';
                    IF instr(l_where_clause,':id') > 0 THEN
                        EXECUTE IMMEDIATE l_sub_query1 USING OUT l_flag,l_opp_id;
                    ELSIF instr(l_where_clause,':snap') > 0 THEN
                        EXECUTE IMMEDIATE l_sub_query1 USING OUT l_flag,l_snap_id;
                    END IF;
                    --dbms_output.put_line('HI' || l_sub_query1);
                END LOOP;
                CLOSE cur_ind_rules;
                --dbms_output.put_line('RULE' || l_rule_id);
                --dbms_output.put_line('JOIN' || l_join_condn);
                l_flag1 := 1;
                --dbms_output.put_line('FLAG 1 - '|| l_flag1);
                --dbms_output.put_line('FLAG 0 - '|| l_flag);

                BEGIN
                    SELECT user_group
                      INTO l_approval_role
                      FROM cct_role_properties
                     WHERE user_group_id = l_approval_role_id;
                EXCEPTION
                WHEN OTHERS THEN
                    g_code := sqlcode;
                    g_msg  := substr(sqlerrm,1,450);
                    cct_create_errors.record_error(
                        P_OPPORTUNITY_ID   => l_opp_id,
                        P_ERROR_MESSAGE    => g_msg,
                        P_ERROR_TYPE       => g_code,
                        P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.get_approval_user',
                        P_CREATED_BY       => sys_context('USERENV','OS_USER'));
                END;

                IF l_join_condn = 'AND' THEN
                    IF l_flag <> 0 AND l_flag1 = 1 THEN
                        IF l_approval_count = 0 THEN
                            insert_approval_user(l_rule_name,l_user_name,l_approval_role);
                            l_approval_count := 1;
                        ELSE
                            insert_approval_user(l_rule_name,l_user_name,l_approval_role);
                            l_approval_count := l_approval_count + 1;
                        END IF;
                        l_flag1 := 1;
                    ELSE
                        l_flag1 := 0;
                    END IF;
                   -- dbms_output.put_line('FINAL' || l_flag1);
                ELSIF l_join_condn = 'OR' THEN
                    IF l_flag <> 0 OR l_flag1 = 1 THEN
                        IF l_approval_count = 0 THEN
                            insert_approval_user(l_rule_name,l_user_name,l_approval_role);
                            l_approval_count := 1;
                        ELSE
                            insert_approval_user(l_rule_name,l_user_name,l_approval_role);
                            l_approval_count := l_approval_count + 1;
                        END IF;
                        l_flag1 := 1;
                    ELSE
                        l_flag1 := 0;
                    END IF;
                ELSIF l_join_condn IS NULL THEN
                    IF l_flag <> 0 THEN
                        IF l_approval_count = 0 THEN
                            insert_approval_user(l_rule_name,l_user_name,l_approval_role);
                            l_approval_count := 1;
                        ELSE
                            insert_approval_user(l_rule_name,l_user_name,l_approval_role);
                            l_approval_count := l_approval_count + 1;
                        END IF;
                        l_flag1 := 1;
                    ELSE
                        l_flag1 := 0;
                    END IF;
                END IF;
            END LOOP;
            CLOSE cur_app_rules;
        END LOOP;
        CLOSE cur_rules;
        **/
        OPEN cur_approval_users;
        LOOP
            FETCH cur_approval_users INTO l_approval_role_id,l_approval_rule,l_user_name;
            EXIT WHEN cur_approval_users%notfound;
            IF l_approval_rule IS NULL THEN
                SELECT group_name,
                       user_group
                  INTO l_approval_rule,
                       l_approval_role
                  FROM cct_role_properties
                 WHERE user_group_id = l_approval_role_id;
            END IF;
            insert_approval_user(l_approval_rule,l_user_name,l_approval_role);
            l_approval_count := l_approval_count + 1;
        END LOOP;
        CLOSE cur_approval_users;
        --update the approval request count for the request
        UPDATE cct_approval_request
           SET num_of_approvals = l_approval_count,
               comments = replace(comments, '- INSERT','')
         WHERE approval_id = P_APPROVAL_ID;
        COMMIT;

        IF UPPER(P_APPROVAL_TYPE.ACTION) = 'CREATE' THEN
            OPEN P_APPROVAL_USER_CUR FOR 'SELECT * FROM CCT_APPROVAL_USERS_TEMP';
        ELSE
            OPEN P_APPROVAL_USER_CUR FOR 'SELECT * FROM dual WHERE 1 = 0';
        END IF;

        IF UPPER(P_APPROVAL_TYPE.SOURCE) = 'OSB' THEN
            create_quote_approvals(
                P_OPP_ID      => l_opp_id,
                P_APPROVAL_ID => P_APPROVAL_ID,
                P_SNAPSHOT_ID => P_APPROVAL_TYPE.SNAPSHOT_ID,
                P_COMMENTS    => P_APPROVAL_TYPE.COMMENTS);
        END IF;

    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            P_OPPORTUNITY_ID   => l_opp_id,
            P_ERROR_MESSAGE    => g_msg,
            P_ERROR_TYPE       => g_code,
            P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.get_approval_user',
            P_CREATED_BY       => sys_context('USERENV','OS_USER'));
    END get_approval_user;

    PROCEDURE create_quote_approvals(
        P_OPP_ID               IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE,
        P_APPROVAL_ID          IN CCT_APPROVAL_REQUEST.APPROVAL_ID%TYPE,
        P_SNAPSHOT_ID          IN CCT_APPROVAL_REQUEST.SNAPSHOT_ID%TYPE,
        P_COMMENTS             IN CCT_APPROVAL_REQUEST.COMMENTS%TYPE)
    AS
        l_revision_id             CCT_SNAPSHOT.QUOTE_REVISION_ID%TYPE;
        l_snapshot_type           CCT_SNAPSHOT.SNAPSHOT_TYPE%TYPE;
        ex_snapshot_type          EXCEPTION;
        l_crf_id                  CCT_CRF.CRF_ID%TYPE;
        l_flag                      NUMBER;
    BEGIN
        BEGIN
            SELECT quote_revision_id,
                   UPPER(snapshot_type)
              INTO l_revision_id,
                   l_snapshot_type
              FROM cct_snapshot
             WHERE snapshot_id = P_SNAPSHOT_ID AND
                   opportunity_id = P_OPP_ID;
        EXCEPTION
        WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            P_OPPORTUNITY_ID   => P_OPP_ID,
            P_ERROR_MESSAGE    => g_msg,
            P_ERROR_TYPE       => g_code,
            P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.create_quote_approvals SNAPSHOT OPTY COMBINATION',
            P_CREATED_BY       => sys_context('USERENV','OS_USER'));
        END;

        IF l_snapshot_type = 'APPROVAL' THEN
            INSERT INTO cct_quote_approvals (
                APPROVAL_ID,
                OPPORTUNITY_ID,
                QUOTE_REVISION_ID,
                APPROVAL_VERSION,
                LDOA_VERSION_ID,
                CREATION_DATE,
                CREATED_BY,
                SNAPSHOT_ID,
                APPROVAL_STATUS,
                PROPOSAL_MANAGER_COMMENTS
            )
            VALUES(
                P_APPROVAL_ID,
                P_OPP_ID,
                l_revision_id,
                '0.1',
                '1.0',
                SYSDATE,
                sys_context('USERENV','OS_USER'),
                P_SNAPSHOT_ID,
                g_approval_status,
                P_COMMENTS);
            COMMIT;

            cct_fetch_fields.get_crf_id(NULL,P_OPP_ID,l_crf_id);

            SELECT COUNT(*) 
              INTO l_flag 
              FROM cct_crf_revision 
             WHERE crf_id = l_crf_id;

            IF l_flag = 0 THEN 
                UPDATE cct_crf_date_management
                   SET offer_approval_sent_date = SYSDATE
                 WHERE crf_id = l_crf_id;
                COMMIT;
            ELSE 
                UPDATE cct_crf_revision 
                   SET offer_approval_sent_date = SYSDATE 
                 WHERE crf_id = l_crf_id AND 
                       crf_revision_id = (SELECT MAX(crf_revision_id) 
                                            FROM cct_crf_revision 
                                           WHERE crf_id = l_crf_id);
                COMMIT;
            END IF;

        ELSE
            RAISE ex_snapshot_type;
        END IF;

    EXCEPTION
    WHEN ex_snapshot_type THEN
        g_code := sqlcode;
        g_msg  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            P_OPPORTUNITY_ID   => p_opp_id,
            P_ERROR_MESSAGE    => g_msg,
            P_ERROR_TYPE       => g_code,
            P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.create_quote_approvals INVALID SNAPSHOT TYPE',
            P_CREATED_BY       => sys_context('USERENV','OS_USER'));
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            P_OPPORTUNITY_ID   => p_opp_id,
            P_ERROR_MESSAGE    => g_msg,
            P_ERROR_TYPE       => g_code,
            P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.create_quote_approvals',
            P_CREATED_BY       => sys_context('USERENV','OS_USER'));
    END create_quote_approvals;

    PROCEDURE insert_approval_user(
        P_APPROVAL_RULE        IN CCT_APPROVAL_RULES.RULE_NAME%TYPE,
        P_APPROVAL_USER        IN CCT_APPROVAL_RULES.USER_NAME%TYPE,
        P_APPROVAL_ROLE        IN CCT_APPROVAL_RULES.APPROVAL_ROLE%TYPE)
    AS
    BEGIN
        INSERT INTO cct_approval_users_temp VALUES (
            P_APPROVAL_RULE,
            P_APPROVAL_USER,
            P_APPROVAL_ROLE
        );
        COMMIT;
    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            P_ERROR_MESSAGE   => g_msg,
            P_ERROR_TYPE      => g_code,
            P_MODULE_NAME     => 'CCT_APPROVAL_PROCESS.insert_approval_user',
            P_CREATED_BY      => sys_context('USERENV','OS_USER'));
    END insert_approval_user;

    PROCEDURE update_approval_status(
        P_APPROVAL_ID          IN CCT_QUOTE_APPROVALS.APPROVAL_ID%TYPE DEFAULT NULL,
        P_SNAPSHOT_ID          IN CCT_QUOTE_APPROVALS.SNAPSHOT_ID%TYPE,
        P_APPROVAL_STATUS      IN CCT_QUOTE_APPROVALS.APPROVAL_STATUS%TYPE)
    AS
        l_old_approval_status     CCT_QUOTE_APPROVALS.APPROVAL_STATUS%TYPE;
        l_new_approval_status     CCT_QUOTE_APPROVALS.APPROVAL_STATUS%TYPE;
        l_opp_id                  CCT_SNAPSHOT.OPPORTUNITY_ID%TYPE;
        l_revision_id             CCT_SNAPSHOT.QUOTE_REVISION_ID%TYPE;
        l_approval_id             CCT_QUOTE_APPROVALS.APPROVAL_ID%TYPE;
        l_approval_type           CCT_APPROVAL_TYPE;
        l_approval_user_cur       SYS_REFCURSOR;
        l_opp_num                 CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE;
        l_crf_id                  CCT_CRF.CRF_ID%TYPE;
        l_flag                    NUMBER;
    BEGIN
         BEGIN
            SELECT  opportunity_id,
                    quote_revision_id
              INTO  l_opp_id,
                    l_revision_id
              FROM  cct_snapshot
             WHERE  snapshot_id = P_SNAPSHOT_ID;
        EXCEPTION
        WHEN OTHERS THEN
            g_code := sqlcode;
            g_msg  := substr(sqlerrm,1,450);
            cct_create_errors.record_error(
                P_ERROR_MESSAGE   => g_msg,
                P_ERROR_TYPE      => g_code,
                P_MODULE_NAME     => 'CCT_APPROVAL_PROCESS.update_approval_status INVALID SNAPSHOT',
                P_CREATED_BY      => sys_context('USERENV','OS_USER'));
        END;

        cct_fetch_fields.get_opty_num(l_opp_id,l_opp_num);

        IF P_APPROVAL_STATUS = 'PASSED BY PROPOSAL MANAGER' OR P_APPROVAL_STATUS = 'PASSED' THEN
            l_approval_type := CCT_APPROVAL_TYPE();
            l_approval_type.OPPORTUNITY_NUMBER := l_opp_num;
            l_approval_type.SOURCE := 'OSB';
            l_approval_type.SNAPSHOT_ID := P_SNAPSHOT_ID;
            l_approval_type.ACTION := 'CREATE';
            get_approval_user(l_approval_type,l_approval_user_cur,l_approval_id);
        ELSE
            cct_fetch_fields.get_latest_approval(P_SNAPSHOT_ID,l_approval_id);
        END IF;

        BEGIN
            SELECT NVL(approval_status,'NULL')
              INTO l_old_approval_status
              FROM cct_quote_approvals
             WHERE approval_id = l_approval_id AND
                   snapshot_id = P_SNAPSHOT_ID;
        EXCEPTION
        WHEN OTHERS THEN
            g_code := sqlcode;
            g_msg  := substr(sqlerrm,1,450);
            cct_create_errors.record_error(
                P_ERROR_MESSAGE     => g_msg,
                P_ERROR_TYPE        => g_code,
                P_QUOTE_REVISION_ID => l_revision_id,
                P_OPPORTUNITY_ID    => l_opp_id,
                P_TASK_ID           => l_approval_id,
                P_MODULE_NAME       => 'CCT_APPROVAL_PROCESS.update_approval_status Fetch Part',
                P_CREATED_BY        => sys_context('USERENV','OS_USER'));
        END;

        IF UPPER(P_APPROVAL_STATUS) = 'PASSED' THEN
            l_new_approval_status := 'PASSED BY PROPOSAL MANAGER';
        ELSE
            l_new_approval_status := UPPER(P_APPROVAL_STATUS);
        END IF;

        IF UPPER(l_old_approval_status) <> 'REJECTED' OR
           UPPER(l_old_approval_status) <> 'CANCELLED' OR
           UPPER(l_old_approval_status) <> 'PASSED BY PROPOSAL MANAGER' THEN
            UPDATE cct_quote_approvals
               SET approval_status = l_new_approval_status
             WHERE approval_id = l_approval_id AND
                   snapshot_id = P_SNAPSHOT_ID;
            COMMIT;
        END IF;

        IF P_APPROVAL_STATUS = 'PASSED BY PROPOSAL MANAGER' OR
           P_APPROVAL_STATUS = 'PASSED' OR
           P_APPROVAL_STATUS = 'APPROVED' THEN

            cct_fetch_fields.get_crf_id(l_opp_num,l_opp_id,l_crf_id);

            SELECT COUNT(*) 
              INTO l_flag 
              FROM cct_crf_revision 
             WHERE crf_id = l_crf_id;

            IF l_flag = 0 THEN 
               UPDATE cct_crf_date_management
                  SET offer_approval_recd_date = SYSDATE
                WHERE crf_id = l_crf_id;
               COMMIT;
            ELSE 
                UPDATE cct_crf_revision 
                   SET offer_approval_recd_date = SYSDATE 
                 WHERE crf_id = l_crf_id AND 
                       crf_revision_id = (SELECT MAX(crf_revision_id) 
                                            FROM cct_crf_revision 
                                           WHERE crf_id = l_crf_id);
                COMMIT;
            END IF; 
        END IF;

    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            P_ERROR_MESSAGE     => g_msg,
            P_ERROR_TYPE        => g_code,
            P_QUOTE_REVISION_ID => l_revision_id,
            P_OPPORTUNITY_ID    => l_opp_id,
            P_TASK_ID           => l_approval_id,
            P_MODULE_NAME       => 'CCT_APPROVAL_PROCESS.update_approval_status',
            P_CREATED_BY        => sys_context('USERENV','OS_USER'));
    END update_approval_status;

    PROCEDURE create_snapshot(
        P_OPP_NUM              IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE,
        P_SNAPSHOT_TYPE        IN CCT_SNAPSHOT.SNAPSHOT_TYPE%TYPE)
    AS
        l_opp_id                  CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE;
        l_quote_revision_id       CCT_QUOTE_PARTS.QUOTE_REVISION_ID%TYPE;
        l_snapshot_id             CCT_SNAPSHOT.SNAPSHOT_ID%TYPE;
        l_snapshot_version        CCT_SNAPSHOT.SNAPSHOT_VERSION%TYPE;
        l_snapshot_ver_old        NUMBER;
        l_count                   NUMBER;
        l_program_sql             CLOB;
        l_program_temp            VARCHAR2(50);
        l_program_type            CCT_OPPORTUNITY_SNAPSHOT.PROGRAM_TYPE%TYPE;
        TYPE curtype IS REF CURSOR;
        cur_program               curtype;
    BEGIN
        BEGIN
            SELECT co.opportunity_id,
                   MAX(cqr.revision_id)
              INTO l_opp_id,
                   l_quote_revision_id
              FROM cct_quote cq,
                   cct_opportunity co,
                   cct_quote_revisions cqr
             WHERE co.opportunity_id = cq.opportunity_id AND
                   cq.quote_id = cqr.quote_id AND
                   co.opportunity_number = P_OPP_NUM
            GROUP BY co.opportunity_id;
        EXCEPTION
        WHEN OTHERS THEN
            g_code := sqlcode;
            g_msg  := substr(sqlerrm,1,450);
            cct_create_errors.record_error(
                P_ERROR_MESSAGE   => g_msg,
                P_ERROR_TYPE      => g_code,
                P_MODULE_NAME     => 'CCT_APPROVAL_PROCESS.create_snapshot - Fetch opty n Revn',
                P_CREATED_BY      => sys_context('USERENV','OS_USER'));
        END;

        BEGIN
            SELECT CASE WHEN COUNT(1) > 0 THEN
                        MAX(TO_NUMBER(snapshot_version))|| '.0'
                    ELSE '0.0'
                    END
              INTO l_snapshot_version
              FROM cct_snapshot
             WHERE opportunity_id = l_opp_id;
        EXCEPTION
        WHEN OTHERS THEN
            g_code := sqlcode;
            g_msg  := substr(sqlerrm,1,450);
            cct_create_errors.record_error(
                P_OPPORTUNITY_ID   => l_opp_id,
                P_ERROR_MESSAGE    => g_msg,
                P_ERROR_TYPE       => g_code,
                P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.create_snapshot - Fetch snapshot info',
                P_CREATED_BY       => sys_context('USERENV','OS_USER'));
        END;

        l_snapshot_id := cct_snapshot_seq.nextval;
        l_snapshot_ver_old := TO_NUMBER(l_snapshot_version);
        l_snapshot_ver_old := l_snapshot_ver_old + 1;
        l_snapshot_version := l_snapshot_ver_old || '.0';

        IF UPPER(P_SNAPSHOT_TYPE) = 'APPROVAL' THEN
            BEGIN
                SELECT count(*)
                  INTO l_count
                  FROM cct_snapshot_approval
                 WHERE opportunity_id = l_opp_id AND
                       snapshot_version IS NOT NULL;
            EXCEPTION
            WHEN OTHERS THEN
               l_count := 0;
            END;
            --log
            cct_create_errors.record_error(
                P_OPPORTUNITY_ID   => l_opp_id,
                P_ERROR_MESSAGE    => 'SUCCESS_COUNT',
                P_ERROR_TYPE       => 10,
                P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.create_snapshot',
                P_CREATED_BY       => sys_context('USERENV','OS_USER'));
            IF l_count > 0 THEN
                update_snapshot_master(l_opp_id);
            END IF;

        END IF;

        INSERT INTO cct_snapshot (
            SNAPSHOT_ID,
            SNAPSHOT_VERSION,
            OPPORTUNITY_ID,
            QUOTE_REVISION_ID,
            CREATED_BY,
            CREATED_DATE,
            SNAPSHOT_TYPE,
            INCLUDE_PMT_TERM
        )
        VALUES(
            l_snapshot_id,
            l_snapshot_version,
            l_opp_id,
            l_quote_revision_id,
            sys_context('USERENV','OS_USER'),
              /*TO_DATE(
              SYSDATE,
                'DD-MON-RRRR HH24:MI:SS'
            ),*/
            SYSDATE,
            P_SNAPSHOT_TYPE,
            'NO');
        COMMIT;

        IF UPPER(P_SNAPSHOT_TYPE) = 'APPROVAL' THEN
            INSERT INTO cct_quote_parts_snapshot_final
                SELECT  l_snapshot_id,
                        cqp.quote_revision_id,
                        cqp.queue_id,
                        cqp.whse_id,
                        cqp.product_id,
                        cqp.quoted_part_number,
                        cqp.customer_pn,
                        cqp.annual_qty,
                        cqp.uom,
                        cqp.approved_source,
                        cqp.disapproved_source,
                        cqp.misc_cust_data,
                        cqp.target_price,
                        cqp.target_margin,
                        cqp.certified_part,
                        cqp.contract_price,
                        cqp.last_price_paid,
                        cqp.highest_contract_price,
                        cqp.lowest_contract_price,
                        cqp.high_tape_price,
                        cqp.middle_tape_price,
                        cqp.low_tape_price,
                        cqp.total_qty_len_contract,
                        cqp.average_cost,
                        cqp.average_cost_lbs,
                        cqp.last_cost,
                        cqp.actual_cost,
                        cqp.vendor_cost,
                        cqp.market_cost,
                        cqp.lta_cost,
                        cqp.poq_cost,
                        cqp.poq_quantity,
                        cqp.delivery_time_poq,
                        cqp.cd_from_jda,
                        cqp.kapco_part_cost,
                        cqp.symph_drop_shipment,
                        cqp.hpp_part_price,
                        cqp.hpp_part,
                        cqp.prime,
                        cqp.dup_prime,
                        cqp.dup_cust_part,
                        cqp.vendor_back_orders,
                        cqp.cust_back_orders,
                        cqp.lta_moq,
                        cqp.lta_exp_date,
                        cqp.lta_mfr,
                        cqp.lta_leadtime_weeks,
                        cqp.vendor_leadtime_weeks,
                        cqp.prod_type_abc,
                        cqp.kapco_leadtime_weeks,
                        cqp.first_plan_purday,
                        cqp.jda_forecast_12m,
                        cqp.control_number,
                        cqp.manf_number,
                        cqp.manf_name,
                        cqp.mfr_cage_code,
                        cqp.shelf_life,
                        cqp.pma,
                        cqp.tso,
                        cqp.certs,
                        cqp.c_and_p,
                        cqp.rev,
                        cqp.part_desc,
                        cqp.tariff,
                        cqp.itar,
                        cqp.eccn,
                        cqp.dfar,
                        cqp.ppp,
                        cqp.stocking_unit_measure,
                        cqp.comments,
                        cqp.product_notes,
                        cqp.restriction_codes,
                        cqp.prod_class,
                        cqp.total_onhand_whses,
                        cqp.other_cust_price,
                        cqp.contract_part_number,
                        cqp.alt_part_number,
                        cqp.total_alt_qty_onhand,
                        cqp.total_vendor_back_orders,
                        cqp.creation_date,
                        cqp.created_by,
                        cqp.last_updated_date,
                        cqp.last_updated_by,
                        cqp.whse_quantity,
                        cqp.line_identifier,
                        cqp.quote_part_id,
                        cqp.resale,
                        cqp.cost_used_for_quote,
                        cqp.calc_abc,
                        cqp.alternatives_avl,
                        cqp.final_cost_source,
                        cqp.extended_cost,
                        cqp.extended_resale,
                        cqp.final_mkt_class,
                        cqp.item_tag,
                        cqp.last_price_offered,
                        cqp.lta_review_item,
                        cqp.potential_uom_issue,
                        cqp.replacement_cost_used,
                        cqp.speciality_products,
                        cqp.pipeline_qty,
                        cqp.dyp_price_break_qty,
                        cqp.ecommerce_price,
                        cqp.price_type,
                        cqp.margin_validation,
                        cqp.customer_final_leadtime,
                        cqp.internal_notes,
                        cqp.external_notes,
                        cqp.lta_cost_cust,
                        cqp.lta_full_qty_used,
                        cqp.sales_restriction,
                        cqp.status,
                        cqp.comment_category,
                        cqp.quoted_qty,
                        cqp.qty_break1,
                        cqp.qty_break2,
                        cqp.moq,
                        cqp.amortiz_infl_pct,
                        cqp.global_quantities,
                        cqp.manufacturer_certs,
                        cqp.test_reports,
                        cqp.fair_fee,
                        cqp.tool_fee,
                        cqp.misc_fee,
                        cqp.dfar_fee,
                        cqp.vendor_comment,
                        cqp.quote_number_from_supplier,
                        cqp.spreadsheet,
                        cqp.price_confidence,
                        cqp.negotiated,
                        cqp.customer_container_size_data,
                        cqp.customer_uom,
                        cqp.cstmr_um_conv_fctr_cardex_unit,
                        cqp.comments_um_conversion,
                        cqp.crdx_unit_size_info_iden_to_s4,
                        cqp.extended_annual_cost,
                        cqp.itl_internal_pn,
                        cqp.vendor_location_country,
                        cqp.alternate_source,
                        cqp.selling_increment,
                        cqp.moq_cardex_unit,
                        cqp.inco_term,
                        cqp.stock_location,
                        cqp.shelf_life_months_general,
                        cqp.hazmat_jn,
                        cqp.storage_group,
                        cqp.tot_sls_val_usd12mths_crdx_uni,
                        cqp.pct_discount,
                        cqp.package_qty,
                        cqp.tech_support_comments,
                        cqp.tech_support_alternate,
                        cqp.tech_support_source,
                        cqp.suggested_cust_final_leadtime,
                        cqp.suggested_quoted_part_number,
                        cqp.suggested_annual_qty,
                        cqp.suggested_uom,
                        cqp.suggested_prime,
                        cqp.suggested_calc_abc,
                        cqp.suggested_cost_used_for_quote,
                        cqp.suggested_manf_name,
                        cqp.suggested_mfr_cage_code,
                        cqp.SUGGESTED_RESALE 
                  FROM  cct_quote_parts_final cqp
                 WHERE  quote_revision_id = l_quote_revision_id;
            COMMIT;
            /** commented for D-50
            l_program_sql := 'SELECT  fieldname
                                FROM (SELECT cmi,vmi,bailment,consignment,edi,
                                             min_max,kitting,pl_3,pl_4,is_fsl_req,
                                             vending_machines,service_fees,
                                             is_site_visit_req
                                        FROM cct_crf_klx_offer
                                       WHERE crf_id = (SELECT crf_id
                                                         FROM cct_crf
                                                        WHERE CRF_NO = ''' || P_OPP_NUM ||'''))
                            UNPIVOT INCLUDE NULLS
                            (fieldvalue FOR fieldname IN (cmi,vmi,bailment,
                                                          consignment,edi,
                                                          min_max,kitting,pl_3,
                                                          pl_4,is_fsl_req as ''FSL'',
                                                          vending_machines,
                                                          service_fees,
                                                          is_site_visit_req as ''SITE_VISIT''))
                        WHERE fieldvalue=''Y''';

            OPEN cur_program FOR l_program_sql;
            LOOP
                FETCH cur_program INTO l_program_temp;
                EXIT WHEN (cur_program%notfound);
                IF l_program_type is NULL THEN
                    l_program_type := l_program_temp;
                ELSE
                    l_program_type := l_program_type || ', ' || l_program_temp;
                END IF;
            END LOOP;
            CLOSE cur_program;**/
            BEGIN
                SELECT ccko.summary 
                  INTO l_program_type 
                  FROM cct_crf_klx_offer ccko
                 WHERE ccko.crf_id = (SELECT crf_id
                                        FROM cct_crf
                                       WHERE CRF_NO = P_OPP_NUM);
            EXCEPTION
            WHEN OTHERS THEN
               cct_create_errors.record_error(
                P_OPPORTUNITY_ID   => l_opp_id,
                P_ERROR_MESSAGE    => g_msg,
                P_ERROR_TYPE       => g_code,
                P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.create_snapshot',
                P_CREATED_BY       => sys_context('USERENV','OS_USER'));
            END;           

            INSERT INTO cct_opportunity_snapshot(
                    SNAPSHOT_ID,
                    OPPORTUNITY_ID,
                    OPPORTUNITY_NUMBER,
                    PROJECT_NUMBER,
                    CUSTOMER_DUE_DATE,
                    CUSTOMER_NUMBER,
                    CUSTOMER_NAME,
                    CURRENCY,
                    CONTRACT_TERM,
                    DFAR_APPLY,
                    REBATE,
                    PAYMENT_TERMS,
                    INCOTERMS,
                    LOL_PROTECTION,
                    TITLE_TRANSFER,
                    RIGHTS_OF_RETURN,
                    INCENTIVES,
                    CANCEL_PRIVILEGES,
                    BILL_HOLD,
                    BUY_BACK,
                    GT25,
                    LIQ_DAMAGES,
                    ORDER_MINIMUM,
                    STOCKING_STRATEGY_CRF,
                    SERVICE_FEES_CRF,
                    LINE_MIN_CRF,
                    CONTRACT_TYPE,
                    PROJECT_TYPE,
                    CUSTOMER_DOC,
                    CUST_LIAB_PROTECTION,
                    PROGRAM_TYPE)
                SELECT  l_snapshot_id,
                        l_opp_id,
                        cc.crf_no,
                        cc.project_no,
                        cc.due_date,
                        cko.customer_no,
                        cko.customer_name,
                        cko.currency_code,
                        cko.contract_length,
                        cko.is_dfar,
                        cko.rebate,
                        cko.payment_terms,
                        cko.inco_terms,
                        cko.lol_protection,
                        cko.unusual_title_transfer,
                        cko.rights_of_return,
                        cko.incentives_credit,
                        cko.cancellation_rights,
                        cko.bill_hold,
                        cko.is_buy_back,
                        cko.is_gt25,
                        cko.liq_damages,
                        cko.order_min,
                        cko.stocking_strategy,
                        cko.service_fees,
                        cko.line_min,
                        cko.contract_type,
                        cko.project_type,
                        cko.legal_format_type,
                        cko.liability_on_invst_b_items,
                        l_program_type
                  FROM  cct_crf_klx_offer cko,
                        cct_crf cc
                 WHERE  cko.crf_id = cc.crf_id AND
                        cc.crf_no = P_OPP_NUM;
            COMMIT;
            INSERT INTO cct_quote_formula_ss_final 
                SELECT  cqv.snapshot_id,
                        cqv.quote_revision_id,
                        cqv.total_product_cost_loc,
                        cqv.total_product_resale_loc,
                        cqv.product_gross_margin,
                        cqv.project_net_margin,
                        cqv.total_parts_requested,
                        cqv.total_parts_quoted,
                        cqv.total_adjustments,
                        cqv.product_class_a_item_count,
                        cqv.product_class_a_cost,
                        cqv.product_class_a_resale,
                        cqv.product_class_a_margin,
                        cqv.product_class_a1a_item_count,
                        cqv.product_class_a1a_cost,
                        cqv.product_class_a1a_resale,
                        cqv.product_class_a1a_margin,
                        cqv.product_class_a1b_item_count,
                        cqv.product_class_a1b_cost,
                        cqv.product_class_a1b_resale,
                        cqv.product_class_a1b_margin,
                        cqv.product_class_b_item_count,
                        cqv.product_class_b_cost,
                        cqv.product_class_b_resale,
                        cqv.product_class_b_margin,
                        cqv.product_class_c_item_count,
                        cqv.product_class_c_cost,
                        cqv.product_class_c_resale,
                        cqv.product_class_c_margin,
                        cqv.product_class_d_item_count,
                        cqv.product_class_d_cost,
                        cqv.product_class_d_resale,
                        cqv.product_class_d_margin 
                  FROM  cct_quote_formula_ss_final_v cqv
                 WHERE  quote_revision_id = l_quote_revision_id AND 
                        snapshot_id = l_snapshot_id;
            COMMIT;
        ELSE
            INSERT INTO cct_quote_parts_snapshot
                SELECT  l_snapshot_id,
                        cqp.quote_revision_id,
                        cqp.queue_id,
                        cqp.whse_id,
                        cqp.product_id,
                        cqp.quoted_part_number,
                        cqp.customer_pn,
                        cqp.annual_qty,
                        cqp.uom,
                        cqp.approved_source,
                        cqp.disapproved_source,
                        cqp.misc_cust_data,
                        cqp.target_price,
                        cqp.target_margin,
                        cqp.certified_part,
                        cqp.contract_price,
                        cqp.last_price_paid,
                        cqp.highest_contract_price,
                        cqp.lowest_contract_price,
                        cqp.high_tape_price,
                        cqp.middle_tape_price,
                        cqp.low_tape_price,
                        cqp.total_qty_len_contract,
                        cqp.average_cost,
                        cqp.average_cost_lbs,
                        cqp.last_cost,
                        cqp.actual_cost,
                        cqp.vendor_cost,
                        cqp.market_cost,
                        cqp.lta_cost,
                        cqp.poq_cost,
                        cqp.poq_quantity,
                        cqp.delivery_time_poq,
                        cqp.cd_from_jda,
                        cqp.kapco_part_cost,
                        cqp.symph_drop_shipment,
                        cqp.hpp_part_price,
                        cqp.hpp_part,
                        cqp.prime,
                        cqp.dup_prime,
                        cqp.dup_cust_part,
                        cqp.vendor_back_orders,
                        cqp.cust_back_orders,
                        cqp.lta_moq,
                        cqp.lta_exp_date,
                        cqp.lta_mfr,
                        cqp.lta_leadtime_weeks,
                        cqp.vendor_leadtime_weeks,
                        cqp.prod_type_abc,
                        cqp.kapco_leadtime_weeks,
                        cqp.first_plan_purday,
                        cqp.jda_forecast_12m,
                        cqp.control_number,
                        cqp.manf_number,
                        cqp.manf_name,
                        cqp.mfr_cage_code,
                        cqp.shelf_life,
                        cqp.pma,
                        cqp.tso,
                        cqp.certs,
                        cqp.c_and_p,
                        cqp.rev,
                        cqp.part_desc,
                        cqp.tariff,
                        cqp.itar,
                        cqp.eccn,
                        cqp.dfar,
                        cqp.ppp,
                        cqp.stocking_unit_measure,
                        cqp.comments,
                        cqp.product_notes,
                        cqp.restriction_codes,
                        cqp.prod_class,
                        cqp.total_onhand_whses,
                        cqp.other_cust_price,
                        cqp.contract_part_number,
                        cqp.alt_part_number,
                        cqp.total_alt_qty_onhand,
                        cqp.total_vendor_back_orders,
                        cqp.creation_date,
                        cqp.created_by,
                        cqp.last_updated_date,
                        cqp.last_updated_by,
                        cqp.whse_quantity,
                        cqp.line_identifier,
                        cqp.quote_part_id,
                        cqp.resale,
                        cqp.cost_used_for_quote,
                        cqp.calc_abc,
                        cqp.alternatives_avl,
                        cqp.final_cost_source,
                        cqp.extended_cost,
                        cqp.extended_resale,
                        cqp.final_mkt_class,
                        cqp.item_tag,
                        cqp.last_price_offered,
                        cqp.lta_review_item,
                        cqp.potential_uom_issue,
                        cqp.replacement_cost_used,
                        cqp.speciality_products,
                        cqp.pipeline_qty,
                        cqp.dyp_price_break_qty,
                        cqp.ecommerce_price,
                        cqp.price_type,
                        cqp.margin_validation,
                        cqp.suggested_cust_final_leadtime,
                        cqp.suggested_quoted_part_number,
                        cqp.suggested_annual_qty,
                        cqp.suggested_uom,
                        cqp.suggested_prime,
                        cqp.suggested_calc_abc,
                        cqp.suggested_cost_used_for_quote,
                        cqp.suggested_manf_name,
                        cqp.suggested_mfr_cage_code,
                        cqp.suggested_resale,
                        cqp.queue_assignment_date,
                        cqp.suggested_total_qty_len_con,
                        cqp.price_confidence,
                        cqp.tech_support_source,
                        cqp.tech_support_alternate,
                        cqp.tech_support_comments,
                        cqp.package_qty,
                        cqp.pct_discount,
                        cqp.tot_sls_val_usd12mths_crdx_uni,
                        cqp.storage_group,
                        cqp.hazmat_jn,
                        cqp.shelf_life_months_general,
                        cqp.stock_location,
                        cqp.inco_term,
                        cqp.moq_cardex_unit,
                        cqp.selling_increment,
                        cqp.alternate_source,
                        cqp.vendor_location_country,
                        cqp.itl_internal_pn,
                        cqp.extended_annual_cost,
                        cqp.crdx_unit_size_info_iden_to_s4,
                        cqp.comments_um_conversion,
                        cqp.cstmr_um_conv_fctr_cardex_unit,
                        cqp.customer_uom,
                        cqp.customer_container_size_data,
                        cqp.negotiated,
                        cqp.spreadsheet,
                        cqp.quote_number_from_supplier,
                        cqp.vendor_comment,
                        cqp.dfar_fee,
                        cqp.misc_fee,
                        cqp.tool_fee,
                        cqp.fair_fee,
                        cqp.test_reports,
                        cqp.manufacturer_certs,
                        cqp.global_quantities,
                        cqp.amortiz_infl_pct,
                        cqp.moq,
                        cqp.qty_break2,
                        cqp.qty_break1,
                        cqp.quoted_qty,
                        cqp.comment_category,
                        cqp.status,
                        cqp.sales_restriction,
                        cqp.lta_full_qty_used,
                        cqp.lta_cost_cust,
                        cqp.external_notes,
                        cqp.internal_notes,
                        cqp.customer_final_leadtime 
                  FROM  cct_quote_parts cqp
                 WHERE  quote_revision_id = l_quote_revision_id;
            COMMIT;

            INSERT INTO cct_opportunity_snapshot
                SELECT  l_snapshot_id,
                        co.opportunity_id,
                        co.opportunity_number,
                        co.opportunity_name,
                        co.customer_number,
                        co.customer_name,
                        co.currency,
                        co.sales_method,
                        co.status,
                        co.customer_due_date,
                        co.contract_term,
                        co.decimal_to_quote,
                        co.faa_certification,
                        co.dfar_apply,
                        co.customer_wh,
                        co.alt_pricing_cust_num,
                        co.cust_appr_sources,
                        co.project_number,
                        co.special_instruction,
                        co.quote_basis,
                        co.deal_type,
                        co.deal_structure,
                        co.source_products,
                        co.rebate,
                        co.payment_terms,
                        co.incoterms,
                        co.agent_commissions,
                        co.line_min_provision,
                        co.lol_protection,
                        co.cust_liab_protection,
                        co.stocking_strategy,
                        co.penalties,
                        co.title_transfer,
                        co.rights_of_return,
                        co.incentives,
                        co.cancel_privileges,
                        co.bill_hold,
                        co.buy_back,
                        co.gt25,
                        co.total_prod_cost,
                        co.total_prod_resale,
                        co.prod_grs_margin,
                        co.proj_net_margin,
                        co.bitem_liability,
                        co.customer_doc,
                        co.total_prod_requested,
                        co.total_prod_quoted,
                        co.total_adjustments,
                        co.total_program_costs,
                        co.shipping_fees,
                        co.shipping_taxes,
                        co.service_fees,
                        co.liq_damages,
                        co.target_percentage,
                        co.target_date,
                        co.contract_team,
                        co.user_name,
                        co.oname,
                        co.emailid,
                        co.user_group,
                        co.ROLE,
                        co.MANAGER,
                        co.sales_rep,
                        co.proposal_manager,
                        co.pricing_manager,
                        co.cust_liab_on_invest,
                        co.ret_fmt_proposal,
                        co.amount,
                        co.program_type,
                        co.expected_revenue,
                        co.primary_contact,
                        co.primary_competitor,
                        co.fwd_stock_location,
                        co.line_minimum,
                        co.ult_destination_of_parts,
                        co.DESCRIPTION,
                        co.ret_fmt_pricing_file,
                        co.can_use_min_ord_qty,
                        co.sales_stage,
                        co.recover_shipping_cost,
                        co.tso_pma_certification,
                        co.win_loss_reason,
                        co.platform_name,
                        co.cust_ref_number,
                        co.win_probability,
                        co.order_minimum,
                        co.team_number,
                        co.existing_contract_num,
                        co.fwd_stock_loc_req,
                        co.close_date,
                        co.recover_prog_cost,
                        co.creation_date,
                        co.created_by,
                        co.last_updated_date,
                        co.last_updated_by,
                        co.other_payment_terms,
                        co.other_incoterms,
                        co.osc_opportunity_id,
                        co.other_cust_approved_sources,
                        co.terms_note,
                        co.project_num_blank,
                        co.sales_project_num_blank,
                        co.award_file_upload,
                        co.doc_rdy_to_submit,
                        co.sales_owner_email,
                        co.is_cust_rebate_requested,
                        co.requested_stocking_strategy,
                        co.date_submitted_to_sales,
                        co.customer_rebate,
                        co.customer_no_crf,
                        co.stocking_strategy_crf,
                        co.service_fees_crf,
                        co.line_min_crf,
                        co.contract_type,
                        co.project_type 
                  FROM  cct_opportunity co
                 WHERE  opportunity_id = l_opp_id;
            COMMIT;

        END IF;

    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            P_OPPORTUNITY_ID   => l_opp_id,
            P_ERROR_MESSAGE    => g_msg,
            P_ERROR_TYPE       => g_code,
            P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.create_snapshot',
            P_CREATED_BY       => sys_context('USERENV','OS_USER'));
    END create_snapshot;

    PROCEDURE fetch_snapshots(
        P_OPP_ID               IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE,
        P_ROLE_TYPE            IN NVARCHAR2 DEFAULT NULL,
        P_SNAPSHOT_CUR        OUT SYS_REFCURSOR)
    AS
        l_select_part             VARCHAR2(1000);
        l_from_part               CLOB;
        l_where_part              VARCHAR2(1000);
        l_order_by                VARCHAR2(100);
        l_sql_query               CLOB;
        l_inner_query             CLOB;
        l_count                   NUMBER := 0;
        l_alias                   VARCHAR2(5);
        l_snapshot_ver            CCT_SNAPSHOT.SNAPSHOT_VERSION%TYPE;
		l_role_type_id 			  CCT_ROLE_PROPERTIES.USER_GROUP_ID%TYPE;
        l_role_type 			  CCT_ROLE_PROPERTIES.USER_GROUP%TYPE;
        CURSOR cur_snapshot_list IS
            SELECT DISTINCT snapshot_version
              FROM CCT_SNAPSHOT_APPROVAL
             WHERE OPPORTUNITY_ID = P_OPP_ID AND
                   snapshot_version IS NOT NULL
            ORDER BY TO_NUMBER(snapshot_version) DESC;
    BEGIN

		cct_properties_pkg.get_soap_param('ALL_SNAPSHOT_ROLE',l_role_type_id);

        BEGIN
            SELECT user_group
              INTO l_role_type
              FROM cct_role_properties
             WHERE user_group_id = l_role_type_id;
        EXCEPTION
        WHEN OTHERS THEN
            g_code := sqlcode;
            g_msg  := substr(sqlerrm,1,450);
            cct_create_errors.record_error(
                P_OPPORTUNITY_ID   => P_OPP_ID,
                P_ERROR_MESSAGE    => g_msg,
                P_ERROR_TYPE       => g_code,
                P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.fetch_snapshots',
                P_CREATED_BY       => sys_context('USERENV','OS_USER'));
        END;
        --log
        cct_create_errors.record_error(
            P_OPPORTUNITY_ID   => P_OPP_ID,
            P_ERROR_MESSAGE    => 'SUCCESS_PRELOAD',
            P_ERROR_TYPE       => 10,
            P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.fetch_snapshots',
            P_CREATED_BY       => sys_context('USERENV','OS_USER'));
        IF UPPER(P_ROLE_TYPE) = UPPER(l_role_type) THEN
            insert_snapshots(P_OPP_ID);
        ELSE
            insert_approval_snapshots(P_OPP_ID);
        END IF;
        --log
        cct_create_errors.record_error(
            P_OPPORTUNITY_ID   => P_OPP_ID,
            P_ERROR_MESSAGE    => 'SUCCESS_POSTLOAD',
            P_ERROR_TYPE       => 20,
            P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.fetch_snapshots',
            P_CREATED_BY       => sys_context('USERENV','OS_USER'));

        l_select_part := 'SELECT $FIELDS$ from';
        l_where_part := ' WHERE 1=1 $COND$';
        l_order_by := ' order by rownum desc';
        l_inner_query := ' (select  fieldvalue,fieldname from (
            select * from CCT_SNAPSHOT_APPROVAL where OPPORTUNITY_ID =' || P_OPP_ID || ' AND SNAPSHOT_VERSION = ''$SNAP$'' )
        UNPIVOT INCLUDE NULLS
        (fieldvalue FOR fieldname IN (
            SNAPSHOT_DATE as ''Snapshot Date'',
            CRF_NUMBER as ''CRF Number'',
            SALES_TEAM as ''Sales Team #'',
            CONTRACT_REF as ''Contract Ref #'',
            CUSTOMER_DUE_DATE as ''Due Date to Customer'',
            CUSTOMER_NAME as ''Customer Name'',
            GT25 as ''GT25'',
            CONTRACT_TYPE as ''Contract Type'',
            PROJECT_TYPE as ''Project Type'',
            CONTRACT_TERM as ''Length of Contract - years'',
            PROGRAM_TYPE as ''Program Type'',
            TOTAL_PRODUCT_COST_LOC as ''Total Product Cost'',
            TOTAL_PRODUCT_RESALE_LOC as ''Total Product Resale'',
            PRODUCT_GROSS_MARGIN as ''Product Gross Margin'',
            TOTAL_ADJUSTMENTS as ''Total Adjustments'',
            PROJECT_NET_MARGIN as ''Project Net Margin'',
            REBATE as ''Rebate - LOC'',
            TOTAL_PROGRAM_COSTS as ''Total Program Costs'',
            SHIPPING_FEE_TAXES as ''Shipping Fees/Taxes'',
            SERVICE_FEES as ''Service Fees'',
            PAYMENT_TERMS as ''Payment Terms'',
            INCOTERMS as ''Incoterms'',
            CUSTOMER_DOC as ''Boeing Distribution or Customer Document'',
            AGENT_COMMISSIONS as ''Agent Commissions'',
            LINE_MIN as ''Line min'',
            ORDER_MIN as ''Order min'',
            LOL_PROTECTION as ''Effective LOL Protection'',
            CUST_LIAB_PROTECTION as ''Customer Liability On Investment'',
            STOCKING_STRATEGY as ''Stocking Strategy'',
            PENALTIES as ''Penalties/Liquidated Damages'',
            TITLE_TRANSFER as ''Unusual Title Transfer'',
            RIGHTS_OF_RETURN as ''Rights of Return'',
            INCENTIVES as ''Incentives/Other Credits'',
            CANCEL_PRIVILEGES as ''Cancellations Privileges'',
            BILL_HOLD as ''Bill and Hold'',
            BUY_BACK as ''Buy Back'',
            CURRENCY as ''Currency'',
            TOTAL_PARTS_REQUESTED as ''Total Parts Requested'',
            TOTAL_PARTS_QUOTED as ''Total Parts Quoted'',
            PRODUCT_CLASS_A_ITEM_COUNT as ''Product Class A Item Count'',
            PRODUCT_CLASS_A_COST as ''Product Class A Cost - LOC'',
            PRODUCT_CLASS_A_RESALE as ''Product Class A Resale - LOC'',
            PRODUCT_CLASS_A_MARGIN as ''Product Class A Margin'',
            PRODUCT_CLASS_A1A_ITEM_COUNT as ''Product Class A1A Item Count'',
            PRODUCT_CLASS_A1A_COST as ''Product Class A1A Cost - LOC'',
            PRODUCT_CLASS_A1A_RESALE as ''Product Class A1A Resale - LOC'',
            PRODUCT_CLASS_A1A_MARGIN as ''Product Class A1A Margin'',
            PRODUCT_CLASS_A1B_ITEM_COUNT as ''Product Class A1B Item Count'',
            PRODUCT_CLASS_A1B_COST as ''Product Class A1B Cost - LOC'',
            PRODUCT_CLASS_A1B_RESALE as ''Product Class A1B Resale - LOC'',
            PRODUCT_CLASS_A1B_MARGIN as ''Product Class A1B Margin'',
            PRODUCT_CLASS_B_ITEM_COUNT as ''Product Class B Item Count'',
            PRODUCT_CLASS_B_COST as ''Product Class B Cost - LOC'',
            PRODUCT_CLASS_B_RESALE as ''Product Class B Resale - LOC'',
            PRODUCT_CLASS_B_MARGIN as ''Product Class B Margin'',
            PRODUCT_CLASS_C_ITEM_COUNT as ''Product Class C Item Count'',
            PRODUCT_CLASS_C_COST as ''Product Class C Cost - LOC'',
            PRODUCT_CLASS_C_RESALE as ''Product Class C Resale - LOC'',
            PRODUCT_CLASS_C_MARGIN as ''Product Class C Margin'',
            PRODUCT_CLASS_D_ITEM_COUNT as ''Product Class D Item Count'',
            PRODUCT_CLASS_D_COST as ''Product Class D Cost - LOC'',
            PRODUCT_CLASS_D_RESALE as ''Product Class D Resale - LOC'',
            PRODUCT_CLASS_D_MARGIN as ''Product Class D Margin'',
            PROPOSAL_MANAGER_COMMENTS as ''Proposal Manager Comments'',
            SALES_LEVEL_1 as ''Sales Level 1'',
            SALES_LEVEL_2 as ''Sales Level 2'',
            GROUP_VP_GENERAL_MANAGER as ''Group VP and General Manager'',
            ACCOUNTING_VICE_PRESIDENT as ''Accounting Vice President'',
            FINANCIAL_VICE_PRESIDENT as ''Financial Vice President'',
            CHIEF_FINANCIAL_OFFICE as ''Chief Financial Officer'',
            OPERATIONS_VICE_PRESIDENT as ''Operations Vice President'',
            SUPPLY_CHAIN_VICE_PRESIDENT as ''Supply Chain Vice President'',
            SR_DIRECTOR_OF_CONTRACTS as ''Sr Director of Contracts'',
            APPROVERS_COMMENTS as ''Approvers Comments'',
            APPROVAL_STATUS as ''Approval Status'',
            DATE_SUBMITTED_TO_SALES as ''Date Submitted to Sales''))) ';
        OPEN cur_snapshot_list;
        LOOP
            FETCH cur_snapshot_list INTO l_snapshot_ver;
            EXIT WHEN cur_snapshot_list%notfound;

            IF l_count = 0 THEN
                l_alias := 'A';
                l_select_part := REPLACE(l_select_part,'$FIELDS$',
                    l_alias || '.fieldname as "SNAPSHOTS"$FIELDS$');
                l_from_part := REPLACE(l_inner_query,'$SNAP$',l_snapshot_ver)
                 || l_alias;
            ELSE
                l_alias := 'A' || l_count;
                l_where_part := REPLACE(l_where_part,'$COND$',
                    'AND A.fieldname=' || l_alias || '.fieldname $COND$');
                l_from_part := l_from_part || ','
                 || REPLACE(l_inner_query,'$SNAP$',l_snapshot_ver) || l_alias;
            END IF;
            l_select_part := REPLACE(l_select_part,'$FIELDS$',',' || l_alias ||
                '.fieldvalue as "Version' || round(l_snapshot_ver) || '"$FIELDS$');
            l_count := l_count + 1;
        END LOOP;
        CLOSE cur_snapshot_list;
        IF l_count = 0 THEN
            l_alias := 'A';
            insert_dummy_snapshot(p_opp_id);
            l_from_part := REPLACE(l_inner_query,'SNAPSHOT_VERSION = ''$SNAP$''',
                'rownum=1') || l_alias;
            l_where_part := REPLACE(l_where_part,'$COND$','');
            l_select_part := REPLACE(l_select_part,'$FIELDS$',
                'A.fieldname as "SNAPSHOTS",A.fieldvalue as "Version"');
            l_sql_query := l_select_part || l_from_part || l_where_part || l_order_by;
        ELSE
            l_where_part := REPLACE(l_where_part,'$COND$','');
            l_select_part := REPLACE(l_select_part,'$FIELDS$','');
            l_sql_query := l_select_part || l_from_part || l_where_part || l_order_by;
        END IF;
        --dbms_output.put_line(l_sql_query);
        OPEN p_snapshot_cur FOR l_sql_query;
        --log
        cct_create_errors.record_error(
            P_OPPORTUNITY_ID   => P_OPP_ID,
            P_ERROR_MESSAGE    => 'SUCCESS_END',
            P_ERROR_TYPE       => 20,
            P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.fetch_snapshots',
            P_CREATED_BY       => sys_context('USERENV','OS_USER'));
    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            P_OPPORTUNITY_ID   => p_opp_id,
            P_ERROR_MESSAGE    => g_msg,
            P_ERROR_TYPE       => g_code,
            P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.fetch_snapshots',
            P_CREATED_BY       => sys_context('USERENV','OS_USER'));
    END fetch_snapshots;

    PROCEDURE insert_snapshots(
        P_OPP_ID               IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE)
    AS
        l_snapshot_ver            CCT_SNAPSHOT.SNAPSHOT_VERSION%TYPE;
        l_ldoa_version            CCT_QUOTE_APPROVALS.LDOA_VERSION_ID%TYPE;
        l_creation_date           CCT_QUOTE_APPROVALS.CREATION_DATE%TYPE;
        l_approval_status         CCT_QUOTE_APPROVALS.APPROVAL_STATUS%TYPE;
        l_proposal_mgr_comments   CCT_QUOTE_APPROVALS.PROPOSAL_MANAGER_COMMENTS%TYPE;
        l_quote_version           CCT_QUOTE_REVISIONS.QUOTE_REVISION%TYPE;
        l_flag                    NUMBER;
        l_dummy_flag              NUMBER;
        l_counter                 NUMBER := 0;
        l_max_snapshot_ver        CCT_SNAPSHOT.SNAPSHOT_VERSION%TYPE;
        l_sales_level_1           CCT_SNAPSHOT_APPROVAL.SALES_LEVEL_1%TYPE;
        l_sales_level_2           CCT_SNAPSHOT_APPROVAL.SALES_LEVEL_2%TYPE;
        l_grp_vp_general_mgr      CCT_SNAPSHOT_APPROVAL.GROUP_VP_GENERAL_MANAGER%TYPE;  
        l_acc_vice_president      CCT_SNAPSHOT_APPROVAL.ACCOUNTING_VICE_PRESIDENT%TYPE;
        l_fin_vice_president      CCT_SNAPSHOT_APPROVAL.FINANCIAL_VICE_PRESIDENT%TYPE;
        l_op_vice_president       CCT_SNAPSHOT_APPROVAL.OPERATIONS_VICE_PRESIDENT%TYPE;
        l_chief_financial_office  CCT_SNAPSHOT_APPROVAL.CHIEF_FINANCIAL_OFFICE%TYPE;
        l_supply_chain_vp         CCT_SNAPSHOT_APPROVAL.SUPPLY_CHAIN_VICE_PRESIDENT%TYPE;
        l_sr_contracts_director   CCT_SNAPSHOT_APPROVAL.SR_DIRECTOR_OF_CONTRACTS%TYPE;
        CURSOR cur_snapshot_list IS
            SELECT DISTINCT cs.snapshot_version,
                   MAX(cq.ldoa_version_id),
                   MAX(cq.creation_date)
              FROM cct_quote_approvals cq RIGHT JOIN
                   (SELECT *
                      FROM cct_snapshot
                     WHERE upper(snapshot_type) = 'APPROVAL') cs
                ON cs.opportunity_id = cq.opportunity_id AND
                   cs.snapshot_id = cq.snapshot_id
             WHERE cs.opportunity_id = P_OPP_ID
            GROUP BY snapshot_version
            ORDER BY to_number(snapshot_version) DESC;
    BEGIN
        update_snapshot_master(P_OPP_ID => P_OPP_ID);
        OPEN cur_snapshot_list;
        LOOP
            FETCH cur_snapshot_list INTO l_snapshot_ver,l_ldoa_version,l_creation_date;
            EXIT WHEN cur_snapshot_list%notfound;
            --get approval status
            SELECT DISTINCT NVL(approval_status,'NOT INITIATED')
              INTO l_approval_status
              FROM (SELECT cq2.approval_status,
                           cq2.opportunity_id,
                           cq2.snapshot_id
                      FROM (SELECT MAX(approval_id) AS approval_id,
                                   snapshot_id
                              FROM cct_quote_approvals
                            group by snapshot_id) cq1,
                            cct_quote_approvals cq2
                     WHERE cq1.approval_id = cq2.approval_id AND
                           cq1.snapshot_id = cq2.snapshot_id) cq RIGHT JOIN
                   cct_snapshot cs
                ON cq.snapshot_id = cs.snapshot_id AND
                   cs.opportunity_id = cq.opportunity_id
             WHERE cs.opportunity_id = P_OPP_ID AND
                   cs.snapshot_version = l_snapshot_ver;
            --log
            cct_create_errors.record_error(
                P_OPPORTUNITY_ID   => p_opp_id,
                P_ERROR_MESSAGE    => 'SUCCESS_APPR_STATUS',
                P_ERROR_TYPE       => 20,
                P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.insert_snapshots',
                P_CREATED_BY       => sys_context('USERENV','OS_USER'));
            --get proposal manager comments
            cct_fetch_fields.get_proposal_comments(
                P_OPPORTUNITY_ID => P_OPP_ID,
                P_SNAPSHOT_VERSION => l_snapshot_ver,
                P_PROPOSAL_COMMENTS => l_proposal_mgr_comments);
            --log
            cct_create_errors.record_error(
                P_OPPORTUNITY_ID   => p_opp_id,
                P_ERROR_MESSAGE    => 'SUCCESS_PRP_MGR',
                P_ERROR_TYPE       => 30,
                P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.insert_snapshots',
                P_CREATED_BY       => sys_context('USERENV','OS_USER'));
            --get quote version..
            SELECT cqp.quote_revision
              INTO l_quote_version
              FROM cct_quote cq,
                   cct_quote_revisions cqp,
                   cct_snapshot cs
             WHERE cs.quote_revision_id = cqp.revision_id AND
                   cqp.quote_id = cq.quote_id AND
                   cq.opportunity_id = cs.opportunity_id AND
                   cs.opportunity_id = P_OPP_ID AND
                   cs.snapshot_version = l_snapshot_ver;
            --log
            cct_create_errors.record_error(
                P_OPPORTUNITY_ID   => p_opp_id,
                P_ERROR_MESSAGE    => 'SUCCESS_QUOTE_REV',
                P_ERROR_TYPE       => 40,
                P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.insert_snapshots',
                P_CREATED_BY       => sys_context('USERENV','OS_USER'));
            --get approval users
            BEGIN
                SELECT approval_user_name 
                  INTO l_sales_level_1
                  FROM cct_approval_users cau,
                       cct_snapshot cs 
                 WHERE cs.snapshot_id = cau.snapshot_id AND
                       cs.opportunity_id = P_OPP_ID AND
                       cs.snapshot_version = l_snapshot_ver AND 
                       cau.approval_role_id='CCT_SALES_DIRECTOR' AND 
                       approval_user_name IS NOT NULL;
            EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                l_sales_level_1 := NULL;
            END;
            BEGIN 
                SELECT approval_user_name 
                  INTO l_sales_level_2
                  FROM cct_approval_users cau,
                       cct_snapshot cs 
                 WHERE cs.snapshot_id = cau.snapshot_id AND
                       cs.opportunity_id = P_OPP_ID AND
                       cs.snapshot_version = l_snapshot_ver AND 
                       cau.approval_role_id='CCT_SALES_VP' AND 
                       approval_user_name IS NOT NULL;
            EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                l_sales_level_2 := NULL;
            END;
            BEGIN 
                SELECT approval_user_name 
                  INTO l_grp_vp_general_mgr
                  FROM cct_approval_users cau,
                       cct_snapshot cs 
                 WHERE cs.snapshot_id = cau.snapshot_id AND
                       cs.opportunity_id = P_OPP_ID AND
                       cs.snapshot_version = l_snapshot_ver AND 
                       cau.approval_role_id='CCT_GROUP_VP_AND_GM' AND 
                       approval_user_name IS NOT NULL;
            EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                l_grp_vp_general_mgr := NULL;
            END;
            BEGIN 
                SELECT approval_user_name 
                  INTO l_acc_vice_president
                  FROM cct_approval_users cau,
                       cct_snapshot cs 
                 WHERE cs.snapshot_id = cau.snapshot_id AND
                       cs.opportunity_id = P_OPP_ID AND
                       cs.snapshot_version = l_snapshot_ver AND 
                       cau.approval_role_id='CCT_ACCOUNTING_DIRECTOR' AND 
                       approval_user_name IS NOT NULL;
            EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                l_acc_vice_president := NULL;
            END;
            BEGIN 
                SELECT approval_user_name 
                  INTO l_fin_vice_president
                  FROM cct_approval_users cau,
                       cct_snapshot cs 
                 WHERE cs.snapshot_id = cau.snapshot_id AND
                       cs.opportunity_id = P_OPP_ID AND
                       cs.snapshot_version = l_snapshot_ver AND 
                       cau.approval_role_id='CCT_FINANCE_VP' AND 
                       approval_user_name IS NOT NULL;
            EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                l_fin_vice_president := NULL;
            END;
            BEGIN 
                SELECT approval_user_name 
                  INTO l_op_vice_president
                  FROM cct_approval_users cau,
                       cct_snapshot cs 
                 WHERE cs.snapshot_id = cau.snapshot_id AND
                       cs.opportunity_id = P_OPP_ID AND
                       cs.snapshot_version = l_snapshot_ver AND 
                       cau.approval_role_id='CCT_OPERATIONS_VP' AND 
                       approval_user_name IS NOT NULL;
            EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                l_op_vice_president := NULL;
            END;
            BEGIN 
                SELECT approval_user_name 
                  INTO l_chief_financial_office
                  FROM cct_approval_users cau,
                       cct_snapshot cs 
                 WHERE cs.snapshot_id = cau.snapshot_id AND
                       cs.opportunity_id = P_OPP_ID AND
                       cs.snapshot_version = l_snapshot_ver AND 
                       cau.approval_role_id='CCT_CORPORATE_CFO' AND 
                       approval_user_name IS NOT NULL;
            EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                l_chief_financial_office := NULL;
            END;
            BEGIN 
                SELECT approval_user_name 
                  INTO l_supply_chain_vp
                  FROM cct_approval_users cau,
                       cct_snapshot cs 
                 WHERE cs.snapshot_id = cau.snapshot_id AND
                       cs.opportunity_id = P_OPP_ID AND
                       cs.snapshot_version = l_snapshot_ver AND 
                       cau.approval_role_id='CCT_SUPPLY_CHAIN_VP' AND 
                       approval_user_name IS NOT NULL;
            EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                l_supply_chain_vp := NULL;
            END;
            BEGIN 
                SELECT approval_user_name 
                  INTO l_sr_contracts_director
                  FROM cct_approval_users cau,
                       cct_snapshot cs 
                 WHERE cs.snapshot_id = cau.snapshot_id AND
                       cs.opportunity_id = P_OPP_ID AND
                       cs.snapshot_version = l_snapshot_ver AND 
                       cau.approval_role_id='CCT_SR_CONTRACTS_DIRECTOR' AND 
                       approval_user_name IS NOT NULL;
            EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                l_sr_contracts_director := NULL;
            END;

            --check already in snapshot master
            SELECT count(*)
              INTO l_flag
              FROM CCT_SNAPSHOT_APPROVAL
             WHERE opportunity_id = P_OPP_ID AND
                   snapshot_version = l_snapshot_ver;
            --log
            cct_create_errors.record_error(
                P_OPPORTUNITY_ID   => p_opp_id,
                P_ERROR_MESSAGE    => 'SUCCESS_COUNT',
                P_ERROR_TYPE       => 50,
                P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.insert_snapshots',
                P_CREATED_BY       => sys_context('USERENV','OS_USER'));
            /**DBMS_OUTPUT.PUT_LINE(l_snapshot_ver);
            DBMS_OUTPUT.PUT_LINE('l_sales_level_1 -' || l_sales_level_1);
            DBMS_OUTPUT.PUT_LINE('l_sales_level_2 -' || l_sales_level_2);
            DBMS_OUTPUT.PUT_LINE('l_grp_vp_general_mgr -' || l_grp_vp_general_mgr);
            DBMS_OUTPUT.PUT_LINE('l_acc_vice_president -' || l_acc_vice_president);
            DBMS_OUTPUT.PUT_LINE('l_fin_vice_president -' || l_fin_vice_president);
            DBMS_OUTPUT.PUT_LINE('l_op_vice_president -' || l_op_vice_president);
            DBMS_OUTPUT.PUT_LINE('l_chief_financial_office -' || l_chief_financial_office);**/
            IF l_flag = 0 THEN
                --delete dummy record
                SELECT count(*)
                  INTO l_dummy_flag
                  FROM CCT_SNAPSHOT_APPROVAL
                 WHERE opportunity_id = P_OPP_ID AND
                       snapshot_version IS NULL;

                IF l_dummy_flag > 0 THEN
                    DELETE FROM CCT_SNAPSHOT_APPROVAL
                     WHERE opportunity_id = P_OPP_ID AND
                           snapshot_version  IS NULL;
                    COMMIT;
                    --log
                    cct_create_errors.record_error(
                        P_OPPORTUNITY_ID   => p_opp_id,
                        P_ERROR_MESSAGE    => 'SUCCESS_DEL_DUMMY',
                        P_ERROR_TYPE       => 60,
                        P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.insert_snapshots',
                        P_CREATED_BY       => sys_context('USERENV','OS_USER'));
                END IF;

                INSERT INTO CCT_SNAPSHOT_APPROVAL
                    SELECT  co.opportunity_id AS opportunity_id,
                            TO_CHAR(cs.snapshot_version) AS snapshot_version,
							TO_CHAR(cs.created_date,'MM/DD/YYYY') || ' ' || TO_CHAR(cs.created_Date, 'HH24:MI:SS') AS snapshot_date,
							co.opportunity_number AS crf_number,
							TO_CHAR(crfi.sales_team_number) AS sales_team,
							co.project_number AS contract_ref,
							TO_CHAR(co.customer_due_date,'MM/DD/YYYY') || ' ' || TO_CHAR(co.customer_due_date, 'HH24:MI:SS') AS customer_due_date,
							co.customer_name,
							CASE WHEN UPPER(co.gt25) = 'TRUE' THEN 'Y'
								 WHEN UPPER(co.gt25) = 'FALSE'  THEN 'N'
							ELSE 'N' END,
							crfr.contract_type AS contract_type,
							crfr.project_type AS project_type,
							TRIM(TO_CHAR(co.contract_term,'999,999,999,999,999')) AS contract_term,
							co.program_type,
							TRIM(TO_CHAR(cqf.total_product_cost_loc,'999,999,999,999,999')) AS total_product_cost_loc,
							TRIM(TO_CHAR(cqf.total_product_resale_loc,'999,999,999,999,999')) AS total_product_resale_loc,
							TRIM(TO_CHAR(cqf.product_gross_margin,'999,999,999,999,999'))||'%' AS product_gross_margin,
							TRIM(TO_CHAR(cqf.total_adjustments,'999,999,999,999,999')) AS total_adjustments,
							TRIM(TO_CHAR(cqf.project_net_margin,'999,999,999,999,999'))||'%' AS project_net_margin,
							TRIM(TO_CHAR(cp.customer_rebate,'999,999,999,999,999')) AS rebate,
							TRIM(TO_CHAR(cp.total_program_costs,'999,999,999,999,999')) AS total_program_costs,
							TRIM(TO_CHAR(cp.shipping_fee_taxes,'999,999,999,999,999')) AS shipping_fees_taxes,
							TRIM(TO_CHAR(cp.service_fees,'999,999,999,999,999')) AS service_fees,
							CASE WHEN UPPER(co.payment_terms)='OTHER' THEN
								co.other_payment_terms
							ELSE co.payment_terms END,
							CASE WHEN UPPER(co.incoterms)='OTHER' THEN
								co.other_incoterms
							ELSE co.incoterms END,
							co.customer_doc,
							TRIM(TO_CHAR(cp.agent_commissions,'999,999,999,999,999')) AS agent_commissions,
							TO_CHAR(co.line_min_crf) AS line_min,
							TO_CHAR(co.order_minimum) AS order_min,
							co.lol_protection,
							co.cust_liab_protection,
							TRIM(TO_CHAR(co.stocking_strategy_crf,'999,999,999,999,999')) AS stocking_strategy,
							co.liq_damages,
							co.title_transfer,
							co.rights_of_return,
							co.incentives,
							co.cancel_privileges,
							co.bill_hold,
							co.buy_back,
							co.currency,
							TRIM(TO_CHAR(cqf.total_parts_requested,'999,999,999,999,999')) AS total_parts_requested,
							TRIM(TO_CHAR(cqf.total_parts_quoted,'999,999,999,999,999')) AS total_parts_quoted,
							TRIM(TO_CHAR(cqf.product_class_a_item_count,'999,999,999,999,999')) AS product_class_a_item_count,
							TRIM(TO_CHAR(cqf.product_class_a_cost,'999,999,999,999,999')) AS product_class_a_cost,
							TRIM(TO_CHAR(cqf.product_class_a_resale,'999,999,999,999,999')) AS product_class_a_resale,
							TRIM(TO_CHAR(cqf.product_class_a_margin,'999,999,999,999,999'))||'%' AS product_class_a_margin,
							TRIM(TO_CHAR(cqf.product_class_a1a_item_count,'999,999,999,999,999')) AS product_class_a1a_item_count,
							TRIM(TO_CHAR(cqf.product_class_a1a_cost,'999,999,999,999,999')) AS product_class_a1a_cost,
							TRIM(TO_CHAR(cqf.product_class_a1a_resale,'999,999,999,999,999')) AS product_class_a1a_resale,
							TRIM(TO_CHAR(cqf.product_class_a1a_margin,'999,999,999,999,999'))||'%' AS product_class_a1a_margin,
							TRIM(TO_CHAR(cqf.product_class_a1b_item_count,'999,999,999,999,999')) AS product_class_a1b_item_count,
							TRIM(TO_CHAR(cqf.product_class_a1b_cost,'999,999,999,999,999')) AS product_class_a1b_cost,
							TRIM(TO_CHAR(cqf.product_class_a1b_resale,'999,999,999,999,999')) AS product_class_a1b_resale,
							TRIM(TO_CHAR(cqf.product_class_a1b_margin,'999,999,999,999,999'))||'%' AS product_class_a1b_margin,
							TRIM(TO_CHAR(cqf.product_class_b_item_count,'999,999,999,999,999')) AS product_class_b_item_count,
							TRIM(TO_CHAR(cqf.product_class_b_cost,'999,999,999,999,999')) AS product_class_b_cost,
							TRIM(TO_CHAR(cqf.product_class_b_resale,'999,999,999,999,999')) AS product_class_b_resale,
							TRIM(TO_CHAR(cqf.product_class_b_margin,'999,999,999,999,999'))||'%' AS product_class_b_margin,
							TRIM(TO_CHAR(cqf.product_class_c_item_count,'999,999,999,999,999')) AS product_class_c_item_count,
							TRIM(TO_CHAR(cqf.product_class_c_cost,'999,999,999,999,999')) AS product_class_c_cost,
							TRIM(TO_CHAR(cqf.product_class_c_resale,'999,999,999,999,999')) AS product_class_c_resale,
							TRIM(TO_CHAR(cqf.product_class_c_margin,'999,999,999,999,999'))||'%' AS product_class_c_margin,
							TRIM(TO_CHAR(cqf.product_class_d_item_count,'999,999,999,999,999')) AS product_class_d_item_count,
							TRIM(TO_CHAR(cqf.product_class_d_cost,'999,999,999,999,999')) AS product_class_d_cost,
							TRIM(TO_CHAR(cqf.product_class_d_resale,'999,999,999,999,999')) AS product_class_d_resale,
							TRIM(TO_CHAR(cqf.product_class_d_margin,'999,999,999,999,999'))||'%' AS product_class_d_margin,
                                                        
							l_proposal_mgr_comments,
							CASE WHEN l_ldoa_version IS NULL THEN 'Not Initiated'
								ELSE 'Not Applicable'
							END,
							CASE WHEN l_ldoa_version IS NULL THEN 'Not Initiated'
								ELSE 'Not Applicable'
							END,
							CASE WHEN l_ldoa_version IS NULL THEN 'Not Initiated'
								ELSE 'Not Applicable'
							END,
							CASE WHEN l_ldoa_version IS NULL THEN 'Not Initiated'
								ELSE 'Not Applicable'
							END,
							CASE WHEN l_ldoa_version IS NULL THEN 'Not Initiated'
								ELSE 'Not Applicable'
							END,
							CASE WHEN l_ldoa_version IS NULL THEN 'Not Initiated'
								ELSE 'Not Applicable'
							END,
							CASE WHEN l_ldoa_version IS NULL THEN 'Not Initiated'
								ELSE 'Not Applicable'
							END,
                            l_approval_status,
							TO_CHAR(co.date_submitted_to_sales,'MM/DD/YYYY') || ' ' || TO_CHAR(co.date_submitted_to_sales, 'HH24:MI:SS') AS date_submitted_to_sales,
							SYSDATE,
							sys_context('USERENV','OS_USER'),
							NULL,
                            NULL,
							NULL,
                            CASE WHEN l_ldoa_version IS NULL THEN 'Not Initiated'
								ELSE 'Not Applicable'
							END ,
                                                        CASE WHEN l_ldoa_version IS NULL THEN 'Not Initiated'
								ELSE 'Not Applicable'
							END
					  FROM  cct_snapshot cs,
							cct_opportunity_snapshot co,
							cct_quote_formula_ss_final cqf,
							cct_program_cost_header cp,
							cct_crf crf,
							cct_crf_customer_request crfr,
							cct_crf_customer_info crfi
					 WHERE  cs.opportunity_id = co.opportunity_id AND
							cs.snapshot_id = co.snapshot_id AND
							cp.opportunity_id = co.opportunity_id AND
							cqf.quote_revision_id = cs.quote_revision_id AND
							cqf.snapshot_id = cs.snapshot_id AND
							crf.crf_no = co.opportunity_number AND
							crf.crf_id = crfr.crf_id AND
							crfr.crf_id = crfi.crf_id AND
							cs.opportunity_id = P_OPP_ID AND
							cs.snapshot_version = l_snapshot_ver;
                COMMIT;
                cct_create_errors.record_error(
                        P_OPPORTUNITY_ID   => p_opp_id,
                        P_ERROR_MESSAGE    => 'SUCCESS_INSERT',
                        P_ERROR_TYPE       => 70,
                        P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.insert_snapshots',
                        P_CREATED_BY       => sys_context('USERENV','OS_USER'));
            ELSE 
                UPDATE cct_snapshot_approval 
                   SET SALES_LEVEL_1 = (CASE WHEN l_ldoa_version IS NULL AND l_sales_level_1 IS NOT NULL THEN 
                                            'Not Initiated - ' || l_sales_level_1 
                                        WHEN l_ldoa_version IS NULL AND l_sales_level_1 IS NULL THEN 
                                            'Not Initiated' 
                                        WHEN l_ldoa_version IS NOT NULL AND l_sales_level_1 IS NOT NULL THEN 
                                            'Not Applicable - ' || l_sales_level_1 
                                        WHEN l_ldoa_version IS NOT NULL AND l_sales_level_1 IS NULL THEN 
                                            'Not Applicable'
                                        END),
                       SALES_LEVEL_2 = (CASE WHEN l_ldoa_version IS NULL AND l_sales_level_2 IS NOT NULL THEN 
                                            'Not Initiated - ' || l_sales_level_2 
                                        WHEN l_ldoa_version IS NULL AND l_sales_level_2 IS NULL THEN 
                                            'Not Initiated' 
                                        WHEN l_ldoa_version IS NOT NULL AND l_sales_level_2 IS NOT NULL THEN 
                                            'Not Applicable - ' || l_sales_level_2 
                                        WHEN l_ldoa_version IS NOT NULL AND l_sales_level_2 IS NULL THEN 
                                            'Not Applicable'
                                        END),
                       GROUP_VP_GENERAL_MANAGER = (CASE WHEN l_ldoa_version IS NULL AND l_grp_vp_general_mgr IS NOT NULL THEN 
                                                        'Not Initiated - ' || l_grp_vp_general_mgr 
                                                   WHEN l_ldoa_version IS NULL AND l_grp_vp_general_mgr IS NULL THEN 
                                                        'Not Initiated' 
                                                   WHEN l_ldoa_version IS NOT NULL AND l_grp_vp_general_mgr IS NOT NULL THEN 
                                                        'Not Applicable - ' || l_grp_vp_general_mgr 
                                                   WHEN l_ldoa_version IS NOT NULL AND l_grp_vp_general_mgr IS NULL THEN 
                                                        'Not Applicable'
                                                   END),
                       ACCOUNTING_VICE_PRESIDENT = (CASE WHEN l_ldoa_version IS NULL AND l_acc_vice_president IS NOT NULL THEN 
                                                        'Not Initiated - ' || l_acc_vice_president 
                                                    WHEN l_ldoa_version IS NULL AND l_acc_vice_president IS NULL THEN 
                                                        'Not Initiated' 
                                                    WHEN l_ldoa_version IS NOT NULL AND l_acc_vice_president IS NOT NULL THEN 
                                                        'Not Applicable - ' || l_acc_vice_president 
                                                    WHEN l_ldoa_version IS NOT NULL AND l_acc_vice_president IS NULL THEN 
                                                        'Not Applicable'
                                                    END),
                       FINANCIAL_VICE_PRESIDENT = (CASE WHEN l_ldoa_version IS NULL AND l_fin_vice_president IS NOT NULL THEN 
                                                        'Not Initiated - ' || l_fin_vice_president 
                                                   WHEN l_ldoa_version IS NULL AND l_fin_vice_president IS NULL THEN 
                                                        'Not Initiated' 
                                                   WHEN l_ldoa_version IS NOT NULL AND l_fin_vice_president IS NOT NULL THEN 
                                                        'Not Applicable - ' || l_fin_vice_president 
                                                   WHEN l_ldoa_version IS NOT NULL AND l_fin_vice_president IS NULL THEN 
                                                        'Not Applicable'
                                                   END),
                       OPERATIONS_VICE_PRESIDENT = (CASE WHEN l_ldoa_version IS NULL AND l_op_vice_president IS NOT NULL THEN 
                                                        'Not Initiated - ' || l_op_vice_president 
                                                    WHEN l_ldoa_version IS NULL AND l_op_vice_president IS NULL THEN 
                                                        'Not Initiated' 
                                                    WHEN l_ldoa_version IS NOT NULL AND l_op_vice_president IS NOT NULL THEN 
                                                        'Not Applicable - ' || l_op_vice_president 
                                                    WHEN l_ldoa_version IS NOT NULL AND l_op_vice_president IS NULL THEN 
                                                        'Not Applicable'
                                                    END),
                       CHIEF_FINANCIAL_OFFICE = (CASE WHEN l_ldoa_version IS NULL AND l_chief_financial_office IS NOT NULL THEN 
                                                    'Not Initiated - ' || l_chief_financial_office 
                                                 WHEN l_ldoa_version IS NULL AND l_chief_financial_office IS NULL THEN 
                                                    'Not Initiated' 
                                                 WHEN l_ldoa_version IS NOT NULL AND l_chief_financial_office IS NOT NULL THEN 
                                                    'Not Applicable - ' || l_chief_financial_office 
                                                 WHEN l_ldoa_version IS NOT NULL AND l_chief_financial_office IS NULL THEN 
                                                    'Not Applicable'
                                                 END),
                       SUPPLY_CHAIN_VICE_PRESIDENT = (CASE WHEN l_ldoa_version IS NULL AND l_supply_chain_vp IS NOT NULL THEN 
                                                    'Not Initiated - ' || l_supply_chain_vp 
                                                 WHEN l_ldoa_version IS NULL AND l_supply_chain_vp IS NULL THEN 
                                                    'Not Initiated' 
                                                 WHEN l_ldoa_version IS NOT NULL AND l_supply_chain_vp IS NOT NULL THEN 
                                                    'Not Applicable - ' || l_supply_chain_vp 
                                                 WHEN l_ldoa_version IS NOT NULL AND l_supply_chain_vp IS NULL THEN 
                                                    'Not Applicable'
                                                 END),
                        SR_DIRECTOR_OF_CONTRACTS = (CASE WHEN l_ldoa_version IS NULL AND l_sr_contracts_director IS NOT NULL THEN 
                                                    'Not Initiated - ' || l_sr_contracts_director 
                                                 WHEN l_ldoa_version IS NULL AND l_sr_contracts_director IS NULL THEN 
                                                    'Not Initiated' 
                                                 WHEN l_ldoa_version IS NOT NULL AND l_sr_contracts_director IS NOT NULL THEN 
                                                    'Not Applicable - ' || l_sr_contracts_director 
                                                 WHEN l_ldoa_version IS NOT NULL AND l_sr_contracts_director IS NULL THEN 
                                                    'Not Applicable'
                                                 END) 
                 WHERE opportunity_id = P_OPP_ID AND
                       snapshot_version = l_snapshot_ver;
                COMMIT;
            END IF;
        END LOOP;
        CLOSE cur_snapshot_list;
    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            P_OPPORTUNITY_ID   => p_opp_id,
            P_ERROR_MESSAGE    => g_msg,
            P_ERROR_TYPE       => g_code,
            P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.insert_snapshots',
            P_CREATED_BY       => sys_context('USERENV','OS_USER'));
    END insert_snapshots;

    PROCEDURE insert_approval_snapshots(
        P_OPP_ID               IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE)
    AS
        l_snapshot_ver            CCT_SNAPSHOT.SNAPSHOT_VERSION%TYPE;
        l_ldoa_version            CCT_QUOTE_APPROVALS.LDOA_VERSION_ID%TYPE;
        l_creation_date           CCT_QUOTE_APPROVALS.CREATION_DATE%TYPE;
        l_approval_status         CCT_QUOTE_APPROVALS.APPROVAL_STATUS%TYPE;
        l_proposal_mgr_comments   CCT_QUOTE_APPROVALS.PROPOSAL_MANAGER_COMMENTS%TYPE;
        l_quote_version           CCT_QUOTE_REVISIONS.QUOTE_REVISION%TYPE;
        l_flag                    NUMBER;
        l_dummy_flag              NUMBER;
        l_max_snap_ver            CCT_SNAPSHOT.SNAPSHOT_VERSION%TYPE ;
        l_sales_level_1           CCT_SNAPSHOT_APPROVAL.SALES_LEVEL_1%TYPE;
        l_sales_level_2           CCT_SNAPSHOT_APPROVAL.SALES_LEVEL_2%TYPE;
        l_grp_vp_general_mgr      CCT_SNAPSHOT_APPROVAL.GROUP_VP_GENERAL_MANAGER%TYPE;  
        l_acc_vice_president      CCT_SNAPSHOT_APPROVAL.ACCOUNTING_VICE_PRESIDENT%TYPE;
        l_fin_vice_president      CCT_SNAPSHOT_APPROVAL.FINANCIAL_VICE_PRESIDENT%TYPE;
        l_op_vice_president       CCT_SNAPSHOT_APPROVAL.OPERATIONS_VICE_PRESIDENT%TYPE;
        l_chief_financial_office  CCT_SNAPSHOT_APPROVAL.CHIEF_FINANCIAL_OFFICE%TYPE;
        l_supply_chain_vp         CCT_SNAPSHOT_APPROVAL.SUPPLY_CHAIN_VICE_PRESIDENT%TYPE;
        l_sr_contracts_director   CCT_SNAPSHOT_APPROVAL.SR_DIRECTOR_OF_CONTRACTS%TYPE;
        CURSOR cur_snapshot_list IS
            SELECT DISTINCT cs.snapshot_version,
                   MAX(cq.ldoa_version_id),
                   MAX(cq.creation_date)
              FROM cct_quote_approvals cq,
                   cct_snapshot cs
             WHERE cq.opportunity_id = P_OPP_ID AND
                   cs.opportunity_id = cq.opportunity_id AND
                   cs.snapshot_id = cq.snapshot_id AND
                   UPPER(cs.snapshot_type) = 'APPROVAL'
            GROUP BY snapshot_version
            ORDER BY to_number(snapshot_version) DESC;

    BEGIN
        update_snapshot_master(P_OPP_ID => P_OPP_ID);
        OPEN cur_snapshot_list;
        LOOP
            FETCH cur_snapshot_list INTO l_snapshot_ver,l_ldoa_version,l_creation_date;
            EXIT WHEN cur_snapshot_list%notfound;
            --get approval status
            SELECT DISTINCT cq1.approval_status
              INTO l_approval_status
              FROM (SELECT MAX(approval_id) AS approval_id,
                           snapshot_id
                      FROM cct_quote_approvals
                   GROUP BY snapshot_id) cq,
                   cct_quote_approvals cq1,
                   cct_snapshot cs
             WHERE cq.snapshot_id = cs.snapshot_id AND
                   cs.opportunity_id = cq1.opportunity_id AND
                   cs.opportunity_id = P_OPP_ID AND
                   cs.snapshot_version = l_snapshot_ver AND
                   cq.snapshot_id = cq1.snapshot_id and
                   cq1.approval_id = cq.approval_id;
            --log
            cct_create_errors.record_error(
                P_OPPORTUNITY_ID   => p_opp_id,
                P_ERROR_MESSAGE    => 'SUCCESS_APPR_STATUS',
                P_ERROR_TYPE       => 20,
                P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.insert_approval_snapshots',
                P_CREATED_BY       => sys_context('USERENV','OS_USER'));
            --get proposal manager comments
            cct_fetch_fields.get_proposal_comments(
                P_OPPORTUNITY_ID => P_OPP_ID,
                P_SNAPSHOT_VERSION => l_snapshot_ver,
                P_PROPOSAL_COMMENTS => l_proposal_mgr_comments
              );
            --log
            cct_create_errors.record_error(
                P_OPPORTUNITY_ID   => p_opp_id,
                P_ERROR_MESSAGE    => 'SUCCESS_PROP_MGR',
                P_ERROR_TYPE       => 30,
                P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.insert_approval_snapshots',
                P_CREATED_BY       => sys_context('USERENV','OS_USER'));
            --get quote version..
            SELECT cqp.quote_revision
              INTO l_quote_version
              FROM cct_quote cq,
                   cct_quote_revisions cqp,
                   cct_snapshot cs
             WHERE cs.quote_revision_id = cqp.revision_id AND
                   cqp.quote_id = cq.quote_id AND
                   cq.opportunity_id = cs.opportunity_id AND
                   cs.opportunity_id = P_OPP_ID AND
                   cs.snapshot_version = l_snapshot_ver;
            --log
            cct_create_errors.record_error(
                P_OPPORTUNITY_ID   => p_opp_id,
                P_ERROR_MESSAGE    => 'SUCCESS_QUOTE_REV',
                P_ERROR_TYPE       => 40,
                P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.insert_approval_snapshots',
                P_CREATED_BY       => sys_context('USERENV','OS_USER'));
           --get approval users
            BEGIN
                SELECT approval_user_name 
                  INTO l_sales_level_1
                  FROM cct_approval_users cau,
                       cct_snapshot cs 
                 WHERE cs.snapshot_id = cau.snapshot_id AND
                       cs.opportunity_id = P_OPP_ID AND
                       cs.snapshot_version = l_snapshot_ver AND 
                       cau.approval_role_id='CCT_SALES_DIRECTOR' AND 
                       approval_user_name IS NOT NULL;
            EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                l_sales_level_1 := NULL;
            END;
            BEGIN 
                SELECT approval_user_name 
                  INTO l_sales_level_2
                  FROM cct_approval_users cau,
                       cct_snapshot cs 
                 WHERE cs.snapshot_id = cau.snapshot_id AND
                       cs.opportunity_id = P_OPP_ID AND
                       cs.snapshot_version = l_snapshot_ver AND 
                       cau.approval_role_id='CCT_SALES_VP' AND 
                       approval_user_name IS NOT NULL;
            EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                l_sales_level_2 := NULL;
            END;
            BEGIN 
                SELECT approval_user_name 
                  INTO l_grp_vp_general_mgr
                  FROM cct_approval_users cau,
                       cct_snapshot cs 
                 WHERE cs.snapshot_id = cau.snapshot_id AND
                       cs.opportunity_id = P_OPP_ID AND
                       cs.snapshot_version = l_snapshot_ver AND 
                       cau.approval_role_id='CCT_GROUP_VP_AND_GM' AND 
                       approval_user_name IS NOT NULL;
            EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                l_grp_vp_general_mgr := NULL;
            END;
            BEGIN 
                SELECT approval_user_name 
                  INTO l_acc_vice_president
                  FROM cct_approval_users cau,
                       cct_snapshot cs 
                 WHERE cs.snapshot_id = cau.snapshot_id AND
                       cs.opportunity_id = P_OPP_ID AND
                       cs.snapshot_version = l_snapshot_ver AND 
                       cau.approval_role_id='CCT_ACCOUNTING_DIRECTOR' AND 
                       approval_user_name IS NOT NULL;
            EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                l_acc_vice_president := NULL;
            END;
            BEGIN 
                SELECT approval_user_name 
                  INTO l_fin_vice_president
                  FROM cct_approval_users cau,
                       cct_snapshot cs 
                 WHERE cs.snapshot_id = cau.snapshot_id AND
                       cs.opportunity_id = P_OPP_ID AND
                       cs.snapshot_version = l_snapshot_ver AND 
                       cau.approval_role_id='CCT_FINANCE_VP' AND 
                       approval_user_name IS NOT NULL;
            EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                l_fin_vice_president := NULL;
            END;
            BEGIN 
                SELECT approval_user_name 
                  INTO l_op_vice_president
                  FROM cct_approval_users cau,
                       cct_snapshot cs 
                 WHERE cs.snapshot_id = cau.snapshot_id AND
                       cs.opportunity_id = P_OPP_ID AND
                       cs.snapshot_version = l_snapshot_ver AND 
                       cau.approval_role_id='CCT_OPERATIONS_VP' AND 
                       approval_user_name IS NOT NULL;
            EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                l_op_vice_president := NULL;
            END;
            BEGIN 
                SELECT approval_user_name 
                  INTO l_chief_financial_office
                  FROM cct_approval_users cau,
                       cct_snapshot cs 
                 WHERE cs.snapshot_id = cau.snapshot_id AND
                       cs.opportunity_id = P_OPP_ID AND
                       cs.snapshot_version = l_snapshot_ver AND 
                       cau.approval_role_id='CCT_CORPORATE_CFO' AND 
                       approval_user_name IS NOT NULL;
            EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                l_chief_financial_office := NULL;
            END;
            BEGIN 
                SELECT approval_user_name 
                  INTO l_supply_chain_vp
                  FROM cct_approval_users cau,
                       cct_snapshot cs 
                 WHERE cs.snapshot_id = cau.snapshot_id AND
                       cs.opportunity_id = P_OPP_ID AND
                       cs.snapshot_version = l_snapshot_ver AND 
                       cau.approval_role_id='CCT_SUPPLY_CHAIN_VP' AND 
                       approval_user_name IS NOT NULL;
            EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                l_supply_chain_vp := NULL;
            END;
            BEGIN 
                SELECT approval_user_name 
                  INTO l_sr_contracts_director
                  FROM cct_approval_users cau,
                       cct_snapshot cs 
                 WHERE cs.snapshot_id = cau.snapshot_id AND
                       cs.opportunity_id = P_OPP_ID AND
                       cs.snapshot_version = l_snapshot_ver AND 
                       cau.approval_role_id='CCT_SR_CONTRACTS_DIRECTOR' AND 
                       approval_user_name IS NOT NULL;
            EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                l_sr_contracts_director := NULL;
            END;
            --check already in snapshot master
            SELECT count(*)
              INTO l_flag
              FROM CCT_SNAPSHOT_APPROVAL
             WHERE opportunity_id = P_OPP_ID AND
                   snapshot_version = l_snapshot_ver;
            --log
            cct_create_errors.record_error(
                P_OPPORTUNITY_ID   => p_opp_id,
                P_ERROR_MESSAGE    => 'SUCCESS_PRESENT_SNAPAPPR',
                P_ERROR_TYPE       => 50,
                P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.insert_approval_snapshots',
                P_CREATED_BY       => sys_context('USERENV','OS_USER'));
            /**DBMS_OUTPUT.PUT_LINE(l_snapshot_ver);
            DBMS_OUTPUT.PUT_LINE('l_sales_level_1 -' || l_sales_level_1);
            DBMS_OUTPUT.PUT_LINE('l_sales_level_2 -' || l_sales_level_2);
            DBMS_OUTPUT.PUT_LINE('l_grp_vp_general_mgr -' || l_grp_vp_general_mgr);
            DBMS_OUTPUT.PUT_LINE('l_acc_vice_president -' || l_acc_vice_president);
            DBMS_OUTPUT.PUT_LINE('l_fin_vice_president -' || l_fin_vice_president);
            DBMS_OUTPUT.PUT_LINE('l_op_vice_president -' || l_op_vice_president);
            DBMS_OUTPUT.PUT_LINE('l_chief_financial_office -' || l_chief_financial_office);**/
            IF l_flag = 0 THEN
                --delete dummy record
                SELECT count(*)
                  INTO l_dummy_flag
                  FROM CCT_SNAPSHOT_APPROVAL
                 WHERE opportunity_id = P_OPP_ID AND
                       snapshot_version  IS NULL;

                IF l_dummy_flag > 0 THEN
                    DELETE FROM CCT_SNAPSHOT_APPROVAL
                     WHERE opportunity_id = P_OPP_ID AND
                           snapshot_version  IS NULL;
                    COMMIT;
                    --log
                    cct_create_errors.record_error(
                        P_OPPORTUNITY_ID   => p_opp_id,
                        P_ERROR_MESSAGE    => 'SUCCESS_DEL_DUMMY',
                        P_ERROR_TYPE       => 60,
                        P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.insert_approval_snapshots',
                        P_CREATED_BY       => sys_context('USERENV','OS_USER'));
                END IF;

                INSERT INTO CCT_SNAPSHOT_APPROVAL
                    SELECT  co.opportunity_id AS opportunity_id,
                            TO_CHAR(cs.snapshot_version) AS snapshot_version,
							TO_CHAR(cs.created_date,'MM/DD/YYYY') || ' ' || TO_CHAR(cs.created_Date, 'HH24:MI:SS') AS snapshot_date,
							co.opportunity_number AS crf_number,
							TO_CHAR(crfi.sales_team_number) AS sales_team,
							co.project_number AS contract_ref,
							TO_CHAR(co.customer_due_date,'MM/DD/YYYY') || ' ' || TO_CHAR(co.customer_due_date, 'HH24:MI:SS') AS customer_due_date,
							co.customer_name,
							CASE WHEN UPPER(co.gt25) = 'TRUE' THEN 'Y'
								 WHEN UPPER(co.gt25) = 'FALSE'  THEN 'N'
							ELSE 'N' END,
							crfr.contract_type AS contract_type,
							crfr.project_type AS project_type,
							TRIM(TO_CHAR(co.contract_term,'999,999,999,999,999')) AS contract_term,
							co.program_type,
							TRIM(TO_CHAR(cqf.total_product_cost_loc,'999,999,999,999,999')) AS total_product_cost_loc,
							TRIM(TO_CHAR(cqf.total_product_resale_loc,'999,999,999,999,999')) AS total_product_resale_loc,
							TRIM(TO_CHAR(cqf.product_gross_margin,'999,999,999,999,999'))||'%' AS product_gross_margin,
							TRIM(TO_CHAR(cqf.total_adjustments,'999,999,999,999,999')) AS total_adjustments,
							TRIM(TO_CHAR(cqf.project_net_margin,'999,999,999,999,999'))||'%' AS project_net_margin,
							TRIM(TO_CHAR(cp.customer_rebate,'999,999,999,999,999')) AS rebate,
							TRIM(TO_CHAR(cp.total_program_costs,'999,999,999,999,999')) AS total_program_costs,
							TRIM(TO_CHAR(cp.shipping_fee_taxes,'999,999,999,999,999')) AS shipping_fees_taxes,
							TRIM(TO_CHAR(cp.service_fees,'999,999,999,999,999')) AS service_fees,
							CASE WHEN UPPER(co.payment_terms)='OTHER' THEN
								co.other_payment_terms
							ELSE co.payment_terms END,
							CASE WHEN UPPER(co.incoterms)='OTHER' THEN
								co.other_incoterms
							ELSE co.incoterms END,
							co.customer_doc,
							TRIM(TO_CHAR(cp.agent_commissions,'999,999,999,999,999')) AS agent_commissions,
							TO_CHAR(co.line_min_crf) AS line_min,
							TO_CHAR(co.order_minimum) AS order_min,
							co.lol_protection,
							co.cust_liab_protection,
							TRIM(TO_CHAR(co.stocking_strategy_crf,'999,999,999,999,999')) AS stocking_strategy,
							co.liq_damages,
							co.title_transfer,
							co.rights_of_return,
							co.incentives,
							co.cancel_privileges,
							co.bill_hold,
							co.buy_back,
							co.currency,
							TRIM(TO_CHAR(cqf.total_parts_requested,'999,999,999,999,999')) AS total_parts_requested,
							TRIM(TO_CHAR(cqf.total_parts_quoted,'999,999,999,999,999')) AS total_parts_quoted,
							TRIM(TO_CHAR(cqf.product_class_a_item_count,'999,999,999,999,999')) AS product_class_a_item_count,
							TRIM(TO_CHAR(cqf.product_class_a_cost,'999,999,999,999,999')) AS product_class_a_cost,
							TRIM(TO_CHAR(cqf.product_class_a_resale,'999,999,999,999,999')) AS product_class_a_resale,
							TRIM(TO_CHAR(cqf.product_class_a_margin,'999,999,999,999,999'))||'%' AS product_class_a_margin,
							TRIM(TO_CHAR(cqf.product_class_a1a_item_count,'999,999,999,999,999')) AS product_class_a1a_item_count,
							TRIM(TO_CHAR(cqf.product_class_a1a_cost,'999,999,999,999,999')) AS product_class_a1a_cost,
							TRIM(TO_CHAR(cqf.product_class_a1a_resale,'999,999,999,999,999')) AS product_class_a1a_resale,
							TRIM(TO_CHAR(cqf.product_class_a1a_margin,'999,999,999,999,999'))||'%' AS product_class_a1a_margin,
							TRIM(TO_CHAR(cqf.product_class_a1b_item_count,'999,999,999,999,999')) AS product_class_a1b_item_count,
							TRIM(TO_CHAR(cqf.product_class_a1b_cost,'999,999,999,999,999')) AS product_class_a1b_cost,
							TRIM(TO_CHAR(cqf.product_class_a1b_resale,'999,999,999,999,999')) AS product_class_a1b_resale,
							TRIM(TO_CHAR(cqf.product_class_a1b_margin,'999,999,999,999,999'))||'%' AS product_class_a1b_margin,
							TRIM(TO_CHAR(cqf.product_class_b_item_count,'999,999,999,999,999')) AS product_class_b_item_count,
							TRIM(TO_CHAR(cqf.product_class_b_cost,'999,999,999,999,999')) AS product_class_b_cost,
							TRIM(TO_CHAR(cqf.product_class_b_resale,'999,999,999,999,999')) AS product_class_b_resale,
							TRIM(TO_CHAR(cqf.product_class_b_margin,'999,999,999,999,999'))||'%' AS product_class_b_margin,
							TRIM(TO_CHAR(cqf.product_class_c_item_count,'999,999,999,999,999')) AS product_class_c_item_count,
							TRIM(TO_CHAR(cqf.product_class_c_cost,'999,999,999,999,999')) AS product_class_c_cost,
							TRIM(TO_CHAR(cqf.product_class_c_resale,'999,999,999,999,999')) AS product_class_c_resale,
							TRIM(TO_CHAR(cqf.product_class_c_margin,'999,999,999,999,999'))||'%' AS product_class_c_margin,
							TRIM(TO_CHAR(cqf.product_class_d_item_count,'999,999,999,999,999')) AS product_class_d_item_count,
							TRIM(TO_CHAR(cqf.product_class_d_cost,'999,999,999,999,999')) AS product_class_d_cost,
							TRIM(TO_CHAR(cqf.product_class_d_resale,'999,999,999,999,999')) AS product_class_d_resale,
							TRIM(TO_CHAR(cqf.product_class_d_margin,'999,999,999,999,999'))||'%' AS product_class_d_margin,
							l_proposal_mgr_comments,
							'Not Applicable',
							'Not Applicable',
							'Not Applicable',
							'Not Applicable',
							'Not Applicable',
							'Not Applicable',
							'Not Applicable',
							l_approval_status,
							TO_CHAR(co.date_submitted_to_sales,'MM/DD/YYYY') || ' ' || TO_CHAR(co.date_submitted_to_sales, 'HH24:MI:SS') AS date_submitted_to_sales,
							SYSDATE,
							sys_context('USERENV','OS_USER'),
							NULL,
							NULL,
                            NULL,
                            'Not Applicable',
                            'Not Applicable'
					  FROM  cct_snapshot cs,
							cct_opportunity_snapshot co,
							cct_quote_formula_ss_final cqf,
							cct_program_cost_header cp,
							cct_crf crf,
							cct_crf_customer_request crfr,
							cct_crf_customer_info crfi
					 WHERE  cs.opportunity_id = co.opportunity_id AND
							cs.snapshot_id = co.snapshot_id AND
							cp.opportunity_id = co.opportunity_id AND
							cqf.quote_revision_id = cs.quote_revision_id AND
							cqf.snapshot_id = cs.snapshot_id AND
							crf.crf_no = co.opportunity_number AND
							crf.crf_id = crfr.crf_id AND
							crf.crf_id = crfi.crf_id AND
							cs.opportunity_id = P_OPP_ID AND
							cs.snapshot_version = l_snapshot_ver;
                COMMIT;
            ELSE 
                UPDATE cct_snapshot_approval 
                   SET SALES_LEVEL_1 = (CASE WHEN l_sales_level_1 IS NOT NULL THEN
                                             'Not Applicable - ' || l_sales_level_1 
                                        ELSE 'Not Applicable' END),
                       SALES_LEVEL_2 = (CASE WHEN l_sales_level_2 IS NOT NULL THEN
                                             'Not Applicable - ' || l_sales_level_2 
                                        ELSE 'Not Applicable' END),
                       GROUP_VP_GENERAL_MANAGER = (CASE WHEN l_grp_vp_general_mgr IS NOT NULL THEN
                                                        'Not Applicable - ' || l_grp_vp_general_mgr 
                                                   ELSE 'Not Applicable' END),
                       ACCOUNTING_VICE_PRESIDENT = (CASE WHEN l_acc_vice_president IS NOT NULL THEN
                                                        'Not Applicable - ' || l_acc_vice_president 
                                                    ELSE 'Not Applicable' END),
                       FINANCIAL_VICE_PRESIDENT = (CASE WHEN l_fin_vice_president IS NOT NULL THEN
                                                        'Not Applicable - ' || l_fin_vice_president 
                                                   ELSE 'Not Applicable' END),
                       OPERATIONS_VICE_PRESIDENT = (CASE WHEN l_op_vice_president IS NOT NULL THEN
                                                        'Not Applicable - ' || l_op_vice_president 
                                                    ELSE 'Not Applicable' END),
                       CHIEF_FINANCIAL_OFFICE = (CASE WHEN l_chief_financial_office IS NOT NULL THEN
                                                    'Not Applicable - ' || l_chief_financial_office 
                                                 ELSE 'Not Applicable' END), 
                       SUPPLY_CHAIN_VICE_PRESIDENT = (CASE WHEN l_supply_chain_vp IS NOT NULL THEN
                                                    'Not Applicable - ' || l_supply_chain_vp 
                                                 ELSE 'Not Applicable' END),
                       SR_DIRECTOR_OF_CONTRACTS = (CASE WHEN l_sr_contracts_director IS NOT NULL THEN
                                                    'Not Applicable - ' || l_sr_contracts_director 
                                                 ELSE 'Not Applicable' END)
                 WHERE opportunity_id = P_OPP_ID AND
                       snapshot_version = l_snapshot_ver;
                COMMIT;
            END IF;

        END LOOP;
        CLOSE cur_snapshot_list;
    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            P_OPPORTUNITY_ID   => p_opp_id,
            P_ERROR_MESSAGE    => g_msg,
            P_ERROR_TYPE       => g_code,
            P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.insert_approval_snapshots',
            P_CREATED_BY       => sys_context('USERENV','OS_USER'));
    END insert_approval_snapshots;

    PROCEDURE insert_dummy_snapshot(
        P_OPP_ID               IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE)
    AS
        l_flag                    NUMBER;
    BEGIN

        SELECT count(*)
          INTO l_flag
          FROM CCT_SNAPSHOT_APPROVAL
         WHERE opportunity_id = P_OPP_ID;

        IF l_flag=0 THEN
            INSERT INTO CCT_SNAPSHOT_APPROVAL VALUES (
                P_OPP_ID, null, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', SYSDATE,
                sys_context('USERENV','OS_USER'), NULL, ' ', ' ', ' ',' ');
            COMMIT;
            --log
            cct_create_errors.record_error(
                P_OPPORTUNITY_ID   => p_opp_id,
                P_ERROR_MESSAGE    => 'SUCCESS_DUMMY_REC',
                P_ERROR_TYPE       => 10,
                P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.insert_dummy_snapshot',
                P_CREATED_BY       => sys_context('USERENV','OS_USER'));
        END IF;

    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            P_OPPORTUNITY_ID   => p_opp_id,
            P_ERROR_MESSAGE    => g_msg,
            P_ERROR_TYPE       => g_code,
            P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.insert_dummy_snapshot',
            P_CREATED_BY       => sys_context('USERENV','OS_USER'));
    END insert_dummy_snapshot;

    PROCEDURE update_snapshot_master(
        P_OPP_ID               IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE)
    AS
        l_proposal_mgr_comments   CCT_QUOTE_APPROVALS.PROPOSAL_MANAGER_COMMENTS%TYPE;
        l_approval_status         CCT_QUOTE_APPROVALS.APPROVAL_STATUS%TYPE := 'NOT INITIATED';
        l_date_submitted_to_sales VARCHAR2(500);
        l_count                   NUMBER;
        l_snapshot_version        CCT_SNAPSHOT.SNAPSHOT_VERSION%TYPE;
    BEGIN

        SELECT count(*)
          INTO l_count
          FROM cct_snapshot_approval
         WHERE opportunity_id = P_OPP_ID AND
               snapshot_version IS NOT NULL;

        IF l_count > 0 THEN
            --SELECT MAX(snapshot_version)
            select MAX(TO_NUMBER(snapshot_version))|| '.0'
              INTO l_snapshot_version
              FROM cct_snapshot_approval
             WHERE opportunity_id = P_OPP_ID AND
                   snapshot_version IS NOT NULL;
            --log
            cct_create_errors.record_error(
                P_OPPORTUNITY_ID   => p_opp_id,
                P_ERROR_MESSAGE    => 'SUCCESS_SNAP_VER',
                P_ERROR_TYPE       => 10,
                P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.update_snapshot_master',
                P_CREATED_BY       => sys_context('USERENV','OS_USER'));
            --get proposal manager comments
            cct_fetch_fields.get_proposal_comments(
                P_OPPORTUNITY_ID => P_OPP_ID,
                P_SNAPSHOT_VERSION => l_snapshot_version,
                P_PROPOSAL_COMMENTS => l_proposal_mgr_comments);
            --log
            cct_create_errors.record_error(
                P_OPPORTUNITY_ID   => p_opp_id,
                P_ERROR_MESSAGE    => 'SUCCESS_PRP_MGR',
                P_ERROR_TYPE       => 20,
                P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.update_snapshot_master',
                P_CREATED_BY       => sys_context('USERENV','OS_USER'));
            --get approval status
            SELECT DISTINCT NVL(approval_status,'NOT INITIATED')
              INTO l_approval_status
              FROM (SELECT cq2.approval_status,
                           cq2.opportunity_id,
                           cq2.snapshot_id
                      FROM (SELECT MAX(approval_id) AS approval_id,
                                   snapshot_id
                              FROM cct_quote_approvals
                            group by snapshot_id) cq1,
                            cct_quote_approvals cq2
                     WHERE cq1.approval_id = cq2.approval_id AND
                           cq1.snapshot_id = cq2.snapshot_id) cq RIGHT JOIN
                   cct_snapshot cs
                ON cq.snapshot_id = cs.snapshot_id AND
                   cs.opportunity_id = cq.opportunity_id
             WHERE cs.opportunity_id = P_OPP_ID AND
                   cs.snapshot_version = l_snapshot_version;
            --log
            cct_create_errors.record_error(
                P_OPPORTUNITY_ID   => p_opp_id,
                P_ERROR_MESSAGE    => 'SUCCESS_APPR_STATUS',
                P_ERROR_TYPE       => 30,
                P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.update_snapshot_master',
                P_CREATED_BY       => sys_context('USERENV','OS_USER'));
            --get date submitted to sales
            SELECT  TO_CHAR(co.date_submitted_to_sales,'MM/DD/YYYY') || ' ' || TO_CHAR(co.date_submitted_to_sales, 'HH24:MI:SS')
              INTO  l_date_submitted_to_sales
              FROM  cct_opportunity_snapshot co,
                    cct_snapshot cs
             WHERE  cs.opportunity_id = P_OPP_ID AND
                    cs.snapshot_version = l_snapshot_version AND
                    cs.snapshot_id = co.snapshot_id;
            --log
            cct_create_errors.record_error(
                P_OPPORTUNITY_ID   => p_opp_id,
                P_ERROR_MESSAGE    => 'SUCCESS_DATE_SUBM',
                P_ERROR_TYPE       => 40,
                P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.update_snapshot_master',
                P_CREATED_BY       => sys_context('USERENV','OS_USER'));
            --update master table
            UPDATE  CCT_SNAPSHOT_APPROVAL
               SET  PROPOSAL_MANAGER_COMMENTS = NVL(l_proposal_mgr_comments,' '),
                    APPROVAL_STATUS = NVL(l_approval_status,' '),
                    DATE_SUBMITTED_TO_SALES = l_date_submitted_to_sales,
                    LAST_UPDATED_DATE = SYSDATE,
                    LAST_UPDATED_BY = sys_context('USERENV','OS_USER')
             WHERE  OPPORTUNITY_ID = P_OPP_ID AND
                    SNAPSHOT_VERSION = l_snapshot_version;
            COMMIT;
            --log
            cct_create_errors.record_error(
                P_OPPORTUNITY_ID   => p_opp_id,
                P_ERROR_MESSAGE    => 'SUCCESS_UPDATE',
                P_ERROR_TYPE       => 50,
                P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.update_snapshot_master',
                P_CREATED_BY       => sys_context('USERENV','OS_USER'));
        END IF;
    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            P_OPPORTUNITY_ID   => p_opp_id,
            P_ERROR_MESSAGE    => g_msg,
            P_ERROR_TYPE       => g_code,
            P_MODULE_NAME      => 'CCT_APPROVAL_PROCESS.update_snapshot_master',
            P_CREATED_BY       => sys_context('USERENV','OS_USER'));
    END update_snapshot_master;

END CCT_APPROVAL_PROCESS;