create or replace PACKAGE BODY CCT_CREATE_OPPORTUNITY AS
/**Handles CCT_OPPORTUNITY_TBL loading
  Modification History
  PERSON        DATE         COMMENTS
  ------------  -----------  ---------------------------------------------
  Sureka        06-Jul-2017  Created insert_opportunity_details proc
  Sureka        13-Jul-2017  Modified insert_opportunity_details proc
  Sureka        13-Jul-2017  Created update_opportunity_details proc
  Ankit Bhatt   28-Jul-2017  Added  quote_revision_id and quote_revision out parameters
                             in create_quote_revisions procedure.
  Ankit Bhatt   08-Aug-2017  Created new procedure calculate_quote_parameters
  Ankit Bhatt   16-Aug-2017  Added additional out paramters to calculate_quote_parameters procedure
  Sureka        22-Aug-2017  Modified insert_opportunity_details proc
  Sureka        22-Aug-2017  Modified update_opportunity_details proc
  Ankit Bhatt   01-Sep-2017  New procedure to update Net margin and Amount fields
  Ankit Bhatt   02-Sep-2017  Modified insert_opportunity_details for inserting the records into
                             cct_program_cost_header and cct_program_cost_lines
  Ankit Bhatt   27-Sep-2017  modified insert_opportunity_details procedure to insert category_subtype_id
                             into CCT_PROGRAM_COST_LINES table
  Ankit Bhatt   10-Oct-2017  Modified date format
  Sureka        17-Oct-2017  Modified insert_opportunity_details proc
  Sureka        17-Oct-2017  Modified update_opportunity_details proc
  Ankit Bhatt   17-Oct-2017  Modified insert_opportunity_details proc. to populate category_subtype_name
                             into CCT_PROGRAM_COST_LINES table
  Ankit Bhatt   06-Nov-2017  Modified procedure update_formula_fields with cct_quote_parts_final
  Sureka        17-Oct-2017  Modified insert_opportunity_details proc
  Sureka        07-Dec-2017  Created submit_to_sales
  Ankit         28-Dec-2017  Modified total adjustment calculations in procedure update_formula_fields
  Sureka        04-Jan-2018  Modified insert_opportunity_details proc
  Sureka        16-Jan-2018  Created map_to_opportunity proc
  Sureka        16-Jan-2018  Created update_to_opportunity proc
  Sureka        20-Feb-2017  Modified update_opportunity_details proc for D-32
  Sureka        23-Feb-2017  Modified submit_to_sales proc CRF_STATUS
  Ankit Bhatt   10-Oct-2018  Modified update_formula_fields proc to change Total Adjustments(CHG0036547) 
**/

    PROCEDURE insert_opportunity_details(
        P_OPP_OBJ                IN CCT_OPPORTUNITY_TYPE,
        P_OPPORTUNITY_TYPE       IN VARCHAR2,
        P_RETURN_STATUS         OUT NOCOPY VARCHAR2,
        P_CUST_DUE_DATE_FLAG    OUT NOCOPY VARCHAR2,
        P_OLD_CUST_DUE_DATE     OUT NOCOPY DATE)
    AS
        l_quote_number              cct_quote.quote_number%TYPE;
        l_quote_revision            cct_quote_revisions.quote_revision%TYPE;
        l_revision_id               cct_quote_revisions.revision_id%TYPE;
        l_cust_due_date             cct_opportunity.customer_due_date%TYPE;
        l_old_cust_due_date         cct_opportunity.customer_due_date%TYPE;
        l_pgm_cost_hdr_id           cct_program_cost_header.program_cost_header_id%TYPE;
        l_opportunity_id            cct_opportunity.opportunity_id%TYPE;
        l_pgm_cost_line_seq         NUMBER;
        l_pgm_hdr_seq               NUMBER;
        l_cat_type_1                NUMBER := 1;
        l_cat_type_2                NUMBER := 2;
        l_cat_type_3                NUMBER := 3;
        l_cat_type_4                NUMBER := 4;
        l_cat_type_5                NUMBER := 5;
    BEGIN
        g_osc_opp_id         := P_OPP_OBJ.OSC_OPPORTUNITY_ID;
        g_created_by         := P_OPP_OBJ.CREATED_BY;
        g_opportunity_number := P_OPP_OBJ.OPPORTUNITY_NUMBER;

        SELECT COUNT(*)
          INTO g_count
          FROM cct_opportunity
         WHERE (osc_opportunity_id = g_osc_opp_id AND
               opportunity_number = g_opportunity_number) OR
               opportunity_number = g_opportunity_number;

        IF g_count = 0 THEN
            INSERT INTO cct_opportunity (
                osc_opportunity_id,
                opportunity_number,
                opportunity_name,
                customer_number,
                customer_name,
                currency,
                sales_method,
                status,
                customer_due_date,
                contract_term,
                decimal_to_quote,
                faa_certification,
                dfar_apply,
                customer_wh,
                alt_pricing_cust_num,
                cust_appr_sources,
                project_number,
                special_instruction,
                quote_basis,
                deal_type,
                deal_structure,
                source_products,
                customer_rebate,
                rebate,
                payment_terms,
                incoterms,
                agent_commissions,
                line_min_provision,
                lol_protection,
                cust_liab_protection,
                stocking_strategy,
                penalties,
                title_transfer,
                rights_of_return,
                incentives,
                cancel_privileges,
                bill_hold,
                buy_back,
                gt25,
                total_prod_cost,
                total_prod_resale,
                prod_grs_margin,
                proj_net_margin,
                bitem_liability,
                customer_doc,
                total_prod_requested,
                total_prod_quoted,
                total_adjustments,
                total_program_costs,
                shipping_fees,
                shipping_taxes,
                service_fees,
                liq_damages,
                target_percentage,
                target_date,
                contract_team,
                user_name,
                oname,
                emailid,
                user_group,
                role,
                manager,
                sales_rep,
                proposal_manager,
                pricing_manager,
                cust_liab_on_invest,
                ret_fmt_proposal,
                amount,
                program_type,
                expected_revenue,
                primary_contact,
                primary_competitor,
                fwd_stock_location,
                line_minimum,
                ult_destination_of_parts,
                description,
                ret_fmt_pricing_file,
                can_use_min_ord_qty,
                sales_stage,
                recover_shipping_cost,
                tso_pma_certification,
                win_loss_reason,
                platform_name,
                cust_ref_number,
                win_probability,
                order_minimum,
                team_number,
                existing_contract_num,
                fwd_stock_loc_req,
                close_date,
                recover_prog_cost,
                creation_date,
                created_by,
                last_updated_date,
                last_updated_by,
                other_payment_terms,
                other_incoterms,
                other_cust_approved_sources,
                terms_note,
                project_num_blank,
                sales_project_num_blank,
                award_file_upload,
                doc_rdy_to_submit,
                sales_owner_email,
                is_cust_rebate_requested,
                requested_stocking_strategy,
                customer_no_crf,
                stocking_strategy_crf,
                service_fees_crf,
                line_min_crf,
                contract_type,
                project_type
            ) VALUES (
                P_OPP_OBJ.OSC_OPPORTUNITY_ID,
                P_OPP_OBJ.OPPORTUNITY_NUMBER,
                P_OPP_OBJ.OPPORTUNITY_NAME,
                P_OPP_OBJ.CUSTOMER_NO_CRF,
                P_OPP_OBJ.CUSTOMER_NAME,
                P_OPP_OBJ.CURRENCY,
                P_OPP_OBJ.SALES_METHOD,
                P_OPP_OBJ.STATUS,
                P_OPP_OBJ.CUSTOMER_DUE_DATE,
                P_OPP_OBJ.CONTRACT_TERM,
                P_OPP_OBJ.DECIMAL_TO_QUOTE,
                P_OPP_OBJ.FAA_CERTIFICATION,
                P_OPP_OBJ.DFAR_APPLY,
                P_OPP_OBJ.CUSTOMER_WH,
                P_OPP_OBJ.ALT_PRICING_CUST_NUM,
                P_OPP_OBJ.CUST_APPR_SOURCES,
                P_OPP_OBJ.PROJECT_NUMBER,
                P_OPP_OBJ.SPECIAL_INSTRUCTION,
                P_OPP_OBJ.QUOTE_BASIS,
                P_OPP_OBJ.DEAL_TYPE,
                P_OPP_OBJ.DEAL_STRUCTURE,
                P_OPP_OBJ.SOURCE_PRODUCTS,
                P_OPP_OBJ.REBATE,
                P_OPP_OBJ.REBATE_CRF,
                P_OPP_OBJ.PAYMENT_TERMS,
                P_OPP_OBJ.INCOTERMS,
                P_OPP_OBJ.AGENT_COMMISSIONS,
                P_OPP_OBJ.LINE_MIN_PROVISION,
                P_OPP_OBJ.LOL_PROTECTION,
                P_OPP_OBJ.CUST_LIAB_PROTECTION,
                P_OPP_OBJ.STOCKING_STRATEGY,
                P_OPP_OBJ.PENALTIES,
                P_OPP_OBJ.TITLE_TRANSFER,
                P_OPP_OBJ.RIGHTS_OF_RETURN,
                P_OPP_OBJ.INCENTIVES,
                P_OPP_OBJ.CANCEL_PRIVILEGES,
                P_OPP_OBJ.BILL_HOLD,
                P_OPP_OBJ.BUY_BACK,
                P_OPP_OBJ.GT25,
                P_OPP_OBJ.TOTAL_PROD_COST,
                P_OPP_OBJ.TOTAL_PROD_RESALE,
                P_OPP_OBJ.PROD_GRS_MARGIN,
                P_OPP_OBJ.PROJ_NET_MARGIN,
                P_OPP_OBJ.BITEM_LIABILITY,
                P_OPP_OBJ.CUSTOMER_DOC,
                P_OPP_OBJ.TOTAL_PROD_REQUESTED,
                P_OPP_OBJ.TOTAL_PROD_QUOTED,
                P_OPP_OBJ.TOTAL_ADJUSTMENTS,
                P_OPP_OBJ.TOTAL_PROGRAM_COSTS,
                P_OPP_OBJ.SHIPPING_FEES,
                P_OPP_OBJ.SHIPPING_TAXES,
                P_OPP_OBJ.SERVICE_FEES,
                P_OPP_OBJ.LIQ_DAMAGES,
                P_OPP_OBJ.TARGET_PERCENTAGE,
                P_OPP_OBJ.TARGET_DATE,
                P_OPP_OBJ.CONTRACT_TEAM,
                P_OPP_OBJ.USER_NAME,
                P_OPP_OBJ.ONAME,
                P_OPP_OBJ.EMAILID,
                P_OPP_OBJ.USER_GROUP,
                P_OPP_OBJ.ROLE,
                P_OPP_OBJ.MANAGER,
                P_OPP_OBJ.SALES_REP,
                P_OPP_OBJ.PROPOSAL_MANAGER,
                P_OPP_OBJ.PRICING_MANAGER,
                P_OPP_OBJ.CUST_LIAB_ON_INVEST,
                P_OPP_OBJ.RET_FMT_PROPOSAL,
                P_OPP_OBJ.AMOUNT,
                P_OPP_OBJ.PROGRAM_TYPE,
                P_OPP_OBJ.EXPECTED_REVENUE,
                P_OPP_OBJ.PRIMARY_CONTACT,
                P_OPP_OBJ.PRIMARY_COMPETITOR,
                P_OPP_OBJ.FWD_STOCK_LOCATION,
                P_OPP_OBJ.LINE_MINIMUM,
                P_OPP_OBJ.ULT_DESTINATION_OF_PARTS,
                P_OPP_OBJ.DESCRIPTION,
                P_OPP_OBJ.RET_FMT_PRICING_FILE,
                P_OPP_OBJ.CAN_USE_MIN_ORD_QTY,
                P_OPP_OBJ.SALES_STAGE,
                P_OPP_OBJ.RECOVER_SHIPPING_COST,
                P_OPP_OBJ.TSO_PMA_CERTIFICATION,
                P_OPP_OBJ.WIN_LOSS_REASON,
                P_OPP_OBJ.PLATFORM_NAME,
                P_OPP_OBJ.CUST_REF_NUMBER,
                P_OPP_OBJ.WIN_PROBABILITY,
                P_OPP_OBJ.ORDER_MINIMUM,
                P_OPP_OBJ.TEAM_NUMBER,
                P_OPP_OBJ.EXISTING_CONTRACT_NUM,
                P_OPP_OBJ.FWD_STOCK_LOC_REQ,
                P_OPP_OBJ.CLOSE_DATE,
                P_OPP_OBJ.RECOVER_PROG_COST,
                P_OPP_OBJ.CREATION_DATE,
                P_OPP_OBJ.CREATED_BY,
                P_OPP_OBJ.LAST_UPDATED_DATE,
                P_OPP_OBJ.LAST_UPDATED_BY,
                P_OPP_OBJ.OTHER_PAYMENT_TERMS,
                P_OPP_OBJ.OTHER_INCOTERMS,
                P_OPP_OBJ.OTHER_CUST_APPROVED_SOURCES,
                P_OPP_OBJ.TERMS_NOTE,
                P_OPP_OBJ.PROJECT_NUM_BLANK,
                P_OPP_OBJ.SALES_PROJECT_NUM_BLANK,
                P_OPP_OBJ.AWARD_FILE_UPLOAD,
                P_OPP_OBJ.DOC_RDY_TO_SUBMIT,
                P_OPP_OBJ.SALES_OWNER_EMAIL,
                P_OPP_OBJ.IS_CUST_REBATE_REQUESTED,
                P_OPP_OBJ.REQUESTED_STOCKING_STRATEGY,
                P_OPP_OBJ.CUSTOMER_NO_CRF,
                P_OPP_OBJ.STOCKING_STRATEGY_CRF,
                P_OPP_OBJ.SERVICE_FEES_CRF,
                P_OPP_OBJ.LINE_MIN_CRF,
                P_OPP_OBJ.CONTRACT_TYPE,
                P_OPP_OBJ.PROJECT_TYPE
            );
            COMMIT;

            BEGIN
                SELECT co.opportunity_id
                  INTO l_opportunity_id
                  FROM cct_opportunity co
                 WHERE (co.osc_opportunity_id = P_OPP_OBJ.OSC_OPPORTUNITY_ID AND
                        co.opportunity_number = P_OPP_OBJ.OPPORTUNITY_NUMBER) OR
                       co.opportunity_number = P_OPP_OBJ.OPPORTUNITY_NUMBER ;
            EXCEPTION
            WHEN OTHERS THEN
                g_code := sqlcode;
                g_msg := substr(sqlerrm,1,500);
                cct_create_errors.record_error(
                    p_opportunity_id   => g_opportunity_id,
                    p_error_message    => g_msg,
                    p_error_type       => g_code,
                    p_module_name      => 'CCT_CREATE_OPPORTUNITY.insert_opportunity_details',
                    p_created_by       => g_created_by
                );
            END;

            BEGIN
               INSERT INTO xxdms.dms_opportunity(
               opportunity_id,
               opportunity_number,
               opportunity_name,
               osc_opportunity_id,
               CREATED_DATE,
               UPDATED_DATE
               )
               VALUES (
                l_opportunity_id,
                P_OPP_OBJ.OPPORTUNITY_NUMBER,
                P_OPP_OBJ.CUSTOMER_NAME,
                l_opportunity_id,
                SYSDATE,
                SYSDATE);
                COMMIT;
            EXCEPTION
               WHEN OTHERS THEN 
                  NULL;
            END;
            
            BEGIN
                SELECT cct_program_cost_hdr_seq.NEXTVAL
                  INTO l_pgm_hdr_seq
                  FROM dual;
            EXCEPTION
            WHEN OTHERS THEN
                g_code := sqlcode;
                g_msg := substr(sqlerrm,1,500);
                cct_create_errors.record_error(
                    p_opportunity_id   => g_opportunity_id,
                    p_error_message    => g_msg,
                    p_error_type       => g_code,
                    p_module_name      => 'CCT_CREATE_OPPORTUNITY.insert_opportunity_details',
                    p_created_by       => g_created_by
                );
            END;

            INSERT INTO cct_program_cost_header (
                opportunity_id,
                program_cost_header_id,
                locations,
                director_or_manager,
                one_time_pgm_setup_cost,
                pgm_costs_year_1,
                pgm_costs_year_2,
                pgm_costs_year_3,
                annual_rec_pgm_costs,
                total_program_costs,
                notes,
                service_fees,
                agent_commissions,
                shipping_fee_taxes,
                creation_date,
                created_by,
                last_updated_date,
                last_updated_by)
            VALUES (l_opportunity_id,
                l_pgm_hdr_seq,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                SYSDATE,
                user,
                SYSDATE,
                user
            );
            COMMIT;

            INSERT INTO cct_program_cost_lines (
                program_cost_header_id,
                program_cost_line_id,
                category_type_id,
				category_subtype_name,
                category_subtype_id,
                category,
                fixed_or_recurring,
                unit_cost,
                quantity,
                no_of_individuals,
                pct_of_workload,
                annual_personnel_cost,
                subtotal,
                notes,
                creation_date,
                created_by,
                last_updated_date,
                last_updated_by
            ) SELECT l_pgm_hdr_seq,
                     cct_program_cost_line_seq.NEXTVAL,
                     l_cat_type_1,
					 cpccs.category_subtype_name,
                     NULL,
                     NULL,
                     NULL,
                     NULL,
                     NULL,
                     NULL,
                     NULL,
                     NULL,
                     NULL,
                     NULL,
                     SYSDATE,
                     user,
                     SYSDATE,
                     user
                FROM cct_program_cost_cat_subtypes cpccs
               WHERE cpccs.category_type_id = l_cat_type_1;
            COMMIT;

            INSERT INTO cct_program_cost_lines (
                program_cost_header_id,
                program_cost_line_id,
                category_type_id,
				category_subtype_name,
                category_subtype_id,
                category,
                fixed_or_recurring,
                unit_cost,
                quantity,
                no_of_individuals,
                pct_of_workload,
                annual_personnel_cost,
                subtotal,
                notes,
                creation_date,
                created_by,
                last_updated_date,
                last_updated_by
            ) SELECT
                l_pgm_hdr_seq,
                cct_program_cost_line_seq.NEXTVAL,
                l_cat_type_2,
				cpccs.category_subtype_name,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                SYSDATE,
                user,
                SYSDATE,
                user
             FROM cct_program_cost_cat_subtypes cpccs
            WHERE cpccs.category_type_id = l_cat_type_2;
            COMMIT;

            INSERT INTO cct_program_cost_lines (
                program_cost_header_id,
                program_cost_line_id,
                category_type_id,
				category_subtype_name,
                category_subtype_id,
                category,
                fixed_or_recurring,
                unit_cost,
                quantity,
                no_of_individuals,
                pct_of_workload,
                annual_personnel_cost,
                subtotal,
                notes,
                creation_date,
                created_by,
                last_updated_date,
                last_updated_by
            ) SELECT
                l_pgm_hdr_seq,
                cct_program_cost_line_seq.NEXTVAL,
                l_cat_type_3,
				cpccs.category_subtype_name,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                SYSDATE,
                user,
                SYSDATE,
                user
             FROM cct_program_cost_cat_subtypes cpccs
            WHERE cpccs.category_type_id = l_cat_type_3;
            COMMIT;

            INSERT INTO cct_program_cost_lines (
                program_cost_header_id,
                program_cost_line_id,
                category_type_id,
				category_subtype_name,
                category_subtype_id,
                category,
                fixed_or_recurring,
                unit_cost,
                quantity,
                no_of_individuals,
                pct_of_workload,
                annual_personnel_cost,
                subtotal,
                notes,
                creation_date,
                created_by,
                last_updated_date,
                last_updated_by
            ) SELECT
                l_pgm_hdr_seq,
                cct_program_cost_line_seq.NEXTVAL,
                l_cat_type_4,
				cpccs.category_subtype_name,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                SYSDATE,
                user,
                SYSDATE,
                user
             FROM cct_program_cost_cat_subtypes cpccs
            WHERE cpccs.category_type_id = l_cat_type_4;
            COMMIT;

            INSERT INTO cct_program_cost_lines (
                program_cost_header_id,
                program_cost_line_id,
                category_type_id,
				category_subtype_name,
                category_subtype_id,
                category,
                fixed_or_recurring,
                unit_cost,
                quantity,
                no_of_individuals,
                pct_of_workload,
                annual_personnel_cost,
                subtotal,
                notes,
                creation_date,
                created_by,
                last_updated_date,
                last_updated_by
            ) SELECT
                l_pgm_hdr_seq,
                cct_program_cost_line_seq.NEXTVAL,
                l_cat_type_5,
				cpccs.category_subtype_name,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                SYSDATE,
                user,
                SYSDATE,
                user
             FROM cct_program_cost_cat_subtypes cpccs
            WHERE cpccs.category_type_id = l_cat_type_5;
            COMMIT;

            INSERT INTO cct_program_cost_cat_summary (
                program_cost_header_id,
                category_type_id,
                fixed_sub_total,
                recurring_sub_total,
                total_personnel_expense,
                notes,
                creation_date,
                created_by,
                last_updated_date,
                last_updated_by)
            SELECT l_pgm_hdr_seq,
                cpccs.category_type_id,
                NULL,
                NULL,
                NULL,
                NULL,
                SYSDATE,
                user,
                SYSDATE,
                user
             FROM cct_program_cost_cat_types cpccs;
            COMMIT;

            INSERT INTO cct_crf_date_management (
                crf_id,
                opportunity_creation_date,
                cust_req_kickoff_date,
                offer_approval_sent_date,
                offer_approval_recd_date,
                offer_sent_to_cust_date,
                revision_creation_date,
                award_creation_date,
                created_by,
                updated_by,
                creation_date,
                update_date)
            SELECT crf.crf_id,
                co.creation_date,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                sys_context('USERENV','OS_USER'),
                NULL,
                SYSDATE,
                NULL
             FROM cct_crf crf,
                  cct_opportunity co
            WHERE crf.crf_no = co.opportunity_number AND
                  co.opportunity_number = g_opportunity_number;
            COMMIT;

            INSERT INTO cct_crf_award_dates (
                crf_id,
                decision_date,
                win_reason,
                loss_to_name,
                signature_date,
                recieved_date,
                pm_confirmation_date,
                effective_date,
                expiry_date,
                flow_down_date,
                loaded_in_71_date,
                ip_completion_date,
                completeness_confirm_date,
                implementation_closed_date,
                creation_date,
                created_by,
                update_date,
                updated_by)
            SELECT crf.crf_id,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                SYSDATE,
                sys_context('USERENV','OS_USER'),
                NULL,
                NULL
             FROM cct_crf crf,
                  cct_opportunity co
            WHERE crf.crf_no = co.opportunity_number AND
                  co.opportunity_number = g_opportunity_number;
            COMMIT;

            P_RETURN_STATUS := 'SUCCESS';
            P_CUST_DUE_DATE_FLAG := 'FALSE';

            IF UPPER(P_OPPORTUNITY_TYPE) = 'NEW' THEN
                cct_quote_creation.create_quote(
                    g_opportunity_number,
                    l_quote_number,
                    l_quote_revision,
                    l_revision_id
                );
            END IF;

        ELSE
            BEGIN
                SELECT trunc(customer_due_date)
                  INTO l_old_cust_due_date
                  FROM cct_opportunity
                 WHERE (osc_opportunity_id = g_osc_opp_id AND
                       opportunity_number = g_opportunity_number) OR
                       opportunity_number = g_opportunity_number;
            EXCEPTION
            WHEN OTHERS THEN
                g_code := sqlcode;
                g_msg := substr(sqlerrm,1,500);
                cct_create_errors.record_error(
                    p_opportunity_id   => g_opportunity_id,
                    p_error_message    => g_msg,
                    p_error_type       => g_code,
                    p_module_name      => 'CCT_CREATE_OPPORTUNITY.insert_opportunity_details',
                    p_created_by       => g_created_by
                );
            END;

            l_cust_due_date := P_OPP_OBJ.CUSTOMER_DUE_DATE;
            IF l_cust_due_date = l_old_cust_due_date THEN
                P_CUST_DUE_DATE_FLAG := 'FALSE';
            ELSE
                P_CUST_DUE_DATE_FLAG := 'TRUE';
            END IF;
            P_OLD_CUST_DUE_DATE := l_old_cust_due_date;

            update_opportunity_details(
                P_OPP_OBJ,
                P_RETURN_STATUS
            );

            IF UPPER(P_OPPORTUNITY_TYPE) = 'REVISE' THEN
                cct_quote_creation.create_quote_revisions(
                    g_opportunity_number,
                    l_revision_id,
                    l_quote_revision
                );
            END IF;
        END IF;

    EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
        P_RETURN_STATUS := 'FAILURE';
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,500);
        cct_create_errors.record_error(
            p_opportunity_id   => g_opportunity_id,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_CREATE_OPPORTUNITY.insert_opportunity_details',
            p_created_by       => g_created_by
        );
    END insert_opportunity_details;

    PROCEDURE update_opportunity_details(
        P_OPP_OBJ                IN CCT_OPPORTUNITY_TYPE,
        P_RETURN_STATUS         OUT NOCOPY VARCHAR2)
    AS 
        --D-32 Added
        l_created_by                CCT_OPPORTUNITY.CREATED_BY%TYPE;
        l_opportunity_number        CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE;
        l_opportunity_id            CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE;
    BEGIN
        /**D-32 Commented 
        IF g_opportunity_number IS NULL THEN
            g_opportunity_number := P_OPP_OBJ.OPPORTUNITY_NUMBER;
        END IF;

        IF g_osc_opp_id IS NULL THEN
            CCT_FETCH_FIELDS.GET_OSC_OPTY_ID(g_opportunity_number,g_osc_opp_id);
        END IF;**/
        --D-32 Added
        l_created_by := sys_context('USERENV','OS_USER');
        l_opportunity_number := P_OPP_OBJ.OPPORTUNITY_NUMBER;

        UPDATE cct_opportunity
            SET opportunity_name = NVL(P_OPP_OBJ.OPPORTUNITY_NAME,opportunity_name),
                customer_number = NVL(P_OPP_OBJ.CUSTOMER_NO_CRF,customer_number),
                customer_name = NVL(P_OPP_OBJ.CUSTOMER_NAME,customer_name),
                currency = NVL(P_OPP_OBJ.CURRENCY,currency),
                sales_method = NVL(P_OPP_OBJ.SALES_METHOD,sales_method),
                status = NVL(P_OPP_OBJ.STATUS,status),
                customer_due_date = NVL(P_OPP_OBJ.CUSTOMER_DUE_DATE,customer_due_date),
                contract_term = NVL(P_OPP_OBJ.CONTRACT_TERM,contract_term),
                decimal_to_quote = NVL(P_OPP_OBJ.DECIMAL_TO_QUOTE,decimal_to_quote),
                faa_certification = NVL(P_OPP_OBJ.FAA_CERTIFICATION,faa_certification),
                dfar_apply = NVL(P_OPP_OBJ.DFAR_APPLY,dfar_apply),
                customer_wh = NVL(P_OPP_OBJ.CUSTOMER_WH,customer_wh),
                alt_pricing_cust_num = NVL(P_OPP_OBJ.ALT_PRICING_CUST_NUM,alt_pricing_cust_num),
                cust_appr_sources = NVL(P_OPP_OBJ.CUST_APPR_SOURCES,cust_appr_sources),
                project_number = NVL(P_OPP_OBJ.PROJECT_NUMBER,project_number),
                special_instruction = NVL(P_OPP_OBJ.SPECIAL_INSTRUCTION,special_instruction),
                quote_basis = NVL(P_OPP_OBJ.QUOTE_BASIS,quote_basis),
                deal_type = NVL(P_OPP_OBJ.DEAL_TYPE,deal_type),
                deal_structure = NVL(P_OPP_OBJ.DEAL_STRUCTURE,deal_structure),
                source_products = NVL(P_OPP_OBJ.SOURCE_PRODUCTS,source_products),
                customer_rebate = NVL(P_OPP_OBJ.REBATE,customer_rebate),
				rebate = NVL(P_OPP_OBJ.REBATE_CRF,rebate),
                payment_terms = NVL(P_OPP_OBJ.PAYMENT_TERMS,payment_terms),
                incoterms = NVL(P_OPP_OBJ.INCOTERMS,incoterms),
                agent_commissions = NVL(P_OPP_OBJ.AGENT_COMMISSIONS,agent_commissions),
                line_min_provision = NVL(P_OPP_OBJ.LINE_MIN_PROVISION,line_min_provision),
                lol_protection = NVL(P_OPP_OBJ.LOL_PROTECTION,lol_protection),
                cust_liab_protection = NVL(P_OPP_OBJ.CUST_LIAB_PROTECTION,cust_liab_protection),
                stocking_strategy = NVL(P_OPP_OBJ.STOCKING_STRATEGY,stocking_strategy),
                penalties = NVL(P_OPP_OBJ.PENALTIES,penalties),
                title_transfer = NVL(P_OPP_OBJ.TITLE_TRANSFER,title_transfer),
                rights_of_return = NVL(P_OPP_OBJ.RIGHTS_OF_RETURN,rights_of_return),
                incentives = NVL(P_OPP_OBJ.INCENTIVES,incentives),
                cancel_privileges = NVL(P_OPP_OBJ.CANCEL_PRIVILEGES,cancel_privileges),
                bill_hold = NVL(P_OPP_OBJ.BILL_HOLD,bill_hold),
                buy_back = NVL(P_OPP_OBJ.BUY_BACK,buy_back),
                gt25 = NVL(P_OPP_OBJ.GT25,gt25),
                total_prod_cost = NVL(P_OPP_OBJ.TOTAL_PROD_COST,total_prod_cost),
                total_prod_resale = NVL(P_OPP_OBJ.TOTAL_PROD_RESALE,total_prod_resale),
                prod_grs_margin = NVL(P_OPP_OBJ.PROD_GRS_MARGIN,prod_grs_margin),
                proj_net_margin = NVL(P_OPP_OBJ.PROJ_NET_MARGIN,proj_net_margin),
                bitem_liability = NVL(P_OPP_OBJ.BITEM_LIABILITY,bitem_liability),
                customer_doc = NVL(P_OPP_OBJ.CUSTOMER_DOC,customer_doc),
                total_prod_requested = NVL(P_OPP_OBJ.TOTAL_PROD_REQUESTED,total_prod_requested),
                total_prod_quoted = NVL(P_OPP_OBJ.TOTAL_PROD_QUOTED,total_prod_quoted),
                total_adjustments = NVL(P_OPP_OBJ.TOTAL_ADJUSTMENTS,total_adjustments),
                total_program_costs = NVL(P_OPP_OBJ.TOTAL_PROGRAM_COSTS,total_program_costs),
                shipping_fees = NVL(P_OPP_OBJ.SHIPPING_FEES,shipping_fees),
                shipping_taxes = NVL(P_OPP_OBJ.SHIPPING_TAXES,shipping_taxes),
                service_fees = NVL(P_OPP_OBJ.SERVICE_FEES,service_fees),
                liq_damages = NVL(P_OPP_OBJ.LIQ_DAMAGES,liq_damages),
                target_percentage = NVL(P_OPP_OBJ.TARGET_PERCENTAGE,target_percentage),
                target_date = NVL(P_OPP_OBJ.TARGET_DATE,target_date),
                contract_team = NVL(P_OPP_OBJ.CONTRACT_TEAM,contract_team),
                user_name = NVL(P_OPP_OBJ.USER_NAME,user_name),
                oname = NVL(P_OPP_OBJ.ONAME,oname),
                emailid = NVL(P_OPP_OBJ.EMAILID,emailid),
                user_group = NVL(P_OPP_OBJ.USER_GROUP,user_group),
                role = NVL(P_OPP_OBJ.ROLE,role),
                manager = NVL(P_OPP_OBJ.MANAGER,manager),
                sales_rep = NVL(P_OPP_OBJ.SALES_REP,sales_rep),
                proposal_manager = NVL(P_OPP_OBJ.PROPOSAL_MANAGER,proposal_manager),
                pricing_manager = NVL(P_OPP_OBJ.PRICING_MANAGER,pricing_manager),
                cust_liab_on_invest = NVL(P_OPP_OBJ.CUST_LIAB_ON_INVEST,cust_liab_on_invest),
                ret_fmt_proposal = NVL(P_OPP_OBJ.RET_FMT_PROPOSAL,ret_fmt_proposal),
                amount = NVL(P_OPP_OBJ.AMOUNT,amount),
                program_type = NVL(P_OPP_OBJ.PROGRAM_TYPE,program_type),
                expected_revenue = NVL(P_OPP_OBJ.EXPECTED_REVENUE,expected_revenue),
                primary_contact = NVL(P_OPP_OBJ.PRIMARY_CONTACT,primary_contact),
                primary_competitor = NVL(P_OPP_OBJ.PRIMARY_COMPETITOR,primary_competitor),
                fwd_stock_location = NVL(P_OPP_OBJ.FWD_STOCK_LOCATION,fwd_stock_location),
                line_minimum = NVL(P_OPP_OBJ.LINE_MINIMUM,line_minimum),
                ult_destination_of_parts = NVL(P_OPP_OBJ.ULT_DESTINATION_OF_PARTS,ult_destination_of_parts),
                description = NVL(P_OPP_OBJ.DESCRIPTION,description),
                ret_fmt_pricing_file = NVL(P_OPP_OBJ.RET_FMT_PRICING_FILE,ret_fmt_pricing_file),
                can_use_min_ord_qty = NVL(P_OPP_OBJ.CAN_USE_MIN_ORD_QTY,can_use_min_ord_qty),
                sales_stage = NVL(P_OPP_OBJ.SALES_STAGE,sales_stage),
                recover_shipping_cost = NVL(P_OPP_OBJ.RECOVER_SHIPPING_COST,recover_shipping_cost),
                tso_pma_certification = NVL(P_OPP_OBJ.TSO_PMA_CERTIFICATION,tso_pma_certification),
                win_loss_reason = NVL(P_OPP_OBJ.WIN_LOSS_REASON,win_loss_reason),
                platform_name = NVL(P_OPP_OBJ.PLATFORM_NAME,platform_name),
                cust_ref_number = NVL(P_OPP_OBJ.CUST_REF_NUMBER,cust_ref_number),
                win_probability = NVL(P_OPP_OBJ.WIN_PROBABILITY,win_probability),
                order_minimum = NVL(P_OPP_OBJ.ORDER_MINIMUM,order_minimum),
                team_number = NVL(P_OPP_OBJ.TEAM_NUMBER,team_number),
                existing_contract_num = NVL(P_OPP_OBJ.EXISTING_CONTRACT_NUM,existing_contract_num),
                fwd_stock_loc_req = NVL(P_OPP_OBJ.FWD_STOCK_LOC_REQ,fwd_stock_loc_req),
                close_date = NVL(P_OPP_OBJ.CLOSE_DATE,close_date),
                recover_prog_cost = NVL(P_OPP_OBJ.RECOVER_PROG_COST,recover_prog_cost),
                last_updated_date = NVL(P_OPP_OBJ.LAST_UPDATED_DATE,sysdate),
                last_updated_by = NVL(P_OPP_OBJ.LAST_UPDATED_BY,sys_context('USERENV','OS_USER')),
                other_payment_terms = NVL(P_OPP_OBJ.OTHER_PAYMENT_TERMS,other_payment_terms),
                other_incoterms = NVL(P_OPP_OBJ.OTHER_INCOTERMS,other_incoterms),
                other_cust_approved_sources = NVL(P_OPP_OBJ.OTHER_CUST_APPROVED_SOURCES,other_cust_approved_sources),
                terms_note = NVL(P_OPP_OBJ.TERMS_NOTE,terms_note),
                project_num_blank = NVL(P_OPP_OBJ.PROJECT_NUM_BLANK,project_num_blank),
                sales_project_num_blank = NVL(P_OPP_OBJ.SALES_PROJECT_NUM_BLANK,sales_project_num_blank),
                award_file_upload = NVL(P_OPP_OBJ.AWARD_FILE_UPLOAD,award_file_upload),
                doc_rdy_to_submit = NVL(P_OPP_OBJ.DOC_RDY_TO_SUBMIT,doc_rdy_to_submit),
                sales_owner_email = NVL(P_OPP_OBJ.SALES_OWNER_EMAIL, sales_owner_email),
                is_cust_rebate_requested = NVL(P_OPP_OBJ.IS_CUST_REBATE_REQUESTED, is_cust_rebate_requested),
                requested_stocking_strategy = NVL(P_OPP_OBJ.REQUESTED_STOCKING_STRATEGY,requested_stocking_strategy),
                customer_no_crf = NVL(P_OPP_OBJ.CUSTOMER_NO_CRF, customer_no_crf),
                stocking_strategy_crf = NVL(P_OPP_OBJ.STOCKING_STRATEGY_CRF, stocking_strategy_crf),
                service_fees_crf = NVL(P_OPP_OBJ.SERVICE_FEES_CRF, service_fees_crf),
                line_min_crf = NVL(P_OPP_OBJ.LINE_MIN_CRF,line_min_crf),
                contract_type = NVL(P_OPP_OBJ.CONTRACT_TYPE,contract_type),
                project_type = NVL(P_OPP_OBJ.PROJECT_TYPE,project_type)
          WHERE opportunity_number = l_opportunity_number;
        COMMIT;

        cct_fetch_fields.get_opty_id(l_opportunity_number,l_opportunity_id);

        P_RETURN_STATUS := 'SUCCESS';
    EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
        P_RETURN_STATUS := 'FAILURE';
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,500);
        cct_create_errors.record_error(
            p_opportunity_id   => l_opportunity_id,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_CREATE_OPPORTUNITY.update_opportunity_details',
            p_created_by       => l_created_by
        );
    END update_opportunity_details;

    PROCEDURE calculate_quote_parameters (
        P_OPP_NUMBER             IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE,
        P_NET_MARGIN            OUT VARCHAR2,
        P_AMOUNT                OUT CCT_OPPORTUNITY.AMOUNT%TYPE,
        P_PAYMENT_TERMS         OUT CCT_OPPORTUNITY.PAYMENT_TERMS%TYPE,
        P_OTHER_PAYMENT_TERMS   OUT CCT_OPPORTUNITY.OTHER_PAYMENT_TERMS%TYPE,
        P_DEAL_STRUCTURE        OUT CCT_OPPORTUNITY.DEAL_STRUCTURE%TYPE)
    AS
        l_net_margin            CCT_OPPORTUNITY.PROJ_NET_MARGIN%TYPE;
        l_amount                CCT_OPPORTUNITY.AMOUNT%TYPE;
        l_payment_terms         CCT_OPPORTUNITY.PAYMENT_TERMS%TYPE;
        l_other_payment_terms   CCT_OPPORTUNITY.OTHER_PAYMENT_TERMS%TYPE;
        l_deal_structure        CCT_OPPORTUNITY.DEAL_STRUCTURE%TYPE;
    BEGIN
        cct_fetch_fields.get_opty_id(P_OPP_NUMBER,g_opportunity_id);

        BEGIN
            SELECT proj_net_margin,
                   amount,
                   payment_terms,
                   other_payment_terms,
                   deal_structure
              INTO l_net_margin,
                   l_amount,
                   l_payment_terms,
                   l_other_payment_terms,
                   l_deal_structure
              FROM cct_opportunity
             WHERE opportunity_number = P_OPP_NUMBER;
        EXCEPTION
        WHEN OTHERS THEN
            g_code := sqlcode;
            g_msg := substr(sqlerrm,1,500);
            cct_create_errors.record_error(
                p_opportunity_id   => g_opportunity_id,
                p_error_message    => g_msg,
                p_error_type       => g_code,
                p_module_name      => 'CCT_CREATE_OPPORTUNITY.calculate_quote_parameters',
                p_created_by       => g_created_by
            );
        END;

        P_NET_MARGIN := l_net_margin;
        P_NET_MARGIN := TO_CHAR(p_net_margin,'FM90.99');
        P_AMOUNT := l_amount;
        P_PAYMENT_TERMS := l_payment_terms;
        P_OTHER_PAYMENT_TERMS := l_other_payment_terms;

        IF UPPER(l_deal_structure) = 'PRODUCTS_ONLY' THEN
            P_DEAL_STRUCTURE := 'LTA';
        ELSE
            P_DEAL_STRUCTURE := 'PROGRAM';
        END IF;

    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,500);
        cct_create_errors.record_error(
            p_opportunity_id   => g_opportunity_id,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_CREATE_OPPORTUNITY.calculate_quote_parameters',
            p_created_by       => g_created_by
        );
    END calculate_quote_parameters;

    PROCEDURE update_formula_fields(
        P_OPP_ID                 IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE)
    AS
    BEGIN
        g_opportunity_id := P_OPP_ID;

        UPDATE cct_opportunity co
           SET co.proj_net_margin = (SELECT case
         when (sum(nvl(b.total_resale,0)) + sum(nvl(c.service_fees,0))) <> 0 then
         round(((( (sum(nvl(b.total_resale,0)) + sum(nvl(c.service_fees,0))) - (sum(nvl(b.total_cost,0)) +
         sum(nvl(c.total_adjustments,0))) ) / (sum(nvl(b.total_resale,0)) + sum(nvl(c.service_fees,0))))*100 ))
         else 0
         end net_margin
        FROM (SELECT  co.opportunity_id,
                  MAX(cqp.quote_revision_id) quote_revision_id
            FROM  cct_quote cq,
                  cct_quote_revisions cqr,
                  CCT_QUOTE_PARTS_FINAL cqp
           WHERE  co.opportunity_id = cq.opportunity_id AND
                  cq.quote_id = cqr.quote_id AND
                  cqr.revision_id = cqp.quote_revision_id
        GROUP BY  co.opportunity_id) a,
           (SELECT  SUM(NVL(cqp.resale,0) * NVL(cqp.total_qty_len_contract,0)) total_resale,
                  SUM(NVL(cqp.cost_used_for_quote,0) * NVL(cqp.total_qty_len_contract,0)) total_cost,
                  cqp.quote_revision_id quote_revision_id
             FROM CCT_QUOTE_PARTS_FINAL cqp,
                  cct_quote_revisions cqr,
                  cct_quote cq,
                  cct_program_cost_header cpch
            WHERE cqp.quote_revision_id = cqr.revision_id AND
                  cqr.quote_id = cq.quote_id AND
                  cq.opportunity_id = co.opportunity_id AND
                  cpch.opportunity_id = co.opportunity_id
                  AND cqp.resale is not null AND -- exclude items for which there are no resale
                  (cqp.queue_id not in (6,8) or cqp.queue_id is null) AND   --added constraint for not considering parts from 'No Bid' and 'Unknown'
                  ((cqp.item_tag <> '5.0' and cqp.item_tag <> '5') or (cqp.item_tag is null)) --added constraint for excluding parts with item_tag 5 for both alphanumeric and number values of item tag
         GROUP BY cqp.quote_revision_id) b,
                  (SELECT  co.opportunity_id,
                SUM(nvl(cpch.service_fees,0)) service_fees,
                 /*((SUM(NVL(cpch.customer_rebate,0) +
                      NVL(cpch.shipping_fee_taxes,0) +
                      NVL(cpch.agent_commissions,0) +
                      NVL(cpch.total_program_costs,0))) -
                  SUM(nvl(cpch.service_fees,0))) total_adjustments
                 */
                  (SUM(NVL(cpch.service_fees,0)) - SUM((NVL(cpch.total_program_costs,0)+ NVL(cpch.customer_rebate,0) + NVL(cpch.agent_commissions,0) + 
                  NVL(cpch.shipping_fee_taxes,0)))) total_adjustments --CHG0036547#modified total adjustments formula by subtracting service fees  
                  FROM  cct_program_cost_header cpch
            where cpch.opportunity_id = co.opportunity_id
          GROUP BY  co.opportunity_id)c
        WHERE a.quote_revision_id = b.quote_revision_id(+)
            and a.opportunity_id  = c.opportunity_id (+)
                    ),
                amount = (SELECT round(SUM(nvl(b.amount,0))) amount
							FROM (SELECT co.opportunity_id,
										 MAX(cqp.quote_revision_id) quote_revision_id
									FROM cct_quote_parts_final cqp,
										 cct_quote cq,
										 cct_quote_revisions cqr
								   WHERE co.opportunity_id = cq.opportunity_id AND
									     cq.quote_id = cqr.quote_id AND
										 cqr.revision_id = cqp.quote_revision_id
								GROUP BY co.opportunity_id) a,
								 (SELECT cqp.quote_revision_id,
										 SUM(nvl(cqp.resale,0) *
											 nvl(cqp.total_qty_len_contract,0)) amount
									FROM cct_quote_parts_final cqp,
										 cct_quote_revisions cqr,
										 cct_quote cq
								   WHERE co.opportunity_id = cq.opportunity_id AND
										 cq.quote_id = cqr.quote_id AND
										 cqr.revision_id = cqp.quote_revision_id
								GROUP BY cqp.quote_revision_id) b
                    WHERE a.quote_revision_id = b.quote_revision_id(+)
                    )
		WHERE co.opportunity_id = P_OPP_ID;
        COMMIT;
    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,500);
        cct_create_errors.record_error(
            p_opportunity_id   => g_opportunity_id,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_CREATE_OPPORTUNITY.update_formula_fields',
            p_created_by       => g_created_by
        );
    END update_formula_fields;

    PROCEDURE submit_to_sales(
        P_OPP_ID                 IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE)
    AS
        l_snapshot_id               CCT_QUOTE_APPROVALS.SNAPSHOT_ID%TYPE;
        l_opp_num                   CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE;
        l_crf_id                    CCT_CRF.CRF_ID%TYPE;
        l_flag                      NUMBER;
    BEGIN
        g_opportunity_id := P_OPP_ID;
        update_formula_fields(P_OPP_ID);

		--        UPDATE cct_opportunity
		--           SET date_submitted_to_sales = SYSDATE
		--         WHERE opportunity_id = P_OPP_ID;
		--        COMMIT;
		--date_submitted_to_sales oly for snapshot
        BEGIN
            SELECT MAX(snapshot_id)
              INTO l_snapshot_id
              FROM cct_quote_approvals
             WHERE (UPPER(approval_status)='APPROVED' OR
                    UPPER(approval_status) = 'PASSED BY PROPOSAL MANAGER') AND
                   opportunity_id = P_OPP_ID;
        EXCEPTION
        WHEN OTHERS THEN
            g_code := sqlcode;
            g_msg  := substr(sqlerrm,1,500);
            cct_create_errors.record_error(
                p_opportunity_id   => g_opportunity_id,
                p_error_message    => g_msg,
                p_error_type       => g_code,
                p_module_name      => 'CCT_CREATE_OPPORTUNITY.submit_to_sales - No approved snapshots',
                p_created_by       => g_created_by
            );
        END;

        IF l_snapshot_id IS NOT NULL THEN
            UPDATE cct_opportunity_snapshot
               SET date_submitted_to_sales = SYSDATE
             WHERE opportunity_id = P_OPP_ID AND
                   snapshot_id = l_snapshot_id;
            COMMIT;
        END IF;
        
        cct_fetch_fields.get_opty_num(P_OPP_ID,l_opp_num);
        cct_fetch_fields.get_crf_id(l_opp_num,P_OPP_ID,l_crf_id);
        
        SELECT COUNT(*) 
          INTO l_flag 
          FROM cct_crf_revision 
         WHERE crf_id = l_crf_id;
        
        IF l_flag = 0 THEN 
            UPDATE cct_crf_date_management 
               SET offer_sent_to_cust_date = SYSDATE 
             WHERE crf_id = l_crf_id;
            COMMIT;
        ELSE 
            UPDATE cct_crf_revision 
               SET offer_sent_to_cust_date = SYSDATE 
             WHERE crf_id = l_crf_id AND 
                   crf_revision_id = (SELECT MAX(crf_revision_id) 
                                        FROM cct_crf_revision 
                                       WHERE crf_id = l_crf_id);
            COMMIT;
        END IF;

        UPDATE cct_crf 
           SET CRF_STATUS = 'Pending'  
         WHERE crf_no = l_opp_num AND 
               UPPER(crf_status) = 'ACTIVE';
        COMMIT;

        UPDATE cct_opportunity 
           SET status = 'Pending'  
         WHERE opportunity_number = l_opp_num AND 
               UPPER(status) = 'ACTIVE';
        COMMIT;

    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,500);
        cct_create_errors.record_error(
            p_opportunity_id   => g_opportunity_id,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_CREATE_OPPORTUNITY.submit_to_sales',
            p_created_by       => g_created_by
        );
    END submit_to_sales;

    PROCEDURE copy_to_opportunity(
        P_CRF_NO                 IN CCT_CRF.CRF_NO%TYPE)
    AS
        l_opp_obj                   CCT_OPPORTUNITY_TYPE := CCT_OPPORTUNITY_TYPE();
        l_opp_type                  VARCHAR2(20) := 'NEW';
        l_return_status             VARCHAR2(20);
        l_cust_due_date_flag        VARCHAR2(20);
        l_old_cust_due_date         DATE;
        ex_crf_insert               EXCEPTION;
    BEGIN
        map_to_opportunity(P_CRF_NO,NULL,l_opp_obj);

        cct_create_opportunity.insert_opportunity_details(
            l_opp_obj,l_opp_type,l_return_status,l_cust_due_date_flag,l_old_cust_due_date);

        IF l_return_status = 'FAILURE' THEN
            RAISE ex_crf_insert;
        END IF;

        cct_fetch_fields.get_opty_id(P_CRF_NO,g_opportunity_id);
    EXCEPTION
    WHEN ex_crf_insert THEN
        g_code := sqlcode;
        g_msg  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            p_opportunity_id   => g_opportunity_id,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_CREATE_OPPORTUNITY.copy_to_opportunity INSERT was unsuccessful',
            p_created_by       => sys_context('USERENV','OS_USER')
        );
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,500);
        cct_create_errors.record_error(
            p_opportunity_id   => g_opportunity_id,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_CREATE_OPPORTUNITY.copy_to_opportunity',
            p_created_by       => g_created_by
        );
    END copy_to_opportunity;

   PROCEDURE map_to_opportunity(
        P_CRF_NO                 IN CCT_CRF.CRF_NO%TYPE,
        P_OPP_ID                 IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE,
        P_OPP_OBJ               OUT CCT_OPPORTUNITY_TYPE)
    AS
        l_crf_id                    CCT_CRF.CRF_ID%TYPE;
    BEGIN
        P_OPP_OBJ   := CCT_OPPORTUNITY_TYPE();

        SELECT  crf_id,
                crf_no,
                crf_status,
                due_date,
                project_no,
                customer_ref
          INTO  l_crf_id,
                P_OPP_OBJ.opportunity_number,
                P_OPP_OBJ.status,
                P_OPP_OBJ.customer_due_date,
                P_OPP_OBJ.project_number,
                P_OPP_OBJ.cust_ref_number
          FROM  cct_crf
         WHERE  crf_no = P_CRF_NO;

        SELECT  customer_no,
                customer_name,
                SALES_TEAM_NUMBER
          INTO  P_OPP_OBJ.customer_no_crf,
                P_OPP_OBJ.customer_name,
                P_OPP_OBJ.team_number
          FROM  cct_crf_customer_info
         WHERE  crf_id = l_crf_id;

        SELECT  currency_code,
                is_dfar,
                CASE source_products WHEN 'Y' THEN 'TRUE' ELSE 'FALSE' END,
                rebate,
                payment_terms,
                inco_terms,
                lol_protection,
                stocking_strategy,
                rights_of_return,
                incentives_credit,
                cancellation_rights,
                bill_hold,
                is_buy_back,
                service_fees,
                liq_damages,
                line_min,
                platform_info,
                order_min,
                contract_type,
                project_type,
                contract_length,
                CASE contract_type WHEN 'Program' THEN 'Products and Services' 
                                   WHEN 'LTA' THEN 'Products Only' END 
          INTO  P_OPP_OBJ.currency,
                P_OPP_OBJ.dfar_apply,
                P_OPP_OBJ.source_products,
                P_OPP_OBJ.rebate_crf,
                P_OPP_OBJ.payment_terms,
                P_OPP_OBJ.incoterms,
                P_OPP_OBJ.lol_protection,
                P_OPP_OBJ.stocking_strategy_crf,
                P_OPP_OBJ.rights_of_return,
                P_OPP_OBJ.incentives,
                P_OPP_OBJ.cancel_privileges,
                P_OPP_OBJ.bill_hold,
                P_OPP_OBJ.buy_back,
                P_OPP_OBJ.service_fees_crf,
                P_OPP_OBJ.liq_damages,
                P_OPP_OBJ.line_min_crf,
                P_OPP_OBJ.platform_name,
                P_OPP_OBJ.order_minimum,
                P_OPP_OBJ.contract_type,
                P_OPP_OBJ.project_type,
                P_OPP_OBJ.contract_term,
                P_OPP_OBJ.deal_structure
          FROM  cct_crf_customer_request
         WHERE  crf_id = l_crf_id;

         P_OPP_OBJ.creation_date := sysdate;
         P_OPP_OBJ.created_by := sys_context('USERENV','OS_USER');
    EXCEPTION
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,500);
        cct_create_errors.record_error(
            p_opportunity_id   => P_OPP_ID,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_CREATE_OPPORTUNITY.map_to_opportunity',
            p_created_by       => g_created_by
        );
    END map_to_opportunity;

    PROCEDURE update_to_opportunity(
        P_CRF_NO                 IN CCT_CRF.CRF_NO%TYPE)
    AS
        l_opp_obj                   CCT_OPPORTUNITY_TYPE := CCT_OPPORTUNITY_TYPE();
        l_opp_type                  VARCHAR2(20) := 'UPDATE';
        l_return_status             VARCHAR2(20);
        l_cust_due_date_flag        VARCHAR2(20);
        l_old_cust_due_date         DATE;
        ex_crf_update               EXCEPTION;
    BEGIN
        cct_fetch_fields.get_opty_id(P_CRF_NO,g_opportunity_id);

        map_to_opportunity(P_CRF_NO,g_opportunity_id,l_opp_obj);

--        cct_create_opportunity.insert_opportunity_details(
--            l_opp_obj,l_opp_type,l_return_status,l_cust_due_date_flag,l_old_cust_due_date);
        
        update_opportunity_details(l_opp_obj,l_return_status);
        
        IF l_return_status = 'FAILURE' THEN
            RAISE ex_crf_update;
        END IF;

    EXCEPTION
    WHEN ex_crf_update THEN
        g_code := sqlcode;
        g_msg  := substr(sqlerrm,1,450);
        cct_create_errors.record_error(
            p_opportunity_id   => g_opportunity_id,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_CREATE_OPPORTUNITY.update_to_opportunity UPDATE was unsuccessful',
            p_created_by       => sys_context('USERENV','OS_USER')
        );
    WHEN OTHERS THEN
        g_code := sqlcode;
        g_msg := substr(sqlerrm,1,500);
        cct_create_errors.record_error(
            p_opportunity_id   => g_opportunity_id,
            p_error_message    => g_msg,
            p_error_type       => g_code,
            p_module_name      => 'CCT_CREATE_OPPORTUNITY.update_to_opportunity',
            p_created_by       => g_created_by
        );
    END update_to_opportunity;

END CCT_CREATE_OPPORTUNITY;