create or replace PACKAGE cct_top50_parts_pkg AS
/* Modification History
  PERSON        DATE         COMMENTS
  ------------  -----------  ------------------------------------------------------
  Infosys       14-Aug-2018  Calculate and insert top50 queue and parts combination
 */
    
    PROCEDURE get_top50_parts ( p_in_quote_rev_id   IN cct_quote_parts.quote_revision_id%TYPE
                               ,p_out_num_ret_code OUT NUMBER
                               ,p_out_chr_err_msg OUT VARCHAR2
                               );
    PROCEDURE get_top50_prod_queue ( p_in_quote_rev_id   IN cct_quote_parts.quote_revision_id%TYPE );

END cct_top50_parts_pkg;