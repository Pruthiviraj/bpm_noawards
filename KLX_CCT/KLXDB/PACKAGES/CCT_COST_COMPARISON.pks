create or replace PACKAGE cct_cost_comparison 
AS 
/**Handles CCT_APPROVAL_RULES table data
   Modification History 
   PERSON        DATE           COMMENTS
   ------------  -----------    ---------------------------------------------
   Sureka        28-Sep-2017    Created insert_cost_header proc
   Sureka        28-Sep-2017    Created setup_snapshot_header proc
   Sureka        28-Sep-2017    Created setup_current_header proc
   Sureka        28-Sep-2017    Created calc_delta proc
   Sureka        29-Sep-2017    Created calc_additional_changes proc
   Sureka        30-Sep-2017    Created insert_cost_lines proc
   Sureka        06-Oct-2017    Modified insert_cost_lines proc
   Sureka        16-Oct-2017    Modified calc_delta proc
   Sureka        06-Nov-2017    Created insert_approvals_header
   Sureka        06-Nov-2017    Modified insert_cost_header
   Sureka        06-Nov-2017    Modified setup_snapshot_header
   Sureka        06-Nov-2017    Modified setup_current_header
   Sureka        06-Nov-2017    Modified calc_delta
   Sureka        20-Nov-2017    Modified calc_additional_changes to include 
								suggested columns
**/
    g_code                      VARCHAR2(50);
    g_msg                       VARCHAR2(500);
    
    PROCEDURE insert_cost_header(
        P_OPP_NUM            IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE);
    
    PROCEDURE insert_approvals_header(
        P_OPP_NUM            IN CCT_OPPORTUNITY.OPPORTUNITY_NUMBER%TYPE);
            
    PROCEDURE setup_snapshot_header(
        P_OPP_ID             IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE,
        P_SNAPSHOT_ID        IN CCT_SNAPSHOT.SNAPSHOT_ID%TYPE,
        P_QUOTE_REVISION_ID  IN CCT_SNAPSHOT.QUOTE_REVISION_ID%TYPE,
        P_HEADER_TYPE        IN VARCHAR2);
    
    PROCEDURE setup_current_header(
        P_OPP_ID             IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE,
        P_QUOTE_REVISION_ID  IN CCT_QUOTE_PARTS.QUOTE_REVISION_ID%TYPE,
        P_HEADER_TYPE        IN VARCHAR2);
        
    PROCEDURE calc_delta(
        P_OPP_ID             IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE,
        P_DML_TYPE           IN VARCHAR2,
        P_DELTA_TYPE         IN VARCHAR2);
    
    PROCEDURE calc_additional_changes(
        P_OPP_ID             IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE,
        P_SNAPSHOT_REV_ID    IN CCT_SNAPSHOT.QUOTE_REVISION_ID%TYPE,
        P_CURRENT_REV_ID     IN CCT_QUOTE_PARTS.QUOTE_REVISION_ID%TYPE,
        P_QUOTE_PART         IN CCT_COST_COMPARE_LINES.QUOTED_PART_NUMBER%TYPE,
        P_CUSTOMER_PN        IN CCT_QUOTE_PARTS_SNAPSHOT.CUSTOMER_PN%TYPE,    
        P_LINE_IDENTIFIER    IN CCT_QUOTE_PARTS_SNAPSHOT.LINE_IDENTIFIER%TYPE, 
        P_FLAG              OUT VARCHAR2);
    
    PROCEDURE insert_cost_lines(
        P_OPP_ID             IN CCT_OPPORTUNITY.OPPORTUNITY_ID%TYPE,
        P_SNAPSHOT_REV_ID    IN CCT_SNAPSHOT.QUOTE_REVISION_ID%TYPE,
        P_CURRENT_REV_ID     IN CCT_QUOTE_PARTS.QUOTE_REVISION_ID%TYPE);
        
END cct_cost_comparison;