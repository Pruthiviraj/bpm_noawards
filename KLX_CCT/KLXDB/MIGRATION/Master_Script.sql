############################################################################################
#                                                                                          #
#  1.0.0 23-Aug-2017 Ankit Bhatt   DB Migration Script for XXCCT schema                    #
#                                  Data insertion in cct_properties table will be done     #
#                                  manually instance wise                                  #     
############################################################################################

spool Migration_Script_Validations.log

SET HEADING ON
SET DEFINE OFF

PRO ############################# TABLES ############################################

PRO # Table creation script for CCT_OPPORTUNITY

@ CCT_OPPORTUNITY.sql

PRO # Table creation script for ALTER_CCT_OPPORTUNITY 

@ ALTER_CCT_OPPORTUNITY.sql

PRO # Table creation script for CCT_QUOTE

@ CCT_QUOTE.sql

PRO # Table creation script for CCT_QUOTE_REVISIONS

@ CCT_QUOTE_REVISIONS.sql

PRO # Table creation script for CCT_PARTS_QUEUE

@ CCT_PARTS_QUEUE.sql

PRO # Table creation script for CCT_QUOTE_PARTS

@ CCT_QUOTE_PARTS.sql

PRO # Table creation script for ALTER_CCT_QUOTE_PARTS 

@ ALTER_CCT_QUOTE_PARTS.sql

PRO # Table creation script for CCT_QUOTE_PARTS_FINAL

@ CCT_QUOTE_PARTS_FINAL.sql

PRO # Table creation script for ALTER_CCT_QUOTE_PARTS_FINAL 

@ ALTER_CCT_QUOTE_PARTS_FINAL.sql

PRO # Table creation script for CCT_APPROVAL_RULE_VERSIONS

@ CCT_APPROVAL_RULE_VERSIONS.sql

PRO # Table creation script for CCT_APPROVAL_RULES

@ CCT_APPROVAL_RULES.sql

PRO # Table creation script for ALTER_CCT_APPROVAL_RULES 

@ ALTER_CCT_APPROVAL_RULES.sql

PRO # Table creation script for CCT_AWARDS

@ CCT_AWARDS.sql

PRO # Table creation script for ALTER_CCT_AWARDS

@ ALTER_CCT_AWARDS.sql

PRO # Table creation script for CCT_ERROR_LOGS

@ CCT_ERROR_LOGS.sql

PRO # Table creation script for CCT_MAPPINGS

@ CCT_MAPPINGS.sql

PRO # Table creation script for CCT_NOTES

@ CCT_NOTES.sql

PRO # Table creation script for ALTER_CCT_NOTES 

@ ALTER_CCT_NOTES.sql

PRO # Table creation script for CCT_NOTIFICATION_LOGS

@ CCT_NOTIFICATION_LOGS.sql

PRO # Table creation script for CCT_NOTIFICATION_REQUEST

@ CCT_NOTIFICATION_REQUEST.sql

PRO # Table creation script for ALTER_CCT_NOTIFICATION_REQUEST 

@ ALTER_CCT_NOTIFICATION_REQUEST.sql

PRO # Table creation script for CCT_NOTIFICATION_TEMP

@ CCT_NOTIFICATION_TEMP.sql

PRO # Table creation script for CCT_NOTIFICATION_TEMPLATE

@ CCT_NOTIFICATION_TEMPLATE.sql

PRO # Table creation script for CCT_NOTIFICATION_TYPE

@ CCT_NOTIFICATION_TYPE.sql

PRO # Table creation script for ALTER_CCT_NOTIFICATION_TYPE

@ ALTER_CCT_NOTIFICATION_TYPE.sql

PRO # Table creation script for CCT_PARTS_CLASS

@ CCT_PARTS_CLASS.sql

PRO # Table creation script for CCT_PROGRAM_COST_HEADER

@ CCT_PROGRAM_COST_HEADER.sql

PRO # Table creation script for ALTER_CCT_PROGRAM_COST_HEADER 

@ ALTER_CCT_PROGRAM_COST_HEADER.sql

PRO # Table creation script for CCT_PROGRAM_COST_CAT_TYPES

@ CCT_PROGRAM_COST_CAT_TYPES.sql

PRO # Table creation script for CCT_PROGRAM_COST_LINES

@ CCT_PROGRAM_COST_LINES.sql

PRO # Table creation script for CCT_PROGRAM_COST_CAT_SUMMARY

@ CCT_PROGRAM_COST_CAT_SUMMARY.sql

PRO # Table creation script for CCT_PROPERTIES

@ CCT_PROPERTIES.sql

PRO # Table creation script for CCT_ROLE_PROPERTIES

@ CCT_ROLE_PROPERTIES.sql

PRO # Table creation script for ALTER_CCT_ROLE_PROPERTIES 

@ ALTER_CCT_ROLE_PROPERTIES.sql

PRO # Table creation script for CCT_TASK_LOGS

@ CCT_TASK_LOGS.sql

PRO # Table creation script for CCT_SNAPSHOT

@ CCT_SNAPSHOT.sql

PRO # Table creation script for CCT_QUOTE_FORMULA_SS_FINAL

@ CCT_QUOTE_FORMULA_SS_FINAL.sql

PRO # Table creation script for CCT_APPROVAL_USERS_TEMP

@ CCT_APPROVAL_USERS_TEMP.sql

PRO # Table creation script for CCT_APPROVAL_REQUEST

@ CCT_APPROVAL_REQUEST.sql

PRO # Table creation script for CCT_PROGRAM_COST_CAT_SUBTYPES

@ CCT_PROGRAM_COST_CAT_SUBTYPES.sql

PRO # Table creation script for CCT_QUOTE_PARTS_SNAPSHOT

@ CCT_QUOTE_PARTS_SNAPSHOT.sql

PRO # Table creation script for ALTER_CCT_QUOTE_PARTS_SNAPSHOT 

@ ALTER_CCT_QUOTE_PARTS_SNAPSHOT.sql

PRO # Table creation script for CCT_QUOTE_PARTS_SNAPSHOT_FINAL

@ CCT_QUOTE_PARTS_SNAPSHOT_FINAL.sql

PRO # Table creation script for ALTER_CCT_QUOTE_PARTS_SNAPSHOT_FINAL 

@ ALTER_CCT_QUOTE_PARTS_SNAPSHOT_FINAL.sql

PRO # Table creation script for CCT_OPPORTUNITY_SNAPSHOT

@ CCT_OPPORTUNITY_SNAPSHOT.sql

PRO # Table creation script for ALTER_CCT_OPPORTUNITY_SNAPSHOT 

@ ALTER_CCT_OPPORTUNITY_SNAPSHOT.sql

PRO # Table creation script for CCT_APPROVAL_LDOA_AUDIT

@ CCT_APPROVAL_LDOA_AUDIT.sql

PRO # Table creation script for ALTER_CCT_APPROVAL_LDOA_AUDIT 

@ ALTER_CCT_APPROVAL_LDOA_AUDIT.sql

PRO # Table creation script for CCT_QUOTE_APPROVALS

@ CCT_QUOTE_APPROVALS.sql

PRO # Table creation script for CCT_SNAPSHOT_APPROVAL

@ CCT_SNAPSHOT_APPROVAL.sql

PRO # Table creation script for ALTER_CCT_SNAPSHOT_APPROVAL

@ ALTER_CCT_SNAPSHOT_APPROVAL.sql

PRO # Table creation script for CCT_AWARD_HEADERS

@ CCT_AWARD_HEADERS.sql

PRO # Table creation script for CCT_COST_COMPARE_HEADER

@ CCT_COST_COMPARE_HEADER.sql

PRO # Table creation script for ALTER_CCT_COST_COMPARE_HEADER 

@ ALTER_CCT_COST_COMPARE_HEADER.sql

PRO # Table creation script for CCT_COST_COMPARE_LINES

@ CCT_COST_COMPARE_LINES.sql

PRO # Table creation script for ALTER_CCT_COST_COMPARE_LINES

@ ALTER_CCT_COST_COMPARE_LINES.sql

PRO # Table creation script for FMW_AUDITING_TBL

@ FMW_AUDITING_TBL.sql

PRO # Table creation script for FMW_RESUBMISSION_COUNT_TBL

@ FMW_RESUBMISSION_COUNT_TBL.sql

PRO # Table creation script for FMW_RESUBMISSION_TBL

@ FMW_RESUBMISSION_TBL.sql

PRO # Table creation script for CCT_AWARDS_SUMMARY

@ CCT_AWARDS_SUMMARY.sql

PRO # Table creation script for CCT_APPROVAL_COMPARE_HEADER

@ CCT_APPROVAL_COMPARE_HEADER.sql

PRO # Table creation script for CCT_APPROVED_COMPARE_HEADER

@ CCT_APPROVED_COMPARE_HEADER.sql

PRO # Table creation script for CCT_ROLE_USER_ACTION

@ CCT_ROLE_USER_ACTION.sql

PRO # Table creation script for CCT_NOTIFICATION_ROLE

@ CCT_NOTIFICATION_ROLE.sql

PRO # Table creation script for CCT_CRF

@ CCT_CRF.sql

PRO # Table creation script for ALTER_CCT_CRF

@ ALTER_CCT_CRF.sql

PRO # Table creation script for CCT_CRF_ASL_CODES

@ CCT_CRF_ASL_CODES.sql

PRO # Table creation script for CCT_CRF_ASL_INFO

@ CCT_CRF_ASL_INFO.sql

PRO # Table creation script for CCT_CRF_AWARD_DATES

@ CCT_CRF_AWARD_DATES.sql

PRO # Table creation script for ALTER_CCT_CRF_AWARD_DATES

@ ALTER_CCT_CRF_AWARD_DATES.sql

PRO # Table creation script for CCT_CRF_CUST_MASTER_CONTRACT

@ CCT_CRF_CUST_MASTER_CONTRACT.sql

PRO # Table creation script for CCT_CRF_CUSTOMER_INFO

@ CCT_CRF_CUSTOMER_INFO.sql

PRO # Table creation script for CCT_CRF_CUSTOMER_MASTER

@ CCT_CRF_CUSTOMER_MASTER.sql

PRO # Table creation script for CCT_CRF_CUSTOMER_MASTER_STG

@ CCT_CRF_CUSTOMER_MASTER_STG.sql

PRO # Table creation script for CCT_CRF_CUSTOMER_REQUEST

@ CCT_CRF_CUSTOMER_REQUEST.sql

PRO # Table creation script for ALTER_CCT_CRF_CUSTOMER_REQUEST 

@ ALTER_CCT_CRF_CUSTOMER_REQUEST.sql

PRO # Table creation script for CCT_CRF_DATE_MANAGEMENT

@ CCT_CRF_DATE_MANAGEMENT.sql

PRO # Table creation script for CCT_CRF_EXISTING_CONTRACT_INFO

@ CCT_CRF_EXISTING_CONTRACT_INFO.sql

PRO # Table creation script for CCT_CRF_KLX_AWARD

@ CCT_CRF_KLX_AWARD.sql

PRO # Table creation script for CCT_CRF_KLX_OFFER

@ CCT_CRF_KLX_OFFER.sql

PRO # Table creation script for CCT_CRF_REVISION

@ CCT_CRF_REVISION.sql

PRO # Table creation script for CCT_CRF_SALES_CONTACT_INFO

@ CCT_CRF_SALES_CONTACT_INFO.sql

PRO # Table creation script for CCT_CRF_SALES_TEAM

@ CCT_CRF_SALES_TEAM.sql

PRO # Table creation script for CCT_LOOK_UP

@ CCT_LOOK_UP.sql

PRO # Table creation script for CCT_SALES_HIERARCHY

@ CCT_SALES_HIERARCHY.sql

PRO # Table creation script for CCT_APPROVAL_USERS

@ CCT_APPROVAL_USERS.sql

PRO # Table creation script for CCT_CRF_ASL_CODES_STG

@ CCT_CRF_ASL_CODES_STG.sql

PRO # Table creation script for CCT_CRF_SALES_TEAM_STG

@ CCT_CRF_SALES_TEAM_STG.sql

PRO # Table creation script for ALTER_CCT_CRF_KLX_OFFER

@ ALTER_CCT_CRF_KLX_OFFER.sql

PRO # Table creation script for ALTER_CCT_CRF_KLX_AWARD

@ ALTER_CCT_CRF_KLX_AWARD.sql

PRO ############################# TYPES ############################################

PRO # Type creation script for CCT_TASK_TYPE_SPEC

@ CCT_TASK_TYPE_SPEC.sql

PRO # Type creation script for CCT_TASK_TYPE_BODY

@ CCT_TASK_TYPE_BODY.sql

PRO # Type creation script for CCT_QUEUE_PROD_CLASS_TBL_TYPE

@ CCT_QUEUE_PROD_CLASS_TBL_TYPE.sql

PRO # Type creation script for CCT_OPPORTUNITY_TYPE_SPEC

@ CCT_OPPORTUNITY_TYPE_SPEC.sql

PRO # Type creation script for CCT_OPPORTUNITY_TYPE_BODY

@ CCT_OPPORTUNITY_TYPE_BODY.sql

PRO # Type creation script for CCT_OPPORTUNITY_TBL_TYPE

@ CCT_OPPORTUNITY_TBL_TYPE.sql

PRO # Type creation script for CCT_PROD_TYPE

@ CCT_PROD_TYPE.sql

PRO # Type creation script for CCT_APPROVAL_TYPE_SPEC

@ CCT_APPROVAL_TYPE_SPEC.sql

PRO # Type creation script for CCT_APPROVAL_TYPE_BODY

@ CCT_APPROVAL_TYPE_BODY.sql

PRO # Type creation script for MESSAGE_FLOW_IDS_TYPE

@ MESSAGE_FLOW_IDS_TYPE.sql

PRO # Type creation script for RESUBMISSION_DETAILS_TYPE

@ RESUBMISSION_DETAILS_TYPE.sql

PRO # Type creation script for RESUBMISSION_DETAILS_TBL_TYPE

@ RESUBMISSION_DETAILS_TBL_TYPE.sql

PRO # Type creation script for RESUBMISSION_IDS_TYPE

@ RESUBMISSION_IDS_TYPE.sql

PRO ############################# SEQUENCE ############################################

PRO # sequence creation script for CCT_AWARD_SEQ

@ CCT_AWARD_SEQ.sql

PRO # sequence creation script for CCT_ERROR_SEQ

@ CCT_ERROR_SEQ.sql

PRO # sequence creation script for CCT_MAPPING_SEQ

@ CCT_MAPPING_SEQ.sql

PRO # sequence creation script for CCT_NOTES_SEQ

@ CCT_NOTES_SEQ.sql

PRO # sequence creation script for CCT_NOTIFICATION_REQ_SEQ

@ CCT_NOTIFICATION_REQ_SEQ.sql

PRO # sequence creation script for CCT_OPPORTUNITY_SEQ

@ CCT_OPPORTUNITY_SEQ.sql

PRO # sequence creation script for CCT_QT_PTS_WH_SEQ

@ CCT_QT_PTS_WH_SEQ.sql

PRO # sequence creation script for CCT_QUEUE_SEQ

@ CCT_QUEUE_SEQ.sql

PRO # sequence creation script for CCT_QUOTE_NUMBER_SEQ

@ CCT_QUOTE_NUMBER_SEQ.sql

PRO # sequence creation script for CCT_QUOTE_PARTS_SEQ

@ CCT_QUOTE_PARTS_SEQ.sql

PRO # sequence creation script for CCT_QUOTE_REVISION_SEQ

@ CCT_QUOTE_REVISION_SEQ.sql

PRO # sequence creation script for CCT_QUOTE_SEQ

@ CCT_QUOTE_SEQ.sql

PRO # sequence creation script for CCT_QUOTE_VERSION_SEQ

@ CCT_QUOTE_VERSION_SEQ.sql

PRO # sequence creation script for CCT_RULE_SEQ

@ CCT_RULE_SEQ.sql

PRO # sequence creation script for CCT_RULE_VERSION_SEQ

@ CCT_RULE_VERSION_SEQ.sql

PRO # sequence creation script for CCT_TASK_SEQ

@ CCT_TASK_SEQ.sql

PRO # sequence creation script for CCT_LDOA_AUDIT_SEQ

@ CCT_LDOA_AUDIT_SEQ.sql

PRO # sequence creation script for CCT_PROGRAM_COST_HDR_SEQ

@ CCT_PROGRAM_COST_HDR_SEQ.sql

PRO # sequence creation script for CCT_PROGRAM_COST_LINE_SEQ

@ CCT_PROGRAM_COST_LINE_SEQ.sql

PRO # sequence creation script for CCT_SNAPSHOT_SEQ

@ CCT_SNAPSHOT_SEQ.sql

PRO # sequence creation script for CCT_APPROVAL_REQ_SEQ

@ CCT_APPROVAL_REQ_SEQ.sql

PRO # sequence creation script for CCT_AWARD_HDR_SEQ

@ CCT_AWARD_HDR_SEQ.sql

PRO # sequence creation script for CCT_QUOTE_PARTS_FINAL_SEQ

@ CCT_QUOTE_PARTS_FINAL_SEQ.sql

PRO # sequence creation script for CCT_ROLE_USER_ACTION_SEQ

@ CCT_ROLE_USER_ACTION_SEQ.sql

PRO # sequence creation script for FMW_AUDITING_SEQ

@ FMW_AUDITING_SEQ.sql

PRO # sequence creation script for FMW_RESUBMISSION_SEQ

@ FMW_RESUBMISSION_SEQ.sql

PRO # sequence creation script for CCT_CRF_SALES_TEAM_SEQ

@ CCT_CRF_SALES_TEAM_SEQ.sql

PRO # sequence creation script for CCT_CRF_SEQ

@ CCT_CRF_SEQ.sql

PRO # sequence creation script for CCT_CRF_CUSTOMER_REQUEST_SEQ

@ CCT_CRF_CUSTOMER_REQUEST_SEQ.sql

PRO # sequence creation script for CCT_CRF_KLX_OFFER_SEQ

@ CCT_CRF_KLX_OFFER_SEQ.sql

PRO # sequence creation script for CCT_CRF_KLX_AWARD_SEQ 

@ CCT_CRF_KLX_AWARD_SEQ.sql

PRO # sequence creation script for CCT_CRF_CUSTOMER_INFO_SEQ

@ CCT_CRF_CUSTOMER_INFO_SEQ.sql

PRO # sequence creation script for CCT_LOOK_UP_SEQ

@ CCT_LOOK_UP_SEQ.sql

PRO # sequence creation script for CCT_APPROVAL_USERS_SEQ

@ CCT_APPROVAL_USERS_SEQ.sql

PRO # sequence creation script for CCT_CRF_REVISION_SEQ

@ CCT_CRF_REVISION_SEQ.sql

PRO ############################# TRIGGERS ############################################

PRO # Trigger creation script for CCT_OPPORTUNITY_SEQ_BI

@ CCT_OPPORTUNITY_SEQ_BI.sql

PRO # Trigger creation script for CCT_QUOTE_PARTS_SEQ_BI

@ CCT_QUOTE_PARTS_SEQ_BI.sql

PRO # Trigger creation script for CCT_RULE_SEQ_BI

@ CCT_RULE_SEQ_BI.sql

PRO # Trigger creation script for CCT_RULE_VERSION_SEQ_BI

@ CCT_RULE_VERSION_SEQ_BI.sql

PRO # Trigger creation script for CCT_AWARD_SEQ_BI

@ CCT_AWARD_SEQ_BI.sql

PRO # Trigger creation script for CCT_APPR_LDOA_AUDIT_AI

@ CCT_APPR_LDOA_AUDIT_AI.sql

PRO # Trigger creation script for CCT_QUOTE_PARTS_FINAL_SEQ_BI

@ CCT_QUOTE_PARTS_FINAL_SEQ_BI.sql

PRO # Trigger creation script for CCT_CRF_SALES_TEAM_SEQ_BI

@ CCT_CRF_SALES_TEAM_SEQ_BI.sql

PRO ############################# VIEWS ############################################

PRO # view creation script for CCT_AWARDS_V

@ CCT_AWARDS_V.sql

PRO # view creation script for CCT_QUOTE_CLASS_V

@ CCT_QUOTE_CLASS_V.sql

PRO # view creation script for CCT_QUOTE_PRICING_V

@ CCT_QUOTE_PRICING_V.sql

PRO # view creation script for CCT_QUOTE_QUEUE_V

@ CCT_QUOTE_QUEUE_V.sql

PRO # view creation script for CCT_QUOTE_QUEUE_FINAL_V

@ CCT_QUOTE_QUEUE_FINAL_V.sql

PRO # view creation script for CCT_TOP50_PARTS_V

@ CCT_TOP50_PARTS_V.sql

PRO # view creation script for CCT_TOP50_PROD_QUEUE_V

@ CCT_TOP50_PROD_QUEUE_V.sql

PRO # view creation script for CCT_FINAL_TOP50_PARTS_V

@ CCT_FINAL_TOP50_PARTS_V.sql

PRO # view creation script for CCT_FINAL_TOP50_PROD_QUEUE_V

@ CCT_FINAL_TOP50_PROD_QUEUE_V.sql

PRO # view creation script for CCT_QUOTE_PRICING_SS_V

@ CCT_QUOTE_PRICING_SS_V.sql

PRO # view creation script for CCT_AWARD_HEADERS_V

@ CCT_AWARD_HEADERS_V.sql

PRO # view creation script for CCT_QUOTE_CLASS_FINAL_V

@ CCT_QUOTE_CLASS_FINAL_V.sql

PRO # view creation script for CCT_QUOTE_CLASS_SS_FINAL_V

@ CCT_QUOTE_CLASS_SS_FINAL_V.sql

PRO # view creation script for CCT_QUOTE_PRICING_FINAL_V

@ CCT_QUOTE_PRICING_FINAL_V.sql

PRO # view creation script for CCT_QUOTE_PRICING_SS_FINAL_V

@ CCT_QUOTE_PRICING_SS_FINAL_V.sql

PRO # view creation script for CCT_TOP50_PARTS_DETAIL_V

@ CCT_TOP50_PARTS_DETAIL_V.sql

PRO # view creation script for CCT_TOP50_PRODUCT_QUEUE_V

@ CCT_TOP50_PRODUCT_QUEUE_V.sql

PRO ############################# PACKAGE SPECIFICATION ############################################

PRO # package specification script for CCT_CREATE_ERRORS_SPEC

@ CCT_CREATE_ERRORS_SPEC.sql

PRO # package specification script for CCT_PROPERTIES_PKG_SPEC

@ CCT_PROPERTIES_PKG_SPEC.sql

PRO # package specification script for CCT_FETCH_FIELDS_SPEC

@ CCT_FETCH_FIELDS_SPEC.sql

PRO # package specification script for CCT_NOTIFICATION_PKG_SPEC

@ CCT_NOTIFICATION_PKG_SPEC.sql

PRO # package specification script for CCT_QUOTE_CREATION_SPEC

@ CCT_QUOTE_CREATION_SPEC.sql

PRO # package specification script for CCT_CREATE_OPPORTUNITY_SPEC

@ CCT_CREATE_OPPORTUNITY_SPEC.sql

PRO # package specification script for CCT_CREATE_QUEUE_TASKS_SPEC

@ CCT_CREATE_QUEUE_TASKS_SPEC.sql

PRO # package specification script for CCT_CREATE_TASK_SPEC

@ CCT_CREATE_TASK_SPEC.sql

PRO # package specification script for CCT_QUOTE_PRICING_SPEC

@ CCT_QUOTE_PRICING_SPEC.sql

PRO # package specification script for CCT_QUOTE_PRICING_FINAL_SPEC

@ CCT_QUOTE_PRICING_FINAL_SPEC.sql

PRO # package specification script for CCT_AWARD_SPEC

@ CCT_AWARD_SPEC.sql

PRO # package specification script for CCT_COST_COMPARISON_SPEC

@ CCT_COST_COMPARISON_SPEC.sql

PRO # package specification script for FMW_RESUBMISSION_PKG_SPEC

@ FMW_RESUBMISSION_PKG_SPEC.sql

PRO ############################# PACKAGE BODY ############################################

PRO # package specification script for CCT_CREATE_ERRORS_BODY

@ CCT_CREATE_ERRORS_BODY.sql

PRO # package specification script for CCT_PROPERTIES_PKG_BODY

@ CCT_PROPERTIES_PKG_BODY.sql

PRO # package specification script for CCT_FETCH_FIELDS_BODY

@ CCT_FETCH_FIELDS_BODY.sql

PRO # package specification script for CCT_NOTIFICATION_PKG_BODY

@ CCT_NOTIFICATION_PKG_BODY.sql

PRO # package specification script for CCT_QUOTE_CREATION_BODY

@ CCT_QUOTE_CREATION_BODY.sql

PRO # package specification script for CCT_CREATE_OPPORTUNITY_BODY

@ CCT_CREATE_OPPORTUNITY_BODY.sql

PRO # package specification script for CCT_CREATE_QUEUE_TASKS_BODY

@ CCT_CREATE_QUEUE_TASKS_BODY.sql

PRO # package specification script for CCT_CREATE_TASK_BODY

@ CCT_CREATE_TASK_BODY.sql

PRO # package specification script for CCT_QUOTE_PRICING_BODY

@ CCT_QUOTE_PRICING_BODY.sql

PRO # package specification script for CCT_QUOTE_PRICING_FINAL_BODY

@ CCT_QUOTE_PRICING_FINAL_BODY.sql

PRO # package specification script for CCT_AWARD_BODY

@ CCT_AWARD_BODY.sql

PRO # package specification script for CCT_COST_COMPARISON_BODY

@ CCT_COST_COMPARISON_BODY.sql

PRO # package specification script for FMW_RESUBMISSION_PKG_BODY

@ FMW_RESUBMISSION_PKG_BODY.sql

PRO ############################# VIEWS ############################################

PRO # view creation script for CCT_QUOTE_FORMULA_SS_FINAL_V

@ CCT_QUOTE_FORMULA_SS_FINAL_V.sql

PRO ############################# PACKAGE SPECIFICATION ############################################

PRO # package specification script for CCT_APPROVAL_PROCESS_SPEC

@ CCT_APPROVAL_PROCESS_SPEC.sql

PRO ############################# PACKAGE BODY ############################################

PRO # package specification script for CCT_APPROVAL_PROCESS_BODY

@ CCT_APPROVAL_PROCESS_BODY.sql

PRO ############################# STORED PROCEDURES ############################################

PRO # procedure script for UPDATE_CUST_MASTER_CONTRACT

@ UPDATE_CUST_MASTER_CONTRACT.sql

PRO # procedure script for UPDATE_CCT_CRF_ASL_CODES

@ UPDATE_CCT_CRF_ASL_CODES.sql

PRO # procedure script for UPDATE_CCT_CRF_SALES_TEAM

@ UPDATE_CCT_CRF_SALES_TEAM.sql

PRO # procedure script for UPDATE_CCT_CRF_CUSTOMER_MASTER

@ UPDATE_CCT_CRF_CUSTOMER_MASTER.sql

PRO ############################# INSERT_SCRIPTS ############################################

PRO # insert script for INSERT_CCT_PARTS_QUEUE

@ INSERT_CCT_PARTS_QUEUE.sql

PRO # insert script for INSERT_CCT_PARTS_CLASS

@ INSERT_CCT_PARTS_CLASS.sql

PRO # insert script for INSERT_CCT_APPROVAL_RULE_VERSIONS

@ INSERT_CCT_APPROVAL_RULE_VERSIONS.sql

PRO # insert script for INSERT_CCT_APPROVAL_RULES

@ INSERT_CCT_APPROVAL_RULES.sql

PRO # insert script for INSERT_CCT_NOTIFICATION_TEMPLATE

@ INSERT_CCT_NOTIFICATION_TEMPLATE.sql

PRO # insert script for INSERT_CCT_NOTIFICATION_TYPE

@ INSERT_CCT_NOTIFICATION_TYPE.sql

PRO # insert script for INSERT_CCT_MAPPINGS

@ INSERT_CCT_MAPPINGS.sql

PRO # insert script for INSERT_CCT_ROLE_PROPERTIES

@ INSERT_CCT_ROLE_PROPERTIES.sql

PRO # insert script for INSERT_CCT_PROGRAM_COST_CAT_TYPES

@ INSERT_CCT_PROGRAM_COST_CAT_TYPES.sql

PRO # insert script for INSERT_CCT_PROGRAM_COST_CAT_SUBTYPES

@ INSERT_CCT_PROGRAM_COST_CAT_SUBTYPES.sql

PRO # insert script for INSERT_CCT_ROLE_USER_ACTION

@ INSERT_CCT_ROLE_USER_ACTION.sql

PRO # insert script for INSERT_CCT_NOTIFICATION_ROLE

@ INSERT_CCT_NOTIFICATION_ROLE.sql

PRO # insert script for INSERT_CCT_LOOK_UP

@ INSERT_CCT_LOOK_UP.sql

PRO # insert script for INSERT_CCT_CRF_CUSTOMER_MASTER

@ INSERT_CCT_CRF_CUSTOMER_MASTER.sql

PRO # insert script for CCT_PROPERTIES

@ PROD_Properties Script.sql

spool off
