set pages 0
set lines 300
set heading off
spool /tmp/sql/drop_objects1.sql
select 'drop table '||owner||'.'||table_name||' cascade constraints purge;'
from dba_tables
where owner = upper('&&owner')
union all
select 'drop '||object_type||' '||owner||'.'||object_name||';'
from dba_objects
where object_type in ('INDEX','PACKAGE','PACKAGE BODY','TRIGGER','SEQUENCE','TYPE','TYPE BODY','VIEW')
and owner = upper('&&owner')
order by 1;
spool off
