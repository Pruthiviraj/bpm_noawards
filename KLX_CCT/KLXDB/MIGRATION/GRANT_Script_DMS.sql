############################################################################################
#                                                                                          #
#  1.0.0 3-Nov-2017 Ankit Bhatt   Create User and GRANT Script (DB User password           #
#                                  differs based on instance)                              #
#                                                                                          #     
############################################################################################


CREATE USER xxdms IDENTIFIED BY xxdmsprd#2017 DEFAULT TABLESPACE XXDMS_TS
/

grant resource, connect to xxdms
/

grant all privileges to xxdms identified by xxdmsprd#2017
/

ALTER USER xxdms quota 100M on XXDMS_TS
/

