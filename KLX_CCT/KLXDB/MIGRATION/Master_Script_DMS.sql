############################################################################################
#                                                                                          #
#  1.0.0 09-Nov-2017 Ankit Bhatt   DB Migration Script for XXDMS schema                    #
#                                                                                          #     
############################################################################################

spool Migration_Script_Validations_DMS.log

SET HEADING ON
SET DEFINE OFF


PRO ############################# TABLES ############################################

PRO # Table creation script for DMS_FILE_TYPE

@ DMS_FILE_TYPE.sql

PRO # Table creation script for DMS_FOLDER

@ DMS_FOLDER.sql

PRO # Table creation script for DMS_OPPORTUNITY

@ DMS_OPPORTUNITY.sql

PRO # Table creation script for DMS_DOCUMENT

@ DMS_DOCUMENT.sql

PRO ############################# SEQUENCE ############################################

PRO # sequence creation script for DMS_DOCUMENT_SEQ

@ DMS_DOCUMENT_SEQ.sql

PRO # sequence creation script for DMS_OPPORTUNITY_SEQ

@ DMS_OPPORTUNITY_SEQ.sql

PRO ############################# INSERT_SCRIPTS ############################################

PRO # insert script for INSERT_DMS_FILE_TYPE

@ INSERT_DMS_FILE_TYPE.sql

PRO # insert script for INSERT_DMS_FOLDER

@ INSERT_DMS_FOLDER

PRO ############################# GRANT ############################################

@ GRANT_Table.sql

spool off
