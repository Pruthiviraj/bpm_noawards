############################################################################################
#                                                                                          #
#  1.0.0 23-Aug-2017 Ankit Bhatt   Create User and GRANT Script (DB User password          #
#                                  differs based on instance)                              #
#                                                                                          #     
############################################################################################


CREATE USER xxcct IDENTIFIED BY xxcctprd#2017 DEFAULT TABLESPACE XXCCT_TS
/

grant resource, connect to xxcct
/

grant all privileges to xxcct identified by xxcctprd#2017
/

/*************Privilages for SOAP Request*********************/

BEGIN
  DBMS_NETWORK_ACL_ADMIN.CREATE_ACL(acl         => 'webservice_xxcct.xml',
                                    description => 'to check new webservice',
                                    principal   => 'XXCCT',
                                    is_grant    => true,
                                    privilege   => 'connect');
 
  DBMS_NETWORK_ACL_ADMIN.ADD_PRIVILEGE(acl       => 'webservice_xxcct.xml',
                                       principal => 'XXCCT',
                                       is_grant  => true,
                                       privilege => 'resolve');
 
  DBMS_NETWORK_ACL_ADMIN.ASSIGN_ACL(acl  => 'webservice_xxcct.xml',
                                    host => '*'); 
END;
/
