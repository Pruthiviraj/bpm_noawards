
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "XXCCT"."CCT_AWARD_HEADERS_V" ("AWARD_HEADER_ID", "OPPORTUNITY_ID", "OPPORTUNITY_NUMBER", "TOTAL_PARTS", "TOTAL_QUOTED", "QUOTED_AND_AWARDED", "QUOTED_AND_NOTAWARDED", "NOTQUOTED_AND_AWARDED", "PCT_WIN") AS 
  SELECT
    award_header_id,
    opportunity_id,
    opportunity_number,
    total_parts,
    total_quoted,
    quoted_and_awarded,
    quoted_and_notawarded,
    notquoted_and_awarded,
    pct_win
 from cct_awards_summary;
