CREATE OR REPLACE FORCE EDITIONABLE VIEW "XXCCT"."CCT_TOP50_PROD_QUEUE_V" ("QUOTE_REVISION_ID", "QUEUE_NAME", "POC", "COUNT_PN_WITH_COST", "TOTAL_COST", "PCT_OF_TOTAL_COST") AS 
  SELECT a.quote_revision_id,
         a.queue_name,
         NULL,
         NVL(c.count_pn_with_cost,0),
         NVL(b.top50_queue_cost,0),
         CASE WHEN NVL(d.all_extended_cost,0) = 0 THEN 0 
         ELSE ROUND(100*(NVL(b.top50_queue_cost,0)/d.all_extended_cost),4) END 
   FROM (SELECT DISTINCT ctqp.quote_revision_id,
				cpq.queue_id,
				cpq.queue_name 
		   FROM cct_top50_parts_v ctqp CROSS JOIN
			    cct_parts_queue cpq 
          WHERE cpq.queue_id <> 9) a,  	
	    (SELECT ctqp.quote_revision_id,
                ctqp.queue_name,
                SUM(ctqp.total_cost) as top50_queue_cost 
           FROM cct_top50_parts_v ctqp 
       GROUP BY ctqp.quote_revision_id,
                ctqp.queue_name 
       ORDER BY ctqp.quote_revision_id,
                ctqp.queue_name) b,
        (SELECT ctqp.quote_revision_id,
                ctqp.queue_name,
                COUNT(ctqp.customer_pn) count_pn_with_cost
           FROM cct_top50_parts_v ctqp 
          WHERE ctqp.cost_used_for_quote IS NOT NULL 
       GROUP BY ctqp.quote_revision_id,
                ctqp.queue_name) c,
        (SELECT cqp.quote_revision_id,
                SUM(total_cost) all_extended_cost 
           FROM cct_quote_queue_v cqp 
       GROUP BY cqp.quote_revision_id) d         		
 WHERE  a.quote_revision_id = b.quote_revision_id(+) AND 
        a.queue_name = b.queue_name(+) AND 
        a.quote_revision_id = c.quote_revision_id(+) AND 
        a.queue_name = c.queue_name(+) AND 
		a.quote_revision_id = d.quote_revision_id  
ORDER BY a.quote_revision_id,
         a.queue_id;
