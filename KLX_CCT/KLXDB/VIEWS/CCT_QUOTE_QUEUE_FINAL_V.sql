
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "XXCCT"."CCT_QUOTE_QUEUE_FINAL_V" ("QUEUE_ID", "QUEUE_NAME", "QUOTE_REVISION_ID", "COUNT_PN_REQUESTED", "COUNT_PN_WITH_COST", "COUNT_PN_QUOTED", "TOTAL_COST", "TOTAL_RESALE", "GP") AS 
  SELECT  f.queue_id queue_id,
        f.queue_name queue_name,
        a.quote_revision_id quote_revision_id,
        SUM(NVL(c.count_pn_requested,0)) count_pn_requested,
        SUM(NVL(e.count_pn_with_cost,0)) count_pn_with_cost,
        SUM(NVL(d.count_pn_quoted,0)) count_pn_quoted,
        ROUND(SUM(NVL(a.total_cost,0)),2) total_cost,
        ROUND(SUM(NVL(a.total_resale,0)),2) total_resale,
        CASE WHEN SUM(NVL(a.total_resale,0)) <> 0 THEN
            ROUND(((SUM(NVL(a.total_resale,0)) - SUM(NVL(a.total_cost,0))) / SUM(NVL(a.total_resale,0))*100),2)
        ELSE 0 END "GP" 
 FROM   (SELECT cqpf.quote_revision_id,
				SUM(NVL(NVL(cqpf.SUGGESTED_COST_USED_FOR_QUOTE,cqpf.COST_USED_FOR_QUOTE),0) * NVL(cqpf.total_qty_len_contract,0)) total_cost,
				SUM(NVL(NVL(cqpf.SUGGESTED_RESALE,cqpf.RESALE),0) * NVL(cqpf.total_qty_len_contract,0)) total_resale,
				NVL(cqp.queue_id,10)	queue_id
		  FROM  cct_quote_parts_final cqpf LEFT JOIN 
				cct_quote_parts cqp 
			ON  cqp.quote_revision_id = cqpf.quote_revision_id AND 
				cqp.quoted_part_number = cqpf.quoted_part_number 
		GROUP BY cqpf.quote_revision_id,
				 cqp.queue_id) a,
		(SELECT co.opportunity_id,
				MAX(cqpf.quote_revision_id) quote_revision_id
		  FROM  cct_opportunity co,
				cct_quote cq,
				cct_quote_revisions cqr,
				cct_quote_parts_final cqpf
		 WHERE  co.opportunity_id = cq.opportunity_id AND
				cq.quote_id = cqr.quote_id AND
				cqr.revision_id = cqpf.quote_revision_id
		GROUP BY co.opportunity_id) b,		 
		(SELECT cqpf.quote_revision_id,
				COUNT(cqpf.customer_pn) count_pn_requested, 
				NVL(cqp.queue_id,10)	queue_id 
		  FROM  cct_quote_parts_final cqpf LEFT JOIN 
				cct_quote_parts cqp 
			ON  cqp.quote_revision_id = cqpf.quote_revision_id AND 
				cqp.quoted_part_number = cqpf.quoted_part_number 
		GROUP BY cqpf.quote_revision_id,
				 cqp.queue_id) c,
		(SELECT cqpf.quote_revision_id,
				COUNT(cqpf.customer_pn) count_pn_quoted, 
				NVL(cqp.queue_id,10) queue_id 
		  FROM  cct_quote_parts_final cqpf LEFT JOIN 
				cct_quote_parts cqp 
			ON  cqp.quote_revision_id = cqpf.quote_revision_id AND 
				cqp.quoted_part_number = cqpf.quoted_part_number 
		 WHERE  NVL(cqpf.SUGGESTED_COST_USED_FOR_QUOTE,cqpf.COST_USED_FOR_QUOTE) IS NOT NULL AND 
				NVL(cqpf.SUGGESTED_RESALE, cqpf.RESALE) IS NOT NULL 
		GROUP BY cqpf.quote_revision_id,
				 cqp.queue_id) d,
		(SELECT cqpf.quote_revision_id,
				COUNT(cqpf.customer_pn) count_pn_with_cost, 
				NVL(cqp.queue_id,10) queue_id 
		  FROM  cct_quote_parts_final cqpf LEFT JOIN 
				cct_quote_parts cqp 
			ON  cqp.quote_revision_id = cqpf.quote_revision_id AND 
				cqp.quoted_part_number = cqpf.quoted_part_number 
		 WHERE  NVL(cqpf.SUGGESTED_COST_USED_FOR_QUOTE,cqpf.COST_USED_FOR_QUOTE) IS NOT NULL  
		GROUP BY cqpf.quote_revision_id,
				 cqp.queue_id) e,
        (SELECT cpq.* 
           FROM (SELECT queue_id,
                        queue_name 
                   FROM cct_parts_queue 
                  UNION
                 SELECT 10 as queue_id,
                        'No Queue' as queue_name
                   FROM dual) cpq
        ORDER BY cpq.queue_id) f 
  WHERE a.quote_revision_id = b.quote_revision_id AND
        c.quote_revision_id = b.quote_revision_id AND
        d.quote_revision_id (+)  = a.quote_revision_id AND
        e.quote_revision_id (+)  = a.quote_revision_id AND
        a.queue_id = c.queue_id (+) AND
        a.queue_id = d.queue_id (+) AND
        a.queue_id = e.queue_id (+) AND 
        a.queue_id = f.queue_id
GROUP BY f.queue_id,
         f.queue_name,
         a.quote_revision_id
ORDER BY a.quote_revision_id,
         f.queue_id;
