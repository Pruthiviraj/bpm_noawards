CREATE VIEW CCT_AWARDS_V
AS SELECT
    a.opportunity_id,
    b.quote_revision_id,
    b.customer_number,
    b.customer_name,
    SUM(a.count_of_awarded_items) count_of_awarded_items,
    SUM(a.count_of_loaded_items) count_of_loaded_items,
    SUM(a.count_of_pending_items) count_of_pending_items,
    SUM(a.count_of_remove_items) count_of_remove_items
FROM
    (
        SELECT
            ca.opportunity_id,
            cqp.quote_revision_id,
            COUNT(ca.invoice_part_number) count_of_awarded_items,
            COUNT(
                CASE
                    WHEN ca.award_tags IN(
                        'Duplicate','Part Already on Contract','Loaded'
                    ) THEN ca.invoice_part_number
                END
            ) count_of_loaded_items,
            COUNT(
                CASE
                    WHEN ca.award_tags IN(
                        'OK to load','Multi-Value','Part Number Discrepancy','Blank Discrepancies - missing UOM,Item Class,Leadtime,Quantity','Parts Need to be Built'
,'Part Built by QA','Negative Margin'
                    ) THEN ca.invoice_part_number
                END
            ) count_of_pending_items,
            COUNT(
                CASE
                    WHEN ca.award_tags IN(
                        'Removed from Contract'
                    ) THEN ca.invoice_part_number
                END
            ) count_of_remove_items
        FROM
            cct_opportunity co,
            cct_quote cq,
            cct_quote_revisions cqr,
            cct_awards ca,
            cct_quote_parts cqp
        WHERE
                co.opportunity_id = cq.opportunity_id
            AND
                co.opportunity_id = ca.opportunity_id
            AND
                ca.invoice_part_number = cqp.customer_pn
            AND
                cq.quote_id = cqr.quote_id
            AND
                cqr.revision_id = cqp.quote_revision_id
        GROUP BY
            ca.opportunity_id,
            cqp.quote_revision_id
    ) a,
    (
        SELECT
            ca.opportunity_id,
            co.customer_number,
            co.customer_name,
            MAX(cqp.quote_revision_id) quote_revision_id
        FROM
            cct_opportunity co,
            cct_quote cq,
            cct_quote_revisions cqr,
            cct_awards ca,
            cct_quote_parts cqp
        WHERE
                co.opportunity_id = cq.opportunity_id
            AND
                co.opportunity_id = ca.opportunity_id
            AND
                ca.invoice_part_number = cqp.customer_pn
            AND
                cq.quote_id = cqr.quote_id
            AND
                cqr.revision_id = cqp.quote_revision_id
        GROUP BY
            ca.opportunity_id,
            co.customer_number,
            co.customer_name
    ) b
WHERE
    a.quote_revision_id = b.quote_revision_id
GROUP BY
    a.opportunity_id,
    b.quote_revision_id,
    b.customer_number,
    b.customer_name
/	
