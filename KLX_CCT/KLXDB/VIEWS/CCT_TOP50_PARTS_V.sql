
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "XXCCT"."CCT_TOP50_PARTS_V" ("QUOTE_REVISION_ID", "RANK", "LINE_IDENTIFIER", "CUSTOMER_PN", "QUOTED_PART_NUMBER", "ANNUAL_QTY", "TOTAL_QTY_LEN_CONTRACT", "UOM", "CERTIFIED_PART", "PRIME", "MANF_NAME", "COST_USED_FOR_QUOTE", "TOTAL_COST", "EXTENDED_COST", "IND_PERC_COST", "CUMULATIVE_PERC_COST", "INTERNAL_NOTES", "EXTERNAL_NOTES", "QUEUE_NAME") AS 
  SELECT    b.quote_revision_id, 
            b.cost_rank,
			b.line_identifier, 
			b.customer_pn,
            b.quoted_part_number,
            b.annual_qty,
			NVL(b.total_qty_len_contract,0),
			b.uom,
			b.certified_part,
			b.prime,
			b.manf_name,
			NVL(b.cost_used_for_quote,0),
            NVL(b.total_cost,0),
			NVL(b.total_cost,0),
			b.ind_perc_cost,
            SUM(b.ind_perc_cost) over (PARTITION BY b.quote_revision_id order by b.cost_rank) as CUM_PERC_COST,
			b.internal_notes,
			b.external_notes,
            b.queue_name  
      FROM  (SELECT a.*,
                    row_number() OVER (PARTITION BY a.quote_revision_id ORDER BY a.total_cost desc,a.line_identifier asc) as cost_rank, 
                    CASE WHEN NVL(a.tot_summary_cost,0) = 0 THEN 0 
                    ELSE ROUND(100*(a.total_cost/a.tot_summary_cost),2) END as ind_perc_cost --change to extended_cost  
               FROM (SELECT cqp.quote_revision_id, 
                            cqp.line_identifier, 
                            cqp.customer_pn,
                            cqp.quoted_part_number,
                            cqp.annual_qty,
                            NVL(suggested_total_qty_len_con,cqp.total_qty_len_contract) as total_qty_len_contract,
                            (NVL(NVL(cqp.suggested_cost_used_for_quote,cqp.cost_used_for_quote),0) * NVL(NVL(suggested_total_qty_len_con,cqp.total_qty_len_contract),0)) as total_cost,
                            cqp.uom,
                            cqp.certified_part, 
                            cqp.prime, 
                            cqp.manf_name, 
                            NVL(cqp.suggested_cost_used_for_quote,cqp.cost_used_for_quote) as cost_used_for_quote,
                            cqp.internal_notes, 
                            cqp.external_notes,
                            cqp.queue_id,
                            cpq.queue_name,
                            cqq.tot_summary_cost 
                       FROM cct_quote_parts cqp,
                            cct_parts_queue cpq, 
                            (SELECT quote_revision_id,
                                    SUM(total_cost) tot_summary_cost 
                               FROM cct_quote_queue_v 
                           GROUP BY quote_revision_id) cqq
                      WHERE cqp.queue_id = cpq.queue_id AND  
                            cqp.quote_revision_id = cqq.quote_revision_id AND 
                            cqp.queue_id <> 9 AND 
                            cqp.quote_Revision_id in (SELECT quote_revision_id from (SELECT co.opportunity_id,
                                                             MAX(cqr.revision_id) quote_revision_id
                                                        FROM cct_opportunity co,
                                                             cct_quote cq,
                                                             cct_quote_revisions cqr
                                                       WHERE co.opportunity_id = cq.opportunity_id AND
                                                             cq.quote_id = cqr.quote_id
                                                    GROUP BY co.opportunity_id))) a 
               WHERE  a.total_cost IS NOT NULL) b
   WHERE b.cost_rank<=50 
ORDER BY b.quote_revision_id,
         b.cost_rank;
