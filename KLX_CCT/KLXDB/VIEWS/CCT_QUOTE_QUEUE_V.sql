CREATE OR REPLACE FORCE EDITIONABLE VIEW "XXCCT"."CCT_QUOTE_QUEUE_V" ("QUEUE_GROUP", "QUOTE_REVISION_ID", "COUNT_PN_REQUESTED", "COUNT_PN_WITH_COST", "COUNT_PN_QUOTED", "TOTAL_COST", "TOTAL_RESALE", "GP") AS 
  SELECT  a.queue_group queue_group,
        a.quote_revision_id quote_revision_id,
        SUM(nvl(c.count_pn_requested,0)) count_pn_requested,
        SUM(nvl(e.count_pn_with_cost,0)) count_pn_with_cost,
        SUM(nvl(d.count_pn_quoted,0)) count_pn_quoted,
        ROUND(SUM(NVL(a.total_cost,0)),2) total_cost,
        ROUND(SUM(NVL(a.total_resale,0)),2) total_resale,
        CASE WHEN SUM(NVL(a.total_resale,0)) <> 0 THEN 
            ROUND(((SUM(NVL(a.total_resale,0)) - SUM(NVL(a.total_cost,0))) / SUM(NVL(a.total_resale,0))*100),2) 
        ELSE 0 END "GP"
 FROM  (SELECT  cpq.queue_name queue_group,
                cqp.quote_revision_id quote_revision_id,
                SUM(NVL(cqp.SUGGESTED_COST_USED_FOR_QUOTE,cqp.COST_USED_FOR_QUOTE) * NVL(suggested_TOTAL_QTY_LEN_CON,cqp.total_qty_len_contract)) total_cost,
                SUM(NVL(cqp.SUGGESTED_RESALE,cqp.RESALE) * NVL(suggested_TOTAL_QTY_LEN_CON,cqp.total_qty_len_contract)) total_resale 
          FROM  cct_parts_queue cpq, 
                cct_quote_parts cqp                
         WHERE  cpq.queue_id = cqp.queue_id 
       GROUP BY cqp.quote_revision_id,
                cpq.queue_name) a,
       (SELECT  co.opportunity_id,
                MAX(cqp.quote_revision_id) quote_revision_id 
          FROM  cct_opportunity co,
                cct_quote cq,
                cct_quote_revisions cqr,
                cct_quote_parts cqp,
                cct_parts_queue cpq 
         WHERE  co.opportunity_id = cq.opportunity_id AND 
                cq.quote_id = cqr.quote_id AND 
                cqr.revision_id = cqp.quote_revision_id AND 
                cqp.queue_id = cpq.queue_id 
       GROUP BY co.opportunity_id) b,
         (SELECT  cqp.quote_revision_id, 
                 cpq.queue_name queue_group,
                 COUNT(cqp.customer_pn) count_pn_requested
            FROM  cct_quote_parts cqp,
            cct_parts_queue cpq
            where cqp.queue_id = cpq.queue_id
        GROUP BY  cqp.quote_revision_id,cpq.queue_name) c,
        (SELECT  cqp.quote_revision_id, 
         cpq.queue_name queue_group,
                 COUNT(cqp.customer_pn) count_pn_quoted
 FROM  cct_quote_parts cqp,
            cct_parts_queue cpq
             where nvl(cqp.SUGGESTED_COST_USED_FOR_QUOTE,cqp.COST_USED_FOR_QUOTE) is not null
            and nvl(cqp.SUGGESTED_RESALE, cqp.RESALE) is not null
            and cqp.queue_id = cpq.queue_id
        GROUP BY  cqp.quote_revision_id,cpq.queue_name) d,
               (SELECT cqp.quote_revision_id,
                        cpq.queue_name queue_group,
                COUNT(cqp.customer_pn) count_pn_with_cost 
           FROM cct_quote_parts cqp,
           cct_parts_queue cpq
          WHERE nvl(cqp.suggested_cost_used_for_quote,cqp.cost_used_for_quote) is not null 
          and cqp.queue_id = cpq.queue_id
       GROUP BY cqp.quote_revision_id,cpq.queue_name) e 
  WHERE a.quote_revision_id = b.quote_revision_id AND 
       c.quote_revision_id = b.quote_revision_id and
       d.quote_revision_id (+)  = a.quote_revision_id and
       e.quote_revision_id (+)  = a.quote_revision_id AND
       a.queue_group = c.queue_group (+) AND
       a.queue_group = d.queue_group (+) AND
       a.queue_group = e.queue_group (+)
GROUP BY a.queue_group,
         a.quote_revision_id
ORDER BY a.quote_revision_id,
         a.queue_group;
