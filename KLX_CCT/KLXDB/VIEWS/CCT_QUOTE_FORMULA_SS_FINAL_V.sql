
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "XXCCT"."CCT_QUOTE_FORMULA_SS_FINAL_V" ("SNAPSHOT_ID", "QUOTE_REVISION_ID", "TOTAL_PRODUCT_COST_LOC", "TOTAL_PRODUCT_RESALE_LOC", "PRODUCT_GROSS_MARGIN", "PROJECT_NET_MARGIN", "TOTAL_PARTS_REQUESTED", "TOTAL_PARTS_QUOTED", "TOTAL_ADJUSTMENTS", "PRODUCT_CLASS_A_ITEM_COUNT", "PRODUCT_CLASS_A_COST", "PRODUCT_CLASS_A_RESALE", "PRODUCT_CLASS_A_MARGIN", "PRODUCT_CLASS_A1A_ITEM_COUNT", "PRODUCT_CLASS_A1A_COST", "PRODUCT_CLASS_A1A_RESALE", "PRODUCT_CLASS_A1A_MARGIN", "PRODUCT_CLASS_A1B_ITEM_COUNT", "PRODUCT_CLASS_A1B_COST", "PRODUCT_CLASS_A1B_RESALE", "PRODUCT_CLASS_A1B_MARGIN", "PRODUCT_CLASS_B_ITEM_COUNT", "PRODUCT_CLASS_B_COST", "PRODUCT_CLASS_B_RESALE", "PRODUCT_CLASS_B_MARGIN", "PRODUCT_CLASS_C_ITEM_COUNT", "PRODUCT_CLASS_C_COST", "PRODUCT_CLASS_C_RESALE", "PRODUCT_CLASS_C_MARGIN", "PRODUCT_CLASS_D_ITEM_COUNT", "PRODUCT_CLASS_D_COST", "PRODUCT_CLASS_D_RESALE", "PRODUCT_CLASS_D_MARGIN") AS 
  SELECT cs.snapshot_id,
        cs.quote_revision_id,
        nvl(cqp.total_cost,0) total_product_cost_loc,
        nvl(cqp.total_resale,0) total_product_resale_loc,
        nvl(cqp.parts_margin,0) product_gross_margin,
        nvl(cqp.net_margin,0) project_net_margin,
        nvl(cqp.total_no_of_parts_requested,0) total_parts_requested,
        nvl(cqp.total_no_of_parts_quoted,0) total_parts_quoted,
        nvl(cqp.total_adjustments,0) total_adjustments, 
        cct_quote_pricing_final.class_count_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'A') AS pn_quoted_a,
        cct_quote_pricing_final.class_cost_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'A') AS total_cost_a,
        cct_quote_pricing_final.class_resale_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'A') AS total_resale_a,
        cct_quote_pricing_final.class_margin_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'A') AS parts_margin_a,
        cct_quote_pricing_final.class_count_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'A1A') AS pn_quoted_a1a,
        cct_quote_pricing_final.class_cost_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'A1A') AS total_cost_a1a,
        cct_quote_pricing_final.class_resale_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'A1A') AS total_resale_a1a,
        cct_quote_pricing_final.class_margin_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'A1A') AS parts_margin_a1a,
        cct_quote_pricing_final.class_count_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'A1B') AS pn_quoted_a1b,
        cct_quote_pricing_final.class_cost_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'A1B') AS total_cost_a1b,
        cct_quote_pricing_final.class_resale_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'A1B') AS total_resale_a1b,
        cct_quote_pricing_final.class_margin_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'A1B') AS parts_margin_a1b,
        cct_quote_pricing_final.class_count_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'B') AS pn_quoted_b,
        cct_quote_pricing_final.class_cost_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'B') AS total_cost_b,
        cct_quote_pricing_final.class_resale_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'B') AS total_resale_b,
        cct_quote_pricing_final.class_margin_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'B') AS parts_margin_b,
        cct_quote_pricing_final.class_count_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'C') AS pn_quoted_c,
        cct_quote_pricing_final.class_cost_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'C') AS total_cost_c,
        cct_quote_pricing_final.class_resale_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'C') AS total_resale_c,
        cct_quote_pricing_final.class_margin_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'C') AS parts_margin_c,
        cct_quote_pricing_final.class_count_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'D') AS pn_quoted_d,
        cct_quote_pricing_final.class_cost_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'D') AS total_cost_d,
        cct_quote_pricing_final.class_resale_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'D') AS total_resale_d,
        cct_quote_pricing_final.class_margin_snapshot(cqp.snapshot_id,cqp.quote_revision_id,'D') AS parts_margin_d
   FROM cct_quote_pricing_ss_final_v cqp RIGHT JOIN 
        cct_snapshot cs 
     ON cs.snapshot_id = cqp.snapshot_id AND 
        cs.quote_revision_id = cqp.quote_revision_id 
ORDER BY cs.snapshot_id,
		 cs.quote_revision_id;
