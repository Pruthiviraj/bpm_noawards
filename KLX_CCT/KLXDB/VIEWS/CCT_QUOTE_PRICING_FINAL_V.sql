CREATE OR REPLACE FORCE EDITIONABLE VIEW "XXCCT"."CCT_QUOTE_PRICING_FINAL_V" ("TOTAL_RESALE", "TOTAL_COST", "TOTAL_ADJUSTMENTS", "CURRENCY", "QUOTE_REVISION_ID", "PARTS_MARGIN", "NET_MARGIN", "TOTAL_NO_OF_PARTS_REQUESTED", "TOTAL_NO_OF_PARTS_QUOTED", "TOTAL_NO_OF_PART_NOT_QUOTED") AS 
  SELECT ROUND(SUM(NVL(a.total_resale,0)),2) total_resale,
         ROUND(SUM(NVL(a.total_cost,0)),2) total_cost,
         ROUND(SUM(NVL(e.total_adjustments,0)),2) total_adjustments,
         e.currency,
         b.quote_revision_id,
         CASE WHEN SUM(NVL(a.total_resale,0)) <> 0  THEN
            ROUND(((SUM(NVL(a.total_resale,0)) - SUM(NVL(a.total_cost,0))) /
                   SUM(NVL(a.total_resale,0))*100),2)
         ELSE 0 END parts_margin,
         CASE WHEN (SUM(NVL(a.total_resale,0)) + SUM(NVL(e.service_fees,0))) <> 0 THEN
            ROUND((
                    (
                    (
                    ( SUM(NVL(e.service_fees,0)) - 
                    ( SUM(NVL(e.customer_rebate,0)) + SUM(NVL(e.shipping_fee_taxes,0)) + SUM(NVL(e.agent_commissions,0)) + SUM(NVL(e.total_program_costs,0))))
                    + SUM(NVL(a.total_resale,0))
                    ) - (SUM(NVL(a.total_cost,0)))
                    )
		 /((( SUM(NVL(e.service_fees,0)) - ( SUM(NVL(e.customer_rebate,0)) + SUM(NVL(e.shipping_fee_taxes,0)) + SUM(NVL(e.agent_commissions,0)) + SUM(NVL(e.total_program_costs,0)))) + SUM(NVL(a.total_resale,0))))
                 )*100,2)
         ELSE 0 END net_margin,
         SUM(NVL(c.total_no_of_parts_requested,0)) total_no_of_parts_requested,
         SUM(NVL(d.total_no_of_parts_quoted,0)) total_no_of_parts_quoted,
         (SUM(NVL(c.total_no_of_parts_requested,0))- SUM(NVL(d.total_no_of_parts_quoted,0))) total_no_of_part_not_quoted
    FROM (SELECT  SUM(NVL(cqp.resale,0) * NVL(cqp.total_qty_len_contract,0)) total_resale,
                  SUM(NVL(cqp.cost_used_for_quote,0) * NVL(cqp.total_qty_len_contract,0)) total_cost,
                  cqp.quote_revision_id
             FROM cct_quote_parts_final cqp,
                  cct_opportunity co,
                  cct_quote_revisions cqr,
                  cct_quote cq
            WHERE cqp.quote_revision_id = cqr.revision_id AND
                  cqr.quote_id = cq.quote_id AND
                  cq.opportunity_id = co.opportunity_id AND
                  cqp.resale IS NOT NULL AND -- exclude items for which there are no resale
                  (cqp.queue_id NOT IN (6,8) or cqp.queue_id IS NULL) AND 
				  --added constraint for not considering parts from 'No Bid' and 'Unknown'
                  ((cqp.item_tag <> '5.0' AND
                    cqp.item_tag <> '5') OR
                    (cqp.item_tag IS NULL)) 
				  --added constraint for excluding parts with item_tag 5 for both alphanumeric and number values of item tag
         GROUP BY cqp.quote_revision_id) a,
         (SELECT  co.opportunity_id,
                  MAX(cqr.revision_id) quote_revision_id
            FROM  cct_opportunity co,
                  cct_quote cq,
                  cct_quote_revisions cqr
           WHERE  co.opportunity_id = cq.opportunity_id AND
                  cq.quote_id = cqr.quote_id
        GROUP BY  co.opportunity_id) b,
         (SELECT  cqp.quote_revision_id,
                  COUNT(cqp.customer_pn) total_no_of_parts_requested
            FROM  cct_quote_parts_final cqp
        GROUP BY  cqp.quote_revision_id) c,
         (SELECT  cqp.quote_revision_id,
                  COUNT(cqp.customer_pn) total_no_of_parts_quoted
            FROM  cct_quote_parts_final cqp
           WHERE  cqp.cost_used_for_quote IS NOT NULL AND
                  cqp.resale IS NOT NULL
        GROUP BY  cqp.quote_revision_id) d,
         (SELECT  co.opportunity_id,
                  co.currency,
                  SUM(NVL(cpch.service_fees,0)) service_fees,
			/*
                  ((SUM(NVL(cpch.customer_rebate,0) +  NVL(cpch.shipping_fee_taxes,0) +
                      NVL(cpch.agent_commissions,0) + NVL(cpch.total_program_costs,0))) -
                  SUM(NVL(cpch.service_fees,0))) total_adjustments, 
				  -- modified total adjustments formula by subtracting service fees
            */			
              (SUM(NVL(cpch.service_fees,0)) - SUM((NVL(cpch.total_program_costs,0)+ NVL(cpch.customer_rebate,0) + NVL(cpch.agent_commissions,0) + 
                  NVL(cpch.shipping_fee_taxes,0)))) total_adjustments, --CHG0036547#modified total adjustments formula by subtracting service fees
                  SUM(NVL(cpch.total_program_costs,0)) total_program_costs,
                  SUM(NVL(cpch.customer_rebate,0)) customer_rebate,
                  SUM(NVL(cpch.agent_commissions,0)) agent_commissions,
                  SUM(NVL(cpch.shipping_fee_taxes,0)) shipping_fee_taxes 
            FROM  cct_opportunity co,
                  cct_program_cost_header cpch
           WHERE  co.opportunity_id = cpch.opportunity_id
        GROUP BY  co.opportunity_id,co.currency) e
WHERE  a.quote_revision_id(+) = b.quote_revision_id AND
       a.quote_revision_id = c.quote_revision_id(+) AND
       a.quote_revision_id = d.quote_revision_id(+) AND
       b.opportunity_id = e.opportunity_id(+)
GROUP BY e.currency,
         b.quote_revision_id;
