CREATE OR REPLACE FORCE EDITIONABLE VIEW "XXCCT"."CCT_QUOTE_CLASS_FINAL_V" ("ITEM_CLASS", "QUOTE_REVISION_ID", "PN_QUOTED", "TOTAL_COST", "TOTAL_RESALE", "PARTS_MARGIN", "PCT_OF_TOTAL_COST") AS 
  SELECT a.item_class,
         a.quote_revision_id,
         SUM(nvl(c.pn_quoted,0)) pn_quoted,
         round(SUM(nvl(a.total_cost,0)),2) total_cost,
         round(SUM(nvl(a.total_resale,0)),2) total_resale,
		 CASE WHEN SUM(nvl(a.total_resale,0)) <> 0 THEN 
			round(((SUM(nvl(a.total_resale,0)) - SUM(nvl(a.total_cost,0))) / 
				   SUM(nvl(a.total_resale,0))) * 100,2)
		 ELSE 0 END parts_margin,
         round(100* RATIO_TO_REPORT(SUM(nvl(a.total_cost,0))) over (partition by a.quote_revision_id),2) pct_of_total_cost
    FROM (SELECT cqp.calc_abc item_class,
                 cqp.quote_revision_id,
                 SUM(cqp.cost_used_for_quote * cqp.total_qty_len_contract) total_cost,
                 SUM(cqp.resale * cqp.total_qty_len_contract) total_resale
            FROM cct_quote_parts_final cqp
           WHERE (cqp.queue_id NOT IN (6,8) OR
                  cqp.queue_id IS NULL) AND --added constraint for not considering parts from 'No Bid' and 'Unknown'
                 ((cqp.item_tag <> '5.0' AND cqp.item_tag <> '5') OR (cqp.item_tag IS NULL)) AND --added constraint for excluding parts with item_tag 5 for both alphanumeric and number values of item tag  
                 cqp.resale IS NOT NULL -- exclude items for which there are no resale
        GROUP BY cqp.calc_abc,
                 cqp.quote_revision_id ) a,
         (SELECT co.opportunity_id,
                 MAX(cqp.quote_revision_id) quote_revision_id
            FROM cct_quote_parts_final cqp,
                 cct_opportunity co,
                 cct_quote cq,
                 cct_quote_revisions cqr
           WHERE co.opportunity_id = cq.opportunity_id AND 
                 cq.quote_id = cqr.quote_id AND 
                 cqr.revision_id = cqp.quote_revision_id
        GROUP BY co.opportunity_id) b,
         (SELECT cqp.quote_revision_id, 
                 cqp.calc_abc item_class,
                 COUNT(cqp.customer_pn) pn_quoted
            FROM cct_quote_parts_final cqp
           WHERE cqp.cost_used_for_quote IS NOT NULL AND 
		         cqp.resale IS NOT NULL
        GROUP BY cqp.quote_revision_id,cqp.calc_abc) c
 WHERE a.quote_revision_id = b.quote_revision_id AND 
       a.quote_revision_id = c.quote_revision_id(+) AND
       a.item_class = c.item_class (+) 
GROUP BY a.item_class,
         a.quote_revision_id 
ORDER BY a.quote_revision_id,
         a.item_class;
