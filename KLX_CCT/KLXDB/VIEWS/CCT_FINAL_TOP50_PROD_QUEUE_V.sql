
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "XXCCT"."CCT_FINAL_TOP50_PROD_QUEUE_V" ("QUOTE_REVISION_ID", "QUEUE_NAME", "POC", "COUNT_PN_WITH_COST", "TOTAL_COST", "TOTAL_RESALE", "PARTS_MARGIN", "PCT_OF_TOTAL_COST") AS 
  SELECT a.quote_revision_id,
       a.queue_name,
       NULL,
       NVL(c.count_pn_with_cost,0),
       NVL(b.top50_queue_cost,0),
       NVL(b.top50_queue_resale,0),
       CASE WHEN NVL(b.top50_queue_resale,0) <> 0 THEN
                ROUND(((NVL(b.top50_queue_resale,0) - NVL(b.top50_queue_cost,0)) / NVL(b.top50_queue_resale,0)*100),2)
       ELSE 0 END,
       CASE WHEN NVL(d.all_extended_cost,0) = 0 THEN 0 
       ELSE ROUND(100*(NVL(b.top50_queue_cost,0)/d.all_extended_cost),4) END 
  FROM (SELECT  DISTINCT cftp.quote_revision_id,
				cpq.queue_id,
				cpq.queue_name 
		   FROM cct_final_top50_parts_v cftp CROSS JOIN
			    (SELECT queue_id,
                        queue_name 
                   FROM cct_parts_queue 
                  WHERE queue_id <> 9) cpq 
          WHERE cftp.queue_name <> 'No Queue'
       UNION 
        SELECT  DISTINCT cftp.quote_revision_id,
				10,
				'No Queue'  
		   FROM cct_final_top50_parts_v cftp 
          WHERE cftp.queue_name = 'No Queue' ) a,
       (SELECT  cftp.quote_revision_id,
                cftp.queue_name,
                SUM(cftp.total_cost) as top50_queue_cost,
                SUM(cftp.extended_cost) as top50_ext_queue_cost, 
                SUM(cftp.total_resale) as top50_queue_resale 
          FROM  cct_final_top50_parts_v cftp 
      GROUP BY  cftp.quote_revision_id,
                cftp.queue_name 
      ORDER BY  cftp.quote_revision_id,
                cftp.queue_name) b,
       (SELECT  cftp.quote_revision_id,
                cftp.queue_name,
                COUNT(cftp.customer_pn) count_pn_with_cost
          FROM  cct_final_top50_parts_v cftp 
         WHERE  cftp.cost_used_for_quote IS NOT NULL 
       GROUP BY cftp.quote_revision_id,
                cftp.queue_name) c,
       (SELECT  cqqf.quote_revision_id,
                SUM(cqqf.total_cost) all_extended_cost 
          FROM  cct_quote_queue_final_v cqqf 
      GROUP BY  cqqf.quote_revision_id) d 
 WHERE  a.quote_revision_id = b.quote_revision_id(+) AND 
        a.queue_name = b.queue_name(+) AND 
        a.quote_revision_id = c.quote_revision_id(+) AND 
        a.queue_name = c.queue_name(+) AND 
        a.quote_revision_id = d.quote_revision_id 
ORDER BY a.quote_revision_id,
         a.queue_id;
