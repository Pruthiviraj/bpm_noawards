CREATE OR REPLACE FORCE EDITIONABLE VIEW "XXCCT"."CCT_FINAL_TOP50_PARTS_V" ("QUOTE_REVISION_ID", "RANK", "LINE_IDENTIFIER", "CUSTOMER_PN", "QUOTED_PART_NUMBER", "ANNUAL_QTY", "TOTAL_QTY_LEN_CONTRACT", "UOM", "CERTIFIED_PART", "PRIME", "MANF_NAME", "COST_USED_FOR_QUOTE", "EXTENDED_COST", "TOTAL_COST", "RESALE", "EXTENDED_RESALE", "TOTAL_RESALE", "MARGIN", "IND_PERC_COST", "CUMULATIVE_PERC_COST", "IND_PERC_RESALE", "CUMULATIVE_PERC_RESALE", "INTERNAL_NOTES", "EXTERNAL_NOTES", "QUEUE_NAME") AS 
SELECT    b.quote_revision_id,
            b.cost_rank,
			b.line_identifier,
			b.customer_pn,
            b.quoted_part_number,
            b.annual_qty,
			NVL(b.total_qty_len_contract,0),
			b.uom,
			b.certified_part,
			b.prime,
			b.manf_name,
			NVL(b.cost_used_for_quote,0),
            NVL(b.total_cost,0),
            NVL(b.total_cost,0),
            NVL(b.resale,0),
            NVL(b.total_resale,0),
            NVL((b.total_resale),0),
            CASE WHEN NVL(b.total_resale,0) <> 0 THEN
                ROUND(((NVL(b.total_resale,0) - NVL(b.total_cost,0)) / NVL(b.total_resale,0)*100),2)
            ELSE 0 END,
            b.ind_perc_cost,
			SUM(b.ind_perc_cost) over (PARTITION BY b.quote_revision_id order by b.cost_rank) as CUM_PERC_COST,
            b.ind_perc_resale,--change to extended_cost
			SUM(b.ind_perc_resale) over (PARTITION BY b.quote_revision_id order by b.cost_rank) as CUM_PERC_RESALE,
			b.internal_notes,
			b.external_notes,
            b.queue_name
      FROM  (SELECT co.opportunity_id,
                    MAX(cqr.revision_id) quote_revision_id
               FROM cct_opportunity co,
                    cct_quote cq,
                    cct_quote_revisions cqr
              WHERE co.opportunity_id = cq.opportunity_id AND
                    cq.quote_id = cqr.quote_id
           GROUP BY co.opportunity_id) a,
            (SELECT cqpf.quote_revision_id,
					cqpf.line_identifier,
					cqpf.customer_pn,
                    cqpf.quoted_part_number,
					cqpf.annual_qty,
                    cqpf.total_qty_len_contract,
                    cqpf.uom,
					cqpf.certified_part,
					cqpf.prime,
					cqpf.manf_name,
					NVL(cqpf.suggested_cost_used_for_quote,cqpf.cost_used_for_quote) as cost_used_for_quote,
                    (NVL(NVL(cqpf.suggested_cost_used_for_quote,cqpf.cost_used_for_quote),0) * NVL(cqpf.total_qty_len_contract,0)) as total_cost,
                    NVL(NVL(cqpf.suggested_resale,cqpf.resale),0) as resale,
                    (NVL(NVL(cqpf.suggested_resale,cqpf.resale),0) * NVL(cqpf.total_qty_len_contract,0)) as total_resale,
					cqpf.internal_notes,
					cqpf.external_notes,
					NVL(cqps.queue_name,'No Queue') queue_name,
                    ROW_NUMBER() OVER (PARTITION BY cqpf.quote_revision_id ORDER BY cqpf.ext_cost_value_rank asc) as cost_rank,
                    CASE WHEN NVL(cqq.tot_summary_cost,0) = 0 THEN 0
                    ELSE
                        ROUND(100*((NVL(NVL(cqpf.suggested_cost_used_for_quote,cqpf.cost_used_for_quote),0) * NVL(cqpf.total_qty_len_contract,0))/cqq.tot_summary_cost),2) END as ind_perc_cost, --change to extended_cost
                    CASE WHEN NVL(cqq.tot_summary_resale,0) = 0 THEN 0
                    ELSE
                        ROUND(100*((NVL(NVL(cqpf.suggested_resale,cqpf.resale),0) * NVL(cqpf.total_qty_len_contract,0))/cqq.tot_summary_resale),2) END as ind_perc_resale
              FROM  (SELECT cpq.queue_name,
                            cqp.quote_revision_id,
                            cqp.quoted_part_number,
                            cqp.customer_pn --added for CHG0034859	INC0138211-TOP 50 duplicates
                       FROM cct_quote_parts cqp,
                            cct_parts_queue cpq
                      WHERE cpq.queue_id = cqp.queue_id AND
                            cqp.queue_id <> 9) cqps RIGHT JOIN
                    cct_quote_parts_final cqpf
                ON  cqps.quote_revision_id = cqpf.quote_revision_id AND
                    cqps.customer_pn = cqpf.customer_pn AND --added for CHG0034859	INC0138211-TOP 50 duplicates
                    cqps.quoted_part_number = cqpf.quoted_part_number JOIN
                    (SELECT quote_revision_id,
                            SUM(total_cost) tot_summary_cost,
                            SUM(total_resale) tot_summary_resale
                       FROM cct_quote_queue_final_v
                   GROUP BY quote_revision_id) cqq
                ON cqpf.quote_revision_id = cqq.quote_revision_id
             WHERE NVL(cqpf.ext_cost_value_rank,0) <> 0) b
   WHERE a.quote_revision_id = b.quote_revision_id AND
         b.cost_rank<=50
ORDER BY b.quote_revision_id,
         b.cost_rank;
