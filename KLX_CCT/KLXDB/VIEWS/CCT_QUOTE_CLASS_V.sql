CREATE OR REPLACE FORCE EDITIONABLE VIEW "XXCCT"."CCT_QUOTE_CLASS_V" ("ITEM_CLASS", "QUOTE_REVISION_ID", "PN_QUOTED", "TOTAL_COST", "TOTAL_RESALE", "PARTS_MARGIN", "PCT_OF_TOTAL_COST") AS 
  SELECT  a.item_class, 
            a.quote_revision_id, 
            SUM(NVL(c.pn_quoted,0)) pn_quoted,
            ROUND(SUM(NVL(a.total_cost,0)),2) total_cost,
            ROUND(SUM(NVL(a.total_resale,0)),2) total_resale,
            CASE WHEN SUM(NVL(a.total_resale,0)) <> 0 THEN 
                ROUND(((SUM(NVL(a.total_resale,0))-SUM(NVL(a.total_cost,0))) / 
                SUM(NVL(a.total_resale,0)))*100,2)
            ELSE 0 END parts_margin,
            round(100* RATIO_TO_REPORT(SUM(nvl(
                a.total_cost,
                0
            ) ) ) over (partition by a.quote_revision_id),2) pct_of_total_cost
      FROM  (SELECT nvl(cqp.SUGGESTED_CALC_ABC,cqp.CALC_ABC) item_class,
                    cqp.quote_revision_id,
                    SUM(nvl(cqp.SUGGESTED_COST_USED_FOR_QUOTE,cqp.COST_USED_FOR_QUOTE) * nvl(cqp.suggested_TOTAL_QTY_LEN_CON,cqp.total_qty_len_contract)) total_cost,
                    SUM(nvl(cqp.SUGGESTED_RESALE,cqp.RESALE) * nvl(cqp.suggested_TOTAL_QTY_LEN_CON,cqp.total_qty_len_contract)) total_resale 
               FROM cct_quote_parts cqp 
              WHERE cqp.queue_id not in (6,8) --add constraint for not considering parts from 'No Bid' and 'Unknown'
           GROUP BY nvl(cqp.SUGGESTED_CALC_ABC,cqp.CALC_ABC),
                    cqp.quote_revision_id)a,
            (SELECT co.opportunity_id,
                    MAX(cqp.quote_revision_id) quote_revision_id 
               FROM cct_quote_parts cqp,
                    cct_opportunity co,
                    cct_quote cq,
                    cct_quote_revisions cqr
              WHERE co.opportunity_id = cq.opportunity_id AND 
                    cq.quote_id = cqr.quote_id AND 
                    cqr.revision_id = cqp.quote_revision_id 
           GROUP BY co.opportunity_id) b,
                   (SELECT  cqp.quote_revision_id, 
                 nvl(cqp.suggested_calc_abc,cqp.calc_abc) item_class,
                COUNT(cqp.customer_pn) 
             pn_quoted
            FROM  CCT_QUOTE_PARTS cqp
            where nvl(cqp.suggested_COST_USED_FOR_QUOTE,cqp.COST_USED_FOR_QUOTE) is not null
            and nvl(cqp.suggested_RESALE,cqp.RESALE) is not null
        GROUP BY  cqp.quote_revision_id,nvl(cqp.suggested_calc_abc,cqp.calc_abc)) c
 WHERE  a.quote_revision_id = b.quote_revision_id AND 
       a.quote_revision_id = c.quote_revision_id(+) and
       a.item_class = c.item_class (+)
 GROUP BY  a.item_class, 
           a.quote_revision_id
 ORDER BY  a.quote_revision_id,
           a.item_class;
