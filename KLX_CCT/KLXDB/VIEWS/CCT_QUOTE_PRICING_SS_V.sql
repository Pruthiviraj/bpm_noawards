CREATE OR REPLACE FORCE EDITIONABLE VIEW "XXCCT"."CCT_QUOTE_PRICING_SS_V" ("TOTAL_RESALE", "TOTAL_COST", "TOTAL_ADJUSTMENTS", "CURRENCY", "SNAPSHOT_ID", "QUOTE_REVISION_ID", "PARTS_MARGIN", "NET_MARGIN", "TOTAL_NO_OF_PARTS_REQUESTED", "TOTAL_NO_OF_PARTS_QUOTED", "TOTAL_NO_OF_PART_NOT_QUOTED", "TOTAL_NO_OF_PARTS_WITH_COST") AS 
  SELECT ROUND(NVL(SUM(a.total_resale),0),2) total_resale,
         ROUND(NVL(SUM(a.total_cost),0),2) total_cost,
         ROUND(NVL(SUM(f.total_adjustments),0),2) total_adjustments,
         f.currency currency,
         b.snapshot_id,
         b.quote_revision_id,
         CASE WHEN SUM(a.total_resale) <> 0 THEN
              ROUND((NVL((SUM(a.total_resale) - SUM(a.total_cost)) / SUM(a.total_resale),0)) * 100,2)
         ELSE 0 END parts_margin,
         CASE WHEN (SUM(NVL(a.total_resale,0))+ SUM(NVL(f.service_fees,0))) <> 0 THEN
              ROUND((((SUM(NVL(a.total_resale,0)) + SUM(NVL(f.service_fees,0))) -
                      (SUM(NVL(a.total_cost,0)) + SUM(NVL(f.total_program_costs,0)) + 
                              SUM(NVL(f.customer_rebate,0)) + SUM(NVL(f.agent_commissions,0)) + 
                              SUM(NVL(f.shipping_fee_taxes,0)))) /
                     (SUM(NVL(a.total_resale,0)) + SUM(NVL(f.service_fees,0))) * 100),2)
         ELSE 0 END net_margin,
         SUM(NVL(c.total_no_of_parts_requested,0)) total_no_of_parts_requested,
         SUM(NVL(d.total_no_of_parts_quoted,0)) total_no_of_parts_quoted,
         (SUM(NVL(c.total_no_of_parts_requested,0)) - SUM(NVL(d.total_no_of_parts_quoted,0))) total_no_of_part_not_quoted,
         SUM(NVL(e.total_no_of_parts_with_cost,0)) total_no_of_parts_with_cost
    FROM (SELECT SUM(NVL(cqp.suggested_resale,cqp.resale) *
                     NVL(cqp.suggested_total_qty_len_con,cqp.total_qty_len_contract)) total_resale,
                 SUM(NVL(cqp.suggested_cost_used_for_quote,cqp.cost_used_for_quote) *
                     NVL(cqp.suggested_total_qty_len_con,cqp.total_qty_len_contract)) total_cost,
                 cqp.snapshot_id snapshot_id,
                 cqp.quote_revision_id quote_revision_id
            FROM cct_quote_parts_snapshot cqp,
                 cct_opportunity_snapshot co,
                 cct_quote_revisions cqr,
                 cct_quote cq
           WHERE cqp.quote_revision_id = cqr.revision_id AND
                 cqr.quote_id = cq.quote_id AND
                 cq.opportunity_id = co.opportunity_id AND
                 co.snapshot_id = cqp.snapshot_id AND
                 cqp.queue_id NOT IN (6,8) 
				 --added constraint for not considering parts from 'No Bid' and 'Unknown'
        GROUP BY co.currency,
                 cqp.snapshot_id,
                 cqp.quote_revision_id) a,
         (SELECT cs.opportunity_id,
                 cs.quote_revision_id,
                 MAX(cs.snapshot_id) snapshot_id
            FROM cct_snapshot cs
           WHERE upper(snapshot_type) <> 'APPROVAL'
        GROUP BY cs.opportunity_id,
                 cs.quote_revision_id) b,
         (SELECT cqp.snapshot_id,
                 cqp.quote_revision_id,
                 COUNT(cqp.customer_pn) total_no_of_parts_requested
            FROM cct_quote_parts_snapshot cqp
        GROUP BY cqp.snapshot_id,
                 cqp.quote_revision_id) c,
         (SELECT cqp.snapshot_id,
                 cqp.quote_revision_id,
                 COUNT(cqp.customer_pn) total_no_of_parts_quoted
            FROM cct_quote_parts_snapshot cqp
           WHERE NVL(cqp.suggested_cost_used_for_quote,cqp.cost_used_for_quote) IS NOT NULL AND
                 NVL(cqp.suggested_resale,cqp.resale) IS NOT NULL
        GROUP BY cqp.snapshot_id,
                 cqp.quote_revision_id) d,
         (SELECT cqp.snapshot_id,
                 cqp.quote_revision_id,
                 COUNT(cqp.customer_pn) total_no_of_parts_with_cost
            FROM cct_quote_parts_snapshot cqp
           WHERE NVL(cqp.suggested_cost_used_for_quote,cqp.cost_used_for_quote) IS NOT NULL
        GROUP BY cqp.snapshot_id,
                 cqp.quote_revision_id) e,
         (SELECT cs.snapshot_id,
                 co.opportunity_id,
                 co.currency,
                 SUM(NVL(cpch.service_fees,0)) service_fees,
                 /* (SUM(NVL(cpch.customer_rebate,0) + NVL(cpch.shipping_fee_taxes,0) +
                      NVL(cpch.agent_commissions,0) + NVL(cpch.total_program_costs,0)) -
                  SUM(NVL(cpch.service_fees,0))) total_adjustments, 
				  -- modified total adjustments formula by subtracting service fees
                */  
                  (SUM(NVL(cpch.service_fees,0)) - SUM((NVL(cpch.total_program_costs,0)+ NVL(cpch.customer_rebate,0) + NVL(cpch.agent_commissions,0) + 
                  NVL(cpch.shipping_fee_taxes,0)))) total_adjustments, --CHG0036547#modified total adjustments formula by subtracting service fees  
                 SUM(NVL(cpch.total_program_costs,0)) total_program_costs,
                 SUM(NVL(cpch.customer_rebate,0)) customer_rebate,
                 SUM(NVL(cpch.agent_commissions,0)) agent_commissions,
                 SUM(NVL(cpch.shipping_fee_taxes,0)) shipping_fee_taxes  
            FROM cct_snapshot cs,
                 cct_opportunity_snapshot co,
                 cct_program_cost_header cpch
           WHERE cpch.opportunity_id = co.opportunity_id AND
                 cs.snapshot_id = co.snapshot_id AND
                 UPPER(cs.snapshot_type) <> 'APPROVAL'
        GROUP BY cs.snapshot_id,
                 co.opportunity_id,
                 co.currency) f
    WHERE a.quote_revision_id(+) = b.quote_revision_id AND
          a.snapshot_id(+) = b.snapshot_id AND
          a.quote_revision_id = c.quote_revision_id(+) AND
          b.snapshot_id = c.snapshot_id(+) AND
          a.quote_revision_id = d.quote_revision_id(+) AND
          b.snapshot_id = d.snapshot_id(+) AND
          b.quote_revision_id = e.quote_revision_id(+) AND
          b.snapshot_id = e.snapshot_id(+) AND
          b.opportunity_id = f.opportunity_id(+) AND 
          b.snapshot_id = f.snapshot_id(+)
GROUP BY b.snapshot_id,
         f.currency,
         b.quote_revision_id
ORDER BY b.snapshot_id,
         b.quote_revision_id;
