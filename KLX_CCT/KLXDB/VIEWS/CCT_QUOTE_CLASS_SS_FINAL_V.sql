CREATE OR REPLACE FORCE EDITIONABLE VIEW "XXCCT"."CCT_QUOTE_CLASS_SS_FINAL_V" ("SNAPSHOT_ID", "ITEM_CLASS", "QUOTE_REVISION_ID", "PN_QUOTED", "TOTAL_COST", "TOTAL_RESALE", "PARTS_MARGIN", "PCT_OF_TOTAL_COST") AS 
  SELECT a.snapshot_id,
         a.item_class,
         a.quote_revision_id,
         SUM(nvl(c.pn_quoted,0)) pn_quoted,
         round(SUM(a.total_cost),2) total_cost,
         round(nvl(SUM(a.total_resale),0),2) total_resale,
         CASE WHEN SUM(nvl(a.total_resale,0)) <> 0 THEN 
			round(((SUM(nvl(a.total_resale,0)) - SUM(nvl(a.total_cost,0))) / 
			       SUM(nvl(a.total_resale,0))) * 100,2)
         ELSE 0 END parts_margin,
         round(100* RATIO_TO_REPORT(SUM(nvl(
                a.total_cost,
                0
            ) ) ) over (partition by a.quote_revision_id),2) pct_of_total_cost
    FROM (SELECT cqp.snapshot_id,
                 cqp.calc_abc item_class,
                 cqp.quote_revision_id,
                 SUM(cqp.cost_used_for_quote * cqp.total_qty_len_contract) total_cost,
                 SUM(cqp.resale * cqp.total_qty_len_contract) total_resale
            FROM cct_quote_parts_snapshot_final cqp
           WHERE (cqp.queue_id NOT IN (6,8) OR 
                  cqp.queue_id IS NULL) --added for not considering parts from 'No Bid' and 'Unknown'
             AND ((cqp.item_tag <> '5.0' AND 
			       cqp.item_tag <> '5') OR 
				   (cqp.item_tag is null)) AND --added for excluding parts with item_tag 5 for alphanumeric and number values of item tag  
                 cqp.resale is not null -- exclude items for which there are no resale
        GROUP BY cqp.snapshot_id,
                 cqp.calc_abc,
                 cqp.quote_revision_id) a,
         (SELECT co.opportunity_id,
                 cqp.quote_revision_id quote_revision_id,
                 cs.snapshot_id
            FROM cct_quote_parts_snapshot_final cqp,
                 cct_opportunity_snapshot co,
                 cct_snapshot cs
           WHERE co.opportunity_id = cs.opportunity_id AND
                 cs.quote_revision_id = cqp.quote_revision_id 
        GROUP BY co.opportunity_id,
                 cqp.quote_revision_id,
                 cs.snapshot_id) b,
         (SELECT cqp.quote_revision_id,
                 cqp.snapshot_id,
                 cqp.calc_abc item_class,
                 COUNT(cqp.customer_pn) pn_quoted
            FROM cct_quote_parts_snapshot_final cqp
           WHERE cqp.cost_used_for_quote IS NOT NULL AND
                 cqp.resale IS NOT NULL 
        GROUP BY cqp.quote_revision_id,
                 snapshot_id,
                 calc_abc) c
   WHERE a.quote_revision_id = b.quote_revision_id AND 
         a.quote_revision_id = c.quote_revision_id (+) AND
         a.snapshot_id = b.snapshot_id AND 
         a.snapshot_id = c.snapshot_id (+) AND 
         a.item_class = c.item_class (+)
GROUP BY a.snapshot_id,
         a.quote_revision_id,
         a.item_class
ORDER BY a.snapshot_id,
         a.quote_revision_id,
         a.item_class;
