package com.klx.common.logger;


import java.util.logging.Level;

import oracle.adf.share.logging.ADFLogger;


/******************************************************************************
 This is  KLXLogger class which provides logging support for the application,
 to be usedas a static reference for logging functional points, exceptions
 and warnings.
 layer.
 *@author Infosys
 *@version 1.0
 *****************************************************************************/
public final class KLXLogger {


    private static ADFLogger aDFLogger;
    private static KLXLogger klxLogger;

    /**private contructor to avoid instantiation and implement singleton
     *
     */
    private KLXLogger() {
        aDFLogger = ADFLogger.createADFLogger("KLXLogger.class");
        aDFLogger.setLevel(Level.ALL);

    }

    /**Method to return KLXLogger instance
     *
     * @return klxLogger
     */
    public static KLXLogger getLogger() {
        if (klxLogger == null) {
            klxLogger = new KLXLogger();
        }
        return klxLogger;
    }

    /**Method to log functional flow information, warnings
     *
     * @param level      Severity Level
     * @param obj        class Object
     * @param method     method name
     * @param message    message of exception
     */
    public void log(final Level level, final Class obj, final String method,
                    final String message) {
        aDFLogger.logp(level, obj.getName(), method,
                       obj.getName() + " - " + method + " - " + message);
    }
    
    public void log(final Level level, final String className, final String method,
                    final String message) {
        aDFLogger.logp(level, className, method,
                       className + " - " + method + " - " + message);
    }
    /**Method to log functional flow information, warnings and Exceptions
     *
     * @param obj        class Object
     * @param method     method name
     * @param e          message of exception
     */
    public void log(final Class obj, final String method, final Exception e) {

        aDFLogger.logp(Level.SEVERE, obj.getName(), method, null, e);
    }

    /**Method to log Exceptions
     *
     * @param level      Severity Level
     * @param obj        class Object
     * @param method     method name
     * @param e    message of exception
     */
    public void log(final Level level, final Class obj, final String method,
                    final Exception e) {

        aDFLogger.logp(level, obj.getName(), method, null, e);
    }
    
    public void log(final Level level, String className, final String method,
                    final Exception e) {

        aDFLogger.logp(level, className, method, null, e);
    }
}
