package com.klx.common.logger;


import java.util.Date;
import java.util.logging.Level;

import oracle.adf.share.logging.ADFLogger;


/**
 This is  KLXDMSLogger class which provides logging support for DMS functionality in application,
 to be usedas a static reference for logging functional points, exceptions
 and warnings.
 layer.
  **/
public final class KLXDMSLogger {


    private static ADFLogger aDFLogger;
    private static KLXDMSLogger klxDmsLogger;

    /**private contructor to avoid instantiation and implement singleton
     *
     */
    private KLXDMSLogger() {
        aDFLogger = ADFLogger.createADFLogger("KLXDMSLogger.class");
        aDFLogger.setLevel(Level.ALL);

    }

    /**Method to return KLXDMSLogger instance
     *
     * @return klxDmsLogger
     */
    public static KLXDMSLogger getLogger() {
        if (klxDmsLogger == null) {
            klxDmsLogger = new KLXDMSLogger();
        }
        return klxDmsLogger;
    }

    
    /**Method to log functional flow information, warnings and Exceptions
     *
     * @param obj        class Object
     * @param method     method name
     * @param e          message of exception
     */
    public void log(final Class obj, final String method,
                      final Exception e) {

        aDFLogger.logp(Level.SEVERE, obj.getName(), method, null, e);
    }

    /**Method to log Exceptions
     *
     * @param level      Severity Level
     * @param obj        class Object
     * @param method     method name
     * @param e    message of exception
     */
    public void log(final Level level, final Class obj, final String method,
                      final Exception e) {

        aDFLogger.logp(level, obj.getName(), method, null, e);
    }
    
    public void log(final Level level, final String className, final String method,
                      final Exception e) {

        aDFLogger.logp(level, className, method, null, e);
    }
    
    public void log(final Level level, final Class obj, final String method,
                              final String message) {
      aDFLogger.logp(level, obj.getName(), method,
                                     obj.getName() + " - " + method + " - " + message);
    }
    public void log(final Level level, final String className, final String method,
                    final String message) {
        aDFLogger.logp(level, className, method,
                       className + " - " + method + " - " + message);
    }


}
