package com.klx.common.exception;

import oracle.jbo.JboException;

public class SystemException extends JboException{
    private static final long serialVersionUID = 0L;
    
    String applicationName;
    String functionality;
    String errorCode;
    String displayMessage;
    String errorMessage;
    String errorDetails;
    String uniqueIdentifier;
    Exception e;
    
    public SystemException(Exception e) {
        super(e);
    }
    
    public SystemException(String errorCode) {
        super(errorCode);
        this.errorCode = errorCode;        
    }
     
    public SystemException(String errorCode, Exception e) {
        super(errorCode);
        this.errorCode = errorCode;
        
    }
    
    public SystemException(String application, String functionality, String errorCode, Exception e) {
        super(errorCode);
        this.errorCode = errorCode;
        
    }
    
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }
    
    public void setFunctionality(String functionality) {
        this.functionality = functionality;
    }

    public String getFunctionality() {
        return functionality;
    }

    public void setE(Exception e) {
        this.e = e;
    }

    public Exception getE() {
        return e;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorDetails(String errorDetails) {
        this.errorDetails = errorDetails;
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    public void setUniqueIdentifier(String uniqueIdentifier) {
        this.uniqueIdentifier = uniqueIdentifier;
    }

    public String getUniqueIdentifier() {
        return uniqueIdentifier;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationName() {
        return applicationName;
    }
}
