package com.klx.common.cache;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.HashMap;
import javax.naming.InitialContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import com.klx.common.constants.QueryConstants;

import java.util.ArrayList;

import javax.sql.DataSource;

/** This is  ParamCache class. which contains  method implementations
 * for fetching master data and properties map from data base, and set it in
 * cache.The class will be executed once during the application startup.
 *
 */

public class ParamCache implements ServletContextListener,
                                             javax.servlet.Servlet {

    // contains external configuration information like web service end points
    public static HashMap propertiesMap = new HashMap();
    
    // contains file type list
    public static HashMap fileTypeMap = new HashMap();
    
    // contains group key and value mapping
    public static HashMap groupsMap = new HashMap();
    
    // contains user action key and role value mapping
    public static HashMap userActionRoleMap = new HashMap();
    
    // contains group key and value mapping
    public static HashMap userRoleMap = new HashMap();
    
    //DS_NAME literal for DATASOURCE NAME
    public static final String DS_NAME = "jdbc/XxKLXDataSource";
        
    public ParamCache() {
        super();
    }

    /**Method to read all external configuration information from DB and store
     * in hashmap
     *
     */
    private void populatePropertiesMap(ServletResponse servletResponse) {

        ResultSet resultset = null;
        Connection connection = null;
        Statement statement = null;
        
        try {
           
            connection = createDBConnection();
            statement = connection.createStatement();
            
            // fetch all external configuration properties from DB
            resultset = statement.executeQuery(QueryConstants.PROPERTIES_QUERY);
           
            while (resultset.next()) {

                if(servletResponse != null){
                    servletResponse.getOutputStream().write(("Param Key :- " + resultset.getString("KEY")+ "<br />").getBytes());
                    servletResponse.getOutputStream().write(("Param Value : " + resultset.getString("VALUE") + "<br />").getBytes());                    
                }
                //Put all external configuration properties from DB into map
                               
                propertiesMap.put(resultset.getString("KEY"),
                                  resultset.getString("VALUE"));
                                            

            }
            
            resultset.close();
            statement.close();
            
        } catch (Exception exception) {

            exception.printStackTrace();
        
        } finally {
            try {                
                    if (resultset != null) {
                        resultset.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
            } catch (SQLException sqlException) {
               
                sqlException.printStackTrace();
                
            }
        }

        
    }
    
    
    /**Method to read all groups code and their respective names
     * in hashmap
     *
     */
    private void populateGroupsMap(ServletResponse servletResponse) {

        ResultSet resultset = null;
        Connection connection = null;
        Statement statement = null;
        
        try {
            
            connection = createDBConnection();            
            statement = connection.createStatement();
            
           
            resultset = statement.executeQuery(QueryConstants.GROUP_QUERY);            
            while (resultset.next()) {
                            
                //Put all external configuration properties from DB into map
                groupsMap.put(resultset.getString("USER_GROUP"),
                                  resultset.getString("GROUP_NAME"));

            }
            
            resultset.close();
            statement.close();
            
            
        } catch (Exception exception) {
            
            exception.printStackTrace();
        
        } finally {
            try {                
                    if (resultset != null) {
                        resultset.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
            } catch (SQLException sqlException) {

                sqlException.printStackTrace();
                
            }
        }

        
    }
    
    /**Method to read all role code and their respective name
     * in hashmap
     *
     */
    private void populateUserRoleMap(ServletResponse servletResponse) {

        ResultSet resultset = null;
        Connection connection = null;
        Statement statement = null;
        
        try {
           
            connection = createDBConnection();            
            statement = connection.createStatement();
            
           
            resultset = statement.executeQuery(QueryConstants.USER_ROLE_QUERY);            
            while (resultset.next()) {
               
                //Put all external configuration properties from DB into map
                userRoleMap.put(resultset.getString("USER_GROUP_ID"),
                                  resultset.getString("USER_GROUP"));

            }
            
            resultset.close();
            statement.close();
       
            
        } catch (Exception exception) {
            
            exception.printStackTrace();
        
        } finally {
            try {                
                    if (resultset != null) {
                        resultset.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
            } catch (SQLException sqlException) {
                
                sqlException.printStackTrace();
                
            }
        }

        
    }
    
    
    
    private void populateFileTypeMap(ServletResponse servletResponse) {

        ResultSet resultset = null;
        Connection connection = null;
        Statement statement = null;
        
        try {
            
            connection = createDBConnection();            
            statement = connection.createStatement();
            
            // fetch all external configuration properties from DB
            resultset = statement.executeQuery(QueryConstants.FILE_TYPE_QUERY);            
            while (resultset.next()) {
                              
                //Put all external configuration properties from DB into map
                fileTypeMap.put(resultset.getString("FILE_CONTENT_TYPE"),
                                  resultset.getString("FILE_TYPE"));

            }
            
            resultset.close();
            statement.close();
        
            
        } catch (Exception exception) {
           
            exception.printStackTrace();
        
        } finally {
            try {                
                    if (resultset != null) {
                        resultset.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
            } catch (SQLException sqlException) {
             
                sqlException.printStackTrace();
                
            }
        }

        
    }
    
    private void populateUserActionRoleMap(ServletResponse servletResponse) {

        ResultSet resultset = null;
        Connection connection = null;
        Statement statement = null;
        String userAction = null;
        ArrayList userRole = null;
        
        try {
          
            connection = createDBConnection();
          
            statement = connection.createStatement();
            
            // fetch all external configuration properties from DB
            resultset = statement.executeQuery(QueryConstants.USER_ACTION_ROLE_QUERY);
           
            while (resultset.next()) {
               
                if(servletResponse != null){
                    servletResponse.getOutputStream().write(("User Action :- " + resultset.getString("USER_ACTION")+ "<br />").getBytes());
                    servletResponse.getOutputStream().write(("User Group : " + resultset.getString("USER_GROUP") + "<br />").getBytes());                    
                }
                //Put all external configuration properties from DB into map
                userAction = resultset.getString("USER_ACTION");
                if(null != userActionRoleMap.get(userAction)){
                    userRole = (ArrayList)userActionRoleMap.get(userAction);
                    userRole.add(resultset.getString("USER_GROUP"));                        
                }else{
                    userRole = new ArrayList();
                    userRole.add(resultset.getString("USER_GROUP"));   
                }
                    
                userActionRoleMap.put(userAction,userRole);
                                            

            }
            
            resultset.close();
            statement.close();
            
        } catch (Exception exception) {
           
            exception.printStackTrace();
        
        } finally {
            try {                
                    if (resultset != null) {
                        resultset.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
            } catch (SQLException sqlException) {
              
                sqlException.printStackTrace();
                
            }
        }

        
    }
    
    
    /**Method to create a DB connection using datasource configured on server.
         *
         *
         */
        public Connection createDBConnection() throws Exception {
        
            Connection connection = null;

            try {
              
                InitialContext context = new InitialContext();
                Object obj = context.lookup(DS_NAME);
             
                DataSource ds = (DataSource)obj;                
                connection = ds.getConnection();
               
            } catch (SQLException sqlException) {
              
                sqlException.printStackTrace();
            }
            return connection;
        }

    public void contextInitialized(final ServletContextEvent servletContextEvent) {
                    
            
            populatePropertiesMap(null);
            populateFileTypeMap(null);
            populateGroupsMap(null);
            populateUserRoleMap(null);
            populateUserActionRoleMap(null);
        
    }


    public void contextDestroyed(final ServletContextEvent servletContextEvent) {
    }

    public void init(final ServletConfig servletConfig) {
           
            

            populatePropertiesMap(null);
            populateFileTypeMap(null);
            populateGroupsMap(null);
            populateUserRoleMap(null);
            populateUserActionRoleMap(null);
			
    }

    
    public void service(final ServletRequest servletRequest,
                        final ServletResponse servletResponse) {
        
        try {
        
       
        servletResponse.getOutputStream().write("Starting of Param Cache Servlet <br />".getBytes());
        
        populatePropertiesMap(servletResponse);
        populateFileTypeMap(servletResponse);
        populateGroupsMap(servletResponse);
        populateUserRoleMap(servletResponse);
        populateUserActionRoleMap(servletResponse);
                        
        
        servletResponse.getOutputStream().write((Integer.toString(propertiesMap.size()) + " values succesfully updated in properties map<br />").getBytes());    
            
        servletResponse.getOutputStream().write("Completion of Param Cache Servlet <br />".getBytes());    
        
       
            
        } catch (Exception exception) {
            
            exception.printStackTrace();
        }    
        
    }
    
    public ServletConfig getServletConfig() {
            return null;
        } 
    
    public String getServletInfo() {
        return null;
    }

    public void destroy() {
    }
}