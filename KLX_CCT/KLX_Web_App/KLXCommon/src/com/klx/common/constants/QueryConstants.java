package com.klx.common.constants;

/**
 * This is  QueryConstants class. Which will contain query constants to be used 
 **/
public class QueryConstants {
    
    // Query to fetch properties list
    public static final String PROPERTIES_QUERY =
            "SELECT KEY, VALUE FROM CCT_PROPERTIES";
    
    
        // Query to fetch file types
    public static final String FILE_TYPE_QUERY =
            "SELECT FILE_CONTENT_TYPE, FILE_TYPE FROM XXDMS.DMS_FILE_TYPE";
    
    //Query to fetch groups
    public static final String GROUP_QUERY =
            "SELECT USER_GROUP,GROUP_NAME FROM CCT_ROLE_PROPERTIES";
    
    //Query to fetch groups
    public static final String USER_ROLE_QUERY =
            "SELECT USER_GROUP_ID,USER_GROUP FROM CCT_ROLE_PROPERTIES";
    
    public static final String USER_ACTION_ROLE_QUERY =
            "SELECT rua.USER_ACTION as USER_ACTION, rp.USER_GROUP as USER_GROUP FROM CCT_ROLE_PROPERTIES rp, CCT_ROLE_USER_ACTION rua WHERE rp.USER_GROUP_ID = rua.USER_GROUP_ID";
    
}
