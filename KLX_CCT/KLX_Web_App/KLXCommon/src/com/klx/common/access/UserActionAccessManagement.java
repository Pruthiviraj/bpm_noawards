package com.klx.common.access;

import com.klx.common.cache.ParamCache;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class UserActionAccessManagement {
    private static HashMap userActionRolesMap = ParamCache.userActionRoleMap;
    public UserActionAccessManagement() {
        
    }
       
    public static HashMap getUserAccess(String[] userRoles, HashMap userAction){
        Iterator actions = userAction.keySet().iterator();
        while(actions.hasNext()){
            String action = (String)actions.next();
            List roles = (ArrayList)userActionRolesMap.get(action);            
            if(roles != null){
                for(int i=0; i<roles.size(); i++){
                    if(null != userRoles && userRoles.length > 0 && Arrays.asList(userRoles).contains(roles.get(i))){
                        userAction.put(action, true);
                        break;
                    }
                }
            }
        }
        return userAction;
    }
}
