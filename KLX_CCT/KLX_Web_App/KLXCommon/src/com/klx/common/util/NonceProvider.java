package com.klx.common.util;

    import java.io.Serializable;
    
    import java.util.Random;
    
    public class NonceProvider implements Serializable {
    
    private static final long serailVersionID = 1L;
    private final Random random = new Random();
    
    private final int nonceLength;
    private final String alphabet;
    
    public NonceProvider(final int nonceLength, final String alphabet){
    this.nonceLength = nonceLength;
    this.alphabet = alphabet;
    }
    
    public String get(){
        final StringBuffer sb = new StringBuffer();
        for(int i=0; i < this.nonceLength; i++) {
        final int index;
        synchronized(this.random){
        index = this.random.nextInt(this.alphabet.length());
        }
        sb.append(this.alphabet.charAt(index));
        }
        return sb.toString();
    }
    
    }