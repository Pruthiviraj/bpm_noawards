package com.klx.common.util;

import com.klx.common.logger.KLXDMSLogger;
import com.klx.common.logger.KLXLogger;

import java.util.logging.Level;

public class ErrorUtil {
    
    private static final NonceProvider NONCE_PROVIDER = new NonceProvider(5,"ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
    private static KLXDMSLogger dmsLogger = KLXDMSLogger.getLogger();
    private static KLXLogger logger = KLXLogger.getLogger();
    private static  String DMS_APPLICATION_NAME = "DMS";
    
    public ErrorUtil() {
        super();
    } 
    
    public static void processError(String applicationName, String functionality, String uniqueIentifier, Exception e){
        logError(applicationName,uniqueIentifier, e);
        saveErrorToDatabase(applicationName,functionality,uniqueIentifier,e);
    }
    
    public static void logError(String applicationName, String uniqueIentifier, Exception e) {
        if(applicationName.equalsIgnoreCase(DMS_APPLICATION_NAME)){
            dmsLogger.log(Level.SEVERE, "ErrorUtil", "logError", "Error Code :: " + uniqueIentifier + " - Error Message :: " + e.getMessage());
            dmsLogger.log(Level.SEVERE, "ErrorUtil", "logError", e);
        }else {
            logger.log(Level.SEVERE, "ErrorUtil", "logError", "Error Code :: " + uniqueIentifier + " - Error Message :: " + e.getMessage());
            logger.log(Level.SEVERE, "ErrorUtil", "logError", e);
        }
        
                
    }
    
    public static void saveErrorToDatabase(String applicationName, String functionality, String uniqueIentifier, Exception e) {
	        
    }
	
    public static String generateUniqueIdentifier(){
            String uniqueIdentifier = NONCE_PROVIDER.get();
            return uniqueIdentifier;
    }

}