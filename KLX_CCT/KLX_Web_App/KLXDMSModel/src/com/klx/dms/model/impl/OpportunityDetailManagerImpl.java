package com.klx.dms.model.impl;


import com.klx.dms.model.pojo.Opportunity;
import com.klx.services.wrapper.OpportunityDetailServiceWrapper;

import java.math.BigDecimal;

public class OpportunityDetailManagerImpl {
    public OpportunityDetailManagerImpl() {
        super();
    }
    
    public Opportunity getOpportunityDetails(String oscOppId){
        Opportunity opportunity = new Opportunity();
        try {
        OpportunityDetailServiceWrapper wrapper=new OpportunityDetailServiceWrapper();
        com.klx.services.entities.Opportunity opptyEntity = wrapper.getOpportunityDetails(oscOppId);
        opportunity.setOscOpportunityId(opptyEntity.getOpportunityId());
        opportunity.setOpportunityNumber(opptyEntity.getOpportunityNumber());
        opportunity.setOpportunityName(opptyEntity.getOpportunityName());
        opportunity.setOpportunityId(null);
        }catch(Exception e){
            e.printStackTrace();
        }
        return opportunity;
    }    
   
}
