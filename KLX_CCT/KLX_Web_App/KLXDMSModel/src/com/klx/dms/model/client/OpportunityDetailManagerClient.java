package com.klx.dms.model.client;

import com.klx.common.logger.KLXDMSLogger;
import com.klx.common.logger.KLXLogger;
import com.klx.dms.model.impl.OpportunityDetailManagerImpl;


import com.klx.dms.model.pojo.Opportunity;

import java.math.BigDecimal;

import java.util.logging.Level;

public class OpportunityDetailManagerClient {
    public OpportunityDetailManagerClient() {
        super();
    }
    private static KLXDMSLogger logger = KLXDMSLogger.getLogger();   
    public Opportunity getOpportunityDetails(String oscOppId){
        logger.log(Level.INFO, getClass(), "getOpportunityDetails",  "Entering getOpportunityDetails method");
        logger.log(Level.INFO, getClass(), "getOpportunityDetails",  "Input Parameter OscOppId value is -"+oscOppId+"-");
        OpportunityDetailManagerImpl impl=new OpportunityDetailManagerImpl();
        logger.log(Level.INFO, getClass(), "getOpportunityDetails",  "Exiting getOpportunityDetails method");
        return(impl.getOpportunityDetails(oscOppId));
    }
    
   
}
