package com.klx.dms.model.appModule;

import com.klx.common.exception.SystemException;
import com.klx.common.logger.KLXDMSLogger;
import com.klx.common.logger.KLXLogger;
import com.klx.dms.model.appModule.common.KLXDMSAppModule;
import com.klx.dms.model.constants.ModelConstants;
import com.klx.dms.model.viewObjects.DocumentVOImpl;
import com.klx.dms.model.viewObjects.DocumentVORowImpl;
import com.klx.dms.model.viewObjects.FolderVOImpl;
import com.klx.dms.model.viewObjects.OpportunityVOImpl;
import com.klx.dms.model.viewObjects.OpportunityVORowImpl;
import com.klx.dms.model.pojo.Document;
import com.klx.dms.model.pojo.Opportunity;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.ResourceBundle;
import java.util.logging.Level;

import oracle.javatools.resourcebundle.BundleFactory;

import oracle.jbo.ApplicationModule;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewCriteriaItem;
import oracle.jbo.ViewCriteriaRow;
import oracle.jbo.common.JboCompOper;
import oracle.jbo.domain.Number;
import oracle.jbo.server.ApplicationModuleImpl;
import oracle.jbo.server.ViewObjectImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Aug 10 08:59:18 EDT 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class KLXDMSAppModuleImpl extends ApplicationModuleImpl implements  KLXDMSAppModule {
    /**
     * This is the default constructor (do not remove).
     */
      
    
    public KLXDMSAppModuleImpl() {
    }
    private static KLXDMSLogger logger = KLXDMSLogger.getLogger();  

    public HashMap moveDocuments(List  documentId ,String destinationFolderId) {
        HashMap hashMap=new HashMap();
        logger.log(Level.INFO, getClass(), "moveDocuments",  "Entering moveDocuments method");
        logger.log(Level.INFO, getClass(), "moveDocuments", "InputParameter documentId value is -"+documentId+"-");
        logger.log(Level.INFO, getClass(), "moveDocuments", "InputParameter destinationFolderId value is -"+destinationFolderId+"-");
        DocumentVOImpl vo = null;
        RowSetIterator createRowSetIterator = null;
        long effectiveCount = 0;
        String isValidPopUp="false";
        hashMap.put("effectiveCount", effectiveCount);
        hashMap.put("isValidPopUp", isValidPopUp);
        try {
            if (documentId == null || destinationFolderId == null) {
                return hashMap;
            }
            vo = this.getDocumentVO();
            //creating a view criteria
               ViewCriteria criteria = vo.createViewCriteria();
                //creating a view criteria row from criteria to set the condition(ViewCriteriaItem)
                ViewCriteriaRow viewRow = criteria.createViewCriteriaRow();
                ViewCriteriaItem viewObjectVCRowItem = viewRow.ensureCriteriaItem(ModelConstants.DOCUMENT_ID_ATTR);
                viewObjectVCRowItem.setOperator(JboCompOper.OPER_IN);
                for (int i = 0; i < documentId.size(); i++) {
                    viewObjectVCRowItem.setValue(i, documentId.get(i));
                }
                criteria.addElement(viewRow);
                vo.applyViewCriteria(criteria);
                vo.executeQuery();
            effectiveCount = vo.getEstimatedRowCount();
            logger.log(Level.INFO, getClass(), "moveDocuments","EstimatedRowCount -"+vo.getEstimatedRowCount()+"-");
            createRowSetIterator = vo.createRowSetIterator(null);
            while (createRowSetIterator.hasNext()) {
                DocumentVORowImpl row = (DocumentVORowImpl) createRowSetIterator.next();
                    if(null!=destinationFolderId)
                    row.setFolderId(new Number(destinationFolderId));
            }
            createRowSetIterator.closeRowSetIterator();
            logger.log(Level.INFO, getClass(), "moveDocuments","effectiveCount -"+effectiveCount+"-");
            if (effectiveCount > 0) {
                this.getDBTransaction().commit();
            }
            vo.applyViewCriteria(null);
            criteria.resetCriteria();

        } catch (Exception e) {
            isValidPopUp="true";
            effectiveCount=0;
            this.getDBTransaction().rollback();
           throw new SystemException(ModelConstants.DMS,ModelConstants.MOVE_DOCUMENTS,ModelConstants.DMS_SYS_EX01, e);
           
        }
        hashMap.put("isValidPopUp", isValidPopUp);
        hashMap.put("effectiveCount", effectiveCount);
        logger.log(Level.INFO, getClass(), "moveDocuments",  "Exiting moveDocuments method");
        return hashMap;
    }
    
   
    public long deleteDocuments(List  documentId) {
        logger.log(Level.INFO, getClass(), "deleteDocuments",  "Entering deleteDocuments method");
        
        DocumentVOImpl vo = null;
        long effectiveCount=0;
        try {
            RowSetIterator createRowSetIterator = null;
            logger.log(Level.INFO, getClass(), "getDocuments", "InputParameter documentId value is -"+documentId+"-");
            if (documentId == null) {
                return 0;
            }
            vo = this.getDocumentVO();

            //creating a view criteria
            ViewCriteria criteria = vo.createViewCriteria();
            //creating a view criteria row from criteria to set the condition(ViewCriteriaItem)
            ViewCriteriaRow viewRow = criteria.createViewCriteriaRow();


            ViewCriteriaItem viewObjectVCRowItem = viewRow.ensureCriteriaItem(ModelConstants.DOCUMENT_ID_ATTR);
            viewObjectVCRowItem.setOperator(JboCompOper.OPER_IN);


            for (int i = 0; i < documentId.size(); i++) {
                viewObjectVCRowItem.setValue(i, documentId.get(i));
            }

            criteria.addElement(viewRow);
            vo.applyViewCriteria(criteria);
            vo.executeQuery();
            effectiveCount = vo.getEstimatedRowCount();
            createRowSetIterator = vo.createRowSetIterator(null);
            while (createRowSetIterator.hasNext()) {
                DocumentVORowImpl row = (DocumentVORowImpl) createRowSetIterator.next();
                row.setIsDeleted(ModelConstants.YES);
            }
            createRowSetIterator.closeRowSetIterator();
            logger.log(Level.INFO, getClass(), "getDocuments", "effectiveCount -"+effectiveCount+"-");
            if (effectiveCount > 0) {
                this.getDBTransaction().commit();
            }
            vo.applyViewCriteria(null);
            criteria.resetCriteria();
        } catch (Exception e) {
            effectiveCount=0;
            this.getDBTransaction().rollback();
            throw new SystemException(ModelConstants.DMS,ModelConstants.DELETE_DOCUMENTS,ModelConstants.DMS_SYS_EX02, e);

        }
        logger.log(Level.INFO, getClass(), "deleteDocuments",  "Exiting deleteDocuments method");
        logger.log(Level.INFO, getClass(), "getDocuments", "Return Parameter effectiveCount -"+effectiveCount+"-");
        return effectiveCount;
    }


    public void getOpportunityDetails(String opportunityNumber) { //filterByOSCOpportunityId
        
    logger.log(Level.INFO, getClass(), "getOpportunityDetails",  "Entering getOpportunityDetails method");
        logger.log(Level.INFO, getClass(), "getOpportunityDetails", "InputParameter opportunityNumber value is -"+opportunityNumber+"-");
        try {
            OpportunityVOImpl opportunityVOImpl = null;
            opportunityVOImpl = getOpportunityVO();
            ViewCriteria opportunityVOImplVC =
                getOpportunityVO().getViewCriteria(ModelConstants.FILTER_BY_OPPORTUNITYNUMBER);
            opportunityVOImpl.applyViewCriteria(opportunityVOImplVC);
            opportunityVOImpl.setNamedWhereClauseParam(ModelConstants.OPPORTUNITY_NUMBER_BIND, opportunityNumber);
            opportunityVOImpl.executeQuery();
            opportunityVOImpl.first();
        } catch (Exception e) {
            throw new SystemException(ModelConstants.DMS,ModelConstants.OPP_DETAILS,ModelConstants.DMS_SYS_EX05, e);  
        }
        logger.log(Level.INFO, getClass(), "getOpportunityDetails",  "Exiting getOpportunityDetails method");
    }
                                          
    
    public List<Document> getDocuments(String opportunityId, String folderId) {
        logger.log(Level.INFO, getClass(), "getDocuments",  "Entering getDocuments method");
        logger.log(Level.INFO, getClass(), "getDocuments", "InputParameter OppId value is -"+opportunityId+"-");
        logger.log(Level.INFO, getClass(), "getDocuments", "InputParameter FolderId value is -"+folderId+"-");
        List<Document> documentList = null;
        DocumentVOImpl documentVOImpl = null;
        RowSetIterator createRowSetIterator = null;
        documentVOImpl = getDocumentVO();
        documentVOImpl.applyViewCriteria(null);
        try {
            ViewCriteria vc = documentVOImpl.getViewCriteria(ModelConstants.FILTER_BY_OPPORTUNITYID_FOLDERID);
            documentVOImpl.applyViewCriteria(vc);
            documentVOImpl.setNamedWhereClauseParam(ModelConstants.OPPORTUNITY_ID_BIND, opportunityId);
            documentVOImpl.setNamedWhereClauseParam(ModelConstants.FOLDER_ID_BIND, new Number(folderId));
            documentVOImpl.executeQuery();
        createRowSetIterator = documentVOImpl.createRowSetIterator(null);
        documentList = new ArrayList<Document>();
            while (createRowSetIterator.hasNext()) {
                DocumentVORowImpl documentVORowImpl = (DocumentVORowImpl) createRowSetIterator.next();
                Document document = new Document();
//                logger.log(Level.INFO, getClass(), "getDocuments",
//                           "CreatedBy -" + documentVORowImpl.getCreatedBy() + "-");
                document.setCreatedBy(documentVORowImpl.getCreatedBy());
                document.setCreatedDate(documentVORowImpl.getCreatedDate());
//                logger.log(Level.INFO, getClass(), "getDocuments",
//                           "FileTitle -" + documentVORowImpl.getFileTitle() + "-");
                document.setFileTitle(documentVORowImpl.getFileTitle());
                // doc.setFileContent(row.getFileContent());
                document.setFileContentType(documentVORowImpl.getFileContentType());
                documentList.add(document);
            }
            
        } catch (Exception e) {
            throw new SystemException(ModelConstants.DMS,ModelConstants.DOCUMENT_DETAILS,ModelConstants.DMS_SYS_EX03, e);
            
        } finally {
            createRowSetIterator.closeRowSetIterator();
        }
        logger.log(Level.INFO, getClass(), "getDocuments",  "Exiting getDocuments method");
        return documentList;
    }
    
    public List<Document> getDocuments(String opportunityId) {
        logger.log(Level.INFO, getClass(), "getDocuments",  "Entering getDocuments method");
        logger.log(Level.INFO, getClass(), "getDocuments", "InputParameter OppId value is -"+opportunityId+"-");
        List<Document> documentList = null;
        DocumentVOImpl documentVOImpl = null;
        RowSetIterator createRowSetIterator = null;
        documentVOImpl = getDocumentVO();
        try {
            ViewCriteria vc = documentVOImpl.getViewCriteria(ModelConstants.FILTER_BY_OPPORTUNITYID);
            documentVOImpl.applyViewCriteria(vc);
            documentVOImpl.setNamedWhereClauseParam(ModelConstants.OPPORTUNITY_ID_BIND, opportunityId);
            documentVOImpl.executeQuery();
            createRowSetIterator = documentVOImpl.createRowSetIterator(null);
            documentList = new ArrayList<Document>();
            while (createRowSetIterator.hasNext()) {
                DocumentVORowImpl row = (DocumentVORowImpl) createRowSetIterator.next();
                Document doc = new Document();
                logger.log(Level.INFO, getClass(), "getDocuments", "CreatedBy -" + row.getCreatedBy() + "-");
                doc.setCreatedBy(row.getCreatedBy());
                logger.log(Level.INFO, getClass(), "getDocuments", "CreatedDate -" + row.getCreatedDate() + "-");
                doc.setCreatedDate(row.getCreatedDate());
                doc.setFileTitle(row.getFileTitle());
                //doc.setFileContent(row.getFileContent());
                logger.log(Level.INFO, getClass(), "getDocuments",
                           "FileContentType -" + row.getFileContentType() + "-");
                doc.setFileContentType(row.getFileContentType());
                documentList.add(doc);
            }
        } catch (Exception e) {
            throw new SystemException(ModelConstants.DMS,ModelConstants.DOCUMENT_DETAILS,ModelConstants.DMS_SYS_EX03, e); 
        } finally {
            createRowSetIterator.closeRowSetIterator();
        }

        logger.log(Level.INFO, getClass(), "getDocuments",  "Exiting getDocuments method");
        return documentList;
    }
    
    public HashMap uploadDocument(Opportunity opportunity, Document document){
         logger.log(Level.INFO, getClass(), "uploadDocument",  "Entering uploadDocument method");
        HashMap hashMap=new HashMap();    
        try {
        
        ViewObjectImpl opportunityVO = this.getOpportunityVO();
            logger.log(Level.INFO, getClass(), "uploadDocument", "OppNumber -"+opportunity.getOpportunityNumber()+"-");
        getOpportunityDetails(opportunity.getOpportunityNumber());
            //logger.log(Level.INFO, getClass(), "uploadDocument", "OppVo EstimatedRowCount -"+getOpportunityVO().getEstimatedRowCount()+"-"); 
        if(getOpportunityVO().getEstimatedRowCount() == 0){
            OpportunityVORowImpl row = (OpportunityVORowImpl) opportunityVO.createRow();             
         row.setOpportunityNumber(opportunity.getOpportunityNumber());
            logger.log(Level.INFO, getClass(), "uploadDocument - created a new record on DMS", "Opportunity Number -"+opportunity.getOpportunityNumber()+"-");                      
            row.setOscOpportunityId(opportunity.getOpportunityId());
            logger.log(Level.INFO, getClass(), "uploadDocument - created a new record on DMS", "OppName -"+opportunity.getOpportunityName()+"-"); 
            logger.log(Level.INFO, getClass(), "uploadDocument - created a new record on DMS", "OppID -"+row.getAttribute("OpportunityId")+"-");
            if(null != opportunity.getOpportunityName())
                row.setOpportunityName(opportunity.getOpportunityName());
            else
                row.setOpportunityName("Opportunity for "+opportunity.getOpportunityNumber());
            getDBTransaction().commit();             
            getOpportunityDetails(opportunity.getOpportunityNumber());
        }
            
        ViewObjectImpl documentVO = this.getDocumentVO();
        DocumentVORowImpl row = (DocumentVORowImpl)documentVO.createRow();
            logger.log(Level.INFO, getClass(), "uploadDocument", "FolderId -"+document.getFolderId()+"-");    
        row.setFolderId(document.getFolderId());
            logger.log(Level.INFO, getClass(), "uploadDocument", "OpportunityId -"+getOpportunityVO().getCurrentRow().getAttribute("OpportunityId")+"-");    
            row.setOpportunityId((Number) getOpportunityVO().getCurrentRow().getAttribute("OpportunityId"));
            logger.log(Level.INFO, getClass(), "uploadDocument", "FileTitle -"+document.getFileTitle()+"-");    
        row.setFileTitle(document.getFileTitle());
        row.setFileName(document.getFileName());
        row.setAttribute("FileContent",document.getFileContentByteArray());
            //logger.log(Level.INFO, getClass(), "uploadDocument", "FileContentType -"+document.getFileContentType()+"-");        
        row.setFileContentType(document.getFileContentType());
            //logger.log(Level.INFO, getClass(), "uploadDocument", "SourceSystem -"+document.getSourceSystem()+"-");        
        row.setSourceSystem(document.getSourceSystem());
            logger.log(Level.INFO, getClass(), "uploadDocument", "FileType -"+document.getFileType()+"-");            
        row.setFileType(document.getFileType());
            logger.log(Level.INFO, getClass(), "uploadDocument", "CreatedBy -"+document.getCreatedBy()+"-");              
        row.setCreatedBy(document.getCreatedBy());
        getDBTransaction().commit(); 
            //logger.log(Level.INFO, getClass(), "uploadDocument", "opportunityId -"+row.getAttribute("OpportunityId")+"-");               
        hashMap.put("opportunityId", row.getAttribute("OpportunityId"));    
        hashMap.put("documentUploaded", "success");
        }catch (Exception e){
            e.printStackTrace();
            throw new SystemException(ModelConstants.DMS,ModelConstants.UPLOAD_DOCUMENT,ModelConstants.DMS_SYS_EX03, e);
            
        }
         logger.log(Level.INFO, getClass(), "uploadDocument",  "Exiting uploadDocument method");
        return hashMap;
                            
     }


    /**
     * Container's getter for FolderVO1.
     * @return FolderVO1
     */
    public FolderVOImpl getFolderVO() {
        return (FolderVOImpl) findViewObject("FolderVO");
    }


    /**
     * Container's getter for OpportunityVO1.
     * @return OpportunityVO1
     */
    public OpportunityVOImpl getOpportunityVO() {
        return (OpportunityVOImpl) findViewObject("OpportunityVO");
    }


    /**
     * Container's getter for DocumentVO3.
     * @return DocumentVO3
     */
    public DocumentVOImpl getDocumentVO() {
        return (DocumentVOImpl) findViewObject("DocumentVO");
    }
    public void filterCreatedDate(HashMap hasMap){
        try {
            DocumentVOImpl documentVO = getDocumentVO();
            documentVO.applyViewCriteria(null);
            documentVO.setWhereClause(null);
            documentVO.executeQuery();
            if (null != hasMap.get("date"))
                documentVO.setWhereClause("TO_DATE(CREATED_DATE,'dd-MON-yy') = TO_DATE('" +
                                          hasMap.get("date").toString() + "','MM/dd/yyyy') AND");
            else
                documentVO.setWhereClause(null);
            documentVO.addWhereClause("(opportunity_id =" + hasMap.get("oppID") + ") AND (folder_id =" +
                                      hasMap.get("folderID") + ") AND (upper(is_deleted) = upper('" +
                                      hasMap.get("flag") + "'))");
           

            documentVO.executeQuery();
        } catch (Exception e) {
            throw new SystemException(ModelConstants.DMS,ModelConstants.DATE_FILTER,ModelConstants.DMS_SYS_EX05, e);  
        }
    }
}
