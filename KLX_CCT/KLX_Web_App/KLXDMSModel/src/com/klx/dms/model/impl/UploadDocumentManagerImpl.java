package com.klx.dms.model.impl;

import com.klx.dms.model.appModule.KLXDMSAppModuleImpl;
import com.klx.dms.model.constants.ModelConstants;
import com.klx.dms.model.pojo.Document;

import com.klx.dms.model.pojo.Opportunity;

import java.io.InputStream;

import java.util.HashMap;

import oracle.adf.share.logging.ADFLogger;

import oracle.jbo.client.Configuration;

public class UploadDocumentManagerImpl {
    
    private static ADFLogger _logger = ADFLogger.createADFLogger(UploadDocumentManagerImpl.class); 
    private static final String CLAZZ_NAME="UploadDocumentManagerImpl";
    
    public UploadDocumentManagerImpl() {
        super();
    }
    
    public HashMap uploadDocument(Opportunity opportunity,Document document){
            _logger.entering(CLAZZ_NAME, "uploadDocument");
            HashMap hashMap=new HashMap();
            KLXDMSAppModuleImpl am=null;
            try{
            am=(KLXDMSAppModuleImpl)Configuration.createRootApplicationModule(ModelConstants.APP_MODULE_IMPL, ModelConstants.APP_MODULE_LOCAL);
            hashMap=am.uploadDocument(opportunity,document);
            }
            catch(Exception e){
                _logger.severe(e.getMessage());
                e.printStackTrace();
            }
            finally{
            Configuration.releaseRootApplicationModule(am, false);
            }
            _logger.exiting(CLAZZ_NAME, "uploadDocument");
            return hashMap;
        }
}
