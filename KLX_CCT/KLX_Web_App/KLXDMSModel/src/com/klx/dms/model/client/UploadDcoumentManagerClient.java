package com.klx.dms.model.client;

import com.klx.common.logger.KLXDMSLogger;
import com.klx.common.logger.KLXLogger;
import com.klx.dms.model.impl.UploadDocumentManagerImpl;
import com.klx.dms.model.pojo.Document;

import com.klx.dms.model.pojo.Opportunity;

import java.io.InputStream;

import java.util.HashMap;
import java.util.logging.Level;

public class UploadDcoumentManagerClient {
    
    public UploadDcoumentManagerClient() {
        super();
    }
    private static KLXDMSLogger logger = KLXDMSLogger.getLogger();   
    public HashMap uploadDocument(Opportunity opportunity, Document document) {
        logger.log(Level.INFO, getClass(), "uploadDocument",  "Entering uploadDocument method");
        UploadDocumentManagerImpl uploadDocumentManagerImpl=new UploadDocumentManagerImpl();
        logger.log(Level.INFO, getClass(), "uploadDocument",  "Exiting uploadDocument method");
        return uploadDocumentManagerImpl.uploadDocument(opportunity,document);      
    }
}
