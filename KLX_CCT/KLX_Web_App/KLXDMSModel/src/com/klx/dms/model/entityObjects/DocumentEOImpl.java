package com.klx.dms.model.entityObjects;

import java.math.BigDecimal;

import java.sql.Timestamp;

import java.util.Calendar;

import oracle.jbo.AttributeList;
import oracle.jbo.Key;
import oracle.jbo.domain.BlobDomain;
import oracle.jbo.domain.DBSequence;
import oracle.jbo.domain.Number;
import oracle.jbo.server.EntityDefImpl;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.SequenceImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Aug 16 02:00:07 EDT 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class DocumentEOImpl extends EntityImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    protected enum AttributesEnum {
        DocumentId,
        OpportunityId,
        FolderId,
        FileTitle,
        FileName,
        FileDescription,
        FileContent,
        SourceSystem,
        IsDeleted,
        FlexAttr1,
        FlexAttr2,
        FlexAttr3,
        FlexAttr4,
        FlexAttr5,
        CreatedBy,
        CreatedDate,
        UpdatedBy,
        UpdatedDate,
        FileType,
        FileContentType;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        protected int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        protected static final int firstIndex() {
            return firstIndex;
        }

        protected static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        protected static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int DOCUMENTID = AttributesEnum.DocumentId.index();
    public static final int OPPORTUNITYID = AttributesEnum.OpportunityId.index();
    public static final int FOLDERID = AttributesEnum.FolderId.index();
    public static final int FILETITLE = AttributesEnum.FileTitle.index();
    public static final int FILENAME = AttributesEnum.FileName.index();
    public static final int FILEDESCRIPTION = AttributesEnum.FileDescription.index();
    public static final int FILECONTENT = AttributesEnum.FileContent.index();
    public static final int SOURCESYSTEM = AttributesEnum.SourceSystem.index();
    public static final int ISDELETED = AttributesEnum.IsDeleted.index();
    public static final int FLEXATTR1 = AttributesEnum.FlexAttr1.index();
    public static final int FLEXATTR2 = AttributesEnum.FlexAttr2.index();
    public static final int FLEXATTR3 = AttributesEnum.FlexAttr3.index();
    public static final int FLEXATTR4 = AttributesEnum.FlexAttr4.index();
    public static final int FLEXATTR5 = AttributesEnum.FlexAttr5.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATEDDATE = AttributesEnum.CreatedDate.index();
    public static final int UPDATEDBY = AttributesEnum.UpdatedBy.index();
    public static final int UPDATEDDATE = AttributesEnum.UpdatedDate.index();
    public static final int FILETYPE = AttributesEnum.FileType.index();
    public static final int FILECONTENTTYPE = AttributesEnum.FileContentType.index();

    /**
     * This is the default constructor (do not remove).
     */
    public DocumentEOImpl() {
    }

    /**
     * @return the definition object for this instance class.
     */
    public static synchronized EntityDefImpl getDefinitionObject() {
        return EntityDefImpl.findDefObject("com.klx.dms.model.entityObjects.DocumentEO");
    }


    /**
     * Gets the attribute value for DocumentId, using the alias name DocumentId.
     * @return the value of DocumentId
     */
    public Number getDocumentId() {
        return (Number) getAttributeInternal(DOCUMENTID);
    }

    /**
     * Sets <code>value</code> as the attribute value for DocumentId.
     * @param value value to set the DocumentId
     */
    public void setDocumentId(Number value) {
        setAttributeInternal(DOCUMENTID, value);
    }

    /**
     * Gets the attribute value for OpportunityId, using the alias name OpportunityId.
     * @return the value of OpportunityId
     */
    public Number getOpportunityId() {
        return (Number) getAttributeInternal(OPPORTUNITYID);
    }

    /**
     * Sets <code>value</code> as the attribute value for OpportunityId.
     * @param value value to set the OpportunityId
     */
    public void setOpportunityId(Number value) {
        setAttributeInternal(OPPORTUNITYID, value);
    }

    /**
     * Gets the attribute value for FolderId, using the alias name FolderId.
     * @return the value of FolderId
     */
    public Number getFolderId() {
        return (Number) getAttributeInternal(FOLDERID);
    }

    /**
     * Sets <code>value</code> as the attribute value for FolderId.
     * @param value value to set the FolderId
     */
    public void setFolderId(Number value) {
        setAttributeInternal(FOLDERID, value);
    }

    /**
     * Gets the attribute value for FileTitle, using the alias name FileTitle.
     * @return the value of FileTitle
     */
    public String getFileTitle() {
        return (String) getAttributeInternal(FILETITLE);
    }

    /**
     * Sets <code>value</code> as the attribute value for FileTitle.
     * @param value value to set the FileTitle
     */
    public void setFileTitle(String value) {
        setAttributeInternal(FILETITLE, value);
    }

    /**
     * Gets the attribute value for FileName, using the alias name FileName.
     * @return the value of FileName
     */
    public String getFileName() {
        return (String) getAttributeInternal(FILENAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for FileName.
     * @param value value to set the FileName
     */
    public void setFileName(String value) {
        setAttributeInternal(FILENAME, value);
    }

    /**
     * Gets the attribute value for FileDescription, using the alias name FileDescription.
     * @return the value of FileDescription
     */
    public String getFileDescription() {
        return (String) getAttributeInternal(FILEDESCRIPTION);
    }

    /**
     * Sets <code>value</code> as the attribute value for FileDescription.
     * @param value value to set the FileDescription
     */
    public void setFileDescription(String value) {
        setAttributeInternal(FILEDESCRIPTION, value);
    }

    /**
     * Gets the attribute value for FileContent, using the alias name FileContent.
     * @return the value of FileContent
     */
    public BlobDomain getFileContent() {
        return (BlobDomain) getAttributeInternal(FILECONTENT);
    }

    /**
     * Sets <code>value</code> as the attribute value for FileContent.
     * @param value value to set the FileContent
     */
    public void setFileContent(BlobDomain value) {
        setAttributeInternal(FILECONTENT, value);
    }


    /**
     * Gets the attribute value for SourceSystem, using the alias name SourceSystem.
     * @return the value of SourceSystem
     */
    public String getSourceSystem() {
        return (String) getAttributeInternal(SOURCESYSTEM);
    }

    /**
     * Sets <code>value</code> as the attribute value for SourceSystem.
     * @param value value to set the SourceSystem
     */
    public void setSourceSystem(String value) {
        setAttributeInternal(SOURCESYSTEM, value);
    }

    /**
     * Gets the attribute value for IsDeleted, using the alias name IsDeleted.
     * @return the value of IsDeleted
     */
    public String getIsDeleted() {
        return (String) getAttributeInternal(ISDELETED);
    }

    /**
     * Sets <code>value</code> as the attribute value for IsDeleted.
     * @param value value to set the IsDeleted
     */
    public void setIsDeleted(String value) {
        setAttributeInternal(ISDELETED, value);
    }

    /**
     * Gets the attribute value for FlexAttr1, using the alias name FlexAttr1.
     * @return the value of FlexAttr1
     */
    public String getFlexAttr1() {
        return (String) getAttributeInternal(FLEXATTR1);
    }

    /**
     * Sets <code>value</code> as the attribute value for FlexAttr1.
     * @param value value to set the FlexAttr1
     */
    public void setFlexAttr1(String value) {
        setAttributeInternal(FLEXATTR1, value);
    }

    /**
     * Gets the attribute value for FlexAttr2, using the alias name FlexAttr2.
     * @return the value of FlexAttr2
     */
    public String getFlexAttr2() {
        return (String) getAttributeInternal(FLEXATTR2);
    }

    /**
     * Sets <code>value</code> as the attribute value for FlexAttr2.
     * @param value value to set the FlexAttr2
     */
    public void setFlexAttr2(String value) {
        setAttributeInternal(FLEXATTR2, value);
    }

    /**
     * Gets the attribute value for FlexAttr3, using the alias name FlexAttr3.
     * @return the value of FlexAttr3
     */
    public String getFlexAttr3() {
        return (String) getAttributeInternal(FLEXATTR3);
    }

    /**
     * Sets <code>value</code> as the attribute value for FlexAttr3.
     * @param value value to set the FlexAttr3
     */
    public void setFlexAttr3(String value) {
        setAttributeInternal(FLEXATTR3, value);
    }

    /**
     * Gets the attribute value for FlexAttr4, using the alias name FlexAttr4.
     * @return the value of FlexAttr4
     */
    public String getFlexAttr4() {
        return (String) getAttributeInternal(FLEXATTR4);
    }

    /**
     * Sets <code>value</code> as the attribute value for FlexAttr4.
     * @param value value to set the FlexAttr4
     */
    public void setFlexAttr4(String value) {
        setAttributeInternal(FLEXATTR4, value);
    }

    /**
     * Gets the attribute value for FlexAttr5, using the alias name FlexAttr5.
     * @return the value of FlexAttr5
     */
    public String getFlexAttr5() {
        return (String) getAttributeInternal(FLEXATTR5);
    }

    /**
     * Sets <code>value</code> as the attribute value for FlexAttr5.
     * @param value value to set the FlexAttr5
     */
    public void setFlexAttr5(String value) {
        setAttributeInternal(FLEXATTR5, value);
    }

    /**
     * Gets the attribute value for CreatedBy, using the alias name CreatedBy.
     * @return the value of CreatedBy
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }


    /**
     * Gets the attribute value for CreatedDate, using the alias name CreatedDate.
     * @return the value of CreatedDate
     */
    public Timestamp getCreatedDate() {
        return (Timestamp) getAttributeInternal(CREATEDDATE);
    }


    /**
     * Gets the attribute value for UpdatedBy, using the alias name UpdatedBy.
     * @return the value of UpdatedBy
     */
    public String getUpdatedBy() {
        return (String) getAttributeInternal(UPDATEDBY);
    }


    /**
     * Gets the attribute value for UpdatedDate, using the alias name UpdatedDate.
     * @return the value of UpdatedDate
     */
    public Timestamp getUpdatedDate() {
        return (Timestamp) getAttributeInternal(UPDATEDDATE);
    }


    /**
     * Gets the attribute value for FileType, using the alias name FileType.
     * @return the value of FileType
     */
    public String getFileType() {
        return (String) getAttributeInternal(FILETYPE);
    }

    /**
     * Sets <code>value</code> as the attribute value for FileType.
     * @param value value to set the FileType
     */
    public void setFileType(String value) {
        setAttributeInternal(FILETYPE, value);
    }


    /**
     * Gets the attribute value for FileContentType, using the alias name FileContentType.
     * @return the value of FileContentType
     */
    public String getFileContentType() {
        return (String) getAttributeInternal(FILECONTENTTYPE);
    }

    /**
     * Sets <code>value</code> as the attribute value for FileContentType.
     * @param value value to set the FileContentType
     */
    public void setFileContentType(String value) {
        setAttributeInternal(FILECONTENTTYPE, value);
    }


    /**
     * @param documentId key constituent

     * @return a Key object based on given key constituents.
     */
    public static Key createPrimaryKey(Number documentId) {
        return new Key(new Object[] { documentId });
    }

    /**
     * Add attribute defaulting logic in this method.
     * @param attributeList list of attribute names/values to initialize the row
     */
    protected void create(AttributeList attributeList) {
        super.create(attributeList);
        SequenceImpl s = new SequenceImpl("DMS_DOCUMENT_SEQ", getDBTransaction());
        super.create(attributeList);
        Object obj = s.getSequenceNumber();
        this.setDocumentId((Number)obj);
    }
}

