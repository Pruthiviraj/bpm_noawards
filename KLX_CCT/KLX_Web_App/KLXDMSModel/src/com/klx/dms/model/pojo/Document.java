package com.klx.dms.model.pojo;

import java.io.Serializable;

import java.sql.Blob;

import java.sql.Timestamp;

import oracle.jbo.domain.BlobDomain;
import java.util.Date;
import oracle.jbo.domain.Number;


public class Document implements Serializable {
    
    private Number documentId;
    private Number opportunityId;
    private Number folderId;
    private String fileTitle;
    private String fileName;
    private String fileDescription;
    private BlobDomain fileContent;
    private byte[] fileContentByteArray;
    private String fileContentType;
    private String fileType;
    private String sourceSystem;
    private String isDeleted;
    private String flexAttr1;
    private String fle_Attr2;
    private String flexAttr3;
    private String flexAttr4;
    private String flexAttr5;
    private String createdBy;
    private Date createdDate;
    private String updatedBy;
    private Date updatedDate;
    
    public Document() {
        super();
    }


    public void setFileTitle(String fileTitle) {
        this.fileTitle = fileTitle;
    }

    public String getFileTitle() {
        return fileTitle;
    }

    
    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setFileContent(BlobDomain fileContent) {
        this.fileContent = fileContent;
    }

    public BlobDomain getFileContent() {
        return fileContent;
    }

    public void setDocumentId(Number documentId) {
        this.documentId = documentId;
    }

    public Number getDocumentId() {
        return documentId;
    }

    public void setOpportunityId(Number opportunityId) {
        this.opportunityId = opportunityId;
    }

    public Number getOpportunityId() {
        return opportunityId;
    }

    public void setFolderId(Number folderId) {
        this.folderId = folderId;
    }

    public Number getFolderId() {
        return folderId;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileDescription(String fileDescription) {
        this.fileDescription = fileDescription;
    }

    public String getFileDescription() {
        return fileDescription;
    }

    public void setFileContentByteArray(byte[] fileContentByteArray) {
        this.fileContentByteArray = fileContentByteArray;
    }

    public byte[] getFileContentByteArray() {
        return fileContentByteArray;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setFlexAttr1(String flexAttr1) {
        this.flexAttr1 = flexAttr1;
    }

    public String getFlexAttr1() {
        return flexAttr1;
    }

    public void setFle_Attr2(String fle_Attr2) {
        this.fle_Attr2 = fle_Attr2;
    }

    public String getFle_Attr2() {
        return fle_Attr2;
    }

    public void setFlexAttr3(String flexAttr3) {
        this.flexAttr3 = flexAttr3;
    }

    public String getFlexAttr3() {
        return flexAttr3;
    }

    public void setFlexAttr4(String flexAttr4) {
        this.flexAttr4 = flexAttr4;
    }

    public String getFlexAttr4() {
        return flexAttr4;
    }

    public void setFlexAttr5(String flexAttr5) {
        this.flexAttr5 = flexAttr5;
    }

    public String getFlexAttr5() {
        return flexAttr5;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileType() {
        return fileType;
    }
}


