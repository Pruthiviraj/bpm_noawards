package com.klx.dms.model.constants;

public class ModelConstants {
    
    public static final String YES = "Y";
    public static final String NO = "N";
    public static final String FILTER_BY_OPPORTUNITYNUMBER = "filterByOpportunityNumber";
    public static final String FILTER_BY_OPPORTUNITYID_FOLDERID = "filterByOpportunityIdFolderId";
    public static final String FILTER_BY_OPPORTUNITYID = "filterByOpportunityId";
    public static final String OPPORTUNITY_ID_BIND = "oppId";
    public static final String OPPORTUNITY_NUMBER_BIND = "opptyNum";
    public static final String FOLDER_ID_BIND = "fldrId";
    public static final String DOCUMENT_ID_ATTR = "DocumentId";    
    public static final String UPLOAD_DOCUMENT = "uploadDocument";
    public static final String APP_MODULE_IMPL = "com.klx.dms.model.appModule.KLXDMSAppModuleImpl";
    public static final String APP_MODULE_LOCAL = "KLXDMSAppModuleLocal";
    public static final String DMS = "DMS";
    public static final String DOWNLOAD_FILE ="DownloadFile";
    public static final String DOCUMENT_DETAILS ="DocumentDetails";
    public static final String DELETE_DOCUMENTS ="DeleteDocuments";
    public static final String MOVE_DOCUMENTS ="MoveDocuments";
    public static final String DATE_FILTER ="DateFilter";
    public static final String OPP_DETAILS ="OppDetails";
    public static final String TAB_LISTNER ="TabListner";
    public static final String OPPTY_ID_NULL_MSG = "The system cannot retrieve document details right now.Please login again.";
    

     public static final String DMS_SYS_EX01= "DMS_SYS_EX01";
     public static final String DMS_SYS_EX02= "DMS_SYS_EX02";
     public static final String CCT_SYS_EX02= "CCT_SYS_EX02";
     public static final String CCT_SYS_EX03= "CCT_SYS_EX03";
     public static final String DMS_SYS_EX03= "DMS_SYS_EX03";
     public static final String DMS_SYS_EX07= "DMS_SYS_EX07";
     public static final String CCT_SYS_EX04= "CCT_SYS_EX04";
     public static final String DMS_SYS_EX04= "DMS_SYS_EX04";
     public static final String DMS_SYS_EX06= "DMS_SYS_EX06";    
     public static final String DMS_GEN_EX01= "DMS_GEN_EX01";
     public static final String DMS_SYS_EX05= "DMS_SYS_EX05";
     public static final String CCT_SYS_EX05= "CCT_SYS_EX05";
     public static final String DMS_SYS_EX034= "DMS_SYS_EX034";
    public ModelConstants() {
        super();
    }
}
