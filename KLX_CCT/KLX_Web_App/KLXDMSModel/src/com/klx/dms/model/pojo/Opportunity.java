package com.klx.dms.model.pojo;

import java.util.Date;
import oracle.jbo.domain.Number;

public class Opportunity {
    
    private Number opportunityId;
    private String oscOpportunityId;
    private String opportunityNumber;
    private String opportunityName;
    private String createdBy;
    private Date createdDate;
    private String updatedBy;
    private Date updatedDate;
        
        
    public Opportunity() {
        super();
    }

    public void setOpportunityId(Number opportunityId) {
        this.opportunityId = opportunityId;
    }

    public Number getOpportunityId() {
        return opportunityId;
    }

    public void setOscOpportunityId(String oscOpportunityId) {
        this.oscOpportunityId = oscOpportunityId;
    }

    public String getOscOpportunityId() {
        return oscOpportunityId;
    }

    public void setOpportunityNumber(String opportunityNumber) {
        this.opportunityNumber = opportunityNumber;
    }

    public String getOpportunityNumber() {
        return opportunityNumber;
    }

    public void setOpportunityName(String opportunityName) {
        this.opportunityName = opportunityName;
    }

    public String getOpportunityName() {
        return opportunityName;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }
}
