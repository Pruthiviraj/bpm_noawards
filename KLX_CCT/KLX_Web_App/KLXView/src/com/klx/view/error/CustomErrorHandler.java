package com.klx.view.error;

import com.klx.common.exception.BusinessException;
import com.klx.common.exception.SystemException;
import com.klx.common.logger.KLXLogger;
import com.klx.common.util.ErrorUtil;
import com.klx.view.beans.constants.ViewConstants;
import com.klx.view.beans.utils.JSFUtils;

import java.util.logging.Level;

import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCErrorHandlerImpl;

import oracle.jbo.JboException;

public class CustomErrorHandler extends DCErrorHandlerImpl {
   
   
    private static final KLXLogger logger = KLXLogger.getLogger();
    private static final String bundlePath = "com.klx.view.properties.ErrorMessages";
       
        public CustomErrorHandler() {
           super(true);
        }
    
    
       public void reportException(DCBindingContainer dCBindingContainer,
                                   Exception exception) {
           String errorMessage = "";
           logger.log(Level.INFO, getClass(),
                                               "reportException",
                                               "Entering reportException method");
           if (exception instanceof SystemException) {               
               SystemException se = (SystemException)exception;               
               processError(se.getApplicationName(),se.getFunctionality(),se.getErrorCode(),se);
               
           } else if (exception instanceof BusinessException) {               
               BusinessException be = (BusinessException)exception;               
               processError(be.getApplicationName(),be.getFunctionality(),be.getErrorCode(),be);
               
           } else if (exception instanceof JboException) {
            JboException jboException = (JboException) exception;
            String code = jboException.getErrorCode();
            if(!"25005".equalsIgnoreCase(code)){
            errorMessage = JSFUtils.getPropertyValueFromBundle(bundlePath,ViewConstants.CCT_SYS_EX05);
            JSFUtils.addFacesErrorMessage(errorMessage);
            }
           } else {
               super.reportException(dCBindingContainer, exception);
           }
           logger.log(Level.SEVERE, "CustomErrorHandler", "reportException", exception);
           logger.log(Level.INFO, getClass(),
                                               "reportException",
                                               "Exiting reportException method");

       }
       
       public static void processError(String applicationName, String functionality, String errorCode, Exception e){
            String uniqueIentifier = applicationName +"-"+ ErrorUtil.generateUniqueIdentifier();            
            ErrorUtil.processError(applicationName,functionality,uniqueIentifier,e);
            String errorMessage = JSFUtils.getPropertyValueFromBundle(bundlePath,errorCode);
            String finalErrorMessage = uniqueIentifier + " :: " + errorMessage;
            JSFUtils.clearFacesMessages();
            JSFUtils.addFacesErrorMessage(finalErrorMessage);
       }
       
}
