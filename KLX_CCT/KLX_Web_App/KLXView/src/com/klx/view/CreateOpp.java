package com.klx.view;

import com.klx.common.access.UserActionAccessManagement;
import com.klx.common.logger.KLXLogger;
import com.klx.model.constants.ModelConstants;
import com.klx.view.beans.constants.ViewConstants;
import com.klx.view.beans.utils.ADFUtils;

import oracle.adf.view.rich.render.ClientEvent;

import org.apache.myfaces.trinidad.util.Service;
import com.klx.view.beans.utils.JSFUtils;
import java.util.Date;

import java.util.HashMap;
import java.util.logging.Level;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import javax.faces.component.UIComponent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.share.ADFContext;

import oracle.binding.BindingContainer;

import oracle.jbo.ViewObject;

public class CreateOpp {
    private RichPopup saveResultPopUp;
    private String saveStatus=ViewConstants.OPP_FAILURE_MSG;
    private java.util.Date currentSystemDate;
    private RichButton saveBind;
    private RichInputText custNoBind;
    private static KLXLogger logger = KLXLogger.getLogger();
    private boolean projectNumber = false;
    private boolean contractTeams = false;

    public CreateOpp() {
        userActionsAccess();
    }

    public void saveData(ActionEvent actionEvent) {
            logger.log(Level.INFO, getClass(), "saveData", "Entering saveData method");
            try {
                if(getSaveBind().isDisabled())
                 return;
                String status = (String) ADFUtils.findOperationBinding("saveCreatedOpportunity").execute();
                if(ModelConstants.STATUS_SUCCESS.equalsIgnoreCase(status)){
                String crfNumber = (String) JSFUtils.resolveExpression("#{bindings.CrfNo.inputValue}");
                setSaveStatus("Opportunity  "+crfNumber+" created successfully");
                getSaveBind().setDisabled(true);
                }
                else{
                    getSaveBind().setDisabled(false);
                    setSaveStatus(ViewConstants.OPP_FAILURE_MSG); 
                }
            } catch (Exception e) {
                getSaveBind().setDisabled(false);
                setSaveStatus(ViewConstants.OPP_FAILURE_MSG);
                logger.log(Level.SEVERE, getClass(), "saveData", e.getMessage());
                //e.printStackTrace();
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(getSaveBind());
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            getSaveResultPopUp().show(hints);
            logger.log(Level.INFO, getClass(), "saveData", "Exiting saveData method");
        }

    public void customerNumberVCL(ValueChangeEvent valueChangeEvent) { 
        logger.log(Level.INFO, getClass(), "customerNumberVCL", "Entering customerNumberVCL method");
        String customerNoNew = (String)valueChangeEvent.getNewValue();
        boolean result = false;
        OperationBinding customerValidateBinding = ADFUtils.findOperationBinding("customerNoValidate");
        customerValidateBinding.getParamsMap().put("custNo", customerNoNew);
        result = ((Boolean)customerValidateBinding.execute()).booleanValue();
        if(result){  
            OperationBinding binding = ADFUtils.findOperationBinding("executeCctCrfVO");
            binding.getParamsMap().put("customerNumber", customerNoNew);
            binding.execute();   
        }
        else{
            getCustNoBind().setValue(null);
            AdfFacesContext.getCurrentInstance().addPartialTarget(getCustNoBind());
            FacesMessage Message = new FacesMessage(ViewConstants.INVALID_CUSTNO_MSG);   
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null,Message);
            
            OperationBinding binding = ADFUtils.findOperationBinding("executeCctCrfVO");
            binding.getParamsMap().put("customerNumber", customerNoNew);
            binding.execute();
            logger.log(Level.INFO, getClass(), "customerNumberVCL", "Exiting customerNumberVCL method");
        }
       
    }

    public void setSaveResultPopUp(RichPopup saveResultPopUp) {
        this.saveResultPopUp = saveResultPopUp;
    }

    public RichPopup getSaveResultPopUp() {
        return saveResultPopUp;
    }

    public void setSaveStatus(String saveStatus) {
        this.saveStatus = saveStatus;
    }

    public String getSaveStatus() {
        return saveStatus;
    }
    
    public void setCurrentSystemDate(Date currentSystemDate) {
        this.currentSystemDate = currentSystemDate;
    }

    public Date getCurrentSystemDate() {
        Date currentDate = new Date(System.currentTimeMillis());        
        return currentDate;
    }

    public void setSaveBind(RichButton saveBind) {
        this.saveBind = saveBind;
    }

    public RichButton getSaveBind() {
        return saveBind;
    }

    public void setCustNoBind(RichInputText custNoBind) {
        this.custNoBind = custNoBind;
    }

    public RichInputText getCustNoBind() {
        return custNoBind;
    }


    public DCBindingContainer getDCBindingsContainer() {
        DCBindingContainer bindingsContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        return bindingsContainer;
    }
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();

    }
    public String rollbackVOChanges() {
        // Add event code here...
        logger.log(Level.INFO, getClass(), "rollbackVOChanges", "Entering rollbackVOChanges method");
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
        return "toTF";
    }
    
    public void userActionsAccess(){
        HashMap actions = new HashMap();
        actions.put(ViewConstants.CREATE_PROJECT_NUMBER, false);  
        actions.put(ViewConstants.CREATE_CONTRACT_TEAM_MEMBERS, false); 
        HashMap userAcess = UserActionAccessManagement.getUserAccess(ADFContext.getCurrent().getSecurityContext().getUserRoles(), actions);  
        projectNumber = ((Boolean)userAcess.get(ViewConstants.CREATE_PROJECT_NUMBER)).booleanValue();
        contractTeams = ((Boolean)userAcess.get(ViewConstants.CREATE_CONTRACT_TEAM_MEMBERS)).booleanValue(); 
    }

    public void setProjectNumber(boolean projectNumber) {
        this.projectNumber = projectNumber;
    }

    public boolean isProjectNumber() {
        return projectNumber;
    }

    public void setContractTeams(boolean contractTeams) {
        this.contractTeams = contractTeams;
    }

    public boolean isContractTeams() {
        return contractTeams;
    }
}
