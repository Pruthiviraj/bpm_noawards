package com.klx.view.beans.scope.pageFlow;

import com.klx.cct.ad.user.v1.User;
import com.klx.common.logger.KLXLogger;
import com.klx.services.entities.Quote;
import com.klx.view.beans.constants.ViewConstants;
import com.klx.view.beans.utils.ADFUtils;
import com.klx.view.beans.utils.JSFUtils;
import com.klx.view.error.CustomErrorHandler;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import java.io.Serializable;
import java.io.OutputStream;
import java.math.BigDecimal;

import java.util.HashMap;
import java.util.logging.Level;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputFile;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;


import oracle.jbo.ViewObject;

import oracle.jbo.uicli.binding.JUCtrlListBinding;

import org.apache.myfaces.trinidad.event.DisclosureEvent;
import org.apache.myfaces.trinidad.model.UploadedFile;
import com.klx.common.access.UserActionAccessManagement;

import oracle.adf.share.ADFContext;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

@SuppressWarnings("oracle.jdeveloper.java.serialversionuid-field-missing")
public class FinalQuoteParts implements Serializable{
    private RichInputFile uploadFile;
    private long startTime;
    private String revisionSelected = "RI";
    private Quote quote;
    private String uploadStatusMessage = ViewConstants.UPL_STATUS_SUCCESS;
    private RichPopup bind_UploadStatus;
    private static KLXLogger logger = KLXLogger.getLogger();
    private String opportunityId;
    private String opportunityNumber;
    private RichPopup bind_UploadFile;
    
private String fileName;
    private RichPopup createPopup;
    private HashMap<String, String> beaconMapping = new HashMap<String, String>();
    private HashMap<String, String> allQuoteVoAttrMapping = new HashMap<String, String>();
    private HashMap<String, String> dataMaping = new HashMap<String, String>();
    private HashMap<String, String> quoteVoAttrMapping = new HashMap<String, String>();
    private HashMap<String, String[]> beaconMapping1 = new HashMap<String, String[]>();
    private boolean createApproval = false;
    private boolean generateQuote = false;
    private boolean readyApproval = false;
    private boolean uploadFinalBeacon = false;
    private boolean downloadFinalBeacon = false;
    
    private boolean downloadFinal = false;
    private boolean top50FinalParts = false;
    private String validateMessage = "";

    public void setCreateApproval(boolean createApproval) {
        this.createApproval = createApproval;
    }

    public boolean isCreateApproval() {
        return createApproval;
    }

    public void setGenerateQuote(boolean generateQuote) {
        this.generateQuote = generateQuote;
    }

    public boolean isGenerateQuote() {
        return generateQuote;
    }

    public void setCreatePopup(RichPopup createPopup) {
        this.createPopup = createPopup;
    }

    public RichPopup getCreatePopup() {
        return createPopup;
    }

    public void closePopup(ActionEvent actionEvent) {
        getCreatePopup().hide();
    }
    
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMddyyyy");
        Date dateNew = new Date();
        String format = dateFormat.format(dateNew);
        String custname=(String)JSFUtils.resolveExpression("#{pageFlowScope.custName}");
        String oppNumber=(String)JSFUtils.resolveExpression("#{pageFlowScope.oppNumb}");
                    String finalStr2=format+"_"+custname+"_"+oppNumber+"_"+"Quote File"+".xlsx";
        return finalStr2;
    }
	
    public void setUploadStatusMessage(String uploadStatusMessage) {
        this.uploadStatusMessage = uploadStatusMessage;
    }

    public String getUploadStatusMessage() {
        return uploadStatusMessage;
    }

    public void setQuote(Quote quote) {
        this.quote = quote;
    }

    public Quote getQuote() {
        return quote;
    }

    public void setRevisionSelected(String revisionSelected) {
        this.revisionSelected = revisionSelected;
    }

    public String getRevisionSelected() {
        return revisionSelected;
    }

    public FinalQuoteParts() {
        userActionsAccess();
    }
    
    public void userActionsAccess(){
        HashMap actions = new HashMap();
        actions.put(ViewConstants.CREATE_APPROVAL_SNAPSHOT, false);
        actions.put(ViewConstants.GENERATE_QUOTE, false);       
        actions.put(ViewConstants.READY_APPROVAL, false);      
        actions.put(ViewConstants.DOWNLOAD_FINAL_BEACON, false);      
        actions.put(ViewConstants.UPLOAD_FINAL_BEACON, false);      
        actions.put(ViewConstants.DOWNLOAD_FINAL, false);
        actions.put(ViewConstants.TOP50_FINAL_PARTS_DETAILS, false);
        HashMap userAcess = UserActionAccessManagement.getUserAccess(ADFContext.getCurrent().getSecurityContext().getUserRoles(), actions);        
        createApproval = ((Boolean)userAcess.get(ViewConstants.CREATE_APPROVAL_SNAPSHOT)).booleanValue();       
        generateQuote = ((Boolean)userAcess.get(ViewConstants.GENERATE_QUOTE)).booleanValue();  
        readyApproval = ((Boolean)userAcess.get(ViewConstants.READY_APPROVAL)).booleanValue();
        uploadFinalBeacon = ((Boolean)userAcess.get(ViewConstants.UPLOAD_FINAL_BEACON)).booleanValue();
        downloadFinalBeacon = ((Boolean)userAcess.get(ViewConstants.DOWNLOAD_FINAL_BEACON)).booleanValue();
        downloadFinal = ((Boolean)userAcess.get(ViewConstants.DOWNLOAD_FINAL)).booleanValue();
        top50FinalParts = ((Boolean)userAcess.get(ViewConstants.TOP50_FINAL_PARTS_DETAILS)).booleanValue();
    }

    public void setDownloadFinal(boolean downloadFinal) {
        this.downloadFinal = downloadFinal;
    }

    public boolean isDownloadFinal() {
        return downloadFinal;
    }

    public void uploadFinalParts(ActionEvent actionEvent) {
        // Add event code here...
    }

    public void uploadFinalBeaconFile(ActionEvent actionEvent) {
        // Add event code here...
        
        UploadedFile newValue = (UploadedFile) getUploadFile().getValue();
        HashMap hashMap = new HashMap();
        if (null != newValue) {
            String filename = newValue.getFilename();
            if (filename.endsWith("xlsx")) {
                try {
                    startTime = System.currentTimeMillis();
                    InputStream inputStream = newValue.getInputStream();
                    BindingContext bindingContext = BindingContext.getCurrent();
                    BindingContainer bindingContainer = bindingContext.getCurrentBindingsEntry();
                    HashMap inputParamMap = new HashMap();
                    String uploadType = (String) JSFUtils.resolveExpression("#{pageFlowScope.uploadType}");
                    String queueType = (String) JSFUtils.resolveExpression("#{pageFlowScope.queueReport}");
                    String lineNumber = (String) JSFUtils.resolveExpression("#{pageFlowScope.lineSequence}");
                    String queueid = (String) JSFUtils.resolveExpression("#{pageFlowScope.queueid}");
                    String sheetname = (String) JSFUtils.resolveExpression("#{pageFlowScope.sheetName}");
                    inputParamMap.put("uploadType", uploadType);
                    inputParamMap.put("queueType", queueType);
                    inputParamMap.put("lineNumber", lineNumber);
                    inputParamMap.put("queueid", queueid);                
                    inputParamMap.put("sheetName",sheetname);
                    OperationBinding binding = bindingContainer.getOperationBinding("uploadBeaconFileData");
                    binding.getParamsMap().put("revisionChoice", getRevisionSelected());
                    binding.getParamsMap().put("inputStream", inputStream);
                    //binding.getParamsMap().put("quoteNumber", resolveExpression("#{pageFlowScope.finalQuoteParts.quote.quoteNumber}"));
                    //binding.getParamsMap().put("quoteRevision", resolveExpression("#{pageFlowScope.finalQuoteParts.quote.quoteRevision}"));
                    //binding.getParamsMap().put("quoteRevisionId", resolveExpression("#{pageFlowScope.finalQuoteParts.quote.quoteRevisionId}"));
                    binding.getParamsMap().put("quoteNumber", resolveExpression("#{bindings.QuoteNumber.inputValue}"));
                    binding.getParamsMap().put("quoteRevision", resolveExpression("#{bindings.QuoteRevision.inputValue}"));
                    binding.getParamsMap().put("quoteRevisionId", resolveExpression("#{bindings.RevisionId.inputValue}"));
                    binding.getParamsMap()
                        .put("opportunityNumber",
                             resolveExpression("#{pageFlowScope.finalQuoteParts.opportunityNumber}"));
                    binding.getParamsMap().put("inputParamMap", inputParamMap);
                    binding.getParamsMap().put("partsType", "finalparts");
                    hashMap = (HashMap) binding.execute();
                   
                    String status = (String) hashMap.get("uploadStatus");
                    String uploadFailureReason = (String) hashMap.get("uploadFailureReason");
                    if ("successful".equalsIgnoreCase(status)) {
                        setUploadStatusMessage(ViewConstants.UPL_STATUS_SUCCESS);
                        setValidateMessage("");
                    } else {
                        setUploadStatusMessage(ViewConstants.UPL_STATUS_FAILURE);
                        setValidateMessage(uploadFailureReason);
                    }
                    setExpressionValue("#{bindings.QuoteRevision.inputValue}", quote.getQuoteRevision());
                    getBind_UploadFile().hide();
                    refreshFinalPartsIterator();
                    RichPopup.PopupHints hints = new RichPopup.PopupHints();
                    getBind_UploadStatus().show(hints);                  
                    logger.log(Level.INFO, getClass(), "uploadFinalBeaconFile", "start time is -" + startTime);
                    logger.log(Level.INFO, getClass(), "uploadFinalBeaconFile", " End time " + System.currentTimeMillis());
                    logger.log(Level.INFO, getClass(), "uploadFinalBeaconFile",
                               "Estimated time of execution is in secs = " +
                               (System.currentTimeMillis() - startTime) / 1000);
                
                } catch (FileNotFoundException e) {
                    logger.log(Level.SEVERE, getClass(),"uploadFinalBeaconFile",e.getMessage()); 
                    CustomErrorHandler.processError("CCT","uploadFinalBeaconFile","CCT_SYS_EX30",e);
                } catch (IOException e) {
                    //e.printStackTrace();
                    logger.log(Level.SEVERE, getClass(),"uploadFinalBeaconFile",e.getMessage()); 
                    CustomErrorHandler.processError("CCT","uploadFinalBeaconFile","CCT_SYS_EX30",e);
                }
            } else {
                FacesMessage fm = new FacesMessage("File is invalid Format");
                fm.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                //context.addMessage(getFileBinding().getClientId(), fm);
            }
        }
    }

    public void cancelFileUpload(ActionEvent actionEvent) {
        // Add event code here...
        getBind_UploadFile().hide();
    }

    public void setUploadFile(RichInputFile uploadFile) {
        this.uploadFile = uploadFile;
    }

    public RichInputFile getUploadFile() {
        return uploadFile;
    }
    
    private Object resolveExpression(String expression) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Application app = facesContext.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        ValueExpression valueExp = elFactory.createValueExpression(elContext, expression, Object.class);
        return valueExp.getValue(elContext);
    }
    
    private void setExpressionValue(String expression, Object newValue) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Application app = facesContext.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        ValueExpression valueExp = elFactory.createValueExpression(elContext, expression, Object.class);
        Class bindClass = valueExp.getType(elContext);
        if (bindClass.isPrimitive() || bindClass.isInstance(newValue)) {
            valueExp.setValue(elContext, newValue);
        }
    }
    
    private void refreshFinalPartsIterator(){
        BigDecimal revisionId = new BigDecimal(resolveExpression("#{bindings.RevisionId.inputValue}").toString());  
        DCBindingContainer bindingContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding binding = bindingContainer.findIteratorBinding("CctQuotePartsFinalVO1Iterator");
        long count = binding.getViewObject().getEstimatedRowCount();
        ViewObject object = binding.getViewObject();
        object.reset();
        object.clearCache();
        object.setNamedWhereClauseParam("bind_QuoteRevisionId", revisionId);
        object.executeQuery();
        System.out.println("VO query----"+object.getQuery());
        System.out.println("Final Parts rows---"+object.getEstimatedRowCount());
        
    }

    public void setBind_UploadStatus(RichPopup bind_UploadStatus) {
        this.bind_UploadStatus = bind_UploadStatus;
    }

    public RichPopup getBind_UploadStatus() {
        return bind_UploadStatus;
    }

    public  void setOpportunityId(String opportunityId) {
        this.opportunityId = opportunityId;
    }

    public  String getOpportunityId() {
        return opportunityId;
    }

    public void setOpportunityNumber(String opportunityNumber) {
        this.opportunityNumber = opportunityNumber;
    }

    public String getOpportunityNumber() {
        return opportunityNumber;
    }

    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public void finalAreaComparisonListener(DisclosureEvent disclosureEvent) {
        if(disclosureEvent.isExpanded()){
        oracle.adf.model.OperationBinding operationBinding =
            (oracle.adf.model.OperationBinding) ADFUtils.findOperationBinding("fetchFinalAreaComparisonDetails");
        operationBinding.getParamsMap().put("opportunityNumber", getOpportunityNumber());
        operationBinding.execute();
        }
    }
    
    public void openUploadPopUp(ActionEvent actionEvent) {
        RichInputFile inputFile = getUploadFile();
        if (null != inputFile) {
            inputFile.setValue(null);
            inputFile.setSubmittedValue(null);
            AdfFacesContext.getCurrentInstance().addPartialTarget(inputFile);
        }
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getBind_UploadFile().show(hints);
    }

    public void setBind_UploadFile(RichPopup bind_UploadFile) {
        this.bind_UploadFile = bind_UploadFile;
    }

    public RichPopup getBind_UploadFile() {
        return bind_UploadFile;
    }
	
	public void OkUpload(ActionEvent actionEvent) {
        // Add event code here...
        getBind_UploadStatus().hide();
	}
	
	public XSSFWorkbook genericLogic(XSSFWorkbook workbook, String Iterator, int sheetrownum, int headerrownum) {
            try {
                logger.log(Level.INFO, getClass(),
                                                                    "genericLogic",
                                                                    "Entering genericLogic method");

                DataFormat numFormatWithoutDecimal = workbook.createDataFormat(); //format defined
                XSSFCellStyle cellStyleNumber = workbook.createCellStyle();
                cellStyleNumber.setDataFormat(numFormatWithoutDecimal.getFormat("#,###.0000"));
                cellStyleNumber.setBorderBottom(XSSFCellStyle.BORDER_THIN);
                cellStyleNumber.setBorderTop(XSSFCellStyle.BORDER_THIN);
                cellStyleNumber.setBorderLeft(XSSFCellStyle.BORDER_THIN);
                cellStyleNumber.setBorderRight(XSSFCellStyle.BORDER_THIN);



                XSSFCellStyle cellStyleForBorder = workbook.createCellStyle();
                cellStyleForBorder.setBorderBottom(XSSFCellStyle.BORDER_THIN);
                cellStyleForBorder.setBorderTop(XSSFCellStyle.BORDER_THIN);
                cellStyleForBorder.setBorderLeft(XSSFCellStyle.BORDER_THIN);
                cellStyleForBorder.setBorderRight(XSSFCellStyle.BORDER_THIN);
                XSSFSheet sheet = workbook.getSheetAt(0);
             
                XSSFRow fRow = sheet.getRow(0);
                XSSFCell cell = fRow.getCell(0);

                String custname=(String)JSFUtils.resolveExpression("#{pageFlowScope.custName}");
        //            String custDate=(String)JSFUtils.resolveExpression("#{pageFlowScope.custDate}");
                String quoteNumber=(String)JSFUtils.resolveExpression("#{bindings.QuoteNumber.inputValue}");
                logger.log(Level.INFO, getClass(),
                                                     "genericLogic",
                                                     "Entering genericLogic method"+cell.getStringCellValue());
              
                String finalStr="Exhibit A to Proposal"+"   "+ "Customer Name :"+ custname+"   "+ "Quote # "+quoteNumber;
                cell.setCellValue(finalStr);
                
                
                XSSFRow row = sheet.getRow(1);
                XSSFCell cell_2 = row.getCell(0);
                String finalStr2 = getFileName();
                cell_2.setCellValue(finalStr2);
               
                DCBindingContainer bindings = ADFUtils.getDCBindingContainer();
                DCIteratorBinding itorBinding = bindings.findIteratorBinding(Iterator);
                ViewObject object = itorBinding.getViewObject();
                RowSetIterator iterator = object.createRowSetIterator(null);
                int i = sheetrownum;
                org.apache.poi.ss.usermodel.Row headerRow = sheet.getRow(headerrownum);

                while (iterator.hasNext()) { //Row iteration
                    Row currentRow = iterator.next();
                    XSSFRow sheetRow = sheet.createRow(i);
                    i++;
                    int index = 0; //Column
                    for (index = 0; index < currentRow.getAttributeCount(); index++) {


                        XSSFCell currentCell = sheetRow.createCell(index);
                        currentCell.setCellStyle(cellStyleForBorder);
                        // Cell newCell = headerRow.getCell(currentCell.getColumnIndex());
                        Cell newCell = headerRow.getCell(index);
                        
                        if (null != newCell && newCell.getCellTypeEnum() != CellType.BLANK) {
                            String newHeader = newCell.getStringCellValue();
                            logger.log(Level.INFO, getClass(),
                                                                                "genericLogic",
                                                                                "Header value of new cell" + newHeader);

                            String voAttribute = allQuoteVoAttrMapping.get(beaconMapping.get(newHeader));
                            String dataFormat=dataMaping.get(newHeader);
                            

                            if (null != currentRow && voAttribute != null && currentRow.getAttribute(voAttribute) != null) {
                                

                                if ("COST".equalsIgnoreCase(dataFormat)) {
                                    currentCell.setCellType(CellType.NUMERIC);
                                    currentCell.setCellStyle(cellStyleNumber);
                                    currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(voAttribute)
                                                                            .toString()));
                                }else if("NUMBER".equalsIgnoreCase(dataFormat))
                                    currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(voAttribute)
                                                                            .toString()));
                                else
                                    currentCell.setCellValue(currentRow.getAttribute(voAttribute).toString());
                                
                            } else {
                                currentCell.setCellValue("");
                            }

                        }

                    }
                }

            } catch (Exception e) {
                // TODO: Add catch code
              e.printStackTrace();
                logger.log(Level.SEVERE, getClass(),
                                                           "genericLogic",
                                                          e.getMessage()); 
                
                CustomErrorHandler.processError("CCT","downloadQuoteRepFinal","CCT_SYS_EX05",e);   
            }
            logger.log(Level.INFO, getClass(),
                                                                "genericLogic",
                                                                "Exiting genericLogic method");

            return workbook;

        }
        
    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public void readyForApprove(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(),
                                            "readyForApprove",
                                            "Entering readyForApprove method");

        BindingContext bindingContext = BindingContext.getCurrent();
            BindingContainer bindingContainer = bindingContext.getCurrentBindingsEntry();
        oracle.binding.OperationBinding binding = bindingContainer.getOperationBinding("readyForApproval");
        logger.log(Level.INFO, getClass(),
                                            "readyForApprove",
                                            "Opportunity Number: "+JSFUtils.resolveExpression("#{bindings.OpportunityNumber.inputValue}"));
        binding.getParamsMap().put("opportunityNumber",(String)JSFUtils.resolveExpression("#{bindings.OpportunityNumber.inputValue}"));
        binding.execute();
        oracle.adf.model.OperationBinding operationBinding =
            (oracle.adf.model.OperationBinding) ADFUtils.findOperationBinding("fetchFinalAreaComparisonDetails");
        operationBinding.getParamsMap().put("opportunityNumber", getOpportunityNumber());
        operationBinding.execute();
        RichPopup.PopupHints hints=new RichPopup.PopupHints();
        getCreatePopup().show(hints);
    }

    public void downloadQuoteRepFinal(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        try {
            logger.log(Level.INFO, getClass(),
                                                "downloadQuoteRepFinal",
                                                "Entering downloadQuoteRepFinal method");

            String dealType = (String) JSFUtils.resolveExpression("#{bindings.DealType.inputValue}");
            String mappingKey=null;
            String fileName=null;
            if("RENEW_CONTRACT".equalsIgnoreCase(dealType)){
                    mappingKey="RENEW_QUOTE_REP";
                     fileName = "RenewalQuoteTemplate.xlsx";
                }
            else{
                    mappingKey="NEW_QUOTE_REP";
                     fileName = "NewContractQuoteTemplate.xlsx";
                }
            
            //model
           // ADFUtils.getBindingContainer().getOperationBinding("getAllQuotePartsVOAttr").execute();
            ADFUtils.getBindingContainer().getOperationBinding("getAllQuotePartsSnapshotFinalVOAttr").execute();
            oracle.adf.model.OperationBinding generateTemplate =(oracle.adf.model.OperationBinding) ADFUtils.getBindingContainer().getOperationBinding("generateTemplate");
           
            generateTemplate.getParamsMap().put("pMappingKey", mappingKey);
            HashMap tempHashMap = (HashMap) generateTemplate.execute();
         
            beaconMapping= (HashMap) tempHashMap.get("templateMapping");
            allQuoteVoAttrMapping = (HashMap) tempHashMap.get("tasksVoAttrMapping");
            dataMaping=(HashMap) tempHashMap.get("dataMapping");
           //to get templete file
            
           String filePath= ViewConstants.FILE_PATH + fileName;
            FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);
            XSSFWorkbook workbook = new XSSFWorkbook(file);
           
           //view layer
            XSSFWorkbook filledWorkbook = genericLogic(workbook, "AllQuotePartsSnapshotFinalVO1Iterator", 3, 2);
          

        try {
                logger.log(Level.SEVERE, getClass(),
                                                           "downloadQuoteRepFinal", "Writing workbook");
                filledWorkbook.write(outputStream);
           
                outputStream.flush();
                outputStream.close();
            } 
            catch (Exception e) {
                logger.log(Level.SEVERE, getClass(),
                                                           "downloadQuoteRepFinal",
                                                          e.getMessage()); 
                CustomErrorHandler.processError("CCT","downloadQuoteRepFinal","CCT_SYS_EX29",e);   
            }


        } 
        catch (Exception e) {
        //     e.printStackTrace();
            logger.log(Level.SEVERE, getClass(),
                                                       "downloadQuoteRepFinal",
                                                      e.getMessage()); 
            CustomErrorHandler.processError("CCT","downloadQuoteRepFinal","CCT_SYS_EX29",e);   
        }
        logger.log(Level.INFO, getClass(),
                                                            "downloadQuoteRepFinal",
                                                            "Exiting downloadQuoteRepFinal method");


        }
public void quoteRevisionChnageListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
            try {
                               //valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
                                logger.log(Level.INFO, getClass(),
                                                                    "quoteRevisionChnageListener",
                                                                    "Entering quoteRevisionChnageListener method");

                    //            DCBindingContainer binds = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
                    //            DCIteratorBinding quoteRevisionIter = binds.findIteratorBinding("DistinctQuoteRevVO1Iterator");
                                //quoteRevisionIter.executeQuery();
                                oracle.adf.model.OperationBinding op = (oracle.adf.model.OperationBinding) BindingContext.getCurrent()
                                                                                       .getCurrentBindingsEntry()
                                                                                       .getOperationBinding("fetchQuoteDetails");
                                
                                JUCtrlListBinding listBinding =(JUCtrlListBinding)BindingContext.getCurrent().getCurrentBindingsEntry().get("DistinctQuoteRevVO1");
                                logger.log(Level.INFO, getClass(),
                                                                    "quoteRevisionChnageListener",
                                           ("List Binding Value====="+listBinding.getSelectedValue()));
                                Row quoteRevisionIter = (Row) listBinding.getCurrentRow();
                                
                                if (null!=quoteRevisionIter) {
                                    logger.log(Level.INFO, getClass(), "quoteRevisionChnageListener",
                                               ("current row ==" + quoteRevisionIter.getAttribute("RevisionId")));
                                    op.getParamsMap().put("revisionID", quoteRevisionIter.getAttribute("RevisionId"));
                                }
                                else{
                                    op.getParamsMap().put("revisionID",null);
                                }
                                logger.log(Level.INFO, getClass(), "quoteRevisionChnageListener",
                                           ("elected revision id===" + valueChangeEvent.getNewValue()));
                                op.getParamsMap().put("quoteVersion", valueChangeEvent.getNewValue());
                                op.execute();
             }
            catch (Exception e) 
                    {
                    // TODO: Add catch code
                    //   e.printStackTrace();
            
            logger.log(Level.SEVERE, getClass(),
                                                       "quoteRevisionChnageListener",
                                                      e.getMessage()); 
            CustomErrorHandler.processError("CCT","quoteRevisionChnageListener","CCT_SYS_EX20",e);   
        }
        logger.log(Level.INFO, getClass(),
                                            "quoteRevisionChnageListener",
                                            "Exiting quoteRevisionChnageListener method");

        }
        public void closeStatusListener(ActionEvent actionEvent) {
        // Add event code here...
        getBind_UploadStatus().hide();
    }

    public void summaryTabRefresh(DisclosureEvent disclosureEvent) {
        if(disclosureEvent.isExpanded()){
            DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter1 = binds.findIteratorBinding("QuotePricingFinalVVO1Iterator");
            iter1.executeQuery();
            DCIteratorBinding iter2 = binds.findIteratorBinding("QuoteClassFinalVVO1Iterator");
                  iter2.executeQuery();
            oracle.adf.model.OperationBinding operationBinding =
                (oracle.adf.model.OperationBinding) ADFUtils.findOperationBinding("getTop50FinalPricingSummary");
            //operationBinding.getParamsMap().put("opportunityNumber", getOpportunityNumber());
            operationBinding.execute();
        }
    }
    
    public DCBindingContainer getDCBindingsContainer() {
        DCBindingContainer bindingsContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        return bindingsContainer;
    }

    public void generateDownloadTemplate(String mappingkey) {
        oracle.binding.OperationBinding generateDownloadTemplate =
            ADFUtils.getBindingContainer().getOperationBinding("generateDownloadTemplate");
        generateDownloadTemplate.getParamsMap().put("pMappingKey", mappingkey);
        HashMap hashMap = (HashMap) generateDownloadTemplate.execute();

        quoteVoAttrMapping = (HashMap) hashMap.get("allQuoteVoAttrMapping");
        beaconMapping1 = (HashMap) hashMap.get("downloadBeaconMapping");
    }
    
    public XSSFWorkbook genericLogic1(XSSFWorkbook workbook, String Iterator, int sheetrownum, int headerrownum, String quoteRevisionId) {
        //CellStyle for DataFormatting Number without Decimal
        DataFormat numFormatWithoutDecimal = workbook.createDataFormat(); //format defined
        XSSFCellStyle cellStyleNumber = workbook.createCellStyle();
        cellStyleNumber.setDataFormat(numFormatWithoutDecimal.getFormat("#,###.0000"));
        cellStyleNumber.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        cellStyleNumber.setBorderTop(XSSFCellStyle.BORDER_THIN);
        cellStyleNumber.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        cellStyleNumber.setBorderRight(XSSFCellStyle.BORDER_THIN);

        //CellStyle for 0 value from DB
        XSSFCellStyle cellStyleFor0 = workbook.createCellStyle();
        cellStyleFor0.setDataFormat(numFormatWithoutDecimal.getFormat("0.0000"));
        cellStyleFor0.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        cellStyleFor0.setBorderTop(XSSFCellStyle.BORDER_THIN);
        cellStyleFor0.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        cellStyleFor0.setBorderRight(XSSFCellStyle.BORDER_THIN);


        //Below cell style is for Border
        XSSFCellStyle cellStyleForBorder = workbook.createCellStyle();
        cellStyleForBorder.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        cellStyleForBorder.setBorderTop(XSSFCellStyle.BORDER_THIN);
        cellStyleForBorder.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        cellStyleForBorder.setBorderRight(XSSFCellStyle.BORDER_THIN);
        XSSFSheet sheet = workbook.getSheetAt(0);

        DCBindingContainer bindings = this.getDCBindingsContainer();
        DCIteratorBinding itorBinding = bindings.findIteratorBinding(Iterator);
        ViewObject object = itorBinding.getViewObject();
        object.setNamedWhereClauseParam("b_QuoteRevisionId", quoteRevisionId);
        object.executeQuery();
      
        
        RowSetIterator iterator = object.createRowSetIterator(null);
        int i = sheetrownum;
        logger.log(Level.INFO, getClass(), "genericLogic1", "Loop started for generic Parts download");

        org.apache.poi.ss.usermodel.Row headerRow = sheet.getRow(headerrownum);
        Row currentRow = null;
        XSSFRow sheetRow = null;
        int logHeaderOnce = 0;
        try {

            while (iterator.hasNext()) { //Row iteration
                currentRow = iterator.next();
                sheetRow = sheet.createRow(i);
                i++;
                int index = 0; //Column
                for (index = 0; index < currentRow.getAttributeCount(); index++) {
                    Cell newCell = headerRow.getCell(index);
                    if (newCell != null) {
                        XSSFCell currentCell = sheetRow.createCell(index);
                        currentCell.setCellStyle(cellStyleForBorder);


                        if (newCell.getCellTypeEnum() != CellType.BLANK) {
                            String newHeader = newCell.getStringCellValue();
                            if (logHeaderOnce == 0) {
                                logger.log(Level.INFO, getClass(), "genericLogic1",
                                           "Header value of new cell= " + newHeader);
                            }
                            String destnFieldName = null;
                            if (beaconMapping1.get(newHeader) != null) {
                                destnFieldName = (String) beaconMapping1.get(newHeader)[0];
                            }

                            String voAttribute = null;
                            String dataFormat = "";
                            if (destnFieldName != null) {
                                voAttribute = quoteVoAttrMapping.get(destnFieldName);
                                dataFormat = (String) beaconMapping1.get(newHeader)[1];
                            } else {
                                //If any column mappings r not found in mappings table
                                voAttribute = null;
                                dataFormat = "";
                            }
                            if (null != currentRow && voAttribute != null &&
                                currentRow.getAttribute(voAttribute) != null &&
                                "" != currentRow.getAttribute(voAttribute).toString()) {

                                switch (dataFormat) {
                                case "COST": //This is for celltype with numeric value with decimal & comma
                                    currentCell.setCellType(CellType.NUMERIC);
                                    if(Double.valueOf(currentRow.getAttribute(voAttribute).toString())< 1.0 ){
                                        currentCell.setCellStyle(cellStyleFor0);
                                        currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(voAttribute)
                                                                                .toString()));
                                    } 
                                    else{
                                    currentCell.setCellStyle(cellStyleNumber);
                                    currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(voAttribute)
                                                                            .toString()));
                                    }
                                    break;
                                case "NUMBER": //This is for celltype with numeric value
                                    currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(voAttribute)
                                                                            .toString()));
                                    break;
                                default:
                                    currentCell.setCellValue(currentRow.getAttribute(voAttribute).toString());
                                }

                            } else {
                                if(newHeader.equalsIgnoreCase(ViewConstants.FINAL_BEACON_DOWNLOAD_HEADERS[0])){                                     
                                    currentCell.setCellType(CellType.NUMERIC);
                                    currentCell.setCellStyle(cellStyleFor0);
                                    currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(ViewConstants.FINAL_PARTS_VO_ATTIBUTES[0]).toString()));
                                }
                                if(newHeader.equalsIgnoreCase(ViewConstants.FINAL_BEACON_DOWNLOAD_HEADERS[1])){
                                    currentCell.setCellType(CellType.NUMERIC);
                                    currentCell.setCellStyle(cellStyleFor0);
                                    currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(ViewConstants.FINAL_PARTS_VO_ATTIBUTES[1]).toString()));
                                }
                                if(!newHeader.equalsIgnoreCase(ViewConstants.FINAL_BEACON_DOWNLOAD_HEADERS[0]) && !newHeader.equalsIgnoreCase(ViewConstants.FINAL_BEACON_DOWNLOAD_HEADERS[1]))
                                    currentCell.setCellValue("");
                                //Below logic is to Handle ASL which is coming from Opportunity table & not from QuoteParts
                                //                                if ("ASL".equals(newHeader)){
                                //                                    currentCell.setCellValue("ASLValue");
                                //                                }
                            }

                        }

                    }
                }
                logHeaderOnce++;
            }
        } catch (Exception nfe) {

            nfe.printStackTrace();
        }

        return workbook;

    }
    
    public void downloadFinalBeaconVersionFile(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        try {
            String quoteVersion = (String) JSFUtils.resolveExpression("#{bindings.DistinctQuoteRevVO1.inputValue}");
           
            DCBindingContainer binds = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding quoteRevisionIter = binds.findIteratorBinding("DistinctQuoteRevVO1Iterator");
            String quoteRevisionId = quoteRevisionIter.getCurrentRow().getAttribute("RevisionId").toString();

            generateDownloadTemplate(ViewConstants.FINAL_BEACON_VERSION_DOWNLOAD_KEY);

            facesContext.getCurrentInstance();
            String fileName = ViewConstants.FINAL_BEACON_VERSION_DOWNLOAD_TEMPLATE;
            String filePath = ViewConstants.FILE_PATH + fileName;
            FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);
            logger.log(Level.INFO, getClass(), "downloadFinalBeaconVersionFile", "Excel read");

            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFWorkbook filledWorkbook = genericLogic1(workbook, "QuotePartsFinalDownloadVO1Iterator", 1, 0, quoteRevisionId);
            XSSFSheet sheet = filledWorkbook.getSheetAt(0);
            XSSFRow topRow = sheet.getRow(0);
            int lovCol = 0;
            for (Cell c : topRow) {
                if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                    String text = c.getStringCellValue();
                    if ("Work Queue".equalsIgnoreCase(text)) {
                        lovCol = c.getColumnIndex();
                        break;
                    }
                }
            }
            filledWorkbook = addExcelLOV(filledWorkbook, 1, lovCol);
            
            try {
                logger.log(Level.INFO, getClass(), "downloadFinalBeaconVersionFile", "Writing workbook");

                filledWorkbook.write(outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (Exception e) {
                logger.log(Level.SEVERE, getClass(), "downloadFinalBeaconVersionFile", e.getMessage());
                CustomErrorHandler.processError("CCT", "downloadFinalBeaconVersionFile", "CCT_SYS_EX34", e);
            }


        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }

    }
    public XSSFWorkbook addExcelLOV(XSSFWorkbook workbook, int rowNum, int colNum) {

        String[] queues = getQueueNames();

        XSSFSheet sheet = workbook.getSheetAt(0);
        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sheet);
        XSSFDataValidationConstraint dvConstraint =
            (XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(queues);
        CellRangeAddressList addressList = new CellRangeAddressList(rowNum, sheet.getLastRowNum(), colNum, colNum);
        XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList);
        validation.setShowErrorBox(true);
        validation.setErrorStyle(XSSFDataValidation.ErrorStyle.STOP);
        validation.createErrorBox("Criteria Mismatch", ViewConstants.LOV_ERROR_MESSAGE);
        sheet.addValidationData(validation);
        validation.setSuppressDropDownArrow(false);


        return workbook;
    }
    public String[] getQueueNames() {
        DCBindingContainer bindingContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding binding = bindingContainer.findIteratorBinding("QuotePartsQueueVO1Iterator");
        ViewObject object = binding.getViewObject();
        int size = object.getRowCount();
        String[] queues = new String[size];
        ArrayList<String> queueNameList = new ArrayList<String>();

        RowSetIterator iterator = object.createRowSetIterator(null);
        while (iterator.hasNext()) {
            Row currentRow = iterator.next();
            String queueName = (String) currentRow.getAttribute("QueueName");
            logger.log(Level.INFO, getClass(), "getQueueNames", "QueueName= " + queueName);
            queueNameList.add(queueName);

        }
        String[] tempArray = new String[queueNameList.size()];
        queues = queueNameList.toArray(tempArray);
        return queues;
    }

    public void setReadyApproval(boolean readyApproval) {
        this.readyApproval = readyApproval;
    }

    public boolean isReadyApproval() {
        return readyApproval;
    }

    public void setUploadFinalBeacon(boolean uploadFinalBeacon) {
        this.uploadFinalBeacon = uploadFinalBeacon;
    }

    public boolean isUploadFinalBeacon() {
        return uploadFinalBeacon;
    }

    public void setDownloadFinalBeacon(boolean downloadFinalBeacon) {
        this.downloadFinalBeacon = downloadFinalBeacon;
    }

    public boolean isDownloadFinalBeacon() {
        return downloadFinalBeacon;
    }

    public void sendNotification(ActionEvent actionEvent) {
        // Add event code here...
        String status="";
//        String opptyNum= (String) JSFUtils.resolveExpression("#{bindings.OpportunityNumber.inputValue}");
        DCIteratorBinding binding = ADFUtils.findIterator("OpportunityVO1Iterator");
        Row currentRow = binding.getCurrentRow();
        
        OperationBinding sendEmail =
            ADFUtils.getBindingContainer().getOperationBinding("sendEmailNotification");
        sendEmail.getParamsMap().put("source", ViewConstants.READY_FOR_APPROVAL);
        sendEmail.getParamsMap().put("opptyNum", currentRow.getAttribute("OpportunityNumber").toString());
        sendEmail.getParamsMap().put("oscId", currentRow.getAttribute("OpportunityId").toString());
        sendEmail.getParamsMap().put("notfcnTemplateId", "18");
        status = (String)sendEmail.execute();
        FacesMessage fm = new FacesMessage("Notification has been sent to Proposal Manager");
        fm.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, fm);
      
    }

	public void setValidateMessage(String validateMessage) {
        this.validateMessage = validateMessage;
    }

    public String getValidateMessage() {
        return validateMessage;
    }

    public void executeFinalTop50Views(DisclosureEvent disclosureEvent) {
        // Add event code here...
        BigDecimal revisionId = new BigDecimal(resolveExpression("#{bindings.RevisionId.inputValue}").toString());  
        DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding cctFinalTop50PartsViewIterator = binds.findIteratorBinding("CctFinalTop50PartsViewIterator");
        ViewObject cctFinalTop50PartsVO = cctFinalTop50PartsViewIterator.getViewObject(); 
        cctFinalTop50PartsVO.setNamedWhereClauseParam("b_quoteRevisionId", revisionId);
        cctFinalTop50PartsVO.executeQuery();
        DCIteratorBinding binding = ADFUtils.findIterator("OpportunityVO1Iterator");
        Row currentRow = binding.getCurrentRow();
        oracle.adf.model.OperationBinding operationBinding =
            (oracle.adf.model.OperationBinding) ADFUtils.findOperationBinding("updatesupportTeam");
        operationBinding.getParamsMap().put("opportunityNumber",currentRow.getAttribute("OpportunityNumber").toString());
        operationBinding.execute();
        
    }

    public void setTop50FinalParts(boolean top50FinalParts) {
        this.top50FinalParts = top50FinalParts;
    }

    public boolean isTop50FinalParts() {
        return top50FinalParts;
    }
}
