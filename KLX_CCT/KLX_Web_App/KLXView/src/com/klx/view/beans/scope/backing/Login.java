package com.klx.view.beans.scope.backing;

import com.klx.common.cache.ParamCache;
import com.klx.common.exception.BusinessException;
import com.klx.common.exception.SystemException;
import com.klx.common.logger.KLXLogger;
import com.klx.view.beans.constants.DMSViewConstants;
import com.klx.model.impl.TaskQueryManagerImpl;
import com.klx.services.entities.Users;
import com.klx.view.beans.constants.ViewConstants;


import com.klx.view.error.CustomErrorHandler;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import java.util.logging.Level;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.PartialResultException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import javax.security.auth.Subject;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.http.HttpSession;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;

import weblogic.security.URLCallbackHandler;
import weblogic.security.services.Authentication;

import weblogic.servlet.security.ServletAuthentication;



public class Login {
    public Login() {
        super();
    }
    protected static String INITCTX =ParamCache.propertiesMap.get(ViewConstants.LDAP_INIT_CTX).toString();   
    protected static String MY_HOST = ParamCache.propertiesMap.get(ViewConstants.LDAP_HOST_URL).toString();  
    protected static String MY_SEARCHBASE = ParamCache.propertiesMap.get(ViewConstants.LDAP_SEARCH_BASE).toString();      
    protected static String MGR_DN = ParamCache.propertiesMap.get(ViewConstants.LDAP_USERNAME).toString();  
    protected static String MGR_PW =ParamCache.propertiesMap.get(ViewConstants.LDAP_PASSWORD).toString();
    private String MY_FILTER = ParamCache.propertiesMap.get(ViewConstants.LDAP_FILTER).toString(); 
    protected static Object LDAP_GROUPS_CONSIDERED =ParamCache.propertiesMap.get(ViewConstants.LDAP_GROUPS_CONSIDERED);
    private String username;
    private String password;
    private String oscOpportunityId;
    private String source;
    private String folderId;
    private String errorMessage;
    private boolean loginFailed=false;
    private String opportunityNumber;
    private String approval;
    private String loginUrl;
    private String groupId;
    private String attachmentPage;
    private boolean isAttachmentThroughEmail = false;
    private boolean isApprovalThroughEmail = false;

    public void setIsApprovalThroughEmail(boolean isApprovalThroughEmail) {
        this.isApprovalThroughEmail = isApprovalThroughEmail;
    }

    public boolean isIsApprovalThroughEmail() {
        return isApprovalThroughEmail;
    }

    public void setIsAttachmentThroughEmail(boolean isAttachmentThroughEmail) {
        this.isAttachmentThroughEmail = isAttachmentThroughEmail;
    }

    public boolean isIsAttachmentThroughEmail() {
        return isAttachmentThroughEmail;
    }


    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupId() {
        FacesContext ctx = FacesContext.getCurrentInstance();        
        Map requestParamMap = ctx.getExternalContext().getRequestParameterMap();
        groupId = (String)requestParamMap.get(ViewConstants.GROUP_ID); 
        return groupId;
    }
    private static KLXLogger logger = KLXLogger.getLogger();
    

    public void setApproval(String approval) {
        this.approval = approval;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }

    public String getLoginUrl() {
        return loginUrl;
    }

    public void setOpportunityNumber(String opportunityNumber) {
        this.opportunityNumber = opportunityNumber;
    }

    public String getOpportunityNumber() {
       FacesContext ctx = FacesContext.getCurrentInstance();
       HttpServletRequest request = (HttpServletRequest)ctx.getExternalContext().getRequest();
       Map requestParamMap = ctx.getExternalContext().getRequestParameterMap();
       opportunityNumber = (String)requestParamMap.get(ViewConstants.OPPORTUNITY_NUMBER); 
       //System.out.println("Opportunity Number"+opportunityNumber);
       return opportunityNumber;
    }
    private static ADFLogger _logger = ADFLogger.createADFLogger(Login.class); 
    private static final String CLAZZ_NAME="Login";
    


    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setLoginFailed(boolean loginFailed) {
        this.loginFailed = loginFailed;
    }

    public boolean isLoginFailed() {
        return loginFailed;
    }

    public void doLogin(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(),
                                            "Login",
                                            "Entering doLogin method");
        try {
        setLoginFailed(false);
        setErrorMessage("");
        String un = getUsername();
        FacesContext ctx = FacesContext.getCurrentInstance();
        String errorMsg=null;
        String result=null;
           
        if(null!=un && null!=getPassword()){
            result=weblogicAuthetication(un.toLowerCase(), getPassword());  // weblogic authentication
            if(null!=result && ViewConstants.SUCCESS.equalsIgnoreCase(result)){
                
            fetchLoggedInUserDetails(un);
            redirectionPage(un);            
            HttpServletResponse response = (HttpServletResponse)ctx.getExternalContext().getResponse();               
            response.sendRedirect(ctx.getExternalContext().getRequestContextPath() + loginUrl);
            ctx.responseComplete();
            }
            else{
                setLoginFailed(true);
                setErrorMessage("Invalid Username or Password. Please try again.");
            }
        }
       
        else{
            setLoginFailed(true);
            errorMsg="Username or Password cannot be empty.";
            setErrorMessage(errorMsg);
            }
        }catch(Exception e) {
            setLoginFailed(true);
            setErrorMessage("Invalid Username or Password. Please try again.");
            logger.log(Level.INFO, getClass(),
                                                "Login",
                                                "Error while login: "+e.getMessage());
        }    
        logger.log(Level.INFO, getClass(),
                                            "Login",
                                            "exiting doLogin method");
    }
    
    private void reportUnexpectedLoginError(String errType, Exception e) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected error during login", "Unexpected error during login (" + errType + "), please consult logs for detail");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        e.printStackTrace();
    }
    
    public String Logout() {
        logger.log(Level.INFO, getClass(),
                                            "Login",
                                            "Entering Logout method");
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = fctx.getExternalContext();
        HttpServletResponse response = (HttpServletResponse)ectx.getResponse();
        HttpSession session = (HttpSession)ectx.getSession(true);
        try {
        //String url = ectx.getRequestContextPath() + "/adfAuthentication?logout=true&end_url=/faces/Login.jspx";     
        session.removeAttribute("InitialContext");
        session.invalidate();
        
       //     ectx.redirect(ectx.getRequestContextPath() + "/faces/pages/login.jspx");
       ectx.redirect(ectx.getRequestContextPath() + ViewConstants.LOGOUT);
        } catch (IOException e) {
            logger.log(Level.SEVERE, getClass(),
                                                "Logout",
                                                e.getMessage());
            CustomErrorHandler.processError("CCT","uploadButtonACL","CCT_SYS_EX05",e);   
        }
        fctx.responseComplete();
        logger.log(Level.INFO, getClass(),
                                            "Login",
                                            "Exiting Logout method");
        return null;
    }

    public String Search() {
       
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = fctx.getExternalContext();
        HttpServletResponse response = (HttpServletResponse)ectx.getResponse();
        HttpSession session = (HttpSession)ectx.getSession(true);
        try {
        //String url = ectx.getRequestContextPath() + "/adfAuthentication?logout=true&end_url=/faces/Login.jspx";     
        session.removeAttribute("InitialContext");
        session.invalidate();
        
       //     ectx.redirect(ectx.getRequestContextPath() + "/faces/pages/login.jspx");
       ectx.redirect(ectx.getRequestContextPath() + ViewConstants.SEARCH);
        } catch (IOException e) {
            logger.log(Level.SEVERE, getClass(),
                                                "Logout",
                                                e.getMessage());
              
        }
        fctx.responseComplete();
        logger.log(Level.INFO, getClass(),
                                            "Login",
                                            "Exiting Logout method");
        return null;
    }
    public void resetLoginFields(ActionEvent actionEvent) {
        // Add event code here...
        setUsername(null);
        setPassword(null);
        setLoginFailed(false);
    }
    
    public String weblogicAuthetication(String un,String pwd){
        logger.log(Level.INFO, getClass(),
                                            "Login",
                                            "Entering weblogicAuthetication method");
        byte[] pw =null;
        String result=ViewConstants.FAILURE;
        try{
        pw = pwd.getBytes();            
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)ctx.getExternalContext().getRequest();        
        Subject subject = Authentication.login(new URLCallbackHandler(un, pw));
        logger.log(Level.INFO, getClass(), "weblogicAuthetication", "Original user and group names from LDAP-"+subject.getPrincipals());
        List userGroups = new ArrayList(); 
        subject.getPrincipals().iterator().forEachRemaining(userGroups::add);        
        List filterUserGroups = filterUserGroups(userGroups);
        subject.getPrincipals().removeAll(userGroups);        
        subject.getPrincipals().addAll(filterUserGroups); 
        logger.log(Level.INFO, getClass(), "weblogicAuthetication", "List of user and groups after removal-"+subject.getPrincipals());
        ServletAuthentication.runAs(subject, request); 
        result=ViewConstants.SUCCESS;
        } 
        catch (LoginException e) {
            setLoginFailed(true);
            setErrorMessage("Invalid Username or Password. Please try again.");
            logger.log(Level.SEVERE, getClass(),
                                            "weblogicAuthetication",
                                           e.getMessage());
        }
        catch(Exception e){
            
            logger.log(Level.SEVERE, getClass(),
                                            "weblogicAuthetication",
                                           e.getMessage());
            CustomErrorHandler.processError("CCT","uploadButtonACL","CCT_SYS_EX26",e);   
        }
        logger.log(Level.INFO, getClass(),
                                            "Login",
                                            "Exiting weblogicAuthetication method");
        return result;

    }
    
    public List filterUserGroups(List userGroups){       
        logger.log(Level.INFO, getClass(), "filterUserGroups", "Entering filterUserGroups method");
        ArrayList filterUserGroups = new ArrayList();
        filterUserGroups.addAll(userGroups);
        for(int i=0; i < userGroups.size(); i++){
            Object groupName = userGroups.get(i);
            if(null != LDAP_GROUPS_CONSIDERED && LDAP_GROUPS_CONSIDERED.toString().equalsIgnoreCase(ViewConstants.NON_PROD)){
                 if(!groupName.toString().contains(ViewConstants.NON_PROD)
                        && groupName.toString().contains(ViewConstants.SC_CCT))                
                    filterUserGroups.remove(groupName);
            } else {
               if(groupName.toString().contains(ViewConstants.NON_PROD)
                   && groupName.toString().contains(ViewConstants.SC_CCT))
                    filterUserGroups.remove(groupName);
            }
        }
        
        
        logger.log(Level.INFO, getClass(), "filterUserGroups", "Exiting filterUserGroups method");
        return filterUserGroups;
    }

    
    public String activeDirAuthentication(String un,String pwd){
        
        String result=ViewConstants.FAILURE;
        try { 
           
          Hashtable params = new Hashtable();    
          params.put(Context.INITIAL_CONTEXT_FACTORY, INITCTX);  
          params.put(Context.PROVIDER_URL, MY_HOST);  
          params.put(Context.SECURITY_AUTHENTICATION, ViewConstants.SECURITY_AUTHENTICATION);  
          params.put(Context.SECURITY_PRINCIPAL, MGR_DN);  
          params.put(Context.SECURITY_CREDENTIALS, MGR_PW);  
        
          DirContext ctx = new InitialDirContext(params);  
        
          SearchControls constraints = new SearchControls();  
        
          constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);  
            
          MY_FILTER=MY_FILTER+un;
         
          NamingEnumeration results = ctx.search(MY_SEARCHBASE, MY_FILTER, constraints);  
        
          if (results != null && results.hasMore()) {  
            SearchResult sr = (SearchResult)results.next();  
            String distinguishedName = sr.getNameInNamespace();
              logger.log(Level.INFO, getClass(),
                                                  "activeDirAuthentication",
                                                  "Desc name is " + distinguishedName);
            Attributes attrs = sr.getAttributes();  
              
              Hashtable paramsUser = new Hashtable();    
              paramsUser.put(Context.INITIAL_CONTEXT_FACTORY, INITCTX);  
              paramsUser.put(Context.PROVIDER_URL, MY_HOST);  
              paramsUser.put(Context.SECURITY_AUTHENTICATION, ViewConstants.SECURITY_AUTHENTICATION);  
              paramsUser.put(Context.SECURITY_PRINCIPAL, distinguishedName);  
              paramsUser.put(Context.SECURITY_CREDENTIALS, pwd);                                           
              DirContext ctxUser = new InitialDirContext(paramsUser); 
              if(null!=ctxUser){
                  logger.log(Level.INFO, getClass(),
                                                      "activeDirAuthentication",
                                                      "Authentication successful");
                  result=ViewConstants.SUCCESS; 
              }
          } else {  
              logger.log(Level.INFO, getClass(),
                                                  "activeDirAuthentication",
                                                  "Not exist User");
              setLoginFailed(true);
              setErrorMessage("User does not exist.");
             
              
          }  
        } catch (AuthenticationException e) {  
            setLoginFailed(true);
            setErrorMessage("Invalid Username or Password. Please try again.");
           
            _logger.severe(e.getMessage());  
        } catch (PartialResultException e) {  
            setLoginFailed(true);
            setErrorMessage("Invalid Username or Password. Please try again.");
            _logger.severe(e.getMessage());  
        } catch (NamingException e) {  
            _logger.severe(e.getMessage()); 
        }  
        return result;
    }
	
	public void setOpportunityId(String opportunityId) {        
        this.oscOpportunityId = opportunityId;
    }

    public String getOpportunityId() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)ctx.getExternalContext().getRequest();
        Map requestParamMap = ctx.getExternalContext().getRequestParameterMap();
        oscOpportunityId = (String)requestParamMap.get(ViewConstants.OPPORTUNITY_ID);        
        return oscOpportunityId;
    }
    public void setSource(String source) {
            this.source = source;
    }

    public String getSource() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)ctx.getExternalContext().getRequest();
        Map requestParamMap = ctx.getExternalContext().getRequestParameterMap();
        source = (String)requestParamMap.get(ViewConstants.SOURCE);
        if (source == null || source.equalsIgnoreCase("")){
            source = ViewConstants.BPM;
        }
        return source;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public String getFolderId() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)ctx.getExternalContext().getRequest();
        Map requestParamMap = ctx.getExternalContext().getRequestParameterMap();
        folderId = (String)requestParamMap.get(ViewConstants.FOLDERID);
//        if (folderId == null || folderId.equalsIgnoreCase("")){
//            folderId = 1;
//        commented}
        return folderId;
    }
    
    public String getApproval() {
            FacesContext ctx = FacesContext.getCurrentInstance();
            HttpServletRequest request = (HttpServletRequest)ctx.getExternalContext().getRequest();
            Map requestParamMap = ctx.getExternalContext().getRequestParameterMap();
            approval = (String)requestParamMap.get(ViewConstants.APPROVAL);
            logger.log(Level.INFO, getClass(),
                                                "getApproval",
                                                "Approval = "+approval);
            return approval;
            
        }

    @SuppressWarnings("unchecked")
    public void redirectionPage(String un){
            loginUrl = (String)ParamCache.propertiesMap.get("COLLABORATION_URL");
            try {
            Users loggedInUser = (Users) ADFContext.getCurrent()
                                                   .getSessionScope()
                                                   .get(ViewConstants.LOGGED_IN_USER);
            List<String> loggedInUserGroupIds = loggedInUser.getUserGroups();

            TaskQueryManagerImpl taskDetails = new TaskQueryManagerImpl();
            if (null != opportunityNumber) {
                String opportunityID = taskDetails.fetchOpportunityID(opportunityNumber);
                
                Integer opportuniyid=0;
                if(null!=opportunityID)
                  opportuniyid= Integer.parseInt(opportunityID);
                ADFContext.getCurrent()
                          .getSessionScope()
                          .put(ViewConstants.OPPORTUNITY_NUMBER, opportunityNumber);
                ADFContext.getCurrent()
                          .getSessionScope()
                          .put(ViewConstants.OPPORTUNITY_ID, opportuniyid);

            }

            if ("Y".equalsIgnoreCase(approval)) {

                List<String> taskDetail = taskDetails.fetchTaskDetails(opportunityNumber, un, groupId);
                this.isApprovalThroughEmail = true;
                if(taskDetail.size() > 0){
                    ADFContext.getCurrent()
                              .getSessionScope()
                              .put(ViewConstants.taskId, taskDetail.get(0));
                    ADFContext.getCurrent()
                              .getSessionScope()
                              .put(ViewConstants.TASK_TYPE, taskDetail.get(1));
                    ADFContext.getCurrent()
                              .getSessionScope()
                              .put(ViewConstants.TASK_OWNER_USER, taskDetail.get(2));                    
                }
                ADFContext.getCurrent()
                          .getSessionScope()
                          .put(ViewConstants.ISAPPROVALTHROUGHEMAIL,isApprovalThroughEmail);
                loginUrl = (String) ParamCache.propertiesMap.get("APPROVAL_URL");
                if(taskDetail.size() > 0){
                    logger.log(Level.INFO, getClass(), "doLogin", "taskId = " + taskDetail.get(0));
                    logger.log(Level.INFO, getClass(), "doLogin", "TASK_TYPE = " + taskDetail.get(1));
                    logger.log(Level.INFO, getClass(), "doLogin", "TASK_OWNER_USER" + taskDetail.get(2));
                }
            }
            if (null != opportunityNumber && "Y".equalsIgnoreCase(attachmentPage)) {
               // System.out.println("Inside attachment");
                loginUrl = (String) ParamCache.propertiesMap.get("DOCUMENT_MGMT_URL");
                this.isAttachmentThroughEmail=true;
                ADFContext.getCurrent()
                          .getSessionScope()
                          .put(ViewConstants.ISATTACHMENTTHROUGHEMAIL,isAttachmentThroughEmail);

            }
            if (null != source) {
                ADFContext.getCurrent()
                          .getSessionScope()
                          .put(ViewConstants.SOURCE, source);
            }
            if (null != folderId) {
                ADFContext.getCurrent()
                          .getSessionScope()
                          .put(ViewConstants.FOLDERID, folderId);
            }
            if (loggedInUserGroupIds.contains(ParamCache.userRoleMap.get(ViewConstants.CCT_SALES))) {
                ADFContext.getCurrent()
                          .getSessionScope()
                          .put(ViewConstants.SALES_USER, true);
                if(attachmentPage == null && approval == null && opportunityNumber == null){                    
                    loginUrl = (String) ParamCache.propertiesMap.get("OPPTY_DASHBOARD_URL");
                }
            } else{
                ADFContext.getCurrent()
                          .getSessionScope()
                          .put(ViewConstants.SALES_USER, false);
            }
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }
        }
        
    public void fetchLoggedInUserDetails(String userId){
                OperationBinding op = (OperationBinding) BindingContext.getCurrent().getCurrentBindingsEntry().getOperationBinding("fetchLoggedInUserDetails");
                op.getParamsMap().put("userId", userId);   
                Users user=(Users)op.execute();
                ADFContext.getCurrent().getSessionScope().put(ViewConstants.LOGGED_IN_USER, user);
            }

    public void setAttachmentPage(String attachment) {
        this.attachmentPage = attachment;
    }

    public String getAttachmentPage() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)ctx.getExternalContext().getRequest();
        Map requestParamMap = ctx.getExternalContext().getRequestParameterMap();
        attachmentPage = (String)requestParamMap.get(ViewConstants.ATTACHMENT_PAGE);
        //System.out.println("attachment = "+attachmentPage);
       /* if(null!=attachmentPage)
            ADFContext.getCurrent()
                      .getSessionScope()
                      .put(ViewConstants.ATTACHMENT_PAGE,attachmentPage);*/
        return attachmentPage;
    }
}
