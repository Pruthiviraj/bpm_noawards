package com.klx.view.beans.scope.pageFlow;

import com.klx.common.access.UserActionAccessManagement;
import com.klx.common.logger.KLXLogger;
import com.klx.view.beans.constants.ViewConstants;
import com.klx.view.beans.utils.ADFUtils;
import com.klx.view.beans.utils.JSFUtils;

import org.apache.poi.ss.usermodel.DataFormat;

import com.klx.view.error.CustomErrorHandler;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List; 
import java.util.Timer;

import java.util.logging.Level;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputFile;
import oracle.adf.view.rich.component.rich.input.RichSelectOneRadio;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.VariableValueManager;
import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewCriteriaManager;
import oracle.jbo.ViewObject;

import oracle.jbo.server.ViewObjectImpl;

import org.apache.myfaces.trinidad.model.UploadedFile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import java.util.Set;
import java.util.HashSet; 
import java.util.Iterator;

public class AwardsBean {
    private RichPopup popupBinding;
    private RichInputFile fileBinding;
    private long startTime;
    private String uploadStatusMessage = ViewConstants.UPL_STATUS_SUCCESS;
    private RichPopup statusPopUpBinding;
    private RichOutputText opptyId;
    private HashMap<String, String> awardsDownloadVoAttrMapping = new HashMap<String, String>();
    private HashMap<String, String[]> downloadAwardsReviewMapping = new HashMap<String, String[]>();
    private static KLXLogger logger = KLXLogger.getLogger();
    private RichButton uploadButton;
    private String awardHeaderId;
    private boolean downloadAwards = false;
    private boolean uploadAwards = false;
    private boolean notQuotedAwards = false;
    private String validationMessage = "";

    public void setValidationMessage(String validationMessage) {
        this.validationMessage = validationMessage;
    }

    public String getValidationMessage() {
        return validationMessage;
    }

    public void setNotQuotedAwards(boolean notQuotedAwards) {
        this.notQuotedAwards = notQuotedAwards;
    }

    public boolean isNotQuotedAwards() {
        return notQuotedAwards;
    }

    public void setDownloadAwards(boolean downloadAwards) {
        this.downloadAwards = downloadAwards;
    }

    public boolean isDownloadAwards() {
        return downloadAwards;
    }

    public void setUploadAwards(boolean uploadAwards) {
        this.uploadAwards = uploadAwards;
    }

    public boolean isUploadAwards() {
        return uploadAwards;
    }

    public void setUploadStatusMessage(String uploadStatusMessage) {
        this.uploadStatusMessage = uploadStatusMessage;
    }

    public String getUploadStatusMessage() {
        return uploadStatusMessage;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public AwardsBean() {
        userActionsAccess();
    }
    
    public void userActionsAccess(){
        HashMap actions = new HashMap();
        actions.put(ViewConstants.DOWNLOAD_AWARD, false);            
        actions.put(ViewConstants.UPLOAD_AWARD, false);
        actions.put(ViewConstants.QUOTED_NOT_AWARDED, false);
        HashMap userAcess = UserActionAccessManagement.getUserAccess(ADFContext.getCurrent().getSecurityContext().getUserRoles(), actions);        
        downloadAwards = ((Boolean)userAcess.get(ViewConstants.DOWNLOAD_AWARD)).booleanValue();    
        uploadAwards = ((Boolean)userAcess.get(ViewConstants.UPLOAD_AWARD)).booleanValue();    
        notQuotedAwards = ((Boolean)userAcess.get(ViewConstants.QUOTED_NOT_AWARDED)).booleanValue();
    }
    private int customerNo;
    private String opptyNo;

    public void setOpptyNo(String opptyNo) {
        this.opptyNo = opptyNo;
    }

    public String getOpptyNo() {
        return opptyNo;
    }

    public void setCustomerNo(int customerNo) {
        this.customerNo = customerNo;
    }

    public int getCustomerNo() {
        return customerNo;
    }

    public void openUploadPopup(ActionEvent actionEvent) {
        // Add event code here...
        try {
            String uploadType = (String) JSFUtils.resolveExpression("#{pageFlowScope.uploadType}");
            RichInputFile inputFile = getFileBinding();
            if (null != inputFile) {
                inputFile.setValue(null);
                inputFile.setSubmittedValue(null);
                AdfFacesContext.getCurrentInstance().addPartialTarget(inputFile);
            }

            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding itorBinding = bindings.findIteratorBinding("showOpptyVO1Iterator");
            ViewObject vo = itorBinding.getViewObject();
            vo.setNamedWhereClauseParam("b_cust_no", getCustomerNo());
            vo.executeQuery();
            if (uploadType.equalsIgnoreCase("CREATE") && vo.getEstimatedRowCount() == 0) {
                FacesMessage Message = new FacesMessage("There is no opportunity with status='Won' for this customer.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                return;
            } else {
                String opptyNum = getOpptyNo();
                RowSetIterator showOpptyIter = vo.createRowSetIterator(null);

                while (showOpptyIter.hasNext()) {
                    Row showOpptyRow = showOpptyIter.next();
                    if (showOpptyRow.getAttribute("OpportunityNumber")
                                    .toString()
                                    .equalsIgnoreCase(opptyNum)) {
                        showOpptyRow.setAttribute("selectBox", "true");
                    }
                }
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                getPopupBinding().show(hints);
               
            }
            

        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
    }


    public void setPopupBinding(RichPopup popupBinding) {
        this.popupBinding = popupBinding;
    }

    public RichPopup getPopupBinding() {
        return popupBinding;
    }

    public void setFileBinding(RichInputFile fileBinding) {
        this.fileBinding = fileBinding;
    }

    public RichInputFile getFileBinding() {
        return fileBinding;
    }

    public void storeOpptyIds(UploadedFile newValue, Object opptyid) {
        //        String strVal= (String)opptyid.toString();
        //        BigDecimal opptyId = new BigDecimal(strVal);
        BindingContainer bindings1 = getBindings();
        BindingContext bindingContext = BindingContext.getCurrent();
        BindingContainer bindingContainer = bindingContext.getCurrentBindingsEntry();
        OperationBinding binding = bindingContainer.getOperationBinding("getMaxAwardHeaderId");
        BigDecimal awardHeaderId = (BigDecimal) binding.execute();
        //  BigDecimal awardHeaderId = maxId.add(new BigDecimal(1));

        String fileName = newValue.getFilename();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        try {

            DCIteratorBinding awdHeaderBind = bindings.findIteratorBinding("CctAwardHeaderVO1Iterator");
            ViewObject awardHeaderVO = awdHeaderBind.getViewObject();
            RowSetIterator awardHeaderIter = awdHeaderBind.getRowSetIterator();

            /*       DCIteratorBinding itorBinding = bindings.findIteratorBinding("showOpptyVO1Iterator");
            ViewObject vo = itorBinding.getViewObject();

            RowSetIterator opptyDetailsIterator = vo.createRowSetIterator(null);
            int cnt = 0;
            while (opptyDetailsIterator.hasNext()) {
                Row showOpptyRow = opptyDetailsIterator.next();
               
                // awardHeaderVO.last();

                // awardHeaderVO.next();

                //Object x = showOpptyRow.getAttribute("selectBox");
                if (showOpptyRow.getAttribute("selectBox") != null) {
                    cnt += 1;
                    if (showOpptyRow.getAttribute("selectBox").equals(true)) {
                        Row awardHeaderRow = awardHeaderVO.createRow();
                        awardHeaderRow.setAttribute("AwardHeaderId", awardHeaderId);
                        awardHeaderRow.setAttribute("SourceOpportunityId", opptyid);
                        awardHeaderRow.setAttribute("AwardFilename", fileName);
                        awardHeaderRow.setAttribute("OpportunityId", showOpptyRow.getAttribute("OpportunityId"));
                        awardHeaderVO.insertRow(awardHeaderRow);
                    }

                }

            }*/
            Row awardHeaderRow = awardHeaderVO.createRow();
            awardHeaderRow.setAttribute("AwardHeaderId", awardHeaderId);
            awardHeaderRow.setAttribute("SourceOpportunityId", opptyid);
            awardHeaderRow.setAttribute("AwardFilename", fileName);
            awardHeaderRow.setAttribute("OpportunityId", opptyid);
            awardHeaderVO.insertRow(awardHeaderRow);

            OperationBinding operationBinding = bindings1.getOperationBinding("Commit");
            operationBinding.execute();
        } catch (Exception ae) {
            ae.printStackTrace();
            OperationBinding operationBinding = bindings1.getOperationBinding("Rollback");
            operationBinding.execute();
            logger.log(Level.SEVERE, getClass(), "storeOpptyIds", ae.getMessage());
            CustomErrorHandler.processError("CCT", "storeOpptyIds", "CCT_SYS_EX39", ae);
        }
    }

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void uploadButtonACL(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(), "uploadButtonACL", "Entering uploadButtonACL method");
       
        UploadedFile newValue = (UploadedFile) getFileBinding().getValue();
        if(null != newValue){
        HashMap hashMap = new HashMap();
        String uploadType = "CREATE"; // As file uploadtype is only Override, it is defaulted to Create.
       
        if (uploadType.equalsIgnoreCase("CREATE")) {
            Object opptyid1 = JSFUtils.resolveExpression("#{bindings.OpportunityId.inputValue}");
            storeOpptyIds(newValue, opptyid1);
        }

        String filename = newValue.getFilename();
        if (filename.endsWith("xlsx") || filename.endsWith("XLSX")) {
            try {
                startTime = System.currentTimeMillis();

                InputStream inputStream = newValue.getInputStream();
                InputStream fileinputStream = newValue.getInputStream();
                BindingContext bindingContext = BindingContext.getCurrent();
                BindingContainer bindingContainer = bindingContext.getCurrentBindingsEntry();
                Object opptyid = JSFUtils.resolveExpression("#{bindings.OpportunityId.inputValue}");
                BigDecimal oppty = (BigDecimal) opptyid;
              
              /**header validation start**/
              OperationBinding binding = bindingContainer.getOperationBinding("uploadAwardsFileData");
              binding.getParamsMap().put("inputStream", fileinputStream);
                hashMap= validateBeaconHeaders( inputStream, "Award Headers", 0);
                HashMap inputParamMap = new HashMap();
                String status = (String) hashMap.get(ViewConstants.UPLOAD_STATUS1);
                String uploadFailureReason = (String) hashMap.get("uploadFailureReason");
                if ("successful".equalsIgnoreCase(status)) {
                    inputParamMap.put(ViewConstants.UPLOAD_TYPE1, uploadType);
                    inputParamMap.put("sheetName", "Award Headers");
                     
                    binding.getParamsMap().put("inputParamMap", inputParamMap);
                    binding.getParamsMap().put("opptyid", opptyid);
                    hashMap = (HashMap) binding.execute();
                      
                    status = (String) hashMap.get(ViewConstants.UPLOAD_STATUS1);
                    uploadFailureReason = (String) hashMap.get("uploadFailureReason");  
                }
                if(status.equalsIgnoreCase("unsuccessful")){
                    setUploadStatusMessage(ViewConstants.UPL_STATUS_FAILURE);
                    setValidationMessage(uploadFailureReason);
                }
                else{ 
                    setUploadStatusMessage(ViewConstants.UPL_STATUS_SUCCESS);
                    setValidationMessage("");
                    /* To retrieve AwardHeaderID from CctAwardHeaderVO*/
                    DCBindingContainer bindings = this.getDCBindingsContainer();
                    DCIteratorBinding cctAwardHeaderVO1Iterator =
                        bindings.findIteratorBinding("CctAwardHeaderVO1Iterator");
                    ViewObject cctAwardHeaderVO = cctAwardHeaderVO1Iterator.getViewObject();

                    ViewCriteriaManager vcm = cctAwardHeaderVO.getViewCriteriaManager();
                    ViewCriteria cctAwardHeaderVOCriteria = vcm.getViewCriteria("CctAwardHeaderVOCriteria");
                    VariableValueManager vvm = cctAwardHeaderVOCriteria.ensureVariableManager();
                    vvm.setVariableValue("b_opptyId", opptyid);
                    cctAwardHeaderVO.applyViewCriteria(cctAwardHeaderVOCriteria);
                    cctAwardHeaderVO.executeQuery();
                    BigDecimal awdHeaderId = null;
                    RowSetIterator awdHeaderIterator = cctAwardHeaderVO.createRowSetIterator(null);
                    while (awdHeaderIterator.hasNext()) {
                        Row awardHeaderRow = awdHeaderIterator.next();
                        awdHeaderId = new BigDecimal(awardHeaderRow.getAttribute("AwardHeaderId").toString());
                        this.awardHeaderId = awdHeaderId.toString();
                    }
                  
                    /*Refreshing AwardHeadersVVO*/
                    DCIteratorBinding awdSummaryBinding = bindings.findIteratorBinding("AwardHeadersVVO1Iterator");
                    ViewObject awdSummaryObject = awdSummaryBinding.getViewObject();
                    awdSummaryObject.setNamedWhereClauseParam("b_award_hdr_id", this.awardHeaderId);
                    awdSummaryObject.setNamedWhereClauseParam("b_opp_id", oppty);
                    awdSummaryObject.executeQuery();

                    /*Refreshing QuotedNotAwardedRVO*/
                    DCIteratorBinding itorBinding2 = bindings.findIteratorBinding("QuotedNotAwardedRVOIterator");
                    ViewObject object2 = itorBinding2.getViewObject();
                    object2.setNamedWhereClauseParam("b_award_hdr_id", this.awardHeaderId);
                    object2.setNamedWhereClauseParam("b_opp_id", opptyid);
                    object2.executeQuery();

                    /*Refreshing AwardsReviewVO*/
                    DCIteratorBinding itorBinding3 = bindings.findIteratorBinding("AwardsReviewVO1Iterator");
                    ViewObject object3 = itorBinding3.getViewObject();
                    object3.setNamedWhereClauseParam("b_award_hdr_id", this.awardHeaderId);
                    object3.setNamedWhereClauseParam("b_opp_id", opptyid);
                    object3.executeQuery();
                  

                    
                }
                /**header validation end**/

                getPopupBinding().hide();
                DCBindingContainer bindings = this.getDCBindingsContainer();
                DCIteratorBinding cctAwardHeaderVO1Iterator = bindings.findIteratorBinding("CctAwardHeaderVO1Iterator");
                ViewObject cctAwardHeaderVO = cctAwardHeaderVO1Iterator.getViewObject();

                ViewCriteriaManager vcm = cctAwardHeaderVO.getViewCriteriaManager();
                ViewCriteria cctAwardHeaderVOCriteria = vcm.getViewCriteria("CctAwardHeaderVOCriteria");
                VariableValueManager vvm = cctAwardHeaderVOCriteria.ensureVariableManager();
                vvm.setVariableValue("b_opptyId", opptyid);
                cctAwardHeaderVO.applyViewCriteria(cctAwardHeaderVOCriteria);
                cctAwardHeaderVO.executeQuery();
                BigDecimal awdHeaderId = null;
                RowSetIterator awdHeaderIterator = cctAwardHeaderVO.createRowSetIterator(null);
                while (awdHeaderIterator.hasNext()) {
                    Row awardHeaderRow = awdHeaderIterator.next();
                    awdHeaderId = new BigDecimal(awardHeaderRow.getAttribute("AwardHeaderId").toString());
                }

              
                refreshAwardsQuoteParts(awdHeaderId, oppty);

                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                getStatusPopUpBinding().show(hints);
                logger.log(Level.INFO, getClass(), "uploadButtonACL", "start time is " + startTime);
                logger.log(Level.INFO, getClass(), "uploadButtonACL", "End time is " + System.currentTimeMillis());
                logger.log(Level.INFO, getClass(), "uploadButtonACL",
                           "Estimated time of execution is in secs =" +
                           (System.currentTimeMillis() - startTime) / 1000);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                logger.log(Level.SEVERE, getClass(), "uploadButtonACL", e.getMessage());
                CustomErrorHandler.processError("CCT", "uploadButtonACL", "CCT_SYS_EX40", e);
            } catch (Exception e) {
                e.printStackTrace();
                logger.log(Level.SEVERE, getClass(), "uploadButtonACL", e.getMessage());
                CustomErrorHandler.processError("CCT", "uploadButtonACL", "CCT_SYS_EX40", e);

            }

        } else {
            FacesMessage fm = new FacesMessage(ViewConstants.INVALID_FILE_FORMAT);
            fm.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext context = FacesContext.getCurrentInstance();

            context.addMessage(getFileBinding().getClientId(), fm);
            logger.log(Level.INFO, getClass(), "uploadButtonACL", "Exiting uploadButtonACL method");
        }
        }else{
            FacesMessage fm = new FacesMessage(ViewConstants.FILE_NOT_SELECTED);
            fm.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext context = FacesContext.getCurrentInstance();

            context.addMessage(getFileBinding().getClientId(), fm);
            logger.log(Level.INFO, getClass(), "uploadButtonACL", "Exiting uploadButtonACL method");
        }
        }

    public void refreshAwardsQuoteParts(BigDecimal awdHeaderId, BigDecimal opptyId) {

        refreshPageIterator("AwardsReviewVO1Iterator", awdHeaderId, opptyId);
    }

    public void refreshPageIterator(String iteratorName, BigDecimal awdHeaderId, BigDecimal opptyId) {
        DCBindingContainer bindingContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding binding = bindingContainer.findIteratorBinding(iteratorName);
        ViewObject object = binding.getViewObject();
        object.setNamedWhereClauseParam("b_award_hdr_id", awdHeaderId);
        object.setNamedWhereClauseParam("b_opp_id", opptyId);
        object.executeQuery();
        logger.log(Level.INFO, getClass(), "refreshPageIterator",
                   "AwardsQuotePartsvO estimated rows= " + object.getEstimatedRowCount());
    }

    public void setStatusPopUpBinding(RichPopup statusPopUpBinding) {
        this.statusPopUpBinding = statusPopUpBinding;
    }

    public RichPopup getStatusPopUpBinding() {
        return statusPopUpBinding;
    }

    public void okACL(ActionEvent actionEvent) {
        // Add event code here...
        getStatusPopUpBinding().hide();
    }

    public void setOpptyId(RichOutputText opptyId) {
        this.opptyId = opptyId;
    }

    public RichOutputText getOpptyId() {
        return opptyId;
    }

    public void CancelListener(ActionEvent actionEvent) {
        // Add event code here...
        getPopupBinding().hide();

    }

    public DCBindingContainer getDCBindingsContainer() {
        DCBindingContainer bindingsContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        return bindingsContainer;
    }

    public void generateAwardsReviewDownloadTemplate(String mappingkey) {
        OperationBinding generateAwardsReviewDownloadTemplate =
            ADFUtils.getBindingContainer().getOperationBinding("generateAwardsReviewDownloadTemplate");
        generateAwardsReviewDownloadTemplate.getParamsMap().put("pMappingKey", mappingkey);
        HashMap hashMap = (HashMap) generateAwardsReviewDownloadTemplate.execute();

        downloadAwardsReviewMapping = (HashMap) hashMap.get("downloadAwardsReviewMapping");
        awardsDownloadVoAttrMapping = (HashMap) hashMap.get("awardsReviewVoAttrMapping");
    }

    public XSSFWorkbook genericLogic(XSSFWorkbook workbook, String Iterator, int sheetrownum, int headerrownum) {

        //CellStyle for DataFormatting Number without Decimal
        DataFormat numFormatWithoutDecimal = workbook.createDataFormat(); //format defined
        XSSFCellStyle cellStyleCost = workbook.createCellStyle();
        cellStyleCost.setDataFormat(numFormatWithoutDecimal.getFormat("#,###.0000"));
        cellStyleCost.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        cellStyleCost.setBorderTop(XSSFCellStyle.BORDER_THIN);
        cellStyleCost.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        cellStyleCost.setBorderRight(XSSFCellStyle.BORDER_THIN);

        //CellStyle for 0 value from DB
        XSSFCellStyle cellStyleFor0 = workbook.createCellStyle();
        cellStyleFor0.setDataFormat(numFormatWithoutDecimal.getFormat("0.0000"));
        cellStyleFor0.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        cellStyleFor0.setBorderTop(XSSFCellStyle.BORDER_THIN);
        cellStyleFor0.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        cellStyleFor0.setBorderRight(XSSFCellStyle.BORDER_THIN);

        //Below cell style is for Border
        XSSFCellStyle cellStyleForBorder = workbook.createCellStyle();
        cellStyleForBorder.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        cellStyleForBorder.setBorderTop(XSSFCellStyle.BORDER_THIN);
        cellStyleForBorder.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        cellStyleForBorder.setBorderRight(XSSFCellStyle.BORDER_THIN);
        
        XSSFCellStyle cellStylePercentage = workbook.createCellStyle();
        cellStylePercentage.setDataFormat(workbook.createDataFormat().getFormat("0.00%"));
        cellStylePercentage.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        cellStylePercentage.setBorderTop(XSSFCellStyle.BORDER_THIN);
        cellStylePercentage.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        cellStylePercentage.setBorderRight(XSSFCellStyle.BORDER_THIN);
        XSSFSheet sheet = workbook.getSheetAt(2);
        
        
        Object opptyid1 = JSFUtils.resolveExpression("#{bindings.OpportunityId.inputValue}");

        DCBindingContainer bindings = this.getDCBindingsContainer();
        DCIteratorBinding itorBinding = bindings.findIteratorBinding(Iterator);
        ViewObject object = itorBinding.getViewObject();
        object.setNamedWhereClauseParam("b_award_hdr_id", getAwardHeaderId());
        object.executeQuery();

        RowSetIterator iterator = object.createRowSetIterator(null);
        int i = sheetrownum;
        int logHeaderOnce = 0;
        logger.log(Level.INFO, getClass(), "genericLogic", "Loop started for generic Awards download");

        org.apache.poi.ss.usermodel.Row headerRow = sheet.getRow(headerrownum);
        String voAttribute = null;
        try {
            while (iterator.hasNext()) { //Row iteration

                Row currentRow = iterator.next();
                XSSFRow sheetRow = sheet.createRow(i);
                i++;
                int index = 0; //Column
                for (index = 0; index < currentRow.getAttributeCount(); index++) {
                    Cell newCell = headerRow.getCell(index);
                    if (newCell != null) {
                        XSSFCell currentCell = sheetRow.createCell(index);
                        currentCell.setCellStyle(cellStyleForBorder);

                        if (newCell.getCellTypeEnum() != CellType.BLANK) {
                            String newHeader = newCell.getStringCellValue();
                            if (logHeaderOnce == 0) {
                                logger.log(Level.INFO, getClass(), "genericLogic",
                                           "Header value of new cell" + newHeader);
                            }
                            String destnFieldName = null;
                            if (downloadAwardsReviewMapping.get(newHeader) != null) {
                                destnFieldName = downloadAwardsReviewMapping.get(newHeader)[0];
                            }

                            String dataFormat = "";
                            if (destnFieldName != null) {
                                voAttribute = awardsDownloadVoAttrMapping.get(destnFieldName);
                                dataFormat = (String) downloadAwardsReviewMapping.get(newHeader)[1];
                            } else {
                                //If any column mappings r not found in mappings table
                                voAttribute = null;
                                dataFormat = "";
                            }
                            if (null != currentRow && voAttribute != null &&
                                currentRow.getAttribute(voAttribute) != null &&
                                "" != currentRow.getAttribute(voAttribute).toString()) {
                                switch (dataFormat) {
                                case "TEXT": 
                                    currentCell.setCellType(CellType.STRING);
                                    currentCell.setCellValue(currentRow.getAttribute(voAttribute).toString());
                                    
                                break;
                                case "COST": //This is for celltype with numeric value with decimal & comma
                                    currentCell.setCellType(CellType.NUMERIC);
                                    if (Double.valueOf(currentRow.getAttribute(voAttribute).toString()) < 1.0) {
                                        currentCell.setCellStyle(cellStyleFor0);
                                        currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(voAttribute)
                                                                                .toString()));
                                    } else {
                                        currentCell.setCellStyle(cellStyleCost);
                                        currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(voAttribute)
                                                                                .toString()));
                                    }
                                    break;
                                case "NUMBER": //This is for celltype with numeric value
                                if(voAttribute.equalsIgnoreCase("MARGIN")){
                                    currentCell.setCellStyle(cellStylePercentage);
                                    }
                                    currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(voAttribute)
                                                                            .toString()));
                                    break;
                                default:
                                    currentCell.setCellValue(currentRow.getAttribute(voAttribute).toString());
                                }
                                logger.log(Level.INFO, getClass(), "genericLogic",newHeader+" = "+
                                                                       "VO Attribute= " + currentRow.getAttribute(voAttribute).toString());
                                
                            } else /*if (voAttribute == null){
                                float awardedPrice = 0;
                                float awardedEau = 0;
                                float cost = 0;
                                if (currentRow.getAttribute("Price") != null) {
                                    String x = currentRow.getAttribute("Price").toString();
                                    awardedPrice = Float.parseFloat(currentRow.getAttribute("Price").toString());
                                } else
                                    awardedPrice = 0;
                                if (currentRow.getAttribute("Eau") != null)
                                    awardedEau = Float.parseFloat(currentRow.getAttribute("Eau").toString());
                                else
                                    awardedEau = 0;
                                if (currentRow.getAttribute("CostUsedForQuote") != null)
                                    cost = Float.parseFloat(currentRow.getAttribute("CostUsedForQuote").toString());
                                else
                                    cost = 0;
                                float extendedSale = awardedPrice * awardedEau;
                                float extendedCost = cost * awardedEau;
                                float margin = 0;
                                if (extendedCost == 0)
                                    margin = 0;
                                else
                                    margin = (extendedSale - extendedCost) / extendedSale;
                                if (newHeader.equalsIgnoreCase("Extended Sales"))
                                    currentCell.setCellValue(extendedSale);
                                if (newHeader.equalsIgnoreCase("Extended  Cost"))
                                    currentCell.setCellValue(extendedCost);
                                if (newHeader.equalsIgnoreCase("Margin"))
                                    currentCell.setCellValue(margin);
                                }
                                else*/
                                    currentCell.setCellValue("");
                            }                            
                    }
                }
                logHeaderOnce++;
                }
                
            
        } catch (Exception nfe) {

            nfe.printStackTrace();
        }
        return workbook;
    }

    public void downloadAwardsReview(FacesContext facesContext, OutputStream outputStream) {
        try {
            
            generateAwardsReviewDownloadTemplate(ViewConstants.AWARDS_REVIEW_DOWNLOAD_KEY);
            facesContext.getCurrentInstance();
            String fileName = ViewConstants.AWARDS_REVIEW_TEMPLATE;
            String filePath = ViewConstants.FILE_PATH + fileName;
            FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);
            logger.log(Level.INFO, getClass(), "downloadAwardsReview", "Excel read");

            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFWorkbook filledWorkbook = genericLogic(workbook, "AwardsDownloadVO1Iterator", 1, 0);
            XSSFSheet sheet = filledWorkbook.getSheetAt(2);
            XSSFRow topRow = sheet.getRow(0);
            int lovCol = 0;
            for (Cell c : topRow) {
                if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                    String text = c.getStringCellValue();
                    if ("Award Tags".equalsIgnoreCase(text)) {
                        lovCol = c.getColumnIndex();
                        break;
                    }
                }
            }
            filledWorkbook = addExcelLOV(filledWorkbook, 1, lovCol);

            try {
                logger.log(Level.INFO, getClass(), "downloadAwardsReview", "Writing workbook");

                filledWorkbook.write(outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (Exception e) {
                logger.log(Level.SEVERE, getClass(), "downloadAwardsReview", e.getMessage());
                CustomErrorHandler.processError("CCT", "downloadAwardsReview", "CCT_SYS_EX41", e);
            }

        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "downloadAwardsReview", e.getMessage());
            CustomErrorHandler.processError("CCT", "downloadAwardsReview", "CCT_SYS_EX41", e);
        }
    }

    public XSSFWorkbook addExcelLOV(XSSFWorkbook workbook, int rowNum, int colNum) {

        String[] queues = getAwardTagsLOV(ViewConstants.AWARD_TAG_LOV);

        XSSFSheet sheet = workbook.getSheetAt(2);
        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sheet);
        XSSFDataValidationConstraint dvConstraint =
            (XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(queues);
        CellRangeAddressList addressList = new CellRangeAddressList(rowNum, sheet.getLastRowNum()*100, colNum, colNum);
        XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList);
        validation.setShowErrorBox(true);
        validation.setErrorStyle(XSSFDataValidation.ErrorStyle.STOP);
        validation.createErrorBox("Criteria Mismatch", ViewConstants.LOV_ERROR_MESSAGE);
        sheet.addValidationData(validation);
        validation.setSuppressDropDownArrow(false);


        return workbook;
    }

    public String[] getAwardTagsLOV(String mappingkey) {
        OperationBinding getAwardTagsLOV = ADFUtils.getBindingContainer().getOperationBinding("getAwardTagsLOV");
        getAwardTagsLOV.getParamsMap().put("pMappingKey", mappingkey);
        String[] tagLOV = (String[]) getAwardTagsLOV.execute();
        return tagLOV;
    }

    public void setUploadButton(RichButton uploadButton) {
        this.uploadButton = uploadButton;
    }

    public RichButton getUploadButton() {
        return uploadButton;
    }

    public void setAwardHeaderId(String awardHeaderId) {
        this.awardHeaderId = awardHeaderId;
    }

	@SuppressWarnings("unchecked")
    public String getAwardHeaderId() {
        Object opptyid1 = JSFUtils.resolveExpression("#{bindings.OpportunityId.inputValue}");
       
        OperationBinding awardsBinding =
            ADFUtils.getBindingContainer().getOperationBinding("getAwardHeaderIdByOpptyId");
     
        awardsBinding.getParamsMap().put("opptyId", opptyid1);
        BigDecimal awardHeaderIds = (BigDecimal) awardsBinding.execute();
        String awardHeaderId = null;
        if(awardHeaderIds != null){
        awardHeaderId = awardHeaderIds.toString();
        }
        return awardHeaderId;
    }
        
    private HashMap validateBeaconHeaders(InputStream inputStream, String sheetName, int startingHeaderRow){
            HashMap hashMap = new HashMap();        
            List<String> columnHeaders = new ArrayList<String>();
            List<String> missingHeaders = new ArrayList<String>();
            List<String>totalAwardHeaders= new ArrayList<String>();
            List<String> AddedInvalidHeaders = new ArrayList<String>();
            for(String header : ViewConstants.totalAwardBeaconHeaders){
                totalAwardHeaders.add(header);
            }
            System.out.println("totalAwardHeaders length--"+totalAwardHeaders.size() + "-- \n totalAwardHeaders--"+totalAwardHeaders);
            String missingHeaderMsg = "It is missing the required headers: ";
            boolean isInvalidHeader= false;
           String invalidHeaderMsg=" There are invalid headers of: ";
            String duplicateHeaderMsg = "There are duplicate headers of: ";
            boolean isDuplicateHeader = false;
            boolean isMissingHeader = false;
            String uploadFailureReason = "";
            
            try {
                System.out.println("before creating worksheet");
            XSSFWorkbook workBook = new XSSFWorkbook(inputStream);
            System.out.println("created an excel object");
            
            Sheet sheet = workBook.getSheet(sheetName);
            org.apache.poi.ss.usermodel.Row headerRow = sheet.getRow(startingHeaderRow);
            Iterator<Cell> cellIterator = headerRow.cellIterator();
            
            missingHeaders.add(ViewConstants.missingAwardBeaconHeaders[0]);
//            missingHeaders.add(ViewConstants.missingAwardBeaconHeaders[1]);
//            missingHeaders.add(ViewConstants.missingAwardBeaconHeaders[2]);
//            missingHeaders.add(ViewConstants.missingAwardBeaconHeaders[3]);
//            missingHeaders.add(ViewConstants.missingAwardBeaconHeaders[4]);
//            missingHeaders.add(ViewConstants.missingAwardBeaconHeaders[5]);
//            missingHeaders.add(ViewConstants.missingAwardBeaconHeaders[6]);
            
            while(cellIterator.hasNext()){
                Cell cell = cellIterator.next();
                String cellValue = cell.getStringCellValue();
                System.out.println("Award header column--"+cellValue);
                if(null!= cellValue && !cellValue.trim().isEmpty()){
                    
                    if(missingHeaders.contains(cellValue.trim().replaceAll("\\s{2,}", " "))){
                       missingHeaders.remove(cellValue);
                    }
                    
                    if(!totalAwardHeaders.contains(cellValue.trim().replaceAll("\\s{2,}", " "))){
                        AddedInvalidHeaders.add(cellValue);
                    }
                    
                    columnHeaders.add(cellValue.trim());
 
                }
                            }
                /**        Checking for invalid headers added * */
                System.out.println("AddedInvalidHeaders--"+AddedInvalidHeaders);
                        if(AddedInvalidHeaders != null && AddedInvalidHeaders.size() > 0){
                            isInvalidHeader = true;
                            StringBuffer invalidHeadersSB = new StringBuffer();
                            
                            for(String invalidHeader : AddedInvalidHeaders){
                                invalidHeadersSB.append(invalidHeader + ", ");
                            }
                            
                            invalidHeaderMsg += removeLastComma(invalidHeadersSB.toString());
                        }
            
            
    //        Checking for missing headers
                System.out.println("missingHeaders--"+missingHeaders);
            if(missingHeaders != null && missingHeaders.size() > 0){
                isMissingHeader = true;
                StringBuffer missingHeadersSB = new StringBuffer();
                
                for(String missingHeader : missingHeaders){
                    missingHeadersSB.append(missingHeader + ", ");
                }
                
                missingHeaderMsg += removeLastComma(missingHeadersSB.toString());
            }

    //      Checking for duplicate headers
            if(columnHeaders.size() > 0){
                Set<String> duplicateSet = findDuplicates(columnHeaders);
                
                if(duplicateSet != null && duplicateSet.size() != 0){
                    isDuplicateHeader = true;
                    StringBuffer duplicateHeaderSB = new StringBuffer();
                    for(String str : duplicateSet){
                        duplicateHeaderSB.append(str + ", ");                    
                    }
                    
                    duplicateHeaderMsg += removeLastComma(duplicateHeaderSB.toString());
                }
                
                if(isDuplicateHeader && isMissingHeader && isInvalidHeader){
                    uploadFailureReason = duplicateHeaderMsg + "\n" + missingHeaderMsg + "\n"+ invalidHeaderMsg;
                }else if(isDuplicateHeader){
                    uploadFailureReason = duplicateHeaderMsg;
                }else if(isMissingHeader){
                    uploadFailureReason = missingHeaderMsg;
                }else if(isInvalidHeader){
                    uploadFailureReason =invalidHeaderMsg ;
                }
                if(isDuplicateHeader || isMissingHeader || isInvalidHeader){
                    hashMap.put("uploadFailureReason", uploadFailureReason);
                    hashMap.put("uploadStatus", "unsuccessful");
                }else{
                    hashMap.put("uploadStatus", "successful");
                    hashMap.put("uploadFailureReason", "");
                }
            }
            } catch (IOException e) {
                hashMap.put("uploadStatus", "unsuccessful");
                hashMap.put("uploadFailureReason", e.getMessage()); 
                e.printStackTrace();
            }
            finally{
                try{
                    inputStream.close();
                }
                catch(IOException ex){
                    ex.printStackTrace();
                }
            }
            return hashMap;
        }
    
    
    private String removeLastComma(String inputStr){
            String returnStr = "";
            int length = inputStr.length();
            
            if(length > 0 && inputStr.charAt(length -2) == ','){
                returnStr = inputStr.substring(0, length-2);
            }else{
                returnStr = inputStr;
            }
            return returnStr;
        }




    private Set<String> findDuplicates(List<String> listData){
            Set<String> duplicateSet = new HashSet<String>();
            Set<String> set1 = new HashSet<String>();
            
            for(String data : listData){
                if(!set1.add(data)){
                    duplicateSet.add(data);
                }
            }
            
            return duplicateSet;
        }


}
