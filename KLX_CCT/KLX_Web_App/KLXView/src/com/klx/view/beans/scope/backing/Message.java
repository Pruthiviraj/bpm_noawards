package com.klx.view.beans.scope.backing;

import oracle.adf.view.rich.component.rich.RichPopup;

public class Message {
    private RichPopup msgPopUp;

    public Message() {
        super();
    }
    
    private String title;
    private String message;


    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMsgPopUp(RichPopup msgPopUp) {
        this.msgPopUp = msgPopUp;
    }

    public RichPopup getMsgPopUp() {
        return msgPopUp;
    }
    
    public void showMessagePopUp(String title, String message){
        resetValues();
        this.title=title;
        this.message=message;  
        showPopUp();
    }
    
    public void resetValues(){
        this.title="";
        this.message="";
    }
    
    public void showPopUp(){
        if(null!=msgPopUp){
            RichPopup.PopupHints hints=new RichPopup.PopupHints();
            msgPopUp.show(hints);
        }
    }
    
    public void closePopUp(){
        if(null!=msgPopUp){            
            msgPopUp.hide();
        }
    }
    
}
