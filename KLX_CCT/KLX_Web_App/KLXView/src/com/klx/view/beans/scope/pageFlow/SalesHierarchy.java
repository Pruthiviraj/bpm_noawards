package com.klx.view.beans.scope.pageFlow;

import com.klx.view.beans.utils.ADFUtils;

import java.math.BigDecimal;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.event.PopupCanceledEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;

public class SalesHierarchy {
    private String commitSuccesMessage = "";
    private RichPopup popupBinding;
    private RichInputText teamNo;
    private RichPopup insertPopupBinding;
    private RichPopup updatePopupBinding;

    public SalesHierarchy() {
    }

    public void setCommitSuccesMessage(String commitSuccesMessage) {
        this.commitSuccesMessage = commitSuccesMessage;
    }

    public String getCommitSuccesMessage() {
        return commitSuccesMessage;
    }

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();

    }

    public String saveSalesHierarchyDetails() {
        // Add event code here...
        RichInputText value = getTeamNo();
        String teamNo =value.getValue().toString();

        DCBindingContainer binding = this.getDCBindingsContainer();
        DCIteratorBinding cctSalesHierarchyNewVOIterator =
            binding.findIteratorBinding("CctSalesHierarchyNewVOIterator");
        ViewObject cctSalesHierarchyNewVO = cctSalesHierarchyNewVOIterator.getViewObject();
        cctSalesHierarchyNewVO.executeQuery();
        
        RowSetIterator salesHierarchyIterator = cctSalesHierarchyNewVO.createRowSetIterator(null);
        String teamno = "";
        boolean flag = false;
        int res = 0;
        while (salesHierarchyIterator.hasNext()) {
            Row salesHierarchyRow = salesHierarchyIterator.next();
            teamno = salesHierarchyRow.getAttribute("TeamNo").toString();
            if(teamNo.equalsIgnoreCase(teamno)) {
                flag = true;
                break;
            }
        }
        if (flag == true) {
            setCommitSuccesMessage("Team Number already exists");
            BindingContainer bindings = getBindings();
            OperationBinding operationBinding = bindings.getOperationBinding("Rollback");
            Object result = operationBinding.execute();

        } else {
            BindingContainer bindings = getBindings();
            OperationBinding operationBinding = bindings.getOperationBinding("Commit");
            Object result = operationBinding.execute();
            if (!operationBinding.getErrors().isEmpty()) {
                return null;
            }
            setCommitSuccesMessage("Sales Team Details Saved Successfully");
        }
        DCIteratorBinding cctSalesHierarchyVOIterator = binding.findIteratorBinding("CctSalesHierarchyVOIterator");
        ViewObject cctSalesHierarchyVO = cctSalesHierarchyVOIterator.getViewObject();
        cctSalesHierarchyVO.executeQuery();
        getInsertPopupBinding().hide();

        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getPopupBinding().show(hints);
        return null;
    }

    public DCBindingContainer getDCBindingsContainer() {
        DCBindingContainer bindingsContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        return bindingsContainer;
    }

    public void setPopupBinding(RichPopup popupBinding) {
        this.popupBinding = popupBinding;
    }

    public RichPopup getPopupBinding() {
        return popupBinding;
    }

    public void homeLinkACL(ActionEvent actionEvent) {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
    }


    public void setTeamNo(RichInputText teamNo) {
        this.teamNo = teamNo;
    }

    public RichInputText getTeamNo() {
        return teamNo;
    }

    public String insertNewRow() {
        // Add event code here...
        BindingContext bindingContext = BindingContext.getCurrent();
        BindingContainer bindingContainer = bindingContext.getCurrentBindingsEntry();
        OperationBinding binding = bindingContainer.getOperationBinding("createSalesHierarchyRow");
        binding.execute();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getInsertPopupBinding().show(hints);

        return null;
    }

    public void setInsertPopupBinding(RichPopup insertPopupBinding) {
        this.insertPopupBinding = insertPopupBinding;
    }

    public RichPopup getInsertPopupBinding() {
        return insertPopupBinding;
    }

    public void setUpdatePopupBinding(RichPopup updatePopupBinding) {
        this.updatePopupBinding = updatePopupBinding;
    }

    public RichPopup getUpdatePopupBinding() {
        return updatePopupBinding;
    }

    public String updateRow() {
        // Add event code here...
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getUpdatePopupBinding().show(hints);
        return null;
    }

    public String updateSalesHierarchy() {
        // Add event code here...
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        setCommitSuccesMessage("Sales Team Details Updated Successfully");
        getUpdatePopupBinding().hide();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getPopupBinding().show(hints);
        return null;
    }

    public String rollbackUpdatedRow() {
        // Add event code here...
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
        getUpdatePopupBinding().hide();
        return null;
    }

    public String rollbackInsertedRow() {
        // Add event code here...
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
        getInsertPopupBinding().hide();
        return null;
    }

    public void RollbackRowOnPopupClose(PopupCanceledEvent popupCanceledEvent) {
        // Add event code here...
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
        
        DCBindingContainer binding = this.getDCBindingsContainer();
        DCIteratorBinding cctSalesHierarchyVOIterator = binding.findIteratorBinding("CctSalesHierarchyVOIterator");
        ViewObject cctSalesHierarchyVO = cctSalesHierarchyVOIterator.getViewObject();
        cctSalesHierarchyVO.executeQuery();
       
    }
}
