package com.klx.view.beans.scope.pageFlow;

import com.klx.dms.model.pojo.Document;

import com.klx.view.beans.constants.ViewConstants;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import com.klx.dms.model.viewObjects.FolderVORowImpl;

import java.util.Date;
import com.klx.view.beans.utils.DateUtil;
import com.klx.common.cache.ParamCache;

import com.klx.common.exception.SystemException;
import com.klx.common.logger.KLXLogger;
import com.klx.dms.model.constants.ModelConstants;
import com.klx.view.beans.constants.DMSViewConstants;

import com.klx.dms.model.pojo.Opportunity;
import com.klx.view.beans.utils.ADFUtils;

import com.klx.view.beans.utils.JSFUtils;

import com.klx.view.error.CustomErrorHandler;

import java.math.BigDecimal;

import java.util.Collection;
import java.util.HashMap;

import java.util.Iterator;

import java.util.Map;

import java.util.ResourceBundle;
import java.util.logging.Level;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.servlet.http.HttpServletRequest;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputFile;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.input.RichSelectOneRadio;

import oracle.adf.view.rich.context.AdfFacesContext;



import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.javatools.resourcebundle.BundleFactory;

import oracle.jbo.ViewObject;
import oracle.jbo.uicli.binding.JUCtrlListBinding;

import org.apache.myfaces.trinidad.model.UploadedFile;
import oracle.jbo.domain.Number;


public class UploadDocumentBean {    
    
    private RichInputFile fileBinding;
    private RichPopup popUpBinding;
    private RichInputText fileTitleBinding;    
    private Number folderId;
    private String uploadStatusMessage="File Uploaded Successfully.";
    private RichPopup statusPopUpBinding;
    private RichSelectOneChoice selectFolder1;
    private static KLXLogger logger = KLXLogger.getLogger();    
    
    public UploadDocumentBean() {
        super();
    }

    public void setFileBinding(RichInputFile fileBinding) {
        this.fileBinding = fileBinding;
    }

    public RichInputFile getFileBinding() {
        return fileBinding;
    }

    public void setPopUpBinding(RichPopup popUpBinding) {
        this.popUpBinding = popUpBinding;
    }

    public RichPopup getPopUpBinding() {
        return popUpBinding;
    }

    public void setUploadStatusMessage(String uploadStatusMessage) {
        this.uploadStatusMessage = uploadStatusMessage;
    }

    public String getUploadStatusMessage() {
        return uploadStatusMessage;
    }

    public void setStatusPopUpBinding(RichPopup statusPopUpBinding) {
        this.statusPopUpBinding = statusPopUpBinding;
    }

    public RichPopup getStatusPopUpBinding() {
        return statusPopUpBinding;
    }
    
    
    
    public void openUploadDocumentPopUp(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(), "openUploadDocumentPopUp",  "Entering openUploadDocumentPopUp method");
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getPopUpBinding().show(hints);
        String tabClicked = ADFUtils.getExpressionValue("pageFlowScope.documentMgmtBean.folderId");
        JUCtrlListBinding listBinding =
        (JUCtrlListBinding)ADFUtils.getDCBindingContainer().get(DMSViewConstants.SELECTED_FOLDER2);        
        logger.log(Level.INFO, getClass(), "openUploadDocumentPopUp", "Tab clicked IntegerValue -"+(Integer.parseInt(tabClicked)-1)+"-");
        listBinding.setInputValue(""+ (Integer.parseInt(tabClicked)-1));
        logger.log(Level.INFO, getClass(), "openUploadDocumentPopUp",  "Exiting openUploadDocumentPopUp method");
    }

    public void uListner(ActionEvent actionEvent) {
    
        getPopUpBinding().hide();
    }

    public void setFileTitleBinding(RichInputText fileTitleBinding) {        
        this.fileTitleBinding = fileTitleBinding;
    }

    public RichInputText getFileTitleBinding() {
        return fileTitleBinding;
    }

    public void uploadDocumentVCL(ValueChangeEvent valueChangeEvent) {
      
    }
    
    public void uploadDocument(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(), "uploadDocument",  "Entering uploadDocument method");
      
        HashMap uploadStatus = new HashMap();
        try {
                UploadedFile file = (UploadedFile) getFileBinding().getValue();
                               
                boolean isValidUploadData = validateUploadFormData();
                logger.log(Level.INFO, getClass(), "uploadDocument",  "isValidUploadData-"+isValidUploadData+"-");
                if(isValidUploadData){
               
                Opportunity opportunity = getOpportunityDetails();
                Document document = getDocumentDetails();
                if(null != document.getFileContentByteArray() && document.getFileContentByteArray().length > 0){
                    BindingContext bindingContext = BindingContext.getCurrent();
                    BindingContainer bindingContainer = bindingContext.getCurrentBindingsEntry();
                    OperationBinding binding = bindingContainer.getOperationBinding(DMSViewConstants.UPLOAD_DOCUMENT);
                    binding.getParamsMap().put(DMSViewConstants.OPPORTUNITY, opportunity);  
                    binding.getParamsMap().put(DMSViewConstants.DOCUMENT, document);                
                    uploadStatus = (HashMap)binding.execute(); 
                    
                    
                    if (null == uploadStatus) {
                        uploadStatus = (HashMap)binding.execute(); 
                    }
                    logger.log(Level.INFO, getClass(), "uploadDocument","Upload Status  -" +uploadStatus+"-");
                    if(null != uploadStatus.get("documentUploaded") && uploadStatus.get("documentUploaded").toString().equalsIgnoreCase("success")) {                                        
                        ADFUtils.setExpressionValue("pageFlowScope.documentMgmtBean.opportunityId",uploadStatus.get("opportunityId").toString());
                        RichPopup.PopupHints hints = new RichPopup.PopupHints();
                        getStatusPopUpBinding().show(hints);
                    
                    } else {
                        JSFUtils.addFacesErrMsgClient(getFileBinding().getClientId(),  getPropertyValue(DMSViewConstants.REQ_NOT_PROC_ERROR));
                    }
                }
                else{
                    getPopUpBinding().hide();
                    CustomErrorHandler.processError(ModelConstants.DMS,ModelConstants.UPLOAD_DOCUMENT,ModelConstants.DMS_SYS_EX07, new SystemException(ModelConstants.DMS_SYS_EX07));                    
                    
                }
            }
            } catch (Exception e) {
                getPopUpBinding().hide();
                getStatusPopUpBinding().hide();
                CustomErrorHandler.processError(ModelConstants.DMS,ModelConstants.UPLOAD_DOCUMENT,ModelConstants.DMS_SYS_EX03,e);
               
            }

        logger.log(Level.INFO, getClass(), "uploadDocument",  "Exiting uploadDocument method");
    }
    
    private boolean validateUploadFormData(){
        logger.log(Level.INFO, getClass(), "validateUploadFormData",  "Entering validateUploadFormData method");
        boolean isValidUploadData = false;
        UploadedFile file = (UploadedFile) getFileBinding().getValue();
        String fileTitle = (String)getFileTitleBinding().getValue();
        logger.log(Level.INFO, getClass(), "validateUploadFormData","FileTitle -" +fileTitle+"-");
        //logger.log(Level.INFO, getClass(), "validateUploadFormData","FileName -" +file.getFilename()+"-");        
        if(validateFolder()){
            if(validateFileTitle(fileTitle)){
                if(validateFile(file)){
                    isValidUploadData = true;      
                }
            }
        }
        logger.log(Level.INFO, getClass(), "validateUploadFormData",  "Exiting validateUploadFormData method");
        return isValidUploadData;
    }
   
    private boolean validateFolder(){
        logger.log(Level.INFO, getClass(), "validateFolder",  "Entering validateFolder method");
        boolean isValidFolder = true;
        logger.log(Level.INFO, getClass(), "validateFolder",  "FolderId -"+folderId+"-");
        logger.log(Level.INFO, getClass(), "validateFolder",  "Entering validateFolder method");
        return isValidFolder;
    }
    
    private boolean validateFileTitle(String fileTitle){
        logger.log(Level.INFO, getClass(), "validateFileTitle",  "Entering validateFileTitle method");
        logger.log(Level.INFO, getClass(), "validateFileTitle","Input parameter fileTitle is -" +fileTitle+"-");
        boolean isValidFileTitle = false;
        if(null != fileTitle && !fileTitle.equalsIgnoreCase("")){
            isValidFileTitle = true;
        }else {
            
            JSFUtils.addFacesErrMsgClient(getFileTitleBinding().getClientId(), getPropertyValue(DMSViewConstants.FILE_TITLE_ERROR));
        }
        
        logger.log(Level.INFO, getClass(), "validateFileTitle","Return parameter isValidFileTitle is:" +isValidFileTitle);
        logger.log(Level.INFO, getClass(), "validateFileTitle",  "Exiting validateFileTitle method");
        return isValidFileTitle;
    }
        
    private boolean validateFile(UploadedFile file){
        logger.log(Level.INFO, getClass(), "validateFile",  "Entering validateFile method");
        logger.log(Level.INFO, getClass(), "validateFile", "Input paramete File is " +file+"-");
        boolean isValidFileType = false;
        if(null != file){
            String fileContentType = file.getContentType();
            HashMap fileTypeMap = ParamCache.fileTypeMap;
            Collection fileContentTypeCol =  fileTypeMap.keySet();
            int i = file.getFilename().length();
            if(i>(DMSViewConstants.FILE_NAME_CHARACTERS)) {
            JSFUtils.addFacesErrMsgClient(getFileBinding().getClientId(),  getPropertyValue(DMSViewConstants.FILE_LENGTH)); 
            }else if(fileContentTypeCol.contains(fileContentType)){
                isValidFileType = true;
            } else {
               
                JSFUtils.addFacesErrMsgClient(getFileBinding().getClientId(),getPropertyValue(DMSViewConstants.FILE_TYPE_ERROR));
            }
        } else {
            JSFUtils.addFacesErrMsgClient(getFileBinding().getClientId(), getPropertyValue(DMSViewConstants.UPLOAD_FILE_ERROR));
        }
        
        
        logger.log(Level.INFO, getClass(), "validateFile",  "Exiting validateFile method");
        return isValidFileType;
    }
    
    private Opportunity getOpportunityDetails()throws Exception{
        logger.log(Level.INFO, getClass(), "getOpportunityDetails",  "Entering getOpportunityDetails method");
        Opportunity opportunity = new Opportunity();
        opportunity.setOscOpportunityId(ADFUtils.getExpressionValue("pageFlowScope.documentMgmtBean.oscOpportunityId"));
        opportunity.setOpportunityNumber(ADFUtils.getExpressionValue("pageFlowScope.documentMgmtBean.opportunityNumber"));
        opportunity.setOpportunityName(ADFUtils.getExpressionValue("pageFlowScope.documentMgmtBean.opportunityName"));        
        opportunity.setOpportunityId(new Number(new BigDecimal(JSFUtils.resolveExpression("#{bindings.OpportunityId1.inputValue}").toString())));
        logger.log(Level.INFO, getClass(), "getOpportunityDetails","OscOppId -" +opportunity.getOscOpportunityId()+"-");
        logger.log(Level.INFO, getClass(), "getOpportunityDetails","OppNumber -"+opportunity.getOpportunityNumber()+"-");
        logger.log(Level.INFO, getClass(), "getOpportunityDetails","OppName -"+opportunity.getOpportunityName()+"-");
        logger.log(Level.INFO, getClass(), "getOpportunityDetails",  "Exiting getOpportunityDetails method");
        return opportunity;
    }
    
    private Object resolveExpression(String expression) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Application app = facesContext.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        ValueExpression valueExp = elFactory.createValueExpression(elContext, expression, Object.class);
        return valueExp.getValue(elContext);
    }
    
    private Document getDocumentDetails() throws Exception{
        logger.log(Level.INFO, getClass(), "getDocumentDetails",  "Entering getDocumentDetails method");
        Document document = new Document();       
       
        JUCtrlListBinding listBinding =
        (JUCtrlListBinding)ADFUtils.getDCBindingContainer().get(DMSViewConstants.SELECTED_FOLDER2);
        FolderVORowImpl folderRow = (FolderVORowImpl)listBinding.getSelectedValue();
        logger.log(Level.INFO, getClass(), "getDocumentDetails",  "Folder Id -" +folderRow.getFolderId()+"-" );   
        setFolderId(folderRow.getFolderId());    
        UploadedFile file = (UploadedFile) getFileBinding().getValue();
        String fileTitle = (String) getFileTitleBinding().getValue();    
        byte[] fileContent = getFileContent(file);
        //byte[] fileContent = new byte[0];        
        document.setFileContentByteArray(fileContent);
        logger.log(Level.INFO, getClass(), "getDocumentDetails","File Name -"  +file.getFilename()+"-");  
        document.setFileName(file.getFilename());
        document.setFileContentType(file.getContentType());
        logger.log(Level.INFO, getClass(), "getDocumentDetails","FileContentType -"  +file.getContentType()+"-");   
        document.setFileType((ParamCache.fileTypeMap.get(file.getContentType())).toString());    
        document.setFolderId(this.getFolderId());
        document.setFileTitle(fileTitle);        
        document.setSourceSystem((String)ADFContext.getCurrent().getSessionScope().get(DMSViewConstants.SOURCE));   
        document.setCreatedBy((String)JSFUtils.resolveExpression("#{sessionScope.loggedInUser.userName}"));
        document.setCreatedDate(DateUtil.formatDateTime(new Date()));
        logger.log(Level.INFO, getClass(), "getDocumentDetails",  "UserName -"+JSFUtils.resolveExpression("#{sessionScope.loggedInUser.userName}")+"-"); 
        logger.log(Level.INFO, getClass(), "getDocumentDetails",  "Exiting getDocumentDetails method");
        return document;
    }
    
    private byte[] getFileContent(UploadedFile file){
        logger.log(Level.INFO, getClass(), "getFileContent",  "Entering getFileContent method");
        int bytesRead = 0;
        byte[] buffer = new byte[8192];    
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            if (null!=file && null!=file.getInputStream()) {
                InputStream input = file.getInputStream();

                while ((bytesRead = input.read(buffer, 0, 4096)) != -1) {
                    output.write(buffer, 0, bytesRead);
                }

                output.flush();
                input.close();
            }
        } catch (FileNotFoundException e) {
            CustomErrorHandler.processError("DMS","UploadDocument",ModelConstants.DMS_SYS_EX06,e);            
        } catch (IOException e) {
            CustomErrorHandler.processError("DMS","UploadDocument",ModelConstants.DMS_SYS_EX06,e);            
        } catch (Exception e){
            CustomErrorHandler.processError("DMS","UploadDocument",ModelConstants.DMS_SYS_EX06,e);           
        }
        logger.log(Level.INFO, getClass(), "getFileContent",  "Exiting getFileContent method");
        return output.toByteArray();
        
    }
    
    
    public void refreshDocListByOppIdFolderID() {
        logger.log(Level.INFO, getClass(), "refreshDocListByOppIdFolderID",  "Entering refreshDocListByOppIdFolderID method");
       
        OperationBinding documentBinding = ADFUtils.findOperationBinding(DMSViewConstants.GET_DOCUMENTS);
       logger.log(Level.INFO, getClass(), "refreshDocListByOppIdFolderID","refreshDocListByOppIdFolderID--2- " +documentBinding);
        Map<String,Object> map = documentBinding.getParamsMap();
        
        map.put(DMSViewConstants.OPPORTUNITY_ID, ADFUtils.getExpressionValue("pageFlowScope.documentMgmtBean.opportunityId"));
        logger.log(Level.INFO, getClass(), "refreshDocListByOppIdFolderID","Folder Id -" +this.getFolderId()+"-");
        map.put(DMSViewConstants.FOLDER_ID, this.getFolderId());
        logger.log(Level.INFO, getClass(), "refreshDocListByOppIdFolderID","refreshDocListByOppIdFolderID -"+ map.values()+"-");
        documentBinding.execute();
        logger.log(Level.INFO, getClass(), "refreshDocListByOppIdFolderID",  "Exiting refreshDocListByOppIdFolderID method");
       
    }
    public String getPropertyValue(String propertyKey){
        logger.log(Level.INFO, getClass(),"getPropertyValue", "Entering getPropertyValue method");
        logger.log(Level.INFO, getClass(),"getPropertyValue","Input parameter Property Key -"  +propertyKey+"-");
        String propertyValue =null;
             String bundle_name= DMSViewConstants.DMSUIMESSAGES_PROPERTIES_PATH;
            ResourceBundle resourceBundle =   BundleFactory.getBundle(bundle_name);
            
            if(resourceBundle != null)
            {
            propertyValue = resourceBundle.getString(propertyKey);
            
        }
        logger.log(Level.INFO, getClass(),"getPropertyValue", "Return parameter Property Value is -"+propertyValue+"-");
        logger.log(Level.INFO, getClass(),"getPropertyValue", "Exiting getPropertyValue method");
        return propertyValue;
    }   
    public void okButtonACL(ActionEvent actionEvent) {
       logger.log(Level.INFO, getClass(), "okButtonACL",  "Entering okButtonACL method");
       try{ 
       getStatusPopUpBinding().hide();
       getPopUpBinding().hide();  
       getFileBinding().setValue(null);
       getFileTitleBinding().setValue(null);
       String tabClicked = ADFUtils.getExpressionValue("pageFlowScope.documentMgmtBean.folderId");	 
       if(tabClicked.contentEquals(getFolderId().toString())) {
            logger.log(Level.INFO, getClass(), "okButtonACL", "tabClicked--"+ tabClicked+"-");
            refreshDocListByOppIdFolderID();
       }   
       }catch (Exception e){
            CustomErrorHandler.processError("DMS","UploadDocument",ModelConstants.DMS_SYS_EX03,e);           
        }
           
        logger.log(Level.INFO, getClass(), "okButtonACL",  "Exiting okButtonACL method");
    }
    
    public void cancelListner(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(), "cancelListner",  "Entering cancelListner method");
        getFileBinding().setValue(null);
        getFileTitleBinding().setValue(null);
        getPopUpBinding().hide();
        logger.log(Level.INFO, getClass(), "cancelListner",  "Exiting cancelListner method");
    }

    public void setSelectFolder1(RichSelectOneChoice selectFolder1) {
        this.selectFolder1 = selectFolder1;
    }

    public RichSelectOneChoice getSelectFolder1() {
        return selectFolder1;
    }

    public void setFolderId(Number folderId) {
        this.folderId = folderId;
    }

    public Number getFolderId() {
        return folderId;
    }
}
