package com.klx.view.beans.scope.pageFlow;

import com.klx.cct.ad.user.v1.Roles;
import com.klx.cct.ad.user.v1.User;
import com.klx.cct.task.v1.TaskT;
import com.klx.common.logger.KLXLogger;
import com.klx.view.beans.constants.ViewConstants;
import com.klx.view.beans.utils.ADFUtils;
import com.klx.view.beans.utils.JSFUtils;
import com.klx.view.error.CustomErrorHandler;

import java.io.OutputStream;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.klx.common.access.UserActionAccessManagement;

import com.klx.common.cache.ParamCache;
import com.klx.model.impl.ActiveDirectoryManagerImpl;

import com.klx.services.entities.Users;

import com.klx.view.beans.constants.DMSViewConstants;

import java.util.List;

import javax.faces.event.ValueChangeEvent;


import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ApprovalBean {
    private RichPopup triggerPopUp;
    private String comments = null;
    private RichPopup approvalPopUp;
    private RichPopup aprroveRejectPopUp;
    private static KLXLogger logger = KLXLogger.getLogger();
    private RichPopup triggerApprovalPopUp;
    private String listofApprovers = null;
    private boolean flag = false;
    private RichTable tableBind;
    private boolean approvalEnabled;
    private boolean rejectApprovalEnabled;
    private boolean triggerApprovalEnabled;
    private boolean cancelInFlightApprovalEnabled;
    private boolean passApprovalEnabled;
    private boolean submitSales = false;
    private RichPopup editApprovalPopUp;
    private RichInputText salesLvlOneBind;
    private RichInputText accntVicePresBind;
    private RichInputText salesLvlTwoBind;
    private RichInputText gvAndGmBind;
    private RichInputText financeVicePresBind;
    private RichInputText operationVicePresBind;
    private RichInputText chiefFinanceOffBind;
    private RichInputText supplyChainVPBind;
    private RichPopup errorPopUpBid;
    private RichInputText srContDirecBind;

    public ApprovalBean() {
        userActionsAccess();
    }

    public void userActionsAccess() {
        HashMap actions = new HashMap();
        actions.put(ViewConstants.APPROVE_ACTION, false);
        actions.put(ViewConstants.REJECT_ACTION, false);
        actions.put(ViewConstants.TRIGGER_APPROVAL_ACTION, false);
        actions.put(ViewConstants.CANCEL_INFLIGHT_APPROVAL_ACTION, false);
        actions.put(ViewConstants.PASS_APPROVAL_ACTION, false);
		actions.put(ViewConstants.SUBMIT_SALES, false);
		HashMap userAcess = new HashMap();
        userAcess = UserActionAccessManagement.getUserAccess(ADFContext.getCurrent()
                                                                               .getSecurityContext()
                                                                               .getUserRoles(), actions);
		getApprovalAccess(userAcess, approvalEnabled, rejectApprovalEnabled);
        //approvalEnabled = ((Boolean) userAcess.get(ViewConstants.APPROVE_ACTION)).booleanValue();
        //rejectApprovalEnabled = ((Boolean) userAcess.get(ViewConstants.REJECT_ACTION)).booleanValue();
        triggerApprovalEnabled = ((Boolean) userAcess.get(ViewConstants.TRIGGER_APPROVAL_ACTION)).booleanValue();
        cancelInFlightApprovalEnabled =
            ((Boolean) userAcess.get(ViewConstants.CANCEL_INFLIGHT_APPROVAL_ACTION)).booleanValue();
        passApprovalEnabled = ((Boolean) userAcess.get(ViewConstants.PASS_APPROVAL_ACTION)).booleanValue();

        //HashMap action = new HashMap();
        
        /*HashMap userAcess1 = UserActionAccessManagement.getUserAccess(ADFContext.getCurrent()
                                                                                .getSecurityContext()
                                                                                .getUserRoles(), action);*/
        submitSales = ((Boolean) userAcess.get(ViewConstants.SUBMIT_SALES)).booleanValue();


    }


    private String triggerStatusMessage;
    private String errorPopUpMessage;

    public void triggerApproval(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(), "triggerApproval", "Entering triggerApproval");
        DCBindingContainer bindingContainer = ADFUtils.getDCBindingContainer();
        DCIteratorBinding opportunityVO1Iterator = bindingContainer.findIteratorBinding("OpportunityVO1Iterator");
        String opportunityNumber = null, opportunityId = null, createdBy = null;
        Row currentRow = opportunityVO1Iterator.getCurrentRow();
        if (ADFContext.getCurrent()
                      .getSessionScope()
                      .get("opportunityNumber") == null) {
            if (null != currentRow)
                opportunityNumber = (String) currentRow.getAttribute("OpportunityNumber");
        } else
            opportunityNumber = ADFContext.getCurrent()
                                          .getSessionScope()
                                          .get("opportunityNumber")
                                          .toString();
        OperationBinding binding = (OperationBinding) ADFUtils.findOperationBinding("getApprovalUsers");
        HashMap hashMap = new HashMap();
        hashMap.put("opportunityNumber", opportunityNumber); //66997
        hashMap.put("snapShotId", null != getSnapShotID() ? getSnapShotID().intValue() : null); //41
        hashMap.put("flag", isFlag());
        binding.getParamsMap().put("hashMap", hashMap);
        logger.log(Level.INFO, getClass(), "Parrameters passing to getApprovalUsers method  -- ",
                   getHashMapString(hashMap));
        ArrayList<String> userList = (ArrayList<String>) binding.execute();
        logger.log(Level.INFO, getClass(), "triggerApproval",
                   "After executing getApprovalUsers , result --" + userList + "--");
        if (null != userList && userList.size() > 0) {
            StringBuilder strb = new StringBuilder();
            strb.append("List of Approvers : <br>");
            for (String listUsers : userList) {
                strb.append("  " + listUsers + "<br>");
            }
            listofApprovers = strb.toString();
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            getTriggerApprovalPopUp().show(hints);
        } else {
            listofApprovers = "No Approvers";
            setErrorPopUpMessage(ViewConstants.NO_APPROVALS);
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            getErrorPopUpBid().show(hints);
        }
        setComments(null);
        
        logger.log(Level.INFO, getClass(), "triggerApproval", "Exiting triggerApproval");
    }

    public void setTriggerStatusMessage(String triggerStatusMessage) {
        this.triggerStatusMessage = triggerStatusMessage;
    }

    public String getTriggerStatusMessage() {
        return triggerStatusMessage;
    }

    public void setTriggerPopUp(RichPopup triggerPopUp) {
        this.triggerPopUp = triggerPopUp;
    }

    public RichPopup getTriggerPopUp() {
        return triggerPopUp;
    }


    public void passOrCancelApproval(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(), "passOrCancelApproval", "Entering passOrCancelApproval");
        DCBindingContainer bindingContainer = ADFUtils.getDCBindingContainer();
        DCIteratorBinding binding = bindingContainer.findIteratorBinding("OpportunityVO1Iterator");
        String opportunityNumber = null, opportunityId = null, createdBy = null;
        try {
            Row currentRow = binding.getCurrentRow();
            if (ADFContext.getCurrent()
                          .getSessionScope()
                          .get("opportunityNumber") == null) {
                if (null != currentRow) {
                    opportunityNumber = (String) currentRow.getAttribute("OpportunityNumber");
                    opportunityId =
                        (String) (currentRow.getAttribute("OpportunityId"))
                        .toString(); //Changed from OpprtunityId to OscOpportunityId to fix defect.
                    createdBy = (String) currentRow.getAttribute("CreatedBy");
                }
            } else {
                opportunityNumber = ADFContext.getCurrent()
                                              .getSessionScope()
                                              .get("opportunityNumber")
                                              .toString();
                opportunityId = ADFContext.getCurrent()
                                          .getSessionScope()
                                          .get("opportunityId")
                                          .toString();
                createdBy = "";
            }
            String approvalType = (String) JSFUtils.resolveExpression("#{pageFlowScope.approvalType}");
            TaskT task = new TaskT();
            task.setOpportunityId(opportunityId);
            task.setOpportunityNumber(opportunityNumber);
            task.setCreatedBy(createdBy);
            task.setTaskTitle(ViewConstants.MANAGE_APPROVAL);
            task.setComments(getComments());
            task.setAction((String) JSFUtils.resolveExpression("#{pageFlowScope.approvalType}"));
            task.setSnapshotId(null != getSnapShotID() ? getSnapShotID().intValue() : null);
            String result = "";
            if (approvalType.equalsIgnoreCase("PASS")) {
                OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                                       .getCurrentBindingsEntry()
                                                                       .getOperationBinding("passApprovalProc");
                op.getParamsMap().put("snapshotId", null != getSnapShotID() ? getSnapShotID().intValue() : null);
                result = (String)op.execute(); 
                
                if ("SUCCESS".equalsIgnoreCase(result)) {
                    setTriggerStatusMessage("This action is successfully completed.");
                    refreshApprovalTable(null,0);
                } else {
                    setTriggerStatusMessage("This action is not successfully completed.");
                }
                
                DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
                DCIteratorBinding itorBinding = bindings.findIteratorBinding("ApprovalButtonsROVOIterator");
                ViewObject vo = itorBinding.getViewObject();
                RowSetIterator iterator = vo.createRowSetIterator(null);
                while (iterator.hasNext()) {
                    Row approvalButonRow = iterator.next();
                    approvalButonRow.setAttribute("PassButton", "true");
                }
                iterator.closeRowSetIterator(); 
            
            } else if (approvalType.equalsIgnoreCase("CANCEL")) {
                OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                                       .getCurrentBindingsEntry()
                                                                       .getOperationBinding("createTask");
                String passedParamsSet1 =
                    "\n 1)OpportunityId=" + task.getOpportunityId() + "\n 2)OpportunityNumber=" +
                    task.getOpportunityNumber() + "\n 3)CreatedBy=" + task.getCreatedBy() + "\n 4)TaskTitle=" +
                    task.getTaskTitle();
                String passedParamsSet2 =
                    "\n 5)Comments=" + task.getComments() + "\n 6)Action=" + task.getAction() + "\n 7)SnapshotId=" +
                    task.getSnapshotId();
                logger.log(Level.INFO, getClass(), "passOrCancelApproval",
                           "Passing parameters to createTask method --" + passedParamsSet1 + passedParamsSet2);
                op.getParamsMap().put("task", task);
                result = (String) op.execute();

            
            logger.log(Level.INFO, getClass(), "passOrCancelApproval", "Result of  createTask method --" + result);

            if ("SUCCESS".equalsIgnoreCase(result)) {
                setTriggerStatusMessage("This action is successfully completed.");                              
                refreshApprovalTable("CANCEL",ViewConstants.DELAY_CANCEL);
            } else {
                setTriggerStatusMessage("This action is not successfully completed.");
            }
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, getClass(), "passOrCancelApproval", ex.getMessage());
            CustomErrorHandler.processError("CCT", "passOrCancelApproval", "CCT_SYS_EX42", ex);
        }
        setComments(null);
        getApprovalPopUp().hide();

        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getTriggerPopUp().show(hints);
        logger.log(Level.INFO, getClass(), "passOrCancelApproval", "Exiting passOrCancelApproval");
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getComments() {
        return comments;
    }


    public void closeApprovalPopUP(ActionEvent actionEvent) {
        setComments(null);
        getApprovalPopUp().hide();
    }

    public void openApprovalPopUP(ActionEvent actionEvent) {
        setComments(null);
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getApprovalPopUp().show(hints);
    }

    public void setApprovalPopUp(RichPopup approvalPopUp) {
        this.approvalPopUp = approvalPopUp;
    }

    public RichPopup getApprovalPopUp() {
        return approvalPopUp;
    }

    public void approveOrRejectTask(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(), "approveOrRejectTask", "Entering approveOrRejectTask");
        try {
            OperationBinding op = (OperationBinding) ADFUtils.findOperationBinding("closeTask");
            String taskID = null;
            if (null != ADFContext.getCurrent()
                                  .getSessionScope()
                                  .get(ViewConstants.taskId))
                taskID = (String) ADFContext.getCurrent()
                                            .getSessionScope()
                                            .get(ViewConstants.taskId);
//            if (null != JSFUtils.resolveExpression("#{pageFlowScope.taskID}"))
//                taskID = (String) JSFUtils.resolveExpression("#{pageFlowScope.taskID}");
             if (null != JSFUtils.resolveExpression("#{pageFlowScope.selectedTaskId}"))
                taskID = (String) JSFUtils.resolveExpression("#{pageFlowScope.selectedTaskId}");
            System.out.println("action value $$"+JSFUtils.resolveExpression("#{pageFlowScope.action}"));
            op.getParamsMap().put("taskId", taskID);
            op.getParamsMap().put("outcome", (String) JSFUtils.resolveExpression("#{pageFlowScope.action}"));
            op.getParamsMap().put("comments", getComments());
            logger.log(Level.INFO, getClass(), "approveOrRejectTask",
                       "Passing parameters to closeTask method--" + "\n 1)taskId=" + taskID + "\n 2)outcome=" +
                       (String) JSFUtils.resolveExpression("#{pageFlowScope.action}"));
            String result = (String) op.execute();
            logger.log(Level.INFO, getClass(), "approveOrRejectTask",
                       "Result of  approveOrRejectTask method --" + result);
            if ("SUCCESS".equalsIgnoreCase(result)) {
                setTriggerStatusMessage("This action is successfully completed.");
                refreshApprovalTable("APPROVE_OR_REJECT",ViewConstants.DELAY_APPROVE);
            } else {
                setTriggerStatusMessage("This action is unsuccessful.");
            }
        } catch (Exception ae) {
            logger.log(Level.SEVERE, getClass(), "approveOrRejectTask", ae.getMessage());
            CustomErrorHandler.processError("CCT", "approveOrRejectTask", "CCT_SYS_EX43", ae);
        }
        setComments(null);
        getAprroveRejectPopUp().hide();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getTriggerPopUp().show(hints);
        logger.log(Level.INFO, getClass(), "approveOrRejectTask", "Exiting approveOrRejectTask");
    }


    //TODO this method is not using anywhere need to remove later.

    public void rejectTask(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(), "rejectTask", "Entering rejectTask");
        DCBindingContainer bindingContainer = ADFUtils.getDCBindingContainer();
        DCIteratorBinding binding = bindingContainer.findIteratorBinding("OpportunityVO1Iterator");
        try {
            Row currentRow = binding.getCurrentRow();
            String opportunityNumber = null, opportunityId = null, createdBy = null;
            if (ADFContext.getCurrent()
                          .getSessionScope()
                          .get("opportunityNumber") == null) {
                opportunityNumber = (String) currentRow.getAttribute("OpportunityNumber");
                opportunityId = (String) (currentRow.getAttribute("OpportunityId")).toString();
                createdBy = (String) currentRow.getAttribute("CreatedBy");
            } else {
                opportunityNumber = ADFContext.getCurrent()
                                              .getSessionScope()
                                              .get("opportunityNumber")
                                              .toString();
                opportunityId = ADFContext.getCurrent()
                                          .getSessionScope()
                                          .get("opportunityId")
                                          .toString();
                createdBy = "";
            }
            TaskT task = new TaskT();
            task.setOpportunityId(opportunityId);
            task.setOpportunityNumber(opportunityNumber);
            task.setCreatedBy(createdBy);
            task.setTaskTitle("ManageApproval");
            task.setComments(getComments());
            task.setAction((String) JSFUtils.resolveExpression("#{pageFlowScope.approvalType}"));
            task.setSnapshotId(getSnapShotID().intValue());
            task.setAction("REJECT");
            OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                                   .getCurrentBindingsEntry()
                                                                   .getOperationBinding("createTask");
            op.getParamsMap().put("task", task);
            String result = (String) op.execute();
            if ("SUCCESS".equalsIgnoreCase(result)) {
                setTriggerStatusMessage(ViewConstants.REJECT_SUCCESS);
            } else {
                setTriggerStatusMessage(ViewConstants.REJECT_FAILED);
            }
        } catch (Exception ae) {
            logger.log(Level.SEVERE, getClass(), "rejectTask", ae.getMessage());
            CustomErrorHandler.processError("CCT", "rejectTask", "CCT_SYS_EX44", ae);
        }
        setComments(null);
        getApprovalPopUp().hide();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getTriggerPopUp().show(hints);
        logger.log(Level.INFO, getClass(), "rejectTask", "Exiting rejectTask");
    }


    public void setAprroveRejectPopUp(RichPopup aprroveRejectPopUp) {
        this.aprroveRejectPopUp = aprroveRejectPopUp;
    }

    public RichPopup getAprroveRejectPopUp() {
        return aprroveRejectPopUp;
    }

    public BigDecimal getSnapShotID() {
        logger.log(Level.INFO, getClass(), "getSnapShotID", "Entering getSnapShotID");
        BigDecimal OpportunityId = BigDecimal.ZERO;
        if (ADFContext.getCurrent()
                      .getSessionScope()
                      .get("opportunityNumber") == null)
            OpportunityId = (BigDecimal) JSFUtils.resolveExpression("#{bindings.OpportunityId.inputValue}");
        else {
            BigDecimal oppid = new BigDecimal(ADFContext.getCurrent()
                                                        .getSessionScope()
                                                        .get("opportunityId")
                                                        .toString());
            OpportunityId = oppid;
        }
        OperationBinding binding = (OperationBinding) ADFUtils.findOperationBinding("getLatestSnapID");
        binding.getParamsMap().put("oppID", OpportunityId);
        logger.log(Level.INFO, getClass(), "getSnapShotID",
                   "Passing parameters to getLatestSnapID method --OpportunityId=" + OpportunityId);
        BigDecimal snapShotId = (BigDecimal) binding.execute();
        logger.log(Level.INFO, getClass(), "getSnapShotID",
                   "Result for getLatestSnapID method --snapShotId=" + snapShotId);
        logger.log(Level.INFO, getClass(), "getSnapShotID", "Exiting getSnapShotID");
        return snapShotId;
    }

    public void openApproveOrRejectPopUp(ActionEvent actionEvent) {
        setComments(null);
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getAprroveRejectPopUp().show(hints);
    }

    public void closeApproveOrRejectPopUP(ActionEvent actionEvent) {
        getAprroveRejectPopUp().hide();
    }

    public void setTriggerApprovalPopUp(RichPopup triggerApprovalPopUp) {
        this.triggerApprovalPopUp = triggerApprovalPopUp;
    }

    public RichPopup getTriggerApprovalPopUp() {
        return triggerApprovalPopUp;
    }

    public void setListofApprovers(String listofApprovers) {
        this.listofApprovers = listofApprovers;
    }

    public String getListofApprovers() {
        return listofApprovers;
    }

    public void triggerApprovalActionListner(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(), "triggerApprovalActionListner", "Entering triggerApprovalActionListner");
        DCBindingContainer bindingContainer = ADFUtils.getDCBindingContainer();
        DCIteratorBinding binding = bindingContainer.findIteratorBinding("OpportunityVO1Iterator");
        try {
            Row currentRow = binding.getCurrentRow();
            String opportunityNumber = null, opportunityId = null, createdBy = null;
            if (ADFContext.getCurrent()
                          .getSessionScope()
                          .get("opportunityNumber") == null) {
                if (null != currentRow) {
                    opportunityNumber = (String) currentRow.getAttribute("OpportunityNumber");
                    try {
                        opportunityId = (String) (currentRow.getAttribute("OpportunityId")).toString();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    createdBy = (String) currentRow.getAttribute("CreatedBy");
                }
            } else {
                opportunityNumber = ADFContext.getCurrent()
                                              .getSessionScope()
                                              .get("opportunityNumber")
                                              .toString();
                opportunityId = ADFContext.getCurrent()
                                          .getSessionScope()
                                          .get("opportunityId")
                                          .toString();
                createdBy = "";
            }
            TaskT task = new TaskT();
            task.setOpportunityId(opportunityId);
            task.setOpportunityNumber(opportunityNumber);
            task.setCreatedBy(createdBy);
            task.setSnapshotId(null != getSnapShotID() ? getSnapShotID().intValue() : null);
            task.setTaskTitle(ViewConstants.MANAGE_APPROVAL);
            task.setAction(ViewConstants.CREATE);
            task.setComments(getComments());
            OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                                   .getCurrentBindingsEntry()
                                                                   .getOperationBinding("createTask");
            op.getParamsMap().put("task", task);
            String passedParamsSet1 =
                "\n 1)OpportunityId=" + task.getOpportunityId() + "\n 2)OpportunityNumber=" +
                task.getOpportunityNumber() + "\n 3)CreatedBy=" + task.getCreatedBy() + "\n 4)TaskTitle=" +
                task.getTaskTitle();
            String passedParamsSet2 =
                "\n 5)Comments=" + task.getComments() + "\n 6)Action=" + task.getAction() + "\n 7)SnapshotId=" +
                task.getSnapshotId();
            logger.log(Level.INFO, getClass(), "triggerApprovalActionListner",
                       "Passed Parameters to createTask method --" + passedParamsSet1 + passedParamsSet2);
            String result = (String) op.execute();
            logger.log(Level.INFO, getClass(), "triggerApprovalActionListner",
                       "Return status from createTask method --result=" + result);
            if ("SUCCESS".equalsIgnoreCase(result)) {
                setTriggerStatusMessage(ViewConstants.TRIGGER_APPROVAL_SUCCESS);
                refreshApprovalTable("TRIGGER",ViewConstants.DELAY_TRIGGER);
            } else {
                setTriggerStatusMessage(ViewConstants.TRIGGER_APPROVAL_FAILED);
            }
        } catch (Exception ae) {
            logger.log(Level.SEVERE, getClass(), "triggerApprovalActionListner", ae.getMessage());
            CustomErrorHandler.processError("CCT", "triggerApprovalActionListner", "CCT_SYS_EX45", ae);
        }
        getTriggerApprovalPopUp().hide();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getTriggerPopUp().show(hints);
        logger.log(Level.INFO, getClass(), "triggerApprovalActionListner", "Exiting triggerApprovalActionListner");
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public boolean isFlag() {
        return flag;
    }

    public String getHashMapString(HashMap hashMap) {
        StringBuilder stb = new StringBuilder();
        int index = 0;
        stb.append("------Passed hashMap Values---");
        if (null != hashMap) {
            Iterator iter = hashMap.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry pair = (Map.Entry) iter.next();
                index = index + 1;
                stb.append("\n" + index + ")" + pair.getKey() + "=" + pair.getValue());
            }

        }
        return stb.toString();
    }
    
    //TODO
    public void refreshApprovalTable(String approvalType,long time) {
        logger.log(Level.INFO, getClass(), "refreshApprovalTable", "Entering refreshApprovalTable");
        if(null!=approvalType){
                try {
                    long startTime = System.currentTimeMillis();
                    Thread.sleep(time);
                    long endTime = System.currentTimeMillis();
                    logger.log(Level.INFO, getClass(), "refreshApprovalTable","Total delay "+(endTime-startTime)/1000+" Secs");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    logger.log(Level.INFO, getClass(), "refreshApprovalTable","Thread sleeping failed.");
                }
            }
        OperationBinding fetchSnapshots = (OperationBinding) ADFUtils.findOperationBinding("fetchSnapshots");
        DCBindingContainer bindingContainer = ADFUtils.getDCBindingContainer();
        DCIteratorBinding binding = bindingContainer.findIteratorBinding("OpportunityVO1Iterator");
        String opportunityNumber = null, opportunityId = null;
        try {
            Row currentRow = binding.getCurrentRow();
        if (ADFContext.getCurrent()
                      .getSessionScope()
                      .get("opportunityNumber") == null) {
            if (null != currentRow) {
                opportunityNumber = (String) currentRow.getAttribute("OpportunityNumber");
                try {
                    opportunityId = (String) (currentRow.getAttribute("OpportunityId")).toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            opportunityNumber = ADFContext.getCurrent()
                                          .getSessionScope()
                                          .get("opportunityNumber")
                                          .toString();
            opportunityId = ADFContext.getCurrent()
                                      .getSessionScope()
                                      .get("opportunityId")
                                      .toString();            
        }
        } catch (Exception ae) {
            logger.log(Level.SEVERE, getClass(), "triggerApprovalActionListner", ae.getMessage());
            CustomErrorHandler.processError("CCT", "triggerApprovalActionListner", "CCT_SYS_EX45", ae);
        }
        fetchSnapshots.getParamsMap()
            .put("opportunityId", opportunityId);
        fetchSnapshots.getParamsMap()
            .put("opportunityNumber",opportunityNumber);
        logger.log(Level.INFO, getClass(), "refreshApprovalTable",
                   "Parameters passing to fetchSnapshots method --\n 1)opportunityId=--" +
                   opportunityId + "-- \n 2)opportunityNumber=--" +opportunityNumber + "--");
        fetchSnapshots.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(getTableBind());
        logger.log(Level.INFO, getClass(), "refreshApprovalTable", "Exiting refreshApprovalTable");
    }

    public void setTableBind(RichTable tableBind) {
        this.tableBind = tableBind;
    }

    public RichTable getTableBind() {
        return tableBind;
    }

    public void setApprovalEnabled(boolean approvalEnabled) {
        this.approvalEnabled = approvalEnabled;
    }

    public boolean isApprovalEnabled() {
        return approvalEnabled;
    }

    public void setRejectApprovalEnabled(boolean rejectApprovalEnabled) {
        this.rejectApprovalEnabled = rejectApprovalEnabled;
    }

    public boolean isRejectApprovalEnabled() {
        return rejectApprovalEnabled;
    }

    public void setTriggerApprovalEnabled(boolean triggerApprovalEnabled) {
        this.triggerApprovalEnabled = triggerApprovalEnabled;
    }

    public boolean isTriggerApprovalEnabled() {
        return triggerApprovalEnabled;
    }

    public void setCancelInFlightApprovalEnabled(boolean cancelInFlightApprovalEnabled) {
        this.cancelInFlightApprovalEnabled = cancelInFlightApprovalEnabled;
    }

    public boolean isCancelInFlightApprovalEnabled() {
        return cancelInFlightApprovalEnabled;
    }

    public void setPassApprovalEnabled(boolean passApprovalEnabled) {
        this.passApprovalEnabled = passApprovalEnabled;
    }

    public boolean isPassApprovalEnabled() {
        return passApprovalEnabled;
    }

    private RichPopup submitToSales;
    //SUBMIT_TO_SALES_POPUP
    private String popUpMsg = ViewConstants.SAVED_POPUP;

    public void submitOSC(ActionEvent actionEvent) {
        // Add event code here...
        try {
            logger.log(Level.INFO, getClass(), "submitOSC", "Entering submitOSC method");
            //JSFUtils.resolveExpression("#{bindings.OpportunityId.inputValue}")
            BigDecimal opportunityId = (BigDecimal) JSFUtils.resolveExpression("#{bindings.OpportunityId.inputValue}");
            String validStatus = (String) JSFUtils.resolveExpression("#{bindings.Status.inputValue}");
           
            if (validStatus != null) {
                OperationBinding binding = (OperationBinding) ADFUtils.findOperationBinding("submitOSCApprovalButton");
                binding.getParamsMap().put("oppID", opportunityId);
                String status = (String) binding.execute();
                if ("success".equalsIgnoreCase(status)) {
                    setPopUpMsg(ViewConstants.QUOTE_SUBMIT_SUCCESS);
                    refreshApprovalTable(null,0);
                } else {
                    setPopUpMsg(ViewConstants.QUOTE_SUBMIT_FAIL);
                }
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                getSubmitToSales().show(hints);
            } else {
                setPopUpMsg(ViewConstants.SUBMIT_TO_SALES_POPUP);
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                getSubmitToSales().show(hints);
            }
        } catch (Exception e) {
            // TODO: Add catch code
            //    e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "submitOSC", e.getMessage());
            CustomErrorHandler.processError("CCT", "submitOSC", "CCT_SYS_EX05", e);
        }
        logger.log(Level.INFO, getClass(), "submitOSC", "Exiting submitOSC method");
    }

    public void setSubmitSales(boolean submitSales) {
        this.submitSales = submitSales;
    }

    public boolean isSubmitSales() {
        return submitSales;
    }

    public void setSubmitToSales(RichPopup submitToSales) {
        this.submitToSales = submitToSales;
    }

    public RichPopup getSubmitToSales() {
        return submitToSales;
    }

    public void setPopUpMsg(String popUpMsg) {
        this.popUpMsg = popUpMsg;
    }

    public String getPopUpMsg() {
        return popUpMsg;
    }

    public void closePopUp(ActionEvent actionEvent) {
        // Add event code here...
        getSubmitToSales().hide();
    }
    
    public void approvalUsersVCL(ValueChangeEvent valueChangeEvent) {
        
        if (null!=valueChangeEvent.getNewValue()) {
            String userInfo = getUserInfo((String)valueChangeEvent.getNewValue(),"");
            getAccntVicePresBind().setValue(userInfo);
            getAccntVicePresBind().setSubmittedValue(userInfo);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(getAccntVicePresBind());
    }
    
    public String getUserInfo(String userid,String userGroup){
            String userName=null;
            String userRole=null;
            ActiveDirectoryManagerImpl adImpl = new ActiveDirectoryManagerImpl();
            User user = adImpl.fetchUserDetailsById(userid);
            if(null!=user && null!=user.getName()){
                userName=user.getName();
            Roles roles = user.getRoles();
            ArrayList<String> list = (ArrayList<String>) roles.getRole();
            if(null!=list)
            for(String role : list){
                logger.log(Level.INFO, getClass(), "getUserInfo roles--","--"+role+"---userName--->"+userName+"--userGroup---"+userGroup); 
            }
            if(null!=list && list.contains(userGroup))
                userRole=null;
            else
                userRole="InCorrectGroup";
                
            if(null!=userRole)
                    return userid+"  #"+userRole;
            else
                return userid+(null!=userName ? "-"+userName :"");             
            }
            else if(null!=userid && !"".equalsIgnoreCase(userid))
                return userid+"  #InCorrectUser";
            else
                return null;
        }

    public void saveEditApprovals(ActionEvent actionEvent) {
        DCBindingContainer bindingContainer = ADFUtils.getDCBindingContainer();
        DCIteratorBinding opportunityVO1Iterator = bindingContainer.findIteratorBinding("OpportunityVO1Iterator");
        String opportunityNumber = null,  createdBy = null;
        BigDecimal opportunityId = null;
        Row currentRow = opportunityVO1Iterator.getCurrentRow();
        if (ADFContext.getCurrent()
                      .getSessionScope()
                      .get("opportunityNumber") == null) {
            if (null != currentRow){
                try {
                    opportunityNumber = (String) currentRow.getAttribute("OpportunityNumber");
                    opportunityId = (BigDecimal) currentRow.getAttribute("OpportunityId");
                } catch (Exception e) {
                    // TODO: Add catch code
                    e.printStackTrace();
                }
            }
        } else
            opportunityNumber = ADFContext.getCurrent()
                                          .getSessionScope()
                                          .get("opportunityNumber")
                                          .toString();
        OperationBinding binding = (OperationBinding) ADFUtils.findOperationBinding("insertApprovalUsers");
        HashMap hashMap = new HashMap();
        hashMap.put("opportunityNumber", opportunityNumber); //66997
        hashMap.put("opportunityId", opportunityId);
        hashMap.put("snapShotId", getSnapShotID()); //41
        binding.getParamsMap().put("hashMap", hashMap);        
        String status = (String) binding.execute();
        refreshApprovalTable(null,0);
        getEditApprovalPopUp().hide();
        if ("success".equalsIgnoreCase(status)) {
            setTriggerStatusMessage("Approvers details saved successfully.");
        } else {
            setTriggerStatusMessage("Approver details could not be saved. Please retry.");
        }        
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getTriggerPopUp().show(hints);
    }


    public void editApprovalAL(ActionEvent actionEvent) {
        DCBindingContainer bindingContainer = ADFUtils.getDCBindingContainer();
        DCIteratorBinding opportunityVO1Iterator = bindingContainer.findIteratorBinding("OpportunityVO1Iterator");
        String opportunityNumber = null,  createdBy = null;
        BigDecimal opportunityId = null;
        Row currentRow = opportunityVO1Iterator.getCurrentRow();
        if (ADFContext.getCurrent()
                      .getSessionScope()
                      .get("opportunityNumber") == null) {
            if (null != currentRow){
                try {
                    opportunityNumber = (String) currentRow.getAttribute("OpportunityNumber");
                    opportunityId = (BigDecimal) currentRow.getAttribute("OpportunityId");
                } catch (Exception e) {
                    // TODO: Add catch code
                    e.printStackTrace();
                }
            }
        } else
            opportunityNumber = ADFContext.getCurrent()
                                          .getSessionScope()
                                          .get("opportunityNumber")
                                          .toString();
        HashMap hashMap=new HashMap();
        hashMap.put("opportunityId", opportunityId);
        hashMap.put("snapShotId", getSnapShotID());
        OperationBinding binding = (OperationBinding) ADFUtils.findOperationBinding("executeApprovalUsers");
        binding.getParamsMap().put("hashMap", hashMap);
        Object execute = binding.execute();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getEditApprovalPopUp().show(hints);
    }

    public void setEditApprovalPopUp(RichPopup editApprovalPopUp) {
        this.editApprovalPopUp = editApprovalPopUp;
    }

    public RichPopup getEditApprovalPopUp() {
        return editApprovalPopUp;
    }


    public void cancelEditApprovals(ActionEvent actionEvent) {
        getEditApprovalPopUp().hide();
    }

    public void setSalesLvlOneBind(RichInputText salesLvlOneBind) {
        this.salesLvlOneBind = salesLvlOneBind;
    }

    public RichInputText getSalesLvlOneBind() {
        return salesLvlOneBind;
    }

    public void salesLvlOneVCL(ValueChangeEvent valueChangeEvent) {
        if (null!=valueChangeEvent.getNewValue()) {
            String userInfo = getUserInfo((String)valueChangeEvent.getNewValue(),(String) ParamCache.userRoleMap.get("CCT_SALES_DIRECTOR"));
            getSalesLvlOneBind().setValue(userInfo);
            getSalesLvlOneBind().setSubmittedValue(userInfo);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(getSalesLvlOneBind());
    }

    public void accntVicePresVCL(ValueChangeEvent valueChangeEvent) {
        if (null!=valueChangeEvent.getNewValue()) {
            String userInfo = getUserInfo((String)valueChangeEvent.getNewValue(),(String) ParamCache.userRoleMap.get("CCT_ACCOUNTING_DIRECTOR"));
            getAccntVicePresBind().setValue(userInfo);
            getAccntVicePresBind().setSubmittedValue(userInfo);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(getAccntVicePresBind());
    }

    public void setAccntVicePresBind(RichInputText accntVicePresBind) {
        this.accntVicePresBind = accntVicePresBind;
    }

    public RichInputText getAccntVicePresBind() {
        return accntVicePresBind;
    }

    public void setSalesLvlTwoBind(RichInputText salesLvlTwoBind) {
        this.salesLvlTwoBind = salesLvlTwoBind;
    }

    public RichInputText getSalesLvlTwoBind() {
        return salesLvlTwoBind;
    }

    public void salesLvlTwoVCL(ValueChangeEvent valueChangeEvent) {
        if (null!=valueChangeEvent.getNewValue()) {
            String userInfo = getUserInfo((String)valueChangeEvent.getNewValue(),(String) ParamCache.userRoleMap.get("CCT_SALES_VP"));
            getSalesLvlTwoBind().setValue(userInfo);
            getSalesLvlTwoBind().setSubmittedValue(userInfo);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(getSalesLvlTwoBind());
    }

    public void gvAndGmVCL(ValueChangeEvent valueChangeEvent) {
        if (null!=valueChangeEvent.getNewValue()) {
            String userInfo = getUserInfo((String)valueChangeEvent.getNewValue(),(String) ParamCache.userRoleMap.get("CCT_GROUP_VP_AND_GM"));
            getGvAndGmBind().setValue(userInfo);
            getGvAndGmBind().setSubmittedValue(userInfo);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(getGvAndGmBind());
    }

    public void setGvAndGmBind(RichInputText gvAndGmBind) {
        this.gvAndGmBind = gvAndGmBind;
    }

    public RichInputText getGvAndGmBind() {
        return gvAndGmBind;
    }

    public void setFinanceVicePresBind(RichInputText financeVicePresBind) {
        this.financeVicePresBind = financeVicePresBind;
    }

    public RichInputText getFinanceVicePresBind() {
        return financeVicePresBind;
    }

    public void financeVicePresVCL(ValueChangeEvent valueChangeEvent) {
        if (null!=valueChangeEvent.getNewValue()) {
            String userInfo = getUserInfo((String)valueChangeEvent.getNewValue(),(String) ParamCache.userRoleMap.get("CCT_FINANCE_VP"));
            getFinanceVicePresBind().setValue(userInfo);
            getFinanceVicePresBind().setSubmittedValue(userInfo);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(getFinanceVicePresBind());
    }

    public void chiefFinOffVCL(ValueChangeEvent valueChangeEvent) {
        if (null!=valueChangeEvent.getNewValue()) {
            String userInfo = getUserInfo((String)valueChangeEvent.getNewValue(),(String) ParamCache.userRoleMap.get("CCT_CORPORATE_CFO"));
            getChiefFinanceOffBind().setValue(userInfo);
            getChiefFinanceOffBind().setSubmittedValue(userInfo);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(getChiefFinanceOffBind());
    }

    public void operationVicePresVCL(ValueChangeEvent valueChangeEvent) {
        if (null!=valueChangeEvent.getNewValue()) {
            String userInfo = getUserInfo((String)valueChangeEvent.getNewValue(),(String) ParamCache.userRoleMap.get("CCT_OPERATIONS_VP"));
            getOperationVicePresBind().setValue(userInfo);
            getOperationVicePresBind().setSubmittedValue(userInfo);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(getOperationVicePresBind());
    }

    public void setOperationVicePresBind(RichInputText operationVicePresBind) {
        this.operationVicePresBind = operationVicePresBind;
    }

    public RichInputText getOperationVicePresBind() {
        return operationVicePresBind;
    }

    public void setChiefFinanceOffBind(RichInputText chiefFinanceOffBind) {
        this.chiefFinanceOffBind = chiefFinanceOffBind;
    }

    public RichInputText getChiefFinanceOffBind() {
        return chiefFinanceOffBind;
    }
    
    public ArrayList<String> sortUserList(ArrayList<String> userList){
        ArrayList<String> newUserList=new ArrayList<String>();
        if(null!=userList){
//            int splitlength = userInfo.indexOf("-");
//            hashMap.put("userId", (userInfo.substring(0, splitlength)).trim());   
        }
        return newUserList;
        }
    
	private void getApprovalAccess(HashMap userAcess, boolean approvalEnabled, boolean rejectApprovalEnabled) {
        logger.log(Level.INFO, getClass(), "getApprovalAccess", "Entering getApprovalAccess");     
        if (ADFContext.getCurrent()
                      .getSessionScope()
                      .get(ViewConstants.taskId) != null) {
            logger.log(Level.INFO, getClass(), "getApprovalAccess", "Task Id = "+ ADFContext.getCurrent()
                              .getSessionScope()
                              .get(ViewConstants.taskId));
            logger.log(Level.INFO, getClass(), "getApprovalAccess", "Task Type = "+ ADFContext.getCurrent()
                              .getSessionScope()
                              .get(ViewConstants.TASK_TYPE));
            logger.log(Level.INFO, getClass(), "getApprovalAccess", "TASK_OWNER_USER = "+ ADFContext.getCurrent()
                              .getSessionScope()
                              .get(ViewConstants.TASK_OWNER_USER));
            if (ADFContext.getCurrent()
                          .getSessionScope()
                          .get(ViewConstants.TASK_TYPE) != null &&
                ADFContext.getCurrent().getSessionScope().get(ViewConstants.TASK_OWNER_USER) !=
                                  null) {
                if ((ADFContext.getCurrent()
                               .getSessionScope()
                               .get(ViewConstants.TASK_TYPE)
                               .equals(ViewConstants.MANAGE_APPROVAL) && ADFContext.getCurrent()
                                                                                   .getSessionScope()
                                                                                   .get(ViewConstants.TASK_OWNER_USER)
                                                                                   .equals(ADFContext.getCurrent()
                                                                                                     .getSecurityContext()
                                                                                                     .getUserName()))) {
                    logger.log(Level.INFO, getClass(), "getApprovalAccess", "Accessing task through email");
                    this.approvalEnabled = ((Boolean) userAcess.get(ViewConstants.APPROVE_ACTION)).booleanValue();
                    this.rejectApprovalEnabled = ((Boolean) userAcess.get(ViewConstants.REJECT_ACTION)).booleanValue();
                    logger.log(Level.INFO, getClass(), "getApprovalAccess", "approvalEnabled"+ this.approvalEnabled);
                    logger.log(Level.INFO, getClass(), "getApprovalAccess", "rejectApprovalEnabled"+ this.rejectApprovalEnabled);
                }
            }
        } else if (ADFContext.getCurrent()
                             .getPageFlowScope()
                             .get("selectedTaskId") != null) {
            
            
            
            Users user = (Users) ADFContext.getCurrent()
                                           .getSessionScope()
                                           .get(ViewConstants.LOGGED_IN_USER);
            ArrayList<String> userGroup = (ArrayList<String>) user.getUserGroups();
            
            if (ADFContext.getCurrent()
                          .getPageFlowScope()
                          .get("selectedTaskType") != null && ADFContext.getCurrent()
                                                                .getPageFlowScope()
                                                                .get("selectedTaskOwner") != null) {
                if (ADFContext.getCurrent()
                              .getPageFlowScope()
                              .get("selectedTaskType")
                              .equals(ViewConstants.MANAGE_APPROVAL) && ADFContext.getCurrent()
                                                                                  .getPageFlowScope()
                                                                                  .get("selectedTaskOwner")
                                                                                  .equals(ADFContext.getCurrent()
                                                                                                    .getSecurityContext()
                                                                                                    .getUserName())) {
                    logger.log(Level.INFO, getClass(), "getApprovalAccess", "Accessing task through task dashboard");
                    this.approvalEnabled = ((Boolean) userAcess.get(ViewConstants.APPROVE_ACTION)).booleanValue();
                    this.rejectApprovalEnabled = ((Boolean) userAcess.get(ViewConstants.REJECT_ACTION)).booleanValue();
                } 
                //ADFContext.getCurrent()
                //                                     .getPageFlowScope()
                 //                                    .get(ViewConstants.TASK_TYPE)
                else if (ADFContext.getCurrent()
                                     .getPageFlowScope()
                                     .get("selectedTaskType")
                                     .equals(ViewConstants.MANAGE_APPROVAL) &&
                           userGroup.contains((String) ParamCache.userRoleMap.get("CCT_CONTRACTS"))) { 
                    logger.log(Level.INFO, getClass(), "getApprovalAccess", "CCT_CONTRACTS");
                    this.approvalEnabled = true;
                    this.rejectApprovalEnabled = true;

                }
            }
        }
    }

    public void supplyChainVicePresVCL(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        if (null!=valueChangeEvent.getNewValue()) {
            String userInfo = getUserInfo((String)valueChangeEvent.getNewValue(),(String) ParamCache.userRoleMap.get("CCT_SUPPLY_CHAIN_VP"));
            getSupplyChainVPBind().setValue(userInfo);
            getSupplyChainVPBind().setSubmittedValue(userInfo);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(getSupplyChainVPBind());
    }

    public void setSupplyChainVPBind(RichInputText supplyChainVPBind) {
        this.supplyChainVPBind = supplyChainVPBind;
    }

    public RichInputText getSupplyChainVPBind() {
        return supplyChainVPBind;
    }
    public void setErrorPopUpBid(RichPopup errorPopUpBid) {
        this.errorPopUpBid = errorPopUpBid;
    }

    public RichPopup getErrorPopUpBid() {
        return errorPopUpBid;
    }


    public void setErrorPopUpMessage(String errorPopUpMessage) {
        this.errorPopUpMessage = errorPopUpMessage;
    }

    public String getErrorPopUpMessage() {
        return errorPopUpMessage;
    }

    public void exportAllApprovals(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        DCBindingContainer bindingContainer = ADFUtils.getDCBindingContainer();
//        DCIteratorBinding iterBinding = bindingContainer.findIteratorBinding("ApprovalDynamicVOIterator");
//        ViewObject approvalDynamicVO = iterBinding.getViewObject();           
//        oracle.jbo.Row[] rows = approvalDynamicVO.getAllRowsInRange();
//        
//        RowSetIterator approvalValues = approvalDynamicVO.createRowSetIterator(null);
//            while(approvalValues.hasNext()) {
//                Row next = approvalValues.next();
//                int attributeCount = next.getAttributeCount();
//                for(int j=0;j<attributeCount-1;j++){
//                System.out.println("approvalValues values"+next.getAttribute(j));
//                }
//            }
        try {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet worksheet = workbook.createSheet("Approval");
        
        DCIteratorBinding iterBinding = bindingContainer.findIteratorBinding("ApprovalDynamicVOIterator");
        ViewObject approvalDynamicVO = iterBinding.getViewObject();         
        oracle.jbo.Row[] rows = approvalDynamicVO.getAllRowsInRange();
        RowSetIterator approvalValues = approvalDynamicVO.createRowSetIterator(null);
        int attributeCount = 0;
//        while(approvalValues.hasNext()) {
//            Row next = approvalValues.next();
//            attributeCount = next.getAttributeCount();
//            for(int j=0;j<attributeCount-1;j++){
//            System.out.println("approvalValues values"+next.getAttribute(j));
//            }
//        }
        XSSFRow  excelrow = null;
        XSSFCellStyle style = workbook.createCellStyle();
        style.setBorderTop((short) 6); // double lines border
        style.setBorderBottom((short) 1); // single line border
        XSSFFont font = workbook.createFont();
        font.setFontHeightInPoints((short) 11);
        font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
        style.setFont(font);                 
                                
        int i = 0;
        int k = 0;
        String[] selectedApprovalAttributes = new String[attributeCount];
        String[] approvalAttributes = new String[attributeCount];
        //for (oracle.jbo.Row row : rows) {
        while(approvalValues.hasNext()) {
        //String[] myTaskAttributes = row.getAttributeNames();
        Row next = approvalValues.next();        
        //print header on first row in excel
        if (i == 0) {
            excelrow = (XSSFRow)worksheet.createRow((short)i);
           // short j = 0;
            int approvalAttributeCount = 0;
            int versionCount = 2;
            attributeCount = next.getAttributeCount();
            XSSFCell cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("SNAPSHOTS");
            cellA1.setCellStyle(style); 
            cellA1 = null;
        
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Current Version");
            cellA1.setCellStyle(style);
            cellA1 = null;
            if(attributeCount > 0){
                approvalAttributeCount = (attributeCount - 2);
            }
            if(approvalAttributeCount > 0){
                for(k=approvalAttributeCount; k > 0; k--){
                    cellA1 = excelrow.createCell(versionCount);
                    cellA1.setCellValue("Version"+k);
                    cellA1.setCellStyle(style);
                    cellA1 = null;
                    versionCount++;
                }
            }
            excelrow = null;
        }
        
        //print data from second row in excel
        ++i;       
        excelrow = worksheet.createRow((short)i);   
        String attributeValue = "";
        String[] attributeValues = new String[10];
        for(int m=0;m<attributeCount;m++){
                XSSFCell cell = excelrow.createCell(m);
                if(next.getAttribute(m) != null){
                    if(i == 65 || i==66 || i==67 || i==68 || i==69 || i==70 || i==71 || i==72 || i==73){                        
                        if(next.getAttribute(m).toString().contains(",")){
                            attributeValues = next.getAttribute(m).toString().split(",");
                            attributeValue = attributeValues[0];
                        }
                        else
                            attributeValue = next.getAttribute(m).toString();
                    }
                    else
                        attributeValue = next.getAttribute(m).toString();
                    
                    cell.setCellValue(attributeValue); 
                }
                cell = null;
        }                               
        excelrow = null;
            //}
         
           worksheet.createFreezePane(0, 1, 0, 1);
        for(int n=0;n<attributeCount;n++)
           worksheet.autoSizeColumn(n);
//           worksheet.autoSizeColumn(1);
//           worksheet.autoSizeColumn(2);
//           worksheet.autoSizeColumn(3);
//           worksheet.autoSizeColumn(4);
//           worksheet.autoSizeColumn(5);           
        } 
            workbook.write(outputStream);
            outputStream.flush();
        
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void srCntDirectVCL(ValueChangeEvent valueChangeEvent) {
        
            if (null!=valueChangeEvent.getNewValue()) {
                String userInfo = getUserInfo((String)valueChangeEvent.getNewValue(),(String) ParamCache.userRoleMap.get("CCT_SR_CONTRACTS_DIRECTOR"));
                getSrContDirecBind().setValue(userInfo);
                //getSrContDirecBind().setSubmittedValue(userInfo);
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(getSrContDirecBind());
        }


    public void setSrContDirecBind(RichInputText srContDirecBind) {
        this.srContDirecBind = srContDirecBind;
    }

    public RichInputText getSrContDirecBind() {
        return srContDirecBind;
    }

}
