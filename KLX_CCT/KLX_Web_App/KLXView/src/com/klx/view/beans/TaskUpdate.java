package com.klx.view.beans;

import com.klx.common.logger.KLXLogger;
import com.klx.constants.ServiceConstants;
import com.klx.services.entities.Tasks;
import com.klx.view.beans.constants.ViewConstants;
import com.klx.view.beans.utils.JSFUtils;
import com.klx.view.error.CustomErrorHandler;
import com.klx.common.access.UserActionAccessManagement;
import java.util.HashMap;
import java.util.logging.Level;

import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.RichPopup;

public class TaskUpdate {
    private static KLXLogger logger = KLXLogger.getLogger();
    private String noteDesc;
    private String selectedTaskId;
    private String selectedTaskNumber;
    private String opportunityNumber;
    private Tasks task=new Tasks();
    private String statusMessage;
    private RichPopup statusPopUp;
    private RichPopup closePopup;
    
    private boolean closeTask = false;
    private boolean addNotes = false;

    public void setCloseTask(boolean closeTask) {
        this.closeTask = closeTask;
    }

    public boolean isCloseTask() {
        return closeTask;
    }

    public void setAddNotes(boolean addNotes) {
        this.addNotes = addNotes;
    }

    public boolean isAddNotes() {
        return addNotes;
    }

    public TaskUpdate() {
		userActionsAccess();
    }

    public void userActionsAccess(){
        HashMap actions = new HashMap();
        actions.put(ViewConstants.TASK_UPDATE_CLOSE_TASK, false);
        actions.put(ViewConstants.TASK_UPDATE_ADD_NOTES, false);
        HashMap userAcess = UserActionAccessManagement.getUserAccess(ADFContext.getCurrent()
                                                                               .getSecurityContext()
                                                                               .getUserRoles(), actions);

        closeTask = ((Boolean) userAcess.get(ViewConstants.TASK_UPDATE_CLOSE_TASK)).booleanValue();
        addNotes = ((Boolean) userAcess.get(ViewConstants.TASK_UPDATE_ADD_NOTES)).booleanValue();
    }

    public void addTaskNotes(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(), "addTaskNotes", "Entering addTaskNotes method");
        OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                               .getCurrentBindingsEntry()
                                                               .getOperationBinding("addNotes");
        String opportunityNumber = (String) JSFUtils.resolveExpression("#{pageFlowScope.taskUpdateBean.task.opportunityNumber}");
        String taskNumber = (String) JSFUtils.resolveExpression("#{pageFlowScope.taskUpdateBean.task.taskNumber}");
        
        op.getParamsMap().put("opportunityNo",opportunityNumber);
        op.getParamsMap().put("taskNo", taskNumber);
        op.getParamsMap().put("noteComments", getNoteDesc());
        op.getParamsMap().put("noteLevel", "TASK");
        logger.log(Level.INFO, getClass(), "addTaskNotes", "Parameters passing to method action\n 1)Opportunity Number:--"+opportunityNumber+"-- \n2)getSelectedTaskNumber:--"+taskNumber+"-- \n3)Note Description:--"+getNoteDesc()+"--");
        op.execute();
        setNoteDesc(null);
        refreshNotes();
    }

    public void closeTaskListener(ActionEvent actionEvent) {
        try {
            logger.log(Level.INFO, getClass(), "closeTaskListener", "Entering closeTaskListener method");
            OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                                   .getCurrentBindingsEntry()
                                                                   .getOperationBinding("closeTask");
            String taskId = (String) JSFUtils.resolveExpression("#{pageFlowScope.taskUpdateBean.task.taskId}");
            String assigneeId = (String) JSFUtils.resolveExpression("#{pageFlowScope.taskUpdateBean.task.assignee}");
            
            op.getParamsMap().put("taskId",taskId);
            op.getParamsMap().put("outcome", "CLOSE");
            op.getParamsMap().put("assignedUser", assigneeId);
            logger.log(Level.INFO, getClass(), "addTaskNotes", "Parameters passing to method closeTask\n1)taskId :--"+taskId+"-- \n2)outcome:--CLOSE--");
            String resultStatus = (String) op.execute();
            if(null!=resultStatus && ServiceConstants.SUCCESS.equalsIgnoreCase(resultStatus)){
                setStatusMessage("Task closed Succesfully");
                this.getClosePopup().hide();
                ADFContext.getCurrent().getPageFlowScope().put("closeButton", "CLOSE");
                ADFContext.getCurrent().getPageFlowScope().put("taskStatus", "COMPLETED"); 
                OperationBinding op2 = (OperationBinding) BindingContext.getCurrent()
                                                                       .getCurrentBindingsEntry()
                                                                       .getOperationBinding("fetchTaskDetailsById");
                op2.execute();
                if(op2.getResult() != null)
                    task = (Tasks)op2.getResult();                                   
               
            }else{
                setStatusMessage("Task not closed");
                this.getClosePopup().hide();
            }
            OperationBinding op3 = (OperationBinding) BindingContext.getCurrent()
                                                                    .getCurrentBindingsEntry()
                                                                    .getOperationBinding("loadTasksList");
            op3.execute();
            refreshListIterators();  
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            getStatusPopUp().show(hints);
        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "closeTaskListener", e.getMessage());
            CustomErrorHandler.processError("CCT", "closeTaskListener", "CCT_BUS_EX03", e);
        }
        logger.log(Level.INFO, getClass(), "closeTaskListener", "Exiting closeTaskListener method");
    }

    public void refreshNotes() {
        logger.log(Level.INFO, getClass(), "refreshNotes", "Entering refreshNotes method");
        DCBindingContainer binds = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter1 = binds.findIteratorBinding("TaskNotesVOIterator");
        iter1.executeQuery();
    }

    public void setNoteDesc(String noteDesc) {
        this.noteDesc = noteDesc;
    }

    public String getNoteDesc() {
        return noteDesc;
    }

    public void setSelectedTaskId(String selectedTaskId) {
        this.selectedTaskId = selectedTaskId;
    }

    public String getSelectedTaskId() {
        return selectedTaskId;
    }

    public void setSelectedTaskNumber(String selectedTaskNumber) {
        this.selectedTaskNumber = selectedTaskNumber;
    }

    public String getSelectedTaskNumber() {
        return selectedTaskNumber;
    }


    public void setOpportunityNumber(String opportunityNumber) {
        this.opportunityNumber = opportunityNumber;
    }

    public String getOpportunityNumber() {
        return opportunityNumber;
    }
    
    public void refreshListIterators(){
      DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
      DCIteratorBinding iter1 = binds.findIteratorBinding("myTasksListIterator");
      iter1.executeQuery();
      DCIteratorBinding iter2 = binds.findIteratorBinding("assignedTaskListIterator");
      iter2.executeQuery();
      DCIteratorBinding iter3 = binds.findIteratorBinding("unassignedTaskListIterator");
      iter3.executeQuery();
    }


    public void setTask(Tasks task) {
        this.task = task;
    }

    public Tasks getTask() {
        return task;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusPopUp(RichPopup statusPopUp) {
        this.statusPopUp = statusPopUp;
    }

    public RichPopup getStatusPopUp() {
        return statusPopUp;
    }

    public void showClosePopup(ActionEvent actionEvent) {
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
                this.getClosePopup().show(hints);
    }

    public void setClosePopup(RichPopup closePopup) {
        this.closePopup = closePopup;
    }

    public RichPopup getClosePopup() {
        return closePopup;
    }

    public void closeTask(ActionEvent actionEvent) {
       this.getClosePopup().hide();
    }
}
