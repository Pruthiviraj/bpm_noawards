package com.klx.view.beans.scope.pageFlow;

import com.klx.common.access.UserActionAccessManagement;
import com.klx.common.logger.KLXLogger;

import com.klx.view.beans.constants.ViewConstants;
import com.klx.view.beans.utils.ADFUtils;

import com.klx.view.beans.utils.JSFUtils;

import java.io.Serializable;

import java.util.HashMap;
import java.util.logging.Level;

import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;

import oracle.binding.BindingContainer;

import oracle.jbo.VariableValueManager;
import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewCriteriaManager;
import oracle.jbo.ViewObject;
import oracle.jbo.uicli.binding.JUCtrlListBinding;

public class NotesFlowBean implements Serializable {
    public NotesFlowBean() {
        super();
        userActionsAccess();
    }
    
    private String opportunityId;
    private String opportunityNumber;
    private String notesDesc;   
    private boolean addSearchNotes;    

    public void userActionsAccess(){
        HashMap actions = new HashMap();
        actions.put(ViewConstants.ADD_SEARCH_NOTES, false);       
        HashMap userAcess = UserActionAccessManagement.getUserAccess(ADFContext.getCurrent().getSecurityContext().getUserRoles(), actions);        
        addSearchNotes = ((Boolean)userAcess.get(ViewConstants.ADD_SEARCH_NOTES)).booleanValue();        
    }

    public void setAddSearchNotes(boolean addSearchNotes) {
        this.addSearchNotes = addSearchNotes;
    }

    public boolean isAddSearchNotes() {
        return addSearchNotes;
    }

    private static ADFLogger _logger = ADFLogger.createADFLogger(NotesFlowBean.class); 
    private static final String CLAZZ_NAME="NotesFlowBean";
    private static KLXLogger logger = KLXLogger.getLogger();    


    public void setOpportunityId(String opportunityId) {
        this.opportunityId = opportunityId;
    }

    public String getOpportunityId() {
        return opportunityId;
    }

    public void setOpportunityNumber(String opportunityNumber) {
        this.opportunityNumber = opportunityNumber;
    }

    public String getOpportunityNumber() {
        return opportunityNumber;
    }

    public void setNotesDesc(String notesDesc) {
        this.notesDesc = notesDesc;
    }

    public String getNotesDesc() {
        return notesDesc;
    }

    public void addOpportunityNotes(ActionEvent actionEvent) {
        // Add event code here...
        logger.log(Level.INFO, getClass(),
                                            "addOpportunityNotes",
                                            "Entering addOpportunityNotes method");
        BindingContainer bindings = BindingContext.getCurrent().getCurrentBindingsEntry();
        JUCtrlListBinding listBinding = (JUCtrlListBinding)bindings.get("NoteType");
        String noteType= (String) listBinding.getInputValue(); 
      
        
//        OperationBinding op = (OperationBinding) BindingContext.getCurrent().getCurrentBindingsEntry().getOperationBinding("addNotes");
//        op.getParamsMap().put("opportunityNo", getOpportunityNumber()); 
//        op.getParamsMap().put("noteComments", getNotesDesc());
//        op.getParamsMap().put("noteLevel", "OPPORTUNITY");
//        op.getParamsMap().put("noteType", noteType);
//        op.execute();
        
        oracle.binding.OperationBinding binding = ADFUtils.findOperationBinding("addNotesData");
        binding.getParamsMap().put("noteType", noteType);
        binding.getParamsMap().put("notesDesc", getNotesDesc());
        binding.getParamsMap().put("crfNo", getOpportunityNumber());
        binding.execute();
        
        setNotesDesc(null);
        refreshNotes();
            
    }
    
    public void refreshNotes(){
        logger.log(Level.INFO, getClass(),
                                            "refreshNotes",
                                            "Entering refreshNotes method");

        DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter1 = binds.findIteratorBinding("CctNotesUpdateOppVOIterator");
            iter1.executeQuery();
    }

    public DCBindingContainer getDCBindingsContainer() {
        DCBindingContainer bindingsContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        return bindingsContainer;
    }
    
    public void searchNotesTypeFilter(ActionEvent actionEvent) {
        // Add event code here...
       // String crfNo = (JSFUtils.resolveExpression("#{bindings.CrfNo.inputValue}")).toString();
        
        BindingContainer binding = BindingContext.getCurrent().getCurrentBindingsEntry();
        JUCtrlListBinding listBinding = (JUCtrlListBinding)binding.get("b_noteType");
        String noteType= (String) listBinding.getInputValue(); 
       
        
        DCBindingContainer bindings = this.getDCBindingsContainer();
        DCIteratorBinding opportunityNotesVOIterator =
            bindings.findIteratorBinding("CctNotesUpdateOppVOIterator");
        ViewObject opportunityNotesVO = opportunityNotesVOIterator.getViewObject();
        
        ViewCriteriaManager vcm = opportunityNotesVO.getViewCriteriaManager();
        ViewCriteria NotesVOSearchCriteria = vcm.getViewCriteria("CctNotesUpdateOppSearchCriteria");
        VariableValueManager vvm = NotesVOSearchCriteria.ensureVariableManager();
        vvm.setVariableValue("b_noteType", noteType);
        vvm.setVariableValue("p_crfNo", getOpportunityNumber());
        opportunityNotesVO.applyViewCriteria(NotesVOSearchCriteria);
        opportunityNotesVO.executeQuery();
       
        refreshNotes();
    }
}
