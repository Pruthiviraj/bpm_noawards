package com.klx.view.beans.scope.pageFlow;

import com.klx.cct.ad.user.v1.Roles;
import com.klx.common.cache.ParamCache;
import com.klx.cct.ad.user.v1.User;
import com.klx.common.access.UserActionAccessManagement;
import com.klx.common.logger.KLXLogger;
import com.klx.model.impl.ActiveDirectoryManagerImpl;
import com.klx.services.entities.Quote;
import com.klx.services.entities.Tasks;
import com.klx.services.entities.Users;
import com.klx.view.AdditionalCost;
import com.klx.view.beans.constants.ViewConstants;
import com.klx.view.beans.utils.ADFUtils;
import com.klx.view.beans.utils.JSFUtils;
import com.klx.view.error.CustomErrorHandler;
import javax.faces.application.ViewHandler;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.layout.RichPanelTabbed;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.QueryEvent;

import oracle.binding.BindingContainer;

import oracle.jbo.Row;

import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;

import org.apache.myfaces.trinidad.event.DisclosureEvent;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class opportunityFlowBean implements Serializable {


    private RichPopup successPopUp;
    private RichPopup openTaskUpdatePopUp;
    private RichPopup assignmentPopUp;
    private RichShowDetailItem bind_Lighting;
    private RichShowDetailItem bind_AdvancedSourcing;
    private HashMap queueNames = new HashMap();
    private Users loggedInUserDetails;
    private String opportunityNumber;
    private String opportunityId;
    private List<String> groupsSelectedForList;
    private List<SelectItem> groupsCheckBoxList;
    private String popupSuccessMessage;
    private String selectedTaskNumber;
    private String selectedTaskId;
    private String assignmentPopUpMessage;
    private List hppPartsList = new ArrayList();
    private List chemicalPartsList = new ArrayList();
    private Quote quote;
    private String noteDesc;
    private String oscLink;
    private String oscOppId;
    private String selectedGroup;
    private String customerName;
    private String customerNumber;
    private Date customerDueDate;
    private Date creationDate;
    private Date completionDate;
    private String dealStructure;
    private static KLXLogger logger = KLXLogger.getLogger();
    private RichPanelTabbed panelTabbedBind;
    private boolean submitSales = false;
    private String forRefresh;
    private String completedBy;

    private String assignedTo;
    private String groupName;
    private String taskDescription;
    private String assigneeId;
    private String projectType;
    private String customerNoCrf;
    private Tasks fetchedOpportunityTask;
    private List<Quote> fetchedQuoteList;
    private RichTable myTasksTable;
    private String crfNumber;
    private boolean approvalTaskTab;
    private String partsPricingFlag;
    private boolean salesHierarchyHidden;  
    
    private boolean createOpportunity;      
    private boolean taskDashboardAssignees;
    private RichShowDetailItem bind_additionalDetailItem;
    private RichPopup additionCostPopUp;
    private RichShowDetailItem bind_ProgramCost;
    private RichPopup programCostPopUp;
    private BigDecimal totalProgramCost = null;
    private RichShowDetailItem bind_FinalPricing;
    private RichShowDetailItem bind_attachmentTab;
    private RichShowDetailItem bind_awardsTab;
    private RichShowDetailItem bind_approvalTab;
    private RichShowDetailItem bind_CostComparisonTab;
    private RichShowDetailItem bind_notesTab;
    private RichShowDetailItem bind_taskTab;
    private RichShowDetailItem bind_partsTab;

    public void setTotalProgramCost(BigDecimal totalProgramCost) {
        this.totalProgramCost = totalProgramCost;
    }

    public BigDecimal getTotalProgramCost() {
        return totalProgramCost;
    }


    public void setPartsPricingFlag(String partsPricingFlag) {
        this.partsPricingFlag = partsPricingFlag;
    }

    public String getPartsPricingFlag() {
        return partsPricingFlag;
    }

    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }

    public String getProjectType() {
        return projectType;
    }

    public void setCustomerNoCrf(String customerNoCrf) {
        this.customerNoCrf = customerNoCrf;
    }

    public String getCustomerNoCrf() {
        return customerNoCrf;
    }

    public void setCrfNumber(String crfNumber) {
        this.crfNumber = crfNumber;
    }

    public String getCrfNumber() {
       
        return crfNumber;
    }


    public void setFetchedQuoteList(List<Quote> fetchedQuoteList) {
        this.fetchedQuoteList = fetchedQuoteList;
    }

    public List<Quote> getFetchedQuoteList() {
        return fetchedQuoteList;
    }

    public void setFetchedOpportunityTask(Tasks fetchedOpportunityTask) {
        this.fetchedOpportunityTask = fetchedOpportunityTask;
    }

    public Tasks getFetchedOpportunityTask() {
        return fetchedOpportunityTask;
    }

    public void setDealStructure(String dealStructure) {
        this.dealStructure = dealStructure;
    }

    public String getDealStructure() {
        return dealStructure;
    }
    private static ADFLogger _logger = ADFLogger.createADFLogger(opportunityFlowBean.class);
    private static final String CLAZZ_NAME = "opportunityFlowBean";


    public void setOpportunityId(String opportunityId) {
        this.opportunityId = opportunityId;
    }

    public String getOpportunityId() {
        return opportunityId;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerDueDate(Date customerDueDate) {
        this.customerDueDate = customerDueDate;
    }

    public Date getCustomerDueDate() {
        return customerDueDate;
    }

    public void setSelectedGroup(String selectedGroup) {
        this.selectedGroup = selectedGroup;
    }

    public String getSelectedGroup() {
        return selectedGroup;
    }

    public void setNoteDesc(String noteDesc) {
        this.noteDesc = noteDesc;
    }

    public String getNoteDesc() {
        return noteDesc;
    }

    public void setOscOppId(String oscOppId) {
        this.oscOppId = oscOppId;
    }

    public String getOscOppId() {
        return oscOppId;
    }

    public void setOscLink(String oscLink) {
        this.oscLink = oscLink;
    }

    public String getOscLink() {
        //  String osc="https://cavm-test.crm.us2.oraclecloud.com/customer/faces/CrmFusionHome?cardToOpen=MOO_OPPTYMGMTOPPORTUNITIES_CRM_CARD&tabToOpen=MOO_OPPTYMGMTOPPORTUNITIES_CRM&TF_skipToEditOptyId="+oscOppId;
        String osc = ParamCache.propertiesMap
                               .get(ViewConstants.OSC_URL)
                               .toString() + oscOppId;
        return osc;
    }


    public opportunityFlowBean() {
        super();
        userActionsAccess();
    }

    public void userActionsAccess() {
        HashMap actions = new HashMap();
        actions.put(ViewConstants.SUBMIT_SALES, false);
        actions.put(ViewConstants.APPROVAL_TASK_TAB, false);
        actions.put(ViewConstants.CREATE_OPPORTUNITY, false);
        actions.put(ViewConstants.TASKDASHBOARD_ASSIGNEES, false);      
        HashMap userAcess = UserActionAccessManagement.getUserAccess(ADFContext.getCurrent()
                                                                               .getSecurityContext()
                                                                               .getUserRoles(), actions);
        submitSales = ((Boolean) userAcess.get(ViewConstants.SUBMIT_SALES)).booleanValue();
        approvalTaskTab = ((Boolean)userAcess.get(ViewConstants.APPROVAL_TASK_TAB)).booleanValue();               
        createOpportunity = ((Boolean)userAcess.get(ViewConstants.CREATE_OPPORTUNITY)).booleanValue();     
        taskDashboardAssignees = ((Boolean)userAcess.get(ViewConstants.TASKDASHBOARD_ASSIGNEES)).booleanValue();
       
    }

    public void setChemicalPartsList(List chemicalPartsList) {
        this.chemicalPartsList = chemicalPartsList;
    }

    public List getChemicalPartsList() {
        return chemicalPartsList;
    }
    private String taskComments;

    public void setAssignmentPopUpMessage(String assignmentPopUpMessage) {
        this.assignmentPopUpMessage = assignmentPopUpMessage;
    }

    public String getAssignmentPopUpMessage() {
        return assignmentPopUpMessage;
    }

    public void setLoggedInUserDetails(Users loggedInUserDetails) {
        this.loggedInUserDetails = loggedInUserDetails;
    }

    public Users getLoggedInUserDetails() {
        return loggedInUserDetails;
    }

    public void setSelectedTaskId(String selectedTaskId) {
        this.selectedTaskId = selectedTaskId;
    }

    public String getSelectedTaskId() {
        return selectedTaskId;
    }

    public void setTaskComments(String taskComments) {
        this.taskComments = taskComments;
    }

    public String getTaskComments() {
        return taskComments;
    }

    public void setSelectedTaskNumber(String selectedTaskNumber) {
        this.selectedTaskNumber = selectedTaskNumber;
    }

    public String getSelectedTaskNumber() {
        return selectedTaskNumber;
    }

    public void setPopupSuccessMessage(String popupSuccessMessage) {
        this.popupSuccessMessage = popupSuccessMessage;
    }

    public String getPopupSuccessMessage() {
        return popupSuccessMessage;
    }

    public void setGroupsCheckBoxList(List<SelectItem> groupsCheckBoxList) {
        this.groupsCheckBoxList = groupsCheckBoxList;
    }

    public void setQuote(Quote quote) {
        this.quote = quote;
    }

    public Quote getQuote() {
        return quote;
    }

    public void setGroupsSelectedForList(List groupsSelectedForList) {
        this.groupsSelectedForList = groupsSelectedForList;
    }

    public List getGroupsSelectedForList() {
        return groupsSelectedForList;
    }

    public void setOpportunityNumber(String opportunityNumber) {
        this.opportunityNumber = opportunityNumber;
    }

    public String getOpportunityNumber() {
        return opportunityNumber;
    }


    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public String setOpportunityDetailsBindVar() {
        try {
            if (!ADFContext.getCurrent()
                           .getSessionScope()
                           .get(ViewConstants.SOURCE)
                           .equals(ViewConstants.BPM))
                ADFContext.getCurrent()
                          .getSessionScope()
                          .put(ViewConstants.SOURCE, ViewConstants.BPM);
                ADFContext.getCurrent()
                                  .getSessionScope()
                                  .put(ViewConstants.ISAPPROVALTHROUGHEMAIL,false);
            logger.log(Level.INFO, getClass(), "setOpportunityDetailsBindVar",
                       "Entering setOpportunityDetailsBindVar method");

            OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                                   .getCurrentBindingsEntry()
                                                                   .getOperationBinding("fetchOpportunityDetails");
            op.getParamsMap().put("opportunityNumber", getOpportunityNumber());
            op.getParamsMap().put("opportunityId", getOpportunityId());
            List<Quote> quoteList = (List<Quote>) op.execute();
            if (null != quoteList) {
                if (quoteList.size() > 0)
                    this.quote = quoteList.get(0);
            }
            try {
                if (null != getPartsPricingFlag() && "partsPricing".equalsIgnoreCase(getPartsPricingFlag())) {
                    DCBindingContainer bindings =
                        (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
                    DCIteratorBinding itorBinding = bindings.findIteratorBinding("OpportunityVO1Iterator");
                    ViewObject vo = itorBinding.getViewObject();
                    if (null!= vo.getCurrentRow() && null != vo.getCurrentRow().getAttribute("DealStructure")) {
                        setDealStructure(vo.getCurrentRow()
                                           .getAttribute("DealStructure")
                                           .toString());
                    } else
                        setDealStructure(null);
                }
                oracle.adf.model.OperationBinding operationBinding =
                   (oracle.adf.model.OperationBinding) ADFUtils.findOperationBinding("getTop50FinalPricingSummary");
                operationBinding.getParamsMap().put("opportunityNumber", getOpportunityNumber());
                operationBinding.execute();
            } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
            }

        } catch (Exception e) {
            // TODO: Add catch code
            //   e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "setOpportunityDetailsBindVar", e.getMessage());
            CustomErrorHandler.processError("CCT", "setOpportunityDetailsBindVar", "CCT_SYS_EX33", e);
        }
        logger.log(Level.INFO, getClass(), "setOpportunityDetailsBindVar",
                   "Exiting setOpportunityDetailsBindVar method");

        return "details";

    }


    public String openTaskUpdate() {
        logger.log(Level.INFO, getClass(), "openTaskUpdate", "Entering openTaskUpdate method");

        OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                               .getCurrentBindingsEntry()
                                                               .getOperationBinding("setTaskNotesBindVar");
        op.getParamsMap().put("taskNumber", getSelectedTaskNumber());
        op.execute();
        return "";
    }


    public void setSuccessPopUp(RichPopup successPopUp) {
        this.successPopUp = successPopUp;
    }

    public RichPopup getSuccessPopUp() {
        return successPopUp;
    }


    public void setOpenTaskUpdatePopUp(RichPopup openTaskUpdatePopUp) {
        this.openTaskUpdatePopUp = openTaskUpdatePopUp;
    }

    public RichPopup getOpenTaskUpdatePopUp() {
        return openTaskUpdatePopUp;
    }

    public void insertTaskComments(ActionEvent actionEvent) {
        // Add event code here...
        logger.log(Level.INFO, getClass(), "insertTaskComments", "Entering insertTaskComments method");
        try {
            OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                                   .getCurrentBindingsEntry()
                                                                   .getOperationBinding("insertTaskComments");
            op.getParamsMap().put("taskNumber", getSelectedTaskNumber());
            op.getParamsMap().put("taskId", getSelectedTaskId());
            op.getParamsMap().put("comment", getTaskComments());
            op.getParamsMap().put("username", getTaskComments());
            op.execute();
        } catch (Exception ae) {
            logger.log(Level.SEVERE, getClass(), "insertTaskComments", ae.getMessage());
            CustomErrorHandler.processError("CCT", "insertTaskComments", "CCT_SYS_EX06", ae);
        }

    }

    public void closeTaskListener(ActionEvent actionEvent) {
        // Add event code here...
        try {
            logger.log(Level.INFO, getClass(), "closeTaskListener", "Entering closeTaskListener method");

            OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                                   .getCurrentBindingsEntry()
                                                                   .getOperationBinding("closeTask");

            op.getParamsMap().put("taskId", getSelectedTaskId());
            op.getParamsMap().put("outcome", "CLOSE");
            op.execute();
            OperationBinding op3 = (OperationBinding) BindingContext.getCurrent()
                                                                    .getCurrentBindingsEntry()
                                                                    .getOperationBinding("loadTasksList");
            op3.execute();

            refreshListIterators();

            getOpenTaskUpdatePopUp().hide();
        } catch (Exception e) {
            // TODO: Add catch code
            //   e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "closeTaskListener", e.getMessage());
            CustomErrorHandler.processError("CCT", "closeTaskListener", "CCT_BUS_EX03", e);
        }
        logger.log(Level.INFO, getClass(), "closeTaskListener", "Exiting closeTaskListener method");


    }

    public void reassignUserMyList(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        try {
            logger.log(Level.INFO, getClass(), "reassignUserMyList", "Entering reassignUserMyList method");

            String username = (String) valueChangeEvent.getNewValue();
            String oldUser = (String) valueChangeEvent.getOldValue();
            String userName = "";
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding tasksBinding = bindings.findIteratorBinding("myTasksListIterator");

            Row selectedRow = tasksBinding.getCurrentRow();
            String taskId = selectedRow.getAttribute("taskId").toString();
            //String oscOppId = selectedRow.getAttribute("oscOppId").toString();

            OperationBinding op1 = (OperationBinding) BindingContext.getCurrent()
                                                                    .getCurrentBindingsEntry()
                                                                    .getOperationBinding("reassignTask");
            op1.getParamsMap().put("taskId", taskId);
            op1.getParamsMap().put("reassignedUserId", username);
            op1.execute();
            
//            OperationBinding op1 = (OperationBinding) BindingContext.getCurrent()
//                                                                    .getCurrentBindingsEntry()
//                                                                    .getOperationBinding("releaseTask");
//            op1.getParamsMap().put("taskId", taskId);
//            op1.getParamsMap().put("username", oldUser);
//            op1.execute();
//
//            OperationBinding op = (OperationBinding) BindingContext.getCurrent()
//                                                                   .getCurrentBindingsEntry()
//                                                                   .getOperationBinding("assignTaskToUser");
//            op.getParamsMap().put("taskId", taskId);
//            op.getParamsMap().put("username", username);
//            op.execute();

            OperationBinding usersOperation =
                (OperationBinding) BindingContext.getCurrent()
                                                                               .getCurrentBindingsEntry()
                                                                               .getOperationBinding("fetchUserDetailsById");
            usersOperation.getParamsMap().put("userId", username);
            User user = (User) usersOperation.execute();

            logger.log(Level.INFO, getClass(), "reassignUserMyList", "usersList values -" + user.getName());

            setAssignmentPopUpMessage(ViewConstants.TASK_ASSIGNMENT + user.getName());
            OperationBinding op3 = (OperationBinding) BindingContext.getCurrent()
                                                                    .getCurrentBindingsEntry()
                                                                    .getOperationBinding("loadTasksList");
            op3.execute();

            /* OperationBinding op4 = (OperationBinding) BindingContext.getCurrent()
                                                                    .getCurrentBindingsEntry()
                                                                    .getOperationBinding("updateOpportunityResource");
            op4.getParamsMap().put("userId", username);
            op4.getParamsMap().put("oscOppId", oscOppId);
            op4.getParamsMap().put("role", user.getRoles().getRole().get(0));
            op4.execute();*/


            refreshListIterators();


            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            getAssignmentPopUp().show(hints);
        } catch (Exception e) {
            // TODO: Add catch code
            //  e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "reassignUserMyList", e.getMessage());
            CustomErrorHandler.processError("CCT", "reassignUserMyList", "CCT_BUS_EX04", e);
        }
        logger.log(Level.INFO, getClass(), "reassignUserMyList", "Exiting reassignUserMyList method");

    }

    public void reassignUserAssignList(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        try {
            logger.log(Level.INFO, getClass(), "reassignUserAssignList", "Entering reassignUserAssignList method");

            String username = (String) valueChangeEvent.getNewValue();
            String userName = "";
            String oldUser = (String) valueChangeEvent.getOldValue();
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding tasksBinding = bindings.findIteratorBinding("assignedTaskListIterator");

            Row selectedRow = tasksBinding.getCurrentRow();
            String taskId = selectedRow.getAttribute("taskId").toString();
            //String oscOppId = selectedRow.getAttribute("oscOppId").toString();
            OperationBinding op1 = (OperationBinding) BindingContext.getCurrent()
                                                                    .getCurrentBindingsEntry()
                                                                    .getOperationBinding("reassignTask");
            op1.getParamsMap().put("taskId", taskId);
            op1.getParamsMap().put("reassignedUserId", username);
            op1.execute();
//            OperationBinding op1 = (OperationBinding) BindingContext.getCurrent()
//                                                                    .getCurrentBindingsEntry()
//                                                                    .getOperationBinding("releaseTask");
//            op1.getParamsMap().put("taskId", taskId);
//            op1.getParamsMap().put("username", oldUser);
//            op1.execute();
//
//            OperationBinding op = (OperationBinding) BindingContext.getCurrent()
//                                                                   .getCurrentBindingsEntry()
//                                                                   .getOperationBinding("assignTaskToUser");
//
//            op.getParamsMap().put("taskId", taskId);
//            op.getParamsMap().put("username", username);
//            op.execute();

            OperationBinding usersOperation =
                (OperationBinding) BindingContext.getCurrent()
                                                                               .getCurrentBindingsEntry()
                                                                               .getOperationBinding("fetchUserDetailsById");
            usersOperation.getParamsMap().put("userId", username);
            User user = (User) usersOperation.execute();

            setAssignmentPopUpMessage(ViewConstants.TASK_ASSIGNMENT + user.getName());
            OperationBinding op3 = (OperationBinding) BindingContext.getCurrent()
                                                                    .getCurrentBindingsEntry()
                                                                    .getOperationBinding("loadTasksList");
            op3.execute();

            /*OperationBinding op4 = (OperationBinding) BindingContext.getCurrent()
                                                                    .getCurrentBindingsEntry()
                                                                    .getOperationBinding("updateOpportunityResource");
            op4.getParamsMap().put("userId", username);
            op4.getParamsMap().put("oscOppId", oscOppId);
            op4.getParamsMap().put("role", user.getRoles().getRole().get(0));
            op4.execute();*/

            refreshListIterators();
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            getAssignmentPopUp().show(hints);
        } catch (Exception e) {
            // TODO: Add catch code
            //  e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "reassignUserAssignList", e.getMessage());
            CustomErrorHandler.processError("CCT", "reassignUserAssignList", "CCT_BUS_EX04", e);
        }
        logger.log(Level.INFO, getClass(), "reassignUserAssignList", "Exiting reassignUserAssignList method");

    }

    public void assignUserUnassignedList(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        try {
            logger.log(Level.INFO, getClass(), "assignUserUnassignedList", "Entering assignUserUnassignedList method");

            String username = (String) valueChangeEvent.getNewValue();
            String userName = "";
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding tasksBinding = bindings.findIteratorBinding("unassignedTaskListIterator");
            Row selectedRow = tasksBinding.getCurrentRow();
            String taskId = selectedRow.getAttribute("taskId").toString();
            //String oscOppId = selectedRow.getAttribute("oscOppId").toString();
            //        String taskNo = selectedRow.getAttribute("taskNumber").toString();
            OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                                   .getCurrentBindingsEntry()
                                                                   .getOperationBinding("assignTaskToUser");

            op.getParamsMap().put("taskId", taskId);
            op.getParamsMap().put("username", username);
            op.execute();

            OperationBinding usersOperation =
                (OperationBinding) BindingContext.getCurrent()
                                                                               .getCurrentBindingsEntry()
                                                                               .getOperationBinding("fetchUserDetailsById");
            usersOperation.getParamsMap().put("userId", username);
            User user = (User) usersOperation.execute();
            System.out.println("username assigned ==="+username);
            setAssignmentPopUpMessage(ViewConstants.TASK_ASSIGNMENT + user.getName());

            OperationBinding op3 = (OperationBinding) BindingContext.getCurrent()
                                                                    .getCurrentBindingsEntry()
                                                                    .getOperationBinding("loadTasksList");
            op3.execute();

            /*OperationBinding op4 = (OperationBinding) BindingContext.getCurrent()
                                                                    .getCurrentBindingsEntry()
                                                                    .getOperationBinding("updateOpportunityResource");
            op4.getParamsMap().put("userId", username);
            op4.getParamsMap().put("oscOppId", oscOppId);
            op4.getParamsMap().put("role", user.getRoles().getRole().get(0));
            op4.execute();*/

            refreshListIterators();
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            getAssignmentPopUp().show(hints);
        } catch (Exception e) {
            // TODO: Add catch code
            //     e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "assignUserUnassignedList", e.getMessage());
            CustomErrorHandler.processError("CCT", "assignUserUnassignedList", "CCT_BUS_EX04", e);
        }
        logger.log(Level.INFO, getClass(), "assignUserUnassignedList", "Exiting assignUserUnassignedList method");

    }

    public void setAssignmentPopUp(RichPopup assignmentPopUp) {
        this.assignmentPopUp = assignmentPopUp;
    }

    public RichPopup getAssignmentPopUp() {
        return assignmentPopUp;
    }

    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public void advancedSourcingRecordsListener(DisclosureEvent disclosureEvent) {
        // Add event code here...
        logger.log(Level.INFO, getClass(), "advancedSourcingRecordsListener",
                   "Entering advancedSourcingRecordsListener method");

        if (disclosureEvent.isExpanded()) {
            OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                                   .getCurrentBindingsEntry()
                                                                   .getOperationBinding("getPartsQueues");

            op.getParamsMap().put("QueueName", "Advance Sourcing");
            op.execute();
        }
    }

    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public void chemicalsRecordListener(DisclosureEvent disclosureEvent) {
        // Add event code here...
        logger.log(Level.INFO, getClass(), "chemicalsRecordListener", "Entering chemicalsRecordListener method");

        if (disclosureEvent.isExpanded()) {
            OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                                   .getCurrentBindingsEntry()
                                                                   .getOperationBinding("getPartsQueues");

            op.getParamsMap().put("QueueName", "Chemical");
            op.execute();
        }
    }

    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public void lightingRecordsListener(DisclosureEvent disclosureEvent) {
        // Add event code here...
        logger.log(Level.INFO, getClass(), "lightingRecordsListener", "Entering lightingRecordsListener method");

        if (disclosureEvent.isExpanded()) {
            OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                                   .getCurrentBindingsEntry()
                                                                   .getOperationBinding("getPartsQueues");

            op.getParamsMap().put("QueueName", "Lighting");
            op.execute();
        }
    }

    public void setBind_Lighting(RichShowDetailItem bind_Lighting) {
        this.bind_Lighting = bind_Lighting;
    }

    public RichShowDetailItem getBind_Lighting() {
        return bind_Lighting;
    }

    public void setBind_AdvancedSourcing(RichShowDetailItem bind_AdvancedSourcing) {
        this.bind_AdvancedSourcing = bind_AdvancedSourcing;
    }

    public RichShowDetailItem getBind_AdvancedSourcing() {
        return bind_AdvancedSourcing;
    }

    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public String changeQueueListener() {
        // Add event code here...
        try {
            logger.log(Level.INFO, getClass(), "changeQueueListener", "Entering changeQueueListener method");

            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding queueIterator = bindings.findIteratorBinding("PartsQueueName1Iterator");
            DCIteratorBinding partsIterator = bindings.findIteratorBinding("QuotePartsVO1Iterator");
            Row[] queueRows = queueIterator.getAllRowsInRange();
            Row selectedpartsRow = partsIterator.getCurrentRow();
            Row currentQueue = queueIterator.getCurrentRow();

            OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                                   .getCurrentBindingsEntry()
                                                                   .getOperationBinding("movePartsToQueues");
            BigDecimal queueid = new BigDecimal(ADFContext.getCurrent()
                                                          .getPageFlowScope()
                                                          .get("queue")
                                                          .toString());
            BigDecimal productID = new BigDecimal(selectedpartsRow.getAttribute("ProductId").toString());
            op.getParamsMap().put("queueID", selectedpartsRow.getAttribute("QueueId").toString());
            op.getParamsMap().put("changedqueueID", queueid);
            op.getParamsMap().put("productID", getHppPartsList());
            op.execute();
        } catch (Exception e) {
            // TODO: Add catch code
            //     e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "changeQueueListener", e.getMessage());
            CustomErrorHandler.processError("CCT", "changeQueueListener", "CCT_SYS_EX09", e);
        }
        logger.log(Level.INFO, getClass(), "changeQueueListener", "Exiting changeQueueListener method");

        return null;
    }

    public void setHppPartsList(List hppPartsList) {
        this.hppPartsList = hppPartsList;
    }

    public List getHppPartsList() {
        return hppPartsList;
    }

    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public void hppQuotePartsSelectionListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        logger.log(Level.INFO, getClass(), "hppQuotePartsSelectionListener",
                   "Entering hppQuotePartsSelectionListener method");

        String productid = ADFContext.getCurrent()
                                     .getViewScope()
                                     .get("HPPProductid")
                                     .toString();
        if (valueChangeEvent != null) {
            boolean checked = Boolean.parseBoolean(valueChangeEvent.getNewValue() + "");
            if (checked)
                this.getHppPartsList().add(productid);
            else
                this.getHppPartsList().remove(productid);
        }
    }

    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public void chemicalQuotePartsSelectionListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        logger.log(Level.INFO, getClass(), "chemicalQuotePartsSelectionListener",
                   "Entering chemicalQuotePartsSelectionListener method");

        String productid = ADFContext.getCurrent()
                                     .getViewScope()
                                     .get("chemicalProductid")
                                     .toString();
        if (valueChangeEvent != null) {
            boolean checked = Boolean.parseBoolean(valueChangeEvent.getNewValue() + "");
            if (checked)
                this.getChemicalPartsList().add(productid);
            else
                this.getChemicalPartsList().remove(productid);
        }
    }

    public void setQueueNames(HashMap queueNames) {
        this.queueNames = queueNames;
    }

    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public HashMap getQueueNames() {
        queueNames.put("HPP", "1");
        queueNames.put("Chemical", "2");
        queueNames.put("Lighting", "3");
        queueNames.put("Kitting", "4");
        queueNames.put("Advance Sourcing", "5");
        queueNames.put("No Bid", "7");
        queueNames.put("Strategic Pricing", "8");
        queueNames.put("Commodity", "9");

        return queueNames;
    }

    public void addTaskNotes(ActionEvent actionEvent) {
        // Add event code here...
        logger.log(Level.INFO, getClass(), "addTaskNotes", "Entering addTaskNotes method");
        OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                               .getCurrentBindingsEntry()
                                                               .getOperationBinding("addNotes");
        op.getParamsMap().put("opportunityNo", getOpportunityNumber());
        op.getParamsMap().put("taskNo", getSelectedTaskNumber());
        op.getParamsMap().put("noteComments", getNoteDesc());
        op.getParamsMap().put("noteLevel", "TASK");
        op.execute();
        setNoteDesc(null);
        refreshNotes();
    }

    public void refreshNotes() {
        logger.log(Level.INFO, getClass(), "refreshNotes", "Entering refreshNotes method");
        DCBindingContainer binds = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter1 = binds.findIteratorBinding("TaskNotesVOIterator");
        iter1.executeQuery();
    }

    public void openTaskUpdateListener(ActionEvent actionEvent) {
        // Add event code here...
        try {
            logger.log(Level.INFO, getClass(), "openTaskUpdateListener", "Entering openTaskUpdateListener method");
            OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                                   .getCurrentBindingsEntry()
                                                                   .getOperationBinding("setTaskNotesBindVar");
            op.getParamsMap().put("taskNumber", getSelectedTaskNumber());
            op.execute();
            //TODO
            String opportunityNumber =
                (String) resolveExpression("#{pageFlowScope.opportunityFlowBean.opportunityNumber}");
            OperationBinding getOpportunityDetails =
                (OperationBinding) BindingContext.getCurrent()
                                                                                      .getCurrentBindingsEntry()
                                                                                      .getOperationBinding("getOpportunityDetails");
            getOpportunityDetails.getParamsMap().put("opportunityNumber", opportunityNumber);
            getOpportunityDetails.execute();

            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            Tasks tasks = new Tasks();
            tasks.setOpportunityNumber(opportunityNumber);
            tasks.setCustomerNo(getCustomerNumber());
            tasks.setCustomerName(getCustomerName());
            tasks.setGroup(getSelectedGroup());
            tasks.setDealStructure(getDealStructure());
            tasks.setAssigneeName(getAssignedTo());
            tasks.setTaskNumber(getSelectedTaskNumber());
            tasks.setTaskDescription(getTaskDescription());
            tasks.setCreationDate(getCreationDate());
            tasks.setTaskId(getSelectedTaskId());
            tasks.setTaskCompletionDate(getCompletionDate());
            tasks.setTaskCompletedBy(getCompletedBy());
            tasks.setAssignee(getAssigneeId());
            tasks.setProjectType(getProjectType());
            tasks.setCustomerNoCrf(getCustomerNoCrf());
            if(null != ADFContext.getCurrent().getPageFlowScope().get("approvalStatus"))
                tasks.setOutcome(ADFContext.getCurrent().getPageFlowScope().get("approvalStatus").toString());
            if(null != ADFContext.getCurrent().getPageFlowScope().get("approvalComments"))
                tasks.setApprovalComments(ADFContext.getCurrent().getPageFlowScope().get("approvalComments").toString());
            if(null != ADFContext.getCurrent().getPageFlowScope().get("taskType"))
                tasks.setTaskType(ADFContext.getCurrent().getPageFlowScope().get("taskType").toString());
            JSFUtils.setExpressionValue("#{pageFlowScope.currentTask}", tasks);
            getOpenTaskUpdatePopUp().show(hints);
            AdfFacesContext.getCurrentInstance().addPartialTarget(getOpenTaskUpdatePopUp());
        } catch (Exception e) {
            // TODO: Add catch code
            //   e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "openTaskUpdateListener", e.getMessage());
            CustomErrorHandler.processError("CCT", "openTaskUpdateListener", "CCT_SYS_EX05", e);
        }
        logger.log(Level.INFO, getClass(), "openTaskUpdateListener", "Exiting openTaskUpdateListener method");
    }


    public void refreshListIterators() {
        try {
            DCBindingContainer binds = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter1 = binds.findIteratorBinding("myTasksListIterator");
            iter1.invalidateCache();
            iter1.executeQuery();
            DCIteratorBinding iter11 = binds.findIteratorBinding("assigneeListIterator");
            iter11.executeQuery();
            DCIteratorBinding iter2 = binds.findIteratorBinding("assignedTaskListIterator");
            iter2.invalidateCache();
            iter2.executeQuery();
            DCIteratorBinding iter21 = binds.findIteratorBinding("assigneeListIterator1");
            iter21.executeQuery();
            DCIteratorBinding iter3 = binds.findIteratorBinding("unassignedTaskListIterator");
            iter3.invalidateCache();
            iter3.executeQuery();
            DCIteratorBinding iter31 = binds.findIteratorBinding("assigneeListIterator2");
            iter31.executeQuery();
			DCIteratorBinding iter4 = binds.findIteratorBinding("approvalTaskListIterator");
            iter4.invalidateCache();
            iter4.executeQuery();
            DCIteratorBinding iter41 = binds.findIteratorBinding("assigneeListIterator3");
            iter41.invalidateCache();
            iter41.executeQuery();
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
    }

    public void assignmentPopUpClose(ActionEvent actionEvent) {
        getAssignmentPopUp().hide();
    }

    public Object resolveExpression(String expression) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Application app = facesContext.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        ValueExpression valueExp = elFactory.createValueExpression(elContext, expression, Object.class);
        return valueExp.getValue(elContext);
    }

    public void closeNotesListener(ActionEvent actionEvent) {
        refreshListIterators();
        AdfFacesContext.getCurrentInstance().addPartialTarget(getPanelTabbedBind());
        getOpenTaskUpdatePopUp().hide();
    }

    public void taskDueDateVCL(ValueChangeEvent valueChangeEvent) {
        logger.log(Level.INFO, getClass(), "taskDueDateVCL", "Entering taskDueDateVCL method");
        if (null != valueChangeEvent.getNewValue()) {
            DCIteratorBinding binding = ADFUtils.findIterator("myTasksListIterator");
            Row currentRow = binding.getCurrentRow();

            OperationBinding operationBinding = (OperationBinding) ADFUtils.findOperationBinding("updateTaskDueDate");
            operationBinding.getParamsMap().put("taskId", currentRow.getAttribute("taskId"));
            operationBinding.getParamsMap().put("newDueDate", valueChangeEvent.getNewValue());
            operationBinding.execute();
        }
    }


    private RichPopup submitToSales;
    private String popUpMsg = ViewConstants.SAVED_POPUP;

    public void submitOSC(ActionEvent actionEvent) {
        try {
            logger.log(Level.INFO, getClass(), "submitOSC", "Entering submitOSC method");

            BigDecimal opportunityId = (BigDecimal) JSFUtils.resolveExpression("#{bindings.OpportunityId.inputValue}");
            OperationBinding binding = (OperationBinding) ADFUtils.findOperationBinding("submitOSC");
            binding.getParamsMap().put("oppID", opportunityId);
            //       binding.execute();

            String status = (String) binding.execute();
            if ("success".equalsIgnoreCase(status))
                setPopUpMsg(ViewConstants.QUOTE_SUBMIT_SUCCESS);
            else
                setPopUpMsg(ViewConstants.QUOTE_SUBMIT_FAIL);

            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            getSubmitToSales().show(hints);
        } catch (Exception e) {
            // TODO: Add catch code
            //    e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "submitOSC", e.getMessage());
            CustomErrorHandler.processError("CCT", "submitOSC", "CCT_SYS_EX05", e);
        }
        logger.log(Level.INFO, getClass(), "submitOSC", "Exiting submitOSC method");

    }

    public void setSubmitToSales(RichPopup submitToSales) {
        this.submitToSales = submitToSales;
    }

    public RichPopup getSubmitToSales() {
        return submitToSales;
    }

    public void setPopUpMsg(String popUpMsg) {
        this.popUpMsg = popUpMsg;
    }

    public String getPopUpMsg() {
        return popUpMsg;
    }

    public void closePopUp(ActionEvent actionEvent) {
        // Add event code here...
        getSubmitToSales().hide();
    }


    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public void programCostListener(DisclosureEvent disclosureEvent) {
        // Add event code here...
        if(disclosureEvent.isExpanded()){
            logger.log(Level.INFO, getClass(), "programCostListener", "Entering programCostListener method");
            try{
                OperationBinding operationBinding = (OperationBinding) ADFUtils.findOperationBinding("programHeaderCostData");
                operationBinding.getParamsMap().put("opportunityId", getOpportunityId());
                operationBinding.execute();
            }
            catch(oracle.jbo.RowInconsistentException ae){
                if (ae.getErrorCode().equals("25014")) {        
                    RichPopup.PopupHints hints = new RichPopup.PopupHints();
                    getProgramCostPopUp().show(hints);
                    setDisclosedFirstTab(getBind_additionalDetailItem());
                } else
                   throw ae;
            }
        }
    }

    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public void getCostComparisonDetails(DisclosureEvent disclosureEvent) {
        if(disclosureEvent.isExpanded()){
            logger.log(Level.INFO, getClass(), "OpportunityFlowBean", "Entering getCostComparisondetails method");
    
    
            OperationBinding operationBinding =
                (OperationBinding) ADFUtils.findOperationBinding("fetchCostComparisonDetails");
            operationBinding.getParamsMap().put("opportunityNumber", getOpportunityNumber());
            operationBinding.execute();
        }

    }


    public void awardsTabDataRefresh(DisclosureEvent disclosureEvent) {
        // Add event code here...
        if(disclosureEvent.isExpanded()){
            Object opptyid = JSFUtils.resolveExpression("#{bindings.OpportunityId.inputValue}");
            OperationBinding operationBinding = (OperationBinding) ADFUtils.findOperationBinding("awardsTabRefresh");
            operationBinding.getParamsMap().put("opptyId", (String) opptyid.toString());
            operationBinding.execute();
        }
    }

    public void setPanelTabbedBind(RichPanelTabbed panelTabbedBind) {
        this.panelTabbedBind = panelTabbedBind;
    }

    public RichPanelTabbed getPanelTabbedBind() {
        return panelTabbedBind;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public String setOpportunityParameters() {
        String oppNo = (String) ADFContext.getCurrent()
                                          .getSessionScope()
                                          .get(ViewConstants.OPPORTUNITY_NUMBER);
        setOpportunityNumber(oppNo);
        Integer oppId = (Integer) ADFContext.getCurrent()
                                            .getSessionScope()
                                            .get(ViewConstants.OPPORTUNITY_ID);
        setOpportunityId(Integer.toString(oppId));
        Tasks task = getFetchedOpportunityTask();
        setOscOppId(task.getOscOppId());
        setCustomerName(task.getCustomerName());
        setCustomerNumber(task.getCustomerNo());
        setCustomerDueDate(task.getCustomerDueDate());
        setDealStructure(task.getDealStructure());

        return "oppDetails";
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void closeNotesAction(ActionEvent actionEvent) {
        //refreshListIterators();
        //        AdfFacesContext.getCurrentInstance().addPartialTarget(getPanelTabbedBind());
        getOpenTaskUpdatePopUp().hide();
    }

    public void setQuoteDteails() {
        if (fetchedQuoteList.size() > 0) {
            setQuote(fetchedQuoteList.get(0));
        }
    }


    public void refreshQuoteTab(DisclosureEvent disclosureEvent) {
        DCBindingContainer binds = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter1 = binds.findIteratorBinding("QuotePricingVVO1Iterator");
        iter1.executeQuery();
        DCIteratorBinding iter2 = binds.findIteratorBinding("QuoteClassVVO1Iterator");
        iter2.executeQuery();

    }

    public void setSubmitSales(boolean submitSales) {
        this.submitSales = submitSales;
    }

    public boolean isSubmitSales() {
        return submitSales;
    }

    public void setForRefresh(String forRefresh) {
        this.forRefresh = forRefresh;
    }

    public String getForRefresh() {
        String expression = (String) JSFUtils.resolveExpression("#{pageFlowScope.refreshFlag}");
        Boolean salesUser = (Boolean) ADFContext.getCurrent()
                                                .getSessionScope()
                                                .get(ViewConstants.SALES_USER);      
        
        String returnNavigation = "";
        String dashboard =(String) JSFUtils.resolveExpression("#{pageFlowScope.opportunityDetails}");
        
        if ("dashboard".equals(dashboard) && salesUser ==true || null == dashboard && salesUser ==true ){
            
            returnNavigation = "opportunityDashboard";
        }
            else {
                returnNavigation = (String) JSFUtils.resolveExpression("#{pageFlowScope.returnNavigation}");
                
            }
        if (null != expression && "true".equalsIgnoreCase(expression)) {
            BindingContext.getCurrent()
                          .getCurrentBindingsEntry()
                          .getOperationBinding("loadTasksList")
                          .execute();
            DCBindingContainer taskDashboardBC = getBindingsContainerOfOtherPage("com_klx_view_taskDashboardPageDef");
            DCIteratorBinding approvalTasksIterator = taskDashboardBC.findIteratorBinding("approvalTaskListIterator");
            approvalTasksIterator.invalidateCache();
            approvalTasksIterator.executeQuery();
            DCIteratorBinding assignedTasksIterator = taskDashboardBC.findIteratorBinding("assignedTaskListIterator");
            assignedTasksIterator.invalidateCache();
            assignedTasksIterator.executeQuery();
            DCIteratorBinding assigneeIterator1 = taskDashboardBC.findIteratorBinding("assigneeListIterator1");
            assigneeIterator1.invalidateCache();
            assigneeIterator1.executeQuery();
            DCIteratorBinding unassignedTasksIterator =
                taskDashboardBC.findIteratorBinding("unassignedTaskListIterator");
            unassignedTasksIterator.invalidateCache();
            unassignedTasksIterator.executeQuery();
            DCIteratorBinding assigneeIterator2 = taskDashboardBC.findIteratorBinding("assigneeListIterator2");
            assigneeIterator2.invalidateCache();
            assigneeIterator2.executeQuery();
            DCIteratorBinding myTasksIterator2 = taskDashboardBC.findIteratorBinding("myTasksListIterator");
            myTasksIterator2.invalidateCache();
            myTasksIterator2.executeQuery();
            DCIteratorBinding assigneeIterator = taskDashboardBC.findIteratorBinding("assigneeListIterator");
            assigneeIterator.invalidateCache();
            assigneeIterator.executeQuery();
            DCIteratorBinding approvalTasksIterator2 = taskDashboardBC.findIteratorBinding("approvalTaskListIterator");
            approvalTasksIterator2.invalidateCache();
            approvalTasksIterator2.executeQuery();
            DCIteratorBinding assigneeIterator3 = taskDashboardBC.findIteratorBinding("assigneeListIterator3");
            assigneeIterator3.invalidateCache();
            assigneeIterator3.executeQuery();
            UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
            UIComponent component = viewRoot.findComponent("pt1");

            AdfFacesContext.getCurrentInstance().addPartialTarget(component);

        }
        JSFUtils.setExpressionValue("#{pageFlowScope.refreshFlag}", "false");
        //return forRefresh;
        return returnNavigation;
    }

    private DCBindingContainer getBindingsContainerOfOtherPage(String pageUsageId) {
            return (DCBindingContainer) JSFUtils.resolveExpression("#{data." + pageUsageId + "}");
        }

    public void refreshWorkTabs(DisclosureEvent disclosureEvent) {
        if(disclosureEvent.isExpanded()){
            BindingContext.getCurrent()
                          .getCurrentBindingsEntry()
                          .getOperationBinding("loadTasksList")
                          .execute();
            refreshListIterators();
            AdfFacesContext.getCurrentInstance().addPartialTarget(getPanelTabbedBind());
        }        
    }            

    public void refreshFinalPricingTab(DisclosureEvent disclosureEvent) {
        // Add event code here...
        if(disclosureEvent.isExpanded()){
            DCBindingContainer binds = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter1 = binds.findIteratorBinding("QuotePricingFinalVVO1Iterator");
            iter1.executeQuery();
            DCIteratorBinding iter2 = binds.findIteratorBinding("QuoteClassFinalVVO1Iterator");
            iter2.executeQuery();
            oracle.adf.model.OperationBinding operationBinding =
                (oracle.adf.model.OperationBinding) ADFUtils.findOperationBinding("getTop50FinalPricingSummary");
            operationBinding.getParamsMap().put("opportunityNumber", getOpportunityNumber());
            operationBinding.execute();
        }
    }

    public void setCompletionDate(Date completionDate) {
        this.completionDate = completionDate;
    }

    public Date getCompletionDate() {
        return completionDate;
    }

    public void setCompletedBy(String completedBy) {
        this.completedBy = completedBy;
    }

    public String getCompletedBy() {
        return completedBy;
    }

    public void setAssigneeId(String assigneeId) {
        this.assigneeId = assigneeId;
    }

    public String getAssigneeId() {
        return assigneeId;
    }

    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public void additionalCostListener(DisclosureEvent disclosureEvent) {
        if(disclosureEvent.isExpanded()){
            logger.log(Level.INFO, getClass(), "OpportunityFlowBean", "Entering additionalCostListener method");
            //refreshPage();
            //setDisclosedFirstTab(getBind_additionalDetailItem());
            DCBindingContainer binds = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();            
            try{
                //if(null == ADFContext.getCurrent().getSessionScope().get("savedProgramCost")){
                    DCIteratorBinding iter1 = binds.findIteratorBinding("ProgramCostEOVO1Iterator");
                    ViewObject vo2 = iter1.getViewObject();
                    vo2.reset();            
                    //vo2.setNamedWhereClauseParam("b_opp_id", getOpportunityId());
                    vo2.setWhereClause("OPPORTUNITY_ID = "+getOpportunityId());
                    vo2.executeQuery();
                    RowSetIterator programCostIterator = vo2.createRowSetIterator(null);
                    while(programCostIterator.hasNext()){
                        Row row = programCostIterator.next();                        
                    }
                    programCostIterator.closeRowSetIterator();
                //}
           }catch(oracle.jbo.RowInconsistentException ae){
                //System.out.println("error =====");
                if (ae.getErrorCode().equals("25014")) {  
                    //System.out.println("error =====");                    
                    RichPopup.PopupHints hints = new RichPopup.PopupHints();
                    getAdditionCostPopUp().show(hints);
                    if(ADFContext.getCurrent().getViewScope().get("opportunityDetailsTabSelector").equals("finalPricing"))
                        setDisclosedFirstTab(getBind_FinalPricing());
                    else if(ADFContext.getCurrent().getViewScope().get("opportunityDetailsTabSelector").equals("attachmentsTab"))
                        setDisclosedFirstTab(getBind_attachmentTab());
                    else if(ADFContext.getCurrent().getViewScope().get("opportunityDetailsTabSelector").equals("awardsTab"))
                        setDisclosedFirstTab(getBind_awardsTab());
                    else if(ADFContext.getCurrent().getViewScope().get("opportunityDetailsTabSelector").equals("approvalTab"))
                        setDisclosedFirstTab(getBind_approvalTab());
                    else if(ADFContext.getCurrent().getViewScope().get("opportunityDetailsTabSelector").equals("costComparisonTab"))
                        setDisclosedFirstTab(getBind_CostComparisonTab());
                    else if(ADFContext.getCurrent().getViewScope().get("opportunityDetailsTabSelector").equals("notesTab"))
                        setDisclosedFirstTab(getBind_notesTab());
                    else if(ADFContext.getCurrent().getViewScope().get("opportunityDetailsTabSelector").equals("taskTab"))
                        setDisclosedFirstTab(getBind_taskTab());
                    else if(ADFContext.getCurrent().getViewScope().get("opportunityDetailsTabSelector").equals("partsTab"))
                        setDisclosedFirstTab(getBind_partsTab());
                    else
                        setDisclosedFirstTab(getBind_ProgramCost());
                } else
                   throw ae;
            }
          
           /* OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                                   .getCurrentBindingsEntry()
                                                                   .getOperationBinding("ProgramCostValues");
            op.getParamsMap().put("opportunityId", getOpportunityId());
            op.execute();*/           
    
            DCBindingContainer binds1 = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter2 = binds1.findIteratorBinding("OpportunityVO1Iterator");
            ViewObject vo = iter2.getViewObject();
            vo.setNamedWhereClauseParam("bind_oppNo", getOpportunityNumber());
            vo.executeQuery();
        }
    }
    
    public void setDisclosedFirstTab(RichShowDetailItem tabBind) {
           RichPanelTabbed richPanelTabbed = getPanelTabbedBind();
           for (UIComponent child : richPanelTabbed.getChildren()) {
           RichShowDetailItem sdi = (RichShowDetailItem) child;
           if (sdi.getClientId().equals(tabBind.getClientId()))
               sdi.setDisclosed(true);
           else
               sdi.setDisclosed(false);    
           }
           AdfFacesContext.getCurrentInstance().addPartialTarget(panelTabbedBind);
       }
       
       protected void refreshPage() {
                 FacesContext fc = FacesContext.getCurrentInstance();
                 String refreshpage = fc.getViewRoot().getViewId();
            ViewHandler ViewH = fc.getApplication().getViewHandler();
                 UIViewRoot UIV = ViewH.createView(fc,refreshpage);
                 UIV.setViewId(refreshpage);
                 fc.setViewRoot(UIV);
           }

    public void taskTabRefresh(DisclosureEvent disclosureEvent) {
        // Add event code here...
        if(disclosureEvent.isExpanded()){
            OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                                   .getCurrentBindingsEntry()
                                                                   .getOperationBinding("fetchTasksListByOppNo");
            op.getParamsMap().put("opportunityNumber", getOpportunityNumber());
    
            op.execute();
        }

    }

    public void myTaskCustomFilter(QueryEvent queryEvent) {


        DCBindingContainer binds = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter1 = binds.findIteratorBinding("myTasksListIterator");
        iter1.invalidateCache();
        iter1.executeQuery();
        DCIteratorBinding iter11 = binds.findIteratorBinding("assigneeListIterator");
        iter11.executeQuery();

        invokeEL("#{bindings.myTasksListQuery.processQuery}", new Class[] { QueryEvent.class },
                 new Object[] { queryEvent });
    }

	public void approvalTaskCustomFilter(QueryEvent queryEvent){
      
        
            DCBindingContainer binds = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter1 = binds.findIteratorBinding("approvalTaskListIterator");
            iter1.invalidateCache();
            iter1.executeQuery();
            DCIteratorBinding iter11 = binds.findIteratorBinding("assigneeListIterator3");            
            iter11.executeQuery();
        
        invokeEL("#{bindings.approvalTaskListQuery.processQuery}", new Class[] {QueryEvent.class}, new Object[]{queryEvent});
    }

    public void assignedTaskCustomFilter(QueryEvent queryEvent) {

        DCBindingContainer binds = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter1 = binds.findIteratorBinding("assignedTaskListIterator");
        iter1.invalidateCache();
        iter1.executeQuery();
        DCIteratorBinding iter11 = binds.findIteratorBinding("assigneeListIterator1");
        iter11.executeQuery();

        invokeEL("#{bindings.assignedTaskList1Query.processQuery}", new Class[] { QueryEvent.class },
                 new Object[] { queryEvent });
    }

    public void unassignedTaskCustomFilter(QueryEvent queryEvent) {

        DCBindingContainer binds = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter1 = binds.findIteratorBinding("unassignedTaskListIterator");
        iter1.invalidateCache();
        iter1.executeQuery();
        DCIteratorBinding iter11 = binds.findIteratorBinding("assigneeListIterator2");
        iter11.executeQuery();

        invokeEL("#{bindings.unassignedTaskListQuery.processQuery}", new Class[] { QueryEvent.class },
                 new Object[] { queryEvent });
    }

    public static Object invokeEL(String el, Class[] paramTypes, Object[] params) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ELContext elContext = facesContext.getELContext();
        ExpressionFactory expressionFactory = facesContext.getApplication().getExpressionFactory();
        MethodExpression exp = expressionFactory.createMethodExpression(elContext, el, Object.class, paramTypes);

        return exp.invoke(elContext, params);

    }

    public void approvalDiscListner(DisclosureEvent disclosureEvent) {
        if(disclosureEvent.isExpanded()){
            try {
                OperationBinding fetchSnapshots = (OperationBinding) ADFUtils.findOperationBinding("fetchSnapshots");
                fetchSnapshots.getParamsMap().put("opportunityId", Integer.parseInt(getOpportunityId()));
                fetchSnapshots.getParamsMap().put("opportunityNumber", getOpportunityNumber());
                logger.log(Level.INFO, getClass(), "approvalDiscListner",
                           "Parameters passing to fetchSnapshots method --\n 1)opportunityId=--" +
                           Integer.parseInt(getOpportunityId()) + "-- \n 2)opportunityNumber=--" + getOpportunityNumber() +
                           "--");
                fetchSnapshots.execute();
            } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
            }
        }
    }


    public String setCrfDetails() {
        // Add event code here...
        getCrfNumber();
        if (crfNumber != null) {
            oracle.binding.OperationBinding initializeUpdateOpportunity =
                ADFUtils.getBindingContainer().getOperationBinding("initializeUpdateOpp");
            initializeUpdateOpportunity.getParamsMap().put("crfNumber", this.crfNumber);

            initializeUpdateOpportunity.execute();
        }
        ADFContext.getCurrent().getRequestScope().put("KLXLegalFormat", "visible");
        ADFContext.getCurrent().getPageFlowScope().put("LegalFormat", "inVisible");
        ADFContext.getCurrent().getRequestScope().put("KLXReturnPart", "visible");
        ADFContext.getCurrent().getPageFlowScope().put("ReturnPart", "inVisible");
        return "toUpdateOppty";

    }

    public void refreshNotesTab(DisclosureEvent disclosureEvent) {
        // Add event code here...
        if(disclosureEvent.isExpanded()){
            OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                                   .getCurrentBindingsEntry()
                                                                   .getOperationBinding("setOppNotesBindVar");
            op.getParamsMap().put("oppNo", getOpportunityNumber());
            op.execute();
        }
    }
	public void setApprovalTaskTab(boolean approvalTaskTab) {
        this.approvalTaskTab = approvalTaskTab;
    }

    public boolean isApprovalTaskTab() {
        return approvalTaskTab;
    }

    public void setSalesHierarchyHidden(boolean salesHierarchyHidden) {
        this.salesHierarchyHidden = salesHierarchyHidden;
    }

    public boolean isSalesHierarchyHidden() {
        ActiveDirectoryManagerImpl adImpl = new ActiveDirectoryManagerImpl();
        boolean visible=false;
        User user = adImpl.fetchUserDetailsById((String) JSFUtils.resolveExpression("#{securityContext.userName}"));
        if(null!=user && null!=user.getName()){
            Roles roles = user.getRoles();
        ArrayList<String> list = (ArrayList<String>) roles.getRole();
        if(null!=list){ 
         ArrayList<String> userActionArray= (ArrayList<String>)ParamCache.userActionRoleMap.get(ViewConstants.SALES_HIERARCHY_LINK);
         for(String userAction:userActionArray){
         if(list.contains(userAction))
           visible= true;
         }
        }
        else
         visible= false;
        }else
        visible= false;
        
        return visible;
    }

    public void setCreateOpportunity(boolean createOpportunity) {
        this.createOpportunity = createOpportunity;
    }

    public boolean isCreateOpportunity() {
        return createOpportunity;
    }
    
    public void exportMyTasksToExcel(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        String fileName = "MyTasksTemplate.xlsx";

        String filePath = ViewConstants.FILE_PATH + fileName;
        FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);
        DCBindingContainer binds = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = (DCIteratorBinding) binds.get("myTasksListIterator");
                //Create RowSetIterator iterate over viewObject
               // RowSetIterator rsi = iter.getViewObject().createRowSetIterator(null);
               RowSetIterator rsi = iter.getRowSetIterator();
                //Create  Workbook object
                XSSFWorkbook xwb = new XSSFWorkbook();
                //Create Sheet in Workbook
                XSSFSheet sheet = xwb.createSheet("My Tasks");

                //No of total rows+ 1 for array sizing
                int totRows = ((int) iter.getEstimatedRowCount()) + 1;
                //Here 4 is the number of columns
                Object[][] content = new String[totRows][10];
                int column = 10;
                //Set header text in first row of table in PDF
                content[0][0] = "Opportunity";
                content[0][1] = "Task";
                content[0][2] = "Task Due Date";
                content[0][3] = "Customer Due";
        content[0][4] = "Customer#";
        content[0][5] = "Customer";
        content[0][6] = "Project Type";
        content[0][7] = "Contract Type";
        content[0][8] = "Sales team";
        content[0][9] = "Assigned To";

                int i = 1;
                while (rsi.hasNext()) {
                    Row nextRow = rsi.next();
                    for (int j = 0; j < column; j++) {
                        if (j == 0 && nextRow.getAttribute("opportunityNumber") != null) {
                            content[i][j] = nextRow.getAttribute("opportunityNumber").toString();
                        }
                        if (j == 1 && nextRow.getAttribute("taskNumber") != null) {
                            content[i][j] = nextRow.getAttribute("taskNumber").toString();
                        }
                        if (j == 2 && nextRow.getAttribute("taskDueDate") != null) {
                            content[i][j] = nextRow.getAttribute("taskDueDate").toString();
                        }
                        if (j == 3 && nextRow.getAttribute("customerDueDate") != null) {
                            content[i][j] = nextRow.getAttribute("customerDueDate").toString();
                        }
                       
                        if (j == 4 && nextRow.getAttribute("customerNo") != null) {
                            content[i][j] = nextRow.getAttribute("customerNo").toString();
                        }
                        if (j == 5 && nextRow.getAttribute("customerName") != null) {
                            content[i][j] = nextRow.getAttribute("customerName").toString();
                        }
                        if (j == 6 && nextRow.getAttribute("projectType") != null) {
                            content[i][j] = nextRow.getAttribute("projectType").toString();
                        }
                        if (j == 7 && nextRow.getAttribute("contractType") != null) {
                            content[i][j] = nextRow.getAttribute("contractType").toString();
                        }
                        
                        if (j == 8 && nextRow.getAttribute("teamNumber") != null) {
                            content[i][j] = nextRow.getAttribute("teamNumber").toString();
                        }
                        if (j == 9 && nextRow.getAttribute("assigneeName") != null) {
                            content[i][j] = nextRow.getAttribute("assigneeName").toString();
                        }
                    }
                    i++;
                }
                //Close RowSetIterator
                rsi.closeRowSetIterator();
                //Set data in Excel Sheet from Object array
                int rowNum = 0;
                //Iterate over Object array for each row
                for (Object[] datatype : content) {
                    //Creating row in Excel Sheet
                    org.apache.poi.ss.usermodel.Row row = sheet.createRow(rowNum++);
                    //Set data in column of a row
                    int colNum = 0;
                    for (Object field : datatype) {
                        System.out.println(field);
                        Cell cell = row.createCell(colNum++);
                        if (field instanceof String) {
                            cell.setCellValue((String) field);
                        } else if (field instanceof Integer) {
                            cell.setCellValue((Integer) field);
                        }
                    }
                }
                try {
                    FileOutputStream fos = new FileOutputStream(filePath);
                    xwb.write(fos);
                    xwb.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println("Done");

    }

    public void exportMyTasksToExcel(ActionEvent actionEvent) {
        // Add event code here...
        String fileName = "MyTasksTemplate.xlsx";

        String filePath = ViewConstants.FILE_PATH + fileName;
        //FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);
        DCBindingContainer binds = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = (DCIteratorBinding) binds.get("myTasksListIterator");
                //Create RowSetIterator iterate over viewObject
               // RowSetIterator rsi = iter.getViewObject().createRowSetIterator(null);
               RowSetIterator rsi = iter.getRowSetIterator();
                //Create  Workbook object
                XSSFWorkbook xwb = new XSSFWorkbook();
                //Create Sheet in Workbook
                XSSFSheet sheet = xwb.createSheet("My Tasks");

                //No of total rows+ 1 for array sizing
                int totRows = ((int) iter.getEstimatedRowCount()) + 1;
                //Here 4 is the number of columns
                Object[][] content = new String[totRows][10];
                int column = 10;
                //Set header text in first row of table in PDF
                content[0][0] = "Opportunity";
                content[0][1] = "Task";
                content[0][2] = "Task Due Date";
                content[0][3] = "Customer Due";
        content[0][4] = "Customer#";
        content[0][5] = "Customer";
        content[0][6] = "Project Type";
        content[0][7] = "Contract Type";
        content[0][8] = "Sales team";
        content[0][9] = "Assigned To";

                int i = 1;
                while (rsi.hasNext()) {
                    Row nextRow = rsi.next();
                    for (int j = 0; j < column; j++) {
                        if (j == 0 && nextRow.getAttribute("opportunityNumber") != null) {
                            content[i][j] = nextRow.getAttribute("opportunityNumber").toString();
                        }
                        if (j == 1 && nextRow.getAttribute("taskNumber") != null) {
                            content[i][j] = nextRow.getAttribute("taskNumber").toString();
                        }
                        if (j == 2 && nextRow.getAttribute("taskDueDate") != null) {
                            content[i][j] = nextRow.getAttribute("taskDueDate").toString();
                        }
                        if (j == 3 && nextRow.getAttribute("customerDueDate") != null) {
                            content[i][j] = nextRow.getAttribute("customerDueDate").toString();
                        }
                       
                        if (j == 4 && nextRow.getAttribute("customerNo") != null) {
                            content[i][j] = nextRow.getAttribute("customerNo").toString();
                        }
                        if (j == 5 && nextRow.getAttribute("customerName") != null) {
                            content[i][j] = nextRow.getAttribute("customerName").toString();
                        }
                        if (j == 6 && nextRow.getAttribute("projectType") != null) {
                            content[i][j] = nextRow.getAttribute("projectType").toString();
                        }
                        if (j == 7 && nextRow.getAttribute("contractType") != null) {
                            content[i][j] = nextRow.getAttribute("contractType").toString();
                        }
                        
                        if (j == 8 && nextRow.getAttribute("teamNumber") != null) {
                            content[i][j] = nextRow.getAttribute("teamNumber").toString();
                        }
                        if (j == 9 && nextRow.getAttribute("assigneeName") != null) {
                            content[i][j] = nextRow.getAttribute("assigneeName").toString();
                        }
                    }
                    i++;
                }
                //Close RowSetIterator
                rsi.closeRowSetIterator();
                //Set data in Excel Sheet from Object array
                int rowNum = 0;
                //Iterate over Object array for each row
                for (Object[] datatype : content) {
                    //Creating row in Excel Sheet
                    org.apache.poi.ss.usermodel.Row row = sheet.createRow(rowNum++);
                    //Set data in column of a row
                    int colNum = 0;
                    for (Object field : datatype) {
                        System.out.println(field);
                        Cell cell = row.createCell(colNum++);
                        if (field instanceof String) {
                            cell.setCellValue((String) field);
                        } else if (field instanceof Integer) {
                            cell.setCellValue((Integer) field);
                        }
                    }
                }
                try {
                    FileOutputStream fos = new FileOutputStream(filePath);
                    xwb.write(fos);
                    xwb.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println("Done");
    }

    public void exportMyTasks(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...       
        ExportTaskDashboardAttributes(outputStream, "My Work", "myTasksListIterator");
    }

    public void exportAssignedTasks(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...               
        ExportTaskDashboardAttributes(outputStream, "Assigned Work", "assignedTaskListIterator");
    }
    
    public void exportUnAssignedTasks(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        ExportTaskDashboardAttributes(outputStream, "UnAssigned Work", "unassignedTaskListIterator");
    }
    
    private void ExportTaskDashboardAttributes(OutputStream outputStream, String sheetName, String iteratorName){
        try {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet worksheet = workbook.createSheet(sheetName);
        
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dcIteratorBindings = bindings.findIteratorBinding(iteratorName);
        dcIteratorBindings.setRangeSize(-1);
        XSSFRow  excelrow = null;
        XSSFCellStyle style = workbook.createCellStyle();
        style.setBorderTop((short) 6); // double lines border
        style.setBorderBottom((short) 1); // single line border
        XSSFFont font = workbook.createFont();
        font.setFontHeightInPoints((short) 11);
        font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
        style.setFont(font);                 
                        
        // Get all the rows of a iterator
        oracle.jbo.Row[] rows = dcIteratorBindings.getAllRowsInRange();
        int i = 0;
        int k = 0;
        String[] selectedTaskAttributes = new String[10];
        String[] taskDashboardAttributes = new String[10];
        for (oracle.jbo.Row row : rows) {
        String[] myTaskAttributes = row.getAttributeNames();
        
            if (i == 0) {
            for(String colName: myTaskAttributes){
                if(colName.equalsIgnoreCase("opportunityNumber")){
                    selectedTaskAttributes[k] = colName;    
                    taskDashboardAttributes[0] = selectedTaskAttributes[k];
                    k++;
                }
                if (colName.equalsIgnoreCase("taskNumber")){
                    selectedTaskAttributes[k] = colName;  
                    taskDashboardAttributes[1] = selectedTaskAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("taskDueDate")){
                    selectedTaskAttributes[k] = colName; 
                    taskDashboardAttributes[2] = selectedTaskAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("customerDueDate")){
                    selectedTaskAttributes[k] = colName;   
                    taskDashboardAttributes[3] = selectedTaskAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("customerNo")){
                    selectedTaskAttributes[k] = colName;    
                    taskDashboardAttributes[4] = selectedTaskAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("customerName")){
                    selectedTaskAttributes[k] = colName; 
                    taskDashboardAttributes[5] = selectedTaskAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("projectType")){
                    selectedTaskAttributes[k] = colName;   
                    taskDashboardAttributes[6] = selectedTaskAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("contractType")){
                    selectedTaskAttributes[k] = colName; 
                    taskDashboardAttributes[7] = selectedTaskAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("teamNumber")){
                    selectedTaskAttributes[k] = colName;   
                    taskDashboardAttributes[8] = selectedTaskAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("assigneeName")){
                    selectedTaskAttributes[k] = colName; 
                    taskDashboardAttributes[9] = selectedTaskAttributes[k];
                    k++;
                }
            }
            }
        
        //print header on first row in excel
        if (i == 0) {
            excelrow = (XSSFRow)worksheet.createRow((short)i);
            short j = 0;
            for (String colName : taskDashboardAttributes) {
               
                    //HSSFCell cellA1 = null;
                if (colName.equalsIgnoreCase("opportunityNumber")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Opportunity");
                    cellA1.setCellStyle(style); 
                }
                else if (colName.equalsIgnoreCase("taskNumber")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Task");
                    cellA1.setCellStyle(style);
                }
                else if (colName.equalsIgnoreCase("taskDueDate")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Task due Date");
                    cellA1.setCellStyle(style);
                }
                else if (colName.equalsIgnoreCase("customerDueDate")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Customer due Date");
                    cellA1.setCellStyle(style);
                }
                else if (colName.equalsIgnoreCase("customerNo")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Customer#");
                    cellA1.setCellStyle(style);
                }
                else if (colName.equalsIgnoreCase("customerName")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Customer Name");
                    cellA1.setCellStyle(style);
                }
                else if (colName.equalsIgnoreCase("projectType")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Project Type");
                    cellA1.setCellStyle(style);
                }
                else if (colName.equalsIgnoreCase("contractType")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Contract Type");
                    cellA1.setCellStyle(style);
                }
                else if (colName.equalsIgnoreCase("teamNumber")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Sales Team");
                    cellA1.setCellStyle(style);
                }
                else if (colName.equalsIgnoreCase("assigneeName")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Assigned To");
                    cellA1.setCellStyle(style);
                }
                    j++;
                 
            }
        }
        
        //print data from second row in excel
        ++i;
        short j = 0;
        excelrow = worksheet.createRow((short)i);
        for (String colName : taskDashboardAttributes) {                                                                          
            if (colName.equalsIgnoreCase("opportunityNumber")) {
                XSSFCell cell = excelrow.createCell(j);                    
                cell.setCellValue(row.getAttribute(colName).toString());                    
            }
               
                else if (colName.equalsIgnoreCase("taskNumber")) {
                    if(null!=row.getAttribute(colName)){
                    XSSFCell cell = excelrow.createCell(j);                    
                    cell.setCellValue(row.getAttribute(colName).toString());
                    }
                }                   
                else if (colName.equalsIgnoreCase("taskDueDate")){
                    if(null!=row.getAttribute(colName)){
                    XSSFCell cell = excelrow.createCell(j);                    
                    cell.setCellValue(row.getAttribute(colName).toString());
                    }
                }
                else if (colName.equalsIgnoreCase("customerDueDate")){
                    if(null!=row.getAttribute(colName)){
                        XSSFCell cell = excelrow.createCell(j);                    
                        cell.setCellValue(row.getAttribute(colName).toString());
                    }
                }
                else if (colName.equalsIgnoreCase("customerNo")){
                    if(null!=row.getAttribute(colName)){
                        XSSFCell cell = excelrow.createCell(j);                    
                    cell.setCellValue(row.getAttribute(colName).toString());
                    }
                } 
                else if (colName.equalsIgnoreCase("customerName")){
                    if(null!=row.getAttribute(colName)){
                        XSSFCell cell = excelrow.createCell(j);                    
                    cell.setCellValue(row.getAttribute(colName).toString());
                    }
                }
                else if (colName.equalsIgnoreCase("projectType")){
                    if(null!=row.getAttribute(colName)){
                        XSSFCell cell = excelrow.createCell(j);                    
                        cell.setCellValue(row.getAttribute(colName).toString());
                    }
                }
                else if (colName.equalsIgnoreCase("contractType")){
                    if(null!=row.getAttribute(colName)){
                        XSSFCell cell = excelrow.createCell(j);                    
                        cell.setCellValue(row.getAttribute(colName).toString());
                    }
                }
                else if (colName.equalsIgnoreCase("teamNumber")){
                    if(null!=row.getAttribute(colName)){
                        XSSFCell cell = excelrow.createCell(j);                    
                        cell.setCellValue(row.getAttribute(colName).toString());
                    }
                }
                else if (colName.equalsIgnoreCase("assigneeName")){
                    if(null!=row.getAttribute(colName)){
                        XSSFCell cell = excelrow.createCell(j);                    
                        cell.setCellValue(row.getAttribute(colName).toString());
                    }
                }              
                j++;
               
            }
         
           worksheet.createFreezePane(0, 1, 0, 1);
           worksheet.autoSizeColumn(0);
           worksheet.autoSizeColumn(1);
           worksheet.autoSizeColumn(2);
           worksheet.autoSizeColumn(3);
           worksheet.autoSizeColumn(4);
           worksheet.autoSizeColumn(5);
           worksheet.autoSizeColumn(6);
           worksheet.autoSizeColumn(7);
           worksheet.autoSizeColumn(8);
           worksheet.autoSizeColumn(9);
        } 
            workbook.write(outputStream);
            outputStream.flush();
        
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setTaskDashboardAssignees(boolean taskDashboardAssignees) {
        this.taskDashboardAssignees = taskDashboardAssignees;
    }

    public boolean isTaskDashboardAssignees() {
        return taskDashboardAssignees;
    }

    public void exportCompletedTasks(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        try {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet worksheet = workbook.createSheet("Completed Tasks");
        
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dcIteratorBindings = bindings.findIteratorBinding("completedTaskListIterator");
        dcIteratorBindings.setRangeSize(-1);
        XSSFRow  excelrow = null;
        XSSFCellStyle style = workbook.createCellStyle();
        style.setBorderTop((short) 6); // double lines border
        style.setBorderBottom((short) 1); // single line border
        XSSFFont font = workbook.createFont();
        font.setFontHeightInPoints((short) 11);
        font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
        style.setFont(font);                 
                        
        // Get all the rows of a iterator
        oracle.jbo.Row[] rows = dcIteratorBindings.getAllRowsInRange();
        int i = 0;
        int k = 0;
        String[] selectedTaskAttributes = new String[8];
        String[] completedTaskAttributes = new String[8];
        for (oracle.jbo.Row row : rows) {
        String[] myTaskAttributes = row.getAttributeNames();
        
            if (i == 0) {
            for(String colName: myTaskAttributes){
                if(colName.equalsIgnoreCase("opportunityNumber")){
                    selectedTaskAttributes[k] = colName;    
                    completedTaskAttributes[0] = selectedTaskAttributes[k];
                    k++;
                }
                if(colName.equalsIgnoreCase("taskNumber")){
                    selectedTaskAttributes[k] = colName;    
                    completedTaskAttributes[1] = selectedTaskAttributes[k];
                    k++;
                }
                if (colName.equalsIgnoreCase("group")){
                    selectedTaskAttributes[k] = colName;  
                    completedTaskAttributes[2] = selectedTaskAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("taskAssignmentDate")){
                    selectedTaskAttributes[k] = colName; 
                    completedTaskAttributes[3] = selectedTaskAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("taskCompletionDate")){
                    selectedTaskAttributes[k] = colName;   
                    completedTaskAttributes[4] = selectedTaskAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("taskCompletedBy")){
                    selectedTaskAttributes[k] = colName;    
                    completedTaskAttributes[5] = selectedTaskAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("customerNo")){
                    selectedTaskAttributes[k] = colName; 
                    completedTaskAttributes[6] = selectedTaskAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("customerName")){
                    selectedTaskAttributes[k] = colName;   
                    completedTaskAttributes[7] = selectedTaskAttributes[k];
                    k++;
                }               
            }
            }
        
        //print header on first row in excel
        if (i == 0) {
            excelrow = (XSSFRow)worksheet.createRow((short)i);
            short j = 0;
            for (String colName : completedTaskAttributes) {
               
                    //HSSFCell cellA1 = null;
                if(colName.equalsIgnoreCase("opportunityNumber")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Opportunity");
                    cellA1.setCellStyle(style); 
                }
                if (colName.equalsIgnoreCase("taskNumber")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Task");
                    cellA1.setCellStyle(style); 
                }
                else if (colName.equalsIgnoreCase("group")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Role");
                    cellA1.setCellStyle(style);
                }
                else if (colName.equalsIgnoreCase("taskAssignmentDate")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Task due Date");
                    cellA1.setCellStyle(style);
                }
                else if (colName.equalsIgnoreCase("taskCompletionDate")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Completion Date");
                    cellA1.setCellStyle(style);
                }
                else if (colName.equalsIgnoreCase("taskCompletedBy")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Closed By");
                    cellA1.setCellStyle(style);
                }
                else if (colName.equalsIgnoreCase("customerNo")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Customer#");
                    cellA1.setCellStyle(style);
                }
                else if (colName.equalsIgnoreCase("customerName")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Customer Name");
                    cellA1.setCellStyle(style);
                }               
                j++;
                 
            }
        }
        
        //print data from second row in excel
        ++i;
        short j = 0;
        excelrow = worksheet.createRow((short)i);
        for (String colName : completedTaskAttributes) {
            if(colName.equalsIgnoreCase("opportunityNumber")){
                XSSFCell cell = excelrow.createCell(j);
                cell.setCellValue(row.getAttribute(colName).toString());                 
            }
            if (colName.equalsIgnoreCase("taskNumber")) {
                XSSFCell cell = excelrow.createCell(j);                    
                cell.setCellValue(row.getAttribute(colName).toString());                    
            }               
            else if (colName.equalsIgnoreCase("group")) {
                if(null!=row.getAttribute(colName)){
                    XSSFCell cell = excelrow.createCell(j);                    
                    cell.setCellValue(row.getAttribute(colName).toString());
                }
            }                   
            else if (colName.equalsIgnoreCase("taskAssignmentDate")){
                if(null!=row.getAttribute(colName)){
                XSSFCell cell = excelrow.createCell(j);                    
                cell.setCellValue(row.getAttribute(colName).toString());
                }
            }
            else if (colName.equalsIgnoreCase("taskCompletionDate")){
                if(null!=row.getAttribute(colName)){
                    XSSFCell cell = excelrow.createCell(j);                    
                    cell.setCellValue(row.getAttribute(colName).toString());
                }
            }
            else if (colName.equalsIgnoreCase("taskCompletedBy")){
                if(null!=row.getAttribute(colName)){
                    XSSFCell cell = excelrow.createCell(j);                    
                cell.setCellValue(row.getAttribute(colName).toString());
                }
            } 
            else if (colName.equalsIgnoreCase("customerNo")){
                if(null!=row.getAttribute(colName)){
                    XSSFCell cell = excelrow.createCell(j);                    
                cell.setCellValue(row.getAttribute(colName).toString());
                }
            }
            else if (colName.equalsIgnoreCase("customerName")){
                if(null!=row.getAttribute(colName)){
                    XSSFCell cell = excelrow.createCell(j);                    
                    cell.setCellValue(row.getAttribute(colName).toString());
                }
            }                            
            j++;
               
            }
         
           worksheet.createFreezePane(0, 1, 0, 1);
           worksheet.autoSizeColumn(0);
           worksheet.autoSizeColumn(1);
           worksheet.autoSizeColumn(2);
           worksheet.autoSizeColumn(3);
           worksheet.autoSizeColumn(4);
           worksheet.autoSizeColumn(5);
           worksheet.autoSizeColumn(6);
           worksheet.autoSizeColumn(7);         
        } 
            workbook.write(outputStream);
            outputStream.flush();
        
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setBind_additionalDetailItem(RichShowDetailItem bind_additionalDetailItem) {
        this.bind_additionalDetailItem = bind_additionalDetailItem;
    }

    public RichShowDetailItem getBind_additionalDetailItem() {
        return bind_additionalDetailItem;
    }

    public void setAdditionCostPopUp(RichPopup additionCostPopUp) {
        this.additionCostPopUp = additionCostPopUp;
    }

    public RichPopup getAdditionCostPopUp() {
        return additionCostPopUp;
    }

    public void setBind_ProgramCost(RichShowDetailItem bind_ProgramCost) {
        this.bind_ProgramCost = bind_ProgramCost;
    }

    public RichShowDetailItem getBind_ProgramCost() {
        return bind_ProgramCost;
    }

    public void setProgramCostPopUp(RichPopup programCostPopUp) {
        this.programCostPopUp = programCostPopUp;
    }

    public RichPopup getProgramCostPopUp() {
        return programCostPopUp;
    }

    public void setBind_FinalPricing(RichShowDetailItem bind_FinalPricing) {
        this.bind_FinalPricing = bind_FinalPricing;
    }

    public RichShowDetailItem getBind_FinalPricing() {
        return bind_FinalPricing;
    }

    public void setBind_attachmentTab(RichShowDetailItem bind_attachmentTab) {
        this.bind_attachmentTab = bind_attachmentTab;
    }

    public RichShowDetailItem getBind_attachmentTab() {
        return bind_attachmentTab;
    }

    public void setBind_awardsTab(RichShowDetailItem bind_awardsTab) {
        this.bind_awardsTab = bind_awardsTab;
    }

    public RichShowDetailItem getBind_awardsTab() {
        return bind_awardsTab;
    }

    public void setBind_approvalTab(RichShowDetailItem bind_approvalTab) {
        this.bind_approvalTab = bind_approvalTab;
    }

    public RichShowDetailItem getBind_approvalTab() {
        return bind_approvalTab;
    }

    public void setBind_CostComparisonTab(RichShowDetailItem bind_CostComparisonTab) {
        this.bind_CostComparisonTab = bind_CostComparisonTab;
    }

    public RichShowDetailItem getBind_CostComparisonTab() {
        return bind_CostComparisonTab;
    }

    public void setBind_notesTab(RichShowDetailItem bind_notesTab) {
        this.bind_notesTab = bind_notesTab;
    }

    public RichShowDetailItem getBind_notesTab() {
        return bind_notesTab;
    }

    public void setBind_taskTab(RichShowDetailItem bind_taskTab) {
        this.bind_taskTab = bind_taskTab;
    }

    public RichShowDetailItem getBind_taskTab() {
        return bind_taskTab;
    }

    public void setBind_partsTab(RichShowDetailItem bind_partsTab) {
        this.bind_partsTab = bind_partsTab;
    }

    public RichShowDetailItem getBind_partsTab() {
        return bind_partsTab;
    }
}


