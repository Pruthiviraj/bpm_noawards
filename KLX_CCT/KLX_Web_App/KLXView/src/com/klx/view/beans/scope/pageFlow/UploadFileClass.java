package com.klx.view.beans.scope.pageFlow;

import com.klx.common.logger.KLXLogger;

import com.klx.view.error.CustomErrorHandler;

import java.io.FileNotFoundException;

import java.io.IOException;

import java.io.InputStream;

import java.util.logging.Level;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputFile;

import oracle.adf.view.rich.component.rich.input.RichSelectOneRadio;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.model.UploadedFile;

public class UploadFileClass {
    private RichInputFile fileBinding;
    private RichSelectOneRadio revisonChoiceBinding;
    private RichPopup popUpBinding;
    private String uploadStatusMessage="File Uploaded Successfully.";
    private RichPopup statusPopUpBinding;
    private static ADFLogger _logger = ADFLogger.createADFLogger(UploadFileClass.class); 
    private static final String CLAZZ_NAME="UploadFileClass";
    private static KLXLogger logger = KLXLogger.getLogger();    

    public UploadFileClass() {
    }
    
    private  long startTime;

    public void uploadedFileVCL(ValueChangeEvent valueChangeEvent) {
      
    }

    public void setFileBinding(RichInputFile fileBinding) {
        this.fileBinding = fileBinding;
    }

    public RichInputFile getFileBinding() {
        return fileBinding;
    }

    public void setRevisonChoiceBinding(RichSelectOneRadio revisonChoiceBinding) {
        this.revisonChoiceBinding = revisonChoiceBinding;
    }

    public RichSelectOneRadio getRevisonChoiceBinding() {
        return revisonChoiceBinding;
    }

    public void setPopUpBinding(RichPopup popUpBinding) {
        this.popUpBinding = popUpBinding;
    }

    public RichPopup getPopUpBinding() {
        return popUpBinding;
    }
    
    public void openUploadPopUp(ActionEvent actionEvent) {
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getPopUpBinding().show(hints);
    }

    public void cancelListner(ActionEvent actionEvent) {
//        getFileBinding().resetValue();
//        getFileBinding().setSubmittedValue(null);
//        getFileBinding().setValue(null);
        getPopUpBinding().hide();
    }

    public void setUploadStatusMessage(String uploadStatusMessage) {
        this.uploadStatusMessage = uploadStatusMessage;
    }

    public String getUploadStatusMessage() {
        return uploadStatusMessage;
    }

    public void setStatusPopUpBinding(RichPopup statusPopUpBinding) {
        this.statusPopUpBinding = statusPopUpBinding;
    }

    public RichPopup getStatusPopUpBinding() {
        return statusPopUpBinding;
    }

    public void okButtonACL(ActionEvent actionEvent) {
       getStatusPopUpBinding().hide();
       getPopUpBinding().hide();
       getFileBinding().setValue(null);
    }

    public void uploadButtonACL(ActionEvent actionEvent) {
        UploadedFile newValue = (UploadedFile) getFileBinding().getValue();
        String filename = newValue.getFilename();
        if (filename.endsWith("xlsx")) {
            try {
                startTime= System.currentTimeMillis() ;
                InputStream inputStream = newValue.getInputStream();
                BindingContext bindingContext = BindingContext.getCurrent();
                BindingContainer bindingContainer = bindingContext.getCurrentBindingsEntry();
                OperationBinding binding = bindingContainer.getOperationBinding("uploadBeaconFileData");
                binding.getParamsMap().put("revisionChoice", (String)getRevisonChoiceBinding().getValue());
                binding.getParamsMap().put("inputStream", inputStream);
                binding.execute();
                        RichPopup.PopupHints hints = new RichPopup.PopupHints();
                        getStatusPopUpBinding().show(hints);
                logger.log(Level.INFO, getClass(),
                                                                    "uploadButtonACL",
                                                                    "start time is -"+startTime);

                logger.log(Level.INFO, getClass(),
                                                                    "uploadButtonACL",
                                                                    " End time "+ System.currentTimeMillis());
                logger.log(Level.INFO, getClass(),
                                                                    "uploadButtonACL",
                                                                    "Estimated time of execution is in secs = "+(System.currentTimeMillis()-startTime)/1000);
            } catch (FileNotFoundException e) {
                //e.printStackTrace();
                logger.log(Level.SEVERE, getClass(),
                                                    "uploadButtonACL",
                                                    e.getMessage());
                CustomErrorHandler.processError("CCT","uploadButtonACL","CCT_SYS_EX28",e);   
            } catch (IOException e) {
                //e.printStackTrace();
                logger.log(Level.SEVERE, getClass(),
                                                    "uploadButtonACL",
                                                    e.getMessage());
                CustomErrorHandler.processError("CCT","uploadButtonACL","CCT_SYS_EX28",e);   
            }

        } else {
            FacesMessage fm = new FacesMessage("File is invalid Format");
            fm.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(getFileBinding().getClientId(), fm);
        }
    }
}
