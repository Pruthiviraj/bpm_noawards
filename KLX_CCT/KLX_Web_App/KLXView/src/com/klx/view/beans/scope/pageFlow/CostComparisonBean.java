package com.klx.view.beans.scope.pageFlow;

import com.klx.common.logger.KLXLogger;

import com.klx.services.entities.Quote;
import com.klx.view.beans.constants.ViewConstants;
import com.klx.view.beans.utils.ADFUtils;

import com.klx.view.beans.utils.JSFUtils;
import com.klx.view.error.CustomErrorHandler;

import java.io.FileInputStream;
import java.io.OutputStream;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.Date;

import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletRequest;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class CostComparisonBean {
    private BigDecimal additionalChangeVal;
    private static KLXLogger logger = KLXLogger.getLogger();
    private String opportunityID;

    private String opportunityNumber;
    private HashMap<String, String> costComparisonLinesVoAttrMapping = new HashMap<String, String>();
    private HashMap<String, String[]> downloadcostComparisonLinesmapping = new HashMap<String, String[]>();


    public CostComparisonBean() {
        super();
    }

    public void setAdditionalChangeVal(BigDecimal additionalChangeVal) {
        this.additionalChangeVal = additionalChangeVal;
    }

    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public BigDecimal getAdditionalChangeVal() {
        logger.log(Level.INFO, getClass(), "CostComparisonBean", "Entering getAdditionalChangeVal method");
        oracle.adf.model.OperationBinding operationBinding =
            (oracle.adf.model.OperationBinding) ADFUtils.findOperationBinding("getAdditionalChangesValue");
        operationBinding.getParamsMap().put("opportunityID", getOpportunityID());
        operationBinding.execute();
        this.additionalChangeVal = (BigDecimal) operationBinding.getResult();

        return additionalChangeVal;
    }

    public void setOpportunityID(String opportunityID) {
        this.opportunityID = opportunityID;
    }

    public String getOpportunityID() {
        return opportunityID;
    }

    public void downloadCostComparisonDetails(FacesContext facesContext, OutputStream outputStream) {
        try {

            generateCostComparisonDownloadTemplate(ViewConstants.COST_COMPARISON_DETAILS);
            facesContext.getCurrentInstance();
            String fileName = ViewConstants.BEACON_TEMPLATE;
            String filePath = ViewConstants.FILE_PATH + fileName;
            FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);
            logger.log(Level.INFO, getClass(), "downloadCostComparisonDetails", "Excel read");


            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFWorkbook filledWorkbook = genericLogic(workbook, "CostCompareLinesQuotePartsVO1Iterator", 1, 0);
            XSSFSheet sheet = filledWorkbook.getSheetAt(0);
            XSSFRow topRow = sheet.getRow(0);
            int lovCol = 0;
            for (Cell c : topRow) {
                if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                    String text = c.getStringCellValue();

                    if (ViewConstants.WORK_QUEUE.equalsIgnoreCase(text)) {
                        lovCol = c.getColumnIndex();
                        break;
                    }
                }
            }
            filledWorkbook = addExcelLOV(filledWorkbook, 1, lovCol);

            try {
                logger.log(Level.INFO, getClass(), "downloadCostComparisonDetails", "Writing workbook");
                filledWorkbook.write(outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (Exception e) {
                logger.log(Level.SEVERE, getClass(), "downloadCostComparisonDetails", e.getMessage());
            }

        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "downloadCostComparisonDetails", e.getMessage());
        }
    }

    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public void generateCostComparisonDownloadTemplate(String mappingkey) {
        OperationBinding generateCostComparisonDownloadTemplate =
            ADFUtils.getBindingContainer().getOperationBinding("generateCostComparisonDownloadTemplate");
        generateCostComparisonDownloadTemplate.getParamsMap().put("pMappingKey", mappingkey);
        HashMap hashMap = (HashMap) generateCostComparisonDownloadTemplate.execute();
        downloadcostComparisonLinesmapping = (HashMap) hashMap.get("downloadCostComparisonMapping");
        costComparisonLinesVoAttrMapping = (HashMap) hashMap.get("costComparisonLinesAttrMapping");

    }

    public XSSFWorkbook genericLogic(XSSFWorkbook workbook, String Iterator, int sheetrownum, int headerrownum) {

        //CellStyle for DataFormatting Number without Decimal
        DataFormat numFormatWithoutDecimal = workbook.createDataFormat(); //format defined
        XSSFCellStyle cellStyleCost = workbook.createCellStyle();
        cellStyleCost.setDataFormat(numFormatWithoutDecimal.getFormat("#,###.0000"));
        cellStyleCost.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        cellStyleCost.setBorderTop(XSSFCellStyle.BORDER_THIN);
        cellStyleCost.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        cellStyleCost.setBorderRight(XSSFCellStyle.BORDER_THIN);

        //CellStyle for 0 value from DB
        XSSFCellStyle cellStyleFor0 = workbook.createCellStyle();
        cellStyleFor0.setDataFormat(numFormatWithoutDecimal.getFormat("0.0000"));
        cellStyleFor0.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        cellStyleFor0.setBorderTop(XSSFCellStyle.BORDER_THIN);
        cellStyleFor0.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        cellStyleFor0.setBorderRight(XSSFCellStyle.BORDER_THIN);


        //Below cell style is for Border
        XSSFCellStyle cellStyleForBorder = workbook.createCellStyle();
        cellStyleForBorder.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        cellStyleForBorder.setBorderTop(XSSFCellStyle.BORDER_THIN);
        cellStyleForBorder.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        cellStyleForBorder.setBorderRight(XSSFCellStyle.BORDER_THIN);
        XSSFSheet sheet = workbook.getSheetAt(0);

        DCBindingContainer bindings = this.getDCBindingsContainer();
        DCIteratorBinding itorBinding = bindings.findIteratorBinding(Iterator);
        ViewObject object = itorBinding.getViewObject();
        object.setNamedWhereClauseParam("b_opp_id", getOpportunityID());
        object.executeQuery();
        RowSetIterator iterator = object.createRowSetIterator(null);
        int i = sheetrownum;
        int logHeaderOnce = 0;
        logger.log(Level.INFO, getClass(), "genericLogic", "Loop started for generic Parts download");

        org.apache.poi.ss.usermodel.Row headerRow = sheet.getRow(headerrownum);

        try{
        while (iterator.hasNext()) { //Row iteration
            Row currentRow = iterator.next();
            XSSFRow sheetRow = sheet.createRow(i);
            i++;
            int index = 0; //Column
            for (index = 0; index < currentRow.getAttributeCount(); index++) {
                Cell newCell = headerRow.getCell(index);
                if (newCell != null) {
                    XSSFCell currentCell = sheetRow.createCell(index);
                    currentCell.setCellStyle(cellStyleForBorder);


                    if (newCell.getCellTypeEnum() != CellType.BLANK) {
                        String newHeader = newCell.getStringCellValue();

                        if (logHeaderOnce == 0) {
                            logger.log(Level.INFO, getClass(), "genericLogic",
                                       "Header value of new cell= " + newHeader);
                        }

                        String destnFieldName = null;
                        if (downloadcostComparisonLinesmapping.get(newHeader) != null) {
                            destnFieldName = downloadcostComparisonLinesmapping.get(newHeader)[0];
                        }
                        String voAttribute = null;
                        String dataFormat = "";
                        if (destnFieldName != null) {
                            voAttribute =costComparisonLinesVoAttrMapping.get(destnFieldName);
                            dataFormat = (String) downloadcostComparisonLinesmapping.get(newHeader)[1];
                           
                        } else {
                            //If any column mappings r not found in mappings table
                            voAttribute = null;
                            dataFormat = "";
                        }
                        if (null != currentRow && voAttribute != null && currentRow.getAttribute(voAttribute) != null &&
                            "" != currentRow.getAttribute(voAttribute).toString()) {

                            switch (dataFormat) {
                           
                            case "COST": //This is for celltype with numeric value with decimal & comma
                                currentCell.setCellType(CellType.NUMERIC);
                                if (Double.valueOf(currentRow.getAttribute(voAttribute).toString()) < 1.0) {
                                    currentCell.setCellStyle(cellStyleFor0);
                                    currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(voAttribute)
                                                                            .toString()));
                                } else {
                                    currentCell.setCellStyle(cellStyleCost);
                                    currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(voAttribute)
                                                                            .toString()));
                                }
                                break;
                            case "NUMBER": //This is for celltype with numeric value
                                currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(voAttribute)
                                                                        .toString()));
                                break;
                            default:
                                currentCell.setCellValue(currentRow.getAttribute(voAttribute).toString());
                            }
                        } else {
                            currentCell.setCellValue("");
                        }

                    }

                }
            }
            logHeaderOnce++;
        }
        }catch (Exception nfe) {

            nfe.printStackTrace();
        }
        
        return workbook;

    }

    public DCBindingContainer getDCBindingsContainer() {
        DCBindingContainer bindingsContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        return bindingsContainer;
    }

    public XSSFWorkbook addExcelLOV(XSSFWorkbook workbook, int rowNum, int colNum) {

        String[] queues = getQueueNames();
        XSSFSheet sheet = workbook.getSheetAt(0);
        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sheet);
        XSSFDataValidationConstraint dvConstraint =
            (XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(queues);
        CellRangeAddressList addressList = new CellRangeAddressList(rowNum, sheet.getLastRowNum(), colNum, colNum);
        XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList);
        validation.setShowErrorBox(true);
        validation.setErrorStyle(XSSFDataValidation.ErrorStyle.STOP);
        validation.createErrorBox("Criteria Mismatch", ViewConstants.LOV_ERROR_MESSAGE);
        sheet.addValidationData(validation);
        validation.setSuppressDropDownArrow(false);
        return workbook;
    }

    public String[] getQueueNames() {
        DCBindingContainer bindingContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding binding = bindingContainer.findIteratorBinding("QuotePartsQueueVO1Iterator");
        ViewObject object = binding.getViewObject();
        int size = object.getRowCount();
        String[] queues = new String[size];
        ArrayList<String> queueNameList = new ArrayList<String>();

        RowSetIterator iterator = object.createRowSetIterator(null);
        while (iterator.hasNext()) {
            Row currentRow = iterator.next();
            String queueName = (String) currentRow.getAttribute("QueueName");
            logger.log(Level.INFO, getClass(), "getQueueNames", "QueueName= " + queueName);
            
            queueNameList.add(queueName);

        }
        String[] tempArray = new String[queueNameList.size()];
        queues = queueNameList.toArray(tempArray);
        return queues;
    }


    public void setOpportunityNumber(String opportunityNumber) {
        this.opportunityNumber = opportunityNumber;
    }

    public String getOpportunityNumber() {
        return opportunityNumber;
    }


}
