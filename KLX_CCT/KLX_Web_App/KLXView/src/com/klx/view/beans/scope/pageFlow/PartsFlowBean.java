package com.klx.view.beans.scope.pageFlow;

import com.klx.cct.task.v1.RoleDetailsType;
import com.klx.cct.task.v1.RoleType;
import com.klx.cct.task.v1.TaskT;
import com.klx.common.access.UserActionAccessManagement;
import com.klx.common.logger.KLXLogger;
import com.klx.services.entities.Quote;
import com.klx.view.beans.constants.ViewConstants;
import com.klx.view.beans.scope.backing.Message;
import com.klx.view.beans.utils.ADFUtils;
import com.klx.view.beans.utils.JSFUtils;
import com.klx.view.error.CustomErrorHandler;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

import java.math.BigDecimal;

import java.util.HashMap;
import java.util.logging.Level;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputFile;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichPanelCollection;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.VariableValueManager;
import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewCriteriaManager;
import oracle.jbo.ViewObject;
import oracle.jbo.server.ViewObjectImpl;

import org.apache.myfaces.trinidad.event.DisclosureEvent;
import org.apache.myfaces.trinidad.model.UploadedFile;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class PartsFlowBean implements Serializable {
    @SuppressWarnings("compatibility:5122969690838860701")
    private static final long serialVersionUID = 981757698021643547L;
    private RichTable allQuotePartsTableBind;
    private RichPopup partsNotesBinding;
    private static ADFLogger _logger = ADFLogger.createADFLogger(PartsFlowBean.class);
    private static final String CLAZZ_NAME = "PartsFlowBean";
    private RichPopup errorTablePopUp;
 private RichPopup showcustHistory;
    private RichPopup errorBindingPopUp;
    private RichPanelCollection allTabPanelCollection;
    private boolean createSnapshot = false;
    private boolean createError = false;
    private boolean uploadBeacon = false;
    private boolean downloadBeacon = false;
    private boolean uploadHPP = false;
    private boolean downloadHPP = false;
    private boolean uploadChemical = false;
    private boolean downloadChemical = false;
    private boolean uploadLighting = false;
    private boolean uploadElectrical = false;
    private boolean downloadLighting = false;
    private boolean downloadElectrical = false;
    private boolean uploadAdvanceSource = false;
    private boolean downloadAdvanceSource = false;
    private boolean uploadPricing = false;
    private boolean downloadPricing  = false;
    private boolean uploadCommodity = false;
    private boolean downloadCommodity  = false;
    private boolean uploadUnknown = false;
    private boolean downloadUnknown = false;
    private boolean downloadNoBid = false;    
    private boolean moveParts = false;
    private boolean uploadNoBid = false;

    public void setUploadNoBid(boolean uploadNoBid) {
        this.uploadNoBid = uploadNoBid;
    }

    public boolean isUploadNoBid() {
        return uploadNoBid;
    }

    public void setCreateError(boolean createError) {
        this.createError = createError;
    }

    public boolean isCreateError() {
        return createError;
    }

    public void setCreateSnapshot(boolean createSnapshot) {
        this.createSnapshot = createSnapshot;
    }

    public boolean isCreateSnapshot() {
        return createSnapshot;
    }


    public PartsFlowBean() {
        super();
        userActionsAccess();
    }
    
    public void userActionsAccess(){
        HashMap actions = new HashMap();
        actions.put(ViewConstants.CREATE_SNAPSHOT, false);            
        actions.put(ViewConstants.UPLOAD_BEACON, false);
        actions.put(ViewConstants.DOWNLOAD_BEACON, false);
        actions.put(ViewConstants.UPLOAD_LIGHTING, false);
        actions.put(ViewConstants.UPLOAD_ELECTRICAL, false);
        actions.put(ViewConstants.DOWNLOAD_LIGHTING, false);
        actions.put(ViewConstants.DOWNLOAD_ELECTRICAL, false);
        actions.put(ViewConstants.UPLOAD_ADVANCE_SOURCE, false);
        actions.put(ViewConstants.DOWNLOAD_ADVANCE_SOURCE, false);
        actions.put(ViewConstants.UPLOAD_COMMODITY, false);
        actions.put(ViewConstants.DOWNLOAD_COMMODITY, false);
        actions.put(ViewConstants.UPLOAD_UNKNOWN, false);
        actions.put(ViewConstants.DOWNLOAD_UNKNOWN, false);
        actions.put(ViewConstants.UPLOAD_PRICING, false);
        actions.put(ViewConstants.DOWNLOAD_PRICING, false);
        actions.put(ViewConstants.DOWNLOAD_NOBID, false);
        actions.put(ViewConstants.MOVE_PARTS, false);
        actions.put(ViewConstants.UPLOAD_HPP, false);
        actions.put(ViewConstants.DOWNLOAD_HPP, false);
        actions.put(ViewConstants.UPLOAD_CHEMICAL, false);
        actions.put(ViewConstants.DOWNLOAD_CHEMICAL, false);
        actions.put(ViewConstants.UPLOAD_NOBID, false);
        HashMap userAcess = UserActionAccessManagement.getUserAccess(ADFContext.getCurrent().getSecurityContext().getUserRoles(), actions);        
        createSnapshot = ((Boolean)userAcess.get(ViewConstants.CREATE_SNAPSHOT)).booleanValue();    
        uploadBeacon = ((Boolean)userAcess.get(ViewConstants.UPLOAD_BEACON)).booleanValue();
        downloadBeacon = ((Boolean)userAcess.get(ViewConstants.DOWNLOAD_BEACON)).booleanValue();
        uploadLighting = ((Boolean)userAcess.get(ViewConstants.UPLOAD_LIGHTING)).booleanValue();
        uploadElectrical = ((Boolean)userAcess.get(ViewConstants.UPLOAD_ELECTRICAL)).booleanValue();
        uploadElectrical = true;
        downloadLighting = ((Boolean)userAcess.get(ViewConstants.DOWNLOAD_LIGHTING)).booleanValue();
        downloadElectrical = ((Boolean)userAcess.get(ViewConstants.DOWNLOAD_ELECTRICAL)).booleanValue();
        downloadElectrical = true;
        uploadAdvanceSource = ((Boolean)userAcess.get(ViewConstants.UPLOAD_ADVANCE_SOURCE)).booleanValue();
        downloadAdvanceSource = ((Boolean)userAcess.get(ViewConstants.DOWNLOAD_ADVANCE_SOURCE)).booleanValue();
        uploadCommodity = ((Boolean)userAcess.get(ViewConstants.UPLOAD_COMMODITY)).booleanValue();
        downloadCommodity = ((Boolean)userAcess.get(ViewConstants.DOWNLOAD_COMMODITY)).booleanValue();
        uploadUnknown = ((Boolean)userAcess.get(ViewConstants.UPLOAD_UNKNOWN)).booleanValue();
        downloadUnknown = ((Boolean)userAcess.get(ViewConstants.DOWNLOAD_UNKNOWN)).booleanValue();
        downloadNoBid = ((Boolean)userAcess.get(ViewConstants.DOWNLOAD_NOBID)).booleanValue();
        moveParts = ((Boolean)userAcess.get(ViewConstants.MOVE_PARTS)).booleanValue();
        downloadPricing = ((Boolean)userAcess.get(ViewConstants.DOWNLOAD_PRICING)).booleanValue();
        uploadPricing = ((Boolean)userAcess.get(ViewConstants.UPLOAD_PRICING)).booleanValue();
        uploadHPP = ((Boolean)userAcess.get(ViewConstants.UPLOAD_HPP)).booleanValue();
        downloadHPP = ((Boolean)userAcess.get(ViewConstants.DOWNLOAD_HPP)).booleanValue();
        uploadChemical = ((Boolean)userAcess.get(ViewConstants.UPLOAD_CHEMICAL)).booleanValue();
        downloadChemical = ((Boolean)userAcess.get(ViewConstants.DOWNLOAD_CHEMICAL)).booleanValue();
        uploadNoBid = ((Boolean)userAcess.get(ViewConstants.UPLOAD_NOBID)).booleanValue();                                 
    }

    private RichInputFile fileBinding;
    private RichPopup popUpBinding;
    private String uploadStatusMessage = ViewConstants.UPL_STATUS_SUCCESS;
    private String validationMessage = "";
    private RichPopup statusPopUpBinding;
    private long startTime;
    private Quote quote;
    private String opportunityNumber;
    private Message message = new Message();
    private String revisionSelected = "UR";
    private String queueID;
    private String notesDesc;
    private String quotePartId;
    private static KLXLogger logger = KLXLogger.getLogger();
  
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }


    public void setQuotePartId(String quotePartId) {
        this.quotePartId = quotePartId;
    }

    public String getQuotePartId() {
        return quotePartId;
    }

    public void setNotesDesc(String notesDesc) {
        this.notesDesc = notesDesc;
    }

    public String getNotesDesc() {
        return notesDesc;
    }

    public void setRevisionSelected(String revisionSelected) {
        this.revisionSelected = revisionSelected;
    }

    public String getRevisionSelected() {
        return revisionSelected;
    }

    public void setQueueID(String queueID) {
        this.queueID = queueID;
    }

    public String getQueueID() {
        return queueID;
    }

    public void setOpportunityNumber(String opportunityNumber) {
        this.opportunityNumber = opportunityNumber;
    }

    public String getOpportunityNumber() {
        return opportunityNumber;
    }


    public void setQuote(Quote quote) {
        this.quote = quote;
    }

    public Quote getQuote() {
        return quote;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setFileBinding(RichInputFile fileBinding) {
        this.fileBinding = fileBinding;
    }

    public RichInputFile getFileBinding() {
        return fileBinding;
    }

    public void setPopUpBinding(RichPopup popUpBinding) {
        this.popUpBinding = popUpBinding;
    }

    public RichPopup getPopUpBinding() {
        return popUpBinding;
    }

    public void setUploadStatusMessage(String uploadStatusMessage) {
        this.uploadStatusMessage = uploadStatusMessage;
    }

    public String getUploadStatusMessage() {
        return uploadStatusMessage;
    }

    public void setStatusPopUpBinding(RichPopup statusPopUpBinding) {
        this.statusPopUpBinding = statusPopUpBinding;
    }

    public RichPopup getStatusPopUpBinding() {
        return statusPopUpBinding;
    }


    public void setMessage(Message message) {
        this.message = message;
    }

    public Message getMessage() {
        return message;
    }

    public void uploadButtonACL(ActionEvent actionEvent) {
        UploadedFile newValue = (UploadedFile) getFileBinding().getValue();
        HashMap hashMap = new HashMap();
        if (null != newValue) {
            String filename = newValue.getFilename();
            if (filename.endsWith("xlsx")) {
                try {
                    startTime = System.currentTimeMillis();
                    InputStream inputStream = newValue.getInputStream();
                    BindingContext bindingContext = BindingContext.getCurrent();
                    BindingContainer bindingContainer = bindingContext.getCurrentBindingsEntry();
                    HashMap inputParamMap = new HashMap();
                    String uploadType = (String) JSFUtils.resolveExpression("#{pageFlowScope.uploadType}");
                    String queueType = "";
                    String lineNumber = (String) JSFUtils.resolveExpression("#{pageFlowScope.lineSequence}");
                    String queueid = (String) JSFUtils.resolveExpression("#{pageFlowScope.queueid}");
                    if(!queueid.equals("0"))
                       setRevisionSelected(ViewConstants.beaconUploadConstants[0]);
                    if(!getRevisionSelected().equals(ViewConstants.beaconUploadConstants[1]))
                       queueType = (String) JSFUtils.resolveExpression("#{pageFlowScope.queueReport}");
                    else
                       queueType = (String) JSFUtils.resolveExpression("#{pageFlowScope.beaconReportUpdate}");
                    String sheetname = (String) JSFUtils.resolveExpression("#{pageFlowScope.sheetName}");
                    inputParamMap.put("uploadType", uploadType);
                    inputParamMap.put("queueType", queueType);
                    inputParamMap.put("lineNumber", lineNumber);
                    inputParamMap.put("queueid", queueid);                
                    inputParamMap.put("sheetName",sheetname);
                    OperationBinding binding = bindingContainer.getOperationBinding("uploadBeaconFileData");
                    binding.getParamsMap().put("revisionChoice", getRevisionSelected());
                    binding.getParamsMap().put("inputStream", inputStream);
                    binding.getParamsMap()
                        .put("quoteNumber", resolveExpression("#{pageFlowScope.partsFlowBean.quote.quoteNumber}"));
                    binding.getParamsMap()
                        .put("quoteRevision", resolveExpression("#{pageFlowScope.partsFlowBean.quote.quoteRevision}"));
                    binding.getParamsMap()
                        .put("quoteRevisionId",
                             resolveExpression("#{pageFlowScope.partsFlowBean.quote.quoteRevisionId}"));
                    binding.getParamsMap()
                        .put("opportunityNumber",
                             resolveExpression("#{pageFlowScope.partsFlowBean.opportunityNumber}"));
                    binding.getParamsMap().put("inputParamMap", inputParamMap);
                    binding.getParamsMap().put("partsType", "parts");
                    hashMap = (HashMap) binding.execute();
                    //Below lines are commented as it is not implemented fully.
                /*    String validateStatus = (String) hashMap.get("validateStatus");
                    if("failure".equalsIgnoreCase(validateStatus)){
                        RichPopup.PopupHints hints = new RichPopup.PopupHints();
                        getErrorBindingPopUp().show(hints);
                        return ;
                    }  */
                    String status = (String) hashMap.get("uploadStatus");
                    String uploadFailureReason = (String) hashMap.get("uploadFailureReason");                    
                    if(uploadFailureReason == null)
                        uploadFailureReason = "";
                    if ("successful".equalsIgnoreCase(status)) {
                        
                        setUploadStatusMessage(ViewConstants.UPL_STATUS_SUCCESS);
                        if("0".equals(queueid)){
                            setUploadStatusMessage(ViewConstants.BEACON_UPL_STATUS_SNAPSHOT);
                        }
                       
                        setValidationMessage("");
                    } else if("unsuccessful".equalsIgnoreCase(status)){
                        setUploadStatusMessage(ViewConstants.UPL_STATUS_FAILURE);
                        setValidationMessage(uploadFailureReason);
                    }
                    else{
                        HashMap quoteMap = (HashMap)ADFContext.getCurrent().getRequestScope().get("unknownErrorScope");
                        String unknownUploadError = (String) quoteMap.get("uploadFailureReason"); 
                        setUploadStatusMessage(ViewConstants.UPL_STATUS_FAILURE_UNKNOWN_REASON+"{CCT"+unknownUploadError+"}");
                        setValidationMessage("");
                    }
                    setExpressionValue("#{bindings.QuoteRevision1.inputValue}", quote.getQuoteRevision());
                    getPopUpBinding().hide();
                    refreshPartsQueues();
                    RichPopup.PopupHints hints = new RichPopup.PopupHints();
                    getStatusPopUpBinding().show(hints);
                    AdfFacesContext facesContext = AdfFacesContext.getCurrentInstance();
                    facesContext.addPartialTarget(allTabPanelCollection);
                    logger.log(Level.INFO, getClass(), "uploadButtonACL", "start time is -" + startTime);
                    logger.log(Level.INFO, getClass(), "uploadButtonACL", " End time " + System.currentTimeMillis());
                    logger.log(Level.INFO, getClass(), "uploadButtonACL",
                               "Estimated time of execution is in secs = " +
                               (System.currentTimeMillis() - startTime) / 1000);
                
                } catch (FileNotFoundException e) {
                    logger.log(Level.SEVERE, getClass(),"uploadButtonACL",e.getMessage()); 
                    CustomErrorHandler.processError("CCT","uploadButtonACL","CCT_SYS_EX30",e);
                } catch (IOException e) {
                    //e.printStackTrace();
                    logger.log(Level.SEVERE, getClass(),"uploadButtonACL",e.getMessage()); 
                    CustomErrorHandler.processError("CCT","uploadButtonACL","CCT_SYS_EX30",e);
                }
            } else {
                FacesMessage fm = new FacesMessage("The file type is incorrect. Please ensure the file type is �.xlsx�.");
                fm.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(getFileBinding().getClientId(), fm);
            }
        }
    }
    
    

    public void cancelListner(ActionEvent actionEvent) {

        getPopUpBinding().hide();
    }

    public void openUploadPopUp(ActionEvent actionEvent) {
        RichInputFile inputFile = getFileBinding();
        if (null != inputFile) {
            inputFile.setValue(null);
            inputFile.setSubmittedValue(null);
            AdfFacesContext.getCurrentInstance().addPartialTarget(inputFile);
        }
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getPopUpBinding().show(hints);
    }

    public void refreshIterator(String iterator) {

        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding(iterator);
        iter.executeQuery();
    }

    public void movePartsToQueue(ActionEvent actionEvent) {
        BindingContext bindingContext = BindingContext.getCurrent();
        BindingContainer bindingContainer = bindingContext.getCurrentBindingsEntry();
        DCBindingContainer dcBindingContainer = (DCBindingContainer) bindingContainer;
        DCIteratorBinding queuesIterator = dcBindingContainer.findIteratorBinding("QuotePartsQueueVO1Iterator");
        Row changedQueue = queuesIterator.getCurrentRow();        
        try{
            logger.log(Level.INFO, getClass(), "movePartsToQueue", "Queue Id of selected parts =="+ADFContext.getCurrent().getViewScope().get("queueId").toString());
            OperationBinding binding = bindingContainer.getOperationBinding("movePartsToQueues");
            binding.getParamsMap().put("fromQueueId", ADFContext.getCurrent()
                                                                .getViewScope()
                                                                .get("queueId")
                                                                .toString());
            binding.getParamsMap().put("toQueueId", changedQueue.getAttribute("QueueId").toString());
            binding.execute();
            createTaskForMovedQueueId(changedQueue.getAttribute("QueueId").toString());
            ADFContext.getCurrent().getViewScope().put("queueId", null);
        }
        catch(Exception ae){
            CustomErrorHandler.processError("CCT","movePartsToQueue","CCT_BUS_EX05",ae);
        }
    }
    public void createTaskForMovedQueueId(String queueId){
        DCBindingContainer bindings =(DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();               
            DCIteratorBinding roleIter = bindings.findIteratorBinding("CreateTaskRolesVO1Iterator");
            ViewObject createTaskRolesVO = roleIter.getViewObject();
            
        ViewCriteriaManager vcm = createTaskRolesVO.getViewCriteriaManager();
        ViewCriteria CreateTaskRolesVOCriteria = vcm.getViewCriteria("CreateTaskRolesByQueueIdVOCriteria");
        VariableValueManager vvm = CreateTaskRolesVOCriteria.ensureVariableManager();
        vvm.setVariableValue("b_queueId", queueId);
        createTaskRolesVO.applyViewCriteria(CreateTaskRolesVOCriteria);
        createTaskRolesVO.executeQuery();
       
        RowSetIterator createTaskRoleIterator = createTaskRolesVO.createRowSetIterator(null);
        String userGrp = "";
        String taskSubject = "";
        while (createTaskRoleIterator.hasNext()) {
            Row createTaskRoleRow = createTaskRoleIterator.next();
            userGrp = createTaskRoleRow.getAttribute("UserGroup").toString();
            taskSubject = createTaskRoleRow.getAttribute("TaskSubject").toString();
        }
        
        DCBindingContainer bindingContainer = ADFUtils.getDCBindingContainer();
        DCIteratorBinding binding = bindingContainer.findIteratorBinding("OpportunityVO1Iterator");
        Row currentRow = binding.getCurrentRow();
        String opportunityId = null;
        if(ADFContext.getCurrent().getSessionScope().get("opportunityNumber") == null){
            if (null!=currentRow) {
                //opportunityId = (String) (currentRow.getAttribute("OscOpportunityId")).toString(); 
                opportunityId = (String) (currentRow.getAttribute("OpportunityId")).toString(); 
                }
        }
        else
            opportunityId = ADFContext.getCurrent().getSessionScope().get("opportunityId").toString();
        

        TaskT task = new TaskT();
        task.setOpportunityId(opportunityId);
        task.setOpportunityNumber(getOpportunityNumber());
        task.setTaskTitle(taskSubject);
        task.setTaskType("NEW");
        task.setComments("");
        task.setAttribute4("BeaconUploadProcess");
        RoleDetailsType roleDetails = new RoleDetailsType();
        RoleType role = new RoleType();
        role.setIsTaskCreationRequired(true);
        role.setRoleName(userGrp);
        roleDetails.getRole().add(role);
        task.setRoleDetails(roleDetails);
        task.setCreatedBy(ADFContext.getCurrent()
                                    .getSecurityContext()
                                    .getUserName());

        oracle.adf.model.OperationBinding op = (oracle.adf.model.OperationBinding) BindingContext.getCurrent()
                                                               .getCurrentBindingsEntry()
                                                               .getOperationBinding("createTask");
        op.getParamsMap().put("task", task);
        String result = (String) op.execute();
                
        
    }
    public void refreshPartsQueues() {
        refreshPageIterator("AllPartsDownloadVO1Iterator");
       // refreshPageIterator("AllQuotePartsVO1Iterator");
        refreshPageIterator("HPPQuotePartsVO1Iterator");
        refreshPageIterator("ChemicalQuotePartsVO1Iterator");
        refreshPageIterator("LightingQuotePartsVO1Iterator");
        refreshPageIterator("AdvancedSourcingQuotePartsVO1Iterator");
        refreshPageIterator("CommodityQuotePartsVO1Iterator");
        refreshPageIterator("UnknownQuotePartsVO1Iterator");
        refreshPageIterator("NoBidQuotePartsVO1Iterator");
        refreshPageIterator("StrategicPricingQuotePartsVO1Iterator");
        refreshPageIterator("ElectricalQuotePartsVO1Iterator");
    }

    public void refreshPageIterator(String iteratorName) {
        DCBindingContainer bindingContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding binding = bindingContainer.findIteratorBinding(iteratorName);
        long count = binding.getViewObject().getEstimatedRowCount();
        ViewObject object = binding.getViewObject();
        BigDecimal revisionId = new BigDecimal(quote.getQuoteRevisionId());
        //object.reset();
        object.clearCache();
        object.cancelQuery();
        object.setWhereClause(null);
        if(iteratorName.equals("AllPartsDownloadVO1Iterator")){
            object.setNamedWhereClauseParam("bind_quoteRevisionId", revisionId);
            object.executeQuery();
        }
//        if(iteratorName.equals("AllQuotePartsVO1Iterator")){
//            object.setNamedWhereClauseParam("bind_quoteRevisionId", revisionId);
//            object.executeQuery();
//        }
        else if(iteratorName.equals("HPPQuotePartsVO1Iterator")){
            object.setNamedWhereClauseParam("bind_queueId",  new BigDecimal("1"));
            object.setNamedWhereClauseParam("bind_quoteRevisionId", revisionId);
            object.executeQuery();
        }
        else if(iteratorName.equals("ChemicalQuotePartsVO1Iterator")){
            object.setNamedWhereClauseParam("bind_queueId",  new BigDecimal("2"));
            object.setNamedWhereClauseParam("bind_quoteRevisionId", revisionId);
            object.executeQuery();
        }
        else if(iteratorName.equals("LightingQuotePartsVO1Iterator")){
            object.setNamedWhereClauseParam("bind_queueId",  new BigDecimal("3"));
            object.setNamedWhereClauseParam("bind_quoteRevisionId", revisionId);
            object.executeQuery();
        }
        else if(iteratorName.equals("ElectricalQuotePartsVO1Iterator")){
            object.setNamedWhereClauseParam("bind_queueId",  new BigDecimal("10"));
            object.setNamedWhereClauseParam("bind_quoteRevisionId", revisionId);
            object.executeQuery();
        }
        else if(iteratorName.equals("AdvancedSourcingQuotePartsVO1Iterator")){
            object.setNamedWhereClauseParam("bind_queueId",  new BigDecimal("5"));
            object.setNamedWhereClauseParam("bind_quoteRevisionId", revisionId);
            object.executeQuery();
        }
        else if(iteratorName.equals("CommodityQuotePartsVO1Iterator")){
            object.setNamedWhereClauseParam("bind_queueId",  new BigDecimal("7"));
            object.setNamedWhereClauseParam("bind_quoteRevisionId", revisionId);
            object.executeQuery();
        }
        else if(iteratorName.equals("UnknownQuotePartsVO1Iterator")){
            object.setNamedWhereClauseParam("bind_queueId",  new BigDecimal("6"));
            object.setNamedWhereClauseParam("bind_quoteRevisionId", revisionId);
            object.executeQuery();
        }
        else if(iteratorName.equals("NoBidQuotePartsVO1Iterator")){
            object.setNamedWhereClauseParam("bind_queueId",  new BigDecimal("8"));
            object.setNamedWhereClauseParam("bind_quoteRevisionId", revisionId);
            object.executeQuery();
        }
        
        else if(iteratorName.equals("StrategicPricingQuotePartsVO1Iterator")){
            object.setNamedWhereClauseParam("bind_queueId",  new BigDecimal("4"));
            object.setNamedWhereClauseParam("bind_quoteRevisionId", revisionId);
            object.executeQuery();
        }
        long countNew = binding.getViewObject().getEstimatedRowCount();
    }

    public void downloadBeaconReport(FacesContext facesContext, OutputStream outputStream) {
        XSSFWorkbook xssfWorkbookResult = null;
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            String fileName = "AcidReport.xlsx";
            // String filePath = "/queueTemplates/" + fileName;
            String filePath = ViewConstants.FILE_PATH + fileName;
            String queueId = "0"; //hard coded for Acid Report
            FileInputStream fileStream = (FileInputStream) context.getExternalContext().getResourceAsStream(filePath);
            XSSFWorkbook xssfWorkbook = new XSSFWorkbook(fileStream);
            BindingContext bindingContext = BindingContext.getCurrent();
            try {
                BindingContainer bindingContainer = bindingContext.getCurrentBindingsEntry();
                OperationBinding binding = bindingContainer.getOperationBinding("downloadBeaconFileData");
                binding.getParamsMap().put("workbook", xssfWorkbook);
                binding.getParamsMap().put("queueId", this.getQueueID());
                xssfWorkbookResult = (XSSFWorkbook) binding.execute();
            } catch (Exception e) {
                //e.printStackTrace();
                logger.log(Level.SEVERE, getClass(),
                                                           "downloadBeaconReport",
                                                          e.getMessage()); 
                CustomErrorHandler.processError("CCT","downloadBeaconReport","CCT_SYS_EX31",e);
            }
            xssfWorkbookResult.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            //e.printStackTrace();
            logger.log(Level.SEVERE, getClass(),
                                                       "downloadBeaconReport",
                                                      e.getMessage()); 
            CustomErrorHandler.processError("CCT","downloadBeaconReport","CCT_SYS_EX31",e);
        }
    }

    public void hppPartsTabListener(DisclosureEvent disclosureEvent) {
        // Add event code here...
        if(disclosureEvent.isExpanded()){
            this.setQueueID("1");
            DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = binds.findIteratorBinding("HPPQuotePartsVO1Iterator");
            iter.executeQuery();
        }
    }

    public void chemicalPartsTabListener(DisclosureEvent disclosureEvent) {
        // Add event code here...
        if(disclosureEvent.isExpanded()){
            this.setQueueID("2");
            DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = binds.findIteratorBinding("ChemicalQuotePartsVO1Iterator");
            iter.executeQuery();
        }
    }

    public void lightingTabListener(DisclosureEvent disclosureEvent) {
        // Add event code here...
        if(disclosureEvent.isExpanded())
        {
            refreshPageIterator("LightingQuotePartsVO1Iterator");
            this.setQueueID("3");
//            DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
//            DCIteratorBinding iter = binds.findIteratorBinding("LightingQuotePartsVO1Iterator");
//            iter.executeQuery();
        }
    }
    
    public void electricalTabListener(DisclosureEvent disclosureEvent) 
    {
        if(disclosureEvent.isExpanded())
        {
            refreshPageIterator("ElectricalQuotePartsVO1Iterator");
            this.setQueueID("10");
//            DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
//            DCIteratorBinding iter = binds.findIteratorBinding("ElectricalQuotePartsVO1Iterator");
//            iter.executeQuery();
        }
    }

    public void advanceSourcingTabListener(DisclosureEvent disclosureEvent) {
        // Add event code here...
        if(disclosureEvent.isExpanded()){
            this.setQueueID("5");
            DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = binds.findIteratorBinding("AdvancedSourcingQuotePartsVO1Iterator");
            iter.executeQuery();
        }
    }

    public void commoditiesTabListener(DisclosureEvent disclosureEvent) {
        // Add event code here...
        if(disclosureEvent.isExpanded()){
            this.setQueueID("9");
            DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = binds.findIteratorBinding("CommodityQuotePartsVO1Iterator");
            iter.executeQuery();
        }
    }

    public void unknownPartsTabListener(DisclosureEvent disclosureEvent) {
        // Add event code here...
        if(disclosureEvent.isExpanded()){
            this.setQueueID("6");
            DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = binds.findIteratorBinding("UnknownQuotePartsVO1Iterator");
            iter.executeQuery();
        }
    }

    public void noBidTabListener(DisclosureEvent disclosureEvent) {
        // Add event code here...
        if(disclosureEvent.isExpanded()){
            this.setQueueID("7");
            DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = binds.findIteratorBinding("NoBidQuotePartsVO1Iterator");
            iter.executeQuery();
        }
    }

    public Object resolveExpression(String expression) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Application app = facesContext.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        ValueExpression valueExp = elFactory.createValueExpression(elContext, expression, Object.class);
        return valueExp.getValue(elContext);
    }

    public void okACL(ActionEvent actionEvent) {
        getStatusPopUpBinding().hide();
    }

    public void setAllQuotePartsTableBind(RichTable allQuotePartsTableBind) {
        this.allQuotePartsTableBind = allQuotePartsTableBind;
    }

    public RichTable getAllQuotePartsTableBind() {
        return allQuotePartsTableBind;
    }

    public void addPartsNotes(ActionEvent actionEvent) {
        // Add event code here...
        
        OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                               .getCurrentBindingsEntry()
                                                               .getOperationBinding("addNotes");
        op.getParamsMap().put("partId", getQuotePartId());
        op.getParamsMap().put("noteComments", getNotesDesc());
        op.getParamsMap().put("noteLevel", "PART");
        op.execute();
        OperationBinding op2 = (OperationBinding) BindingContext.getCurrent()
                                                                .getCurrentBindingsEntry()
                                                                .getOperationBinding("getPartsQueues");
        op2.execute();
        refreshNotes();
        getPartsNotesBinding().hide();
    }

    public void refreshNotes() {
        DCBindingContainer binds = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter1 = binds.findIteratorBinding("PartsNotesVOIterator");
        iter1.executeQuery();
    }


    public void openPartsNotes(ActionEvent actionEvent) {
        // Add event code here...
        setNotesDesc("");
        OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                               .getCurrentBindingsEntry()
                                                               .getOperationBinding("setPartsNotesBindVar");
        op.getParamsMap().put("quotePartId", getQuotePartId());
        op.execute();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getPartsNotesBinding().show(hints);
    }

    public void setExpressionValue(String expression, Object newValue) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Application app = facesContext.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        ValueExpression valueExp = elFactory.createValueExpression(elContext, expression, Object.class);
        Class bindClass = valueExp.getType(elContext);
        if (bindClass.isPrimitive() || bindClass.isInstance(newValue)) {
            valueExp.setValue(elContext, newValue);
        }
    }

    public void setPartsNotesBinding(RichPopup partsNotesBinding) {
        this.partsNotesBinding = partsNotesBinding;
    }

    public RichPopup getPartsNotesBinding() {
        return partsNotesBinding;
    }

    public void strategicPricingTabListener(DisclosureEvent disclosureEvent) {
        // Add event code here...
        if(disclosureEvent.isExpanded())
            this.setQueueID("4");
    }

    public void cancelPartsNotes(ActionEvent actionEvent) {
        // Add event code here...
        getPartsNotesBinding().hide();
    }

    public void updateQuotePartsFromFile(ActionEvent actionEvent) {
        //todo
    }

    public void setCustomerPn(String customerPn) {
        this.customerPn = customerPn;
    }

    public String getCustomerPn() {
        return customerPn;
    }

    private String customerPn;
    public DCBindingContainer getDCBindingsContainer() {
        DCBindingContainer bindingsContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        return bindingsContainer;
    }

    public void showCustPartNoHistoryDetails(ActionEvent actionEvent) {
        // Add event code here...
        logger.log(Level.INFO, getClass(), "showCustPartNoHistoryDetails", "Entering showCustPartNoHistoryDetails method");
        DCBindingContainer bindings = this.getDCBindingsContainer();
        DCIteratorBinding itorBinding = bindings.findIteratorBinding("CustomerPNHistoryVO1Iterator");
        ViewObjectImpl customerPN = (ViewObjectImpl) itorBinding.getViewObject();
        ViewCriteria criteria = customerPN.getViewCriteria("CustomerPNHistoryVOCriteria");
       //Displaying customerpartno and oppNo values
        logger.log(Level.INFO, getClass(),"showCustPartNoHistoryDetails",""+getCustomerPn());
        logger.log(Level.INFO, getClass(),"showCustPartNoHistoryDetails",""+getOpportunityNumber());
        customerPN.setNamedWhereClauseParam("b_OppNo", getOpportunityNumber());
        customerPN.applyViewCriteria(criteria);
        logger.log(Level.INFO, getClass(), "showCustPartNoHistoryDetails","view Criteria applied ");
        BindingContext bindingContext = BindingContext.getCurrent();
        BindingContainer bindingContainer = bindingContext.getCurrentBindingsEntry();
        // creating methodbinding to use the method showCustPNHistory of AppModuleImpl class to execute 
        //custPNHistory VO with view criteria
        OperationBinding binding = bindingContainer.getOperationBinding("showCustPNHistory");
        binding.getParamsMap().put("custPN", getCustomerPn());
        logger.log(Level.INFO, getClass(), "showCustPartNoHistoryDetails","Custpn value passed ");
        binding.getParamsMap().put("oppNo", getOpportunityNumber());
        logger.log(Level.INFO, getClass(), "showCustPartNoHistoryDetails","OppNo value passed ");
        binding.execute();
        //checking row count of VO query result
        logger.log(Level.INFO, getClass(), "showCustPartNoHistoryDetails","History VO------"+customerPN.getEstimatedRowCount());
        //showing the popup 
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getShowcustHistory().show(hints);
        
        logger.log(Level.INFO, getClass(), "showCustPartNoHistoryDetails", "Exiting showCustPartNoHistoryDetails method" ); 
        
    }
 public void setShowcustHistory(RichPopup showcustHistory) {
        this.showcustHistory = showcustHistory;
    }

    public RichPopup getShowcustHistory() {
        return showcustHistory;
    }

    public void setErrorTablePopUp(RichPopup errorTablePopUp) {
        this.errorTablePopUp = errorTablePopUp;
    }

    public RichPopup getErrorTablePopUp() {
        return errorTablePopUp;
    }

    public void createErrorACL(ActionEvent actionEvent) {
        OperationBinding binding = ADFUtils.findOperationBinding("createErroTRVO");
        binding.execute();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getErrorBindingPopUp().show(hints);
        
    }

    public void setErrorBindingPopUp(RichPopup errorBindingPopUp) {
        this.errorBindingPopUp = errorBindingPopUp;
    }

    public RichPopup getErrorBindingPopUp() {
        return errorBindingPopUp;
    }
    
    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public void createSnapshot(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(),
                                            "PartsFlowBean",
                                            "Entering createSnapshot method");

        oracle.adf.model.OperationBinding operationBinding = (oracle.adf.model.OperationBinding) ADFUtils.findOperationBinding("createSnapshot");
        operationBinding.getParamsMap().put("opportunityNumber", getOpportunityNumber());  
        operationBinding.getParamsMap().put("snapshotType", "Cost_Comparison");   
        operationBinding.execute();
        refresApprovalTable();
        FacesMessage fm = new FacesMessage("Snapshot Created Successfully");
        fm.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, fm);
    }
    
    public void refresApprovalTable(){
            logger.log(Level.INFO,getClass(),"refresApprovalTable","Entering refresApprovalTable");
            oracle.adf.model.OperationBinding fetchSnapshots = (oracle.adf.model.OperationBinding) ADFUtils.findOperationBinding("fetchSnapshots");
            fetchSnapshots.getParamsMap().put("opportunityId", JSFUtils.resolveExpression("#{bindings.OpportunityId.inputValue}"));
            fetchSnapshots.getParamsMap().put("opportunityNumber", JSFUtils.resolveExpression("#{bindings.OpportunityNumber.inputValue}"));
            logger.log(Level.INFO,getClass(),"refresApprovalTable","Parameters passing to fetchSnapshots method --\n 1)opportunityId=--"+JSFUtils.resolveExpression("#{bindings.OpportunityId.inputValue}")+"-- \n 2)opportunityNumber=--"+ JSFUtils.resolveExpression("#{bindings.OpportunityNumber.inputValue}")+"--");
            fetchSnapshots.execute();
            logger.log(Level.INFO,getClass(),"refresApprovalTable","Exiting refresApprovalTable");
        }


    public void setAllTabPanelCollection(RichPanelCollection allTabPanelCollection) {
        this.allTabPanelCollection = allTabPanelCollection;
    }

    public RichPanelCollection getAllTabPanelCollection() {
        return allTabPanelCollection;
    }

    public void refreshSummaryTab(DisclosureEvent disclosureEvent) {
        if(disclosureEvent.isExpanded()){
            DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter1 = binds.findIteratorBinding("QuoteQueueVVO1Iterator");
            iter1.executeQuery();
        }
    }

    public void setUploadBeacon(boolean uploadBeacon) {
        this.uploadBeacon = uploadBeacon;
    }

    public boolean isUploadBeacon() {
        return uploadBeacon;
    }

    public void setDownloadBeacon(boolean downloadBeacon) {
        this.downloadBeacon = downloadBeacon;
    }

    public boolean isDownloadBeacon() {
        return downloadBeacon;
    }

    public void setUploadHPP(boolean uploadHPP) {
        this.uploadHPP = uploadHPP;
    }

    public boolean isUploadHPP() {
        return uploadHPP;
    }

    public void setDownloadHPP(boolean downloadHPP) {
        this.downloadHPP = downloadHPP;
    }

    public boolean isDownloadHPP() {
        return downloadHPP;
    }

    public void setUploadChemical(boolean uploadChemical) {
        this.uploadChemical = uploadChemical;
    }

    public boolean isUploadChemical() {
        return uploadChemical;
    }

    public void setDownloadChemical(boolean downloadChemical) {
        this.downloadChemical = downloadChemical;
    }

    public boolean isDownloadChemical() {
        return downloadChemical;
    }

    public void setUploadLighting(boolean uploadLighting) {
        this.uploadLighting = uploadLighting;
    }

    public boolean isUploadLighting() {
        return uploadLighting;
    }

    public void setDownloadLighting(boolean downloadLighting) {
        this.downloadLighting = downloadLighting;
    }

    public boolean isDownloadLighting() {
        return downloadLighting;
    }

    public void setUploadAdvanceSource(boolean uploadAdvanceSource) {
        this.uploadAdvanceSource = uploadAdvanceSource;
    }

    public boolean isUploadAdvanceSource() {
        return uploadAdvanceSource;
    }

    public void setDownloadAdvanceSource(boolean downloadAdvanceSource) {
        this.downloadAdvanceSource = downloadAdvanceSource;
    }

    public boolean isDownloadAdvanceSource() {
        return downloadAdvanceSource;
    }

    public void setUploadPricing(boolean uploadPricing) {
        this.uploadPricing = uploadPricing;
    }

    public boolean isUploadPricing() {
        return uploadPricing;
    }

    public void setDownloadPricing(boolean downloadPricing) {
        this.downloadPricing = downloadPricing;
    }

    public boolean isDownloadPricing() {
        return downloadPricing;
    }

    public void setUploadCommodity(boolean uploadCommodity) {
        this.uploadCommodity = uploadCommodity;
    }

    public boolean isUploadCommodity() {
        return uploadCommodity;
    }

    public void setDownloadCommodity(boolean downloadCommodity) {
        this.downloadCommodity = downloadCommodity;
    }

    public boolean isDownloadCommodity() {
        return downloadCommodity;
    }

    public void setUploadUnknown(boolean uploadUnknown) {
        this.uploadUnknown = uploadUnknown;
    }

    public boolean isUploadUnknown() {
        return uploadUnknown;
    }

    public void setDownloadUnknown(boolean downloadUnknown) {
        this.downloadUnknown = downloadUnknown;
    }

    public boolean isDownloadUnknown() {
        return downloadUnknown;
    }

    public void setDownloadNoBid(boolean downloadNoBid) {
        this.downloadNoBid = downloadNoBid;
    }

    public boolean isDownloadNoBid() {
        return downloadNoBid;
    }

    public void setMoveParts(boolean moveParts) {
        this.moveParts = moveParts;
    }  
    
    public boolean isMoveParts() {
        return moveParts;
    }

    public void setValidationMessage(String validationMessage) {
        this.validationMessage = validationMessage;
    }

    public String getValidationMessage() {
        return validationMessage;
    }

    public void refreshAllTab(DisclosureEvent disclosureEvent) {
        // Add event code here...
        if(disclosureEvent.isExpanded()){
            DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = binds.findIteratorBinding("AllPartsDownloadVO1Iterator");
            iter.executeQuery();
        }
    }

    public void executeTop50PartsViews(DisclosureEvent disclosureEvent) {
        if(disclosureEvent.isExpanded()){
        BigDecimal revisionId = new BigDecimal(quote.getQuoteRevisionId());
        BigDecimal ret_code = null;
        oracle.adf.model.OperationBinding getTop50PartsBinding =
            (oracle.adf.model.OperationBinding) ADFUtils.findOperationBinding("getTop50Parts");
        getTop50PartsBinding.getParamsMap().put("quoteRevisionId", revisionId);
        ret_code = (BigDecimal)getTop50PartsBinding.execute();        
        if (ret_code.compareTo(new BigDecimal(0)) != 0){
            FacesMessage fm = new FacesMessage(ViewConstants.INVALID_TOP50_PARTS);
            fm.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, fm);
        }
        
            oracle.adf.model.OperationBinding operationBinding =
                (oracle.adf.model.OperationBinding) ADFUtils.findOperationBinding("updatesupportTeam");
            operationBinding.getParamsMap().put("opportunityNumber",
                                 resolveExpression("#{pageFlowScope.partsFlowBean.opportunityNumber}"));
            operationBinding.execute();
            
        DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding cctTop50PartsViewIterator = binds.findIteratorBinding("CctTop50PartsVOIterator");
        ViewObject cctTop50PartsVO = cctTop50PartsViewIterator.getViewObject(); 
        cctTop50PartsVO.setNamedWhereClauseParam("b_quoteRevisionId", revisionId);
        cctTop50PartsVO.executeQuery(); 
        }
    }

    public void setDownloadElectrical(boolean downloadElectrical)
    {
        this.downloadElectrical = downloadElectrical;
    }

    public boolean isDownloadElectrical()
    {
        return downloadElectrical;
    }

    public void setUploadElectrical(boolean uploadElectrical)
    {
        this.uploadElectrical = uploadElectrical;
    }

    public boolean isUploadElectrical()
    {
        return uploadElectrical;
    }
}
