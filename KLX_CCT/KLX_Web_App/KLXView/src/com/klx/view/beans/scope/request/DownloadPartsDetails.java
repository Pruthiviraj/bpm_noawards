package com.klx.view.beans.scope.request;

import com.klx.view.beans.constants.ViewConstants;
import com.klx.view.beans.utils.ADFUtils;

import com.klx.view.beans.utils.JSFUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

//import java.sql.Date;


import java.sql.Timestamp;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Date;
import java.util.Collection;



import java.util.Iterator;

import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.share.logging.ADFLogger;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;

import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.klx.common.logger.KLXLogger;
import com.klx.common.cache.ParamCache;

import com.klx.view.error.CustomErrorHandler;

import java.text.DateFormat;

import java.util.Calendar;
import java.util.logging.Level;

import com.klx.cct.ad.user.v1.User;

import java.math.BigDecimal;

import java.util.Collections;
import java.util.List;

import oracle.adf.share.ADFContext;

import oracle.jbo.AttributeDef;

import org.apache.poi.ss.usermodel.DataFormat;

public class DownloadPartsDetails {
    public DownloadPartsDetails() {
    }

    private HashMap<String, String> opptyMapping = new HashMap<String, String>();
    private HashMap<String, String[]> beaconMapping = new HashMap<String, String[]>();
    private HashMap<String, String> quoteVoAttrMapping = new HashMap<String, String>();
    private HashMap<String, String> opptyVoAttrMapping = new HashMap<String, String>();
    private HashMap<String, String> allQuoteVoAttrMapping = new HashMap<String, String>();
    private HashMap<String, String> downloadBeaconMapping = new HashMap<String, String>();
    private static KLXLogger logger = KLXLogger.getLogger();

    public DCBindingContainer getDCBindingsContainer() {
        DCBindingContainer bindingsContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        return bindingsContainer;
    }

    public void generateDownloadTemplate(String mappingkey) {
        OperationBinding generateDownloadTemplate =
            ADFUtils.getBindingContainer().getOperationBinding("generateDownloadTemplate");
        generateDownloadTemplate.getParamsMap().put("pMappingKey", mappingkey);
        HashMap hashMap = (HashMap) generateDownloadTemplate.execute();

        quoteVoAttrMapping = (HashMap) hashMap.get("allQuoteVoAttrMapping");
        beaconMapping = (HashMap) hashMap.get("downloadBeaconMapping");
    }
    
    public void generateFinalTop50DownloadTemplate(String mappingkey) {
        OperationBinding generateDownloadTemplate =
            ADFUtils.getBindingContainer().getOperationBinding("generateFinalTop50DownloadTemplate");
        generateDownloadTemplate.getParamsMap().put("pMappingKey", mappingkey);
        HashMap hashMap = (HashMap) generateDownloadTemplate.execute();

        quoteVoAttrMapping = (HashMap) hashMap.get("cctFinalTOP50AttrMapping");
        beaconMapping = (HashMap) hashMap.get("downloadCctFinalTOP50Mapping");
    }
    
    public void generateFinalDownloadTemplate(String mappingkey) {
        OperationBinding generateDownloadTemplate =
            ADFUtils.getBindingContainer().getOperationBinding("generateFinalDownloadTemplate");
        generateDownloadTemplate.getParamsMap().put("pMappingKey", mappingkey);
        HashMap hashMap = (HashMap) generateDownloadTemplate.execute();

        quoteVoAttrMapping = (HashMap) hashMap.get("allQuoteVoAttrMapping");
        beaconMapping = (HashMap) hashMap.get("downloadBeaconMapping");
    }

    public void generateHeaderTemplate(String mappingkey) {
        OperationBinding generateOpptyTemplate =
            ADFUtils.getBindingContainer().getOperationBinding("generateOpptyTemplate");
        generateOpptyTemplate.getParamsMap().put("pMappingKey", mappingkey);
        HashMap hashMap = (HashMap) generateOpptyTemplate.execute();
        opptyVoAttrMapping = (HashMap) hashMap.get("opptyVoAttrMapping");
        opptyMapping = (HashMap) hashMap.get("opptyMapping");
        //  return hashMap;
    }

    public XSSFWorkbook genericLogic(XSSFWorkbook workbook, String Iterator, int sheetrownum, int headerrownum) {
        //CellStyle for DataFormatting Number without Decimal
        DataFormat numFormatWithoutDecimal = workbook.createDataFormat(); //format defined
        XSSFCellStyle cellStyleNumber = workbook.createCellStyle();
        cellStyleNumber.setDataFormat(numFormatWithoutDecimal.getFormat("#,###.0000"));
        cellStyleNumber.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        cellStyleNumber.setBorderTop(XSSFCellStyle.BORDER_THIN);
        cellStyleNumber.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        cellStyleNumber.setBorderRight(XSSFCellStyle.BORDER_THIN);
        
        //CellStyle for 0 value from DB
        XSSFCellStyle cellStyleFor0 = workbook.createCellStyle();
        cellStyleFor0.setDataFormat(numFormatWithoutDecimal.getFormat("0.0000"));
        cellStyleFor0.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        cellStyleFor0.setBorderTop(XSSFCellStyle.BORDER_THIN);
        cellStyleFor0.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        cellStyleFor0.setBorderRight(XSSFCellStyle.BORDER_THIN);


        //Below cell style is for Border
        XSSFCellStyle cellStyleForBorder = workbook.createCellStyle();
        cellStyleForBorder.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        cellStyleForBorder.setBorderTop(XSSFCellStyle.BORDER_THIN);
        cellStyleForBorder.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        cellStyleForBorder.setBorderRight(XSSFCellStyle.BORDER_THIN);
        XSSFSheet sheet = workbook.getSheetAt(0);

        DCBindingContainer bindings = this.getDCBindingsContainer();
        DCIteratorBinding itorBinding = bindings.findIteratorBinding(Iterator);
        ViewObject object = itorBinding.getViewObject();
        
        RowSetIterator iterator = object.createRowSetIterator(null);
        int i = sheetrownum;
        logger.log(Level.INFO, getClass(), "genericLogic", "Loop started for generic Parts download");

        org.apache.poi.ss.usermodel.Row headerRow = sheet.getRow(headerrownum);
        Row currentRow = null;
        XSSFRow sheetRow = null;
        int logHeaderOnce = 0;
        try {

            while (iterator.hasNext()) { //Row iteration
                currentRow = iterator.next();
                sheetRow = sheet.createRow(i);
                i++;
                int index = 0; //Column
                for (index = 0; index < currentRow.getAttributeCount(); index++) {
                    Cell newCell = headerRow.getCell(index);
                    if (newCell != null) {
                        XSSFCell currentCell = sheetRow.createCell(index);
                        currentCell.setCellStyle(cellStyleForBorder);


                        if (newCell.getCellTypeEnum() != CellType.BLANK) {
                            String newHeader = newCell.getStringCellValue();
                            if (logHeaderOnce == 0) {
                                logger.log(Level.INFO, getClass(), "genericLogic",
                                           "Header value of new cell= " + newHeader);
                            }
                            String destnFieldName = null;
                            if (beaconMapping.get(newHeader) != null) {
                                destnFieldName = (String) beaconMapping.get(newHeader)[0];
                            }

                            String voAttribute = null;
                            String dataFormat = "";
                            if (destnFieldName != null) {
                                voAttribute = quoteVoAttrMapping.get(destnFieldName);
                                dataFormat = (String) beaconMapping.get(newHeader)[1];
                            } else {
                                //If any column mappings r not found in mappings table
                                voAttribute = null;
                                dataFormat = "";
                            }
                            if (null != currentRow && voAttribute != null &&
                                currentRow.getAttribute(voAttribute) != null &&
                                "" != currentRow.getAttribute(voAttribute).toString()) {

                                switch (dataFormat) {
                                case "COST": //This is for celltype with numeric value with decimal & comma
                                    currentCell.setCellType(CellType.NUMERIC);
                                    if(Double.valueOf(currentRow.getAttribute(voAttribute).toString())< 1.0 ){
                                        currentCell.setCellStyle(cellStyleFor0);
                                        currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(voAttribute)
                                                                                .toString()));
                                    }
                                    else{
                                    currentCell.setCellStyle(cellStyleNumber);
                                    currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(voAttribute)
                                                                            .toString()));
                                    }
                                    break;
                                case "NUMBER": //This is for celltype with numeric value
                                    currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(voAttribute)
                                                                            .toString()));
                                    break;
                                default:
                                    currentCell.setCellValue(currentRow.getAttribute(voAttribute).toString());
                                }

                            } else {
                                if(newHeader.equalsIgnoreCase(ViewConstants.FINAL_BEACON_DOWNLOAD_HEADERS[0])){                                     
                                    currentCell.setCellType(CellType.NUMERIC);
                                    currentCell.setCellStyle(cellStyleFor0);
                                    currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(ViewConstants.FINAL_PARTS_VO_ATTIBUTES[0]).toString()));
                                }
                                if(newHeader.equalsIgnoreCase(ViewConstants.FINAL_BEACON_DOWNLOAD_HEADERS[1])){
                                    currentCell.setCellType(CellType.NUMERIC);
                                    currentCell.setCellStyle(cellStyleFor0);
                                    currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(ViewConstants.FINAL_PARTS_VO_ATTIBUTES[1]).toString()));
                                }
                                if(!newHeader.equalsIgnoreCase(ViewConstants.FINAL_BEACON_DOWNLOAD_HEADERS[0]) && !newHeader.equalsIgnoreCase(ViewConstants.FINAL_BEACON_DOWNLOAD_HEADERS[1]))
                                    currentCell.setCellValue("");
                                //Below logic is to Handle ASL which is coming from Opportunity table & not from QuoteParts
                                //                                if ("ASL".equals(newHeader)){
                                //                                    currentCell.setCellValue("ASLValue");
                                //                                }
                            }

                        }

                    }
                }
                logHeaderOnce++;
            }
        } catch (Exception nfe) {

            nfe.printStackTrace();
        }

        return workbook;

    }

    public String getPricingMgr(String opptyNo) {
        User user = new User();
        OperationBinding getPricingMgr =
            ADFUtils.getBindingContainer().getOperationBinding("fetchUserForOpportunityRole");
        getPricingMgr.getParamsMap().put("opportunityNumber", opptyNo);
        getPricingMgr.getParamsMap().put("role", (String)ParamCache.userRoleMap.get("CCT_STRATEGIC_PRICING"));
        user = (User) getPricingMgr.execute();
        String contractsRep = user.getName();
        return contractsRep;

    }

    public int writeHeader(XSSFWorkbook workbook, String iterator, String mappingKey, int lineNo) {


        try{        

        XSSFSheet sheet = workbook.getSheetAt(0);
        logger.log(Level.INFO, getClass(), "writeHeader", "Getting sheet");

        DCBindingContainer bindings = this.getDCBindingsContainer();
        // For filling Opportunity details
        DCIteratorBinding opportunityIterator = bindings.findIteratorBinding(iterator);
        Row r = opportunityIterator.getCurrentRow();

        generateHeaderTemplate(mappingKey);
        int cnt = 0;
        HashMap<String, String> tempMap = opptyMapping;

        //   Iterator it = tempMap.entrySet().iterator();

        for (int i = 0; i <= lineNo; i++) {
            XSSFRow row = sheet.getRow(i);
            String header = (String) row.getCell(0).toString();
            String voAttribute = opptyVoAttrMapping.get(opptyMapping.get(header));
            logger.log(Level.INFO, getClass(), "writeHeader", "VO Attribute->" + voAttribute);
            if (header.equalsIgnoreCase("Contracts Representative")) {
                String opptyNo = (String) JSFUtils.resolveExpression("#{bindings.OpportunityNumber.inputValue}");
                String contractsRep = getPricingMgr(opptyNo);
                row.getCell(5).setCellValue(contractsRep);
            }
            if (voAttribute != null) {
                if (header.equalsIgnoreCase(ViewConstants.DATE_DUE_BACK_CONTRACTS)) {
                    //  String customerDate = ((String) r.getAttribute(voAttribute).toString()).substring(0, 10);
                    java.sql.Timestamp customerDate = (Timestamp) r.getAttribute(voAttribute);                  

                     /**enhancement CHG0041086 **/
                    
                // String newDate = subFourDays(customerDate);
//                    row.getCell(5).setCellValue(newDate);             
                
                 String queue = null;
                    Date maxDate = null;
            if(JSFUtils.resolveExpression("#{pageFlowScope.queueid}").toString().equals("3")){
                queue = "Lighting";
            }else if(JSFUtils.resolveExpression("#{pageFlowScope.queueid}").toString().equals("5")){
                queue = "Chemical";
            }else if(JSFUtils.resolveExpression("#{pageFlowScope.queueid}").toString().equals("10")){
                queue = "Electrical";
            }
            if(null!= queue){
                
            
            oracle.adf.model.OperationBinding op = (oracle.adf.model.OperationBinding) BindingContext.getCurrent()
                                                                   .getCurrentBindingsEntry()
                                                                   .getOperationBinding("fetchTasksListByOppNo");
            op.getParamsMap().put("opportunityNumber", (String) JSFUtils.resolveExpression("#{bindings.OpportunityNumber.inputValue}"));
            op.getParamsMap().put("userID", (String) JSFUtils.resolveExpression("{securityContext.userName}"));
            op.execute();     
        DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter1 = binds.findIteratorBinding("fetchTasksListByOppNoIterator");
        ViewObject taskvo = iter1.getViewObject();
        Row filteredRows[] = taskvo.getFilteredRows("groupName", queue);
        List<java.util.Date> date = new ArrayList<java.util.Date>();
        String sDate1=null;
        java.util.Date date1 = new java.util.Date();
        

        for (Row rr : filteredRows) {
        
             sDate1=rr.getAttribute("creationDate").toString();
           
                date1 = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy").parse(sDate1);
            
           
        date.add(date1); 
        }
         maxDate= Collections.max(date);
                for (Row rr : filteredRows) {
                
                     sDate1=rr.getAttribute("creationDate").toString();
                   
                        date1 = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy").parse(sDate1);
                   System.out.println("creationmaxDate--"+maxDate+"crationDate--"+date1+"--compare----"+date1.compareTo(maxDate));
                    if(date1.compareTo(maxDate) == 0){
                        maxDate = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy").parse(rr.getAttribute("taskDueDate").toString());
                        break;
                    }
                }
            }
//            if(null!= maxDate.){
//                row.getCell(5).setCellValue(maxDate);
//            }else{
//                row.getCell(5).setCellValue(customerDate);
//            }
            row.getCell(5).setCellValue(maxDate);
        /** CHG0041086 **/
                   
                } else {
                    if (r.getAttribute(voAttribute) != null)
                        row.getCell(5).setCellValue((String) r.getAttribute(voAttribute).toString());
                    else
                        row.getCell(5).setCellValue("");
                }
            }
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        return lineNo;

        //        while (it.hasNext()) {
        //            XSSFRow row = sheet.getRow(cnt);
        //            String header = (String) row.getCell(0).toString();
        //
        //            cnt++;
        //            HashMap.Entry entry = (HashMap.Entry) it.next();
        //            String key = (String) entry.getKey();
        //            String val = (String) entry.getValue();
        //            logger.log(Level.INFO, getClass(), "writeHeader", "key,val:" + key + "," + val);
        //            String voAttribute = opptyVoAttrMapping.get(opptyMapping.get(header));
        //            logger.log(Level.INFO, getClass(), "writeHeader", "VO Attribute->" + voAttribute);
        //
        //            if (voAttribute != null) {
        //                if (header.equalsIgnoreCase(ViewConstants.DATE_DUE_BACK_CONTRACTS)) {
        //                    //  String customerDate = ((String) r.getAttribute(voAttribute).toString()).substring(0, 10);
        //                    java.sql.Timestamp customerDate = (Timestamp) r.getAttribute(voAttribute);
        //                    String newDate = subFourDays(customerDate);
        //
        //                    row.getCell(5).setCellValue(newDate);
        //                } else if (header.equalsIgnoreCase("Contracts Representative")) {
        //
        //                    String contractsRep = (String) JSFUtils.resolveExpression("#{sessionScope.loggedInUser.userName}");
        //                    row.getCell(5).setCellValue(contractsRep);
        //                } else {
        //                    if (r.getAttribute(voAttribute) != null)
        //                        row.getCell(5).setCellValue((String) r.getAttribute(voAttribute).toString());
        //                    else
        //                        row.getCell(5).setCellValue("");
        //                }
        //            }
        //        }
        //        logger.log(Level.INFO, getClass(), "writeHeader", "Count after iteration=" + cnt);

    }

    public String subFourDays(Timestamp customerDate) {
        String newDate = null;
        DateFormat format = new SimpleDateFormat("MM/dd/yyyy");

        java.util.Date date = null;
        java.util.Date beforeFourDays = null;
        try {
            date = new java.util.Date(customerDate.getTime());

            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.DATE, -4);
            beforeFourDays = cal.getTime();
            newDate = format.format(beforeFourDays);
           
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newDate;
    }

    public void downloadParts(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...

        try {
           // ADFContext adfcontext = ADFContext.getCurrent();
           // String lineNumber = (String) adfcontext.getRequestScope().get("lineNumber");
           String lineNumber = (String) JSFUtils.resolveExpression("#{pageFlowScope.lineNumber}");
            int lineNo = Integer.parseInt(lineNumber);

            generateDownloadTemplate(ViewConstants.ACID_REP_KEY);

            facesContext.getCurrentInstance();
            String fileName = ViewConstants.ACID_REPORT_TEMPLATE;
            String filePath = ViewConstants.FILE_PATH + fileName;
            FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);
            logger.log(Level.INFO, getClass(), "downloadParts", "Excel read");

            XSSFWorkbook workbook = new XSSFWorkbook(file);

            int cnt = writeHeader(workbook, "OpportunityVO1Iterator", ViewConstants.ACID_REP_HEADER_KEY, lineNo);

            XSSFWorkbook filledWorkbook = genericLogic(workbook, "ChemicalQuotePartsVO1Iterator", cnt + 3, cnt + 2);
            XSSFSheet sheet = filledWorkbook.getSheetAt(0);
            XSSFRow topRow = sheet.getRow(7);
            int lovCol = 0;
            for (Cell c : topRow) {
                if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                    String text = c.getStringCellValue();
                    if ("Work Queue".equalsIgnoreCase(text)) {
                        lovCol = c.getColumnIndex();
                        break;
                    }
                }
            }
            filledWorkbook = addExcelLOV(filledWorkbook, 1, lovCol);
            try {
                logger.log(Level.INFO, getClass(), "downloadParts", "Writing workbook");

                filledWorkbook.write(outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (Exception e) {
                logger.log(Level.SEVERE, getClass(), "downloadParts", e.getMessage());
                CustomErrorHandler.processError("CCT", "downloadParts", "CCT_SYS_EX34", e);
            }

        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "downloadParts", e.getMessage());
            CustomErrorHandler.processError("CCT", "downloadParts", "CCT_SYS_EX34", e);
        }
    }

    public void downloadFinalBeacon(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        try {
            BigDecimal quoteRevisionId = (BigDecimal)JSFUtils.resolveExpression("#{bindings.RevisionId.inputValue}");
            DCBindingContainer binds = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding QuotePartsFinalDownloadVO1Iterator = binds.findIteratorBinding("QuotePartsFinalDownloadVO1Iterator");
            ViewObject QuotePartsFinalDownloadVO = QuotePartsFinalDownloadVO1Iterator.getViewObject();
            QuotePartsFinalDownloadVO.setNamedWhereClauseParam("b_QuoteRevisionId", quoteRevisionId);
            QuotePartsFinalDownloadVO.executeQuery();
           
            generateFinalDownloadTemplate(ViewConstants.FINAL_BEACON_DOWNLOAD_KEY);

            facesContext.getCurrentInstance();
            String fileName = ViewConstants.FINAL_BEACON_TEMPLATE;

            String filePath = ViewConstants.FILE_PATH + fileName;
            FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFWorkbook filledWorkbook = genericLogic(workbook, "QuotePartsFinalDownloadVO1Iterator", 1, 0);
            XSSFSheet sheet = filledWorkbook.getSheetAt(0);
            XSSFRow topRow = sheet.getRow(0);
            int lovCol = 0;
            for (Cell c : topRow) {
                if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                    String text = c.getStringCellValue();
                    if (ViewConstants.WORK_QUEUE.equalsIgnoreCase(text)) {
                        lovCol = c.getColumnIndex();
                        break;
                    }
                }
            }
            filledWorkbook = addExcelLOV(filledWorkbook, 1, lovCol);

            try {
                logger.log(Level.INFO, getClass(), "downloadBeacon", "Writing workbook");
                filledWorkbook.write(outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (Exception e) {
                logger.log(Level.SEVERE, getClass(), "downloadBeacon", e.getMessage());
                CustomErrorHandler.processError("CCT", "downloadBeacon", "CCT_SYS_EX35", e);
            }


        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "downloadBeacon", e.getMessage());
            CustomErrorHandler.processError("CCT", "downloadBeacon", "CCT_SYS_EX35", e);
        }
    }

    public void downloadBeacon(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        try {
            generateDownloadTemplate(ViewConstants.BEACON_DOWNLOAD_KEY);

            facesContext.getCurrentInstance();
            String fileName = ViewConstants.BEACON_TEMPLATE;

            String filePath = ViewConstants.FILE_PATH + fileName;
            FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFWorkbook filledWorkbook = genericLogic(workbook, "AllPartsDownloadVO1Iterator", 1, 0);
            XSSFSheet sheet = filledWorkbook.getSheetAt(0);
            XSSFRow topRow = sheet.getRow(0);
            int lovCol = 0;
            for (Cell c : topRow) {
                if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                    String text = c.getStringCellValue();
                    if (ViewConstants.WORK_QUEUE.equalsIgnoreCase(text)) {
                        lovCol = c.getColumnIndex();
                        break;
                    }
                }
            }
            filledWorkbook = addExcelLOV(filledWorkbook, 1, lovCol);

            try {
                logger.log(Level.INFO, getClass(), "downloadBeacon", "Writing workbook");
                filledWorkbook.write(outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (Exception e) {
                logger.log(Level.SEVERE, getClass(), "downloadBeacon", e.getMessage());
                CustomErrorHandler.processError("CCT", "downloadBeacon", "CCT_SYS_EX35", e);
            }


        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "downloadBeacon", e.getMessage());
            CustomErrorHandler.processError("CCT", "downloadBeacon", "CCT_SYS_EX35", e);
        }
    }

    public void downloadUnknown(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        try {
            generateDownloadTemplate(ViewConstants.UNKNOWN_DOWNLOAD_KEY);

            facesContext.getCurrentInstance();
            String fileName = ViewConstants.UNKNOWN_TEMPLATE;

            String filePath = ViewConstants.FILE_PATH + fileName;
            FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFWorkbook filledWorkbook = genericLogic(workbook, "UnknownQuotePartsVO1Iterator", 1, 0);
            XSSFSheet sheet = filledWorkbook.getSheetAt(0);
            XSSFRow topRow = sheet.getRow(0);
            int lovCol = 0;
            for (Cell c : topRow) {
                if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                    String text = c.getStringCellValue();
                    if (ViewConstants.WORK_QUEUE.equalsIgnoreCase(text)) {
                        lovCol = c.getColumnIndex();
                        break;
                    }
                }
            }
            filledWorkbook = addExcelLOV(filledWorkbook, 1, lovCol);

            try {
                logger.log(Level.INFO, getClass(), "downloadUnknown", "Writing workbook");
                filledWorkbook.write(outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (Exception e) {
                logger.log(Level.SEVERE, getClass(), "downloadUnknown", e.getMessage());
                CustomErrorHandler.processError("CCT", "downloadUnknown", "CCT_SYS_EX35", e);
            }


        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "downloadUnknown", e.getMessage());
            CustomErrorHandler.processError("CCT", "downloadUnknown", "CCT_SYS_EX35", e);
        }
    }


    public void downloadHPP(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        try {
            generateDownloadTemplate(ViewConstants.HPP_DOWNLOAD_KEY);

            facesContext.getCurrentInstance();
            String fileName = ViewConstants.HPP_TEMPLATE;
            String filePath = ViewConstants.FILE_PATH + fileName;
            FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFWorkbook filledWorkbook = genericLogic(workbook, "HPPQuotePartsVO1Iterator", 1, 0);
            XSSFSheet sheet = filledWorkbook.getSheetAt(0);
            XSSFRow topRow = sheet.getRow(0);
            int lovCol = 0;
            for (Cell c : topRow) {
                if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                    String text = c.getStringCellValue();
                    if ("Work Queue".equalsIgnoreCase(text)) {
                        lovCol = c.getColumnIndex();
                        break;
                    }
                }
            }
            filledWorkbook = addExcelLOV(filledWorkbook, 1, lovCol);

            try {
                logger.log(Level.INFO, getClass(), "downloadHPP", "Writing workbook");
                filledWorkbook.write(outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (Exception e) {
                logger.log(Level.SEVERE, getClass(), "downloadHPP", e.getMessage());
                CustomErrorHandler.processError("CCT", "downloadHPP", "CCT_SYS_EX36", e);
            }


        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "downloadHPP", e.getMessage());
            CustomErrorHandler.processError("CCT", "downloadHPP", "CCT_SYS_EX36", e);
        }

    }

    public void downloadLighting(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        try {
//            ADFContext adfcontext = ADFContext.getCurrent();
//            String lineNumber = (String) adfcontext.getRequestScope().get("lineNumber");
             String lineNumber = (String) JSFUtils.resolveExpression("#{pageFlowScope.lineNumber}");
            int lineNo = Integer.parseInt(lineNumber);
            generateDownloadTemplate(ViewConstants.LIGHTING_DOWNLOAD_KEY);
            facesContext.getCurrentInstance();
            String fileName = ViewConstants.FLASHLIGHT_TEMPLATE;

            String filePath = ViewConstants.FILE_PATH + fileName;
            FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);

            XSSFWorkbook workbook = new XSSFWorkbook(file);
            int cnt = writeHeader(workbook, "OpportunityVO1Iterator", ViewConstants.LIGHTING_HEADER_KEY, lineNo);

            XSSFWorkbook filledWorkbook = genericLogic(workbook, "LightingQuotePartsVO1Iterator", cnt + 3, cnt + 2);
            XSSFSheet sheet = filledWorkbook.getSheetAt(0);
            XSSFRow topRow = sheet.getRow(5);
            int lovCol = 0;
            for (Cell c : topRow) {
                if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                    String text = c.getStringCellValue();
                    if ("Work Queue".equalsIgnoreCase(text)) {
                        lovCol = c.getColumnIndex();
                        break;
                    }
                }
            }
            filledWorkbook = addExcelLOV(filledWorkbook, 1, lovCol);
            try {
                logger.log(Level.INFO, getClass(), "downloadLighting", "Writing workbook");
                filledWorkbook.write(outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (Exception e) {
                logger.log(Level.SEVERE, getClass(), "downloadLighting", e.getMessage());
                CustomErrorHandler.processError("CCT", "downloadLighting", "CCT_SYS_EX37", e);
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "downloadLighting", e.getMessage());
            CustomErrorHandler.processError("CCT", "downloadLighting", "CCT_SYS_EX37", e);

        }
    }
    
    public void downloadElectrical(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        try {
    //            ADFContext adfcontext = ADFContext.getCurrent();
    //            String lineNumber = (String) adfcontext.getRequestScope().get("lineNumber");
             String lineNumber = (String) JSFUtils.resolveExpression("#{pageFlowScope.lineNumber}");
            int lineNo = Integer.parseInt(lineNumber);
            generateDownloadTemplate(ViewConstants.ELECTRICAL_DOWNLOAD_KEY);
            facesContext.getCurrentInstance();
            String fileName = ViewConstants.ELECTRICAL_QUEUE_TEMPLATE;

            String filePath = ViewConstants.FILE_PATH + fileName;
            FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);

            XSSFWorkbook workbook = new XSSFWorkbook(file);
            int cnt = writeHeader(workbook, "OpportunityVO1Iterator", ViewConstants.LIGHTING_HEADER_KEY, lineNo);

            XSSFWorkbook filledWorkbook = genericLogic(workbook, "ElectricalQuotePartsVO1Iterator", cnt + 3, cnt + 2);
            XSSFSheet sheet = filledWorkbook.getSheetAt(0);
            XSSFRow topRow = sheet.getRow(5);
            int lovCol = 0;
            for (Cell c : topRow) {
                if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                    String text = c.getStringCellValue();
                    if ("Work Queue".equalsIgnoreCase(text)) {
                        lovCol = c.getColumnIndex();
                        break;
                    }
                }
            }
            filledWorkbook = addExcelLOV(filledWorkbook, 1, lovCol);
            try {
                logger.log(Level.INFO, getClass(), "downloadElectrical", "Writing workbook");
                filledWorkbook.write(outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (Exception e) {
                logger.log(Level.SEVERE, getClass(), "downloadElectrical", e.getMessage());
                CustomErrorHandler.processError("CCT", "downloadElectrical", "CCT_SYS_EX37", e);
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "downloadElectrical", e.getMessage());
            CustomErrorHandler.processError("CCT", "downloadElectrical", "CCT_SYS_EX37", e);

        }
    }
    
    public void downloadAST(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        try {
            generateDownloadTemplate(ViewConstants.AST_DOWNLOAD_KEY);

            facesContext.getCurrentInstance();
            String fileName = ViewConstants.AST_TEMPLATE;
            String filePath = ViewConstants.FILE_PATH + fileName;
            FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFWorkbook filledWorkbook = genericLogic(workbook, "AdvancedSourcingQuotePartsVO1Iterator", 1, 0);
            try {
                XSSFSheet sheet = filledWorkbook.getSheetAt(0);
                XSSFRow topRow = sheet.getRow(0);
                int lovCol = 0;
                for (Cell c : topRow) {
                    if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                        String text = c.getStringCellValue();
                        if (ViewConstants.WORK_QUEUE.equalsIgnoreCase(text)) {
                            lovCol = c.getColumnIndex();
                            break;
                        }
                    }
                }
                filledWorkbook = addExcelLOV(filledWorkbook, 1, lovCol);
            } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
            }

            try {
                logger.log(Level.INFO, getClass(), "downloadAST", "Writing workbook");
                filledWorkbook.write(outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (Exception e) {
                logger.log(Level.SEVERE, getClass(), "downloadAST", e.getMessage());
                CustomErrorHandler.processError("CCT", "downloadAST", "CCT_SYS_EX38", e);
            }


        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "downloadAST", e.getMessage());
            CustomErrorHandler.processError("CCT", "downloadAST", "CCT_SYS_EX38", e);
        }
    }

    public void downloadCommodity(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        try {
            generateDownloadTemplate(ViewConstants.AST_DOWNLOAD_KEY);

            facesContext.getCurrentInstance();
            String fileName = "CommodityTemplate.xlsx";
            String filePath = ViewConstants.FILE_PATH + fileName;
            FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFWorkbook filledWorkbook = genericLogic(workbook, "CommodityQuotePartsVO1Iterator", 1, 0);
            try {
                XSSFSheet sheet = filledWorkbook.getSheetAt(0);
                XSSFRow topRow = sheet.getRow(0);
                int lovCol = 0;
                for (Cell c : topRow) {
                    if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                        String text = c.getStringCellValue();
                        if ("Work Queue".equalsIgnoreCase(text)) {
                            lovCol = c.getColumnIndex();
                            break;
                        }
                    }
                }
                filledWorkbook = addExcelLOV(filledWorkbook, 1, lovCol);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                logger.log(Level.INFO, getClass(), "downloadCommodity", "Writing workbook");
                filledWorkbook.write(outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (Exception e) {
                logger.log(Level.SEVERE, getClass(), "downloadCommodity", e.getMessage());
                CustomErrorHandler.processError("CCT", "downloadCommodity", "CCT_SYS_EX38", e);
            }


        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "downloadCommodity", e.getMessage());
            CustomErrorHandler.processError("CCT", "downloadCommodity", "CCT_SYS_EX38", e);
        }
    }


    public void downloadNoBid(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        try {
            generateDownloadTemplate(ViewConstants.NOBID_DOWNLOAD_KEY);

            facesContext.getCurrentInstance();
            String fileName = "NoBidTemplate.xlsx";
            String filePath = ViewConstants.FILE_PATH + fileName;
            FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFWorkbook filledWorkbook = genericLogic(workbook, "NoBidQuotePartsVO1Iterator", 1, 0);
            try {
                XSSFSheet sheet = filledWorkbook.getSheetAt(0);
                XSSFRow topRow = sheet.getRow(0);
                int lovCol = 0;
                for (Cell c : topRow) {
                    if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                        String text = c.getStringCellValue();
                        if ("Work Queue".equalsIgnoreCase(text)) {
                            lovCol = c.getColumnIndex();
                            break;
                        }
                    }
                }
                filledWorkbook = addExcelLOV(filledWorkbook, 1, lovCol);
            } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
            }
            try {
                logger.log(Level.INFO, getClass(), "downloadNoBid", "Writing workbook");
                filledWorkbook.write(outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (Exception e) {
                logger.log(Level.SEVERE, getClass(), "downloadNoBid", e.getMessage());
                CustomErrorHandler.processError("CCT", "downloadNoBid", "CCT_SYS_EX38", e);
            }


        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "downloadNoBid", e.getMessage());
            CustomErrorHandler.processError("CCT", "downloadNoBid", "CCT_SYS_EX38", e);
        }
    }

    public void downloadStrategicPricing(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        try {
            generateDownloadTemplate(ViewConstants.SP_DOWNLOAD_KEY);

            facesContext.getCurrentInstance();
            String fileName = "StratPricingTemplate.xlsx";
            String filePath = ViewConstants.FILE_PATH + fileName;
            FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFWorkbook filledWorkbook = genericLogic(workbook, "StrategicPricingQuotePartsVO1Iterator", 1, 0);
            try {
                XSSFSheet sheet = filledWorkbook.getSheetAt(0);
                XSSFRow topRow = sheet.getRow(0);
                int lovCol = 0;
                for (Cell c : topRow) {
                    if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                        String text = c.getStringCellValue();
                        if ("Work Queue".equalsIgnoreCase(text)) {
                            lovCol = c.getColumnIndex();
                            break;
                        }
                    }
                }
                filledWorkbook = addExcelLOV(filledWorkbook, 1, lovCol);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                logger.log(Level.INFO, getClass(), "downloadStrategicPricing", "Writing workbook");
                filledWorkbook.write(outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (Exception e) {
                logger.log(Level.SEVERE, getClass(), "downloadStrategicPricing", e.getMessage());
                CustomErrorHandler.processError("CCT", "downloadStrategicPricing", "CCT_SYS_EX38", e);
            }


        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "downloadStrategicPricing", e.getMessage());
            CustomErrorHandler.processError("CCT", "downloadStrategicPricing", "CCT_SYS_EX38", e);
        }
    }


    public String[] getQueueNames() {
        DCBindingContainer bindingContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding binding = bindingContainer.findIteratorBinding("QuotePartsQueueVO1Iterator");
        ViewObject object = binding.getViewObject();
        int size = object.getRowCount();
        String[] queues = new String[size];
        ArrayList<String> queueNameList = new ArrayList<String>();

        RowSetIterator iterator = object.createRowSetIterator(null);
        while (iterator.hasNext()) {
            Row currentRow = iterator.next();
            String queueName = (String) currentRow.getAttribute("QueueName");
            logger.log(Level.INFO, getClass(), "getQueueNames", "QueueName= " + queueName);
            queueNameList.add(queueName);

        }
        String[] tempArray = new String[queueNameList.size()];
        queues = queueNameList.toArray(tempArray);
        return queues;
    }

    public XSSFWorkbook addExcelLOV(XSSFWorkbook workbook, int rowNum, int colNum) {

        String[] queues = getQueueNames();

        XSSFSheet sheet = workbook.getSheetAt(0);
        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sheet);
        XSSFDataValidationConstraint dvConstraint =
            (XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(queues);
        CellRangeAddressList addressList = new CellRangeAddressList(rowNum, sheet.getLastRowNum(), colNum, colNum);
        XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList);
        validation.setShowErrorBox(true);
        validation.setErrorStyle(XSSFDataValidation.ErrorStyle.STOP);
        validation.createErrorBox("Criteria Mismatch", ViewConstants.LOV_ERROR_MESSAGE);
        sheet.addValidationData(validation);
        validation.setSuppressDropDownArrow(false);


        return workbook;
    }


    public void downloadAllParts(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        try {
                   generateDownloadTemplate(ViewConstants.ALLPARTS_DOWNLOAD_KEY);

                   facesContext.getCurrentInstance();
                   String fileName = "AllPartsDownloadTemplate.xlsx";
                   String filePath = ViewConstants.FILE_PATH + fileName;
                   FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);
                   XSSFWorkbook workbook = new XSSFWorkbook(file);
                   XSSFWorkbook filledWorkbook = genericLogic(workbook, "AllQuotePartsVO1Iterator", 1, 0);
                   try {
                       XSSFSheet sheet = filledWorkbook.getSheetAt(0);
                       XSSFRow topRow = sheet.getRow(0);
                       int lovCol = 0;
                       for (Cell c : topRow) {
                           if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                               String text = c.getStringCellValue();
                               if ("Work Queue".equalsIgnoreCase(text)) {
                                   lovCol = c.getColumnIndex();
                                   break;
                               }
                           }
                       }
                       filledWorkbook = addExcelLOV(filledWorkbook, 1, lovCol);
                   } catch (Exception e) {
                       e.printStackTrace();
                   }
                   try {
                       logger.log(Level.INFO, getClass(), "downloadAllParts", "Writing workbook");
                       filledWorkbook.write(outputStream);
                       outputStream.flush();
                       outputStream.close();
                   } catch (Exception e) {
                       logger.log(Level.SEVERE, getClass(), "downloadAllParts", e.getMessage());
                       CustomErrorHandler.processError("CCT", "downloadAllParts", "CCT_SYS_EX38", e);
                   }


               } catch (Exception e) {
                   logger.log(Level.SEVERE, getClass(), "downloadAllParts", e.getMessage());
                   CustomErrorHandler.processError("CCT", "downloadAllParts", "CCT_SYS_EX38", e);
               }

    }

    public void downloadTOP50Parts(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        try {
                   generateFinalTop50DownloadTemplate(ViewConstants.TOP50_PARTS_DOWNLOAD_KEY);

                   facesContext.getCurrentInstance();
                   String fileName = "TOP50BeaconDownloadTemplate.xlsx";
                   String filePath = ViewConstants.FILE_PATH + fileName;
                   FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);
                   XSSFWorkbook workbook = new XSSFWorkbook(file);
                   XSSFWorkbook filledWorkbook = genericLogic(workbook, "CctTop50PartsVOIterator", 1, 0);
                   try {
                       XSSFSheet sheet = filledWorkbook.getSheetAt(0);
                       XSSFRow topRow = sheet.getRow(0);
                       int lovCol = 0;
                       for (Cell c : topRow) {
                           if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                               String text = c.getStringCellValue();
                               if ("Work Queue".equalsIgnoreCase(text)) {
                                   lovCol = c.getColumnIndex();
                                   break;
                               }
                           }
                       }
                       filledWorkbook = addExcelLOV(filledWorkbook, 1, lovCol);
                   } catch (Exception e) {
                       e.printStackTrace();
                   }
                   try {
                       logger.log(Level.INFO, getClass(), "downloadAllParts", "Writing workbook");
                       filledWorkbook.write(outputStream);
                       outputStream.flush();
                       outputStream.close();
                   } catch (Exception e) {
                       logger.log(Level.SEVERE, getClass(), "downloadAllParts", e.getMessage());
                       CustomErrorHandler.processError("CCT", "downloadAllParts", "CCT_SYS_EX38", e);
                   }


               } catch (Exception e) {
                   logger.log(Level.SEVERE, getClass(), "downloadAllParts", e.getMessage());
                   CustomErrorHandler.processError("CCT", "downloadAllParts", "CCT_SYS_EX38", e);
               }


    }

    public void downloadTOP50FinalBeaconFile(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        try {
                   generateFinalTop50DownloadTemplate(ViewConstants.TOP50_FINAL_PARTS_DOWNLOAD_KEY);

                   facesContext.getCurrentInstance();
                   String fileName = ViewConstants.TOP_50_FINAL_DOWNLOAD_TEMPLATE;
                   String filePath = ViewConstants.FILE_PATH + fileName;
                   FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);
                   XSSFWorkbook workbook = new XSSFWorkbook(file);
                   XSSFWorkbook filledWorkbook = genericLogic(workbook, "CctFinalTop50PartsViewIterator", 1, 0);
                   try {
                       XSSFSheet sheet = filledWorkbook.getSheetAt(0);
                       XSSFRow topRow = sheet.getRow(0);
                       int lovCol = 0;
                       for (Cell c : topRow) {
                           if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                               String text = c.getStringCellValue();
                               if ("Work Queue".equalsIgnoreCase(text)) {
                                   lovCol = c.getColumnIndex();
                                   break;
                               }
                           }
                       }
                       filledWorkbook = addExcelLOV(filledWorkbook, 1, lovCol);
                   } catch (Exception e) {
                       e.printStackTrace();
                   }
                   try {
                       logger.log(Level.INFO, getClass(), "downloadAllParts", "Writing workbook");
                       filledWorkbook.write(outputStream);
                       outputStream.flush();
                       outputStream.close();
                   } catch (Exception e) {
                       logger.log(Level.SEVERE, getClass(), "downloadAllParts", e.getMessage());
                       CustomErrorHandler.processError("CCT", "downloadAllParts", "CCT_SYS_EX38", e);
                   }


               } catch (Exception e) {
                   logger.log(Level.SEVERE, getClass(), "downloadAllParts", e.getMessage());
                   CustomErrorHandler.processError("CCT", "downloadAllParts", "CCT_SYS_EX38", e);
               }


    }
}
