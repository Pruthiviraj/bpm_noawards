package com.klx.view.beans.scope.pageFlow;

import com.klx.view.beans.utils.JSFUtils;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.jbo.ViewObject;

public class awardReportBean {
    public awardReportBean() {
    }

    public void awardReportChangeListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
 //       refreshAwardsQuoteParts();
     DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
     DCIteratorBinding awardsIter = binds.findIteratorBinding("AwardsQuotePartsVO11Iterator");
        OperationBinding op = (OperationBinding) BindingContext.getCurrent().getCurrentBindingsEntry().getOperationBinding("awardReports");
   //     op.getParamsMap().put("revisionID",awardsIter.getCurrentRow().getAttribute("RevisionId").toString()); 
        op.getParamsMap().put("criteria",valueChangeEvent.getNewValue());
 //       String msg="";
        op.execute();
 
 
    }
}
