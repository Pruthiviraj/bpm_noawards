package com.klx.view.beans.scope.pageFlow;

import com.klx.common.access.UserActionAccessManagement;
import com.klx.view.beans.constants.ViewConstants;
import com.klx.view.beans.utils.ADFUtils;

import com.klx.view.beans.utils.JSFUtils;

import java.io.OutputStream;

import java.text.DateFormat;

import java.text.SimpleDateFormat;

import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import com.klx.common.logger.KLXLogger;

import com.klx.model.constants.ModelConstants;

import java.io.IOException;

import java.math.BigDecimal;

import java.sql.Timestamp;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelAccordion;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.VariableValueManager;
import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewCriteriaManager;
import oracle.jbo.ViewObject;
import oracle.jbo.uicli.binding.JUCtrlListBinding;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;

import javax.faces.component.UIComponent;

import javax.faces.model.SelectItem;

import oracle.jbo.RowSetIterator;

import org.apache.myfaces.trinidad.event.DisclosureEvent;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class UpdateOpportunityBean {
    private RichPopup statusPopupBinding;
    private RichInputDate offrSentToCustomer;
    private RichPanelAccordion dateManagementAccordion;
    private String source;
    private RichPopup revisionPopupBinding;
    private RichInputText crfStatusBind;
    private RichSelectOneChoice opptyStatusBind;
    private RichInputDate winLoassDateBind;
    private RichInputDate closeDateBind;
    private RichInputText winLoassCommentBind;
    private RichInputText closeCommentBind;
	private static KLXLogger logger = KLXLogger.getLogger();
    private RichInputText custNoBind;
    private Date oldDueDate = null;
    private String changedOpptyOutcome = "";
    private RichPopup revisionConfirmationBinding;
    private RichPanelFormLayout revisionDatesFormBinding;
    private RichInputDate requestKickOffDateBind;
    private RichInputDate offerApprovalSentBind;
    private RichInputDate offerApprovalReceivedBind;
    private RichInputDate offerSentToCustBind;
    private boolean  klxOfferDisable = false;
    private RichPopup convertToOppBind;
    private boolean projectNumber = false;
    private boolean contractTeams = false;
    private RichPopup confirmConvertPopUp;
    private static final String linebreak = "\n";
    private static final String nextcell = "\t";

    private boolean convertOpportunity = false;
    private boolean createRevision = false;
    private boolean updateEditOpportunity = false;   
    private boolean addSearchNotes = false;   
    private ArrayList<SelectItem> LegalFormat = new ArrayList<SelectItem>();
    private ArrayList<SelectItem> returnPart = new ArrayList<SelectItem>();
    private RichPopup dueDatePopupBinding;

    public void setCreateRevision(boolean createRevision) {
        this.createRevision = createRevision;
    }

    public boolean isCreateRevision() {
        return createRevision;
    }

    public void setChangedOpptyOutcome(String changedOpptyOutcome) {
        this.changedOpptyOutcome = changedOpptyOutcome;
    }

    public String getChangedOpptyOutcome() {
        return changedOpptyOutcome;
    }

    public void setOldDueDate(Date oldDueDate) {
        this.oldDueDate = oldDueDate;
    }

    public Date getOldDueDate() {
        return oldDueDate;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSource() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)ctx.getExternalContext().getRequest();
        Map requestParamMap = ctx.getExternalContext().getRequestParameterMap();
        source = (String)requestParamMap.get(ViewConstants.SOURCE);        
        if (source == null || source.equalsIgnoreCase("")){
            source = ViewConstants.NOT_BPM;
        }        
        return source;
    }

    public UpdateOpportunityBean() {
        super();
        userActionsAccess();
    }
    
    private String crfNumber;
    private String successmsg="";
    private String notesDesc;
    private boolean editMode = false;
    private String revisionConfirmationmsg="";

    public void setRevisionConfirmationmsg(String revisionConfirmationmsg) {
        this.revisionConfirmationmsg = revisionConfirmationmsg;
    }

    public String getRevisionConfirmationmsg() {
        return revisionConfirmationmsg;
    }


    public void setNotesDesc(String notesDesc) {
        this.notesDesc = notesDesc;
    }

    public String getNotesDesc() {
        return notesDesc;
    }

    public void setSuccessmsg(String successmsg) {
        this.successmsg = successmsg;
    }

    public String getSuccessmsg() {
        return successmsg;
    }

    private String crfNo;

    public void setCrfNo(String crfNo) {
        this.crfNo = crfNo;
    }

    public String getCrfNo() {
        return crfNo;
    }

    public void setCrfNumber(String crfNumber) {
        this.crfNumber = crfNumber;
    }

    public String getCrfNumber() {
        return crfNumber;
    }

    public void searchOpportunityByCrfNo(ActionEvent actionEvent) {
        // Add event code here...
        String crfNumber = getCrfNumber();
    
        OperationBinding binding = ADFUtils.findOperationBinding("initializeUpdateOpp");
        binding.getParamsMap().put("crfNumber", crfNumber);
        binding.execute();
        
       
    }

    public void refreshNotes(){

        DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter1 = binds.findIteratorBinding("CctNotesUpdateOppVOIterator");
            iter1.executeQuery();
    }

    public void okACL(ActionEvent actionEvent) {
        getStatusPopupBinding().hide();
        getConvertToOppBind().hide();
    }

    public void setStatusPopupBinding(RichPopup statusPopupBinding) {
        this.statusPopupBinding = statusPopupBinding;
    }

    public RichPopup getStatusPopupBinding() {
        return statusPopupBinding;
    }
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();

    }
    public String saveDateAwardsDetails() {
        String crf_No = (JSFUtils.resolveExpression("#{bindings.CrfNo.inputValue}")).toString();
        
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("updateOpportunity");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        setSuccessmsg(ViewConstants.DATE_MGMT_SAVE_MSG);
      
        if(getOffrSentToCustomer().getValue() != null){
            OperationBinding binding = ADFUtils.findOperationBinding("changeCRFStatus");
            binding.getParamsMap().put("crfNumber", crf_No);
            binding.execute();
        }
        
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getStatusPopupBinding().show(hints);
        return null;
    }
    
    protected void refreshPage() {
        FacesContext fc = FacesContext.getCurrentInstance();
        String refreshpage = fc.getViewRoot().getViewId();
        ViewHandler ViewH = fc.getApplication().getViewHandler();
        UIViewRoot UIV = ViewH.createView(fc, refreshpage);
        UIV.setViewId(refreshpage);
        fc.setViewRoot(UIV);
    }
    
    public void openInEditMode(ActionEvent actionEvent) {
        this.editMode=true;        
        refreshPage();
        ADFContext.getCurrent().getRequestScope().put("KLXLegalFormat", "inVisible");
        ADFContext.getCurrent().getPageFlowScope().put("LegalFormat", "visible");
        ADFContext.getCurrent().getRequestScope().put("KLXReturnPart", "inVisible");
        ADFContext.getCurrent().getPageFlowScope().put("ReturnPart", "visible");
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setDateManagementAccordion(RichPanelAccordion dateManagementAccordion) {
        this.dateManagementAccordion = dateManagementAccordion;
    }

    public RichPanelAccordion getDateManagementAccordion() {
        return dateManagementAccordion;
    }

    public void addNotesData(ActionEvent actionEvent) {
        // Add event code here...
       // String crfNumber = getCrfNumber();
       String crfNumber = (JSFUtils.resolveExpression("#{bindings.CrfNo.inputValue}")).toString();
        
        BindingContainer bindings = BindingContext.getCurrent().getCurrentBindingsEntry();
        JUCtrlListBinding listBinding = (JUCtrlListBinding)bindings.get("NoteType");
        String noteType= (String) listBinding.getInputValue(); 
    
        
        OperationBinding binding = ADFUtils.findOperationBinding("addNotesData");
        binding.getParamsMap().put("noteType", noteType);
        binding.getParamsMap().put("notesDesc", getNotesDesc());
        binding.getParamsMap().put("crfNo", crfNumber);
        binding.execute();
        setNotesDesc(null);
    
      
        refreshNotes();
    }


    public void setOffrSentToCustomer(RichInputDate offrSentToCustomer) {
        this.offrSentToCustomer = offrSentToCustomer;
    }

    public RichInputDate getOffrSentToCustomer() {
        return offrSentToCustomer;
    }
    
//    CHG0043113 - User creating revision without saving the customer due date.
    private void saveUpdateOpportunity(){
        logger.log(Level.INFO, getClass(), "saveUpdateOpportunity", "Enetring saveUpdateOpportunity method");
        String crfID = null; 
        String opptyOutcomeinDB = null;
        
        Object crfno = JSFUtils.resolveExpression("#{bindings.CrfNo.inputValue}");
        Object crfStatus = JSFUtils.resolveExpression("#{bindings.CrfStatus.inputValue}");
        Object opptyStatus = getOpptyStatusBind().getValue();
        
        BindingContext bindingContext = BindingContext.getCurrent();
        BindingContainer bindingContainer = bindingContext.getCurrentBindingsEntry();
        OperationBinding opptyOutcomeBinding = bindingContainer.getOperationBinding("getOpptyOutcomeinDB");      
        opptyOutcomeBinding.getParamsMap().put("crfNo", crfno.toString());
        opptyOutcomeinDB = (String)opptyOutcomeBinding.execute();
        
        logger.log(Level.INFO, getClass(), "saveUpdateOpportunity", "Checking equality of Opptyoutcome in DB and in UI");
        
        // Fix for CHG0034887
        
        if (null == opptyOutcomeinDB && !"".equalsIgnoreCase((String) opptyStatus))
            setChangedOpptyOutcome((String) opptyStatus);
        else if (null != opptyOutcomeinDB && "".equalsIgnoreCase((String) opptyStatus))
            setChangedOpptyOutcome((String) opptyStatus);
        else if (null != opptyOutcomeinDB && !"".equalsIgnoreCase((String) opptyStatus)) {
            if (!opptyOutcomeinDB.equalsIgnoreCase((String) opptyStatus)) {
                setChangedOpptyOutcome((String) opptyStatus);
            }
        }
                     
        if (null != opptyStatus && !"".equalsIgnoreCase(opptyStatus.toString()))
            JSFUtils.setExpressionValue("#{bindings.CrfStatus.inputValue}", opptyStatus);
        else {
            JSFUtils.setExpressionValue("#{bindings.WinLossDate.inputValue}", null);
            getWinLoassCommentBind().setValue(null);
            getWinLoassCommentBind().setSubmittedValue(null);
            JSFUtils.setExpressionValue("#{bindings.CloseComments.inputValue}", null);
            JSFUtils.setExpressionValue("#{bindings.WinLossComments.inputValue}", null);
            if (null != crfStatus && !"Pending".equalsIgnoreCase(crfStatus.toString()))
                JSFUtils.setExpressionValue("#{bindings.CrfStatus.inputValue}", "Active");
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(getCrfStatusBind());
        AdfFacesContext.getCurrentInstance().addPartialTarget(getWinLoassDateBind());
        AdfFacesContext.getCurrentInstance().addPartialTarget(getWinLoassCommentBind());
        AdfFacesContext.getCurrentInstance().addPartialTarget(getCloseDateBind());
        AdfFacesContext.getCurrentInstance().addPartialTarget(getCloseCommentBind());
        
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("updateOpportunity");
        Object result = operationBinding.execute();
        
        OperationBinding binding = bindingContainer.getOperationBinding("updateToOpportunity");      
        binding.getParamsMap().put("crfNo", crfno.toString());
        binding.execute();
        
        if (null != getOldDueDate()) {
            OperationBinding getCrfIDbinding = ADFUtils.findOperationBinding("getCrfID");
            getCrfIDbinding.getParamsMap().put("CrfNo", crfno);
            getCrfIDbinding.execute();
            
            if (getCrfIDbinding.getResult() != null) {
                crfID = (String) getCrfIDbinding.getResult();
            }
            
            OperationBinding customerDueDateChangebinding = ADFUtils.findOperationBinding("customerDueDateChange");
            customerDueDateChangebinding.getParamsMap().put("oppNumber", crfno);
            customerDueDateChangebinding.getParamsMap().put("oppID", crfID);
            customerDueDateChangebinding.getParamsMap().put("oldDuedate", getOldDueDate());
            customerDueDateChangebinding.execute();
            
            custDueDateTrack(crfID, getOldDueDate(), crfno.toString()); /**CHG0043633**/
        }
        
        setOldDueDate(null);
        String opptyId = "", status = "";

        if (null != JSFUtils.resolveExpression("#{bindings.OpportunityId.inputValue}")) {
            opptyId = JSFUtils.resolveExpression("#{bindings.OpportunityId.inputValue}").toString();
        }
        
        if (null != getChangedOpptyOutcome() && !"".equalsIgnoreCase(getChangedOpptyOutcome())) {
            if (("Won").equalsIgnoreCase(getChangedOpptyOutcome())) {
                BindingContainer createAwardTaskBinding = getBindings();
                OperationBinding createOperationBinding = createAwardTaskBinding.getOperationBinding("createAwardTask");
                createOperationBinding.getParamsMap().put("oppNumber", crfno.toString());
                createOperationBinding.getParamsMap().put("oppID", opptyId);
                createOperationBinding.getParamsMap().put("opptyOutcome", getChangedOpptyOutcome());
                status = (String) createOperationBinding.execute();
            }
            
            OperationBinding sendEmail = ADFUtils.getBindingContainer().getOperationBinding("sendEmailNotification");
            sendEmail.getParamsMap().put("source", ViewConstants.OPPTY_STATUS_CHANGE);
            sendEmail.getParamsMap().put("opptyNum", crfno.toString());
            sendEmail.getParamsMap().put("oscId", opptyId);
            sendEmail.getParamsMap().put("notfcnTemplateId", "13");
            status = (String) sendEmail.execute();
        }           
        setChangedOpptyOutcome(null);
    }
    public void custDueDateTrack(String crfID, Date getOldDueDate, String crfno){
        try{
        Date dueDate = new Date();           
        String dateInString = resolveExpression("#{bindings.DueDate.inputValue}").toString();
        DateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        dueDate = inputFormatter.parse(dateInString);
           String user = ADFContext.getCurrent().getSecurityContext().getUserName();
       ViewObject custDueDateVo = ADFUtils.findIterator("CctCustomerDueDateHistoryEOViewIterator").getViewObject();
        Row newRow = custDueDateVo.createRow();
        newRow.setAttribute("Crfid", crfID);
        newRow.setAttribute("PreviousDuedate", getOldDueDate);
        newRow.setAttribute("NewDuedate", dueDate);
        newRow.setAttribute("UpdatedBy", user);
            newRow.setAttribute("Opportunity", crfno);
            custDueDateVo.insertRow(newRow);   
            ADFUtils.findOperationBinding("Commit").execute();
            custDueDateVo.executeQuery();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void saveUpdatedOppty(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(), "saveUpdatedOppty", "Enetring saveUpdatedOppty method");
        saveUpdateOpportunity();
        setSuccessmsg("Opportunity Details Updated successfully!!");                      
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getStatusPopupBinding().show(hints);        
    }

    public void openDocumentManagement(ActionEvent actionEvent) {
        ADFContext.getCurrent().getSessionScope().put(ViewConstants.SOURCE, getSource());        
    }
    
    public DCBindingContainer getDCBindingsContainer() {
        DCBindingContainer bindingsContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        return bindingsContainer;
    }
    
    public void searchNotesTypeFilter(ActionEvent actionEvent) {
        String crfNo = (JSFUtils.resolveExpression("#{bindings.CrfNo.inputValue}")).toString();
        BindingContainer binding = BindingContext.getCurrent().getCurrentBindingsEntry();
        JUCtrlListBinding listBinding = (JUCtrlListBinding)binding.get("b_noteType");
        String noteType= (String) listBinding.getInputValue();    
        
        DCBindingContainer bindings = this.getDCBindingsContainer();
        DCIteratorBinding cctNotesUpdateOppVOIterator =
            bindings.findIteratorBinding("CctNotesUpdateOppVOIterator");
        ViewObject cctNotesUpdateOppVO = cctNotesUpdateOppVOIterator.getViewObject();
        
        ViewCriteriaManager vcm = cctNotesUpdateOppVO.getViewCriteriaManager();
        ViewCriteria cctNotesUpdateOppSearchCriteria = vcm.getViewCriteria("CctNotesUpdateOppSearchCriteria");
        VariableValueManager vvm = cctNotesUpdateOppSearchCriteria.ensureVariableManager();
        vvm.setVariableValue("b_noteType", noteType);
        vvm.setVariableValue("p_crfNo", crfNo);
        cctNotesUpdateOppVO.applyViewCriteria(cctNotesUpdateOppSearchCriteria);
        cctNotesUpdateOppVO.executeQuery();
     
        refreshNotes();
    }
    
//  Start : Create Revision Process
    
    public void createRevision(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(), "createRevision", "Enetring createRevision method");
        
//      CHG0043113 - User creating revision without saving the customer due date.        
        if(getOldDueDate() != null){
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            getDueDatePopupBinding().show(hints);
        }else{
            openRevisionPopup();
        }      
    }
    
    private void openRevisionPopup(){
        logger.log(Level.INFO, getClass(), "openRevisionPopup", "Enetring openRevisionPopup method");
        String crfNumber = getCrfNo();
        OperationBinding binding = ADFUtils.findOperationBinding("initializeCctCrfRevisionData");
        binding.getParamsMap().put("crfNo",crfNumber);
        binding.execute();
        
        setRevisionConfirmationmsg("Are you sure you want to create a revision for this opportunity?");
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getRevisionPopupBinding().show(hints);
    }
    
    public void callCreateRevision(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(), "callCreateRevision", "Enetring callCreateRevision method");        
        getRevisionPopupBinding().hide();
        setRevisionConfirmationmsg(ViewConstants.REVISION_CONFIRMATION_MSG);
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getRevisionConfirmationBinding().show(hints);        
    }

    public void confirmCreateRevision(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(), "confirmCreateRevision", "Enetring confirmCreateRevision method");        

        String crfNo = getCrfNo();
        OperationBinding binding = ADFUtils.findOperationBinding("createRevisionForOppty");
        binding.getParamsMap().put("opptyNo", crfNo);
        binding.execute();
        
        OperationBinding bindings = ADFUtils.findOperationBinding("changeCRFstatusForRevision");
        bindings.getParamsMap().put("crfNumber", crfNo);
        bindings.execute();
        
        setSuccessmsg(ViewConstants.REVISION_SUCCESS_MSG + crfNo);
        getRevisionConfirmationBinding().hide();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getStatusPopupBinding().show(hints); 
        
        BigDecimal crfId = (BigDecimal) JSFUtils.resolveExpression("#{bindings.CrfId1.inputValue}");
        DCBindingContainer bindings2 = this.getDCBindingsContainer();
        DCIteratorBinding cctCrfRevisionDateMgmtVOIterator =
            bindings2.findIteratorBinding("CctCrfRevisionDateMgmtVOIterator");
        ViewObject cctCrfRevisionDateMgmtVO = cctCrfRevisionDateMgmtVOIterator.getViewObject();
        cctCrfRevisionDateMgmtVO.reset();
        cctCrfRevisionDateMgmtVO.clearCache();
        
        ViewCriteriaManager vcm = cctCrfRevisionDateMgmtVO.getViewCriteriaManager();
        ViewCriteria cctCrfRevisionDateMgmtCriteria = vcm.getViewCriteria("CctCrfRevisionDateMgmtCriteria");
        VariableValueManager vvm = cctCrfRevisionDateMgmtCriteria.ensureVariableManager();
        vvm.setVariableValue("b_crfId", crfId);
        cctCrfRevisionDateMgmtVO.applyViewCriteria(cctCrfRevisionDateMgmtCriteria);
        cctCrfRevisionDateMgmtVO.executeQuery();
        refreshPage();
    }
    
    public void cancelCreateRevision(ActionEvent actionEvent) { 
        logger.log(Level.INFO, getClass(), "cancelCreateRevision", "Enetring cancelCreateRevision method"); 
        OperationBinding operationBinding = ADFUtils.findOperationBinding("rollbackCurrentRevisionRow");
        operationBinding.execute();
        getRevisionConfirmationBinding().hide();
    }
    
    /**
     * Update the Opportunity and then open the Revision Popup
     * @param actionEvent
     */
    public void updateAndOpenRevisionPopupAL(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(), "updateAndOpenRevisionPopupAL", "Enetring updateAndOpenRevisionPopupAL method");
        saveUpdateOpportunity();
        getDueDatePopupBinding().hide();
        openRevisionPopup();
    }
    
    public void cancelDueDatePopupAL(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(), "cancelDueDatePopupAL", "Enetring cancelDueDatePopupAL method");
        getDueDatePopupBinding().hide();
    }
    
    public void setDueDatePopupBinding(RichPopup dueDatePopupBinding) {
        this.dueDatePopupBinding = dueDatePopupBinding;
    }

    public RichPopup getDueDatePopupBinding() {
        return dueDatePopupBinding;
    }
    
//    End : Create Revision Process

    public void setRevisionPopupBinding(RichPopup revisionPopupBinding) {
        this.revisionPopupBinding = revisionPopupBinding;
    }

    public RichPopup getRevisionPopupBinding() {
        return revisionPopupBinding;
    }

    public void duedateChangeListener(ValueChangeEvent valueChangeEvent) {
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date oldDuedate = df.parse(valueChangeEvent.getOldValue().toString());
            setOldDueDate(oldDuedate);
        } catch (Exception ae) {
            ae.printStackTrace();
        }
    }

    public void opptyStatusVCL(ValueChangeEvent valueChangeEvent) {
        setChangedOpptyOutcome((String)valueChangeEvent.getNewValue());
    }

    public void setCrfStatusBind(RichInputText crfStatusBind) {
        this.crfStatusBind = crfStatusBind;
    }

    public RichInputText getCrfStatusBind() {
        return crfStatusBind;
    }

    public void homeACL(ActionEvent actionEvent) {
//        getOpptyStatusBind().resetValue();
//        getOpptyStatusBind().setValue(null);
//        JSFUtils.setExpressionValue("#{bindings.OpptyOutcome.inputValue}", null);
//        AdfFacesContext.getCurrentInstance().addPartialTarget(getOpptyStatusBind());
    }

    public void setOpptyStatusBind(RichSelectOneChoice opptyStatusBind) {
        this.opptyStatusBind = opptyStatusBind;
    }

    public RichSelectOneChoice getOpptyStatusBind() {
        return opptyStatusBind;
    }

    public void hideRevisionPopup(ActionEvent actionEvent) {
        // Add event code here...
        
        OperationBinding operationBinding = ADFUtils.findOperationBinding("rollbackCurrentRevisionRow");
        operationBinding.execute();
        getRevisionPopupBinding().hide();
       
        UIComponent myPopupComponent = actionEvent.getComponent();
        oracle.adf.view.rich.util.ResetUtils.reset(myPopupComponent);
    }

    public void setWinLoassDateBind(RichInputDate winLoassDateBind) {
        this.winLoassDateBind = winLoassDateBind;
    }

    public RichInputDate getWinLoassDateBind() {
        return winLoassDateBind;
    }

    public void setCloseDateBind(RichInputDate closeDateBind) {
        this.closeDateBind = closeDateBind;
    }

    public RichInputDate getCloseDateBind() {
        return closeDateBind;
    }

    public void setWinLoassCommentBind(RichInputText winLoassCommentBind) {
        this.winLoassCommentBind = winLoassCommentBind;
    }

    public RichInputText getWinLoassCommentBind() {
        return winLoassCommentBind;
    }

    public void setCloseCommentBind(RichInputText closeCommentBind) {
        this.closeCommentBind = closeCommentBind;
    }

    public RichInputText getCloseCommentBind() {
        return closeCommentBind;
    }

    public void retrieveCustomerDetails(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        String customerNoNew = (String) valueChangeEvent.getNewValue();
        String customerNoOld = (String) valueChangeEvent.getOldValue();
        boolean result = false;
        OperationBinding customerValidateBinding = ADFUtils.findOperationBinding("customerNoValidate");
        customerValidateBinding.getParamsMap().put("custNo", customerNoNew);
        result = ((Boolean) customerValidateBinding.execute()).booleanValue();
        if (result) {
            OperationBinding binding = ADFUtils.findOperationBinding("executeCctCrfVO");
            binding.getParamsMap().put("customerNumber", customerNoNew);
            binding.getParamsMap().put("executeFlag", "updateScreen");
            binding.execute();
        } else {
            getCustNoBind().setValue(customerNoOld);
            AdfFacesContext.getCurrentInstance().addPartialTarget(getCustNoBind());
            FacesMessage Message = new FacesMessage(ViewConstants.INVALID_UPDATE_CUSTNO_MSG);
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
    }

    public void setCustNoBind(RichInputText custNoBind) {
        this.custNoBind = custNoBind;
    }

    public RichInputText getCustNoBind() {
        return custNoBind;
    }

    public void setRevisionConfirmationBinding(RichPopup revisionConfirmationBinding) {
        this.revisionConfirmationBinding = revisionConfirmationBinding;
    }

    public RichPopup getRevisionConfirmationBinding() {
        return revisionConfirmationBinding;
    }

    public void executeCrfRevisionVO(DisclosureEvent disclosureEvent) {
        // Add event code here...
        BigDecimal crfId = (BigDecimal) JSFUtils.resolveExpression("#{bindings.CrfId1.inputValue}");
        DCBindingContainer bindings = this.getDCBindingsContainer();
        DCIteratorBinding cctCrfRevisionDateMgmtVOIterator =
            bindings.findIteratorBinding("CctCrfRevisionDateMgmtVOIterator");
        ViewObject cctCrfRevisionDateMgmtVO = cctCrfRevisionDateMgmtVOIterator.getViewObject();
        cctCrfRevisionDateMgmtVO.reset();
        cctCrfRevisionDateMgmtVO.clearCache();
    
        ViewCriteriaManager vcm = cctCrfRevisionDateMgmtVO.getViewCriteriaManager();
        ViewCriteria cctCrfRevisionDateMgmtCriteria = vcm.getViewCriteria("CctCrfRevisionDateMgmtCriteria");
        VariableValueManager vvm = cctCrfRevisionDateMgmtCriteria.ensureVariableManager();
        vvm.setVariableValue("b_crfId", crfId);
        cctCrfRevisionDateMgmtVO.applyViewCriteria(cctCrfRevisionDateMgmtCriteria);
        cctCrfRevisionDateMgmtVO.executeQuery();
        
    }

    public void setRevisionDatesFormBinding(RichPanelFormLayout revisionDatesFormBinding) {
        this.revisionDatesFormBinding = revisionDatesFormBinding;
    }

    public RichPanelFormLayout getRevisionDatesFormBinding() {
        return revisionDatesFormBinding;
    }

    public void setRequestKickOffDateBind(RichInputDate requestKickOffDateBind) {
        this.requestKickOffDateBind = requestKickOffDateBind;
    }

    public RichInputDate getRequestKickOffDateBind() {
        return requestKickOffDateBind;
    }

    public void setOfferApprovalSentBind(RichInputDate offerApprovalSentBind) {
        this.offerApprovalSentBind = offerApprovalSentBind;
    }

    public RichInputDate getOfferApprovalSentBind() {
        return offerApprovalSentBind;
    }

    public void setOfferApprovalReceivedBind(RichInputDate offerApprovalReceivedBind) {
        this.offerApprovalReceivedBind = offerApprovalReceivedBind;
    }

    public RichInputDate getOfferApprovalReceivedBind() {
        return offerApprovalReceivedBind;
    }

    public void setOfferSentToCustBind(RichInputDate offerSentToCustBind) {
        this.offerSentToCustBind = offerSentToCustBind;
    }

    public RichInputDate getOfferSentToCustBind() {
        return offerSentToCustBind;
    }


    public void setklxOfferDisable(boolean  klxOfferDisable) {
        this. klxOfferDisable =  klxOfferDisable;
    }

    public boolean isklxOfferDisable() {
        String crfNum = (JSFUtils.resolveExpression("#{bindings.CrfNo.inputValue}")).toString();
        this.klxOfferDisable = false;
        logger.log(Level.INFO, getClass(), "isklxOfferDisable", " klxOfferDisable in beginnning=" + klxOfferDisable);

        try {
            java.sql.Timestamp OfferApprovalSentDate =
                (java.sql.Timestamp) (JSFUtils.resolveExpression("#{bindings.OfferApprovalSentDate.inputValue}"));
            java.sql.Timestamp OfferSentToCustDate =
                (java.sql.Timestamp) (JSFUtils.resolveExpression("#{bindings.OfferSentToCustDate.inputValue}"));

            java.sql.Timestamp revisionApprovalSentDate =
                (java.sql.Timestamp) (JSFUtils.resolveExpression("#{bindings.OfferApprovalSentDate1.inputValue}"));
            java.sql.Timestamp revisionSentToCustDate =
                (java.sql.Timestamp) (JSFUtils.resolveExpression("#{bindings.OfferSentToCustDate1.inputValue}"));

            OperationBinding binding = ADFUtils.findOperationBinding("getRevisionDetails");
            binding.getParamsMap().put("crfNumber", crfNum);
            String revisionCreated = (String) binding.execute();
            if (null != revisionCreated) {
                logger.log(Level.INFO, getClass(), "is klxOfferDisable", "revisionCreated=" + revisionCreated);
                if (revisionCreated.equalsIgnoreCase("Yes")) {

                    if (revisionApprovalSentDate != null || revisionSentToCustDate != null) {
                        this.klxOfferDisable = true;
                    }
                } else {
                    if (OfferApprovalSentDate != null || OfferSentToCustDate != null) {
                        this.klxOfferDisable = true;
                    }
                }
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "is klxOfferDisable", "Exception=" + e.getMessage());
            e.printStackTrace();
        }
        logger.log(Level.INFO, getClass(), "is klxOfferDisable", " klxOfferDisable=" + klxOfferDisable);

        return klxOfferDisable;
    }
    
    public void userActionsAccess(){
        HashMap actions = new HashMap();
        actions.put(ViewConstants.UPDATE_PROJECT_NUMBER, false);
        actions.put(ViewConstants.UPDATE_CONTRACT_TEAM_MEMBERS, false);
        actions.put(ViewConstants.CONVERT_TO_OPPORTUNITY, false);
        actions.put(ViewConstants.CREATE_REVISION, false);
        actions.put(ViewConstants.UPDATE_EDIT_OPPORTUNITY, false);       
        actions.put(ViewConstants.ADD_SEARCH_OPPTY_NOTES, false);      
        HashMap userAcess = UserActionAccessManagement.getUserAccess(ADFContext.getCurrent()
                                                                               .getSecurityContext()
                                                                               .getUserRoles(), actions);

        projectNumber = ((Boolean) userAcess.get(ViewConstants.UPDATE_PROJECT_NUMBER)).booleanValue();
        contractTeams = ((Boolean) userAcess.get(ViewConstants.UPDATE_CONTRACT_TEAM_MEMBERS)).booleanValue();
        convertOpportunity = ((Boolean) userAcess.get(ViewConstants.CONVERT_TO_OPPORTUNITY)).booleanValue();
        createRevision = ((Boolean) userAcess.get(ViewConstants.CREATE_REVISION)).booleanValue();
        updateEditOpportunity = ((Boolean) userAcess.get(ViewConstants.UPDATE_EDIT_OPPORTUNITY)).booleanValue();       
        addSearchNotes = ((Boolean) userAcess.get(ViewConstants.ADD_SEARCH_OPPTY_NOTES)).booleanValue();        
    }

    public void setUpdateEditOpportunity(boolean updateEditOpportunity) {
        this.updateEditOpportunity = updateEditOpportunity;
    }

    public boolean isUpdateEditOpportunity() {
        return updateEditOpportunity;
    }

    public void setProjectNumber(boolean projectNumber) {
        this.projectNumber = projectNumber;
    }

    public boolean isProjectNumber() {
        return projectNumber;
    }

    public void setContractTeams(boolean contractTeams) {
        this.contractTeams = contractTeams;
    }

    public boolean isContractTeams() {
        return contractTeams;
    }

    public void convertToOpp(ActionEvent actionEvent) {
        String crfNum = (JSFUtils.resolveExpression("#{bindings.CrfNo.inputValue}")).toString();
        OperationBinding binding = ADFUtils.findOperationBinding("createOpportunity");
        binding.getParamsMap().put("oppNumber", crfNum);
        String status =(String)binding.execute();
        Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
        if(ModelConstants.STATUS_SUCCESS.equalsIgnoreCase(status)){
        setSuccessmsg("Opportunity converted sucessfully.");
            JSFUtils.setExpressionValue("#{bindings.CrfStatus.inputValue}", "Active");
            JSFUtils.setExpressionValue("#{bindings.QualificationStage.inputValue}", "No");
            JSFUtils.setExpressionValue("#{bindings.OpportunityCreationDate.inputValue}",currentTimestamp);
            ADFUtils.findOperationBinding("Commit").execute();
        }
        else
        setSuccessmsg("Opportunity not converted.");
        
        getConfirmConvertPopUp().hide();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getConvertToOppBind().show(hints);
    }

    public void setConvertToOppBind(RichPopup convertToOppBind) {
        this.convertToOppBind = convertToOppBind;
    }

    public RichPopup getConvertToOppBind() {
        return convertToOppBind;
    }

    public void setConfirmConvertPopUp(RichPopup confirmConvertPopUp) {
        this.confirmConvertPopUp = confirmConvertPopUp;
    }

    public RichPopup getConfirmConvertPopUp() {
        return confirmConvertPopUp;
    }

    public void closeCnfmPopUp(ActionEvent actionEvent) {
       getConfirmConvertPopUp().hide();
    }

    public void openCnfmPopUp(ActionEvent actionEvent) {
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getConfirmConvertPopUp().show(hints);
    }

    public void ExportToExcel(FacesContext facesContext, OutputStream outputStream) throws IOException{
        // Add event code here...
        try {
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet worksheet = workbook.createSheet("Update Opportunity");
            
//            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
//            DCIteratorBinding dcIteratorBindings = bindings.findIteratorBinding(iteratorName);
            XSSFRow  excelrow = null;
            XSSFCellStyle style = workbook.createCellStyle();          
            XSSFFont font = workbook.createFont();
            font.setFontHeightInPoints((short) 11);
            font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
            style.setFont(font);
            short j = 0;
            Date dueDate = new Date();           
            String dateInString = resolveExpression("#{bindings.DueDate.inputValue}").toString();
           
            DateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
            dueDate = inputFormatter.parse(dateInString);
           
            excelrow = (XSSFRow)worksheet.createRow(1);
            XSSFCell cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("CRF #:");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.CrfNo.inputValue}").toString());               
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("Customer Ref#").toString());              
            cellA1 = null;            
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.CustomerRef.inputValue}") == null ? "" : resolveExpression("#{bindings.CustomerRef.inputValue}").toString());               
            cellA1 = null;            
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("CRF Status:");               
            cellA1 = null;
            cellA1 = excelrow.createCell(6);
            cellA1.setCellValue(resolveExpression("#{bindings.CrfStatus.inputValue}") == null ? "" : resolveExpression("#{bindings.CrfStatus.inputValue}").toString());               
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(2);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Customer Due Date :");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            //cellA1.setCellValue(dueDate); 
            cellA1.setCellValue(dateInString);               
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("Sales Ref #: ").toString());             
            cellA1 = null;            
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesRef.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesRef.inputValue}").toString());               
            cellA1 = null;            
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("Sales Status:");               
            cellA1 = null;
            cellA1 = excelrow.createCell(6);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesStatus.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesStatus.inputValue}").toString());               
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(3);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Opportunity Outcome");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.OpptyOutcome.inputValue}") == null ? "" : resolveExpression("#{bindings.OpptyOutcome.inputValue}").toString());               
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("Project #:");              
            cellA1 = null;            
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.ProjectNo.inputValue}") == null ? "" : resolveExpression("#{bindings.ProjectNo.inputValue}").toString());               
            cellA1 = null;            
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("Qualification Stage:");               
            cellA1 = null;
            cellA1 = excelrow.createCell(6);
            cellA1.setCellValue(resolveExpression("#{bindings.QualificationStage.inputValue}") == null ? "" : resolveExpression("#{bindings.QualificationStage.inputValue}").toString());               
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(4);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("");               
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("");              
            cellA1 = null;            
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("");               
            cellA1 = null;            
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("Contract Teams:");               
            cellA1 = null;
            cellA1 = excelrow.createCell(6);
            cellA1.setCellValue(resolveExpression("#{bindings.ContractTeams1.inputValue}") == null ? "" : resolveExpression("#{bindings.ContractTeams1.inputValue}").toString());               
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(5);        
            excelrow = null;

            excelrow = (XSSFRow)worksheet.createRow(6);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Customer Information");
            cellA1.setCellStyle(style);
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("");               
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("");              
            cellA1 = null;            
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("");               
            cellA1 = null;            
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("");               
            cellA1 = null;
            cellA1 = excelrow.createCell(6);
            cellA1.setCellValue("");               
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(7);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Descriptions");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("Cardex Customer Data");              
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("Customer Request Data");              
            cellA1 = null;            
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("KLX Offer");                           
            cellA1 = null;                       
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("Award");               
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(8);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Customer Number");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.CustomerNo.inputValue}") == null ? "" : resolveExpression("#{bindings.CustomerNo.inputValue}").toString());               
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.CustomerNo2.inputValue}") == null ? "" : resolveExpression("#{bindings.CustomerNo2.inputValue}").toString());              
            cellA1 = null;            
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.CustomerNo3.inputValue}") == null ? "" : resolveExpression("#{bindings.CustomerNo3.inputValue}").toString());               
            cellA1 = null;                        
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("");               
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(9);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Customer Name");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.CustomerName.inputValue}") == null ? "" : resolveExpression("#{bindings.CustomerName.inputValue}").toString());               
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.CustomerName2.inputValue}") == null ? "" : resolveExpression("#{bindings.CustomerName2.inputValue}").toString());              
            cellA1 = null;            
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.CustomerName3.inputValue}") == null ? "" : resolveExpression("#{bindings.CustomerName3.inputValue}").toString());               
            cellA1 = null;                        
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("");               
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(10);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Account Type:");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.AccountType.inputValue}") == null ? "" : resolveExpression("#{bindings.AccountType.inputValue}").toString());               
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.AccountType2.inputValue}") == null ? "" : resolveExpression("#{bindings.AccountType2.inputValue}").toString());              
            cellA1 = null;                                      
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.AccountType1.inputValue}") == null ? "" : resolveExpression("#{bindings.AccountType1.inputValue}").toString());                           
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(11);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("GT 25:");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.IsGt25.inputValue}") == null ? "" : resolveExpression("#{bindings.IsGt25.inputValue}").toString());               
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.IsGt251.inputValue}") == null ? "" : resolveExpression("#{bindings.IsGt251.inputValue}").toString());              
            cellA1 = null;                                      
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.IsGt252.inputValue}") == null ? "" : resolveExpression("#{bindings.IsGt252.inputValue}").toString());                           
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(12);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("E-Commerce Access:");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.IsEcomm.inputValue}") == null ? "" : resolveExpression("#{bindings.IsEcomm.inputValue}").toString());               
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.IsEcomm2.inputValue}") == null ? "" : resolveExpression("#{bindings.IsEcomm2.inputValue}").toString());             
            cellA1 = null;                                      
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.IsEcomm1.inputValue}") == null ? "" : resolveExpression("#{bindings.IsEcomm1.inputValue}").toString());                           
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(13);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Currency:");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.CurrencyCode.inputValue}") == null ? "" : resolveExpression("#{bindings.CurrencyCode.inputValue}").toString());               
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.CurrencyCode1.inputValue}") == null ? "" : resolveExpression("#{bindings.CurrencyCode1.inputValue}").toString());             
            cellA1 = null;                                      
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.CurrencyCode2.inputValue}") == null ? "" : resolveExpression("#{bindings.CurrencyCode2.inputValue}").toString());                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.CurrencyCode3.inputValue}") == null ? "" : resolveExpression("#{bindings.CurrencyCode3.inputValue}").toString());                           
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(14);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Payment Terms:");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.PaymentTerms.inputValue}") == null ? "" : resolveExpression("#{bindings.PaymentTerms.inputValue}").toString());               
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.PaymentTerms1.inputValue}") == null ? "" : resolveExpression("#{bindings.PaymentTerms1.inputValue}").toString());              
            cellA1 = null;                                      
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.PaymentTerms2.inputValue}") == null ? "" : resolveExpression("#{bindings.PaymentTerms2.inputValue}").toString());                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.PaymentTerms3.inputValue}") == null ? "" : resolveExpression("#{bindings.PaymentTerms3.inputValue}").toString());                           
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(15);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Incoterms:");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.IncoTerms.inputValue}") == null ? "" : resolveExpression("#{bindings.IncoTerms.inputValue}").toString());               
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.IncoTerms1.inputValue}") == null ? "" : resolveExpression("#{bindings.IncoTerms1.inputValue}").toString());              
            cellA1 = null;                                      
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.IncoTerms3.inputValue}") == null ? "" : resolveExpression("#{bindings.IncoTerms3.inputValue}").toString());                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.IncoTerms2.inputValue}") == null ? "" : resolveExpression("#{bindings.IncoTerms2.inputValue}").toString());                           
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(16);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Ultimate Destination:");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.UltimtateDestination.inputValue}") == null ? "" : resolveExpression("#{bindings.UltimtateDestination.inputValue}").toString());               
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.UltimtateDestination1.inputValue}") == null ? "" : resolveExpression("#{bindings.UltimtateDestination1.inputValue}").toString());              
            cellA1 = null;                                      
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.UltimtateDestination2.inputValue}") == null ? "" : resolveExpression("#{bindings.UltimtateDestination2.inputValue}").toString());                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.UltimtateDestination3.inputValue}") == null ? "" : resolveExpression("#{bindings.UltimtateDestination3.inputValue}").toString());                           
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(17);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Existing Contract #:");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.ExistingContract.inputValue}") == null ? "" : resolveExpression("#{bindings.ExistingContract.inputValue}").toString());               
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");              
            cellA1 = null;                                      
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");                           
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(18);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Existing Contract Type:");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.ExistingContractType.inputValue}") == null ? "" : resolveExpression("#{bindings.ExistingContractType.inputValue}").toString());               
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");              
            cellA1 = null;                                      
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");                           
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(19);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Competition:");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.Competition.inputValue}") == null ? "" : resolveExpression("#{bindings.Competition.inputValue}").toString());               
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");              
            cellA1 = null;                                      
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");                           
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(20);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Platform Information:");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.PlatformInfo.inputValue}") == null ? "" : resolveExpression("#{bindings.PlatformInfo.inputValue}").toString());               
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");              
            cellA1 = null;                                      
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");                           
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(21);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Customer Focus:");           
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.CustomerFocus.inputValue}") == null ? "" : resolveExpression("#{bindings.CustomerFocus.inputValue}").toString());               
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("");              
            cellA1 = null;                                      
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.CustomerFocus1.inputValue}") == null ? "" : resolveExpression("#{bindings.CustomerFocus1.inputValue}").toString());                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");                           
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(22);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Contract Type:");            
            cellA1 = null;       
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                           
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.ContractType.inputValue}") == null ? "" : resolveExpression("#{bindings.ContractType.inputValue}").toString());               
            cellA1 = null;                                                           
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.ContractType1.inputValue}") == null ? "" : resolveExpression("#{bindings.ContractType1.inputValue}").toString());                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.ContractType2.inputValue}") == null ? "" : resolveExpression("#{bindings.ContractType2.inputValue}").toString());             
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(23);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Project Type:");            
            cellA1 = null;       
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                           
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.ProjectType.inputValue}") == null ? "" : resolveExpression("#{bindings.ProjectType.inputValue}").toString());               
            cellA1 = null;                                                           
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.ProjectType2.inputValue}") == null ? "" : resolveExpression("#{bindings.ProjectType2.inputValue}").toString());                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.ProjectType1.inputValue}") == null ? "" : resolveExpression("#{bindings.ProjectType1.inputValue}").toString());             
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(24);           
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(25);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Sales Contact Section");
            cellA1.setCellStyle(style);
            cellA1 = null;       
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("");                           
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("");               
            cellA1 = null;                                                           
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("");             
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(26);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Descriptions");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("Cardex Customer Data");              
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("Customer Request Data");              
            cellA1 = null;            
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("KLX Offer");                           
            cellA1 = null;                       
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("Award");               
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(27);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Opportunity created by:");            
            cellA1 = null;       
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                           
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.createdByUsername.inputValue}") == null ? "" : resolveExpression("#{bindings.createdByUsername.inputValue}").toString());               
            cellA1 = null;                                                           
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.createdByUsername.inputValue}") == null ? "" : resolveExpression("#{bindings.createdByUsername.inputValue}").toString());                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.createdByUsername.inputValue}") == null ? "" : resolveExpression("#{bindings.createdByUsername.inputValue}").toString());             
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(28);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Sales Owner:");            
            cellA1 = null;  
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesOwner.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesOwner.inputValue}").toString());               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                           
            cellA1 = null;                                                                     
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesOwner.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesOwner.inputValue}").toString());                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesOwner.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesOwner.inputValue}").toString());             
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(29);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Sales Team:");            
            cellA1 = null;  
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesTeam.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesTeam.inputValue}").toString());               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                           
            cellA1 = null;                                                                     
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesTeam.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesTeam.inputValue}").toString());                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesTeam.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesTeam.inputValue}").toString());             
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(30);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Sales Manager:");            
            cellA1 = null;  
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesManager.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesManager.inputValue}").toString());               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                           
            cellA1 = null;                                                                     
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesManager.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesManager.inputValue}").toString());                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesManager.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesManager.inputValue}").toString());             
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(31);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Sales Director:");            
            cellA1 = null;  
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesDirector.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesDirector.inputValue}").toString());               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                           
            cellA1 = null;                                                                     
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesDirector.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesDirector.inputValue}").toString());                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesDirector.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesDirector.inputValue}").toString());             
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(32);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Sales VP:");            
            cellA1 = null;  
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesVp.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesVp.inputValue}").toString());               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesVp.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesVp.inputValue}").toString());                           
            cellA1 = null;                                                                     
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesVp.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesVp.inputValue}").toString());                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesVp.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesVp.inputValue}").toString());             
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(33);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Outside Sales:");            
            cellA1 = null;  
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesOutside.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesOutside.inputValue}").toString());               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("");                           
            cellA1 = null;                                                                     
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesOutside.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesOutside.inputValue}").toString());                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.SalesOutside.inputValue}") == null ? "" : resolveExpression("#{bindings.SalesOutside.inputValue}").toString());             
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(34);           
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(35);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Parts Details");   
            cellA1.setCellStyle(style);
            cellA1 = null;  
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("");                           
            cellA1 = null;                                                                     
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("");             
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(36);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Descriptions");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("Cardex Customer Data");              
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("Customer Request Data");              
            cellA1 = null;            
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("KLX Offer");                           
            cellA1 = null;                       
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("Award");               
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(37);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Total Part Count:");            
            cellA1 = null;  
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.TotalPartCount.inputValue}") == null ? "" : resolveExpression("#{bindings.TotalPartCount.inputValue}").toString());               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.TotalPartCount.inputValue}") == null ? "" : resolveExpression("#{bindings.TotalPartCount.inputValue}").toString());                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("");                           
            cellA1 = null;                                                                                
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("");             
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(38);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Part File (Decimals):");            
            cellA1 = null;  
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                           
            cellA1 = null;  
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.DecimalPlacesPartFile.inputValue}") == null ? "" : resolveExpression("#{bindings.DecimalPlacesPartFile.inputValue}").toString());               
            cellA1 = null; 
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.DecimalPlacesPartFile1.inputValue}") == null ? "" : resolveExpression("#{bindings.DecimalPlacesPartFile1.inputValue}").toString());                 
            cellA1 = null;                                                                                         
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.DecimalPlacesPartFile2.inputValue}") == null ? "" : resolveExpression("#{bindings.DecimalPlacesPartFile2.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(39);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Contract Length(yrs):");            
            cellA1 = null;  
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                           
            cellA1 = null;  
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.ContractLength.inputValue}") == null ? "" : resolveExpression("#{bindings.ContractLength.inputValue}").toString());               
            cellA1 = null; 
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.ContractLength1.inputValue}") == null ? "" : resolveExpression("#{bindings.ContractLength1.inputValue}").toString());                 
            cellA1 = null;                                                                                         
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.ContractLength2.inputValue}") == null ? "" : resolveExpression("#{bindings.ContractLength2.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(40);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("GT 25:");            
            cellA1 = null;             
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.IsGt25.inputValue}") == null ? "" : resolveExpression("#{bindings.IsGt25.inputValue}").toString());               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                           
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.IsGt251.inputValue}") == null ? "" : resolveExpression("#{bindings.IsGt251.inputValue}").toString());                 
            cellA1 = null;                                                                                         
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.IsGt252.inputValue}") == null ? "" : resolveExpression("#{bindings.IsGt252.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(41);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("DFAR applies:");            
            cellA1 = null;             
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.IsDfar.inputValue}") == null ? "" : resolveExpression("#{bindings.IsDfar.inputValue}").toString());               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.IsDfar1.inputValue}") == null ? "" : resolveExpression("#{bindings.IsDfar1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.IsDfar2.inputValue}") == null ? "" : resolveExpression("#{bindings.IsDfar2.inputValue}").toString());                 
            cellA1 = null;                                                                                         
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.IsDfar3.inputValue}") == null ? "" : resolveExpression("#{bindings.IsDfar3.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(42);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Alternate Customers:");            
            cellA1 = null;             
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.AlternateCustomers1.inputValue}") == null ? "" : resolveExpression("#{bindings.AlternateCustomers1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.AlternateCustomers2.inputValue}") == null ? "" : resolveExpression("#{bindings.AlternateCustomers2.inputValue}").toString());                 
            cellA1 = null;                                                                                         
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.AlternateCustomers.inputValue}") == null ? "" : resolveExpression("#{bindings.AlternateCustomers.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(43);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("TSO/PMA applies:");            
            cellA1 = null;             
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.IsTsoPma.inputValue}") == null ? "" : resolveExpression("#{bindings.IsTsoPma.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.IsTsoPma1.inputValue}") == null ? "" : resolveExpression("#{bindings.IsTsoPma1.inputValue}").toString());                 
            cellA1 = null;                                                                                         
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.IsTsoPma2.inputValue}") == null ? "" : resolveExpression("#{bindings.IsTsoPma2.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(44);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("ASL applies:");            
            cellA1 = null;             
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.IsAslApplies.inputValue}") == null ? "" : resolveExpression("#{bindings.IsAslApplies.inputValue}").toString());               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.IsAsl.inputValue}") == null ? "" : resolveExpression("#{bindings.IsAsl.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.IsAslApplicable1.inputValue}") == null ? "" : resolveExpression("#{bindings.IsAslApplicable1.inputValue}").toString());                 
            cellA1 = null;                                                                                         
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.IsAslApplicable.inputValue}") == null ? "" : resolveExpression("#{bindings.IsAslApplicable.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(45);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("ASL detail:");            
            cellA1 = null;             
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.AslDetails.inputValue}") == null ? "" : resolveExpression("#{bindings.AslDetails.inputValue}").toString());               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.AslDetails1.inputValue}") == null ? "" : resolveExpression("#{bindings.AslDetails1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.AslDetails2.inputValue}") == null ? "" : resolveExpression("#{bindings.AslDetails2.inputValue}").toString());                 
            cellA1 = null;                                                                                         
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.AslDetails3.inputValue}") == null ? "" : resolveExpression("#{bindings.AslDetails3.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(46);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Warehouse detail:");            
            cellA1 = null;             
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.WarehouseInfo.inputValue}") == null ? "" : resolveExpression("#{bindings.WarehouseInfo.inputValue}").toString());               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.WarehouseInfo1.inputValue}") == null ? "" : resolveExpression("#{bindings.WarehouseInfo1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.WarehouseInfo3.inputValue}") == null ? "" : resolveExpression("#{bindings.WarehouseInfo3.inputValue}").toString());                 
            cellA1 = null;                                                                                         
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.WarehouseInfo2.inputValue}") == null ? "" : resolveExpression("#{bindings.WarehouseInfo2.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(47);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Source Products:");            
            cellA1 = null;             
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.SourceProducts.inputValue}") == null ? "" : resolveExpression("#{bindings.SourceProducts.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.SourceProducts1.inputValue}") == null ? "" : resolveExpression("#{bindings.SourceProducts1.inputValue}").toString());                 
            cellA1 = null;                                                                                         
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.SourceProducts2.inputValue}") == null ? "" : resolveExpression("#{bindings.SourceProducts2.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(48);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("MOQ's or Amortize:");            
            cellA1 = null;             
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.MoqsAmortize.inputValue}") == null ? "" : resolveExpression("#{bindings.MoqsAmortize.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.MoqsAmortize1.inputValue}") == null ? "" : resolveExpression("#{bindings.MoqsAmortize1.inputValue}").toString());                 
            cellA1 = null;                                                                                         
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.MoqsAmortize2.inputValue}") == null ? "" : resolveExpression("#{bindings.MoqsAmortize2.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(49);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Return part list format:");            
            cellA1 = null;             
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.ReturnPartListFormat.inputValue}") == null ? "" : resolveExpression("#{bindings.ReturnPartListFormat.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.ReturnPartListFormat2.inputValue}") == null ? "" : resolveExpression("#{bindings.ReturnPartListFormat2.inputValue}").toString());                 
            cellA1 = null;                                                                                         
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.ReturnPartListFormat1.inputValue}") == null ? "" : resolveExpression("#{bindings.ReturnPartListFormat1.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(50);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Target margin (%):");            
            cellA1 = null;             
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.TargetMargin.inputValue}") == null ? "" : resolveExpression("#{bindings.TargetMargin.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.TargetMargin1.inputValue}") == null ? "" : resolveExpression("#{bindings.TargetMargin1.inputValue}").toString());                 
            cellA1 = null;                                                                                         
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.TargetMargin2.inputValue}") == null ? "" : resolveExpression("#{bindings.TargetMargin2.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(51);          
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(52);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Program Information");    
            cellA1.setCellStyle(style);
            cellA1 = null;             
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("");               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("");                 
            cellA1 = null;                                                                                         
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("");                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(53);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Descriptions");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("Cardex Customer Data");              
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("Customer Request Data");              
            cellA1 = null;            
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("KLX Offer");                           
            cellA1 = null;                       
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("Award");               
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(54);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("CMI:");            
            cellA1 = null;    
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.Cmi1.inputValue}") == null ? "" : resolveExpression("#{bindings.Cmi1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.Cmi.inputValue}") == null ? "" : resolveExpression("#{bindings.Cmi.inputValue}").toString());                 
            cellA1 = null;                 
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.Cmi2.inputValue}") == null ? "" : resolveExpression("#{bindings.Cmi2.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(55);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("VMI:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.Vmi1.inputValue}") == null ? "" : resolveExpression("#{bindings.Vmi1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.Vmi2.inputValue}") == null ? "" : resolveExpression("#{bindings.Vmi2.inputValue}").toString());                 
            cellA1 = null;                  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.Vmi.inputValue}") == null ? "" : resolveExpression("#{bindings.Vmi.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(56);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Bailment:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.Bailment1.inputValue}") == null ? "" : resolveExpression("#{bindings.Bailment1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.Bailment.inputValue}") == null ? "" : resolveExpression("#{bindings.Bailment.inputValue}").toString());                 
            cellA1 = null;                  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.Bailment2.inputValue}") == null ? "" : resolveExpression("#{bindings.Bailment2.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(56);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Bailment:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.Bailment1.inputValue}") == null ? "" : resolveExpression("#{bindings.Bailment1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.Bailment.inputValue}") == null ? "" : resolveExpression("#{bindings.Bailment.inputValue}").toString());                 
            cellA1 = null;                  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.Bailment2.inputValue}") == null ? "" : resolveExpression("#{bindings.Bailment2.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(57);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Consignment:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.Consignment1.inputValue}") == null ? "" : resolveExpression("#{bindings.Consignment1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.Consignment2.inputValue}") == null ? "" : resolveExpression("#{bindings.Consignment2.inputValue}").toString());                 
            cellA1 = null;                  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.Consignment.inputValue}") == null ? "" : resolveExpression("#{bindings.Consignment.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(58);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("EDI:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.Edi1.inputValue}") == null ? "" : resolveExpression("#{bindings.Edi1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.Edi.inputValue}") == null ? "" : resolveExpression("#{bindings.Edi.inputValue}").toString());                 
            cellA1 = null;                  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.Edi2.inputValue}") == null ? "" : resolveExpression("#{bindings.Edi2.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(59);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("MIN/MAX:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.MinMax1.inputValue}") == null ? "" : resolveExpression("#{bindings.MinMax1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.MinMax2.inputValue}") == null ? "" : resolveExpression("#{bindings.MinMax2.inputValue}").toString());                 
            cellA1 = null;                  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.MinMax.inputValue}") == null ? "" : resolveExpression("#{bindings.MinMax.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(60);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Kitting:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.Kitting1.inputValue}") == null ? "" : resolveExpression("#{bindings.Kitting1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.Kitting.inputValue}") == null ? "" : resolveExpression("#{bindings.Kitting.inputValue}").toString());                 
            cellA1 = null;                  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.Kitting2.inputValue}") == null ? "" : resolveExpression("#{bindings.Kitting2.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(61);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("3PL:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.Pl31.inputValue}") == null ? "" : resolveExpression("#{bindings.Pl31.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.Pl3.inputValue}") == null ? "" : resolveExpression("#{bindings.Pl3.inputValue}").toString());                 
            cellA1 = null;                  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.Pl32.inputValue}") == null ? "" : resolveExpression("#{bindings.Pl32.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(62);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("4PL:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.Pl41.inputValue}") == null ? "" : resolveExpression("#{bindings.Pl41.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.Pl4.inputValue}") == null ? "" : resolveExpression("#{bindings.Pl4.inputValue}").toString());                 
            cellA1 = null;                  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.Pl42.inputValue}") == null ? "" : resolveExpression("#{bindings.Pl42.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(63);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("FSL required:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.IsFslReq1.inputValue}") == null ? "" : resolveExpression("#{bindings.IsFslReq1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.IsFslReq.inputValue}") == null ? "" : resolveExpression("#{bindings.IsFslReq.inputValue}").toString());                 
            cellA1 = null;                  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.IsFslReq2.inputValue}") == null ? "" : resolveExpression("#{bindings.IsFslReq2.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(64);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("FSL Location:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.FslLocation.inputValue}") == null ? "" : resolveExpression("#{bindings.FslLocation.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.FslLocation1.inputValue}") == null ? "" : resolveExpression("#{bindings.FslLocation1.inputValue}").toString());                 
            cellA1 = null;                  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.FslLocation2.inputValue}") == null ? "" : resolveExpression("#{bindings.FslLocation2.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(65);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("MOQ's or Amortize:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.MoqsAmortize.inputValue}") == null ? "" : resolveExpression("#{bindings.MoqsAmortize.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.MoqsAmortize1.inputValue}") == null ? "" : resolveExpression("#{bindings.MoqsAmortize1.inputValue}").toString());                 
            cellA1 = null;                  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.MoqsAmortize2.inputValue}") == null ? "" : resolveExpression("#{bindings.MoqsAmortize2.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(66);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Vending Machines:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.VendingMachines1.inputValue}") == null ? "" : resolveExpression("#{bindings.VendingMachines1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.VendingMachines.inputValue}") == null ? "" : resolveExpression("#{bindings.VendingMachines1.inputValue}").toString());                 
            cellA1 = null;                  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.VendingMachines2.inputValue}") == null ? "" : resolveExpression("#{bindings.VendingMachines2.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(67);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Service Fee Applies:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.ServiceFees1.inputValue}") == null ? "" : resolveExpression("#{bindings.ServiceFees1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.ServiceFees.inputValue}") == null ? "" : resolveExpression("#{bindings.ServiceFees.inputValue}").toString());                 
            cellA1 = null;                  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.ServiceFees2.inputValue}") == null ? "" : resolveExpression("#{bindings.ServiceFees2.inputValue}").toString());                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(68);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Site Visit required:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.IsSiteVisitReq1.inputValue}") == null ? "" : resolveExpression("#{bindings.IsSiteVisitReq1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.IsSiteVisitReq.inputValue}") == null ? "" : resolveExpression("#{bindings.IsSiteVisitReq.inputValue}").toString());                 
            cellA1 = null;                  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(69);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Summary:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.Summary.inputValue}") == null ? "" : resolveExpression("#{bindings.Summary.inputValue}").toString());                 
            cellA1 = null;                  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(70);
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(71);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Terms and Conditions");    
            cellA1.setCellStyle(style);
            cellA1 = null;             
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("");               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("");                 
            cellA1 = null;                                                                                         
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("");                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(72);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Descriptions");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("Cardex Customer Data");              
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("Customer Request Data");              
            cellA1 = null;            
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("KLX Offer");                           
            cellA1 = null;                       
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("Award");               
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(73);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Format:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.LegalFormatType1.inputValue}") == null ? "" : resolveExpression("#{bindings.LegalFormatType1.inputValue}").toString());                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.LegalFormatType.inputValue}") == null ? "" : resolveExpression("#{bindings.LegalFormatType.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.LegalFormatType2.inputValue}") == null ? "" : resolveExpression("#{bindings.LegalFormatType2.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(74);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Stocking Strategy:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.StockingStrategy1.inputValue}") == null ? "" : resolveExpression("#{bindings.StockingStrategy1.inputValue}").toString());                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.StockingStrategy.inputValue}") == null ? "" : resolveExpression("#{bindings.StockingStrategy.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.StockingStrategy2.inputValue}") == null ? "" : resolveExpression("#{bindings.StockingStrategy2.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(75);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Liability on investment:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.LiabilityOnInvstBItems1.inputValue}") == null ? "" : resolveExpression("#{bindings.LiabilityOnInvstBItems1.inputValue}").toString());                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.LiabilityOnInvstBItems.inputValue}") == null ? "" : resolveExpression("#{bindings.LiabilityOnInvstBItems.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.LiabilityOnInvstBItems2.inputValue}") == null ? "" : resolveExpression("#{bindings.LiabilityOnInvstBItems2.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(76);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("HPP Language:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.HppLanguage.inputValue}") == null ? "" : resolveExpression("#{bindings.HppLanguage.inputValue}").toString());                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.HppLanguage1.inputValue}") == null ? "" : resolveExpression("#{bindings.HppLanguage1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.HppLanguage2.inputValue}") == null ? "" : resolveExpression("#{bindings.HppLanguage2.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(77);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("CSM Language:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.CsmLanguage.inputValue}") == null ? "" : resolveExpression("#{bindings.CsmLanguage.inputValue}").toString());                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.CsmLanguage1.inputValue}") == null ? "" : resolveExpression("#{bindings.CsmLanguage1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.CsmLanguage2.inputValue}") == null ? "" : resolveExpression("#{bindings.CsmLanguage2.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(78);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("D item language:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.DItemLanguage.inputValue}") == null ? "" : resolveExpression("#{bindings.DItemLanguage.inputValue}").toString());                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.DItemLanguage1.inputValue}") == null ? "" : resolveExpression("#{bindings.DItemLanguage1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.DItemLanguage2.inputValue}") == null ? "" : resolveExpression("#{bindings.DItemLanguage2.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(79);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Rebate:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.Rebate.inputValue}") == null ? "" : resolveExpression("#{bindings.Rebate.inputValue}").toString());                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.Rebate1.inputValue}") == null ? "" : resolveExpression("#{bindings.Rebate1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.Rebate2.inputValue}") == null ? "" : resolveExpression("#{bindings.Rebate2.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(80);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Line Min:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.LineMin1.inputValue}") == null ? "" : resolveExpression("#{bindings.LineMin1.inputValue}").toString());                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.LineMin.inputValue}") == null ? "" : resolveExpression("#{bindings.LineMin.inputValue}").toString());                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.LineMin2.inputValue}") == null ? "" : resolveExpression("#{bindings.LineMin2.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.LineMin3.inputValue}") == null ? "" : resolveExpression("#{bindings.LineMin3.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(81);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Order Min:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.OrderMin1.inputValue}") == null ? "" : resolveExpression("#{bindings.OrderMin1.inputValue}").toString());                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.OrderMin.inputValue}") == null ? "" : resolveExpression("#{bindings.OrderMin.inputValue}").toString());                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.OrderMin2.inputValue}") == null ? "" : resolveExpression("#{bindings.OrderMin2.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.OrderMin3.inputValue}") == null ? "" : resolveExpression("#{bindings.OrderMin3.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(82);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Effective LOL:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.LolProtection.inputValue}") == null ? "" : resolveExpression("#{bindings.LolProtection.inputValue}").toString());                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.LolProtection1.inputValue}") == null ? "" : resolveExpression("#{bindings.LolProtection1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.LolProtection2.inputValue}") == null ? "" : resolveExpression("#{bindings.LolProtection2.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(83);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Penalties Damages:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.LiqDamages.inputValue}") == null ? "" : resolveExpression("#{bindings.LiqDamages.inputValue}").toString());                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.LiqDamages1.inputValue}") == null ? "" : resolveExpression("#{bindings.LiqDamages1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.LiqDamages2.inputValue}") == null ? "" : resolveExpression("#{bindings.LiqDamages2.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(84);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Unusual Title Transfer:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.UnusualTitleTransfer.inputValue}") == null ? "" : resolveExpression("#{bindings.UnusualTitleTransfer.inputValue}").toString());                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.UnusualTitleTransfer1.inputValue}") == null ? "" : resolveExpression("#{bindings.UnusualTitleTransfer1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.UnusualTitleTransfer2.inputValue}") == null ? "" : resolveExpression("#{bindings.UnusualTitleTransfer2.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(85);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Rights of Return:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.RightsOfReturn.inputValue}") == null ? "" : resolveExpression("#{bindings.RightsOfReturn.inputValue}").toString());                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.RightsOfReturn1.inputValue}") == null ? "" : resolveExpression("#{bindings.RightsOfReturn1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.RightsOfReturn2.inputValue}") == null ? "" : resolveExpression("#{bindings.RightsOfReturn2.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(86);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Incentives/Credits:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.IncentivesCredit.inputValue}") == null ? "" : resolveExpression("#{bindings.IncentivesCredit.inputValue}").toString());                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.IncentivesCredit1.inputValue}") == null ? "" : resolveExpression("#{bindings.IncentivesCredit1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.IncentivesCredit2.inputValue}") == null ? "" : resolveExpression("#{bindings.IncentivesCredit2.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(87);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Cancellation Privileges:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.CancellationRights.inputValue}") == null ? "" : resolveExpression("#{bindings.CancellationRights.inputValue}").toString());                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.CancellationRights1.inputValue}") == null ? "" : resolveExpression("#{bindings.CancellationRights1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.CancellationRights2.inputValue}") == null ? "" : resolveExpression("#{bindings.CancellationRights2.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(88);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Bill and Hold:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.BillHold.inputValue}") == null ? "" : resolveExpression("#{bindings.BillHold.inputValue}").toString());                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.BillHold1.inputValue}") == null ? "" : resolveExpression("#{bindings.BillHold1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.BillHold2.inputValue}") == null ? "" : resolveExpression("#{bindings.BillHold2.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(89);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Buy Back:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue(resolveExpression("#{bindings.IsBuyBack.inputValue}") == null ? "" : resolveExpression("#{bindings.IsBuyBack.inputValue}").toString());                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.IsBuyBack1.inputValue}") == null ? "" : resolveExpression("#{bindings.IsBuyBack1.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.IsBuyBack2.inputValue}") == null ? "" : resolveExpression("#{bindings.IsBuyBack2.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(90);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Confirm T/ C's Topics:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue(resolveExpression("#{bindings.Comments.inputValue}") == null ? "" : resolveExpression("#{bindings.Comments.inputValue}").toString());               
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.Comments1.inputValue}") == null ? "" : resolveExpression("#{bindings.Comments1.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(91);
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(92);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Supporting Team Members");    
            cellA1.setCellStyle(style);
            cellA1 = null;             
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("");               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("");                 
            cellA1 = null;                                                                                         
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("");                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(93);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Proposal:");            
            cellA1 = null;               
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.Proposal.inputValue}") == null ? "" : resolveExpression("#{bindings.Proposal.inputValue}").toString());               
            cellA1 = null;                                      
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(94);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Pricing:");            
            cellA1 = null;               
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.Pricing.inputValue}") == null ? "" : resolveExpression("#{bindings.Pricing.inputValue}").toString());               
            cellA1 = null;                                      
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(95);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Advance Sourcing:");            
            cellA1 = null;               
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.AdvancedSourcing.inputValue}") == null ? "" : resolveExpression("#{bindings.AdvancedSourcing.inputValue}").toString());               
            cellA1 = null;                                      
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(96);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Hpp:");            
            cellA1 = null;               
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.Hpp.inputValue}") == null ? "" : resolveExpression("#{bindings.Hpp.inputValue}").toString());               
            cellA1 = null;                                      
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(97);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Lighting:");            
            cellA1 = null;               
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.Lighting.inputValue}") == null ? "" : resolveExpression("#{bindings.Lighting.inputValue}").toString());               
            cellA1 = null;                                      
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(98);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Chemical:");            
            cellA1 = null;               
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.Chemical.inputValue}") == null ? "" : resolveExpression("#{bindings.Chemical.inputValue}").toString());               
            cellA1 = null;                                      
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(99);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Legal:");            
            cellA1 = null;               
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.Legal1.inputValue}") == null ? "" : resolveExpression("#{bindings.Legal1.inputValue}").toString());               
            cellA1 = null;                                      
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(100);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Operations:");            
            cellA1 = null;               
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.Operations.inputValue}") == null ? "" : resolveExpression("#{bindings.Operations.inputValue}").toString());               
            cellA1 = null;                                      
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(101);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Program Solutions:");            
            cellA1 = null;               
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.ProgramSolutions.inputValue}") == null ? "" : resolveExpression("#{bindings.ProgramSolutions.inputValue}").toString());               
            cellA1 = null;                                      
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(102);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Quality:");            
            cellA1 = null;               
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.Quality.inputValue}") == null ? "" : resolveExpression("#{bindings.Quality.inputValue}").toString());               
            cellA1 = null;                                      
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(103);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Contracts Administrator:");            
            cellA1 = null;               
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue(resolveExpression("#{bindings.ContractsAdministrator.inputValue}") == null ? "" : resolveExpression("#{bindings.ContractsAdministrator.inputValue}").toString());               
            cellA1 = null;                                      
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(104);
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(105);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Award Details");    
            cellA1.setCellStyle(style);
            cellA1 = null;             
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("");               
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("");               
            cellA1 = null;  
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("");                 
            cellA1 = null;                                                                                         
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("");                 
            cellA1 = null;
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(106);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Descriptions");            
            cellA1 = null;                
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("Cardex Customer Data");              
            cellA1 = null;           
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("Customer Request Data");              
            cellA1 = null;            
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("KLX Offer");                           
            cellA1 = null;                       
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue("Award");               
            cellA1 = null;  
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(107);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Awards:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.AwardsDetails.inputValue}") == null ? "" : resolveExpression("#{bindings.AwardsDetails.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(108);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("IP:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.Ip.inputValue}") == null ? "" : resolveExpression("#{bindings.Ip.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(109);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Awarded Part Count:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.AwardedPartCount.inputValue}") == null ? "" : resolveExpression("#{bindings.AwardedPartCount.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(110);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Date of Signature:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.DateOfSignature.inputValue}") == null ? "" : resolveExpression("#{bindings.DateOfSignature.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(111);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Effective Date:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.EffectiveDate1.inputValue}") == null ? "" : resolveExpression("#{bindings.EffectiveDate1.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(112);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Date Sent to Contracts:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.SubmittedToContractDate.inputValue}") == null ? "" : resolveExpression("#{bindings.SubmittedToContractDate.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(113);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Flown Down Date:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.FlowDownDate1.inputValue}") == null ? "" : resolveExpression("#{bindings.FlowDownDate1.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(114);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("New Parts Requested:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.NewPartsRequested.inputValue}") == null ? "" : resolveExpression("#{bindings.NewPartsRequested.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(115);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("New Part Setup Completed:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.NewPartSetupCompleted.inputValue}") == null ? "" : resolveExpression("#{bindings.NewPartSetupCompleted.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(116);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Date Contract Loaded:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.ContractLoadedDate.inputValue}") == null ? "" : resolveExpression("#{bindings.ContractLoadedDate.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(117);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Date IP Completed:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.IpCompleteDate.inputValue}") == null ? "" : resolveExpression("#{bindings.IpCompleteDate.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(118);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Contracts Compliance Completed:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.ContractComplianceComplete.inputValue}") == null ? "" : resolveExpression("#{bindings.ContractComplianceComplete.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(119);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Final Awarded Part Count:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.FinalAwardedPartCount.inputValue}") == null ? "" : resolveExpression("#{bindings.FinalAwardedPartCount.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(120);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Completeness Check:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.CompletenessCheck.inputValue}") == null ? "" : resolveExpression("#{bindings.CompletenessCheck.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(121);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Risk Report Completed:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.RiskReportCompleted.inputValue}") == null ? "" : resolveExpression("#{bindings.RiskReportCompleted.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(122);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Percentage Tested:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.PercentageTested.inputValue}") == null ? "" : resolveExpression("#{bindings.PercentageTested.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(123);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Test Results:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.TestResults.inputValue}") == null ? "" : resolveExpression("#{bindings.TestResults.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(124);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Testing comments:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.TestingComments.inputValue}") == null ? "" : resolveExpression("#{bindings.TestingComments.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(125);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Testing comments:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.TestingComments.inputValue}") == null ? "" : resolveExpression("#{bindings.TestingComments.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(126);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Date Closed:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.DateClosed.inputValue}") == null ? "" : resolveExpression("#{bindings.DateClosed.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            excelrow = (XSSFRow)worksheet.createRow(127);
            cellA1 = excelrow.createCell(0);
            cellA1.setCellValue("");                
            cellA1 = null;
            cellA1 = excelrow.createCell(1);
            cellA1.setCellValue("Quarter:");            
            cellA1 = null;   
            cellA1 = excelrow.createCell(2);
            cellA1.setCellValue("NA");                 
            cellA1 = null; 
            cellA1 = excelrow.createCell(3);
            cellA1.setCellValue("NA");                 
            cellA1 = null;
            cellA1 = excelrow.createCell(4);
            cellA1.setCellValue("NA");
            cellA1 = null;  
            cellA1 = excelrow.createCell(5);
            cellA1.setCellValue(resolveExpression("#{bindings.Quarter.inputValue}") == null ? "" : resolveExpression("#{bindings.Quarter.inputValue}").toString());                 
            cellA1 = null;                              
            excelrow = null;
            
            worksheet.createFreezePane(0, 1, 0, 1);            
            worksheet.autoSizeColumn(1);
            worksheet.autoSizeColumn(2);
            worksheet.autoSizeColumn(3);
            worksheet.autoSizeColumn(4);
            worksheet.autoSizeColumn(5);
            worksheet.autoSizeColumn(6);
            
            workbook.write(outputStream);
            outputStream.flush();
           // }
        }        
        catch (Exception e) {
            e.printStackTrace();
        }
        DCBindingContainer dc = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding binding = dc.findIteratorBinding("UpdateCctCrfEoVOIterator");
            ViewObject vo = binding.getViewObject();
            StringBuilder sb = new StringBuilder();


//        sb.append(" ").append(nextcell).append(" ").append(nextcell).append(" ").append(nextcell).append(" ").
//           append(nextcell).append(" ").append(nextcell).append(" ").append(linebreak);
//        
//        sb.append("").append(nextcell).append("Award Details").append(nextcell).append("").append(nextcell).append("").
//           append(nextcell).append("").append(nextcell).append("").append(linebreak);
//        
//        sb.append(" ").append(nextcell).append("Descriptions").append(nextcell).append("Cardex Customer Data").append(nextcell).append("Customer Request Data").
//           append(nextcell).append("KLX Offer").append(nextcell).append("Award").append(linebreak);
//        
//        sb.append("").append(nextcell).append("Awards:").append(nextcell).append("NA").append(nextcell).append("NA").
//           append(nextcell).append("NA").append(nextcell).append(resolveExpression("#{bindings.AwardsDetails.inputValue}") == null ? "" : resolveExpression("#{bindings.AwardsDetails.inputValue}")).append(linebreak);
//        sb.append("").append(nextcell).append("IP:").append(nextcell).append("NA").append(nextcell).append("NA").
//           append(nextcell).append("NA").append(nextcell).append(resolveExpression("#{bindings.Ip.inputValue}") == null ? "" : resolveExpression("#{bindings.Ip.inputValue}")).append(linebreak);
//        sb.append("").append(nextcell).append("Awarded Part Count:").append(nextcell).append("NA").append(nextcell).append("NA").
//           append(nextcell).append("NA").append(nextcell).append(resolveExpression("#{bindings.AwardedPartCount.inputValue}") == null ? "" : resolveExpression("#{bindings.AwardedPartCount.inputValue}")).append(linebreak);
//        sb.append("").append(nextcell).append("Date of Signature:").append(nextcell).append("NA").append(nextcell).append("NA").
//           append(nextcell).append("NA").append(nextcell).append(resolveExpression("#{bindings.DateOfSignature.inputValue}") == null ? "" : resolveExpression("#{bindings.DateOfSignature.inputValue}")).append(linebreak);
//        sb.append("").append(nextcell).append("Effective Date:").append(nextcell).append("NA").append(nextcell).append("NA").
//           append(nextcell).append("NA").append(nextcell).append(resolveExpression("#{bindings.EffectiveDate1.inputValue}") == null ? "" : resolveExpression("#{bindings.EffectiveDate1.inputValue}")).append(linebreak);
//        sb.append("").append(nextcell).append("Date Sent to Contracts:").append(nextcell).append("NA").append(nextcell).append("NA").
//           append(nextcell).append("NA").append(nextcell).append(resolveExpression("#{bindings.SubmittedToContractDate.inputValue}") == null ? "" : resolveExpression("#{bindings.SubmittedToContractDate.inputValue}")).append(linebreak);
//        sb.append("").append(nextcell).append("Flown Down Date:").append(nextcell).append("NA").append(nextcell).append("NA").
//           append(nextcell).append("NA").append(nextcell).append(resolveExpression("#{bindings.FlowDownDate1.inputValue}") == null ? "" : resolveExpression("#{bindings.FlowDownDate1.inputValue}")).append(linebreak);        
//        sb.append("").append(nextcell).append("New Parts Requested:").append(nextcell).append("NA").append(nextcell).append("NA").
//           append(nextcell).append("NA").append(nextcell).append(resolveExpression("#{bindings.NewPartsRequested.inputValue}") == null ? "" : resolveExpression("#{bindings.NewPartsRequested.inputValue}")).append(linebreak);
//        sb.append("").append(nextcell).append("New Part Setup Completed:").append(nextcell).append("NA").append(nextcell).append("NA").
//           append(nextcell).append("NA").append(nextcell).append(resolveExpression("#{bindings.NewPartSetupCompleted.inputValue}") == null ? "" : resolveExpression("#{bindings.NewPartSetupCompleted.inputValue}")).append(linebreak);
//        sb.append("").append(nextcell).append("Date Contract Loaded:").append(nextcell).append("NA").append(nextcell).append("NA").
//           append(nextcell).append("NA").append(nextcell).append(resolveExpression("#{bindings.ContractLoadedDate.inputValue}") == null ? "" : resolveExpression("#{bindings.ContractLoadedDate.inputValue}")).append(linebreak);
//        sb.append("").append(nextcell).append("Date IP Completed:").append(nextcell).append("NA").append(nextcell).append("NA").
//           append(nextcell).append("NA").append(nextcell).append(resolveExpression("#{bindings.IpCompleteDate.inputValue}") == null ? "" : resolveExpression("#{bindings.IpCompleteDate.inputValue}")).append(linebreak);
//        sb.append("").append(nextcell).append("Contracts Compliance Completed:").append(nextcell).append("NA").append(nextcell).append("NA").
//           append(nextcell).append("NA").append(nextcell).append(resolveExpression("#{bindings.ContractComplianceComplete.inputValue}") == null ? "" : resolveExpression("#{bindings.ContractComplianceComplete.inputValue}")).append(linebreak);
//        sb.append("").append(nextcell).append("Final Awarded Part Count:").append(nextcell).append("NA").append(nextcell).append("NA").
//           append(nextcell).append("NA").append(nextcell).append(resolveExpression("#{bindings.FinalAwardedPartCount.inputValue}") == null ? "" : resolveExpression("#{bindings.FinalAwardedPartCount.inputValue}")).append(linebreak);
//        sb.append("").append(nextcell).append("Completeness Check:").append(nextcell).append("NA").append(nextcell).append("NA").
//           append(nextcell).append("NA").append(nextcell).append(resolveExpression("#{bindings.CompletenessCheck.inputValue}") == null ? "" : resolveExpression("#{bindings.CompletenessCheck.inputValue}")).append(linebreak);
//        sb.append("").append(nextcell).append("Risk Report Completed:").append(nextcell).append("NA").append(nextcell).append("NA").
//           append(nextcell).append("NA").append(nextcell).append(resolveExpression("#{bindings.RiskReportCompleted.inputValue}") == null ? "" : resolveExpression("#{bindings.RiskReportCompleted.inputValue}")).append(linebreak);
//        sb.append("").append(nextcell).append("Percentage Tested:").append(nextcell).append("NA").append(nextcell).append("NA").
//           append(nextcell).append("NA").append(nextcell).append(resolveExpression("#{bindings.PercentageTested.inputValue}") == null ? "" : resolveExpression("#{bindings.PercentageTested.inputValue}")).append(linebreak);
//        sb.append("").append(nextcell).append("Test Results:").append(nextcell).append("NA").append(nextcell).append("NA").
//           append(nextcell).append("NA").append(nextcell).append(resolveExpression("#{bindings.TestResults.inputValue}") == null ? "" : resolveExpression("#{bindings.TestResults.inputValue}")).append(linebreak);
//        sb.append("").append(nextcell).append("Testing comments:").append(nextcell).append("NA").append(nextcell).append("NA").
//           append(nextcell).append("NA").append(nextcell).append(resolveExpression("#{bindings.TestingComments.inputValue}") == null ? "" : resolveExpression("#{bindings.TestingComments.inputValue}").toString()).append(linebreak);
//        sb.append("").append(nextcell).append("Date Closed:").append(nextcell).append("NA").append(nextcell).append("NA").
//           append(nextcell).append("NA").append(nextcell).append(resolveExpression("#{bindings.DateClosed.inputValue}") == null ? "" : resolveExpression("#{bindings.DateClosed.inputValue}")).append(linebreak);
//        sb.append("").append(nextcell).append("Quarter:").append(nextcell).append("NA").append(nextcell).append("NA").
//           append(nextcell).append("NA").append(nextcell).append(resolveExpression("#{bindings.Quarter.inputValue}") == null ? "" : resolveExpression("#{bindings.Quarter.inputValue}")).append(linebreak);
            /*vo.setCurrentRow(vo.first());
            Row current = vo.getCurrentRow();
            while (current != null) {
                String firstName = (String)current.getAttribute("FirstName");
                String lastName = (String)current.getAttribute("LastName");
                String email = (String)current.getAttribute("Email");
                sb.append(firstName).append(NEXTCELL).append(lastName).append(NEXTCELL).append(email).append(NEXTCELL).append(LINEBREAK);
                current = vo.next();
            }*/
//            outputStream.write(sb.toString().getBytes());
//            outputStream.flush();
    }
    
    public Object resolveExpression(String expression) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Application app = facesContext.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        ValueExpression valueExp = elFactory.createValueExpression(elContext, expression, Object.class);
        return valueExp.getValue(elContext);
    }

    public void setConvertOpportunity(boolean convertOpportunity) {
        this.convertOpportunity = convertOpportunity;
    }

    public boolean isConvertOpportunity() {
        return convertOpportunity;
    }

    public void setAddSearchNotes(boolean addSearchNotes) {
        this.addSearchNotes = addSearchNotes;
    }

    public boolean isAddSearchNotes() {
        return addSearchNotes;
    }

    public void setLegalFormat(ArrayList<SelectItem> LegalFormat) {
        this.LegalFormat = LegalFormat;
    }

    public ArrayList<SelectItem> getLegalFormat() {
        ArrayList<SelectItem> LegalFormat = new ArrayList<SelectItem>();

        LegalFormat.add(new SelectItem(ViewConstants.legalFormat[0],ViewConstants.legalFormat[0]));
        LegalFormat.add(new SelectItem(ViewConstants.legalFormat[1],ViewConstants.legalFormat[1]));
        LegalFormat.add(new SelectItem(ViewConstants.legalFormat[2],ViewConstants.legalFormat[2]));
        return LegalFormat;
    }

    public void setReturnPart(ArrayList<SelectItem> returnPart) {
        this.returnPart = returnPart;
    }

    public ArrayList<SelectItem> getReturnPart() {
        ArrayList<SelectItem> returnPart = new ArrayList<SelectItem>();

        returnPart.add(new SelectItem(ViewConstants.returnFormat[0],ViewConstants.returnFormat[0]));
        returnPart.add(new SelectItem(ViewConstants.returnFormat[1],ViewConstants.returnFormat[1]));
        returnPart.add(new SelectItem(ViewConstants.returnFormat[2],ViewConstants.returnFormat[2]));
        returnPart.add(new SelectItem(ViewConstants.returnFormat[3],ViewConstants.returnFormat[3]));
        returnPart.add(new SelectItem(ViewConstants.returnFormat[4],ViewConstants.returnFormat[4]));
        return returnPart;
    }
}
