package com.klx.view.beans.constants;

import java.math.BigDecimal;


public class ViewConstants {
    public ViewConstants() {
        super();
    }
	//for Program cost button enabling for CCT_OPERATIONS_VP
    public static final String SAVE_HEADER_BUTTON="cct.opportunitydetails.programcost.saveheader-button";
    public static final String CANCEL_HEADER_BUTTON="cct.opportunitydetails.programcost.cancelheader-button";
    public static final String SAVE_EHS_BUTTON="cct.opportunitydetails.programcost.saveEHS-button";
    public static final String CANCEL_EHS_BUTTON="cct.opportunitydetails.programcost.cancelEHS-button";
    public static final String ADD_EHS_BUTTON="cct.opportunitydetails.programcost.addEHS-button";
    public static final String SAVE_IMP_BUTTON="cct.opportunitydetails.programcost.saveImp-button";
    public static final String CANCEL_IMP_BUTTON="cct.opportunitydetails.programcost.cancelImp-button";
    public static final String ADD_IMP_BUTTON="cct.opportunitydetails.programcost.addImp-button";
    public static final String SAVE_OTH_BUTTON="cct.opportunitydetails.programcost.saveOth-button";
    public static final String CANCEL_OTH_BUTTON="cct.opportunitydetails.programcost.cancelOth-button";
    public static final String ADD_OTH_BUTTON="cct.opportunitydetails.programcost.addOth-button";
    public static final String SAVE_FAC_BUTTON="cct.opportunitydetails.programcost.saveFac-button";
    public static final String CANCEL_FAC_BUTTON="cct.opportunitydetails.programcost.cancelFac-button";
    public static final String ADD_FAC_BUTTON="cct.opportunitydetails.programcost.addFac-button";
    public static final String SAVE_PER_BUTTON="cct.opportunitydetails.programcost.savePer-button";
    public static final String CANCEL_PER_BUTTON="cct.opportunitydetails.programcost.cancelPer-button";
    public static final String ADD_PER_BUTTON="cct.opportunitydetails.programcost.addPer-button";
    //for additional cost save button
    public static final String SAVE_MARGIN_BUTTON="cct.opportunitydetails.additionalcost.savemargin-button";
    public static final String UPL_STATUS_SUCCESS = "File Upload Successful.";
    public static final String BEACON_UPL_STATUS_SNAPSHOT = "File Uploaded Successfully - SNAPSHOT REQUIRED!!!";
    public static final String UPL_STATUS_FAILURE = "File Upload Not Successful.";
    public static final String UPL_STATUS_FAILURE_UNKNOWN_REASON = "Sorry, the file could not be uploaded. Please provide IT with reference number: ";
    public static final String LDAP_HOST_URL = "LDAP_HOST_URL";
    public static final String LDAP_SEARCH_BASE = "LDAP_SEARCH_BASE";
    public static final String LDAP_USERNAME = "LDAP_USERNAME";
    public static final String LDAP_PASSWORD = "LDAP_PASSWORD";
    public static final String GROUP_CONTAINS_STR = "GROUP_CONTAINS_STR";
    public static final String LDAP_GROUPS_CONSIDERED = "LDAP_GROUPS_CONSIDERED";
    public static final String NON_PROD = "Non_Prod";    
    public static final String SC_CCT = "scCCT";
    public static final String LDAP_FILTER = "LDAP_FILTER";
    public static final String LDAP_INIT_CTX = "LDAP_INIT_CTX";
    public static final String COLLABORATION_URL = "COLLABORATION_URL";
    public static final String SECURITY_AUTHENTICATION = "Simple";
    public static final String SUCCESS = "Success";
    public static final String FAILURE = "Failure";
    public static final String OSC_URL = "OSC_URL";
    public static final String LOGOUT = "/faces/pages/login.jspx";
    public static final String SEARCH = "/faces/pages/Search.jspx";
    public static final String FILE_PATH = "/queueTemplates/";
    public static final String OPPORTUNITY_ID = "opportunityId";
    public static final String OPPORTUNITY_NUMBER = "opportunityNumber";
    public static final String taskId="taskId";
    public static final String SOURCE = "source";
    public static final String FOLDERID = "folderId";
    public static final String BPM = "BPM";
    public static final String NOT_BPM = "NOT_BPM";
    public static final String UPLOAD_DOCUMENT = "uploadDocument";
    public static final String DOCUMENT = "document";
    public static final String APPROVAL_URL="APPROVAL_URL";
    public static final String SELECTEDFOLDER1 = "selectedFolder1";
    public static final String SELECTED_FOLDER2 = "selectedFolder2";
    public static final String TRIGGER_APPROVAL_SUCCESS = "Approval Process Successfully Triggered";
    public static final String TRIGGER_APPROVAL_FAILED = "Approval Process Trigger Was Unsuccessful";
    public static final String APPROVAL_SUCCESS = "Approval successfully completed";
    public static final String APPROVAL_FAILED = "Approval was unsuccessful";
    public static final String REJECT_SUCCESS = "Rejection was successful";
    public static final String REJECT_FAILED = "Rejection was unsuccessful";
    public static final String CANCEL_INFLIGHT_APPROVALS_SUCCESS = "This action was successfully completed";
    public static final String CANCEL_INFLIGHT_APPROVALS_FAILURE = "This action was unsuccessful";
    public static final String PASS_APPROVAL_SUCCESS = "Pass approval was successfully completed";
    public static final String PASS_APPROVAL_FAILURE = "Pass approval was unsuccessful";
    public static final String PROGRAM_LINES_EHS ="ProgramLinesEHSIterator";
    public static final String PROGRAM_LINES_FCT ="ProgramLineFctIterator";
    public static final String PROGRAM_LINES_IMPS ="ProgramLinesImpsIterator";
    public static final String PROGRAM_LINES_SERVICE ="ProgramCostLinesOtherServiceIterator";
    public static final String PROGRAM_LINES_PERSONNEL ="ProgramCostLinesParIterator";
    public static final String PROGRAM_COST_HEADER ="ProgramCostHeaderId";
    public static final String PROGRAM_COST_HEADER_ITER= "CctProgramCostHeaderEOVO2Iterator";
    public static final String PROGRAM_CATEGORY ="CategoryTypeId";
    public static final String FIXED_PROGRAM ="Fixed";
    public static final String RECURRING_PROGRAM ="Recurring";
    public static final String ELECTRONICS ="electronics";
    public static final String FACILITIES ="facilities";
    public static final String IMPLEMENTATION ="programLinesImplementation";
    public static final String OTHER_SERVICES ="otherServices";
    public static final String PERSONNEL_LINES ="Personnel";
    public static final String ELECTRONICS_PROGRAM_TYPE = "electronicsProgramType";
    public static final String IMPLEMENTATION_PROGRAM_TYPE = "implementationProgramType";
    public static final String OTHER_SERVICES_PROGRAM_TYPE = "otherServicesProgramType";
    public static final String FACILITY_PROGRAM_TYPE = "facilitiesProgramType";    
    public static final String PERSONNEL_LINES_PROGRAM_TYPE = "personnelProgramType";
    public static final String PROGRAM_TYPE ="programType";
    public static final String INVALID_FILE_FORMAT = "File Format is invalid";

    public static final String FILE_NOT_SELECTED = "Plese select file";
    public static final String LOV_ERROR_MESSAGE = "The field should be one of them from the dropdown";
    public static final String DATE_DUE_BACK_CONTRACTS = "Date due back to Contracts";
    public static final String WORK_QUEUE = "Work Queue";
    public static final String AST_TEMPLATE = "ASTTemplate.xlsx";
    public static final String FLASHLIGHT_TEMPLATE = "FlashlightTemplate.xlsx";
    public static final String ELECTRICAL_QUEUE_TEMPLATE = "ElectricalQueueTemplate.xlsx";
    public static final String HPP_TEMPLATE = "HPPTemplate.xlsx";
    public static final String BEACON_TEMPLATE = "BeaconTemplate.xlsx";
    public static final String FINAL_BEACON_TEMPLATE = "FinalBeaconTemplate.xlsx";
    public static final String UNKNOWN_TEMPLATE = "UnknownTemplate.xlsx";
    public static final String ACID_REPORT_TEMPLATE = "AcidReport.xlsx";
    public static final String BEACON_VERSION_DOWNLOAD_TEMPLATE = "BeaconVersionDownloadTemplate.xlsx";
    public static final String FINAL_BEACON_VERSION_DOWNLOAD_TEMPLATE = "FinalBeaconVersionTemplate.xlsx";
    public static final String PROG_COST_DOWNLOAD_TEMPLATE = "Program_Cost_Template.xlsx";
    public static final String ACID_REP_KEY = "ACID_REPORT_DOWNLOAD";
    public static final String BEACON_VERSION_DOWNLOAD_KEY = "BEACON_VERSION_DOWNLOAD";
    public static final String FINAL_BEACON_VERSION_DOWNLOAD_KEY = "FINAL_BEACON_VERSION_DOWNLOAD";
    public static final String BEACON_DOWNLOAD_KEY = "BEACON_FILE_DOWNLOAD";
    public static final String FINAL_BEACON_DOWNLOAD_KEY = "FINAL_BEACON_DOWNLOAD";
    public static final String UNKNOWN_DOWNLOAD_KEY = "UNKNOWN_DOWNLOAD";
    public static final String HPP_DOWNLOAD_KEY = "HPP_REPORT_DOWNLOAD";
    public static final String LIGHTING_DOWNLOAD_KEY = "LIGHTING_REPORT_DOWNLOAD";
    public static final String ELECTRICAL_DOWNLOAD_KEY = "ELECTRICAL_REPORT_DOWNLOAD";
    public static final String AST_DOWNLOAD_KEY = "AST_REPORT_DOWNLOAD";
     public static final String SP_DOWNLOAD_KEY = "STRATEGIC_PRICING_DOWNLOAD";
    public static final String NOBID_DOWNLOAD_KEY = "NO_BID_DOWNLOAD";
    public static final String ALLPARTS_DOWNLOAD_KEY = "ALL_PARTS_DOWNLOAD";
    public static final String TOP50_PARTS_DOWNLOAD_KEY = "TOP50_PARTS_DOWNLOAD";
    public static final String TOP50_FINAL_PARTS_DOWNLOAD_KEY = "TOP50_FINAL_PARTS_DOWNLOAD";
    public static final String LIGHTING_HEADER_KEY = "LIGHT_REP_HEADER";
    public static final String ELECTRICAL_QUEUE_HEADER_KEY = "ELECTRICAL_REP_HEADER";
    public static final String ACID_REP_HEADER_KEY = "ACID_REP_HEADER";
    public static final String AWARD_TAG_LOV = "AWARD_TAG_LOV";
    public static final String AWARDS_REVIEW_DOWNLOAD_KEY = "AWARDS_REVIEW_DOWNLOAD";
    public static final String AWARDS_REVIEW_TEMPLATE = "Award Review Template.xlsx";
    public static final String TOP_50_FINAL_DOWNLOAD_TEMPLATE = "TOP50FinalBeaconDownloadTemplate.xlsx";
    public static final String UPLOAD_TYPE1 = "uploadType";
    public static final String UPLOAD_STATUS1 = "uploadStatus";
    public static final String TASK_ASSIGNMENT = "Task has been reassigned successfully to  ";
    public static final String SAVED_POPUP = "Saved Successfully";
    public static final String SUBMIT_TO_SALES_POPUP = "There is no approved snapshot to submit to Sales";
    public static final String QUOTE_SUBMIT_SUCCESS = "Sales has been notified that documents are ready to submit.";
    public static final String QUOTE_SUBMIT_FAIL = "Quote submit unsuccessful";
    public static final String MARGIN_SAVE_SUCCESS = "Details saved Successfully";
    public static final String MARGIN_SAVE_FAIL = "Details unsuccessful";
    public static final String MANAGE_APPROVAL = "ManageApproval";
    public static final String TASK_TYPE = "TaskType";
    public static final String TASK_OWNER_USER = "TaskOwnerUser";
    public static final String CREATE = "CREATE";
    public static final BigDecimal[] categoryID = {new BigDecimal(1),new BigDecimal(2),new BigDecimal(3),new BigDecimal(4),new BigDecimal(5)};
    public static final String LOGGED_IN_USER = "loggedInUser";
    public static final String COST_COMPARISON_DETAILS = "COST_COMPARISON_DETAILS";
    public static final String APPROVAL = "approval";
    public static final String[] FINAL_BEACON_DOWNLOAD_HEADERS = {"Total Cost", "Total Resale"};
    public static final String[] FINAL_PARTS_VO_ATTIBUTES = {"TotalCost", "TotalResale"};
    public static final String GENERIC_EXCEPTION = "approval";
    public static final String CCT_SYS_EX05 = "CCT_SYS_EX05";
    public static final String[] beaconUploadConstants = {"RI", "UP"};
    public static final String CREATE_APPROVAL_SNAPSHOT = "cct.opportunitydetails.finalpricing.create-approval-snapshot-button";
    public static final String GENERATE_QUOTE = "cct.opportunitydetails.finalpricing.generate-quote-button";
    public static final String SUBMIT_SALES = "cct.opportunitydetails.opportunity.submit-to-sales-button";
    public static final String QUOTE_DOWNLOAD = "cct.opportunitydetails.quote.quote-download-link";
    public static final String FINAL_QUOTE_DOWNLOAD = "cct.opportunitydetails.finalpricing.quote-download-link";
    public static final String READY_APPROVAL = "cct.opportunitydetails.finalpricing.ready-for-approval-button";
    public static final String CREATE_SNAPSHOT = "cct.opportunitydetails.parts.create-snapshot-button";
    public static final String FINAL_CREATE_SNAPSHOT = "cct.opportunitydetails.finalparts.create-snapshot-button";
    public static final String CREATE_ERROR = "CreateError";
    public static final String DOWNLOAD_AWARD = "cct.opportunitydetails.awards.download-link";
    public static final String UPLOAD_AWARD = "cct.opportunitydetails.awards.upload-link";
    public static final String CREATE_TASK = "cct.opportunitydetails.task.create-button";
    
    //following added on 29/05/18
    public static final String DOWNLOAD_FINAL = "cct.opportunitydetails.finalpricing.downloadquote-link";
    public static final String CLOSE_TASK = "cct.opportunitydetails.taskupdate.task.close-button";    
    public static final String ADD_SEARCH_NOTES = "cct.opportunitydetails.notes.addsearchnotes-button";
    public static final String DOWNLOAD_COST = "cct.opportunitydetails.additionalcost.downloadcost-link";
    public static final String QUOTED_NOT_AWARDED = "cct.opportunitydetails.awards.quotednotawarded-link";
    public static final String MOVE_DELETE_UPLOAD_DMS_DOCUMENT = "cct.opportunitydetails.dms.move.delete.upload.document-button";
    public static final String CREATE_OPPORTUNITY = "cct.taskdashboard.createopportunity-button";
    public static final String TASKDASHBOARD_ASSIGNEES = "cct.taskdashboard.taskdashboardassignees-selectchoice";
    public static final String CONVERT_TO_OPPORTUNITY = "cct.updateopportunitydetails.converttoopportunity-button";
    public static final String CREATE_REVISION = "cct.updateopportunitydetails.createrevision-button";
    public static final String UPDATE_EDIT_OPPORTUNITY = "cct.updateopportunitydetails.update.edit.opportunity-button";    
    public static final String ADD_SEARCH_OPPTY_NOTES = "cct.updateopportunitydetails.notes.addsearchnotes-button";
    public static final String TOP50_FINAL_PARTS_DETAILS = "cct.opportunitydetails.finalpricing.downloadtop50finalparts-link";
    public static final String OPPORTUNITY_DASHBOARD_ASSIGNEE = "cct.opportunitydashboard.opportunitydashboardassignees-selectchoice";  
    public static final String OPPORTUNITY_DASHBOARD_CREATE_OPPORTUNITY = "cct.opportunitydashboard.createopportunity-button";
    public static final String SALES_HIERARCHY_ADD_TEAM = "cct.saleshierarchy.addteam-button";   
    public static final String SALES_HIERARCHY_EDIT_TEAM = "cct.saleshierarchy.editteam-button";  
    public static final String TASK_UPDATE_CLOSE_TASK = "cct.opportunitydetails.taskupdate.closetask-button";
    public static final String TASK_UPDATE_ADD_NOTES = "cct.opportunitydetails.taskupdate.addnotes-button";
    
    public static final String GROUP_ID = "groupId";
    public static final String UPLOAD_FINAL_BEACON = "cct.opportunitydetails.finalpricing.detail.quote-upload-link";
    public static final String DOWNLOAD_FINAL_BEACON = "cct.opportunitydetails.finalpricing.detail.quote-download-link";
    public static final String UPLOAD_BEACON = "cct.opportunitydetails.parts.all.upload-link";
    public static final String DOWNLOAD_BEACON = "cct.opportunitydetails.parts.all.download-link";
    public static final String UPLOAD_LIGHTING = "cct.opportunitydetails.parts.lighting.upload-link";
    public static final String UPLOAD_ELECTRICAL = "cct.opportunitydetails.parts.electrical.upload-link";
    public static final String UPLOAD_NOBID = "cct.opportunitydetails.parts.nobid.upload-link";
    public static final String DOWNLOAD_LIGHTING = "cct.opportunitydetails.parts.lighting.download-link";
    public static final String DOWNLOAD_ELECTRICAL = "cct.opportunitydetails.parts.electrical.download-link";
    public static final String UPLOAD_ADVANCE_SOURCE = "cct.opportunitydetails.parts.advancedsourcing.upload-link";
    public static final String DOWNLOAD_ADVANCE_SOURCE = "cct.opportunitydetails.parts.advancedsourcing.download-link";
    public static final String UPLOAD_COMMODITY = "cct.opportunitydetails.parts.commodities.upload-link";
    public static final String DOWNLOAD_COMMODITY= "cct.opportunitydetails.parts.commodities.download-link";
    public static final String UPLOAD_UNKNOWN = "cct.opportunitydetails.parts.unknown.upload-link";
    public static final String DOWNLOAD_UNKNOWN = "cct.opportunitydetails.parts.unknown.download-link";
    public static final String DOWNLOAD_NOBID = "cct.opportunitydetails.parts.nobid.download-link";
    public static final String UPLOAD_PRICING = "cct.opportunitydetails.parts.strategicpricing.upload-link";
    public static final String DOWNLOAD_PRICING = "cct.opportunitydetails.parts.strategicpricing.download-link";
    public static final String UPLOAD_HPP = "cct.opportunitydetails.parts.hpp.upload-link";
    public static final String DOWNLOAD_HPP = "cct.opportunitydetails.parts.hpp.download-link";
    public static final String UPLOAD_CHEMICAL = "cct.opportunitydetails.parts.chemical.upload-link";
    public static final String DOWNLOAD_CHEMICAL = "cct.opportunitydetails.parts.chemical.download-link";
    public static final String MOVE_PARTS = "cct.opportunitydetails.parts.go-button";
    public static final String APPROVE_ACTION = "cct.opportunitydetails.approval.approve-button";
    public static final String REJECT_ACTION = "cct.opportunitydetails.approval.reject-button";
    public static final String TRIGGER_APPROVAL_ACTION = "cct.opportunitydetails.approval.trigger-approval-button";
    public static final String CANCEL_INFLIGHT_APPROVAL_ACTION = "cct.opportunitydetails.approval.cancel-In-Flight-approval-button";
    public static final String PASS_APPROVAL_ACTION = "cct.opportunitydetails.approval.pass-approval-button";
    public static final String UPDATE_PROJECT_NUMBER = "cct.updateopportunitydetails.projectNumber.input-text";
    public static final String UPDATE_CONTRACT_TEAM_MEMBERS = "cct.updateopportunitydetails.contractTeamMembers.drop-down";
    public static final String CREATE_PROJECT_NUMBER = "cct.createopportunity.projectNumber.input-text";
    public static final String CREATE_CONTRACT_TEAM_MEMBERS = "cct.createopportunity.contractTeamMembers.drop-down";
    public static final String READY_FOR_APPROVAL = "APPROVAL SOURCE";
    public static final String OPP_SUCESS_MSG = "Opportunity created Successfully";
    public static final String OPP_FAILURE_MSG = "Sorry, Opportunity creation unsuccessful, please try again.";
    public static final String CCT_SALES = "CCT_SALES";
    public static final String SALES_USER = "salesUser" ;
    public static final String APPROVAL_TASK_TAB = "cct.home.taskDashboard.approval-task-tab";
    public static final String SALES_HIERARCHY_LINK="cct.taskDashBoard.salesHierarchy-link";
    public static final String ATTACHMENT_PAGE = "attachmentPage";
    public static final String ISATTACHMENTTHROUGHEMAIL = "isAttachmentThroughEmail"; 
    public static final String ISAPPROVALTHROUGHEMAIL = "isApprovalThroughEmail"; 
    public static final String OPPTY_STATUS_CHANGE = "OPPORTUNITY_STATUS_CHANGE";
    public static final String INVALID_CUSTNO_MSG = "Enter Valid Customer Number, Leading Zero applies.";
    public static final String INVALID_UPDATE_CUSTNO_MSG = "Please enter a valid Customer Number.";
    public static final String REVISION_CONFIRMATION_MSG = "A new revision will be created. Press OK to proceed";
    public static final String REVISION_SUCCESS_MSG = "Revision has been activated for ";
    public static final String DATE_MGMT_SAVE_MSG = "Opportunity Details Updated successfully!!";
    public static final long DELAY_TRIGGER=4000;
    public static final long DELAY_CANCEL=4000;
    public static final long DELAY_APPROVE=2000;
    public static final long DELAY_REJECT=2000;
    public static final String NO_APPROVALS="Please add the approvers before triggering an approval.";
    //following added on 29/06/18
    public static final String[] opportunityStatus = {"Active", "Pending", "Qualification"};
    public static final String[] returnFormat = {"Both KLX and Customer format", "KLX format", "Both Boeing Distribution and Customer format", "Boeing Distribution format", "Customer Format"};
    public static final String[] legalFormat = {"KLX Standard Terms", "Boeing Distribution Standard Terms", "Customer Document"};
    //following added on 9/08/18
    public static final String INVALID_TOP50_PARTS = "This busines request cannot  be processed right now.Please try after some time";
    public static final String PROGRAM_COST_DATA_SHEET = "ProgramCost";
    
    public static final String[] PROG_COST_TEMPLATE_HEADERS = {"Locations:", "One Time Program Set Up Costs:", "Total Program Costs:", "Director/Manager:", "Annual Recurring Program Cost:", "Comments:"};
    public static final String PROG_COST_ELEC_HEADER            = "Electronics - Hardware, Software, Services";
    public static final String PROG_COST_OTHER_SER_HEADER       = "Other Service Solutions";
    public static final String PROG_COST_IMPL_HEADER            = "Implentation & Setup";
    public static final String PROG_COST_FAC_HEADER             = "Facility/WHSE";
    public static final String PROG_COST_PER_HEADER0             = "Personnel";
    public static final String PROG_COST_PER_HEADER1             = "# of Individuals";
    public static final String PROG_COST_PER_HEADER2             = "% of Workload";
    public static final String PROG_COST_PER_HEADER3             = "Annual Personnel Costs";
    public static final String PROG_COST_PER_HEADER4             = "Sub Total";
    
    public static final String PROG_COST_LINES_EHS_ITER         = "ProgramLinesEHSIterator";
    public static final String PROG_COST_LINES_IMPS_ITER        = "ProgramLinesImpsIterator";
    public static final String PROG_COST_LINES_OTHER_SER_ITER   = "ProgramCostLinesOtherServiceIterator";
    public static final String PROG_COST_LINES_FCT_ITER         = "ProgramLineFctIterator";
    public static final String PROG_COST_LINES_PER_ITER         = "ProgramCostLinesParIterator";
    public static final String PROG_COST_EHS_NOTES_ITER         = "CctProgramCostSummNotesEHSVOIterator";
    public static final String PROG_COST_IMPS_NOTES_ITER         = "CctProgramCostSummNotesImpVOIterator";
    public static final String PROG_COST_OTH_NOTES_ITER         = "CctProgramCostSummNotesOthVOIterator";
    public static final String PROG_COST_FAC_NOTES_ITER         = "CctProgramCostSummNotesFacVOIterator";
    public static final String PROG_COST_PER_NOTES_ITER         = "CctProgramCostSummNotesPerVOIterator";
    public static final String EMPTY_STRING = "";
    
    public static final String PROG_COST_CAT_REC_SUB_TOT_HDR    = "Category Recurring Sub Total";
    public static final String PROG_COST_CAT_FIX_SUB_TOT_HDR    = "Category Fixed Sub Total";
    public static final String PROG_COST_NOTES_HDR              = "Notes";
    public static final String PROG_COST_PER_NOTES_HDR          = "Personnel Expense Comments";
    public static final String PROG_COST_TOT_PER_EXP_HDR          = "Total Personnel Expense";
    public static final String[] missingBeaconHeaders = {
        "Customer Part Number", "Customer PN", "L/I", "Quoted  Part Number", "Annual  Qty", "UOM",
        "Total  Qty Length  Contract"};
    public static final String[] missingAwardBeaconHeaders = {
        "Original Awarded Part Number", "Inv Part", "Loaded Certified Part Number", "Awarded Price", "Awarded UOM", "Awarded Item Class",
        "Awarded Lead-Time"};
    public static final String[] totalAwardBeaconHeaders = {
    "Award Tags",
    "Original Awarded Part Number",
    "Inv Part",
    "Loaded Certified Part Number",
    "Awarded EAU",
    "Awarded Price",
    "Awarded UOM",
    "Awarded Item Class",
    "Awarded Lead-Time",
    "Awarded Cust Reference",
    "Loaded Prime",
    "Awarded Approved Sources",
    "Extended Sales",
    "Extended Cost",
    "Margin",
    "Loaded CP#",
    "Opportunity #",
    "Amendment",
    "L/N",       "L/I",
    "Customer Part Number", "Customer PN",
    "Certified Part #",
    "Annual Qty",
    "Price",
    "UOM",
    "Item Classification",
    "Leadtime",
    "Comment",
    "Boeing Distribution Quote #",
    "Prime",
    "Cost",
    "Cost Source",
    "Manufacturer",    "Manf Name",
    "Work Queue",
    "Part Description"                                
};
}

