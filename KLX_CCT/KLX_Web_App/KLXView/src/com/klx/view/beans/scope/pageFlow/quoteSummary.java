package com.klx.view.beans.scope.pageFlow;

import com.klx.common.access.UserActionAccessManagement;
import com.klx.common.logger.KLXLogger;
//import com.klx.services.entities.Quote;

import com.klx.view.beans.constants.ViewConstants;
import com.klx.view.beans.utils.ADFUtils;

import com.klx.view.beans.utils.JSFUtils;

import com.klx.view.error.CustomErrorHandler;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.math.BigDecimal;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import java.util.logging.Level;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.binding.BindingContainer;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;
import oracle.jbo.uicli.binding.JUCtrlListBinding;
import oracle.adf.share.ADFContext;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class quoteSummary {
   // private RichPopup createPopup;
   // private String fileName;
//   private HashMap<String, String> beaconMapping = new HashMap<String, String>();
//   private HashMap<String, String> allQuoteVoAttrMapping = new HashMap<String, String>();
//   private HashMap<String, String> dataMaping = new HashMap<String, String>();
    
    public quoteSummary() {
    }

    private static KLXLogger logger = KLXLogger.getLogger();    
    private BigDecimal quote;
    private String opportunityNumber;
    private HashMap<String, String[]> beaconMapping = new HashMap<String, String[]>();
    private HashMap<String, String> quoteVoAttrMapping = new HashMap<String, String>();
    private boolean downloadQuote = false;
    
    public void userActionsAccess(){
        HashMap actions = new HashMap();
        actions.put(ViewConstants.QUOTE_DOWNLOAD, false);        
        HashMap userAcess = UserActionAccessManagement.getUserAccess(ADFContext.getCurrent().getSecurityContext().getUserRoles(), actions);        
        downloadQuote = ((Boolean)userAcess.get(ViewConstants.QUOTE_DOWNLOAD)).booleanValue();               
    }

    public void setDownloadQuote(boolean downloadQuote) {
        this.downloadQuote = downloadQuote;
    }

    public boolean isDownloadQuote() {
        return downloadQuote;
    }


    public void setQuote(BigDecimal quote) {
        this.quote = quote;
    }

    public BigDecimal getQuote() {
        logger.log(Level.INFO, getClass(),
                                        "getQuote",
                                        "quote value ==="+quote);

        return quote;
    }


    public String getOpportunityNumber() {
        return opportunityNumber;
    }


    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public void quoteRevisionChnageListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...

        try {
            logger.log(Level.INFO, getClass(),
                                                "quoteRevisionChnageListener",
                                                "Entering quoteRevisionChnageListener method");

            DCBindingContainer binds = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding quoteRevisionIter = binds.findIteratorBinding("DistinctQuoteRevVO1Iterator");
            //quoteRevisionIter.executeQuery();
            logger.log(Level.INFO, getClass(),
                                                "quoteRevisionChnageListener",
                                                ("current row ==" + quoteRevisionIter.getCurrentRow().getAttribute("RevisionId")));
            /*JUCtrlListBinding listBinding =(JUCtrlListBinding)BindingContext.getCurrent().getCurrentBindingsEntry().get("DistinctQuoteRevVO1");
            Row selectedValue = (Row) listBinding.getSelectedValue();*/
            OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                                   .getCurrentBindingsEntry()
                                                                   .getOperationBinding("fetchQuoteDetails");
            logger.log(Level.INFO, getClass(),
                                                "quoteRevisionChnageListener",
                                                ("elected revision id===" + valueChangeEvent.getNewValue()));

            op.getParamsMap().put("revisionID", quoteRevisionIter.getCurrentRow()
                                                                 .getAttribute("RevisionId")
                                                                 .toString());
            op.getParamsMap().put("quoteVersion", valueChangeEvent.getNewValue());
            String msg = "";
            op.execute();
        } catch (Exception e) {
            // TODO: Add catch code
         //   e.printStackTrace();
            
            logger.log(Level.SEVERE, getClass(),
                                                       "quoteRevisionChnageListener",
                                                      e.getMessage()); 
            CustomErrorHandler.processError("CCT","quoteRevisionChnageListener","CCT_SYS_EX20",e);   
        }
        logger.log(Level.INFO, getClass(),
                                            "quoteRevisionChnageListener",
                                            "Exiting quoteRevisionChnageListener method");

    }
/**
    * needed for ready for approval button  of quote
    * now moved to FinalPricing
    */ 
//    public void readyForApprove(ActionEvent actionEvent) {
//        logger.log(Level.INFO, getClass(),
//                                            "readyForApprove",
//                                            "Entering readyForApprove method");
//
//        BindingContext bindingContext = BindingContext.getCurrent();
//            BindingContainer bindingContainer = bindingContext.getCurrentBindingsEntry();
//        oracle.binding.OperationBinding binding = bindingContainer.getOperationBinding("readyForApproval");
//        logger.log(Level.INFO, getClass(),
//                                            "readyForApprove",
//                                            "Opportunity Number: "+JSFUtils.resolveExpression("#{bindings.OpportunityNumber.inputValue}"));
//        binding.getParamsMap().put("opportunityNumber",(String)JSFUtils.resolveExpression("#{bindings.OpportunityNumber.inputValue}"));
//        binding.execute();
//        RichPopup.PopupHints hints=new RichPopup.PopupHints();
//       getCreatePopup().show(hints);
//    }
    /**
    * needed for genertae quote button  of quote
    * now moved to FinalPricing
    */
//    public void downloadQuoteRep(FacesContext facesContext, OutputStream outputStream) {
//        try {
//            logger.log(Level.INFO, getClass(),
//                                                "downloadQuoteRep",
//                                                "Entering downloadQuoteRep method");
//
//            String dealType = (String) JSFUtils.resolveExpression("#{bindings.DealType.inputValue}");
//            String mappingKey=null;
//            String fileName=null;
//            if("RENEW_CONTRACT".equalsIgnoreCase(dealType)){
//                    mappingKey="RENEW_QUOTE_REP";
//                     fileName = "RenewalQuoteTemplate.xlsx";
//                }
//            else{
//                    mappingKey="NEW_QUOTE_REP";
//                     fileName = "NewContractQuoteTemplate.xlsx";
//                }
//            
//            //model
//           // ADFUtils.getBindingContainer().getOperationBinding("getAllQuotePartsVOAttr").execute();
//            ADFUtils.getBindingContainer().getOperationBinding("getAllQuotePartsSnapshotFinalVOAttr").execute();
//            OperationBinding generateTemplate =(OperationBinding) ADFUtils.getBindingContainer().getOperationBinding("generateTemplate");
//           
//            generateTemplate.getParamsMap().put("pMappingKey", mappingKey);
//            HashMap tempHashMap = (HashMap) generateTemplate.execute();
//         
//            beaconMapping= (HashMap) tempHashMap.get("templateMapping");
//            allQuoteVoAttrMapping = (HashMap) tempHashMap.get("tasksVoAttrMapping");
//            dataMaping=(HashMap) tempHashMap.get("dataMapping");
//           //to get templete file
//            
//           String filePath= ViewConstants.FILE_PATH + fileName;
//            FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);
//            XSSFWorkbook workbook = new XSSFWorkbook(file);
//           
//           //view layer
//            XSSFWorkbook filledWorkbook = genericLogic(workbook, "AllQuotePartsSnapshotFinalVO1Iterator", 3, 2);
//          
//
//            try {
//                logger.log(Level.SEVERE, getClass(),
//                                                           "downloadQuoteRep", "Writing workbook");
//                filledWorkbook.write(outputStream);
//           
//                outputStream.flush();
//                outputStream.close();
//            } catch (Exception e) {
//                logger.log(Level.SEVERE, getClass(),
//                                                           "downloadQuoteRep",
//                                                          e.getMessage()); 
//                CustomErrorHandler.processError("CCT","downloadQuoteRep","CCT_SYS_EX29",e);   
//            }
//
//
//        } catch (Exception e) {
//       //     e.printStackTrace();
//            logger.log(Level.SEVERE, getClass(),
//                                                       "downloadQuoteRep",
//                                                      e.getMessage()); 
//            CustomErrorHandler.processError("CCT","downloadQuoteRep","CCT_SYS_EX29",e);   
//        }
//        logger.log(Level.INFO, getClass(),
//                                                            "downloadQuoteRep",
//                                                            "Exiting downloadQuoteRep method");
//
//
//    }
    /**
    * needed in downloadQuoteRep  method of genertae quote button  of quote
    * now moved to FinalPricing
    */
    
//    public XSSFWorkbook genericLogic(XSSFWorkbook workbook, String Iterator, int sheetrownum, int headerrownum) {
//        try {
//            logger.log(Level.INFO, getClass(),
//                                                                "genericLogic",
//                                                                "Entering genericLogic method");
//
//            DataFormat numFormatWithoutDecimal = workbook.createDataFormat(); //format defined
//            XSSFCellStyle cellStyleNumber = workbook.createCellStyle();
//            cellStyleNumber.setDataFormat(numFormatWithoutDecimal.getFormat("#,###.0000"));
//            cellStyleNumber.setBorderBottom(XSSFCellStyle.BORDER_THIN);
//            cellStyleNumber.setBorderTop(XSSFCellStyle.BORDER_THIN);
//            cellStyleNumber.setBorderLeft(XSSFCellStyle.BORDER_THIN);
//            cellStyleNumber.setBorderRight(XSSFCellStyle.BORDER_THIN);
//
//
//
//            XSSFCellStyle cellStyleForBorder = workbook.createCellStyle();
//            cellStyleForBorder.setBorderBottom(XSSFCellStyle.BORDER_THIN);
//            cellStyleForBorder.setBorderTop(XSSFCellStyle.BORDER_THIN);
//            cellStyleForBorder.setBorderLeft(XSSFCellStyle.BORDER_THIN);
//            cellStyleForBorder.setBorderRight(XSSFCellStyle.BORDER_THIN);
//            XSSFSheet sheet = workbook.getSheetAt(0);
//         
//            XSSFRow fRow = sheet.getRow(0);
//            XSSFCell cell = fRow.getCell(0);
//
//            String custname=(String)JSFUtils.resolveExpression("#{pageFlowScope.custName}");
////            String custDate=(String)JSFUtils.resolveExpression("#{pageFlowScope.custDate}");
//            String quoteNumber=(String)JSFUtils.resolveExpression("#{bindings.QuoteNumber.inputValue}");
//          
//            String finalStr="Exhibit A to Proposal"+"   "+ "Customer Name :"+ custname+"   "+ "Quote # "+quoteNumber;
//            cell.setCellValue(finalStr);
//            
//            
//            XSSFRow row = sheet.getRow(1);
//            XSSFCell cell_2 = row.getCell(0);
//            String finalStr2 = getFileName();
//            cell_2.setCellValue(finalStr2);
//           
//            DCBindingContainer bindings = ADFUtils.getDCBindingContainer();
//            DCIteratorBinding itorBinding = bindings.findIteratorBinding(Iterator);
//            ViewObject object = itorBinding.getViewObject();
//            RowSetIterator iterator = object.createRowSetIterator(null);
//            int i = sheetrownum;
//            org.apache.poi.ss.usermodel.Row headerRow = sheet.getRow(headerrownum);
//
//            while (iterator.hasNext()) { //Row iteration
//                Row currentRow = iterator.next();
//                XSSFRow sheetRow = sheet.createRow(i);
//                i++;
//                int index = 0; //Column
//                for (index = 0; index < currentRow.getAttributeCount(); index++) {
//
//
//                    XSSFCell currentCell = sheetRow.createCell(index);
//                    currentCell.setCellStyle(cellStyleForBorder);
//                    // Cell newCell = headerRow.getCell(currentCell.getColumnIndex());
//                    Cell newCell = headerRow.getCell(index);
//                    
//                    if (null != newCell && newCell.getCellTypeEnum() != CellType.BLANK) {
//                        String newHeader = newCell.getStringCellValue();
//                        logger.log(Level.INFO, getClass(),
//                                                                            "genericLogic",
//                                                                            "Header value of new cell" + newHeader);
//
//                        String voAttribute = allQuoteVoAttrMapping.get(beaconMapping.get(newHeader));
//                        String dataFormat=dataMaping.get(newHeader);
//                        
//
//                        if (null != currentRow && voAttribute != null && currentRow.getAttribute(voAttribute) != null) {
//                            
//
//                            if ("COST".equalsIgnoreCase(dataFormat)) {
//                                currentCell.setCellType(CellType.NUMERIC);
//                                currentCell.setCellStyle(cellStyleNumber);
//                                currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(voAttribute)
//                                                                        .toString()));
//                            }else if("NUMBER".equalsIgnoreCase(dataFormat))
//                                currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(voAttribute)
//                                                                        .toString()));
//                            else
//                                currentCell.setCellValue(currentRow.getAttribute(voAttribute).toString());
//                            
//                        } else {
//                            currentCell.setCellValue("");
//                        }
//
//                    }
//
//                }
//            }
//
//        } catch (Exception e) {
//            // TODO: Add catch code
//          e.printStackTrace();
//            logger.log(Level.SEVERE, getClass(),
//                                                       "genericLogic",
//                                                      e.getMessage()); 
//            CustomErrorHandler.processError("CCT","downloadQuoteRep","CCT_SYS_EX05",e);   
//        }
//        logger.log(Level.INFO, getClass(),
//                                                            "genericLogic",
//                                                            "Exiting genericLogic method");
//
//        return workbook;
//
//    }
    /**
    * needed for ready for approval button to show popup  of quote
    * now moved to FinalPricing
    */
//    public void setCreatePopup(RichPopup createPopup) {
//        this.createPopup = createPopup;
//    }
//
//    public RichPopup getCreatePopup() {
//        return createPopup;
//    }
//
//    public void closePopup(ActionEvent actionEvent) {
//        getCreatePopup().hide();
//    }


    //    public void setFileName(String fileName) {
//        this.fileName = fileName;
//    }
//
//    public String getFileName() {
//        SimpleDateFormat dateFormat = new SimpleDateFormat("MMddyyyy");
//        Date dateNew = new Date();
//        String format = dateFormat.format(dateNew);
//        String custname=(String)JSFUtils.resolveExpression("#{pageFlowScope.custName}");
//        String oppNumber=(String)JSFUtils.resolveExpression("#{pageFlowScope.oppNumb}");
//                    String finalStr2=format+"_"+custname+"_"+oppNumber+"_"+"Quote File"+".xlsx";
//        return finalStr2;
//    }

    /**
     * needed for Generate quote button of quote
     * now moved to FinalPricing
     */
    public DCBindingContainer getDCBindingsContainer() {
        DCBindingContainer bindingsContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        return bindingsContainer;
    }

    public void generateDownloadTemplate(String mappingkey) {
        oracle.binding.OperationBinding generateDownloadTemplate =
            ADFUtils.getBindingContainer().getOperationBinding("generateDownloadTemplate");
        generateDownloadTemplate.getParamsMap().put("pMappingKey", mappingkey);
        HashMap hashMap = (HashMap) generateDownloadTemplate.execute();

        quoteVoAttrMapping = (HashMap) hashMap.get("allQuoteVoAttrMapping");
        beaconMapping = (HashMap) hashMap.get("downloadBeaconMapping");
    }
    
    public XSSFWorkbook genericLogic(XSSFWorkbook workbook, String Iterator, int sheetrownum, int headerrownum, String quoteRevisionId) {
        //CellStyle for DataFormatting Number without Decimal
        DataFormat numFormatWithoutDecimal = workbook.createDataFormat(); //format defined
        XSSFCellStyle cellStyleNumber = workbook.createCellStyle();
        cellStyleNumber.setDataFormat(numFormatWithoutDecimal.getFormat("#,###.0000"));
        cellStyleNumber.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        cellStyleNumber.setBorderTop(XSSFCellStyle.BORDER_THIN);
        cellStyleNumber.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        cellStyleNumber.setBorderRight(XSSFCellStyle.BORDER_THIN);


        //Below cell style is for Border
        XSSFCellStyle cellStyleForBorder = workbook.createCellStyle();
        cellStyleForBorder.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        cellStyleForBorder.setBorderTop(XSSFCellStyle.BORDER_THIN);
        cellStyleForBorder.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        cellStyleForBorder.setBorderRight(XSSFCellStyle.BORDER_THIN);
        XSSFSheet sheet = workbook.getSheetAt(0);

        DCBindingContainer bindings = this.getDCBindingsContainer();
        DCIteratorBinding itorBinding = bindings.findIteratorBinding(Iterator);
        ViewObject object = itorBinding.getViewObject();
        object.setNamedWhereClauseParam("bind_quoteRevisionId", quoteRevisionId);
        object.executeQuery();
        
        RowSetIterator iterator = object.createRowSetIterator(null);
        int i = sheetrownum;
        logger.log(Level.INFO, getClass(), "genericLogic", "Loop started for generic Parts download");

        org.apache.poi.ss.usermodel.Row headerRow = sheet.getRow(headerrownum);
        Row currentRow = null;
        XSSFRow sheetRow = null;
        int logHeaderOnce = 0;
        try {

            while (iterator.hasNext()) { //Row iteration
                currentRow = iterator.next();
                sheetRow = sheet.createRow(i);
                i++;
                int index = 0; //Column
                for (index = 0; index < currentRow.getAttributeCount(); index++) {
                    Cell newCell = headerRow.getCell(index);
                    if (newCell != null) {
                        XSSFCell currentCell = sheetRow.createCell(index);
                        currentCell.setCellStyle(cellStyleForBorder);


                        if (newCell.getCellTypeEnum() != CellType.BLANK) {
                            String newHeader = newCell.getStringCellValue();
                            if (logHeaderOnce == 0) {
                                logger.log(Level.INFO, getClass(), "genericLogic",
                                           "Header value of new cell= " + newHeader);
                            }
                            String destnFieldName = null;
                            if (beaconMapping.get(newHeader) != null) {
                                destnFieldName = (String) beaconMapping.get(newHeader)[0];
                            }

                            String voAttribute = null;
                            String dataFormat = "";
                            if (destnFieldName != null) {
                                voAttribute = quoteVoAttrMapping.get(destnFieldName);
                                dataFormat = (String) beaconMapping.get(newHeader)[1];
                            } else {
                                //If any column mappings r not found in mappings table
                                voAttribute = null;
                                dataFormat = "";
                            }
                            if (null != currentRow && voAttribute != null &&
                                currentRow.getAttribute(voAttribute) != null &&
                                "" != currentRow.getAttribute(voAttribute).toString()) {

                                switch (dataFormat) {
                                case "COST": //This is for celltype with numeric value with decimal & comma
                                    currentCell.setCellType(CellType.NUMERIC);
                                    currentCell.setCellStyle(cellStyleNumber);
                                    currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(voAttribute)
                                                                            .toString()));
                                    break;
                                case "NUMBER": //This is for celltype with numeric value
                                    currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(voAttribute)
                                                                            .toString()));
                                    break;
                                default:
                                    currentCell.setCellValue(currentRow.getAttribute(voAttribute).toString());
                                }

                            } else {
                                if(newHeader.equalsIgnoreCase(ViewConstants.FINAL_BEACON_DOWNLOAD_HEADERS[0])){                                     
                                    currentCell.setCellType(CellType.NUMERIC);
                                    currentCell.setCellStyle(cellStyleNumber);
                                    currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(ViewConstants.FINAL_PARTS_VO_ATTIBUTES[0]).toString()));
                                }
                                if(newHeader.equalsIgnoreCase(ViewConstants.FINAL_BEACON_DOWNLOAD_HEADERS[1])){
                                    currentCell.setCellType(CellType.NUMERIC);
                                    currentCell.setCellStyle(cellStyleNumber);
                                    currentCell.setCellValue(Double.valueOf(currentRow.getAttribute(ViewConstants.FINAL_PARTS_VO_ATTIBUTES[1]).toString()));
                                }
                                if(!newHeader.equalsIgnoreCase(ViewConstants.FINAL_BEACON_DOWNLOAD_HEADERS[0]) && !newHeader.equalsIgnoreCase(ViewConstants.FINAL_BEACON_DOWNLOAD_HEADERS[1]))
                                    currentCell.setCellValue("");
                                //Below logic is to Handle ASL which is coming from Opportunity table & not from QuoteParts
                                //                                if ("ASL".equals(newHeader)){
                                //                                    currentCell.setCellValue("ASLValue");
                                //                                }
                            }

                        }

                    }
                }
                logHeaderOnce++;
            }
        } catch (Exception nfe) {

            nfe.printStackTrace();
        }

        return workbook;

    }
    
    
    public void downloadBeaconVersionFile(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        try {
            String quoteVersion = (String) JSFUtils.resolveExpression("#{bindings.DistinctQuoteRevVO1.inputValue}");
           
            DCBindingContainer binds = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding quoteRevisionIter = binds.findIteratorBinding("DistinctQuoteRevVO1Iterator");
            String quoteRevisionId = quoteRevisionIter.getCurrentRow().getAttribute("RevisionId").toString();

            generateDownloadTemplate(ViewConstants.BEACON_VERSION_DOWNLOAD_KEY);

            facesContext.getCurrentInstance();
            String fileName = ViewConstants.BEACON_VERSION_DOWNLOAD_TEMPLATE;
            String filePath = ViewConstants.FILE_PATH + fileName;
            FileInputStream file = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(filePath);
            logger.log(Level.INFO, getClass(), "downloadBeaconVersionFile", "Excel read");

            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFWorkbook filledWorkbook = genericLogic(workbook, "AllQuotePartsVO1Iterator", 1, 0, quoteRevisionId);
            XSSFSheet sheet = filledWorkbook.getSheetAt(0);
            XSSFRow topRow = sheet.getRow(0);
            int lovCol = 0;
            for (Cell c : topRow) {
                if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                    String text = c.getStringCellValue();
                    if ("Work Queue".equalsIgnoreCase(text)) {
                        lovCol = c.getColumnIndex();
                        break;
                    }
                }
            }
            filledWorkbook = addExcelLOV(filledWorkbook, 1, lovCol);
            
            
            try {
                logger.log(Level.INFO, getClass(), "downloadBeaconVersionFile", "Writing workbook");

                filledWorkbook.write(outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (Exception e) {
                logger.log(Level.SEVERE, getClass(), "downloadBeaconVersionFile", e.getMessage());
                CustomErrorHandler.processError("CCT", "downloadBeaconVersionFile", "CCT_SYS_EX34", e);
            }


        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }

    }
    public XSSFWorkbook addExcelLOV(XSSFWorkbook workbook, int rowNum, int colNum) {

        String[] queues = getQueueNames();

        XSSFSheet sheet = workbook.getSheetAt(0);
        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sheet);
        XSSFDataValidationConstraint dvConstraint =
            (XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(queues);
        CellRangeAddressList addressList = new CellRangeAddressList(rowNum, sheet.getLastRowNum(), colNum, colNum);
        XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList);
        validation.setShowErrorBox(true);
        validation.setErrorStyle(XSSFDataValidation.ErrorStyle.STOP);
        validation.createErrorBox("Criteria Mismatch", ViewConstants.LOV_ERROR_MESSAGE);
        sheet.addValidationData(validation);
        validation.setSuppressDropDownArrow(false);


        return workbook;
    }
    public String[] getQueueNames() {
        DCBindingContainer bindingContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding binding = bindingContainer.findIteratorBinding("QuotePartsQueueVO1Iterator");
        ViewObject object = binding.getViewObject();
        int size = object.getRowCount();
        String[] queues = new String[size];
        ArrayList<String> queueNameList = new ArrayList<String>();

        RowSetIterator iterator = object.createRowSetIterator(null);
        while (iterator.hasNext()) {
            Row currentRow = iterator.next();
            String queueName = (String) currentRow.getAttribute("QueueName");
            logger.log(Level.INFO, getClass(), "getQueueNames", "QueueName= " + queueName);
            queueNameList.add(queueName);

        }
        String[] tempArray = new String[queueNameList.size()];
        queues = queueNameList.toArray(tempArray);
        return queues;
    }
}
