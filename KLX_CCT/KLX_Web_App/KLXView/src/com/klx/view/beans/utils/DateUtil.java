package com.klx.view.beans.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;

import com.klx.common.logger.KLXLogger;

import java.sql.Timestamp;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class DateUtil
{
    private static KLXLogger logger = KLXLogger.getLogger();
    static DateFormat dtf = new SimpleDateFormat("mm/dd/yy hh:mm:ss");
    static DateFormat df = new SimpleDateFormat("mm/dd/yy");

    public DateUtil()
    {
        super();
    }

    public static Date formatDate(Date date)
    {

        return new Date(df.format(date));
    }


    public static Date formatDateTime(Date date)
    {
        return new Date(dtf.format(date));
    }

    public static Date getCurrentDate() throws Exception
    {
        logger.log(Level.FINE, getClassName(), "In getCurrentDate()", "");
        Calendar cal = null;
        Date currDate = null;
        DateFormat formatter = null;
        String currentDate = null;
        Date formattedDate = null;
        try
        {
            cal = Calendar.getInstance();
            currDate = cal.getTime();
            formatter = new SimpleDateFormat("yyyy-MM-dd");
            currentDate = formatter.format(currDate);
            formattedDate = formatter.parse(currentDate);
        }
        catch (Exception ex)
        {
            logger.log(Level.SEVERE, getClassName(), "getCurrentDate()", "Error: " + ex);
            throw new Exception("Error: " + ex.getMessage());
        }
        logger.log(Level.FINE, getClassName(), "Leaving getCurrentDate()", "Return Date: " + formattedDate);
        return formattedDate;
    }

    public static XMLGregorianCalendar getXMLGregDate(Date date) throws Exception
    {
        logger.log(Level.FINE, getClassName(), "In getXMLGregDate()", "");
        XMLGregorianCalendar xmlGregDate = null;
        try
        {
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTime(date);
            xmlGregDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
        }
        catch (DatatypeConfigurationException ex)
        {
            logger.log(Level.SEVERE, getClassName(), "getXMLGregDate()", "Error: " + ex);
            throw new Exception("Error: " + ex.getMessage());
        }
        logger.log(Level.FINE, getClassName(), "In getXMLGregDate()", "xmlGregDate: " + xmlGregDate);
        return xmlGregDate;
    }

    private static String getClassName()
    {
        return "DateUtil";
    }
}
