package com.klx.view.beans.scope.request;

import com.klx.common.access.UserActionAccessManagement;
import com.klx.common.logger.KLXLogger;

import com.klx.view.beans.utils.ADFUtils;

import com.klx.view.beans.utils.JSFUtils;

import java.math.BigDecimal;

import java.util.ArrayList;

import java.util.logging.Level;

import javax.el.ELContext;

import javax.el.ExpressionFactory;

import javax.el.ValueExpression;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.share.ADFContext;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;
import javax.faces.model.SelectItem;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import com.klx.view.beans.constants.ViewConstants;

import java.util.HashMap;

import oracle.adf.model.binding.DCDataControl;

import oracle.jbo.server.ViewObjectImpl;
import com.klx.view.error.CustomErrorHandler;

import java.io.FileInputStream;
import java.io.OutputStream;

import java.text.NumberFormat;

import java.util.Currency;
import java.util.Iterator;
import java.util.Locale;

import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.jbo.ViewObject;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ProgramCost {

    private RichPopup bind_categoryName;

    public ProgramCost() {
    }
    
    private ArrayList<SelectItem> electronicsProgram = new ArrayList<SelectItem>();
    private ArrayList<SelectItem> implementationProgram = new ArrayList<SelectItem>();
    private ArrayList<SelectItem> facilityProgram = new ArrayList<SelectItem>();
    private ArrayList<SelectItem> servicesProgram = new ArrayList<SelectItem>();
    private static KLXLogger logger = KLXLogger.getLogger();    
    private Boolean saveHeaderButton = false;
    private Boolean cancelHeaderButton = false;
    private Boolean saveEHSButton = false;
    private Boolean cancelEHSButton = false;
    private Boolean saveImpButton = false;
    private Boolean cancelImpButton = false;
    private Boolean saveOthButton = false;
    private Boolean cancelOthButton = false;
    private Boolean saveFacButton = false;
    private Boolean cancelFacButton = false;
    private Boolean savePerButton = false;
    private Boolean cancelPerButton = false;
    private Boolean addEHSButton = false;
    private Boolean addImpButton = false;
    private Boolean addOthButton = false;
    private Boolean addFacButton = false;
    private Boolean addPerButton = false;
    private int progCostExcelRowCnt = 0;

        public void userActionsAccess() {
            
            logger.log(Level.INFO, getClass(), "Program Cost Bean  userActionsAccess method",
                       "save HeaderButton(Before) ----- " + saveHeaderButton);
           
            HashMap actions = new HashMap();
            actions.put(ViewConstants.SAVE_HEADER_BUTTON, false);
            actions.put(ViewConstants.CANCEL_HEADER_BUTTON, false);
            actions.put(ViewConstants.ADD_EHS_BUTTON, false);
            actions.put(ViewConstants.SAVE_EHS_BUTTON, false);
            actions.put(ViewConstants.CANCEL_EHS_BUTTON, false);
            actions.put(ViewConstants.ADD_IMP_BUTTON, false);
            actions.put(ViewConstants.SAVE_IMP_BUTTON, false);
            actions.put(ViewConstants.CANCEL_IMP_BUTTON, false);
            actions.put(ViewConstants.ADD_OTH_BUTTON, false);
            actions.put(ViewConstants.SAVE_OTH_BUTTON, false);
            actions.put(ViewConstants.CANCEL_OTH_BUTTON, false);
            actions.put(ViewConstants.ADD_FAC_BUTTON, false);
            actions.put(ViewConstants.SAVE_FAC_BUTTON, false);
            actions.put(ViewConstants.CANCEL_FAC_BUTTON, false);
            actions.put(ViewConstants.ADD_PER_BUTTON, false);
            actions.put(ViewConstants.SAVE_PER_BUTTON, false);
            actions.put(ViewConstants.CANCEL_PER_BUTTON, false);
            HashMap userAcess = UserActionAccessManagement.getUserAccess(ADFContext.getCurrent()
                                                                                   .getSecurityContext()
                                                                                   .getUserRoles(), actions);
            saveHeaderButton = ((Boolean) userAcess.get(ViewConstants.SAVE_HEADER_BUTTON)).booleanValue();
            cancelHeaderButton = ((Boolean) userAcess.get(ViewConstants.CANCEL_HEADER_BUTTON)).booleanValue();
            addEHSButton = ((Boolean) userAcess.get(ViewConstants.ADD_EHS_BUTTON)).booleanValue();
            saveEHSButton = ((Boolean) userAcess.get(ViewConstants.SAVE_EHS_BUTTON)).booleanValue();
            cancelEHSButton = ((Boolean) userAcess.get(ViewConstants.CANCEL_EHS_BUTTON)).booleanValue();
            addImpButton = ((Boolean) userAcess.get(ViewConstants.ADD_IMP_BUTTON)).booleanValue();
            saveImpButton = ((Boolean) userAcess.get(ViewConstants.SAVE_IMP_BUTTON)).booleanValue();
            cancelImpButton = ((Boolean) userAcess.get(ViewConstants.CANCEL_IMP_BUTTON)).booleanValue();
            addOthButton = ((Boolean) userAcess.get(ViewConstants.ADD_OTH_BUTTON)).booleanValue();
            saveOthButton = ((Boolean) userAcess.get(ViewConstants.SAVE_OTH_BUTTON)).booleanValue();
            cancelOthButton = ((Boolean) userAcess.get(ViewConstants.CANCEL_OTH_BUTTON)).booleanValue();
            addFacButton = ((Boolean) userAcess.get(ViewConstants.ADD_FAC_BUTTON)).booleanValue();
            saveFacButton = ((Boolean) userAcess.get(ViewConstants.SAVE_FAC_BUTTON)).booleanValue();
            cancelFacButton = ((Boolean) userAcess.get(ViewConstants.CANCEL_FAC_BUTTON)).booleanValue();
            addPerButton = ((Boolean) userAcess.get(ViewConstants.ADD_PER_BUTTON)).booleanValue();
            savePerButton = ((Boolean) userAcess.get(ViewConstants.SAVE_PER_BUTTON)).booleanValue();
            cancelPerButton = ((Boolean) userAcess.get(ViewConstants.CANCEL_PER_BUTTON)).booleanValue();
            logger.log(Level.INFO, getClass(), "Program Cost Bean  userActionsAccess method",
                       "save HeaderButton(After) ----- " + saveHeaderButton);
            
        }

    public void setSaveHeaderButton(Boolean saveHeaderButton) {
        this.saveHeaderButton = saveHeaderButton;
    }

    public Boolean getSaveHeaderButton() {
        return saveHeaderButton;
    }

    public void setCancelHeaderButton(Boolean cancelHeaderButton) {
        this.cancelHeaderButton = cancelHeaderButton;
    }

    public Boolean getCancelHeaderButton() {
        return cancelHeaderButton;
    }

    public void setSaveEHSButton(Boolean saveEHSButton) {
        this.saveEHSButton = saveEHSButton;
    }

    public Boolean getSaveEHSButton() {
        return saveEHSButton;
    }

    public void setCancelEHSButton(Boolean cancelEHSButton) {
        this.cancelEHSButton = cancelEHSButton;
    }

    public Boolean getCancelEHSButton() {
        return cancelEHSButton;
    }

    public void setSaveImpButton(Boolean saveImpButton) {
        this.saveImpButton = saveImpButton;
    }

    public Boolean getSaveImpButton() {
        return saveImpButton;
    }

    public void setCancelImpButton(Boolean cancelImpButton) {
        this.cancelImpButton = cancelImpButton;
    }

    public Boolean getCancelImpButton() {
        return cancelImpButton;
    }

   
    public void setSaveFacButton(Boolean saveFacButton) {
        this.saveFacButton = saveFacButton;
    }

    public Boolean getSaveFacButton() {
        return saveFacButton;
    }

    public void setCancelFacButton(Boolean cancelFacButton) {
        this.cancelFacButton = cancelFacButton;
    }

    public Boolean getCancelFacButton() {
        return cancelFacButton;
    }

    public void setSavePerButton(Boolean savePerButton) {
        this.savePerButton = savePerButton;
    }

    public Boolean getSavePerButton() {
        return savePerButton;
    }

    public void setCancelPerButton(Boolean cancelPerButton) {
        this.cancelPerButton = cancelPerButton;
    }

    public Boolean getCancelPerButton() {
        return cancelPerButton;
    }

    public void setAddEHSButton(Boolean addEHSButton) {
        this.addEHSButton = addEHSButton;
    }

    public Boolean getAddEHSButton() {
        return addEHSButton;
    }

    public void setAddImpButton(Boolean addImpButton) {
        this.addImpButton = addImpButton;
    }

    public Boolean getAddImpButton() {
        return addImpButton;
    }

    public void setAddFacButton(Boolean addFacButton) {
        this.addFacButton = addFacButton;
    }

    public Boolean getAddFacButton() {
        return addFacButton;
    }

    public void setAddPerButton(Boolean addPerButton) {
        this.addPerButton = addPerButton;
    }

    public Boolean getAddPerButton() {
        return addPerButton;
    }
    

    public void setElectronicsProgram(ArrayList<SelectItem> electronicsProgram) {
        this.electronicsProgram = electronicsProgram;
    }

    public ArrayList<SelectItem> getElectronicsProgram() {
        ArrayList<SelectItem> electronicsProgram = new ArrayList<SelectItem>();

        electronicsProgram.add(new SelectItem(ViewConstants.FIXED_PROGRAM,ViewConstants.FIXED_PROGRAM));
        electronicsProgram.add(new SelectItem(ViewConstants.RECURRING_PROGRAM,ViewConstants.RECURRING_PROGRAM));
        return electronicsProgram;
    }

    public void createNewCategory(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(),
                                            "ProgramCost",
                                            "Entering createNewCategory method");
        // Add event code here...       
        String programType = ADFContext.getCurrent().getPageFlowScope().get("programType").toString();        
        DCIteratorBinding dciter = null;
        BigDecimal categoryID = BigDecimal.ZERO;
        DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();      
        
        if(programType.equals(ViewConstants.ELECTRONICS)){
            ADFContext.getCurrent().getPageFlowScope().put(ViewConstants.ELECTRONICS_PROGRAM_TYPE, ViewConstants.ELECTRONICS);
            dciter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_LINES_EHS);
            categoryID = ViewConstants.categoryID[0];
        }
        else if(programType.equals(ViewConstants.FACILITIES)){
            ADFContext.getCurrent().getPageFlowScope().put(ViewConstants.FACILITY_PROGRAM_TYPE, ViewConstants.FACILITIES);
            dciter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_LINES_FCT);
            categoryID = ViewConstants.categoryID[3];
        }
        else if(programType.equals(ViewConstants.IMPLEMENTATION)){
            ADFContext.getCurrent().getPageFlowScope().put(ViewConstants.IMPLEMENTATION_PROGRAM_TYPE, ViewConstants.IMPLEMENTATION);
            dciter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_LINES_IMPS);
            categoryID = ViewConstants.categoryID[1];
        }
        else if(programType.equals(ViewConstants.OTHER_SERVICES)){
            ADFContext.getCurrent().getPageFlowScope().put(ViewConstants.OTHER_SERVICES_PROGRAM_TYPE, ViewConstants.OTHER_SERVICES);
            dciter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_LINES_SERVICE);
            categoryID = ViewConstants.categoryID[2];
        }
        else{ 
            ADFContext.getCurrent().getPageFlowScope().put(ViewConstants.PERSONNEL_LINES_PROGRAM_TYPE, ViewConstants.PERSONNEL_LINES);
            dciter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_LINES_PERSONNEL);
            categoryID = ViewConstants.categoryID[4];
        }
        if(dciter != null){
            logger.log(Level.INFO, getClass(),
                                                "ProgramCost",
                                                "VO is not null");
        }
        else{
            logger.log(Level.INFO, getClass(),
                                                "ProgramCost",
                                                "VO is null");
        }

        RowSetIterator rsi = dciter.getRowSetIterator();        
        Row lastRow = rsi.last();       
        int lastRowIndex = rsi.getRangeIndexOf(lastRow);        
        Row newRow = rsi.createRow();
        //initialize the row
        newRow.setNewRowState(Row.STATUS_INITIALIZED);        
        //add row to last index + 1 so it becomes last in the range set
        rsi.insertRowAtRangeIndex(lastRowIndex +1, newRow); 
        //rsi.insertRow(newRow);
        newRow.setAttribute(ViewConstants.PROGRAM_COST_HEADER, ADFContext.getCurrent().getSessionScope().get("programHeaderId"));        
        //newRow.setAttribute("ProgramCostLineId", 48);
        newRow.setAttribute(ViewConstants.PROGRAM_CATEGORY, categoryID);
        //make row the current row so it is displayed correctly
        rsi.setCurrentRow(newRow);        
        rsi.closeRowSetIterator();
        logger.log(Level.INFO, getClass(),
                                            "ProgramCost",
                                            "Exiting createNewCategory method");
    }

    public void programTypeListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        setValueToEL("#{row.bindings.FixedOrRecurring.inputValue}", valueChangeEvent.getNewValue().toString());
    }
    
    
    public void setValueToEL(String el, Object val) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ELContext elContext = facesContext.getELContext();
            ExpressionFactory expressionFactory = facesContext.getApplication().getExpressionFactory();
            ValueExpression valueExp = expressionFactory.createValueExpression(elContext, el,
                                                                               Object.class);
            valueExp.setValue(elContext, val);
        }

    public void saveAllPrograms(ActionEvent actionEvent) {
        // Add event code here...
        logger.log(Level.INFO, getClass(),
                                            "saveAllPrograms",
                                            "Entering saveAllPrograms method");
        try{
        BigDecimal opportunityId = (BigDecimal) JSFUtils.resolveExpression("#{bindings.OpportunityId.inputValue}");
        String totalProgCost = JSFUtils.resolveExpression("#{bindings.TotalProgramCosts.inputValue}").toString();
        totalProgCost = totalProgCost.replace("$", "").replace(",", "").trim();       
        BigDecimal totalProgramCost = new BigDecimal(totalProgCost);
        //BigDecimal totalProgramCost = (BigDecimal) JSFUtils.resolveExpression("#{bindings.TotalProgramCosts.inputValue}");
        
       // OperationBinding oBinding = (OperationBinding)BindingContext.getCurrent().getCurrentBindingsEntry().getOperationBinding("Commit");
        //oBinding.execute();
        
        String EhsNotes=(String)JSFUtils.resolveExpression("#{bindings.Notes2.inputValue}");
        String ImpNotes=(String)JSFUtils.resolveExpression("#{bindings.Notes3.inputValue}");
        String OthNotes=(String)JSFUtils.resolveExpression("#{bindings.Notes4.inputValue}");
        String FacNotes=(String)JSFUtils.resolveExpression("#{bindings.Notes5.inputValue}");
        String PerNotes=(String)JSFUtils.resolveExpression("#{bindings.Notes1.inputValue}");
        oracle.adf.model.OperationBinding operationBinding = (oracle.adf.model.OperationBinding) ADFUtils.findOperationBinding("setValueToNotesSumm");
        operationBinding.getParamsMap().put("costHeaderID",ADFContext.getCurrent().getSessionScope().get("programHeaderId"));
        operationBinding.getParamsMap().put("EhsNotes", EhsNotes);
        operationBinding.getParamsMap().put("ImpNotes", ImpNotes);  
        operationBinding.getParamsMap().put("OthNotes", OthNotes);  
        operationBinding.getParamsMap().put("FacNotes", FacNotes);  
        operationBinding.getParamsMap().put("PerNotes", PerNotes);               
        operationBinding.getParamsMap().put("opportunityID", opportunityId);  
        operationBinding.getParamsMap().put("totalProgramCosts", totalProgramCost);  
        String result = operationBinding.execute().toString();
        Boolean operationResult = new Boolean(result);
       
        
        String programType = ADFContext.getCurrent().getPageFlowScope().get(ViewConstants.PROGRAM_TYPE).toString();
        DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        
        DCIteratorBinding implementationDCiter = null, electronicsDCiter = null, servicesDCiter = null, 
                            faclityDCiter = null, personnelDCiter = null,headerDCiter=null;
        
        headerDCiter= (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_COST_HEADER_ITER);
        headerDCiter.executeQuery();        
        
        implementationDCiter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_LINES_IMPS);
                        
        electronicsDCiter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_LINES_EHS);        
               
        faclityDCiter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_LINES_FCT);                
        
        servicesDCiter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_LINES_SERVICE);        
        
        //servicesDCiter.setRefreshed(true);
        personnelDCiter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_LINES_PERSONNEL);                
        //personnelDCiter.setRefreshed(true);
        OperationBinding submitOSC = ADFUtils.findOperationBinding("submitOSC");
        submitOSC.getParamsMap().put("oppID", opportunityId);
        String status = (String) submitOSC.execute();  
        ViewObject impl = implementationDCiter.getViewObject();
        ViewObject electronicsVO = electronicsDCiter.getViewObject();
        ViewObject facilitiesVO = faclityDCiter.getViewObject();
        ViewObject othServicesVO = servicesDCiter.getViewObject();
        ViewObject personnelVO = personnelDCiter.getViewObject();

        RowSetIterator programCostLines = null;
        if(operationResult){
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            getBind_categoryName().show(hints);
            if(programType.equals(ViewConstants.ELECTRONICS)){
                programCostLines = electronicsDCiter.getRowSetIterator();        
                Row lastRow = programCostLines.last(); 
                lastRow.remove();
                programCostLines.closeRowSetIterator();                    
                electronicsVO.clearCache();   
            }
            else if(programType.equals(ViewConstants.IMPLEMENTATION)){
                programCostLines = implementationDCiter.getRowSetIterator();        
                Row lastRow = programCostLines.last(); 
                lastRow.remove();
                programCostLines.closeRowSetIterator();                    
                impl.clearCache();    
            }
            else if(programType.equals(ViewConstants.FACILITIES)){
                programCostLines = faclityDCiter.getRowSetIterator();        
                Row lastRow = programCostLines.last(); 
                lastRow.remove();
                programCostLines.closeRowSetIterator();                    
                facilitiesVO.clearCache();    
            } 
            else if(programType.equals(ViewConstants.OTHER_SERVICES)){
                programCostLines = servicesDCiter.getRowSetIterator();        
                Row lastRow = programCostLines.last(); 
                lastRow.remove();
                programCostLines.closeRowSetIterator();                    
                othServicesVO.clearCache();    
            }
            else{
                programCostLines = personnelDCiter.getRowSetIterator();        
                Row lastRow = programCostLines.last(); 
                lastRow.remove();
                programCostLines.closeRowSetIterator();                    
                personnelVO.clearCache();
            }
        }
        electronicsVO.executeQuery();
        impl.executeQuery();
        facilitiesVO.executeQuery();
            othServicesVO.executeQuery();
            personnelVO.executeQuery();
        ADFContext.getCurrent().getPageFlowScope().put(ViewConstants.ELECTRONICS_PROGRAM_TYPE, null);
        ADFContext.getCurrent().getPageFlowScope().put(ViewConstants.IMPLEMENTATION_PROGRAM_TYPE, null);
        ADFContext.getCurrent().getPageFlowScope().put(ViewConstants.OTHER_SERVICES_PROGRAM_TYPE, null);
        ADFContext.getCurrent().getPageFlowScope().put(ViewConstants.FACILITY_PROGRAM_TYPE, null);
        ADFContext.getCurrent().getPageFlowScope().put(ViewConstants.PERSONNEL_LINES_PROGRAM_TYPE, null);    
        ADFContext.getCurrent().getPageFlowScope().put(ViewConstants.PROGRAM_TYPE, null);
        }catch(Exception ae){
            ae.printStackTrace();            
        }
    
        logger.log(Level.INFO, getClass(),
                                            "saveAllPrograms",
                                            "Exiting saveAllPrograms method");
    }

    public void setImplementationProgram(ArrayList<SelectItem> implementationProgram) {
        this.implementationProgram = implementationProgram;
    }

    public ArrayList<SelectItem> getImplementationProgram() {
        ArrayList<SelectItem> implementationProgram = new ArrayList<SelectItem>();

        implementationProgram.add(new SelectItem(ViewConstants.FIXED_PROGRAM,ViewConstants.FIXED_PROGRAM));
        implementationProgram.add(new SelectItem(ViewConstants.RECURRING_PROGRAM,ViewConstants.RECURRING_PROGRAM));
        return implementationProgram;
    }

    public void setFacilityProgram(ArrayList<SelectItem> facilityProgram) {
        this.facilityProgram = facilityProgram;
    }

    public ArrayList<SelectItem> getFacilityProgram() {
        ArrayList<SelectItem> facilityProgram = new ArrayList<SelectItem>();

        facilityProgram.add(new SelectItem(ViewConstants.FIXED_PROGRAM,ViewConstants.FIXED_PROGRAM));
        facilityProgram.add(new SelectItem(ViewConstants.RECURRING_PROGRAM,ViewConstants.RECURRING_PROGRAM));
        return facilityProgram;
    }

    public void setServicesProgram(ArrayList<SelectItem> servicesProgram) {
        this.servicesProgram = servicesProgram;
    }

    public ArrayList<SelectItem> getServicesProgram() {
        ArrayList<SelectItem> servicesProgram = new ArrayList<SelectItem>();

        servicesProgram.add(new SelectItem(ViewConstants.FIXED_PROGRAM,ViewConstants.FIXED_PROGRAM));
        servicesProgram.add(new SelectItem(ViewConstants.RECURRING_PROGRAM,ViewConstants.RECURRING_PROGRAM));
        return servicesProgram;
    }

    public void cancelAllPrograms(ActionEvent actionEvent) {
        // Add event code here...
        logger.log(Level.INFO, getClass(),
                                            "canceleAllPrograms",
                                            "Entering canceleAllPrograms method");
        //BigDecimal opportunityId = (BigDecimal) JSFUtils.resolveExpression("#{bindings.OpportunityId.inputValue}");
        OperationBinding oBinding = (OperationBinding)BindingContext.getCurrent().getCurrentBindingsEntry().getOperationBinding("Rollback");
        oBinding.execute();
        ADFContext.getCurrent().getPageFlowScope().put("programType", null);
//        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
//        DCIteratorBinding itorBinding = bindings.findIteratorBinding("ProgramLinesEHSIterator");
//        ViewObjectImpl voi = (ViewObjectImpl) itorBinding.getViewObject();
//        voi.getDBTransaction().rollback();
        
        //------------------------  
             
//        BindingContext bindingContext = BindingContext.getCurrent();
//        DCDataControl dc = bindingContext.findDataControl("KLXAppModuleDataControl");
//        KLXAppModuleImpl am = (KLXAppModuleImpl)dc.getDataProvider();
//        BigDecimal opportunityId = (BigDecimal) JSFUtils.resolveExpression("#{bindings.OpportunityId.inputValue}");
//        am.programHeaderCostData(""+opportunityId);
        //--------------------
//        itorBinding.getCurrentRow().removeFromCollection();
        DCIteratorBinding implementationDCiter = null, electronicsDCiter = null, servicesDCiter = null, 
                            faclityDCiter = null, personnelDCiter = null;
        DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        implementationDCiter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_LINES_IMPS);
        implementationDCiter.executeQuery();
        implementationDCiter.setRefreshed(true);
        electronicsDCiter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_LINES_EHS);
        electronicsDCiter.executeQuery();
        electronicsDCiter.setRefreshed(true);
        faclityDCiter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_LINES_FCT);
        faclityDCiter.executeQuery();
        faclityDCiter.setRefreshed(true);
        servicesDCiter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_LINES_SERVICE);
        servicesDCiter.executeQuery();
        servicesDCiter.setRefreshed(true);
        personnelDCiter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_LINES_PERSONNEL);
        personnelDCiter.executeQuery();
        personnelDCiter.setRefreshed(true);
        ADFContext.getCurrent().getPageFlowScope().put("programType", null);

        logger.log(Level.INFO, getClass(),
                                            "cancelAllPrograms",
                                            "Exiting canceleAllPrograms method");
        
    }
	    public void confirmCancelAllChange(DialogEvent dialogEvent) {
        // Add event code here...
        logger.log(Level.INFO, getClass(),
                                            "confirmCancelAllChange",
                                            "Entering confirmCancelAllChange method");
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok)
            {
             // write your custom code for ok event
             OperationBinding oBinding = (OperationBinding)BindingContext.getCurrent().getCurrentBindingsEntry().getOperationBinding("Rollback");
             oBinding.execute();
             ADFContext.getCurrent().getPageFlowScope().put("programType", null);
            DCIteratorBinding implementationDCiter = null, electronicsDCiter = null, servicesDCiter = null, 
            headerDCiter = null, faclityDCiter = null, personnelDCiter = null;
            DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            headerDCiter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_COST_HEADER_ITER);
            headerDCiter.executeQuery();
            headerDCiter.setRefreshed(true);
            implementationDCiter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_LINES_IMPS);
            implementationDCiter.executeQuery();
            implementationDCiter.setRefreshed(true);
            electronicsDCiter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_LINES_EHS);
            electronicsDCiter.executeQuery();
            electronicsDCiter.setRefreshed(true);
            faclityDCiter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_LINES_FCT);
            faclityDCiter.executeQuery();
            faclityDCiter.setRefreshed(true);
            servicesDCiter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_LINES_SERVICE);
            servicesDCiter.executeQuery();
            servicesDCiter.setRefreshed(true);
            personnelDCiter = (DCIteratorBinding) binds.get(ViewConstants.PROGRAM_LINES_PERSONNEL);
            personnelDCiter.executeQuery();
            personnelDCiter.setRefreshed(true);
            ADFContext.getCurrent().getPageFlowScope().put("programType", null);

           
            } 
        else
            {
              // write your custom code for cancel event
             
       
           } 
        logger.log(Level.INFO, getClass(),
                                            "confirmCancelAllChange",
                                            "Exiting confirmCancelAllChange method");
    }

    public void setAddOthButton(Boolean addOthButton) {
        this.addOthButton = addOthButton;
    }

    public Boolean getAddOthButton() {
        return addOthButton;
    }

    public void setSaveOthButton(Boolean saveOthButton) {
        this.saveOthButton = saveOthButton;
    }

    public Boolean getSaveOthButton() {
        return saveOthButton;
    }

    public void setCancelOthButton(Boolean cancelOthButton) {
        this.cancelOthButton = cancelOthButton;
    }

    public Boolean getCancelOthButton() {
        return cancelOthButton;
    }    
    
    public void downloadProgramCostData(FacesContext facesContext, OutputStream outputStream) 
    {
        logger.log(Level.FINE, getClass(), "In downloadProgramCostData()", "");
        String progCostTemplate = null;
        String templatePath = null;
        FileInputStream fis = null;
        XSSFWorkbook workbook = null;
        XSSFSheet worksheet = null;
        
        try
        {
            progCostTemplate = ViewConstants.PROG_COST_DOWNLOAD_TEMPLATE;
            templatePath = ViewConstants.FILE_PATH + progCostTemplate;
            fis = (FileInputStream) facesContext.getExternalContext().getResourceAsStream(templatePath);
            workbook = new XSSFWorkbook(fis);
            worksheet = workbook.getSheetAt(0);
            //Populate top section on program costs
            populateHeaderData(worksheet);
            //Populate other sections data of program costs.
            progCostExcelRowCnt = 9;// Row num 10 in the template
            
            populateSubSectionsData(workbook, worksheet, ViewConstants.PROG_COST_LINES_EHS_ITER);
            incrementRowCnt();
            //Populate EHS notes, recurring & fixed sub totals.
            populateFixedSubTotals(workbook, worksheet, ViewConstants.PROG_COST_LINES_EHS_ITER, "FixedSubTotalEHS");            
            incrementRowCnt();
            populateRecSubTotals(workbook, worksheet, ViewConstants.PROG_COST_LINES_EHS_ITER, "RecurringSubTotalEHS");
            incrementRowCnt();
            String ehsNotes = (String)JSFUtils.resolveExpression("#{bindings.Notes2.inputValue}");
            populateNotes(workbook, worksheet, ViewConstants.PROG_COST_EHS_NOTES_ITER, ehsNotes);
            incrementRowCnt();
        
            populateSubSectionsData(workbook, worksheet, ViewConstants.PROG_COST_LINES_IMPS_ITER);
            incrementRowCnt();
            //Populate IMPS notes, recurring & fixed sub totals.
            populateFixedSubTotals(workbook, worksheet, ViewConstants.PROG_COST_LINES_EHS_ITER, "FixedSubTotalImpl");            
            incrementRowCnt();            
            populateRecSubTotals(workbook, worksheet, ViewConstants.PROG_COST_LINES_EHS_ITER, "RecurringSubTotalImpl");
            incrementRowCnt();
            String impsNotes = (String)JSFUtils.resolveExpression("#{bindings.Notes3.inputValue}");
            populateNotes(workbook, worksheet, ViewConstants.PROG_COST_IMPS_NOTES_ITER, impsNotes);
            incrementRowCnt();

            populateSubSectionsData(workbook, worksheet, ViewConstants.PROG_COST_LINES_OTHER_SER_ITER);
            incrementRowCnt();
            //Populate Others notes, recurring & fixed sub totals.
            populateFixedSubTotals(workbook, worksheet, ViewConstants.PROG_COST_LINES_EHS_ITER, "FixedSubTotalOther");            
            incrementRowCnt();  
            populateRecSubTotals(workbook, worksheet, ViewConstants.PROG_COST_LINES_EHS_ITER, "RecurringSubTotalOther");
            incrementRowCnt();
            String othNotes = (String)JSFUtils.resolveExpression("#{bindings.Notes4.inputValue}");
            populateNotes(workbook, worksheet, ViewConstants.PROG_COST_OTH_NOTES_ITER, othNotes);
            incrementRowCnt();
            
            populateSubSectionsData(workbook, worksheet, ViewConstants.PROG_COST_LINES_FCT_ITER);
            incrementRowCnt();
            //Populate Fac notes, recurring & fixed sub totals.
            populateFixedSubTotals(workbook, worksheet, ViewConstants.PROG_COST_LINES_EHS_ITER, "FixedSubTotalFct");            
            incrementRowCnt();     
            populateRecSubTotals(workbook, worksheet, ViewConstants.PROG_COST_LINES_EHS_ITER, "RecurringSubTotalFct");
            incrementRowCnt();
            String facNotes = (String)JSFUtils.resolveExpression("#{bindings.Notes5.inputValue}");
            populateNotes(workbook, worksheet, ViewConstants.PROG_COST_FAC_NOTES_ITER, facNotes);
            incrementRowCnt();
            
            //populateSubSectionsData(workbook, worksheet, ViewConstants.PROG_COST_LINES_PER_ITER);
            populatePersonnelData(workbook, worksheet, ViewConstants.PROG_COST_LINES_PER_ITER);
            //Populate Personnel notes, recurring & fixed sub totals.
            incrementRowCnt();
            String perNotes = (String)JSFUtils.resolveExpression("#{bindings.Notes1.inputValue}");
            populateNotes(workbook, worksheet, ViewConstants.PROG_COST_PER_NOTES_ITER, perNotes);
            incrementRowCnt();
            populateFixedSubTotals(workbook, worksheet, ViewConstants.PROGRAM_LINES_PERSONNEL, "FixedSubTotalPer");
            
            workbook.write(outputStream);
            outputStream.flush();
            outputStream.close();
        }
        catch(Exception ex)
        {
            logger.log(Level.SEVERE, getClass(), "downloadProgramCostData()", "Error while downloading program cost data. Error: " + ex);
            CustomErrorHandler.processError("CCT", "downloadProgramCostData()", "CCT_SYS_EX90", ex);
        }
        logger.log(Level.FINE, getClass(), "Leaving downloadProgramCostData()", "");
    }
    
    private void populateRecSubTotals(XSSFWorkbook workbook, XSSFSheet worksheet, String itername, String recSubTotAttrName)
    {
        DCBindingContainer bindingContainer = ADFUtils.getDCBindingContainer();
        DCIteratorBinding iter = bindingContainer.findIteratorBinding(itername);
        Row currrow = iter.getViewObject().getCurrentRow();
        try
        {
            XSSFRow row = worksheet.createRow(progCostExcelRowCnt);   

            XSSFCell cell0 = row.createCell(0);
            cell0.setCellStyle(createNotesHeaderCellStyle(workbook));
            cell0.setCellValue(ViewConstants.PROG_COST_CAT_REC_SUB_TOT_HDR);     
            
            XSSFCell cell1 = row.createCell(1);
            cell1.setCellStyle(createNotesHeaderCellStyle(workbook));
            cell1.setCellValue("");     

            XSSFCell cell2 = row.createCell(2);
            cell2.setCellStyle(createNotesHeaderCellStyle(workbook));
            cell2.setCellValue("");     
            
            XSSFCell cell3 = row.createCell(3);
            cell3.setCellStyle(createSubTotalCellStyle(workbook));
            cell3.setCellValue("");
                        
            XSSFCell cell4 = row.createCell(4);
            cell4.setCellStyle(createSubTotalCellStyle(workbook));
            cell4.setCellValue("");
            
            XSSFCell cell5 = row.createCell(5);
            cell5.setCellStyle(createSubTotalCellStyle(workbook));
            if(currrow.getAttribute(recSubTotAttrName) != null)
            {
                cell5.setCellValue(formatNumber((BigDecimal)currrow.getAttribute(recSubTotAttrName)));     
            }   
            worksheet.addMergedRegionUnsafe(new CellRangeAddress(progCostExcelRowCnt, progCostExcelRowCnt, 0, 1));
        }
        catch(Exception ex)
        {
            
        }
    }
    
    private void populateFixedSubTotals(XSSFWorkbook workbook, XSSFSheet worksheet, String itername, String fixedSubTotAttrName)
    {
        DCBindingContainer bindingContainer = ADFUtils.getDCBindingContainer();
        DCIteratorBinding iter = bindingContainer.findIteratorBinding(itername);
        Row currrow = iter.getViewObject().getCurrentRow();
        try
        {
            XSSFRow row = worksheet.createRow(progCostExcelRowCnt);   

            XSSFCell cell0 = row.createCell(0);
            cell0.setCellStyle(createNotesHeaderCellStyle(workbook));
            cell0.setCellValue(ViewConstants.PROG_COST_CAT_FIX_SUB_TOT_HDR);
            if(itername.equalsIgnoreCase(ViewConstants.PROGRAM_LINES_PERSONNEL))
            {
                cell0.setCellValue(ViewConstants.PROG_COST_TOT_PER_EXP_HDR);
            }
            
            XSSFCell cell1 = row.createCell(1);
            cell1.setCellStyle(createNotesHeaderCellStyle(workbook));
            cell1.setCellValue("");     

            XSSFCell cell2 = row.createCell(2);
            cell2.setCellStyle(createNotesHeaderCellStyle(workbook));
            cell2.setCellValue("");     
            
            XSSFCell cell3 = row.createCell(3);
            cell3.setCellStyle(createSubTotalCellStyle(workbook));
            cell3.setCellValue("");
                        
            XSSFCell cell4 = row.createCell(4);
            cell4.setCellStyle(createSubTotalCellStyle(workbook));
            cell4.setCellValue("");
            
            XSSFCell cell5 = row.createCell(5);
            cell5.setCellStyle(createSubTotalCellStyle(workbook));
            if(currrow.getAttribute(fixedSubTotAttrName) != null)
            {
                cell5.setCellValue(formatNumber((BigDecimal)currrow.getAttribute(fixedSubTotAttrName)));     
            } 
            worksheet.addMergedRegionUnsafe(new CellRangeAddress(progCostExcelRowCnt, progCostExcelRowCnt, 0, 1));
        }
        catch(Exception ex)
        {
            
        }
    }
    
    private void populateNotes(XSSFWorkbook workbook, XSSFSheet worksheet, String itername, String notes)
    {
        try
        {
            XSSFRow row = worksheet.createRow(progCostExcelRowCnt);   

            XSSFCell cell0 = row.createCell(0);
            cell0.setCellStyle(createNotesHeaderCellStyle(workbook));
            cell0.setCellValue(ViewConstants.PROG_COST_NOTES_HDR);   
            if(itername.equalsIgnoreCase(ViewConstants.PROG_COST_PER_NOTES_ITER))
            {
                cell0.setCellValue(ViewConstants.PROG_COST_PER_NOTES_HDR);   
            }

            XSSFCell cell1 = row.createCell(1);
            cell1.setCellStyle(createNotesHeaderCellStyle(workbook));
            cell1.setCellValue(ViewConstants.EMPTY_STRING);     

            XSSFCell cell2 = row.createCell(2);
            cell2.setCellStyle(createNotesHeaderCellStyle(workbook));
            cell2.setCellValue(ViewConstants.EMPTY_STRING);     
            
            XSSFCell cell3 = row.createCell(3);
            cell3.setCellStyle(createNotesCellStyle(workbook));
            cell3.setCellValue(notes);    
                        
            XSSFCell cell4 = row.createCell(4);
            cell4.setCellStyle(createNotesCellStyle(workbook));
            cell4.setCellValue(ViewConstants.EMPTY_STRING);
            
            XSSFCell cell5 = row.createCell(5);
            cell5.setCellStyle(createNotesCellStyle(workbook));
            cell5.setCellValue(ViewConstants.EMPTY_STRING);     
            
            row.setHeight((short)1000);
            worksheet.addMergedRegionUnsafe(new CellRangeAddress(progCostExcelRowCnt, progCostExcelRowCnt, 0, 1));
            worksheet.addMergedRegionUnsafe(new CellRangeAddress(progCostExcelRowCnt, progCostExcelRowCnt, 3, 5));
        }
        catch(Exception ex)
        {
               
        }
    }
    
    private void populateSubSectionsData(XSSFWorkbook workbook, XSSFSheet worksheet, String iter)
    {
        DCBindingContainer bindingContainer = ADFUtils.getDCBindingContainer();
        DCIteratorBinding iterBind = bindingContainer.findIteratorBinding(iter);
        ViewObject vo = iterBind.getViewObject();
        RowSetIterator rows = vo.createRowSetIterator(null);
        
        try
        {
            //Create header before populating data.
            createHeaders(workbook, worksheet, iter);
               
            while(rows.hasNext()) 
            {
                Row nextRow = rows.next();        
                progCostExcelRowCnt++;
                XSSFRow row = worksheet.createRow(progCostExcelRowCnt);   

                XSSFCell cell0 = row.createCell(0);
                cell0.setCellStyle(createCategoryCellStyle(workbook));
                if(nextRow.getAttribute("Category") != null)
                {
                    cell0.setCellValue(nextRow.getAttribute("Category").toString());     
                }

                XSSFCell cell1 = row.createCell(1);
                cell1.setCellStyle(createCategoryCellStyle(workbook));
                cell1.setCellValue("");     

                XSSFCell cell2 = row.createCell(2);
                cell2.setCellStyle(createCategoryCellStyle(workbook));
                if(nextRow.getAttribute("FixedOrRecurring") != null)
                {
                    cell2.setCellValue(nextRow.getAttribute("FixedOrRecurring").toString());     
                }
                
                XSSFCell cell3 = row.createCell(3);
                cell3.setCellStyle(createUnitCostCellStyle(workbook));
                if(nextRow.getAttribute("UnitCost") != null)
                {
                    cell3.setCellValue(formatNumber((BigDecimal)nextRow.getAttribute("UnitCost")));     
                }
                
                XSSFCell cell4 = row.createCell(4);
                cell4.setCellStyle(createUnitCostCellStyle(workbook));
                if(nextRow.getAttribute("Quantity") != null)
                {
                    cell4.setCellValue(nextRow.getAttribute("Quantity").toString());     
                }
                
                XSSFCell cell5 = row.createCell(5);
                cell5.setCellStyle(createSubTotalCellStyle(workbook));
                if(nextRow.getAttribute("Subtotal") != null)
                {
                    cell5.setCellValue(formatNumber((BigDecimal)nextRow.getAttribute("Subtotal")));     
                }
                worksheet.addMergedRegionUnsafe(new CellRangeAddress(progCostExcelRowCnt, progCostExcelRowCnt, 0, 1));
            }
        }
        catch(Exception ex)
        {
            logger.log(Level.SEVERE, getClass(), "populateSubSectionsData", ex);   
        }
    }
    
    private void populatePersonnelData(XSSFWorkbook workbook, XSSFSheet worksheet, String iter)
    {
        DCBindingContainer bindingContainer = ADFUtils.getDCBindingContainer();
        DCIteratorBinding iterBind = bindingContainer.findIteratorBinding(iter);
        ViewObject vo = iterBind.getViewObject();
        RowSetIterator rows = vo.createRowSetIterator(null);
        
        try
        {
            //Create header before populating data.
            createPersonnelHeader(workbook, worksheet, iter);
               
            while(rows.hasNext()) 
            {
                Row nextRow = rows.next();        
                progCostExcelRowCnt++;
                XSSFRow row = worksheet.createRow(progCostExcelRowCnt);   

                XSSFCell cell0 = row.createCell(0);
                cell0.setCellStyle(createCategoryCellStyle(workbook));
                if(nextRow.getAttribute("Category") != null)
                {
                    cell0.setCellValue(nextRow.getAttribute("Category").toString());     
                }

                XSSFCell cell1 = row.createCell(1);
                cell1.setCellStyle(createCategoryCellStyle(workbook));
                cell1.setCellValue("");     

                XSSFCell cell2 = row.createCell(2);
                cell2.setCellStyle(createCategoryCellStyle(workbook));
                if(nextRow.getAttribute("NoOfIndividuals") != null)
                {
                    //cell2.setCellValue(formatNumber((BigDecimal)nextRow.getAttribute("NoOfIndividuals")));     
                    cell2.setCellValue(nextRow.getAttribute("NoOfIndividuals").toString());     
                }
                
                XSSFCell cell3 = row.createCell(3);
                cell3.setCellStyle(createUnitCostCellStyle(workbook));
                if(nextRow.getAttribute("PctOfWorkload") != null)
                {
                    cell3.setCellValue(nextRow.getAttribute("PctOfWorkload").toString());     
                }
                
                XSSFCell cell4 = row.createCell(4);
                cell4.setCellStyle(createUnitCostCellStyle(workbook));
                if(nextRow.getAttribute("AnnualPersonnelCost") != null)
                {
                    cell4.setCellValue(formatNumber((BigDecimal)nextRow.getAttribute("AnnualPersonnelCost")));     
                }
                
                XSSFCell cell5 = row.createCell(5);
                cell5.setCellStyle(createSubTotalCellStyle(workbook));
                if(nextRow.getAttribute("Subtotal") != null)
                {
                    cell5.setCellValue(formatNumber((BigDecimal)nextRow.getAttribute("Subtotal")));     
                }
                worksheet.addMergedRegionUnsafe(new CellRangeAddress(progCostExcelRowCnt, progCostExcelRowCnt, 0, 1));
            }
        }
        catch(Exception ex)
        {
            logger.log(Level.SEVERE, getClass(), "populatePersonnelData", ex);   
        }
    }
    
    private XSSFCellStyle createCategoryCellStyle(XSSFWorkbook workbook) 
    {
        XSSFCellStyle style = workbook.createCellStyle();
        style = setCommonBorder(style);
        return style;
    }
    
    private XSSFCellStyle createNotesHeaderCellStyle(XSSFWorkbook workbook) 
    {
        XSSFCellStyle style = workbook.createCellStyle();
        style = setCommonBorder(style);
        style.setFont(createHeaderFontStyle(workbook));
        return style;
    }
    
    private XSSFCellStyle createUnitCostCellStyle(XSSFWorkbook workbook) 
    {
        XSSFCellStyle style = workbook.createCellStyle();
        style = setCommonBorder(style);
        style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());    
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        return style;
    }
    
    private XSSFCellStyle createNotesCellStyle(XSSFWorkbook workbook) 
    {
        XSSFCellStyle style = workbook.createCellStyle();
        style = setCommonBorder(style);
        style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());    
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setWrapText(true);
        return style;
    }
    
    private XSSFCellStyle createSubTotalCellStyle(XSSFWorkbook workbook) 
    {
        XSSFCellStyle style = workbook.createCellStyle();
        style = setCommonBorder(style);
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());    
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        return style;
    }
    
    private XSSFCellStyle createHeaderCellStyle(XSSFWorkbook workbook, boolean grey) 
    {
        XSSFCellStyle style = workbook.createCellStyle();
        style = setCommonBorder(style);
        if(grey)
        {
            style.setFont(createFontStyle(workbook));
            style.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());    
        }
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        return style;
    }
    
    private XSSFCellStyle setCommonBorder(XSSFCellStyle style)
    {
        style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        style.setBorderTop(XSSFCellStyle.BORDER_THIN);
        style.setBorderRight(XSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        return style;
    }
    
    private void populateHeaderData(XSSFSheet worksheet)
    {
        XSSFRow row = null;
        int rowNum = 4;//This is the 5th row number in the template.
        Iterator iterator = null;
        Cell cell = null;
        
        for(int i = 1; i <= rowNum; i++)
        {
            row = worksheet.getRow(i);
            iterator = row.cellIterator();
            while(iterator.hasNext())
            {
                cell = (Cell)iterator.next();
                if(cell != null && !"".equals(cell))
                {
                    if(cell.getStringCellValue().equalsIgnoreCase(ViewConstants.PROG_COST_TEMPLATE_HEADERS[0]))
                    {
                        String location = String.valueOf(JSFUtils.resolveExpression("#{bindings.Locations.inputValue}"));
                        if(location != null)
                        {
                            row.getCell(1).setCellValue(location);
                        }
                    }
                    if(cell.getStringCellValue().equalsIgnoreCase(ViewConstants.PROG_COST_TEMPLATE_HEADERS[1]))
                    {
                        BigDecimal oneTimePgmSetupCostTR = (BigDecimal)JSFUtils.resolveExpression("#{bindings.OneTimePgmSetupCostTR.inputValue}");
                        if(oneTimePgmSetupCostTR != null)
                        {
                            row.getCell(3).setCellValue(formatNumber(oneTimePgmSetupCostTR));    
                        }
                    }
                    if(cell.getStringCellValue().equalsIgnoreCase(ViewConstants.PROG_COST_TEMPLATE_HEADERS[2]))
                    {
                        BigDecimal totalProgramCostsTR = (BigDecimal)JSFUtils.resolveExpression("#{bindings.TotalProgramCostsTR.inputValue}");
                        if(totalProgramCostsTR != null)
                        {
                            row.getCell(5).setCellValue(formatNumber(totalProgramCostsTR));    
                        }
                    }
                    if(cell.getStringCellValue().equalsIgnoreCase(ViewConstants.PROG_COST_TEMPLATE_HEADERS[3]))
                    {
                        String dirMgr = String.valueOf(JSFUtils.resolveExpression("#{bindings.DirectorOrManager.inputValue}"));
                        if(dirMgr != null)
                        {
                            row.getCell(1).setCellValue(dirMgr);
                        }
                    }
                    if(cell.getStringCellValue().equalsIgnoreCase(ViewConstants.PROG_COST_TEMPLATE_HEADERS[4]))
                    {
                        BigDecimal annualRecPgmCostsTR = (BigDecimal)JSFUtils.resolveExpression("#{bindings.AnnualRecPgmCostsTR.inputValue}");
                        if(annualRecPgmCostsTR != null)
                        {
                            row.getCell(3).setCellValue(formatNumber(annualRecPgmCostsTR));    
                        }
                    }
                    if(cell.getStringCellValue().equalsIgnoreCase(ViewConstants.PROG_COST_TEMPLATE_HEADERS[5]))
                    {
                        String notes = String.valueOf(JSFUtils.resolveExpression("#{bindings.Notes.inputValue}"));
                        if(notes != null)
                        {
                            row.getCell(1).setCellValue(notes);
                        }
                    }
                }
            }
        }
    }
        
    public void setProgCostExcelRowCnt(int progCostExcelRowCnt)
    {
        this.progCostExcelRowCnt = progCostExcelRowCnt;
    }

    public int getProgCostExcelRowCnt()
    {
        return progCostExcelRowCnt;
    }
        
    private String formatNumber(BigDecimal value)
    {
        String retVal = "";
        if(value != null)
        {
            Currency currency = Currency.getInstance(Locale.US);
            NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.US);
            formatter.setCurrency(currency);
            formatter.setMaximumFractionDigits(currency.getDefaultFractionDigits());
            return formatter.format(value);    
        }
        return retVal;
    }
    
    private void createHeaders(XSSFWorkbook workbook, XSSFSheet worksheet, String iter)
    {
        XSSFRow hdrrow = worksheet.createRow(progCostExcelRowCnt);   
        XSSFCell cell0 = hdrrow.createCell(0);
        cell0.setCellStyle(createHeaderCellStyle(workbook, true));
        if(iter.equalsIgnoreCase(ViewConstants.PROG_COST_LINES_EHS_ITER))
        {
            cell0.setCellValue(ViewConstants.PROG_COST_ELEC_HEADER);    
        }
        else if(iter.equalsIgnoreCase(ViewConstants.PROG_COST_LINES_IMPS_ITER))
        {
            cell0.setCellValue(ViewConstants.PROG_COST_IMPL_HEADER);    
        }
        else if(iter.equalsIgnoreCase(ViewConstants.PROG_COST_LINES_OTHER_SER_ITER))
        {
            cell0.setCellValue(ViewConstants.PROG_COST_OTHER_SER_HEADER);    
        }
        else if(iter.equalsIgnoreCase(ViewConstants.PROG_COST_LINES_FCT_ITER))
        {
            cell0.setCellValue(ViewConstants.PROG_COST_FAC_HEADER);    
        }
        else if(iter.equalsIgnoreCase(ViewConstants.PROG_COST_LINES_PER_ITER))
        {
            cell0.setCellValue(ViewConstants.PROG_COST_PER_HEADER0);    
        }
        XSSFCell cell1 = hdrrow.createCell(1);
        cell1.setCellStyle(createHeaderCellStyle(workbook, true));
        cell1.setCellValue(ViewConstants.EMPTY_STRING);
        
        XSSFCell cell2 = hdrrow.createCell(2);
        cell2.setCellStyle(createHeaderCellStyle(workbook, true));
        cell2.setCellValue(ViewConstants.EMPTY_STRING);
        
        XSSFCell cell3 = hdrrow.createCell(3);
        cell3.setCellStyle(createHeaderCellStyle(workbook, false));
        cell3.setCellValue(ViewConstants.EMPTY_STRING);
        
        XSSFCell cell4 = hdrrow.createCell(4);
        cell4.setCellStyle(createHeaderCellStyle(workbook, false));
        cell4.setCellValue(ViewConstants.EMPTY_STRING);
        
        XSSFCell cell5 = hdrrow.createCell(5);
        cell5.setCellStyle(createHeaderCellStyle(workbook, false));
        cell5.setCellValue(ViewConstants.EMPTY_STRING);
        
        worksheet.addMergedRegionUnsafe(new CellRangeAddress(progCostExcelRowCnt, progCostExcelRowCnt, 0, 1));
    }
    
    private void createPersonnelHeader(XSSFWorkbook workbook, XSSFSheet worksheet, String iter)
    {
        XSSFRow hdrrow = worksheet.createRow(progCostExcelRowCnt);   
        XSSFCell cell0 = hdrrow.createCell(0);
        cell0.setCellStyle(createHeaderCellStyle(workbook, true));
        cell0.setCellValue(ViewConstants.PROG_COST_PER_HEADER0);    
        
        XSSFCell cell1 = hdrrow.createCell(1);
        cell1.setCellStyle(createHeaderCellStyle(workbook, true));
        cell1.setCellValue(ViewConstants.EMPTY_STRING);
        
        XSSFCell cell2 = hdrrow.createCell(2);
        cell2.setCellStyle(createHeaderCellStyle(workbook, true));
        cell2.setCellValue(ViewConstants.PROG_COST_PER_HEADER1);
        
        XSSFCell cell3 = hdrrow.createCell(3);
        cell3.setCellStyle(createHeaderCellStyle(workbook, true));
        cell3.setCellValue(ViewConstants.PROG_COST_PER_HEADER2);
        
        XSSFCell cell4 = hdrrow.createCell(4);
        cell4.setCellStyle(createHeaderCellStyle(workbook, true));
        cell4.setCellValue(ViewConstants.PROG_COST_PER_HEADER3);
        
        XSSFCell cell5 = hdrrow.createCell(5);
        cell5.setCellStyle(createHeaderCellStyle(workbook, true));
        cell5.setCellValue(ViewConstants.PROG_COST_PER_HEADER4);
        
        worksheet.addMergedRegionUnsafe(new CellRangeAddress(progCostExcelRowCnt, progCostExcelRowCnt, 0, 1));
    }
    
    private XSSFFont createFontStyle(XSSFWorkbook workbook)
    {
        XSSFFont fontStyle= workbook.createFont();
        fontStyle.setBold(true);
        fontStyle.setColor(IndexedColors.WHITE.getIndex());
        return fontStyle;
    }
    
    private XSSFFont createHeaderFontStyle(XSSFWorkbook workbook)
    {
        XSSFFont fontStyle= workbook.createFont();
        fontStyle.setBold(true);
        fontStyle.setColor(IndexedColors.BROWN.getIndex());
        return fontStyle;
    }
    
    private void incrementRowCnt()
    {
        progCostExcelRowCnt++;
    }

    public void setBind_categoryName(RichPopup bind_categoryName) {
        this.bind_categoryName = bind_categoryName;
    }

    public RichPopup getBind_categoryName() {
        return bind_categoryName;
    }
}
