package com.klx.view.beans.constants;

public class DMSViewConstants {
    
    
    
    public static final String GET_OPPORTUNITY_DETAILS = "getOpportunityDetails";
    public static final String OPPORTUNITY_ID = "opportunityId";
    public static final String OPPORTUNITY_NUMBER = "opportunityNumber";
    public static final String USER_NAME = "userName";
    public static final String CUSTOMER_INITIAL = "1";
    public static final String WORKING_QUOTE_DOCS = "2";
    public static final String CUSTOMER_FINAL = "3";
    public static final String AWARDS = "4";
    public static final String MIGRATED_DOCS = "5";
    public static final String GET_DOCUMENTS = "getDocuments";
    public static final String OPPORTUNITY_ID_ATTR = "OpportunityId";
    public static final String FOLDER_ID = "folderId";
    public static final String DOCUMENT_VO_ITERATOR = "DocumentVOIterator";
    public static final String SELECTEDFOLDER1 = "selectedFolder1";
    public static final String MOVE_DOCUMENTS = "moveDocuments";
    public static final String DOCUMENT_ID = "documentId";
    public static final String DOCUMENT_ID_ATTR = "DocumentId";
    public static final String DESTINATION_FOLDER_ID = "destinationFolderId";
    public static final String DELETE_DOCUMENTS = "deleteDocuments";
    public static final String SOURCE ="source";
    public static final String BPM ="BPM";
    public static final String UPLOAD_DOCUMENT ="uploadDocument";
    public static final String DOCUMENT ="document";
    public static final String OPPORTUNITY ="opportunity";
    public static final String SELECTED_FOLDER2 ="selectedFolder2";
    public static final String FACES_PAGES_LOGOUT_JSPX ="/faces/pages/logout.jspx";
    public static final String INITIAL_CONTEXT ="InitialContext";
    public static final String DMSUIMESSAGES_PROPERTIES_PATH ="com.klx.view.properties.DMSUIMessages";
    public static final String FOLDER_SELECTION_MSG = "FOLDER_SELECTION_MSG";
    public static final String DOCUMENT_SELECTION_MSG = "DOCUMENT_SELECTION_MSG";
    public static final String SAME_FOLDER_VALIDATION_MSG = "SAME_FOLDER_VALIDATION_MSG";
    public static final String MOVE_SUCCESS_MSG = "MOVE_SUCCESS_MSG";
    public static final String REMOVE_SUCCESS_MSG = "REMOVE_SUCCESS_MSG";
     public static final String UPLOAD_FILE_ERROR="UPLOAD_FILE_ERROR";
    public static final String  FILE_TYPE_ERROR="FILE_TYPE_ERROR";
    public static final String FILE_TITLE_ERROR="FILE_TITLE_ERROR";
    public static final String REQ_NOT_PROC_ERROR="REQ_NOT_PROC_ERROR";
     public static final String FILE_LENGTH="FILE_LENGTH";
    public static final int FILE_NAME_CHARACTERS = 240;
    public DMSViewConstants() {
        super();
    }
}
