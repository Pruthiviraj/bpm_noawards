package com.klx.view.beans.scope.pageFlow;

import com.klx.common.access.UserActionAccessManagement;
import com.klx.common.cache.ParamCache;
import com.klx.services.entities.Users;
import com.klx.view.beans.constants.ViewConstants;
import com.klx.view.beans.utils.ADFUtils;
import com.klx.view.beans.utils.JSFUtils;

import java.io.OutputStream;

import java.math.BigDecimal;

import java.util.ArrayList;

import java.util.Comparator;
import java.util.HashMap;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.jbo.VariableValueManager;
import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewCriteriaManager;
import oracle.jbo.ViewObject;

import org.apache.myfaces.trinidad.event.DisclosureEvent;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import oracle.adf.share.ADFContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewCriteriaItem;
import oracle.jbo.ViewCriteriaRow;
import oracle.jbo.common.JboCompOper;
import oracle.jbo.server.ViewObjectImpl;
import oracle.jbo.uicli.binding.JUCtrlListBinding;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class OpptyDashboard {
    private RichPopup statusPopupBinding;
    private String assignmentPopUpMessage;
    private RichSelectOneChoice selectOneChoice;
    private boolean opportunityDashboardAssignees;
    private boolean createOpportunity;

    public void setCreateOpportunity(boolean createOpportunity) {
        this.createOpportunity = createOpportunity;
    }

    public boolean isCreateOpportunity() {
        return createOpportunity;
    }

    public void setAssignmentPopUpMessage(String assignmentPopUpMessage) {
        this.assignmentPopUpMessage = assignmentPopUpMessage;
    }

    public String getAssignmentPopUpMessage() {
        return assignmentPopUpMessage;
    }

    public OpptyDashboard() {
        userActionsAccess();
    }
    
    public void userActionsAccess(){
        HashMap actions = new HashMap();
        actions.put(ViewConstants.OPPORTUNITY_DASHBOARD_ASSIGNEE, false);              
        actions.put(ViewConstants.OPPORTUNITY_DASHBOARD_CREATE_OPPORTUNITY, false); 
        HashMap userAcess = UserActionAccessManagement.getUserAccess(ADFContext.getCurrent().getSecurityContext().getUserRoles(), actions);        
        opportunityDashboardAssignees = ((Boolean)userAcess.get(ViewConstants.OPPORTUNITY_DASHBOARD_ASSIGNEE)).booleanValue();       
        createOpportunity = ((Boolean)userAcess.get(ViewConstants.OPPORTUNITY_DASHBOARD_CREATE_OPPORTUNITY)).booleanValue();       
    }

    private List<SelectItem> assignee = null;
    private List<SelectItem> teamAssignee = null;
    private HashMap<String, String> teamUSers = new HashMap<String, String>();
    private HashMap<String, String> salesUSers = new HashMap<String, String>();
    private String crfNumber = null;

    public void setTeamAssignee(List<SelectItem> teamAssignee) {
        this.teamAssignee = teamAssignee;
    }

    public List<SelectItem> getTeamAssignee() {
        if (null == teamAssignee) {
            List<SelectItem> newAssignee1 = new ArrayList<SelectItem>();

            try {
                String loggedInUser = (String) JSFUtils.resolveExpression("#{securityContext.userName}");
                if(null == ADFContext.getCurrent().getPageFlowScope().get("salesTeamNumber")){
                    OperationBinding getSalesTeamNoOfUser =
                        ADFUtils.getBindingContainer().getOperationBinding("getSalesTeamNoOfUser");
                    getSalesTeamNoOfUser.getParamsMap().put("loggedInUser", loggedInUser);
                
                    //BigDecimal salesTeamNo = (BigDecimal) getSalesTeamNoOfUser.execute();
                    List<BigDecimal> salesTeamNo = (List<BigDecimal>) getSalesTeamNoOfUser.execute();   
                    ADFContext.getCurrent().getPageFlowScope().put("salesTeamNumber", salesTeamNo);
                }
                //if (salesTeamNo != null) {
                if(ADFContext.getCurrent().getPageFlowScope().get("salesTeamNumber") != null){
                    DCBindingContainer bindings = this.getDCBindingsContainer();
                    DCIteratorBinding itorBinding = bindings.findIteratorBinding("CctCrfSalesTeamROVOIterator");
                    ViewObject cctCrfSalesTeamROVO = itorBinding.getViewObject();
                    if(null == ADFContext.getCurrent().getPageFlowScope().get("salesTeamRowCounts")){
                        List<BigDecimal> salesTeamNo = (List<BigDecimal>)ADFContext.getCurrent().getPageFlowScope().get("salesTeamNumber");
                        if(salesTeamNo.size() > 1){
                            ViewCriteria vc =  cctCrfSalesTeamROVO.createViewCriteria();
                            ViewCriteriaRow vcr =  vc.createViewCriteriaRow();                                                                                       
                              ViewCriteriaItem vci =vcr.ensureCriteriaItem("SalesTeamNumber");
                              vci.setOperator(JboCompOper.OPER_IN);
                              vci.setValueMinCardinality(1);
                              vci.setValueMaxCardinality(1000);
                            for (int i = 0; i < salesTeamNo.size(); i++)                                        
                              vci.setValue(i, salesTeamNo.get(i));
                             
                              vc.addRow(vcr);
                              cctCrfSalesTeamROVO.applyViewCriteria(vc);
                        }
                        else{
                            BigDecimal salesTeamNumber = BigDecimal.ZERO;
                            
                            if(salesTeamNo.size() == 1)
                                salesTeamNumber = BigDecimal.valueOf(Double.valueOf(ADFContext.getCurrent().getPageFlowScope().get("salesTeamNumber").toString().replace("[", "").replace("]", "")));
                            ViewCriteriaManager vcm = cctCrfSalesTeamROVO.getViewCriteriaManager();
                            ViewCriteria cctCrfSalesTeamROVOCriteria = vcm.getViewCriteria("CctCrfSalesTeamROVOCriteria");
                            VariableValueManager vvm = cctCrfSalesTeamROVOCriteria.ensureVariableManager();
                            //vvm.setVariableValue("b_salesTeamNo", salesTeamNo);
                            vvm.setVariableValue("b_salesTeamNo", salesTeamNumber);
                            cctCrfSalesTeamROVO.applyViewCriteria(cctCrfSalesTeamROVOCriteria);
                        }
                        cctCrfSalesTeamROVO.executeQuery();
                    }
                

                    RowSetIterator iter = cctCrfSalesTeamROVO.createRowSetIterator(null);
                    if(null == ADFContext.getCurrent().getPageFlowScope().get("salesTeamRowCounts"))
                        ADFContext.getCurrent().getPageFlowScope().put("salesTeamRowCounts", iter.getRowCount());
                    while (iter.hasNext()) {
                        Row currentRow = iter.next();
                        teamUSers.put(currentRow.getAttribute("SalesOwnerId").toString(),
                                      currentRow.getAttribute("SalesOwner").toString());
                    }

                    for (String key : teamUSers.keySet()) {
                        String value = teamUSers.get(key);
                       
                        SelectItem item = new SelectItem();
                        item.setLabel(value);
                        item.setValue(key);
                        newAssignee1.add(item);
                    }
                }
                newAssignee1.sort(Comparator.comparing(SelectItem::getLabel));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return newAssignee1;
        } else {
            return teamAssignee;
        }
    }

    public void setAssignee(List<SelectItem> assignee) {
        this.assignee = assignee;
    }

    public List<SelectItem> getAssignee() {
        if (null == assignee) {
            List<SelectItem> newAssignee = new ArrayList<SelectItem>();
            //  List<Users> usersList = new ArrayList<Users>();

            try {
                DCBindingContainer bindings = this.getDCBindingsContainer();
                DCIteratorBinding itorBinding = bindings.findIteratorBinding("CctCrfSalesTeamAllOpptyROVOIterator");
                ViewObject cctCrfSalesTeamROVO = itorBinding.getViewObject();
                cctCrfSalesTeamROVO.executeQuery();
              
                
                RowSetIterator iter = cctCrfSalesTeamROVO.createRowSetIterator(null);
                while (iter.hasNext()) {
                    Row currentRow = iter.next();
                    salesUSers.put(currentRow.getAttribute("SalesOwnerId").toString(),
                                   currentRow.getAttribute("SalesOwner").toString());
                }

                for (String key : salesUSers.keySet()) {
                    String value = salesUSers.get(key);
                  
                    SelectItem item = new SelectItem();
                    item.setLabel(value);
                    item.setValue(key);
                    newAssignee.add(item);
                }
                newAssignee.sort(Comparator.comparing(SelectItem::getLabel));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return newAssignee;
        } else {
            return assignee;
        }
    }

    public DCBindingContainer getDCBindingsContainer() {
        DCBindingContainer bindingsContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        return bindingsContainer;
    }

    public void refreshMyOppty(DisclosureEvent disclosureEvent) {
        // Add event code here...
        if(disclosureEvent.isExpanded()){
            OperationBinding executeMyOppty =
                ADFUtils.getBindingContainer().getOperationBinding("executeMyOpptySearchROVO");
            executeMyOppty.execute();
        }
    }

    public void commonrefreshAssignOppty() {
        DCBindingContainer bindings = this.getDCBindingsContainer();
        DCIteratorBinding itorBinding = bindings.findIteratorBinding("AssignedOpptySearchROVOIterator");
        ViewObject cCTOpptySearchROVO = itorBinding.getViewObject();

      

        ViewCriteriaManager vcm = cCTOpptySearchROVO.getViewCriteriaManager();
        ViewCriteria cCTOpptySearchROVOCriteria = vcm.getViewCriteria("CCTOpptySearchROVOCriteria");
        VariableValueManager vvm = cCTOpptySearchROVOCriteria.ensureVariableManager();
        vvm.setVariableValue("b_crfStatus1", "Active");
        vvm.setVariableValue("b_crfStatus2", "Pending");
        vvm.setVariableValue("b_crfStatus3", "Qualification");
        vvm.setVariableValue("b_othersAssign", (String) JSFUtils.resolveExpression("#{securityContext.userName}"));
        cCTOpptySearchROVO.applyViewCriteria(cCTOpptySearchROVOCriteria);
        cCTOpptySearchROVO.executeQuery();
      
    }

    public void refreshAssignedOppty(DisclosureEvent disclosureEvent) {
        // Add event code here...
        if(disclosureEvent.isExpanded())
            commonrefreshAssignOppty();
    }

    public void refreshClosedOppty(DisclosureEvent disclosureEvent) {
        // Add event code here...
        if(disclosureEvent.isExpanded()){
            DCBindingContainer bindings = this.getDCBindingsContainer();
            DCIteratorBinding itorBinding = bindings.findIteratorBinding("ClosedOpptySearchROVOIterator");
            ViewObject cCTOpptySearchROVO = itorBinding.getViewObject();
    
            ViewCriteriaManager vcm = cCTOpptySearchROVO.getViewCriteriaManager();
            ViewCriteria cCTOpptySearchROVOCriteria = vcm.getViewCriteria("CCTOpptySearchROVOCriteria");
            VariableValueManager vvm = cCTOpptySearchROVOCriteria.ensureVariableManager();
            vvm.setVariableValue("b_crfStatus1", "Won");
            vvm.setVariableValue("b_crfStatus2", "Lost");
            vvm.setVariableValue("b_crfStatus3", "Closed");
            vvm.setVariableValue("b_crfStatus4", "Won - Transactional");
            cCTOpptySearchROVO.applyViewCriteria(cCTOpptySearchROVOCriteria);
            cCTOpptySearchROVO.executeQuery();
        }
      
    }

    public void assignMyOppty(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        Object toUser = valueChangeEvent.getNewValue();

     
        //               Object toUser = valueChangeEvent.getNewValue();
      
        //        BindingContainer bindings1 = BindingContext.getCurrent().getCurrentBindingsEntry();
        //        JUCtrlListBinding listBinding = (JUCtrlListBinding) bindings1.get("AssignedTo");
        //        Object toUser = listBinding.getInputValue();
     

        genericAssign("MyOpptySearchROVOIterator", toUser.toString());
    }

    public void setStatusPopupBinding(RichPopup statusPopupBinding) {
        this.statusPopupBinding = statusPopupBinding;
    }

    public RichPopup getStatusPopupBinding() {
        return statusPopupBinding;
    }

    public void assignmentPopUpClose(ActionEvent actionEvent) {
        // Add event code here...
        OperationBinding executeMyOppty =
            ADFUtils.getBindingContainer().getOperationBinding("executeMyOpptySearchROVO");
        executeMyOppty.execute();
        commonrefreshAssignOppty();
        getStatusPopupBinding().hide();
    }

    public void assignAssignedOppty(ValueChangeEvent valueChangeEvent) {
        // Add event code here...

        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        Object toUser = valueChangeEvent.getNewValue();

      
        if (null != toUser) {
            genericAssign("AssignedOpptySearchROVOIterator", toUser.toString());
        }

    }

    public void genericAssign(String iterator, String toUser) {
        BigDecimal crfId = null;
        String result = null;

        DCBindingContainer bindings = this.getDCBindingsContainer();
        DCIteratorBinding myOpptySearchROVOIterator = bindings.findIteratorBinding(iterator);
        Row r = myOpptySearchROVOIterator.getCurrentRow();
        crfId = (BigDecimal) r.getAttribute("CrfId");

        OperationBinding assignOpptytoUsers = ADFUtils.getBindingContainer().getOperationBinding("assignOpptytoUsers");
        assignOpptytoUsers.getParamsMap().put("toUser", toUser);
        assignOpptytoUsers.getParamsMap().put("crfId", crfId);
        result = (String) assignOpptytoUsers.execute();

        if (result.equalsIgnoreCase("success")) {
            setAssignmentPopUpMessage("Opportunity Assigned Successfully!!");
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            getStatusPopupBinding().show(hints);
        }

    }

    public void setSelectOneChoice(RichSelectOneChoice selectOneChoice) {
        this.selectOneChoice = selectOneChoice;
    }

    public RichSelectOneChoice getSelectOneChoice() {
        return selectOneChoice;
    }

    public void refreshTeamOppty(DisclosureEvent disclosureEvent) {
        // Add event code here...
        if(disclosureEvent.isExpanded()){
        String loggedInUser = (String) JSFUtils.resolveExpression("#{securityContext.userName}");
        OperationBinding getSalesTeamNoOfUser =
            ADFUtils.getBindingContainer().getOperationBinding("getSalesTeamNumbersOfUser");
        getSalesTeamNoOfUser.getParamsMap().put("loggedInUser", loggedInUser);

        List<BigDecimal> salesTeamNo = (List<BigDecimal>) getSalesTeamNoOfUser.execute();        
        DCBindingContainer bindings = this.getDCBindingsContainer();
        DCIteratorBinding itorBinding = bindings.findIteratorBinding("TeamOpptySearchROVOIterator");        
                    
        ViewObjectImpl cCTOpptySearchROVO = (ViewObjectImpl)itorBinding.getViewObject();
        String salesTeam = salesTeamNo.toString().replace("[", "").replace("]", "");
        String[] opportunityStatus = ViewConstants.opportunityStatus;
        if (salesTeamNo != null) {
            if(salesTeamNo.size() > 1){
              
                ViewCriteria vc =  cCTOpptySearchROVO.createViewCriteria();
                ViewCriteriaRow vcr =  vc.createViewCriteriaRow();                                        
                ViewCriteriaItem vci2 = vcr.ensureCriteriaItem("CrfStatus");
                vci2.setOperator(JboCompOper.OPER_IN);
                vci2.setValueMinCardinality(1);
                vci2.setValueMaxCardinality(5);
                for (int i = 0; i < opportunityStatus.length; i++)                                        
                  vci2.setValue(i, opportunityStatus[i]);
                vcr.setConjunction(ViewCriteriaRow.VC_CONJ_AND);
                  ViewCriteriaItem vci =vcr.ensureCriteriaItem("SalesTeamNumber");
                  vci.setOperator(JboCompOper.OPER_IN);
                  vci.setValueMinCardinality(1);
                  vci.setValueMaxCardinality(1000);
                for (int i = 0; i < salesTeamNo.size(); i++)                                        
                  vci.setValue(i, salesTeamNo.get(i));
                 
                  vc.addRow(vcr);
                  cCTOpptySearchROVO.applyViewCriteria(vc);
                  cCTOpptySearchROVO.executeQuery();
            }        
            else{
                BigDecimal salesTeamNumber = BigDecimal.ZERO;
                
                if(salesTeamNo.size() == 1)
                    salesTeamNumber = BigDecimal.valueOf(Double.valueOf(salesTeamNo.toString().replace("[", "").replace("]", "")));                
                    
                ViewCriteriaManager vcm = cCTOpptySearchROVO.getViewCriteriaManager();
                ViewCriteria cCTOpptySearchROVOCriteria = vcm.getViewCriteria("CCTTeamOpptySearchROVOCriteria");
                VariableValueManager vvm = cCTOpptySearchROVOCriteria.ensureVariableManager();
                vvm.setVariableValue("b_crfStatus1", "Active");
                vvm.setVariableValue("b_crfStatus2", "Pending");
                vvm.setVariableValue("bind_crfStatus3", "Qualification");
                vvm.setVariableValue("b_salesTeamNo", salesTeamNumber);
                cCTOpptySearchROVO.applyViewCriteria(cCTOpptySearchROVOCriteria);
                cCTOpptySearchROVO.executeQuery();
            }
        }
        else{                
                cCTOpptySearchROVO.executeEmptyRowSet();
            }                            
        }
    }

    public void assignTeamOppty(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        Object toUser = valueChangeEvent.getNewValue();

      
        if (null != toUser) {
            genericAssign("TeamOpptySearchROVOIterator", toUser.toString());
        }
    }

    public void setCrfNumber(String crfNumber) {
        this.crfNumber = crfNumber;
    }

    public String getCrfNumber() {
       
        return crfNumber;
    }

    @SuppressWarnings("unchecked")
    public String setCrfDetails() {
        String oppID = null;
        getCrfNumber();
        if (crfNumber != null) {
            OperationBinding initializeUpdateOpportunity =
                ADFUtils.getBindingContainer().getOperationBinding("initializeUpdateOpp");
            initializeUpdateOpportunity.getParamsMap().put("crfNumber", this.crfNumber);

            initializeUpdateOpportunity.execute();
        }
        ADFContext.getCurrent().getRequestScope().put("KLXLegalFormat", "visible");
        ADFContext.getCurrent().getPageFlowScope().put("LegalFormat", "inVisible");
        ADFContext.getCurrent().getRequestScope().put("KLXReturnPart", "visible");
        ADFContext.getCurrent().getPageFlowScope().put("ReturnPart", "inVisible");
        OperationBinding binding = ADFUtils.findOperationBinding("getCrfID");
        binding.getParamsMap().put("CrfNo", getCrfNumber());
        binding.execute();
        if(binding.getResult() != null){
            oppID = (String)binding.getResult();
        }
        return "editOpportunity";
    } 

    public void exportMyOpportunities(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        ExportOpportunityDashboardAttributes(outputStream, "My Opportunity", "MyOpptySearchROVOIterator");
    }
    
    private void ExportOpportunityDashboardAttributes(OutputStream outputStream, String sheetName, String iteratorName){
        try {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet worksheet = workbook.createSheet(sheetName);
        
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dcIteratorBindings = bindings.findIteratorBinding(iteratorName);
        dcIteratorBindings.setRangeSize(-1);
        XSSFRow  excelrow = null;
        XSSFCellStyle style = workbook.createCellStyle();
        style.setBorderTop((short) 6); // double lines border
        style.setBorderBottom((short) 1); // single line border
        XSSFFont font = workbook.createFont();
        font.setFontHeightInPoints((short) 11);
        font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
        style.setFont(font);                 
                        
        // Get all the rows of a iterator
        oracle.jbo.Row[] rows = dcIteratorBindings.getAllRowsInRange();
        int i = 0;
        int k = 0;
        String[] selectedOpportunityAttributes = null;
        String[] opportunityDashboardAttributes = null;
        if(iteratorName.equals("TeamOpptySearchROVOIterator")){
            selectedOpportunityAttributes = new String[10];
            opportunityDashboardAttributes = new String[10];
        }else if(iteratorName.equals("AssignedOpptySearchROVOIterator")){
            selectedOpportunityAttributes = new String[11];
            opportunityDashboardAttributes = new String[11];
        }else{
            selectedOpportunityAttributes = new String[9];
            opportunityDashboardAttributes = new String[9];
        }
        for (oracle.jbo.Row row : rows) {
        String[] myTaskAttributes = row.getAttributeNames();
        
            if (i == 0) {
            for(String colName: myTaskAttributes){
                if(colName.equalsIgnoreCase("CrfNo")){
                    selectedOpportunityAttributes[k] = colName;    
                    opportunityDashboardAttributes[0] = selectedOpportunityAttributes[k];
                    k++;
                }
                if (colName.equalsIgnoreCase("ProjectNo")){
                    selectedOpportunityAttributes[k] = colName;  
                    opportunityDashboardAttributes[1] = selectedOpportunityAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("CrfStatus")){
                    selectedOpportunityAttributes[k] = colName; 
                    opportunityDashboardAttributes[2] = selectedOpportunityAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("CustomerRef")){
                    selectedOpportunityAttributes[k] = colName;   
                    opportunityDashboardAttributes[3] = selectedOpportunityAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("CustomerNo")){
                    selectedOpportunityAttributes[k] = colName;    
                    opportunityDashboardAttributes[4] = selectedOpportunityAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("TeamNo")){
                    if(iteratorName.equals("AssignedOpptySearchROVOIterator")){
                        selectedOpportunityAttributes[k] = colName;   
                        opportunityDashboardAttributes[5] = selectedOpportunityAttributes[k];
                        k++;
                    }
                }
                else if (colName.equalsIgnoreCase("SalesTeamRegion")){
                    if(iteratorName.equals("AssignedOpptySearchROVOIterator")){
                        selectedOpportunityAttributes[k] = colName;   
                        opportunityDashboardAttributes[6] = selectedOpportunityAttributes[k];
                        k++;
                    }
                }
                else if (colName.equalsIgnoreCase("CustomerName")){
                    if(iteratorName.equals("AssignedOpptySearchROVOIterator")){
                        selectedOpportunityAttributes[k] = colName; 
                        opportunityDashboardAttributes[7] = selectedOpportunityAttributes[k];
                        k++;
                    }
                    else{
                    selectedOpportunityAttributes[k] = colName; 
                    opportunityDashboardAttributes[5] = selectedOpportunityAttributes[k];
                    k++;
                    }
                }
                else if (colName.equalsIgnoreCase("DueDate")){
                    if(iteratorName.equals("AssignedOpptySearchROVOIterator")){
                        selectedOpportunityAttributes[k] = colName; 
                        opportunityDashboardAttributes[8] = selectedOpportunityAttributes[k];
                        k++;
                    }
                    else{
                    selectedOpportunityAttributes[k] = colName;   
                    opportunityDashboardAttributes[6] = selectedOpportunityAttributes[k];
                    k++;
                    }
                }                
                else if (colName.equalsIgnoreCase("CreationDate")){
                    if(iteratorName.equals("AssignedOpptySearchROVOIterator")){
                        selectedOpportunityAttributes[k] = colName; 
                        opportunityDashboardAttributes[9] = selectedOpportunityAttributes[k];
                        k++;
                    }
                    else{
                    selectedOpportunityAttributes[k] = colName;   
                    opportunityDashboardAttributes[7] = selectedOpportunityAttributes[k];
                    k++;
                    }
                }
                else if (colName.equalsIgnoreCase("SalesTeamNumber")){
                    if(iteratorName.equals("TeamOpptySearchROVOIterator")){
                        selectedOpportunityAttributes[k] = colName;   
                        opportunityDashboardAttributes[8] = selectedOpportunityAttributes[k];
                        k++;
                    }
                }
                else if (colName.equalsIgnoreCase("AssignedTo")){
                    if(iteratorName.equals("TeamOpptySearchROVOIterator")){
                        selectedOpportunityAttributes[k] = colName; 
                        opportunityDashboardAttributes[9] = selectedOpportunityAttributes[k];
                        k++;
                    }else if(iteratorName.equals("AssignedOpptySearchROVOIterator")){
                        selectedOpportunityAttributes[k] = colName; 
                        opportunityDashboardAttributes[10] = selectedOpportunityAttributes[k];
                        k++;
                    }else{
                        selectedOpportunityAttributes[k] = colName; 
                        opportunityDashboardAttributes[8] = selectedOpportunityAttributes[k];
                        k++;
                    }
                }
            }
            }
        
        //print header on first row in excel
        if (i == 0) {
            excelrow = (XSSFRow)worksheet.createRow((short)i);
            short j = 0;
            for (String colName : opportunityDashboardAttributes) {
               
                    //HSSFCell cellA1 = null;
                if (colName.equalsIgnoreCase("CrfNo")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);                    
                    if(iteratorName.equals("TeamOpptySearchROVOIterator"))
                        cellA1.setCellValue("CrfNo");
                    else
                        cellA1.setCellValue("Opportunity Number");
                    cellA1.setCellStyle(style); 
                    cellA1 = null;
                }
                else if (colName.equalsIgnoreCase("ProjectNo")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);                    
                    if(iteratorName.equals("TeamOpptySearchROVOIterator"))
                        cellA1.setCellValue("ProjectNo");
                    else
                        cellA1.setCellValue("Project#");
                    cellA1.setCellStyle(style);
                    cellA1 = null;
                }
                else if (colName.equalsIgnoreCase("CrfStatus")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);                    
                    if(iteratorName.equals("TeamOpptySearchROVOIterator"))
                        cellA1.setCellValue("CrfStatus");
                    else
                        cellA1.setCellValue("Status");
                    cellA1.setCellStyle(style);
                    cellA1 = null;
                }
                else if (colName.equalsIgnoreCase("CustomerRef")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    if(iteratorName.equals("TeamOpptySearchROVOIterator"))
                        cellA1.setCellValue("CustomerRef");
                    else
                        cellA1.setCellValue("Customer Ref#");
                    cellA1.setCellStyle(style);
                    cellA1 = null;
                }
                else if (colName.equalsIgnoreCase("CustomerNo")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    if(iteratorName.equals("TeamOpptySearchROVOIterator"))
                        cellA1.setCellValue("CustomerNo");
                    else
                        cellA1.setCellValue("Customer Number");
                    cellA1.setCellStyle(style);
                    cellA1 = null;
                }
                else if (colName.equalsIgnoreCase("TeamNo")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    if(iteratorName.equals("AssignedOpptySearchROVOIterator"))
                        cellA1.setCellValue("Team");
                    cellA1.setCellStyle(style);
                    cellA1 = null;
                }
                else if (colName.equalsIgnoreCase("SalesTeamRegion")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    if(iteratorName.equals("AssignedOpptySearchROVOIterator"))
                        cellA1.setCellValue("Region");
                    cellA1.setCellStyle(style);
                    cellA1 = null;
                }
                else if (colName.equalsIgnoreCase("CustomerName")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    if(iteratorName.equals("TeamOpptySearchROVOIterator"))
                        cellA1.setCellValue("CustomerName");
                    else
                        cellA1.setCellValue("Customer Name");
                    cellA1.setCellStyle(style);
                    cellA1 = null;
                }
                else if (colName.equalsIgnoreCase("DueDate")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    if(iteratorName.equals("TeamOpptySearchROVOIterator"))
                        cellA1.setCellValue("DueDate");
                    else
                        cellA1.setCellValue("Customer Due Date");
                    cellA1.setCellStyle(style);
                    cellA1 = null;
                }              
                else if (colName.equalsIgnoreCase("CreationDate")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    if(iteratorName.equals("TeamOpptySearchROVOIterator"))
                        cellA1.setCellValue("CreationDate");
                    else
                        cellA1.setCellValue("Creation Date");
                    cellA1.setCellStyle(style);
                }
                else if (colName.equalsIgnoreCase("SalesTeamNumber")){
                    if(iteratorName.equals("TeamOpptySearchROVOIterator")){
                        XSSFCell cellA1 = excelrow.createCell((short) j);                    
                        cellA1.setCellValue("SalesTeamNumber");
                        cellA1.setCellStyle(style);
                        cellA1 = null;
                    }
                }
                else if (colName.equalsIgnoreCase("AssignedTo")){
                    if(!iteratorName.equals("ClosedOpptySearchROVOIterator")){
                        XSSFCell cellA1 = excelrow.createCell((short) j);
                        cellA1.setCellValue("Assigned To");
                        cellA1.setCellStyle(style);
                        cellA1 = null;
                    }
                }
                    j++;
                 
            }
        }
        
        //print data from second row in excel
        ++i;
        short j = 0;
        excelrow = worksheet.createRow((short)i);
        for (String colName : opportunityDashboardAttributes) {                                                                          
            if (colName.equalsIgnoreCase("CrfNo")) {
                XSSFCell cell = excelrow.createCell(j);                    
                cell.setCellValue(row.getAttribute(colName).toString());    
                cell = null;
            }
               
                else if (colName.equalsIgnoreCase("ProjectNo")) {
                    if(null!=row.getAttribute(colName)){
                        XSSFCell cell = excelrow.createCell(j);                    
                        cell.setCellValue(row.getAttribute(colName).toString());
                        cell = null;
                    }
                }                   
                else if (colName.equalsIgnoreCase("CrfStatus")){
                    if(null!=row.getAttribute(colName)){
                        XSSFCell cell = excelrow.createCell(j);                    
                        cell.setCellValue(row.getAttribute(colName).toString());
                        cell = null;
                    }
                }
                else if (colName.equalsIgnoreCase("CustomerRef")){
                    if(null!=row.getAttribute(colName)){
                        XSSFCell cell = excelrow.createCell(j);                    
                        cell.setCellValue(row.getAttribute(colName).toString());
                        cell = null;
                    }
                }
                else if (colName.equalsIgnoreCase("CustomerNo")){
                    if(null!=row.getAttribute(colName)){
                        XSSFCell cell = excelrow.createCell(j);                    
                        cell.setCellValue(row.getAttribute(colName).toString());
                        cell = null;
                    }
                }
                else if (colName.equalsIgnoreCase("TeamNo")){
                    if(iteratorName.equals("AssignedOpptySearchROVOIterator")){
                        if(null!=row.getAttribute(colName)){
                            XSSFCell cell = excelrow.createCell(j);                    
                            cell.setCellValue(row.getAttribute(colName).toString());
                            cell = null;
                        }
                    }
                }
                else if (colName.equalsIgnoreCase("SalesTeamRegion")){
                    if(iteratorName.equals("AssignedOpptySearchROVOIterator")){
                        if(null!=row.getAttribute(colName)){
                            XSSFCell cell = excelrow.createCell(j);                    
                            cell.setCellValue(row.getAttribute(colName).toString());
                            cell = null;
                        }
                    }
                }
                else if (colName.equalsIgnoreCase("CustomerName")){
                    if(null!=row.getAttribute(colName)){
                        XSSFCell cell = excelrow.createCell(j);                    
                        cell.setCellValue(row.getAttribute(colName).toString());
                        cell = null;
                    }
                }
                else if (colName.equalsIgnoreCase("DueDate")){
                    if(null!=row.getAttribute(colName)){
                        XSSFCell cell = excelrow.createCell(j);                    
                        cell.setCellValue(row.getAttribute(colName).toString());
                        cell = null;
                    }
                }               
                else if (colName.equalsIgnoreCase("CreationDate")){
                    if(null!=row.getAttribute(colName)){
                        XSSFCell cell = excelrow.createCell(j);                    
                        cell.setCellValue(row.getAttribute(colName).toString());
                        cell = null;
                    }
                }
                else if (colName.equalsIgnoreCase("SalesTeamNumber")){
                    if(iteratorName.equals("TeamOpptySearchROVOIterator")){
                        if(null!=row.getAttribute(colName)){
                            XSSFCell cell = excelrow.createCell(j);                    
                            cell.setCellValue(row.getAttribute(colName).toString());
                            cell = null;
                        }
                    }
                }
                else if (colName.equalsIgnoreCase("AssignedTo")){
                    if(!iteratorName.equals("ClosedOpptySearchROVOIterator")){
                        DCIteratorBinding dcIteratorBindings2 = bindings.findIteratorBinding("CctCrfSalesTeamROVOIterator");
                        ViewObject cctCrfSalesTeamROVO = dcIteratorBindings2.getViewObject();
                        String salesOwnerName = "";
                        if(null!=row.getAttribute(colName)){
                            ViewCriteriaManager vcm = cctCrfSalesTeamROVO.getViewCriteriaManager();
                            ViewCriteria cctCrfSalesTeamROVOCriteria = vcm.getViewCriteria("CctCrfSalesTeamROVOCriteria2");
                            VariableValueManager vvm = cctCrfSalesTeamROVOCriteria.ensureVariableManager();
                            vvm.setVariableValue("bind_salesOwnerId", row.getAttribute(colName));
                            cctCrfSalesTeamROVO.applyViewCriteria(cctCrfSalesTeamROVOCriteria);
                            cctCrfSalesTeamROVO.executeQuery();
                            RowSetIterator iterator = cctCrfSalesTeamROVO.createRowSetIterator(null);
                            while(iterator.hasNext()){
                                Row currentRow = iterator.next();
                                salesOwnerName = currentRow.getAttribute("SalesOwner").toString();
                            }
                            XSSFCell cell = excelrow.createCell(j);                    
                            cell.setCellValue(salesOwnerName);
                            cell = null;
                        }
                    }
                }              
                j++;
               
            }
         
           worksheet.createFreezePane(0, 1, 0, 1);
           worksheet.autoSizeColumn(0);
           worksheet.autoSizeColumn(1);
           worksheet.autoSizeColumn(2);
           worksheet.autoSizeColumn(3);
           worksheet.autoSizeColumn(4);
           worksheet.autoSizeColumn(5);
           worksheet.autoSizeColumn(6);
           worksheet.autoSizeColumn(7);
           worksheet.autoSizeColumn(8);     
           if(iteratorName.equals("TeamOpptySearchROVOIterator"))
               worksheet.autoSizeColumn(9); 
        } 
            workbook.write(outputStream);
            outputStream.flush();
        
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exportTeamOpportunity(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        ExportOpportunityDashboardAttributes(outputStream, "Team Opportunity", "TeamOpptySearchROVOIterator");
    }

    public void exportAllOpportunity(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        ExportOpportunityDashboardAttributes(outputStream, "All Opportunity", "AssignedOpptySearchROVOIterator");
    }

    public void exportClosedOpportunity(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        ExportOpportunityDashboardAttributes(outputStream, "Closed Opportunity", "ClosedOpptySearchROVOIterator");
    }

    public void setOpportunityDashboardAssignees(boolean opportunityDashboardAssignees) {
        this.opportunityDashboardAssignees = opportunityDashboardAssignees;
    }

    public boolean isOpportunityDashboardAssignees() {
        return opportunityDashboardAssignees;
    }
}
