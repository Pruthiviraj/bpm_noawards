package com.klx.view.beans.scope.pageFlow;

import com.klx.cct.task.v1.RoleDetailsType;
import com.klx.cct.task.v1.RoleType;
import com.klx.cct.task.v1.TaskT;
import com.klx.common.access.UserActionAccessManagement;
import com.klx.common.logger.KLXLogger;
import com.klx.services.entities.Tasks;
import com.klx.view.beans.constants.ViewConstants;
import com.klx.view.beans.utils.ADFUtils;
import com.klx.view.beans.utils.JSFUtils;
import com.klx.view.error.CustomErrorHandler;

import java.io.OutputStream;

import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.component.rich.output.RichPanelCollection;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;

import oracle.jbo.Row;
import oracle.jbo.uicli.binding.JUCtrlListBinding;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.klx.view.beans.utils.DateUtil;

import java.sql.Timestamp;

import java.util.GregorianCalendar;
import javax.faces.event.ValueChangeEvent;

public class OppTasksFlowBean {
    private RichPopup createTaskPopUp;
    private RichPopup messagePopUp;
    private static KLXLogger logger = KLXLogger.getLogger();
    private RichSelectOneChoice groupLovBind;
    private RichPopup updateTaskPopUp;
    private RichPanelCollection panelCollectBind;
    private RichOutputText errorMsgComp;

    public OppTasksFlowBean() {
        super();
        userActionsAccess();
    }

    private String opportunityNumber;
    private String opportunityId;
    private String oscOpportunityId;
    private String description;
    private String message;
    private String selectedGroup;
    private String customerName;
    private String customerNumber;
    private Date customerDueDate;
    private Date custDueDate;
    private String dealStructure;
    private String assignedTo;
    private String selectedTaskNumber;
    private String selectedTaskId;
    private String taskDescription;
    private String groupName;
    private Date creationDate;
    private boolean createTask = false;
    private boolean closeTask = false;
    private String customerNoCRF;
    private String projectType;
    private String taskCompletedBy;
    private Date taskCompletionDate;
    private Date dueDate;
    private Date minDate;

    public void setTaskCompletedBy(String taskCompletedBy) {
        this.taskCompletedBy = taskCompletedBy;
    }

    public String getTaskCompletedBy() {
        return taskCompletedBy;
    }

    public void setTaskCompletionDate(Date taskCompletionDate) {
        this.taskCompletionDate = taskCompletionDate;
    }

    public Date getTaskCompletionDate() {
        return taskCompletionDate;
    }

    public void userActionsAccess(){
        HashMap actions = new HashMap();
        actions.put(ViewConstants.CREATE_TASK, false);
        actions.put(ViewConstants.CLOSE_TASK, false);
        HashMap userAcess = UserActionAccessManagement.getUserAccess(ADFContext.getCurrent().getSecurityContext().getUserRoles(), actions);        
        createTask = ((Boolean)userAcess.get(ViewConstants.CREATE_TASK)).booleanValue(); 
        closeTask = ((Boolean)userAcess.get(ViewConstants.CLOSE_TASK)).booleanValue(); 
    }

    public void setCloseTask(boolean closeTask) {
        this.closeTask = closeTask;
    }

    public boolean isCloseTask() {
        return closeTask;
    }

    public void setCreateTask(boolean createTask) {
        this.createTask = createTask;
    }

    public boolean isCreateTask() {
        return createTask;
    }

    public void setOscOpportunityId(String oscOpportunityId) {
        this.oscOpportunityId = oscOpportunityId;
    }

    public String getOscOpportunityId() {
        return oscOpportunityId;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setOpportunityId(String opportunityId) {
        this.opportunityId = opportunityId;
    }

    public String getOpportunityId() {
        return opportunityId;
    }

    public void setOpportunityNumber(String opportunityNumber) {
        this.opportunityNumber = opportunityNumber;
    }

    public String getOpportunityNumber() {
        return opportunityNumber;
    }

    public void createTaskForGroup(ActionEvent actionEvent) {
        BindingContainer bindings = BindingContext.getCurrent().getCurrentBindingsEntry();
        JUCtrlListBinding listBind = (JUCtrlListBinding) bindings.get("CreateTaskRolesVO1");
        Row currentRow = listBind.getCurrentRow();
        Object groupId = currentRow.getAttribute("UserGroup");
        boolean isValid = true;
        message = null;
        message = null;
        logger.log(Level.INFO, getClass(), "createTaskForGroup", "Task Due Date: " + this.getDueDate() + ", Cust Due Date: " + this.getCustDueDate());  
        /**RITM0038022-Task due date rules--comented condition**/
/*  if(this.getDueDate() != null  && this.getCustDueDate() != null && this.getDueDate().after(this.getCustDueDate()))
        {
            logger.log(Level.INFO, getClass(), "createTaskForGroup", "Task Due Date is greater that Customer Due Date");   
            message = "Task Due Date cannot be greater than Customer Due Date.";
            isValid = false;
        }  */  
        try 
        {
            if(isValid)
            {
                TaskT task = new TaskT();
                task.setOpportunityId(getOscOpportunityId());
                task.setOpportunityNumber(getOpportunityNumber());
                task.setTaskTitle("Manage");
                task.setTaskType("NEW");
                task.setComments(this.getDescription());
                System.out.println(this.getCustDueDate().toString().contains("Tue")|| this.getCustDueDate().toString().contains("Mon"));
                    
                /* if(groupId.equals("scCCT_Legal") && null == dueDate){
                    Date newDueDate;
                      newDueDate = new Date(this.getCustDueDate().getTime() - (1000 * 60 * 60 * 24)*2);
                    if(newDueDate.toString().contains("Sun")|| newDueDate.toString().contains("Sat")){
                         newDueDate = new Date(newDueDate.getTime() - (1000 * 60 * 60 * 24)*2);
                        
                    }
                   
                    logger.log(Level.INFO, getClass(), "createTaskForGroup", "Group ID and Task Due Date: "+groupId+"---" + this.getDueDate()
                         +"--newDueDate--->" + newDueDate+"--Cust Due Date: " + this.getCustDueDate());  
                    
                    task.setTaskDueDate(DateUtil.getXMLGregDate(newDueDate));
                } */
                if(null != this.getDueDate())
                {
                    task.setTaskDueDate(DateUtil.getXMLGregDate(this.getDueDate()));    
                }
                RoleDetailsType roleDetails = new RoleDetailsType();
                RoleType role = new RoleType();
                role.setIsTaskCreationRequired(true);
                role.setRoleName(groupId.toString());
                roleDetails.getRole().add(role);
                task.setRoleDetails(roleDetails);
                task.setCreatedBy(ADFContext.getCurrent()
                                            .getSecurityContext()
                                            .getUserName());

                OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                                       .getCurrentBindingsEntry()
                                                                       .getOperationBinding("createTask");
                op.getParamsMap().put("task", task);
                String result = (String) op.execute();
                if (null != result && "SUCCESS".equalsIgnoreCase(result)) {
                    this.message = "Task Created successfully for ";
                    isValid = true;
                    //ADFUtils.findOperationBinding("fetchTasksListByOppNo").execute();
                    DCIteratorBinding binding = ADFUtils.findIterator("fetchTasksListByOppNoIterator");
                    //binding.clear();
                    binding.executeQuery();
                } else {
                    this.message = "Error occured while creating task for ";
                }    
            }
        } catch (Exception ae) {
            logger.log(Level.SEVERE, getClass(), "createTaskForGroup", ae.getMessage());
            CustomErrorHandler.processError("CCT", "createTaskForGroup", "CCT_SYS_EX32", ae);
        }
        if(isValid)
        {
            getCreateTaskPopUp().hide();
            setDescription(null);
            setDueDate(null);
        }
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getMessagePopUp().show(hints);
    }

    public void setCreateTaskPopUp(RichPopup createTaskPopUp) {
        this.createTaskPopUp = createTaskPopUp;
    }

    public RichPopup getCreateTaskPopUp() {
        return createTaskPopUp;
    }

    public void openCreateTaskPopUp(ActionEvent actionEvent) 
    {
        try
        {
            setDescription(null);
            setDueDate(null);
            setMinDate(DateUtil.getCurrentDate());
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            getCreateTaskPopUp().show(hints);
        }
        catch(Exception ex)
        {
            logger.log(Level.SEVERE, getClass(), "openCreateTaskPopUp()", ex);
            CustomErrorHandler.processError("CCT", "openCreateTaskPopUp()", "CCT_SYS_EX91", ex);      
        }
        logger.log(Level.FINE, getClass(), "Leaving openCreateTaskPopUp()", "");
    }

    public void closeCreateTaskPopUp(ActionEvent actionEvent) {
        setDescription(null);
        setDueDate(null);
        getCreateTaskPopUp().hide();
    }

    public void setMessagePopUp(RichPopup messagePopUp) {
        this.messagePopUp = messagePopUp;
    }

    public RichPopup getMessagePopUp() {
        return messagePopUp;
    }


    public void setGroupLovBind(RichSelectOneChoice groupLovBind) {
        this.groupLovBind = groupLovBind;
    }

    public RichSelectOneChoice getGroupLovBind() {
        return groupLovBind;
    }

    public void setUpdateTaskPopUp(RichPopup updateTaskPopUp) {
        this.updateTaskPopUp = updateTaskPopUp;
    }

    public RichPopup getUpdateTaskPopUp() {
        return updateTaskPopUp;
    }

    public void closeTaskUpdatePopUp(ActionEvent actionEvent) {
        refreshListIterators();
        AdfFacesContext.getCurrentInstance().addPartialTarget(getPanelCollectBind());
        getUpdateTaskPopUp().hide();
    }

    public void openTaskUpdateListener(ActionEvent actionEvent) {
        try {
            logger.log(Level.INFO, getClass(), "openTaskUpdateListener", "Entering openTaskUpdateListener method");
            OperationBinding op = (OperationBinding) BindingContext.getCurrent()
                                                                   .getCurrentBindingsEntry()
                                                                   .getOperationBinding("setTaskNotesBindVar");
            op.getParamsMap().put("taskNumber",JSFUtils.resolveExpression("#{pageFlowScope.oppTasksFlowBean.selectedTaskNumber}"));
            op.execute();
            //TODO
            OperationBinding getOpportunityDetails =
                (OperationBinding) BindingContext.getCurrent()
                                                                                      .getCurrentBindingsEntry()
                                                                                      .getOperationBinding("getOpportunityDetails");
            getOpportunityDetails.getParamsMap().put("opportunityNumber", JSFUtils.resolveExpression("#{pageFlowScope.oppTasksFlowBean.opportunityNumber}"));
            getOpportunityDetails.execute();

            Tasks tasks=new Tasks();
            tasks.setOpportunityNumber(opportunityNumber);
            tasks.setCustomerNo(getCustomerNumber());
            tasks.setCustomerName(getCustomerName());
            tasks.setGroup(getSelectedGroup());
            tasks.setDealStructure(getDealStructure());
            tasks.setAssigneeName(getAssignedTo());
            tasks.setTaskNumber(getSelectedTaskNumber());
            tasks.setTaskDescription(getTaskDescription());
            tasks.setCreationDate(getCreationDate());
            tasks.setTaskId(getSelectedTaskId());
            tasks.setTaskCompletionDate(getTaskCompletionDate());
            tasks.setTaskCompletedBy(getTaskCompletedBy());
            tasks.setProjectType(getProjectType());
            tasks.setCustomerNoCrf(getCustomerNoCRF());
            if(null != ADFContext.getCurrent().getPageFlowScope().get("approvalStatus"))
                tasks.setOutcome(ADFContext.getCurrent().getPageFlowScope().get("approvalStatus").toString());
            if(null != ADFContext.getCurrent().getPageFlowScope().get("approvalComments"))
                tasks.setApprovalComments(ADFContext.getCurrent().getPageFlowScope().get("approvalComments").toString());
            if(null != ADFContext.getCurrent().getPageFlowScope().get("taskType"))
                tasks.setTaskType(ADFContext.getCurrent().getPageFlowScope().get("taskType").toString());
            JSFUtils.setExpressionValue("#{pageFlowScope.currentTask}", tasks);
            AdfFacesContext.getCurrentInstance().addPartialTarget(getUpdateTaskPopUp());
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            getUpdateTaskPopUp().show(hints);
        } catch (Exception e) {
            logger.log(Level.SEVERE, getClass(), "openTaskUpdateListener", e.getMessage());
            CustomErrorHandler.processError("CCT", "openTaskUpdateListener", "CCT_SYS_EX05", e);
        }
        logger.log(Level.INFO, getClass(), "openTaskUpdateListener", "Exiting openTaskUpdateListener method");
    }

    public void setSelectedGroup(String selectedGroup) {
        this.selectedGroup = selectedGroup;
    }

    public String getSelectedGroup() {
        return selectedGroup;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerDueDate(Date customerDueDate) {
        this.customerDueDate = customerDueDate;
    }

    public Date getCustomerDueDate() {
        return customerDueDate;
    }

    public void setDealStructure(String dealStructure) {
        this.dealStructure = dealStructure;
    }

    public String getDealStructure() {
        return dealStructure;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setSelectedTaskNumber(String selectedTaskNumber) {
        this.selectedTaskNumber = selectedTaskNumber;
    }

    public String getSelectedTaskNumber() {
        return selectedTaskNumber;
    }

    public void setSelectedTaskId(String selectedTaskId) {
        this.selectedTaskId = selectedTaskId;
    }

    public String getSelectedTaskId() {
        return selectedTaskId;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupName() {
        return groupName;
    }

    

    public void setPanelCollectBind(RichPanelCollection panelCollectBind) {
        this.panelCollectBind = panelCollectBind;
    }

    public RichPanelCollection getPanelCollectBind() {
        return panelCollectBind;
    }
    public void refreshListIterators(){
      DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
      DCIteratorBinding iter1 = binds.findIteratorBinding("fetchTasksListByOppNoIterator");
      iter1.executeQuery();
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCustomerNoCRF(String customerNoCRF) {
        this.customerNoCRF = customerNoCRF;
    }

    public String getCustomerNoCRF() {
        return customerNoCRF;
    }

    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }

    public String getProjectType() {
        return projectType;
    }

    public void exportTasksToExcel(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        try {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet worksheet = workbook.createSheet("Tasks");
        
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dcIteratorBindings = bindings.findIteratorBinding("fetchTasksListByOppNoIterator");
        dcIteratorBindings.setRangeSize(-1);
        XSSFRow  excelrow = null;
        XSSFCellStyle style = workbook.createCellStyle();
        style.setBorderTop((short) 6); // double lines border
        style.setBorderBottom((short) 1); // single line border
        XSSFFont font = workbook.createFont();
        font.setFontHeightInPoints((short) 11);
        font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
        style.setFont(font);                 
                        
        // Get all the rows of a iterator
        oracle.jbo.Row[] rows = dcIteratorBindings.getAllRowsInRange();
        int i = 0;
        int k = 0;
        String[] selectedTaskAttributes = new String[6];
        String[] taskDashboardAttributes = new String[6];
        for (oracle.jbo.Row row : rows) {
        String[] myTaskAttributes = row.getAttributeNames();
        
            if (i == 0) {
            for(String colName: myTaskAttributes){
                if(colName.equalsIgnoreCase("taskNumber")){
                    selectedTaskAttributes[k] = colName;    
                    taskDashboardAttributes[0] = selectedTaskAttributes[k];
                    k++;
                }
                if (colName.equalsIgnoreCase("title")){
                    selectedTaskAttributes[k] = colName;  
                    taskDashboardAttributes[1] = selectedTaskAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("assigneeName")){
                    selectedTaskAttributes[k] = colName; 
                    taskDashboardAttributes[2] = selectedTaskAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("groupName")){
                    selectedTaskAttributes[k] = colName;   
                    taskDashboardAttributes[3] = selectedTaskAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("taskDueDate")){
                    selectedTaskAttributes[k] = colName;    
                    taskDashboardAttributes[4] = selectedTaskAttributes[k];
                    k++;
                }
                else if (colName.equalsIgnoreCase("status")){
                    selectedTaskAttributes[k] = colName; 
                    taskDashboardAttributes[5] = selectedTaskAttributes[k];
                    k++;
                }               
            }
            }
        
        //print header on first row in excel
        if (i == 0) {
            excelrow = (XSSFRow)worksheet.createRow((short)i);
            short j = 0;
            for (String colName : taskDashboardAttributes) {
               
                    //HSSFCell cellA1 = null;
                if (colName.equalsIgnoreCase("taskNumber")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Task Number");
                    cellA1.setCellStyle(style); 
                    cellA1 = null;
                }
                else if (colName.equalsIgnoreCase("title")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Subject");
                    cellA1.setCellStyle(style);
                    cellA1 = null;
                }
                else if (colName.equalsIgnoreCase("assigneeName")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Owner");
                    cellA1.setCellStyle(style);
                    cellA1 = null;
                }
                else if (colName.equalsIgnoreCase("groupName")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Type");
                    cellA1.setCellStyle(style);
                    cellA1 = null;
                }
                else if (colName.equalsIgnoreCase("taskDueDate")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Due Date");
                    cellA1.setCellStyle(style);
                    cellA1 = null;
                }
                else if (colName.equalsIgnoreCase("status")){
                    XSSFCell cellA1 = excelrow.createCell((short) j);
                    cellA1.setCellValue("Status");
                    cellA1.setCellStyle(style);
                    cellA1 = null;
                }                
                    j++;
                 
            }
        }
        
        //print data from second row in excel
        ++i;
        short j = 0;
        excelrow = worksheet.createRow((short)i);
        for (String colName : taskDashboardAttributes) {                                                                          
            if (colName.equalsIgnoreCase("taskNumber")) {
                XSSFCell cell = excelrow.createCell(j);                    
                cell.setCellValue(row.getAttribute(colName).toString()); 
                cell = null;
            }
               
                else if (colName.equalsIgnoreCase("title")) {
                    if(null!=row.getAttribute(colName)){
                        XSSFCell cell = excelrow.createCell(j);                    
                        cell.setCellValue(row.getAttribute(colName).toString());
                        cell = null;
                    }
                }                   
                else if (colName.equalsIgnoreCase("assigneeName")){
                    if(null!=row.getAttribute(colName)){
                        XSSFCell cell = excelrow.createCell(j);                    
                        cell.setCellValue(row.getAttribute(colName).toString());
                        cell = null;
                    }
                }
                else if (colName.equalsIgnoreCase("groupName")){
                    if(null!=row.getAttribute(colName)){
                        XSSFCell cell = excelrow.createCell(j);                    
                        cell.setCellValue(row.getAttribute(colName).toString());
                        cell = null;
                    }
                }
                else if (colName.equalsIgnoreCase("taskDueDate")){
                    if(null!=row.getAttribute(colName)){
                        XSSFCell cell = excelrow.createCell(j);                    
                        cell.setCellValue(row.getAttribute(colName).toString());
                        cell = null;
                    }
                } 
                else if (colName.equalsIgnoreCase("status")){
                    if(null!=row.getAttribute(colName)){
                        XSSFCell cell = excelrow.createCell(j);   
                        if(row.getAttribute(colName).toString().equalsIgnoreCase("COMPLETED"))
                            cell.setCellValue(row.getAttribute(colName).toString());
                        else if(null == row.getAttribute("assigneeName"))
                            cell.setCellValue("UNASSIGNED");
                        else
                            cell.setCellValue(row.getAttribute(colName).toString());
                        
                        cell = null;
                    }
                }                        
                j++;
               
            }
         
           worksheet.createFreezePane(0, 1, 0, 1);
           worksheet.autoSizeColumn(0);
           worksheet.autoSizeColumn(1);
           worksheet.autoSizeColumn(2);
           worksheet.autoSizeColumn(3);
           worksheet.autoSizeColumn(4);
           worksheet.autoSizeColumn(5);           
        } 
            workbook.write(outputStream);
            outputStream.flush();
        
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void setDueDate(Date dueDate)
    {
        this.dueDate = dueDate;
    }

    public Date getDueDate()
    {
        return dueDate;
    }

    public void setCustDueDate(Date custDueDate)
    {
        this.custDueDate = custDueDate;
    }

    public Date getCustDueDate()
    {
        return custDueDate;
    }

    public void setErrorMsgComp(RichOutputText errorMsgComp)
    {
        this.errorMsgComp = errorMsgComp;
    }

    public RichOutputText getErrorMsgComp()
    {
        return errorMsgComp;
    }

    public void setMinDate(Date minDate)
    {
        this.minDate = minDate;
    }

    public Date getMinDate()
    {
        return minDate;
    }
}
