package com.klx.view.beans.scope.pageFlow;


import com.klx.common.access.UserActionAccessManagement;
import com.klx.common.cache.ParamCache;
import com.klx.common.logger.KLXLogger;
import com.klx.dms.model.constants.ModelConstants;
import com.klx.dms.model.impl.OpportunityDetailManagerImpl;
import com.klx.dms.model.viewObjects.DocumentVOImpl;
import com.klx.dms.model.viewObjects.DocumentVORowImpl;
import com.klx.dms.model.viewObjects.FolderVORowImpl;
import com.klx.dms.model.viewObjects.OpportunityVOImpl;
import com.klx.dms.model.viewObjects.OpportunityVORowImpl;
import com.klx.dms.model.pojo.Opportunity;
import com.klx.view.beans.constants.DMSViewConstants;
import com.klx.view.beans.constants.ViewConstants;
import com.klx.view.beans.utils.ADFUtils;
import com.klx.view.error.CustomErrorHandler;

import oracle.javatools.resourcebundle.BundleFactory;
import java.util.ResourceBundle;



import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.logging.Level;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.http.HttpSession;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.event.QueryEvent;
import oracle.adf.view.rich.model.FilterableQueryDescriptor;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;
import oracle.jbo.domain.BlobDomain;
import oracle.jbo.uicli.binding.JUCtrlListBinding;

import org.apache.myfaces.trinidad.event.DisclosureEvent;

public class DocumentMgmtBean {
    private RichPopup goPopUp;
    private RichPopup deleterPopUp;
    private RichPopup errorPopUp;
    private RichPopup infoPopUp;
   private String successMsg;
    private String oscOpportunityId;
    private String folderId;
   
    private String opportunityNumber;
    private String opportunityName;
    private String opportunityId;

   
  
    private RichTable customerIntTable;
    private RichTable awardsTable;
    private RichTable migrateDocsTable;
    private RichTable customerFinalTable;
    private RichTable workingQotTable;
    private String isValidOpportunity="true";
    private static KLXLogger logger = KLXLogger.getLogger(); 
    private String oscLink;
    private String isValidPopUp="false";
    private String dateFilter;
    private boolean movedeleteuploadDocument;


    public void setCustomerIntTable(RichTable customerIntTable) {
        this.customerIntTable = customerIntTable;
       
    }

    public RichTable getCustomerIntTable() {
        return customerIntTable;
    }

    public void setAwardsTable(RichTable awardsTable) {
        this.awardsTable = awardsTable;
    }

    public RichTable getAwardsTable() {
        return awardsTable;
    }

    public void setMigrateDocsTable(RichTable migrateDocsTable) {
        this.migrateDocsTable = migrateDocsTable;
    }

    public RichTable getMigrateDocsTable() {
        return migrateDocsTable;
    }

    public void setCustomerFinalTable(RichTable customerFinalTable) {
        this.customerFinalTable = customerFinalTable;
    }

    public RichTable getCustomerFinalTable() {
        return customerFinalTable;
    }

    public void setWorkingQotTable(RichTable workingQotTable) {
        this.workingQotTable = workingQotTable;
    }

    public RichTable getWorkingQotTable() {
        return workingQotTable;
    }
  

    public void setSuccessMsg(String successMsg) {
        this.successMsg = successMsg;
    }

    public String getSuccessMsg() {
        return successMsg;
    }

    public DocumentMgmtBean() {
        super();
        userActionsAccess();
    }

    public void userActionsAccess() {
        HashMap actions = new HashMap();
        actions.put(ViewConstants.MOVE_DELETE_UPLOAD_DMS_DOCUMENT, false);        
       
        HashMap userAcess = new HashMap();
        userAcess = UserActionAccessManagement.getUserAccess(ADFContext.getCurrent()
                                                                               .getSecurityContext()
                                                                               .getUserRoles(), actions);
                
        movedeleteuploadDocument = ((Boolean) userAcess.get(ViewConstants.MOVE_DELETE_UPLOAD_DMS_DOCUMENT)).booleanValue();
    }
    
    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public String getFolderId() {
        return folderId;
    }
   


    public void setOscOpportunityId(String oscOpportunityId) {
        this.oscOpportunityId = oscOpportunityId;
    }

    public String getOscOpportunityId() {
        return oscOpportunityId;
    }


    @SuppressWarnings("unchecked")
    public void fetchOpportunityDetails() {
        logger.log(Level.INFO, getClass(), "fetchOpportunityDetails",  "Entering fetchOpportunityDetails method");
        try {
            BindingContext bindingContext = BindingContext.getCurrent();
            BindingContainer bindingContainer = bindingContext.getCurrentBindingsEntry();
            OperationBinding opportunityDetails =
                bindingContainer.getOperationBinding(DMSViewConstants.GET_OPPORTUNITY_DETAILS);
            
            if(this.getOpportunityNumber()==null){
                
                this.opportunityNumber = (String) ADFContext.getCurrent()
                                                   .getSessionScope()
                                                   .get("CRFNo");
                if(null==opportunityNumber)
                    opportunityNumber=(String) ADFContext.getCurrent()
                                                   .getSessionScope()
                                                   .get(ViewConstants.OPPORTUNITY_NUMBER);
                 setOpportunityNumber(this.opportunityNumber);
            }
            opportunityDetails.getParamsMap().put(DMSViewConstants.OPPORTUNITY_NUMBER, getOpportunityNumber()); 
            
            logger.log(Level.INFO, getClass(), "fetchOpportunityDetails",
                       "OPPORTUNITY_NUMBER value -" + getOpportunityNumber() + "-");
            opportunityDetails.execute();
            OpportunityVOImpl opportunityVOImpl =
                (OpportunityVOImpl) ADFUtils.findIterator("OpportunityVOIterator").getViewObject();
            logger.log(Level.INFO, getClass(), "fetchOpportunityDetails",
                       "EstimatedRowCount number from DMS DB is -" + opportunityVOImpl.getEstimatedRowCount() + "-");
            
            if (opportunityVOImpl.getEstimatedRowCount() > 0) {
                OpportunityVORowImpl opportunityVORowImpl = (OpportunityVORowImpl) opportunityVOImpl.getCurrentRow();
                this.setOpportunityName(opportunityVORowImpl.getOpportunityName());
//                logger.log(Level.INFO, getClass(), "fetchOpportunityDetails",
//                           "Opportunity Name from DMS DB is -" + opportunityVORowImpl.getOpportunityName() + "-");
                this.setOpportunityNumber(opportunityVORowImpl.getOpportunityNumber());
                logger.log(Level.INFO, getClass(), "fetchOpportunityDetails",
                           "Opportunity Number from DMS DB is -" + opportunityVORowImpl.getOpportunityNumber() + "-");
                this.setOpportunityId(opportunityVORowImpl.getOpportunityId().toString());
                //this.setOpportunityId(null);
            } else {
                ViewObject opportunityVO1Impl = ADFUtils.findIterator("OpportunityVO1Iterator").getViewObject();
                opportunityVO1Impl.setNamedWhereClauseParam("bind_oppNo", opportunityNumber);
                opportunityVO1Impl.executeQuery();
//                OpportunityDetailManagerImpl opportunityDetailManagerImpl = new OpportunityDetailManagerImpl();
//                Opportunity opportunity =
//                    opportunityDetailManagerImpl.getOpportunityDetails(this.getOscOpportunityId());
                if (opportunityVO1Impl.getEstimatedRowCount() > 0) {
                    RowSetIterator iterator = opportunityVO1Impl.createRowSetIterator(null);
                    Row[] r = opportunityVO1Impl.getAllRowsInRange();
                    while(iterator.hasNext()){
                        Row currentRow = iterator.next();
                    if(null != currentRow.getAttribute("OpportunityName"))
                        this.setOpportunityName(currentRow.getAttribute("OpportunityName").toString());
//                    logger.log(Level.INFO, getClass(), "fetchOpportunityDetails",
//                               "Opportunity Name from Main Opportunity Table is -" + opportunityVOImpl.getCurrentRow().getAttribute("OpportunityName") + "-");
                    this.setOpportunityNumber(currentRow.getAttribute("OpportunityNumber").toString());
//                    logger.log(Level.INFO, getClass(), "fetchOpportunityDetails",
//                               "Opportunity Number from Main Opportunity Table is -" + opportunityVOImpl.getCurrentRow().getAttribute("OpportunityNumber") + "-");
                    this.setOpportunityId(currentRow.getAttribute("OpportunityId").toString());
                    }
                    /*if (opportunity.getOpportunityNumber() == null ||
                        opportunity.getOpportunityNumber().equalsIgnoreCase(""))
                        setIsValidOpportunity("false");*/
                } else {
                    this.setOpportunityName(null);
                    this.setOpportunityNumber(null);
                    this.setOpportunityId("0");

                }

            }
        } catch (Exception e) {
            CustomErrorHandler.processError(ModelConstants.DMS,ModelConstants.OPP_DETAILS,ModelConstants.DMS_SYS_EX05,e);
        }
        logger.log(Level.INFO, getClass(),"fetchOpportunityDetails", "Exiting fetchOpportunityDetails method");
       
    }

    public void fetchDocumentList() {
        this.setFolderId(DMSViewConstants.CUSTOMER_INITIAL);
        this.getDocuments();       
    }
    
    public void customerIntTabListener(DisclosureEvent disclosureEvent) {
        logger.log(Level.INFO, getClass(),"customerIntTabListener", "Entering customerIntTabListener method");
        try {
            if (disclosureEvent.isExpanded()) {
                this.setFolderId(DMSViewConstants.CUSTOMER_INITIAL);
                logger.log(Level.INFO, getClass(), "customerIntTabListener",
                           "Folder Id of customerIntTab is -" + DMSViewConstants.CUSTOMER_INITIAL + "-");
                this.getDocuments();
            } else {
                FilterableQueryDescriptor queryDescriptor =
                    (FilterableQueryDescriptor) getCustomerIntTable().getFilterModel();
                if (queryDescriptor != null && queryDescriptor.getFilterCriteria() != null) {
                    queryDescriptor.getFilterCriteria().clear();
                    getCustomerIntTable().queueEvent(new QueryEvent(getCustomerIntTable(), queryDescriptor));
                }
            }
        } catch (Exception e) { 
            CustomErrorHandler.processError(ModelConstants.DMS,ModelConstants.TAB_LISTNER,ModelConstants.DMS_GEN_EX01,e);
        }
        logger.log(Level.INFO, getClass(),"customerIntTabListener","Exiting customerIntTabListener method");
    }
    
    

    public void workingQotTabListener(DisclosureEvent disclosureEvent) {
        logger.log(Level.INFO, getClass(),"workingQotTabListener", "Entering workingQotTabListener method");
        try {
            if (disclosureEvent.isExpanded()) {
                this.setFolderId(DMSViewConstants.WORKING_QUOTE_DOCS);
                logger.log(Level.INFO, getClass(), "workingQotTabListener",
                           "Folder Id of workingQuot Tab is -" + DMSViewConstants.WORKING_QUOTE_DOCS + "-");
                this.getDocuments();
            } else {
                FilterableQueryDescriptor queryDescriptor =
                    (FilterableQueryDescriptor) getWorkingQotTable().getFilterModel();
                if (queryDescriptor != null && queryDescriptor.getFilterCriteria() != null) {
                    queryDescriptor.getFilterCriteria().clear();
                    getWorkingQotTable().queueEvent(new QueryEvent(getWorkingQotTable(), queryDescriptor));
                }
            }
        } catch (Exception e) {
            CustomErrorHandler.processError(ModelConstants.DMS,ModelConstants.TAB_LISTNER,ModelConstants.DMS_GEN_EX01,e);
        }
        logger.log(Level.INFO, getClass(),"workingQotTabListener","Exiting workingQotTabListener method");
    }

    public void customerFinalTabListener(DisclosureEvent disclosureEvent) {
        logger.log(Level.INFO, getClass(),"customerFinalTabListener", "Entering customerFinalTabListener method");
        try {
            if (disclosureEvent.isExpanded()) {
                this.setFolderId(DMSViewConstants.CUSTOMER_FINAL);
                logger.log(Level.INFO, getClass(), "customerFinalTabListener",
                           "Folder Id of customerfinal Tab is -" + DMSViewConstants.CUSTOMER_FINAL + "-");
                this.getDocuments();
            } else {
                FilterableQueryDescriptor queryDescriptor =
                    (FilterableQueryDescriptor) getCustomerFinalTable().getFilterModel();
                if (queryDescriptor != null && queryDescriptor.getFilterCriteria() != null) {
                    queryDescriptor.getFilterCriteria().clear();
                    getCustomerFinalTable().queueEvent(new QueryEvent(getCustomerFinalTable(), queryDescriptor));
                }
            }
        } catch (Exception e) {
            CustomErrorHandler.processError(ModelConstants.DMS,ModelConstants.TAB_LISTNER,ModelConstants.DMS_GEN_EX01,e);
        }
        logger.log(Level.INFO, getClass(),"customerFinalTabListener", "Exiting customerFinalTabListener method");
    }
    
    public void awardsTabListener(DisclosureEvent disclosureEvent) {
        logger.log(Level.INFO, getClass(),"awardsTabListener", "Entering awardsTabListener method");
        try {
            if (disclosureEvent.isExpanded()) {
                this.setFolderId(DMSViewConstants.AWARDS);
                logger.log(Level.INFO, getClass(), "awardsTabListener",
                           "Folder Id of awards Tab is -" + DMSViewConstants.AWARDS + "-");
                this.getDocuments();
            } else {
                FilterableQueryDescriptor queryDescriptor =
                    (FilterableQueryDescriptor) getAwardsTable().getFilterModel();
                if (queryDescriptor != null && queryDescriptor.getFilterCriteria() != null) {
                    queryDescriptor.getFilterCriteria().clear();
                    getAwardsTable().queueEvent(new QueryEvent(getAwardsTable(), queryDescriptor));
                }
            }
        } catch (Exception e) {
            CustomErrorHandler.processError(ModelConstants.DMS,ModelConstants.TAB_LISTNER,ModelConstants.DMS_GEN_EX01,e);
        }
        logger.log(Level.INFO, getClass(),"awardsTabListener", "Exiting awardsTabListener method");
    }

    public void migratedDocsTabListener(DisclosureEvent disclosureEvent) {
        logger.log(Level.INFO, getClass(),"migratedDocsTabListener", "Entering migratedDocsTabListener method");

        try {
            if (disclosureEvent.isExpanded()) {
                this.setFolderId(DMSViewConstants.MIGRATED_DOCS);
                logger.log(Level.INFO, getClass(), "migratedDocsTabListener",
                           "Folder Id of MigratedDocs Tab is -" + DMSViewConstants.MIGRATED_DOCS + "-");
                this.getDocuments();
            } else {
                FilterableQueryDescriptor queryDescriptor =
                    (FilterableQueryDescriptor) getMigrateDocsTable().getFilterModel();
                if (queryDescriptor != null && queryDescriptor.getFilterCriteria() != null) {
                    queryDescriptor.getFilterCriteria().clear();
                    getMigrateDocsTable().queueEvent(new QueryEvent(getMigrateDocsTable(), queryDescriptor));
                }
            }
        } catch (Exception e) {
            CustomErrorHandler.processError(ModelConstants.DMS,ModelConstants.TAB_LISTNER,ModelConstants.DMS_GEN_EX01,e);
        }
        logger.log(Level.INFO, getClass(),"migratedDocsTabListener", "Exiting migratedDocsTabListener method");
    }
    
    public void getDocuments() {
        logger.log(Level.INFO, getClass(), "getDocuments", "Entering getDocuments method");
        try {
          
                if (this.opportunityId == null) {
                    FacesMessage Message = new FacesMessage(ModelConstants.OPPTY_ID_NULL_MSG);   
                    Message.setSeverity(FacesMessage.SEVERITY_ERROR);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);      
                }
                else{
                    OperationBinding getDocuments = ADFUtils.findOperationBinding(DMSViewConstants.GET_DOCUMENTS);
                    Map map = getDocuments.getParamsMap();
                    //logger.log(Level.INFO, getClass(), "getDocuments", "opportunity Id is -" + this.getOpportunityId() + "-");
                    map.put(DMSViewConstants.OPPORTUNITY_ID, this.getOpportunityId());
                    map.put(DMSViewConstants.FOLDER_ID, this.getFolderId());
                    //logger.log(Level.INFO, getClass(), "getDocuments", "folder Id is -" + this.getFolderId() + "-");
                    getDocuments.execute();
                }
        } catch (Exception e) {
            CustomErrorHandler.processError(ModelConstants.DMS,ModelConstants.DOCUMENT_DETAILS,ModelConstants.DMS_GEN_EX01,e);
        }
        logger.log(Level.INFO, getClass(), "getDocuments", "Exiting getDocuments method");
    }


    public void selectAllVCL(ValueChangeEvent valueChangeEvent) {
        logger.log(Level.INFO, getClass(),"selectAllVCL", "Entering selectAllVCL method");
        try {
            DocumentVOImpl documentVOImpl = null;
            RowSetIterator createRowSetIterator = null;
            Boolean checkFlag = (Boolean) valueChangeEvent.getNewValue();
            logger.log(Level.INFO, getClass(), "selectAllVCL",
                       " valueChangeEvent.getNewValue() -" + valueChangeEvent.getNewValue() + "checkFlag Value -" +
                       checkFlag + "-");
            documentVOImpl =
                (DocumentVOImpl) ADFUtils.findIterator(DMSViewConstants.DOCUMENT_VO_ITERATOR).getViewObject();
            createRowSetIterator = documentVOImpl.createRowSetIterator(null);
            while (createRowSetIterator.hasNext()) {
                DocumentVORowImpl docuRow = (DocumentVORowImpl) createRowSetIterator.next();
                logger.log(Level.INFO, getClass(), "selectAllVCL", "docuRow object -" + docuRow + "-");
                if (checkFlag != null && checkFlag) {
                    docuRow.setCheckSelect(true);
                } else {
                    docuRow.setCheckSelect(false);
                }
            }
            logger.log(Level.INFO, getClass(), "selectAllVCL", "Exiting selectAllVCL method");
        } catch (Exception e) {
            CustomErrorHandler.processError(ModelConstants.DMS,ModelConstants.TAB_LISTNER,ModelConstants.DMS_GEN_EX01,e);
        }
    }
    
    public void errorAL(ActionEvent actionEvent) {
        this.getErrorPopUp().hide();
    }

    
    public void goCancel(ActionEvent actionEvent) {
        this.getGoPopUp().hide();
    }
    public void deleteCancel(ActionEvent actionEvent) {
        this.getDeleterPopUp().hide();
    }
    
     
    public void errorCancel(ActionEvent actionEvent) {
        this.getErrorPopUp().hide();
    }
    
    public void infoCancel(ActionEvent actionEvent) {
        this.getInfoPopUp().hide();
    }
    public void moveDocumentsAL(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(),"moveDocumentsAL", "Entering moveDocumentsAL method");
        RowSetIterator createRowSetIterator = null;
        try {
            DocumentVOImpl documentVOImpl = null;
           
            List documentId = new ArrayList();

            JUCtrlListBinding listBinding = (JUCtrlListBinding) ADFUtils.getDCBindingContainer().get(DMSViewConstants.SELECTEDFOLDER1);
            FolderVORowImpl floderRow = (FolderVORowImpl) listBinding.getSelectedValue();

            if (floderRow == null) {
                this.getGoPopUp().hide();
                this.setSuccessMsg(this.getPropertyValue(DMSViewConstants.FOLDER_SELECTION_MSG));
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                this.getErrorPopUp().show(hints);
                return;
            }
            documentVOImpl =
                (DocumentVOImpl) ADFUtils.findIterator(DMSViewConstants.DOCUMENT_VO_ITERATOR).getViewObject();
            createRowSetIterator = documentVOImpl.createRowSetIterator(null);
            while (createRowSetIterator.hasNext()) {
                DocumentVORowImpl documentVORowImpl = (DocumentVORowImpl) createRowSetIterator.next();
                if (documentVORowImpl.getCheckSelect() != null && documentVORowImpl.getCheckSelect()) {
                    logger.log(Level.INFO, getClass(), "moveDocumentsAL",
                               "documentVORowImpl.getDocumentId() -" + documentVORowImpl.getDocumentId() + "-");
                    documentId.add(documentVORowImpl.getDocumentId());
                }
            }
            
            if (documentId != null && documentId.isEmpty()) {
                this.getGoPopUp().hide();
                this.setSuccessMsg(this.getPropertyValue(DMSViewConstants.DOCUMENT_SELECTION_MSG));
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                this.getErrorPopUp().show(hints);
                return;
            } else if (floderRow.getFolderId().equals(getFolderId())) {
                this.getGoPopUp().hide();

                this.setSuccessMsg(this.getPropertyValue(DMSViewConstants.SAME_FOLDER_VALIDATION_MSG));
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                this.getErrorPopUp().show(hints);
                return;
            }

            OperationBinding getDocuments = ADFUtils.findOperationBinding(DMSViewConstants.MOVE_DOCUMENTS);
            Map map = getDocuments.getParamsMap();
            map.put(DMSViewConstants.DOCUMENT_ID, documentId);
            logger.log(Level.INFO, getClass(), "moveDocumentsAL",
                       "Destination Folder ID is  -" + floderRow.getFolderId() + "-");
            map.put(DMSViewConstants.DESTINATION_FOLDER_ID, floderRow.getFolderId());
            HashMap hashMap = (HashMap) getDocuments.execute();
            long count = (Long) hashMap.get("effectiveCount");
            logger.log(Level.INFO, getClass(), "moveDocumentsAL", "No of Documents -" + count + "-");
            if (count > 0) {
                this.getDocuments();
                this.getGoPopUp().hide();
                this.setSuccessMsg(count + " " + this.getPropertyValue(DMSViewConstants.MOVE_SUCCESS_MSG));
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                this.getInfoPopUp().show(hints);

            }
            String isValidPopUp = (String) hashMap.get("isValidPopUp");
            if ("true".equalsIgnoreCase(isValidPopUp)) {
                getGoPopUp().hide();
            }
        } catch (Exception e) {
            CustomErrorHandler.processError(ModelConstants.DMS,ModelConstants.MOVE_DOCUMENTS,ModelConstants.DMS_SYS_EX01,e);
        } finally {
            createRowSetIterator.closeRowSetIterator();
        }

        logger.log(Level.INFO, getClass(),"moveDocumentsAL", "Exiting moveDocumentsAL method");
      
    }
    
    public void showDeletePopUpAL(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(),"showDeletePopUpAL", "Entering showDeletePopUpAL method");
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
                 this.getDeleterPopUp().show(hints);
        logger.log(Level.INFO, getClass(),"showDeletePopUpAL", "Exiting showDeletePopUpAL method");
    }
    
    
    
    
    
    
    public void deleteDocumentsAL(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(),"deleteDocumentsAL", "Entering removeDocumentsAL method");
        RowSetIterator createRowSetIterator = null;
        try {
            DocumentVOImpl documentVOImpl = null;
            
            List documentId = new ArrayList();
            documentVOImpl =
                (DocumentVOImpl) ADFUtils.findIterator(DMSViewConstants.DOCUMENT_VO_ITERATOR).getViewObject();
            createRowSetIterator = documentVOImpl.createRowSetIterator(null);
            while (createRowSetIterator.hasNext()) {
                DocumentVORowImpl documentVORowImpl = (DocumentVORowImpl) createRowSetIterator.next();
                if (documentVORowImpl.getCheckSelect() != null && documentVORowImpl.getCheckSelect()) {
                    logger.log(Level.INFO, getClass(), "deleteDocumentsAL",
                               "Document Id is: " + documentVORowImpl.getDocumentId());
                    documentId.add(documentVORowImpl.getDocumentId());
                }
            }
           
            if (documentId != null && documentId.isEmpty()) {
                this.getDeleterPopUp().hide();
                this.setSuccessMsg(this.getPropertyValue(DMSViewConstants.DOCUMENT_SELECTION_MSG));
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                this.getErrorPopUp().show(hints);
                return;
            }

            OperationBinding deleteDocuments = ADFUtils.findOperationBinding(DMSViewConstants.DELETE_DOCUMENTS);
            Map map = deleteDocuments.getParamsMap();
            map.put(DMSViewConstants.DOCUMENT_ID, documentId);
            long count = (Long) deleteDocuments.execute();
            logger.log(Level.INFO, getClass(), "deleteDocumentsAL", "No of Documents -" + count + "-");
            if (count > 0) {

                this.getDocuments();
                this.getDeleterPopUp().hide();
                this.setSuccessMsg(count + " " + this.getPropertyValue(DMSViewConstants.REMOVE_SUCCESS_MSG));
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                this.getInfoPopUp().show(hints);
            }
        } catch (Exception e) {
            CustomErrorHandler.processError(ModelConstants.DMS,ModelConstants.DELETE_DOCUMENTS,ModelConstants.DMS_SYS_EX02,e);
        } finally {
            createRowSetIterator.closeRowSetIterator();
        }
        logger.log(Level.INFO, getClass(),"deleteDocumentsAL", "Exiting removeDocumentsAL method");
    }

    public void fileDownloadListner(FacesContext facesContext, OutputStream outputStream) {
        logger.log(Level.INFO, getClass(),"fileDownloadListner", "Entering fileDownloadListner method");
        DCIteratorBinding dcItr = ADFUtils.findIterator(DMSViewConstants.DOCUMENT_VO_ITERATOR);
        RowSetIterator rsIter = dcItr .getRowSetIterator(); 
       
        try {
               DocumentVORowImpl curRow = (DocumentVORowImpl) rsIter.getCurrentRow();
            
               logger.log(Level.INFO, getClass(),"fileDownloadListner","DocumentVORowImpl row object -"+curRow+"-");
               BlobDomain blob = curRow.getFileContent();
               
               HttpServletResponse response =
                   (HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();

               InputStream in = blob.getBinaryStream();
               outputStream = response.getOutputStream();

               byte[] buf = new byte[1024];
               int count;
               while ((count = in.read(buf)) >= 0) {
                   outputStream.write(buf, 0, count);
               }

               in.close();
               outputStream.flush();
               outputStream.close();
               blob.closeInputStream();
               blob.closeOutputStream();
               facesContext.responseComplete();
           } catch (IOException ex) {
               CustomErrorHandler.processError(ModelConstants.DMS,ModelConstants.DOWNLOAD_FILE,ModelConstants.DMS_SYS_EX04,ex);
           } catch (Exception e) {
             CustomErrorHandler.processError(ModelConstants.DMS,ModelConstants.DOWNLOAD_FILE,ModelConstants.DMS_SYS_EX04,e);
           }
        logger.log(Level.INFO, getClass(),"fileDownloadListner", "Exiting fileDownloadListner method");
    }
    
    
    
    public String getPropertyValue(String propertyKey){
        logger.log(Level.INFO, getClass(),"getPropertyValue", "Entering getPropertyValue method");
        logger.log(Level.INFO, getClass(),"getPropertyValue","Input parameter Property Key -"  +propertyKey+"-");
        String propertyValue =null;
             String bundle_name= DMSViewConstants.DMSUIMESSAGES_PROPERTIES_PATH;
            ResourceBundle resourceBundle =   BundleFactory.getBundle(bundle_name);
            
            if(resourceBundle != null)
            {
            propertyValue = resourceBundle.getString(propertyKey);
            
        }
        logger.log(Level.INFO, getClass(),"getPropertyValue", "Return parameter Property Value is -"+propertyValue+"-");
        logger.log(Level.INFO, getClass(),"getPropertyValue", "Exiting getPropertyValue method");
        return propertyValue;
    }   

    public void cancelPopUp(ActionEvent actionEvent) {
        RichPopup richPopup = (RichPopup) actionEvent.getComponent();
        richPopup.hide();
    }

    public void goShowPopUpAL(ActionEvent actionEvent) {
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
                 this.getGoPopUp().show(hints);
    }

    public void setGoPopUp(RichPopup goPopUp) {
        this.goPopUp = goPopUp;
    }

    public RichPopup getGoPopUp() {
        return goPopUp;
    }

    public void setDeleterPopUp(RichPopup deleterPopUp) {
        this.deleterPopUp = deleterPopUp;
    }

    public RichPopup getDeleterPopUp() {
        return deleterPopUp;
    }

    public void setErrorPopUp(RichPopup errorPopUp) {
        this.errorPopUp = errorPopUp;
    }

    public RichPopup getErrorPopUp() {
        return errorPopUp;
    }


    public void setInfoPopUp(RichPopup infoPopUp) {
        this.infoPopUp = infoPopUp;
    }

    public RichPopup getInfoPopUp() {
        return infoPopUp;
    }

    public void setOpportunityNumber(String opportunityNumber) {
        this.opportunityNumber = opportunityNumber;
    }

    public String getOpportunityNumber() {
        return opportunityNumber;
    }

    public void setOpportunityName(String opportunityName) {
        this.opportunityName = opportunityName;
    }

    public String getOpportunityName() {
        return opportunityName;
    }

    public void setOpportunityId(String opportunityId) {
        this.opportunityId = opportunityId;
    }

    public String getOpportunityId() {
        return opportunityId;
    }


   

    public void setIsValidOpportunity(String isValidOpportunity) {
        this.isValidOpportunity = isValidOpportunity;
    }

    public String getIsValidOpportunity() {
        return isValidOpportunity;
    }

    public void setOscLink(String oscLink) {
        this.oscLink = oscLink;
    }

    public String getOscLink() {
        logger.log(Level.INFO, getClass(),"getOscLink", "Entering getOscLink method");
        String osc= ParamCache.propertiesMap.get(ViewConstants.OSC_URL).toString()+oscOpportunityId;
        logger.log(Level.INFO, getClass(),"getOscLink", "OSC Url is -"+osc+"-");
        logger.log(Level.INFO, getClass(),"getOscLink", "Exiting getOscLink method");
        return osc;
    }

    public void setIsValidPopUp(String isValidPopUp) {
        this.isValidPopUp = isValidPopUp;
    }

    public String getIsValidPopUp() {
        return isValidPopUp;
    }
    public void filterDateVCL(ValueChangeEvent valueChangeEvent) {
        if (null!=valueChangeEvent && (null!=valueChangeEvent.getNewValue()))
    {
            try {
                valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
                OperationBinding op =
                    (oracle.adf.model.OperationBinding) BindingContext.getCurrent()
                                                                                        .getCurrentBindingsEntry()
                                                                                        .getOperationBinding("filterCreatedDate");
                HashMap hashMap = new HashMap();
                hashMap.put("date", getDateFilter());
               
                hashMap.put("oppID", getOpportunityId());
                hashMap.put("folderID", null == getFolderId() ? 1 : new BigDecimal(getFolderId()));
                hashMap.put("flag", "N");
                op.getParamsMap().put("hasMap", hashMap);
                op.execute();
            } catch (Exception e) {
                CustomErrorHandler.processError(ModelConstants.DMS,ModelConstants.DATE_FILTER,ModelConstants.DMS_SYS_EX05,e);
            }
    }
    }

    public void setDateFilter(String dateFilter) {
        this.dateFilter = dateFilter;
    }

    public String getDateFilter() {
        return dateFilter;
    }

    public void setMovedeleteuploadDocument(boolean movedeleteuploadDocument) {
        this.movedeleteuploadDocument = movedeleteuploadDocument;
    }

    public boolean isMovedeleteuploadDocument() {
        return movedeleteuploadDocument;
    }
}
