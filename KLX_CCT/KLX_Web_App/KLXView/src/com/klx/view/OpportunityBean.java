package com.klx.view;

import com.klx.view.beans.utils.ADFUtils;
import com.klx.view.beans.utils.JSFUtils;

import java.math.BigDecimal;

import javax.faces.event.ActionEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.RichPopup;

public class OpportunityBean {
    private RichPopup submitToSales;
    private String popUpMsg="Saved Successfully";


    public OpportunityBean() {
    }

    public void submitOSC(ActionEvent actionEvent) {
        BigDecimal opportunityId = (BigDecimal) JSFUtils.resolveExpression("#{bindings.OpportunityId.inputValue}");
        OperationBinding binding = (OperationBinding) ADFUtils.findOperationBinding("submitOSC");
        binding.getParamsMap().put("oppID", opportunityId);
 //       binding.execute();
        
        String status = (String) binding.execute();
        if("success".equalsIgnoreCase(status))
        setPopUpMsg("Quote submitted successfully");
        else
        setPopUpMsg("Quote submit UnSuccessful");
        
        RichPopup.PopupHints hints=new  RichPopup.PopupHints();
        getSubmitToSales().show(hints);
    }

    public void setSubmitToSales(RichPopup submitToSales) {
        this.submitToSales = submitToSales;
    }

    public RichPopup getSubmitToSales() {
        return submitToSales;
    }

    public void setPopUpMsg(String popUpMsg) {
        this.popUpMsg = popUpMsg;
    }

    public String getPopUpMsg() {
        return popUpMsg;
    }

    public void closePopUp(ActionEvent actionEvent) {
        // Add event code here...
        getSubmitToSales().hide();
    }
}
