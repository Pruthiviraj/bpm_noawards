package com.klx.view;

import com.klx.common.access.UserActionAccessManagement;
import com.klx.common.logger.KLXLogger;
import com.klx.view.beans.constants.ViewConstants;
import com.klx.view.beans.utils.ADFUtils;
import com.klx.view.beans.utils.JSFUtils;

import java.math.BigDecimal;

import java.util.HashMap;
import java.util.logging.Level;

import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.binding.OperationBinding;

import oracle.jbo.ViewObject;

public class AdditionalCost {
    private RichPopup savePopUp;
    private String popUpMsg=ViewConstants.SAVED_POPUP;
    private static KLXLogger logger = KLXLogger.getLogger(); 
    private BigDecimal customerRebate = null;
    private BigDecimal agentCommissions = null;
    private BigDecimal serviceFees = null;
    private BigDecimal shippingFeeTaxes = null;
    private Boolean saveMarginButton = false;
    private BigDecimal totalProgramCost = null;

    public void setTotalProgramCost(BigDecimal totalProgramCost) {
        this.totalProgramCost = totalProgramCost;
    }

    public BigDecimal getTotalProgramCost() {
        return totalProgramCost;
    }


    public AdditionalCost() {
    }
    
    public void userActionsAccess() {
            
            logger.log(Level.INFO, getClass(), "Additional Cost Bean  userActionsAccess method",
                           "save MarginButton(Before) ----- " + saveMarginButton);
            
            HashMap actions = new HashMap();
            actions.put(ViewConstants.SAVE_MARGIN_BUTTON, false);
            HashMap userAcess = UserActionAccessManagement.getUserAccess(ADFContext.getCurrent()
                                                                                       .getSecurityContext()
                                                                                       .getUserRoles(), actions);
            saveMarginButton = ((Boolean) userAcess.get(ViewConstants.SAVE_MARGIN_BUTTON)).booleanValue();
            
            logger.log(Level.INFO, getClass(), "Additional Cost Bean  userActionsAccess method",
                           "save MarginButton(After) ----- " + saveMarginButton);
            }
    
    public void saveMarginDetails(ActionEvent actionEvent) {
        logger.log(Level.INFO, getClass(),
                                            "saveMarginDetails",
                                            "Entering saveMarginDetails method");
        try{
        BigDecimal opportunityId = (BigDecimal) JSFUtils.resolveExpression("#{bindings.OpportunityId.inputValue}");
        DCBindingContainer binds = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding programCostIter = null;
        programCostIter= (DCIteratorBinding) binds.get("ProgramCostEOVO1Iterator");
        ViewObject programCostVO = programCostIter.getViewObject();
        programCostVO.clearCache();
        programCostVO.executeQuery();
        OperationBinding oBinding = (OperationBinding)BindingContext.getCurrent().getCurrentBindingsEntry().getOperationBinding("Commit");
        oBinding.execute();
        OperationBinding submitOSC = ADFUtils.findOperationBinding("submitOSC");
        submitOSC.getParamsMap().put("oppID", opportunityId);
        String status = (String) submitOSC.execute();            
        //programCostVO.setRefreshed(true);
        if("success".equalsIgnoreCase(status))
        setPopUpMsg(ViewConstants.MARGIN_SAVE_SUCCESS);
        else
            setPopUpMsg(ViewConstants.MARGIN_SAVE_FAIL);
            RichPopup.PopupHints hints=new  RichPopup.PopupHints();
            getSavePopUp().show(hints);
        }catch(Exception e) {
                logger.log(Level.SEVERE, getClass(),
                                                           "setOpportunityDetailsBindVar",
                                                          e.getMessage());
                e.printStackTrace();
            }
        logger.log(Level.INFO, getClass(),
                                                            "saveMarginDetails",
                                                            "Exiting saveMarginDetails method");

    
    }

    public void setSavePopUp(RichPopup savePopUp) {
        this.savePopUp = savePopUp;
    }

    public RichPopup getSavePopUp() {
        return savePopUp;
    }

    public void closePopUp(ActionEvent actionEvent) {
       getSavePopUp().hide();
    }

    public void setPopUpMsg(String popUpMsg) {
        this.popUpMsg = popUpMsg;
    }

    public String getPopUpMsg() {
        return popUpMsg;
    }

    public void setCustomerRebate(BigDecimal customerRebate) {
        this.customerRebate = customerRebate;
    }

    public BigDecimal getCustomerRebate() {
        return customerRebate;
    }

    public void setAgentCommissions(BigDecimal agentCommissions) {
        this.agentCommissions = agentCommissions;
    }

    public BigDecimal getAgentCommissions() {
        return agentCommissions;
    }

    public void setServiceFees(BigDecimal serviceFees) {
        this.serviceFees = serviceFees;
    }

    public BigDecimal getServiceFees() {
        return serviceFees;
    }

    public void setShippingFeeTaxes(BigDecimal shippingFeeTaxes) {
        this.shippingFeeTaxes = shippingFeeTaxes;
    }

    public BigDecimal getShippingFeeTaxes() {
        return shippingFeeTaxes;
    }

    public void setSaveMarginButton(Boolean saveMarginButton) {
        this.saveMarginButton = saveMarginButton;
    }

    public Boolean getSaveMarginButton() {
        return saveMarginButton;
    }
}
