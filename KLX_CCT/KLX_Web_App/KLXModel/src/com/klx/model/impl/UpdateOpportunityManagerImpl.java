package com.klx.model.impl;

import com.klx.services.wrapper.UpdateOpportunityServiceWrapper;

import java.math.BigDecimal;
import java.util.Date;

public class UpdateOpportunityManagerImpl {
    public UpdateOpportunityManagerImpl() {
        super();
    }
    
    public String updateOpportunityResource(String userId, String oscOppId,  String role){
        UpdateOpportunityServiceWrapper wrapper=new UpdateOpportunityServiceWrapper();
        return(wrapper.updateOpportunityResource(userId, oscOppId, role));
    }
    
    public String updateOpportunityData(String oscOppId,BigDecimal amount, BigDecimal netMargin, Boolean documentReadyToSubmit ){
        UpdateOpportunityServiceWrapper wrapper=new UpdateOpportunityServiceWrapper();
        return(wrapper.updateOpportunityData(oscOppId, amount, netMargin, documentReadyToSubmit));
    }
    
    public String createOpportunityRevision(String oppNumber, String oppID){
        UpdateOpportunityServiceWrapper wrapper=new UpdateOpportunityServiceWrapper();
        return wrapper.createOpportunityRevision(oppNumber, oppID);        
    }
    
    public String customerDueDateChange(String oppNumber, String oppID, Date oldDuedate){
        UpdateOpportunityServiceWrapper wrapper=new UpdateOpportunityServiceWrapper();
        return wrapper.customerDueDateChange(oppNumber, oppID, oldDuedate);
    }
    public String createAwardTask(String oppNumber, String oppID, String opptyOutcome){
        UpdateOpportunityServiceWrapper wrapper=new UpdateOpportunityServiceWrapper();
        return wrapper.createAwardTask(oppNumber, oppID, opptyOutcome);
       
    }
}
