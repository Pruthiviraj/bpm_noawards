package com.klx.model.impl;

import com.klx.model.appModule.KLXAppModuleImpl;

import java.io.InputStream;

import java.util.HashMap;

import oracle.adf.share.logging.ADFLogger;

import oracle.jbo.client.Configuration;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class BeaconFileManagerImpl {
    
    private static ADFLogger _logger = ADFLogger.createADFLogger(BeaconFileManagerImpl.class); 
    private static final String CLAZZ_NAME="BeaconFileManagerImpl";
    
    public BeaconFileManagerImpl() {
        super();
    }
    
    public HashMap uploadAwardsFileData(InputStream inputStream,HashMap inputParamMap,Object opptyid){
        _logger.entering(CLAZZ_NAME, "uploadAwardsFileData");
        HashMap hashMap=new HashMap();
        KLXAppModuleImpl am=null;
        String amDef = "com.klx.model.appModule.KLXAppModule";
        String config = "KLXAppModuleLocal";
        try{
        am=(KLXAppModuleImpl)Configuration.createRootApplicationModule(amDef, config);
        hashMap=am.insertAwardsFileInDB(inputStream,inputParamMap,opptyid);
        }
        catch(Exception e){
            _logger.severe(e.getMessage());
            e.printStackTrace();
        }
        finally{
        Configuration.releaseRootApplicationModule(am, false);
        }
        _logger.exiting(CLAZZ_NAME, "uploadAwardsFileData");
        return hashMap;
    }
    public HashMap uploadBeaconFileData(String revisionChoice,InputStream inputStream,String quoteNumber,String quoteRevision,String quoteRevisionId, String opportunityNumber,HashMap inputParamMap, String partsType){
            _logger.entering(CLAZZ_NAME, "uploadBeaconFileData");
            HashMap hashMap=new HashMap();
            KLXAppModuleImpl am=null;
            String amDef = "com.klx.model.appModule.KLXAppModule";
            String config = "KLXAppModuleLocal";
            try{
            am=(KLXAppModuleImpl)Configuration.createRootApplicationModule(amDef, config);
            hashMap=am.insertUploadFileInDB(revisionChoice,inputStream, quoteNumber,quoteRevision,quoteRevisionId,opportunityNumber,inputParamMap, partsType);
            }
            catch(Exception e){
                e.printStackTrace();
                _logger.severe(e.getMessage());
            }
            finally{
            Configuration.releaseRootApplicationModule(am, false);
            }
            _logger.exiting(CLAZZ_NAME, "uploadBeaconFileData");
            return hashMap;
        }
    
    public XSSFWorkbook downloadBeaconFileData(XSSFWorkbook workbook,String queueId, String quoteRevisionId, String opportunityNumber){
            _logger.entering(CLAZZ_NAME, "downloadBeaconFileData");
            KLXAppModuleImpl am=null;
            XSSFWorkbook xssfWorkbook=null;
            String amDef = "com.klx.model.appModule.KLXAppModule";
            String config = "KLXAppModuleLocal";
            try{
            am=(KLXAppModuleImpl)Configuration.createRootApplicationModule(amDef, config);
            xssfWorkbook=am.downloadFileFromDB(workbook, queueId,quoteRevisionId,opportunityNumber);
            }
            catch(Exception e){
                e.printStackTrace();
            }
            finally{
            Configuration.releaseRootApplicationModule(am, false);
            }
            _logger.exiting(CLAZZ_NAME, "downloadBeaconFileData");
            return xssfWorkbook;
            
        }
}
