package com.klx.model.impl;

import com.klx.cct.ad.user.v1.User;
import java.util.List;
import com.klx.services.entities.Users;
import com.klx.services.wrapper.ActiveDirectoryServiceWrapper;

public class ActiveDirectoryManagerImpl {
    public ActiveDirectoryManagerImpl() {
        super();
    }
    
    public User fetchUserDetailsById(String userId){
        ActiveDirectoryServiceWrapper wrapper=new ActiveDirectoryServiceWrapper();
        return (wrapper.fetchUserDetailsById(userId));
    }
    
    public List<Users> fetchUsersForRole(String role){
        ActiveDirectoryServiceWrapper wrapper=new ActiveDirectoryServiceWrapper();
        return (wrapper.fetchUsersForRole(role));
    }
}
