package com.klx.model.impl;

import com.klx.cct.task.v1.TaskT;
import com.klx.services.wrapper.ManageBPMTaskServiceWrapper;

import oracle.adf.share.logging.ADFLogger;

public class ManageBpmTaskImpl {
    public ManageBpmTaskImpl() {
        super();
    }
    
    private static ADFLogger _logger = ADFLogger.createADFLogger(ManageBpmTaskImpl.class); 
    private static final String CLAZZ_NAME="ManageBpmTaskImpl";
    
    
    public String createTask(TaskT task){
        ManageBPMTaskServiceWrapper wrapper=new ManageBPMTaskServiceWrapper();
        return(wrapper.createTask(task));
    }
}
