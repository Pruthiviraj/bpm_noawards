package com.klx.model.impl;

import com.klx.cct.ad.user.v1.User;
import com.klx.services.wrapper.TaskQueryUtilityWrapper;

public class TaskQueryUtilityImpl {
    public TaskQueryUtilityImpl() {
        super();
    }
    
    public User fetchUserForOpportunityRole(String opportunityNumber, String role){
        TaskQueryUtilityWrapper wrapper=new TaskQueryUtilityWrapper();
        return wrapper.fetchUserForOpportunityRole(opportunityNumber, role);
    }
    public String reassignTask(String taskId, String reassignedUserId, String loggedInUser){
        TaskQueryUtilityWrapper wrapper=new TaskQueryUtilityWrapper();
        return wrapper.reassignTask(taskId, reassignedUserId, loggedInUser);
    }
}
