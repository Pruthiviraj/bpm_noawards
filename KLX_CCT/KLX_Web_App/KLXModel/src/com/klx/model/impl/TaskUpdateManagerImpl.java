package com.klx.model.impl;

import com.klx.common.logger.KLXLogger;
import com.klx.services.wrapper.TaskUpdateServiceWrapper;

import java.util.Date;
import java.util.logging.Level;

import oracle.adf.share.logging.ADFLogger;




public class TaskUpdateManagerImpl {
    
    private static ADFLogger _logger = ADFLogger.createADFLogger(TaskUpdateManagerImpl.class); 
    private static final String CLAZZ_NAME="TaskUpdateManagerImpl";
    private static KLXLogger logger = KLXLogger.getLogger();    
    
    public TaskUpdateManagerImpl() {
        super();
    }
    
    
    public String assignTaskToUser(String username, String taskId){
       TaskUpdateServiceWrapper wrapper=new TaskUpdateServiceWrapper();
       String result=wrapper.assignTaskToUser(username, taskId);
       return result;
    }
    
    public String reassignTaskToUser(String username, String taskId){
        TaskUpdateServiceWrapper wrapper=new TaskUpdateServiceWrapper();
        String result=wrapper.reassignTaskToUser(username, taskId);
        return result;
    }
    
    public String releaseTask(String username, String taskId){
        TaskUpdateServiceWrapper wrapper=new TaskUpdateServiceWrapper();
        String result=wrapper.releaseTask(username, taskId);
        return result;
    }
    
    public String closeTask(String username, String taskId, String outcome,String comments,String assignedUser){
        logger.log(Level.INFO, getClass(),
                                            "closeTask",
                                            "Entering closeTask method");
       TaskUpdateServiceWrapper wrapper=new TaskUpdateServiceWrapper();
       String result=wrapper.closeTask(username, taskId, outcome,comments,assignedUser);
        logger.log(Level.INFO, getClass(),
                                            "closeTask",
                                            "Exiting closeTask method");
       return result;
    }
    
    public String updateTaskDueDate(String taskId, Date newDueDate){
        TaskUpdateServiceWrapper wrapper=new TaskUpdateServiceWrapper();
        String result=wrapper.updateTaskDueDate( taskId, newDueDate);
        return result;
    }
    
    public String approveOrRejectTask(String senderUserId, String taskNumber, String outcome,String comments, String approvalSource){
        logger.log(Level.INFO, getClass(),
                                            "approveOrRejectTask",
                                            "Entering approveOrRejectTask method");
        TaskUpdateServiceWrapper wrapper=new TaskUpdateServiceWrapper();
        String result=wrapper.approveOrRejectTask(senderUserId, taskNumber, outcome,comments,approvalSource);
        logger.log(Level.INFO, getClass(),
                                            "approveOrRejectTask",
                                            "Exiting approveOrRejectTask method");
        
        return result;
    }
        
}
