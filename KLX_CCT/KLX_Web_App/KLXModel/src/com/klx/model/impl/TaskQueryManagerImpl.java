package com.klx.model.impl;


import com.klx.cct.ad.user.v1.Roles;
import com.klx.cct.ad.user.v1.User;
import com.klx.common.cache.ParamCache;
import com.klx.common.logger.KLXLogger;
import com.klx.model.appModule.KLXAppModuleImpl;
import com.klx.services.entities.Approval;
import com.klx.services.entities.Opportunity;
import com.klx.services.entities.Tasks;
import com.klx.services.entities.Users;
import com.klx.services.wrapper.TaskQueryServiceWrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import oracle.adf.share.logging.ADFLogger;

import oracle.bpel.services.workflow.task.model.StateEnum;
import oracle.bpel.services.workflow.task.model.SubstateEnum;

import oracle.bpel.services.workflow.task.model.Task;

import oracle.jbo.client.Configuration;

public class TaskQueryManagerImpl {

    private static ADFLogger _logger = ADFLogger.createADFLogger(TaskQueryManagerImpl.class);
    private static final String CLAZZ_NAME = "TaskQueryManagerImpl";
    private static KLXLogger logger = KLXLogger.getLogger();
    private List<Users> myTaskFilterList = new ArrayList<Users>();
    private List<Users> assignedTaskFilterList = new ArrayList<Users>();

    public TaskQueryManagerImpl() {
        super();
    }

    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public Map<String, List<Tasks>> loadTasksList(String username) {
        logger.log(Level.INFO, getClass(), "loadTasksList", "Entering loadTasksList method");
        long loadTasksStartTime = System.currentTimeMillis();                
        Map<String, List<Tasks>> taskListMap = new HashMap<String, List<Tasks>>();
        List<Tasks> allTasksList = new ArrayList<Tasks>();
        List<Tasks> myTaskList = new ArrayList<Tasks>();
        List<Tasks> assignedTaskList = new ArrayList<Tasks>();
        List<Tasks> unassignedTaskList = new ArrayList<Tasks>();
        List<Tasks> approvalTaskList = new ArrayList<Tasks>();
        
        Opportunity oppForTask = null;
        Map<String, List<Users>> usersForRoleMap = new HashMap<String, List<Users>>();
        Map<String, Users> userDetailsMap = new HashMap<String, Users>();
        HashMap<String, Opportunity> oppForTaskMap = null;
        Map<String, Users> assignedListMap = new HashMap<String, Users>(); 
        KLXAppModuleImpl am = null;
        String amDef = "com.klx.model.appModule.KLXAppModule";
        String config = "KLXAppModuleLocal";
        am = (KLXAppModuleImpl) Configuration.createRootApplicationModule(amDef, config);
        List<Users> assigneeList = new ArrayList<Users>();       
        try {
            TaskQueryServiceWrapper wrapper = new TaskQueryServiceWrapper();
            ActiveDirectoryManagerImpl impl = new ActiveDirectoryManagerImpl();
          
            Users loggedInUserDetails = fetchLoggedInUserDetails(username);
            List<String> userRoles = loggedInUserDetails.getUserGroups(); 
//            for(String userRole: userRoles){
//                myTaskFilterList = impl.fetchUsersForRole(userRole);
//                assignedTaskFilterList = impl.fetchUsersForRole(userRole);
//            }
            allTasksList = wrapper.fetchMyTasksList(username, userRoles);            
            oppForTaskMap = am.fetchAllOpportunity();
            
            if (allTasksList.size() > 0) {
                for (Tasks task : allTasksList) {

                    try {
                        if (!task.getOpportunityNumber().equals("0")) {
                            if(oppForTaskMap.size() > 0){                                                        
                                if(oppForTaskMap.get(task.getOpportunityNumber()) != null){
                                oppForTask = oppForTaskMap.get(task.getOpportunityNumber());                               
                            
                            task.setOpportunityId(oppForTask.getOpportunityId());
                            task.setSalesMethod(oppForTask.getSalesMethod());
                            task.setPricingManagerRep(oppForTask.getPricingManagerRep());
                            task.setProposalManagerRep(oppForTask.getProposalManagerRep());
                            task.setSalesRep(oppForTask.getSalesRep());
                            task.setCustomerName(oppForTask.getCustomerName());
                            task.setCustomerNo(oppForTask.getCustomerNo());
                            task.setDealType(oppForTask.getDealType());
                            task.setDealStructure(oppForTask.getDealStructure());                           
                            task.setCustomerDueDate(oppForTask.getCustomerDueDate());
                            task.setTeamNumber(oppForTask.getTeamNumber());
                            task.setProjectType(oppForTask.getProjectType());
                            task.setContractType(oppForTask.getContractType());
                            task.setCustomerNoCrf(oppForTask.getCustomerNoCrf());                            
                           
                                }                            
                            }
                            
                        }
                        
                        if(null == usersForRoleMap.get(task.getGroup())){
                            updateUsersMap(task.getGroup(),usersForRoleMap,userDetailsMap);
                        }    
                        assigneeList = usersForRoleMap.get(task.getGroup());
                        task.setAssigneeList(assigneeList); 
                        
                        if (null != task.getAssignee() && !"".equalsIgnoreCase(task.getAssignee())) {
                             
                            if(null != userDetailsMap.get(task.getAssignee())){
                                Users userDetails =  userDetailsMap.get(task.getAssignee());
                                task.setAssigneeName(userDetails.getUserName());                              
                            }
                            else {                                
                                User userDetails =  impl.fetchUserDetailsById(task.getAssignee());  
                                
                                task.setAssigneeName(userDetails.getName());
                                task.setInactiveUser(true);                               
                            }
                            
                        }     

                        //task.setAssigneeList(assigneeList);
                        if (null != task.getState() && StateEnum.ASSIGNED
                                                                .value()
                                                                .equalsIgnoreCase(task.getState())) {
                           

                                if (task.getGroup().equals(ParamCache.userRoleMap.get("CCT_SALES_VP"))||
                                    task.getGroup().equals((String) ParamCache.userRoleMap.get("CCT_SALES_DIRECTOR"))|| 
                                    task.getGroup().equals((String) ParamCache.userRoleMap.get("CCT_OPERATIONS_VP"))||
                                    task.getGroup().equals((String) ParamCache.userRoleMap.get("CCT_GROUP_VP_AND_GM"))||
                                    task.getGroup().equals((String) ParamCache.userRoleMap.get("CCT_FINANCE_VP"))||
                                    task.getGroup().equals((String) ParamCache.userRoleMap.get("CCT_CORPORATE_CFO"))||
                                    task.getGroup().equals((String) ParamCache.userRoleMap.get("CCT_SUPPLY_CHAIN_VP"))||
                                    task.getGroup().equals((String) ParamCache.userRoleMap.get("CCT_ACCOUNTING_DIRECTOR"))||
                            task.getGroup().equals((String) ParamCache.userRoleMap.get("CCT_SR_CONTRACTS_DIRECTOR")))
                                    {
                                        approvalTaskList.add(task);                                       
                                     } 
                          
                            
                            if(userRoles.contains(task.getGroup())){ 
                                if(task.isInactiveUser()){
                                    if(assignedListMap.get(task.getAssignee()) == null){
                                        Users userDetail = new Users();
                                        userDetail.setUserID(task.getAssignee());
                                        userDetail.setUserName(task.getAssigneeName());                                        
                                        assignedListMap.put(task.getAssignee(), userDetail);
                                    }                                   
                                }
                            if (null != task.getSubState() &&
                                (SubstateEnum.ACQUIRED.value().equalsIgnoreCase(task.getSubState()) ||
                                 SubstateEnum.REASSIGNED.value().equalsIgnoreCase(task.getSubState()))) {
                                if (null != task.getAssignee() && username.equalsIgnoreCase(task.getAssignee())) {                                    
                                    myTaskList.add(task);
                                } else if (null != task.getAssignee()) { 
                                    assignedTaskList.add(task);                                    
                                } else {                                   
                                    unassignedTaskList.add(task);
                                }
                            } else if (null != task.getAssignee() && !"".equalsIgnoreCase(task.getAssignee())) {
                                if (username.equalsIgnoreCase(task.getAssignee())) {                                    
                                    myTaskList.add(task);
                                } else {                                                                                                        
                                    assignedTaskList.add(task);                                                                        
                                    //addInactiveUserToAssigneeList(assignedTaskList.get(0),task,userDetailsMap);                                        
                                }
                            } else {                               
                                unassignedTaskList.add(task);
                            }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if(assignedTaskList.size() > 0 && assignedListMap.size() > 0)
                    updateAssignedListWithInactiveUsers(assignedTaskList,assignedListMap);                
                else if(assignedTaskList.size() > 0)
                    assignedTaskFilterList.addAll(assignedTaskList.get(0).getAssigneeList());
                if(myTaskList.size() > 0)
                    myTaskFilterList.addAll(myTaskList.get(0).getAssigneeList());
                else if(unassignedTaskList.size() > 0)
                    myTaskFilterList.addAll(unassignedTaskList.get(0).getAssigneeList());
                taskListMap.put("myTaskList", myTaskList);
                taskListMap.put("assignedTaskList", assignedTaskList);
                taskListMap.put("unassignedTaskList", unassignedTaskList);
                taskListMap.put("approvalTaskList", approvalTaskList);
            }
        } catch (Exception e) {
            //e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "loadTasksList", e.getMessage());
        } finally {
            Configuration.releaseRootApplicationModule(am, false);
        }
        long loadTasksEndTime = System.currentTimeMillis();           
        logger.log(Level.INFO, getClass(), "loadTasksList", "Total time taken by loadTasksList method is: "+(loadTasksEndTime - loadTasksStartTime));
        logger.log(Level.INFO, getClass(), "loadTasksList", "Exiting loadTasksList method");
        return taskListMap;
    }

    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    private void updateAssignedListWithInactiveUsers(List assignedTaskList, Map assignedListMap){
        List assignedList = new ArrayList();
        assignedList.addAll(((Tasks)assignedTaskList.get(0)).getAssigneeList());          
        assignedList.addAll(assignedListMap.values());                   
        for(int i=0; i<assignedTaskList.size(); i++){
            Tasks task = (Tasks)assignedTaskList.get(i);            
            task.setAssigneeList(assignedList);
        }
        
        assignedTaskFilterList.addAll(assignedList);
    }

    public List<Tasks> loadCompleteTaskList(String username) {
        logger.log(Level.INFO, getClass(), "loadCompleteTaskList", "Entering loadCompleteTaskList method");
        List<Tasks> completedTaskList = new ArrayList<Tasks>();
        List<Tasks> allTasksList = new ArrayList<Tasks>();
        KLXAppModuleImpl am = null;
        String amDef = "com.klx.model.appModule.KLXAppModule";
        String config = "KLXAppModuleLocal";
        am = (KLXAppModuleImpl) Configuration.createRootApplicationModule(amDef, config);
        List<Users> assigneeList = new ArrayList<Users>();
        try {
            TaskQueryServiceWrapper wrapper = new TaskQueryServiceWrapper();
            ActiveDirectoryManagerImpl impl = new ActiveDirectoryManagerImpl();
            User loggedInUser = impl.fetchUserDetailsById(username);
            allTasksList = wrapper.fetchCompletedTasksList(username);
            if (allTasksList.size() > 0) {
                for (Tasks task : allTasksList) {
                    if (!task.getOpportunityNumber().equals("0")) {
                        Tasks oppTask = am.fetchOpportunityForTask(task.getOpportunityNumber());
                        task.setCustomerName(oppTask.getCustomerName());
                        task.setCustomerNo(oppTask.getCustomerNo());
                        task.setDealStructure(oppTask.getDealStructure());
                        task.setProjectType(oppTask.getProjectType());
                        task.setCustomerNoCrf(oppTask.getCustomerNoCrf());
                        //                              task.setAssigneeName(oppTask.getAssigneeName());
                        task.setGroupName(oppTask.getGroupName());
                        // task.setAssignee(task.getAssignee());

                    }
                    //                    if (null != task.getAssignee() && !"".equalsIgnoreCase(task.getAssignee())) {
                    //                        User userDetails = impl.fetchUserDetailsById(task.getAssignee());
                    //                        task.setAssigneeName(userDetails.getName());
                    //                    }
                    //                                            assigneeList = impl.fetchUsersForRole(task.getGroup());
                    //                    task.setAssigneeList(assigneeList);
                    completedTaskList.add(task);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "loadCompleteTaskList", e.getMessage());
        }

        logger.log(Level.INFO, getClass(), "loadCompleteTaskList", "Exiting loadCompleteTaskList method");
        return completedTaskList;
    }

    private void updateUsersMap(String groupName, Map usersForRoleMap, Map userDetailsMap){
        ActiveDirectoryManagerImpl impl = new ActiveDirectoryManagerImpl();
        List<Users> assigneeList = impl.fetchUsersForRole(groupName);
        usersForRoleMap.put(groupName, assigneeList);        
        for(int i=0; i < assigneeList.size(); i++){            
            Users userDetails = assigneeList.get(i);
            userDetailsMap.put(userDetails.getUserID(), userDetails);
        }
    }
    public List<Tasks> fetchTasksListByOppNo(String opportunityNumber, String userID) {
        List<Tasks> allOpportunityTasks = new ArrayList<Tasks>();
        List<Tasks> alltasksForOpportunity = new ArrayList<Tasks>();
        KLXAppModuleImpl am = null;
        String amDef = "com.klx.model.appModule.KLXAppModule";
        String config = "KLXAppModuleLocal";
        am = (KLXAppModuleImpl) Configuration.createRootApplicationModule(amDef, config);
        try {
            TaskQueryServiceWrapper wrapper = new TaskQueryServiceWrapper();
            ActiveDirectoryManagerImpl impl = new ActiveDirectoryManagerImpl();
//            User loggedInUser = impl.fetchUserDetailsById(userID);
//            Roles role = loggedInUser.getRoles();
//            List<String> userRole = role.getRole();
            //            String userRole = role.getRole().get(0);
            allOpportunityTasks = wrapper.fetchTasksListByOppNo(opportunityNumber, null);
            if (allOpportunityTasks.size() > 0) {
                for (Tasks task : allOpportunityTasks) {
                    Tasks oppTask = am.fetchOpportunityForTask(task.getOpportunityNumber());
                    task.setOpportunityId(oppTask.getOpportunityId());
                    task.setSalesMethod(oppTask.getSalesMethod());
                    task.setPricingManagerRep(oppTask.getPricingManagerRep());
                    task.setProposalManagerRep(oppTask.getProposalManagerRep());
                    task.setSalesRep(oppTask.getSalesRep());
                    task.setCustomerName(oppTask.getCustomerName());
                    task.setCustomerNo(oppTask.getCustomerNo());
                    task.setDealType(oppTask.getDealType());
                    task.setDealStructure(oppTask.getDealStructure());
                    task.setOscOppId(oppTask.getOscOppId());
                    task.setCustomerDueDate(oppTask.getCustomerDueDate());
                    task.setTeamNumber(oppTask.getTeamNumber());
                    task.setProjectType(oppTask.getProjectType());
                    task.setCustomerNoCrf(oppTask.getCustomerNoCrf());
                    if (!task.getOpportunityNumber().equals("0")) {
                        if (null != task.getAssignee()) {
                            User userDetails = impl.fetchUserDetailsById(task.getAssignee());
                            if (null != userDetails && null != userDetails.getName()) {
                                task.setAssigneeName(userDetails.getName());
                            } else {
                                task.setAssigneeName(null);
                            }
                        } else
                            task.setAssigneeName("");


                    }
                    alltasksForOpportunity.add(task);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Configuration.releaseRootApplicationModule(am, false);
        }
        //return (wrapper.fetchTasksListByOppNo(opportunityNumber));
        return alltasksForOpportunity;
    }

    public Users fetchLoggedInUserDetails(String userId) {

        Users userDetails = new Users();
        try {
            ActiveDirectoryManagerImpl impl = new ActiveDirectoryManagerImpl();
            User user = impl.fetchUserDetailsById(userId);
            userDetails.setUserID(user.getUserId());
            userDetails.setUserName(user.getName());
            userDetails.setUserGroup(user.getRoles()
                                         .getRole()
                                         .get(0));
            
            userDetails.setUserGroups(user.getRoles().getRole());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return userDetails;
    }

    public List<Approval> fetchApprovalTasksForSnapshot(String opportunityNumber, String snapshotId) {
        TaskQueryServiceWrapper wrapper = new TaskQueryServiceWrapper();
        return (wrapper.fetchApprovalTasksForSnapshot(opportunityNumber, snapshotId));
    }

    public ArrayList<String> fetchTaskDetails(String opportunityNumber, String userID, String groupId) {
        List<Tasks> allOpportunityTasks = new ArrayList<Tasks>();
        String taskID = null;
		String taskType = null;
          String ownerUser = "";
		ArrayList<String> taskDetails = new ArrayList<String>();
        try {
            TaskQueryServiceWrapper wrapper = new TaskQueryServiceWrapper();
            allOpportunityTasks = wrapper.fetchTasksListByEmailOppNo(opportunityNumber, userID, groupId);
            if (allOpportunityTasks.size() > 0) {
                for (Tasks task : allOpportunityTasks) {
                    if (!task.getOpportunityNumber().equals("0")) {
                        taskID = task.getTaskId();
                        taskType = task.getTaskType();
                                if(task.getOwnerUser() != null)
                                    ownerUser = task.getOwnerUser();
                                break; 
                    }
                }
				taskDetails.add(taskID);
                  taskDetails.add(taskType);
                  taskDetails.add(ownerUser);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //return (wrapper.fetchTasksListByOppNo(opportunityNumber));
        return taskDetails;
    }

    public String fetchOpportunityID(String opportunityNumber) {
        KLXAppModuleImpl am = null;
        String amDef = "com.klx.model.appModule.KLXAppModule";
        String config = "KLXAppModuleLocal";
        am = (KLXAppModuleImpl) Configuration.createRootApplicationModule(amDef, config);
        Tasks oppTask = am.fetchOpportunityForTask(opportunityNumber);
        return oppTask.getOpportunityId();
    }
    
    public Tasks fetchTaskDetailsByID(String opportunityNumber, String taskId){
        Tasks task = new Tasks();
        try {
            TaskQueryServiceWrapper wrapper = new TaskQueryServiceWrapper();
            task = wrapper.fetchTaskDetailsByID(opportunityNumber, taskId);
        }
        catch(Exception ae){
            ae.printStackTrace();
        }
        return task;
    }
    
    public Task fetchTaskDetailsByNumber(String taskNumber) {
        Task task = new Task();
        TaskQueryServiceWrapper wrapper = new TaskQueryServiceWrapper();
        task = wrapper.fetchTaskDetailsByNumber(taskNumber);
        return task;
    }

    public void setMyTaskFilterList(List<Users> myTaskFilterList) {
        this.myTaskFilterList = myTaskFilterList;
    }

    public List<Users> getMyTaskFilterList() {
        return myTaskFilterList;
    }

    public void setAssignedTaskFilterList(List<Users> assignedTaskFilterList) {
        this.assignedTaskFilterList = assignedTaskFilterList;
    }

    public List<Users> getAssignedTaskFilterList() {
        return assignedTaskFilterList;
    }
}
