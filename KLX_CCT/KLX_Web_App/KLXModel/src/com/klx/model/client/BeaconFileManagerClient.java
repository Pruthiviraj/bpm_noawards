package com.klx.model.client;

import com.klx.model.impl.BeaconFileManagerImpl;

import com.klx.services.entities.Quote;

import java.io.InputStream;
import java.io.OutputStream;

import java.util.HashMap;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class BeaconFileManagerClient {
    public BeaconFileManagerClient() {
        super();
    }
    
    public HashMap uploadBeaconFileData(String revisionChoice,InputStream inputStream,String quoteNumber,String quoteRevision,String quoteRevisionId, String opportunityNumber,HashMap inputParamMap, String partsType) {
        BeaconFileManagerImpl beaconFileManagerImpl=new BeaconFileManagerImpl();
        return beaconFileManagerImpl.uploadBeaconFileData(revisionChoice,inputStream,quoteNumber,quoteRevision,quoteRevisionId,opportunityNumber,inputParamMap, partsType);      
    }
    public HashMap uploadAwardsFileData(InputStream inputStream,HashMap inputParamMap,Object opptyid) {
        BeaconFileManagerImpl beaconFileManagerImpl=new BeaconFileManagerImpl();
        return beaconFileManagerImpl.uploadAwardsFileData(inputStream,inputParamMap,opptyid);      
    }
    
    public void downloadBeaconFileData(String queueId, String quoteRevisionId, String opportunityNumber) {
        BeaconFileManagerImpl beaconFileManagerImpl=new BeaconFileManagerImpl();
  //      return(beaconFileManagerImpl.downloadBeaconFileData(workbook,queueId,quoteRevisionId,opportunityNumber));      
    }
}
