package com.klx.model.client;

import com.klx.cct.task.v1.TaskT;
import com.klx.model.impl.ManageBpmTaskImpl;

public class ManageBpmTaskClient {
    public ManageBpmTaskClient() {
        super();
    }
    
    public String createTask(TaskT task){
        ManageBpmTaskImpl impl=new ManageBpmTaskImpl();
        return(impl.createTask(task));
    }
}
