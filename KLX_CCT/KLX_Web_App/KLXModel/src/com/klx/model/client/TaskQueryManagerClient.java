package com.klx.model.client;

import com.klx.model.impl.TaskQueryManagerImpl;
import com.klx.services.entities.Tasks;
import com.klx.services.entities.Users;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import oracle.bpel.services.workflow.task.model.Task;

public class TaskQueryManagerClient {
    public TaskQueryManagerClient() {
        super();
    }
    
    public List<Tasks> myTasksList=new ArrayList<Tasks>();
    public List<Tasks> assignedTaskList=new ArrayList<Tasks>();
    public List<Tasks> unassignedTaskList=new ArrayList<Tasks>();
    public List<Tasks> completedTaskList=new ArrayList<Tasks>();
    public List<Tasks> approvalTaskList = new ArrayList<Tasks>();
    private List<Users> myTaskFilterList = new ArrayList<Users>();
    private List<Users> assignedTaskFilterList = new ArrayList<Users>();

    public void setMyTaskFilterList(List<Users> myTaskFilterList) {
        this.myTaskFilterList = myTaskFilterList;
    }

    public List<Users> getMyTaskFilterList() {
        return myTaskFilterList;
    }

    public void setAssignedTaskFilterList(List<Users> assignedTaskFilterList) {
        this.assignedTaskFilterList = assignedTaskFilterList;
    }

    public List<Users> getAssignedTaskFilterList() {
        return assignedTaskFilterList;
    }

    public void setMyTasksList(List<Tasks> myTasksList) {
        this.myTasksList = myTasksList;
    }

    public List<Tasks> getMyTasksList() {
        return myTasksList;
    }

    public void setAssignedTaskList(List<Tasks> assignedTaskList) {
        this.assignedTaskList = assignedTaskList;
    }

    public List<Tasks> getAssignedTaskList() {
        return assignedTaskList;
    }

    public void setUnassignedTaskList(List<Tasks> unassignedTaskList) {
        this.unassignedTaskList = unassignedTaskList;
    }

    public List<Tasks> getUnassignedTaskList() {
        return unassignedTaskList;
    }
    
    public void loadTasksList(String username){
        TaskQueryManagerImpl impl=new TaskQueryManagerImpl();
        Map<String,List<Tasks>> tasksListMap=impl.loadTasksList(username);
        myTaskFilterList = impl.getMyTaskFilterList();
        assignedTaskFilterList = impl.getAssignedTaskFilterList();
        myTasksList=tasksListMap.get("myTaskList");
        assignedTaskList=tasksListMap.get("assignedTaskList");
        unassignedTaskList=tasksListMap.get("unassignedTaskList");
        approvalTaskList=tasksListMap.get("approvalTaskList");
    }
    
    public List<Tasks> fetchTasksListByOppNo(String opportunityNumber, String userID){
        TaskQueryManagerImpl impl=new TaskQueryManagerImpl();
          return(impl.fetchTasksListByOppNo(opportunityNumber, userID));
    }

    public Users fetchLoggedInUserDetails(String userId){
        TaskQueryManagerImpl impl=new TaskQueryManagerImpl();
          return(impl.fetchLoggedInUserDetails(userId));
    }
    
    public void loadCompleteTaskList(String username){
        TaskQueryManagerImpl impl=new TaskQueryManagerImpl();
        completedTaskList=impl.loadCompleteTaskList(username);
    }
    
    public Tasks fetchTaskDetailsById(String opportunityNumber, String taskId){
        TaskQueryManagerImpl impl=new TaskQueryManagerImpl();
        return (impl.fetchTaskDetailsByID(opportunityNumber, taskId));
    }

    public void setCompletedTaskList(List<Tasks> completedTaskList) {
        this.completedTaskList = completedTaskList;
    }

    public List<Tasks> getCompletedTaskList() {
        return completedTaskList;
    }


    public void setApprovalTaskList(List<Tasks> approvalTaskList) {
        this.approvalTaskList = approvalTaskList;
    }

    public List<Tasks> getApprovalTaskList() {
        return approvalTaskList;
    }
    
    public Task fetchTaskDetailsByNumber(String taskNumber){
        TaskQueryManagerImpl impl=new TaskQueryManagerImpl();
        return (impl.fetchTaskDetailsByNumber(taskNumber)); 
    }
}
