package com.klx.model.client;

import com.klx.cct.ad.user.v1.User;
import com.klx.cct.ad.user.v1.UsersList;
import com.klx.model.impl.ActiveDirectoryManagerImpl;
import com.klx.services.entities.Users;

import java.util.List;

public class ActiveDirectoryManagerClient {
    public ActiveDirectoryManagerClient() {
        super();
    }
    
    public User fetchUserDetailsById(String userId){
        ActiveDirectoryManagerImpl impl=new ActiveDirectoryManagerImpl();
        return (impl.fetchUserDetailsById(userId));
    }
    
    public List<Users> fetchUsersForRole(String role){
        ActiveDirectoryManagerImpl impl=new ActiveDirectoryManagerImpl();
        return (impl.fetchUsersForRole(role));
    }
}
