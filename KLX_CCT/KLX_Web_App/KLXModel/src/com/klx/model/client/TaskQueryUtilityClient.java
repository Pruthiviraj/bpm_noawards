package com.klx.model.client;
import com.klx.cct.ad.user.v1.User;
import com.klx.model.impl.TaskQueryUtilityImpl;
import com.klx.services.wrapper.TaskQueryUtilityWrapper;

public class TaskQueryUtilityClient {
    public TaskQueryUtilityClient() {
        super();
    }
    public User fetchUserForOpportunityRole(String opportunityNumber, String role){
           TaskQueryUtilityImpl impl=new TaskQueryUtilityImpl();
             return(impl.fetchUserForOpportunityRole(opportunityNumber,role));
       }
    public String reassignTask(String taskId, String reassignedUserId, String loggedInUser){
        TaskQueryUtilityWrapper impl=new TaskQueryUtilityWrapper();
        return impl.reassignTask(taskId, reassignedUserId, loggedInUser);
    }
}
