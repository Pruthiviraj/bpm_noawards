package com.klx.model.client;

import com.klx.model.impl.UpdateOpportunityManagerImpl;


import com.klx.services.wrapper.UpdateOpportunityServiceWrapper;

import java.math.BigDecimal;
import java.util.Date;

public class UpdateOpportunityManagerClient {
    public UpdateOpportunityManagerClient() {
        super();
    }
    
    public String updateOpportunityResource(String userId, String oscOppId,  String role){
        UpdateOpportunityManagerImpl impl=new UpdateOpportunityManagerImpl();
        return(impl.updateOpportunityResource(userId, oscOppId, role));
    }
    
    public String updateOpportunityData(String oscOppId,BigDecimal amount, BigDecimal netMargin, Boolean documentReadyToSubmit ){
        UpdateOpportunityManagerImpl impl=new UpdateOpportunityManagerImpl();
        return(impl.updateOpportunityData(oscOppId, amount, netMargin, documentReadyToSubmit));
    }
    
    public String customerDueDateChange(String oppNumber, String oppID, Date oldDuedate){
        UpdateOpportunityManagerImpl impl=new UpdateOpportunityManagerImpl();
        return (impl.customerDueDateChange(oppNumber, oppID, oldDuedate));
    }
    public String createAwardTask(String oppNumber, String oppID, String opptyOutcome){
        UpdateOpportunityManagerImpl impl=new UpdateOpportunityManagerImpl();
        return (impl.createAwardTask(oppNumber, oppID, opptyOutcome));
       
    }
}
