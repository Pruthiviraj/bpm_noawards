package com.klx.model.client;

import com.klx.model.impl.EmailUtilityImpl;

public class EmailUtilityClient {
    public EmailUtilityClient() {
        super();
    }
    
    public String sendEmailNotification(String source, String opptyNum, String oscId, String notfcnTemplateId){
        EmailUtilityImpl impl = new EmailUtilityImpl();
        return impl.sendEmailNotification(source, opptyNum, oscId, notfcnTemplateId);
    }
}
