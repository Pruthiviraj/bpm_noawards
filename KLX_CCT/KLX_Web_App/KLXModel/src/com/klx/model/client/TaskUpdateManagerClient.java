package com.klx.model.client;

import com.klx.model.impl.TaskUpdateManagerImpl;

import java.util.Date;

public class TaskUpdateManagerClient {
    public TaskUpdateManagerClient() {
        super();
    }
    
    public String assignTaskToUser(String username, String taskId){
       TaskUpdateManagerImpl impl=new TaskUpdateManagerImpl();
       return(impl.assignTaskToUser(username, taskId));
    }
    
    public String reassignTaskToUser(String username, String taskId){
        TaskUpdateManagerImpl impl=new TaskUpdateManagerImpl();
        return(impl.reassignTaskToUser(username, taskId));   
    }
    
    public String releaseTask(String username, String taskId){
        TaskUpdateManagerImpl impl=new TaskUpdateManagerImpl();
        return(impl.releaseTask(username, taskId));
    }
    
    public String closeTask(String username, String taskId, String outcome,String comments,String assignedUser){
        TaskUpdateManagerImpl impl=new TaskUpdateManagerImpl();
        return(impl.closeTask(username, taskId, outcome,comments,assignedUser));
    }
    
    public String updateTaskDueDate(String taskId, Date newDueDate){
        TaskUpdateManagerImpl impl=new TaskUpdateManagerImpl();
        return(impl.updateTaskDueDate(taskId, newDueDate));
    }
    
    public String approveOrRejectTask(String senderUserId, String taskNumber, String outcome,String comments, String approvalSource){
        TaskUpdateManagerImpl impl=new TaskUpdateManagerImpl();
        return(impl.approveOrRejectTask(senderUserId, taskNumber, outcome,comments,approvalSource));
    }
}
