package com.klx.model.appModule;


import com.klx.cct.ad.user.v1.Roles;
import com.klx.cct.ad.user.v1.User;
import com.klx.common.cache.ParamCache;
import com.klx.common.exception.BusinessException;
import com.klx.common.exception.SystemException;
import com.klx.common.logger.KLXLogger;
import com.klx.model.appModule.common.KLXAppModule;
import com.klx.model.client.UpdateOpportunityManagerClient;
import com.klx.model.constants.ModelConstants;
import com.klx.model.domain.common.CctApprovalType;
import com.klx.model.impl.ActiveDirectoryManagerImpl;
import com.klx.model.impl.TaskQueryManagerImpl;
import com.klx.model.viewObjects.AllQuotePartsSnapshotFinalVOImpl;
import com.klx.model.viewObjects.AwardHeadersVVOImpl;
import com.klx.model.viewObjects.AwardsQuotePartsVOImpl;
import com.klx.model.viewObjects.CctApprovalUsersEOVOImpl;
import com.klx.model.viewObjects.CctCrfAwardDatesVOImpl;
import com.klx.model.viewObjects.CctCrfCustomerInfoEOVOImpl;
import com.klx.model.viewObjects.CctCrfCustomerRequestEOVOImpl;
import com.klx.model.viewObjects.CctCrfDateMgmtVOImpl;
import com.klx.model.viewObjects.CctCrfEoVOImpl;
import com.klx.model.viewObjects.CctCrfKlxAwardVOImpl;
import com.klx.model.viewObjects.CctCrfKlxMasterContractVOImpl;
import com.klx.model.viewObjects.CctCrfKlxOfferEOVOImpl;
import com.klx.model.viewObjects.CctCrfOpptyDashboardVOImpl;
import com.klx.model.viewObjects.CctCrfRevisionVOImpl;
import com.klx.model.viewObjects.CctProgramCostHeaderEOVOImpl;
import com.klx.model.viewObjects.CctProgramCostLinesEOVOImpl;
import com.klx.model.viewObjects.CctProgramCostSummNotesVOImpl;
import com.klx.model.viewObjects.CctQuotePartsFinalVOImpl;
import com.klx.model.viewObjects.CctSalesHierarchyNewVOImpl;
import com.klx.model.viewObjects.CctSalesHierarchyVOImpl;
import com.klx.model.viewObjects.CctTop50ProdQueueVOImpl;
import com.klx.model.viewObjects.CreateTaskRolesVOImpl;
import com.klx.model.viewObjects.CrfCustomerMasterEOVOImpl;
import com.klx.model.viewObjects.DistinctQuoteRevVOImpl;

import com.klx.model.viewObjects.OpportunityEVOImpl;
import com.klx.model.viewObjects.ProgramCostEOVOImpl;
import com.klx.model.viewObjects.QuoteClassFinalVVOImpl;

import com.klx.model.viewObjects.QuoteClassVVOImpl;
import com.klx.model.viewObjects.QuotePartsFinalDownloadVOImpl;
import com.klx.model.viewObjects.QuotePartsQueueVOImpl;
import com.klx.model.viewObjects.QuotePartsVOImpl;

import com.klx.model.viewObjects.QuotePricingFinalVVOImpl;

import com.klx.model.viewObjects.QuotePricingVVOImpl;
import com.klx.model.viewObjects.QuoteQueueVVOImpl;
import com.klx.model.viewObjects.ReadOnlyOpportunityRVOImpl;
import com.klx.model.viewObjects.ReadOnlyOpportunityRVORowImpl;
import com.klx.model.viewObjects.SysdateVOImpl;
import com.klx.model.viewObjects.rovo.ApprovalStatusROVOImpl;
import com.klx.model.viewObjects.rovo.ApprovalUserROVOImpl;
import com.klx.model.viewObjects.rovo.CctApprovalCompareHeaderVOImpl;
import com.klx.model.viewObjects.rovo.CctApprovedComparedHeaderVOImpl;
import com.klx.model.viewObjects.rovo.CctFinalTop50PartsViewImpl;
import com.klx.model.viewObjects.rovo.CctFinalTop50ProdQueueViewImpl;
import com.klx.model.viewObjects.rovo.CctTop50FinalPricingSummaryImpl;
import com.klx.model.viewObjects.rovo.CctcrfSalesOwnerROVOImpl;
import com.klx.model.viewObjects.rovo.CostCompareHeaderVOImpl;
import com.klx.model.viewObjects.rovo.CostCompareLinesQuotePartsVOImpl;
import com.klx.model.viewObjects.rovo.CostCompareLinesVOImpl;
import com.klx.model.viewObjects.rovo.LatestRevisonROVOImpl;
import com.klx.model.viewObjects.rovo.SalesTeamROVOImpl;
import com.klx.model.viewObjects.rovo.UploadErrorROVOImpl;
import com.klx.model.viewObjects.rovo.ValidateSnapshotRVOImpl;
import com.klx.model.viewObjects.rovo.supportTeamROVOImpl;
import com.klx.model.viewObjects.trvo.ApprovalUsersTRVOImpl;
import com.klx.model.viewObjects.trvo.UploadErrorsTRVOImpl;
import com.klx.services.entities.Approval;
import com.klx.services.entities.Quote;
import com.klx.services.entities.Tasks;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import java.math.BigDecimal;
import java.math.RoundingMode;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import com.klx.common.exception.SystemException;
import com.klx.common.cache.ParamCache;

import com.klx.model.client.ActiveDirectoryManagerClient;
import com.klx.model.client.EmailUtilityClient;
import com.klx.model.impl.UpdateOpportunityManagerImpl;
import com.klx.model.viewObjects.CctApprovalUsersEOVORowImpl;
import com.klx.model.viewObjects.CctCrfAwardDatesVORowImpl;
import com.klx.model.viewObjects.CctCrfCustomerInfoEOVORowImpl;
import com.klx.model.viewObjects.CctCrfCustomerRequestEOVORowImpl;
import com.klx.model.viewObjects.CctCrfDateMgmtVORowImpl;
import com.klx.model.viewObjects.CctCrfEoVORowImpl;

import com.klx.model.viewObjects.CctCrfKlxOfferEOVORowImpl;
import com.klx.model.viewObjects.CctCrfKlxAwardVORowImpl;
import com.klx.model.viewObjects.CctCrfKlxMasterContractVORowImpl;
import com.klx.model.viewObjects.CctCrfRevisionVORowImpl;
import com.klx.model.viewObjects.CctSalesHierarchyNewVORowImpl;
import com.klx.model.viewObjects.CctSalesHierarchyVORowImpl;

import com.klx.model.viewObjects.CctTop50ProdQueueVORowImpl;
import com.klx.model.viewObjects.CrfCustomerMasterEOVORowImpl;

import com.klx.model.viewObjects.OpportunityEVORowImpl;
import com.klx.model.viewObjects.ProgramCostEOVORowImpl;
import com.klx.model.viewObjects.rovo.ApprovalUserROVORowImpl;
import com.klx.model.viewObjects.rovo.CctFinalTop50ProdQueueViewRowImpl;

import com.klx.model.viewObjects.rovo.supportTeamROVORowImpl;
import com.klx.model.viewObjects.trvo.ApprovalUsersTRVORowImpl;
import com.klx.services.proxy.taskProviderService.OpportunityPortType;
import com.klx.services.proxy.taskProviderService.OpportunityPortType_Service;
import com.klx.services.wrapper.UpdateOpportunityServiceWrapper;
import com.klx.xmlns.schema.opportunity.v1.Opportunity;
import com.klx.xmlns.schema.opportunity.v1.OpportunityHeaderInfoType;
import com.klx.xmlns.schema.opportunity.v1.Response;

import java.io.File;

import java.sql.JDBCType;

import java.util.Arrays;

import java.util.TreeMap;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;

import oracle.jbo.AttributeDef;
import oracle.jbo.AttributeHints;
import oracle.jbo.JboException;
import oracle.jbo.NameValuePairs;
import oracle.jbo.Row;
import oracle.jbo.RowSet;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewObject;
import oracle.jbo.server.ApplicationModuleImpl;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewAttributeDefImpl;
import oracle.jbo.server.ViewDefImpl;
import oracle.jbo.server.ViewLinkImpl;
import oracle.jbo.server.ViewObjectImpl;
import oracle.jbo.server.ViewRowImpl;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Sun Jun 18 03:56:28 EDT 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class KLXAppModuleImpl extends ApplicationModuleImpl implements KLXAppModule {
    /**
     * This is the default constructor (do not remove).
     */
    public KLXAppModuleImpl() {
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getStartTime() {
        return startTime;
    }
    private long startTime;
    private HashMap<String, String> opptyMapping = new HashMap<String, String>();
    private HashMap<String, String> templateMapping = new HashMap<String, String>();
    private HashMap<String, String> quoteVoAttrMapping = new HashMap<String, String>();
    private HashMap<String, String> quoteVoAttrDataMapping = new HashMap<String, String>();
    private HashMap<String, String> opptyVoAttrMapping = new HashMap<String, String>();
    private HashMap<String, String[]> downloadBeaconMapping = new HashMap<String, String[]>();
    private HashMap<String, String> allQuoteVoAttrMapping = new HashMap<String, String>();
    private HashMap<String, String> awardsVoAttrMapping = new HashMap<String, String>();
    private HashMap<String, String> tasksVoAttrMapping = new HashMap<String, String>();
    private HashMap<String, String> awardsReviewVoAttrMapping = new HashMap<String, String>();
    private HashMap<String, String[]> downloadAwardsReviewMapping = new HashMap<String, String[]>();
    private HashMap<String, String> mandatoryFiledMap = new HashMap<String, String>();
    private HashMap<String, String> formulaFiledMap = new HashMap<String, String>();
    private HashMap<String, String> manadotyFieldMap = new HashMap<String, String>();
    private HashMap<String, String> fieldPrecisionMap = new HashMap<String, String>();
    private HashMap<String, String[]> downloadCostComparisonMapping = new HashMap<String, String[]>();
    private HashMap<String, String> costComparisonLinesAttrMapping = new HashMap<String, String>();
    private HashMap<String, String[]> uploadAwardsMapping = new HashMap<String, String[]>();
    private HashMap<String, String> cctFinalTOP50AttrMapping = new HashMap<String, String>();
    private HashMap<String, String[]> downloadCctFinalTOP50Mapping = new HashMap<String, String[]>();
    private static KLXLogger logger = KLXLogger.getLogger();

    /**
     * Container's getter for OpportunityVO1.
     * @return OpportunityVO1
     */
    public ViewObjectImpl getOpportunityVO1() {
        return (ViewObjectImpl) findViewObject("OpportunityVO1");
    }

    public void getAwardsReviewVOAttr() {
        ViewAttributeDefImpl[] attributeDefImpl = getAwardsDownloadVO1().getViewAttributeDefImpls();
        for (ViewAttributeDefImpl viewAttrDef : attributeDefImpl) {
            awardsReviewVoAttrMapping.put(viewAttrDef.getColumnName(), viewAttrDef.getName());
        }
    }

    public void getCostComparisonLinesVOAttr() {
        ViewAttributeDefImpl[] attributeDefImpl = getCostCompareLinesQuotePartsVO1().getViewAttributeDefImpls();
        for (ViewAttributeDefImpl viewAttrDef : attributeDefImpl) {
            costComparisonLinesAttrMapping.put(viewAttrDef.getColumnName(), viewAttrDef.getName());
        }
    }
    
    public HashMap<String, com.klx.services.entities.Opportunity> fetchAllOpportunity(){        
        HashMap<String, com.klx.services.entities.Opportunity> opportunitiesListFromDB = new HashMap<String, com.klx.services.entities.Opportunity>();        
        try {
            //ViewObject vo = getOpportunityVO1();
            ViewObjectImpl oppVo = getOppROVO();                       
            oppVo.executeQuery();                    
            RowSetIterator oppDataSetIterator = oppVo.createRowSetIterator(null);            
           
            while(oppDataSetIterator.hasNext()) {  
                com.klx.services.entities.Opportunity opportunity = new com.klx.services.entities.Opportunity();                
                Row row = oppDataSetIterator.next();               
                opportunity.setOpportunityNumber(row.getAttribute("OpportunityNumber").toString());
                if (null != row.getAttribute("OpportunityId")) {
                    opportunity.setOpportunityId(row.getAttribute("OpportunityId").toString());
                }
                if (null != row.getAttribute(ModelConstants.SALES_METHOD)) {
                    opportunity.setSalesMethod(row.getAttribute(ModelConstants.SALES_METHOD).toString());
                }
                if (null != row.getAttribute(ModelConstants.SALES_REPRESENTATIVE)) {
                    opportunity.setSalesRep(row.getAttribute(ModelConstants.SALES_REPRESENTATIVE).toString());
                }
                if (null != row.getAttribute(ModelConstants.PRICING_MANAGER)) {
                    opportunity.setPricingManagerRep(row.getAttribute(ModelConstants.PRICING_MANAGER).toString());
                }
                if (null != row.getAttribute(ModelConstants.PROPOSAL_MANAGER)) {
                    opportunity.setProposalManagerRep(row.getAttribute(ModelConstants.PROPOSAL_MANAGER).toString());
                }
                if (null != row.getAttribute(ModelConstants.CUSTOMER_NAME)) {
                    opportunity.setCustomerName(row.getAttribute(ModelConstants.CUSTOMER_NAME).toString());
                }
                if (null != row.getAttribute(ModelConstants.CUSTOMER_NUMBER)) {
                    opportunity.setCustomerNo(row.getAttribute(ModelConstants.CUSTOMER_NUMBER).toString());
                }                

                if (null != row.getAttribute(ModelConstants.CUSTOMER_DUE_DATE)) {
                    Timestamp customerDueDate = (Timestamp) row.getAttribute(ModelConstants.CUSTOMER_DUE_DATE);
                    long milliseconds = customerDueDate.getTime() + (customerDueDate.getNanos() / 1000000);
                    opportunity.setCustomerDueDate(new java.util.Date(milliseconds));
                    //             Object value = new SimpleDateFormat("MM/dd/yyyy").format((Date)row.getAttribute("CustomerDueDate"));

                }
                if (null != row.getAttribute(ModelConstants.DEAL_TYPE)) {
                    opportunity.setDealType(row.getAttribute(ModelConstants.DEAL_TYPE).toString());
                }
                if (null != row.getAttribute(ModelConstants.DEAL_STRUCTURE)) {
                    opportunity.setDealStructure(row.getAttribute(ModelConstants.DEAL_STRUCTURE).toString());
                }
                if (null != row.getAttribute(ModelConstants.TEAM_NUMBER))
                    opportunity.setTeamNumber(row.getAttribute(ModelConstants.TEAM_NUMBER).toString());
                if (null != row.getAttribute(ModelConstants.PROJECT_TYPE))
                    opportunity.setProjectType(row.getAttribute(ModelConstants.PROJECT_TYPE).toString());
                if (null != row.getAttribute(ModelConstants.CONTRACT_TYPE))
                    opportunity.setContractType(row.getAttribute(ModelConstants.CONTRACT_TYPE).toString());
                if (null != row.getAttribute(ModelConstants.CUSTOMER_CRF_NO))
                    opportunity.setCustomerNoCrf(row.getAttribute(ModelConstants.CUSTOMER_CRF_NO).toString());
                 
                opportunitiesListFromDB.put(row.getAttribute("OpportunityNumber").toString(), opportunity);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "fetchOpportunityForTask", e.getMessage());
            throw new SystemException("CCT", "fetchOpportunityForTask", "CCT_BUS_EX01", e);

        }
        //logger.log(Level.INFO, getClass(), "fetchOpportunityForTask", "Exiting fetchOpportunityForTask method");
        return opportunitiesListFromDB;
    }

    public Tasks fetchOpportunityForTask(String opportunityNumber) {
        //logger.log(Level.INFO, getClass(), "fetchOpportunityForTask", "Entering fetchOpportunityForTask method");
        Tasks task = new Tasks();
        try {
            ViewObject vo = getOpportunityVO1();
            vo.setNamedWhereClauseParam("bind_oppNo", opportunityNumber);
            vo.executeQuery();
            Row[] rows = vo.getAllRowsInRange();
            if (rows.length > 0) {
                Row row = rows[0];
                task.setOpportunityNumber(opportunityNumber);
                if (null != row.getAttribute("OpportunityId")) {
                    task.setOpportunityId(row.getAttribute("OpportunityId").toString());
                }
                if (null != row.getAttribute(ModelConstants.SALES_METHOD)) {
                    task.setSalesMethod(row.getAttribute(ModelConstants.SALES_METHOD).toString());
                }
                if (null != row.getAttribute(ModelConstants.SALES_REPRESENTATIVE)) {
                    task.setSalesRep(row.getAttribute(ModelConstants.SALES_REPRESENTATIVE).toString());
                }
                if (null != row.getAttribute(ModelConstants.PRICING_MANAGER)) {
                    task.setPricingManagerRep(row.getAttribute(ModelConstants.PRICING_MANAGER).toString());
                }
                if (null != row.getAttribute(ModelConstants.PROPOSAL_MANAGER)) {
                    task.setProposalManagerRep(row.getAttribute(ModelConstants.PROPOSAL_MANAGER).toString());
                }
                if (null != row.getAttribute(ModelConstants.CUSTOMER_NAME)) {
                    task.setCustomerName(row.getAttribute(ModelConstants.CUSTOMER_NAME).toString());
                }
                if (null != row.getAttribute(ModelConstants.CUSTOMER_NUMBER)) {
                    task.setCustomerNo(row.getAttribute(ModelConstants.CUSTOMER_NUMBER).toString());
                }
                if (null != row.getAttribute(ModelConstants.OSC_OPPORTUNITY_ID)) {
                    task.setOscOppId(row.getAttribute(ModelConstants.OSC_OPPORTUNITY_ID).toString());
                }

                if (null != row.getAttribute(ModelConstants.CUSTOMER_DUE_DATE)) {
                    Timestamp customerDueDate = (Timestamp) row.getAttribute(ModelConstants.CUSTOMER_DUE_DATE);
                    long milliseconds = customerDueDate.getTime() + (customerDueDate.getNanos() / 1000000);
                    task.setCustomerDueDate(new java.util.Date(milliseconds));
                    //             Object value = new SimpleDateFormat("MM/dd/yyyy").format((Date)row.getAttribute("CustomerDueDate"));

                }
                if (null != row.getAttribute(ModelConstants.DEAL_TYPE)) {
                    task.setDealType(row.getAttribute(ModelConstants.DEAL_TYPE).toString());
                }
                if (null != row.getAttribute(ModelConstants.DEAL_STRUCTURE)) {
                    task.setDealStructure(row.getAttribute(ModelConstants.DEAL_STRUCTURE).toString());
                }
                if (null != row.getAttribute(ModelConstants.TEAM_NUMBER))
                    task.setTeamNumber(row.getAttribute(ModelConstants.TEAM_NUMBER).toString());
                if (null != row.getAttribute(ModelConstants.PROJECT_TYPE))
                    task.setProjectType(row.getAttribute(ModelConstants.PROJECT_TYPE).toString());
                if (null != row.getAttribute(ModelConstants.CONTRACT_TYPE))
                    task.setContractType(row.getAttribute(ModelConstants.CONTRACT_TYPE).toString());
                if (null != row.getAttribute(ModelConstants.CUSTOMER_CRF_NO))
                    task.setCustomerNoCrf(row.getAttribute(ModelConstants.CUSTOMER_CRF_NO).toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "fetchOpportunityForTask", e.getMessage());
            throw new SystemException("CCT", "fetchOpportunityForTask", "CCT_BUS_EX01", e);

        }
        //logger.log(Level.INFO, getClass(), "fetchOpportunityForTask", "Exiting fetchOpportunityForTask method");
        return task;
    }

    public void awardsTabRefresh(String opptyId) {
        BigDecimal awdHeaderId = getAwdHdrIdByOpptyId(opptyId);

        ViewObject awardsReviewVo = getAwardsReviewVO1();
        awardsReviewVo.setNamedWhereClauseParam("b_award_hdr_id", awdHeaderId);
        awardsReviewVo.setNamedWhereClauseParam("b_opp_id", opptyId);
        awardsReviewVo.executeQuery();

        ViewObject awardsHeadersVVo = getAwardHeadersVVO1();
        awardsHeadersVVo.setNamedWhereClauseParam("b_award_hdr_id", awdHeaderId);
        awardsHeadersVVo.setNamedWhereClauseParam("b_opp_id", opptyId);
        awardsHeadersVVo.executeQuery();

        /*ViewObject QAAobject = getBothQuoteAwardRVO();
        QAAobject.setNamedWhereClauseParam("b_award_hdr_id", awdHeaderId);
        QAAobject.executeQuery();

        ViewObject ANQobject = getAwardedNotQuotedRVO();
        ANQobject.setNamedWhereClauseParam("b_award_hdr_id", awdHeaderId);
        ANQobject.executeQuery();*/

        ViewObject QNAobject = getQuotedNotAwardedRVO();
        QNAobject.setNamedWhereClauseParam("b_award_hdr_id", awdHeaderId);
        QNAobject.setNamedWhereClauseParam("b_opp_id", opptyId);
        QNAobject.executeQuery();

    }

    public List<Quote> fetchOpportunityDetails(String opportunityNumber, String opportunityId) {

        ViewObject vo = getOpportunityVO1();
        vo.setNamedWhereClauseParam("bind_oppNo", opportunityNumber);
        vo.executeQuery();
        logger.log(Level.INFO, getClass(), "fetchOpportunityDetails", "opp id------------------" + opportunityId);
        try {
            logger.log(Level.INFO, getClass(), "fetchOpportunityDetails", "Entering fetchOpportunityDetails method");


            BigDecimal awdHeaderId = getAwdHdrIdByOpptyId((Object) opportunityId);

            ViewObject awardsReviewVo = getAwardsReviewVO1();
            awardsReviewVo.setNamedWhereClauseParam("b_award_hdr_id", awdHeaderId);
            awardsReviewVo.executeQuery();

            ViewObject awardsHeadersVVo = getAwardHeadersVVO1();
            awardsHeadersVVo.setNamedWhereClauseParam("b_award_hdr_id", awdHeaderId);
            awardsHeadersVVo.executeQuery();

            ViewObject QAAobject = getBothQuoteAwardRVO();
            QAAobject.setNamedWhereClauseParam("b_award_hdr_id", awdHeaderId);
            QAAobject.executeQuery();

            ViewObject ANQobject = getAwardedNotQuotedRVO();
            ANQobject.setNamedWhereClauseParam("b_award_hdr_id", awdHeaderId);
            ANQobject.executeQuery();

            ViewObject QNAobject = getQuotedNotAwardedRVO();
            QNAobject.setNamedWhereClauseParam("b_award_hdr_id", awdHeaderId);
            QNAobject.executeQuery();

            AllQuotePartsSnapshotFinalVOImpl allQuotePartsSnapshotFinalVO1 = getAllQuotePartsSnapshotFinalVO1();
            allQuotePartsSnapshotFinalVO1.setNamedWhereClauseParam("pOppNumber", opportunityNumber);
            allQuotePartsSnapshotFinalVO1.executeQuery();

            ViewObject programCostEoVo = getProgramCostEOVO1();
            programCostEoVo.reset();
            programCostEoVo.setNamedWhereClauseParam("b_opp_id", opportunityId);
            programCostEoVo.executeQuery();

        } catch (Exception e) {
            //e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "fetchOpportunityDetails", e.getMessage());
            throw new SystemException("CCT", "fetchOpportunityDetails", "CCT_SYS_EX01", e);
        }
        List<Quote> quoteList = null;
        try {
            currentQuoteRevisionID(opportunityNumber);

            finalQuoteRevisionID(opportunityNumber);

            Row[] rows = vo.getAllRowsInRange();
            Quote quote = getOpportunityQuoteRevison(opportunityNumber);
            if (null != quote && null != quote.getQuoteRevisionId())
                getPartsQueues(new BigDecimal(quote.getQuoteRevisionId()));
            setOppNotesBindVar(opportunityNumber);
            quoteList = new ArrayList<Quote>();
            quoteList.add(quote);
            if (null != opportunityId)
                fetchSnapshots(new Integer(opportunityId), opportunityNumber);  
        
            //updatesupportTeam(opportunityNumber);
            selectDropDownValues(new BigDecimal(opportunityId));
            logger.log(Level.INFO, getClass(), "fetchOpportunityDetails", "Exiting fetchOpportunityDetails method");
        } catch (Exception nfe) {
            //nfe.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "fetchOpportunityDetails", nfe.getMessage());
            throw new SystemException("CCT", "fetchOpportunityDetails", "CCT_SYS_EX01", nfe);
        }

        return quoteList;
    }

    public void showCustPNHistory(String custPN, String oppNo) {
        ViewObject vo = getCustomerPNHistoryVO1();
        vo.setNamedWhereClauseParam("b_CustPN", custPN);
        vo.setNamedWhereClauseParam("b_OppNo", oppNo);
        vo.executeQuery();
        logger.log(Level.INFO, getClass(), "showCustPartNoHistoryAMImpl", "VO executed ");
    }

    public void setTaskCommentsBindVar(String taskNumber) {
        ViewObject vo = getTaskCommentsVO1();
        vo.setNamedWhereClauseParam("bind_taskNo", taskNumber);
        vo.executeQuery();
    }

    public void fetchTaskComments(String taskID) {
        ViewObject taskCommentsVO = getTaskCommentsVO1();
        taskCommentsVO.reset();
        taskCommentsVO.setWhereClause("TaskId = '" + taskID + "'");
        taskCommentsVO.executeQuery();
    }


    /**
     * Container's getter for TaskCommentsVO1.
     * @return TaskCommentsVO1
     */
    public ViewObjectImpl getTaskCommentsVO1() {
        return (ViewObjectImpl) findViewObject("TaskCommentsVO1");
    }

    public void insertTaskComments(String taskId, String taskNumber, String comment, String username) {
        logger.log(Level.INFO, getClass(), "insertTaskComments", "Entering insertTaskComments method");

        ViewObjectImpl taskCommentsVo = this.getTaskCommentsVO1();
        Row r = taskCommentsVo.createRow();
        r.setAttribute("TaskId", taskId);
        r.setAttribute("TaskNumber", taskNumber);
        r.setAttribute("UserComments", comment);
        r.setAttribute("UserName", username);

        this.getDBTransaction().commit();
        taskCommentsVo.executeQuery();
    }

    public HashMap generateAwardsReviewDownloadTemplate(String pMappingKey) {
        HashMap tempHashMap = new HashMap();
        getAwardsReviewVOAttr();
        ViewObjectImpl cctMappingsROVO = getCctMappingsROVO();
        cctMappingsROVO.setNamedWhereClauseParam("pMappingKey", pMappingKey);
        cctMappingsROVO.executeQuery();
        RowSetIterator createRowSetIterator = cctMappingsROVO.createRowSetIterator(null);
        String[] arrStr = null;
        while (createRowSetIterator.hasNext()) {
            Row row = createRowSetIterator.next();
            arrStr = new String[2];
            arrStr[0] = (String) row.getAttribute("DestinationField");
            arrStr[1] = (String) row.getAttribute("DataFormat");
            if (arrStr[1] == null) {
                arrStr[1] = "";
            }
            downloadAwardsReviewMapping.put((String) row.getAttribute("SourceField"), arrStr);
            logger.log(Level.INFO, getClass(), "generateAwardsReviewDownloadTemplate",
                       "Source Field :-- " + row.getAttribute("SourceField"));


        }
        createRowSetIterator.closeRowSetIterator();
        tempHashMap.put("downloadAwardsReviewMapping", downloadAwardsReviewMapping);
        tempHashMap.put("awardsReviewVoAttrMapping", awardsReviewVoAttrMapping);
        return tempHashMap;
    }

    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public String getPartsQueues(BigDecimal quoteRevisionId) {
        logger.log(Level.INFO, getClass(), "getPartsQueues", "Entering getPartsQueues method");
        getFinalQuoteParts(getCctQuotePartsFinalVO1(), quoteRevisionId);
        getAllPartsDownload(getAllPartsDownloadVO1(), quoteRevisionId);
        fetchPartsQueues(getAllQuotePartsVO1(), BigDecimal.ZERO, quoteRevisionId);
        fetchPartsQueues(getHPPQuotePartsVO1(), new BigDecimal("1"), quoteRevisionId);
        fetchPartsQueues(getChemicalQuotePartsVO1(), new BigDecimal("2"), quoteRevisionId);
        fetchPartsQueues(getLightingQuotePartsVO1(), new BigDecimal("3"), quoteRevisionId);
        fetchPartsQueues(getLightingQuotePartsVO1(), new BigDecimal("10"), quoteRevisionId);
        fetchPartsQueues(getStrategicPricingQuotePartsVO1(), new BigDecimal("4"), quoteRevisionId);
        fetchPartsQueues(getAdvancedSourcingQuotePartsVO1(), new BigDecimal("5"), quoteRevisionId);
        fetchPartsQueues(getUnknownQuotePartsVO1(), new BigDecimal("6"), quoteRevisionId);
        fetchPartsQueues(getCommodityQuotePartsVO1(), new BigDecimal("7"), quoteRevisionId);
        fetchPartsQueues(getNoBidQuotePartsVO1(), new BigDecimal("8"), quoteRevisionId);
        fetchPartsQueues(getKittingQuotePartsVO1(), new BigDecimal("9"), quoteRevisionId);
        return "partsQueue";
    }

    private void getAllPartsDownload(ViewObjectImpl AllPartsDownload, BigDecimal quoteRevisionId) {
        AllPartsDownload.setNamedWhereClauseParam("bind_quoteRevisionId", quoteRevisionId);
        AllPartsDownload.executeQuery();
       
    }

    private void getFinalQuoteParts(ViewObjectImpl finalPartsQueues, BigDecimal quoteRevisionId) {
        finalPartsQueues.setNamedWhereClauseParam("bind_QuoteRevisionId", quoteRevisionId);
        finalPartsQueues.executeQuery();
    }

    private void fetchPartsQueues(ViewObjectImpl partsQueues, BigDecimal queueId, BigDecimal quoteRevisionId) {
        try {
            //
            if (BigDecimal.ZERO != queueId) {
                partsQueues.setNamedWhereClauseParam("bind_queueId", queueId);
            }
            //
            partsQueues.setNamedWhereClauseParam("bind_quoteRevisionId", quoteRevisionId);
            //
            partsQueues.executeQuery();
            //
        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "fetchPartsQueues", e.getMessage());
            throw new SystemException("CCT", "fetchPartsQueues", "CCT_SYS_EX01", e);
        }
    }


    public void movePartsToQueues(String fromQueueId, String toQueueId, String opportunityNumber) {
        logger.log(Level.INFO, getClass(), "movePartsToQueues", "Entering movePartsToQueues method");

        ViewObjectImpl vo = null;
        BigDecimal queueId = new BigDecimal(toQueueId);

        if ("0".equalsIgnoreCase(fromQueueId)) {
            vo = getAllQuotePartsVO1();
        } else if ("1".equalsIgnoreCase(fromQueueId)) {
            vo = getHPPQuotePartsVO1();
        } else if ("2".equalsIgnoreCase(fromQueueId)) {
            vo = getChemicalQuotePartsVO1();
        } else if ("3".equalsIgnoreCase(fromQueueId)) {
            vo = getLightingQuotePartsVO1();
        } else if ("4".equalsIgnoreCase(fromQueueId)) {
            vo = getStrategicPricingQuotePartsVO1();
        } else if ("5".equalsIgnoreCase(fromQueueId)) {
            vo = getAdvancedSourcingQuotePartsVO1();
        } else if ("6".equalsIgnoreCase(fromQueueId)) {
            vo = getUnknownQuotePartsVO1();
        } else if ("7".equalsIgnoreCase(fromQueueId)) {
            vo = getCommodityQuotePartsVO1();
        } else if ("8".equalsIgnoreCase(fromQueueId)) {
            vo = getNoBidQuotePartsVO1();
        } else if ("9".equalsIgnoreCase(fromQueueId)) {
            vo = getKittingQuotePartsVO1();
        }
        LatestRevisonROVOImpl latestRevisonROVO = getLatestRevisonROVO();
        latestRevisonROVO.setpOpportunityNumber(opportunityNumber);
        latestRevisonROVO.executeQuery();
        RowSetIterator createRowSetIterator = null;

        String quoteRevisonID = "null";

        createRowSetIterator = latestRevisonROVO.createRowSetIterator(null);
        while (createRowSetIterator.hasNext()) {
            Row row = createRowSetIterator.next();
            quoteRevisonID = row.getAttribute("RevisionId").toString();
        }
        createRowSetIterator.closeRowSetIterator(); //Need to close rowset iterator.
        movePartsQueue(vo, queueId);
        BigDecimal revisionId = new BigDecimal(quoteRevisonID);
        getPartsQueues(revisionId);
        logger.log(Level.INFO, getClass(), "movePartsToQueues", "Exiting movePartsToQueues method");

    }


    public void movePartsQueue(ViewObjectImpl partsQueues, BigDecimal queueId) {
        Row[] rows = partsQueues.getFilteredRows("Selected", "true");
        // java.util.Date date = new java.util.Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
        java.util.Date date = new java.util.Date();
        String datetime = ModelConstants.PART_MOVED_ON + sdf.format(date);

        for (Row row : rows) {
            row.setAttribute("QueueId", queueId);
            row.setAttribute("Status", datetime);
        }
        getDBTransaction().commit();
    }


    /**
     * Container's getter for QuoteVO1.
     * @return QuoteVO1
     */
    public ViewObjectImpl getQuoteVO1() {
        return (ViewObjectImpl) findViewObject("QuoteVO1");
    }


    /**
     * Container's getter for QuotePartsVO1.
     * @return QuotePartsVO1
     */
    public QuotePartsVOImpl getChemicalQuotePartsVO1() {
        return (QuotePartsVOImpl) findViewObject("ChemicalQuotePartsVO1");
    }


    /**
     * Container's getter for QuotePartsQueueVO2.
     * @return QuotePartsQueueVO2
     */
    public QuotePartsQueueVOImpl getQuotePartsQueueVO2() {
        return (QuotePartsQueueVOImpl) findViewObject("QuotePartsQueueVO2");
    }

    /**
     * Container's getter for QuoteRevisionVO1.
     * @return QuoteRevisionVO1
     */
    public ViewObjectImpl getQuoteRevisionVO1() {
        return (ViewObjectImpl) findViewObject("QuoteRevisionVO1");
    }


    /**
     * Container's getter for QuotePartsQueueVO1.
     * @return QuotePartsQueueVO1
     */
    public QuotePartsQueueVOImpl getQuotePartsQueueVO1() {
        return (QuotePartsQueueVOImpl) findViewObject("QuotePartsQueueVO1");
    }


    /**
     * Container's getter for QuotePricingVVO1.
     * @return QuotePricingVVO1
     */
    public QuotePricingVVOImpl getQuotePricingVVO1() {
        return (QuotePricingVVOImpl) findViewObject("QuotePricingVVO1");
    }

    /**
     * Container's getter for QuoteQueueVVO1.
     * @return QuoteQueueVVO1
     */
    public QuoteQueueVVOImpl getQuoteQueueVVO1() {
        return (QuoteQueueVVOImpl) findViewObject("QuoteQueueVVO1");
    }

    /**
     * Container's getter for QuoteClassVVO1.
     * @return QuoteClassVVO1
     */
    public QuoteClassVVOImpl getQuoteClassVVO1() {
        return (QuoteClassVVOImpl) findViewObject("QuoteClassVVO1");
    }

    public ArrayList<BigDecimal> returnAwardIDs(BigDecimal opptyid) {
        ArrayList<BigDecimal> awardHeaderIDs = new ArrayList<BigDecimal>();
        ViewObjectImpl AwardHeaderVO = getCctAwardHeaderVO1();
        ViewCriteria criteria = AwardHeaderVO.getViewCriteria("CctAwardHeaderVOCriteria");
        //AwardHeaderVO.setNamedWhereClauseParam("b_sourceOppty", opptyid);
        AwardHeaderVO.setNamedWhereClauseParam("b_opptyId", opptyid);
        AwardHeaderVO.applyViewCriteria(criteria);
        AwardHeaderVO.setOrderByClause("AWARD_HEADER_ID desc");
        AwardHeaderVO.executeQuery();
        BigDecimal awdHeaderId = null;
        RowSetIterator awdHeaderIterator = AwardHeaderVO.createRowSetIterator(null);
        int i = 0;
        while (awdHeaderIterator.hasNext()) {
            Row awardHeaderRow = awdHeaderIterator.next();
            awdHeaderId = new BigDecimal(awardHeaderRow.getAttribute("AwardHeaderId").toString());
           
            awardHeaderIDs.add(awdHeaderId);
        }

        awdHeaderIterator.closeRowSetIterator();
        return awardHeaderIDs;
    }

    public void deleteAwardsRows(Object opptyid) {
        BigDecimal opptyId = (BigDecimal) opptyid;
        ArrayList<BigDecimal> awardHeaderIDs = new ArrayList<BigDecimal>();
        awardHeaderIDs = returnAwardIDs(opptyId);
        BigDecimal awdHeaderId = null;
        for (int j = 0; j < awardHeaderIDs.size(); j++) {
            awdHeaderId = awardHeaderIDs.get(j);

            /*To remove duplicate rows from CC_AWARDS table*/
            ViewObjectImpl cctAwardsVO = getCctAwardsVO1();
            ViewCriteria criteria2 = cctAwardsVO.getViewCriteria("DeleteDuplicateAwardRows");
            cctAwardsVO.setNamedWhereClauseParam("b_awdHeaderId", awdHeaderId);
            // cctAwardsVO.setNamedWhereClauseParam("b_oppty_id", opptyid);
            cctAwardsVO.applyViewCriteria(criteria2);
            cctAwardsVO.executeQuery();
            RowSetIterator createRowSetIterator = cctAwardsVO.createRowSetIterator(null);
            while (createRowSetIterator.hasNext()) {
                ViewRowImpl currentRow = (ViewRowImpl) createRowSetIterator.next();
                BigDecimal deletingRowAwardId = (BigDecimal) currentRow.getAttribute("AwardHeaderId");
               
                EntityImpl entityImpl = (EntityImpl) currentRow.getEntity(0);
                if (EntityImpl.STATUS_NEW != entityImpl.getEntityState()) {
                    currentRow.remove();
                    
                }
            }
            /*To remove duplicate rows from CC_AWARD_HEADERS table*/
            ViewObjectImpl cctAwardsHeaderVo = getCctAwardHeaderVO1();
            ViewCriteria criteria1 = cctAwardsHeaderVo.getViewCriteria("DeleteDuplicateRowsUsingAwdHdrIdCriteria");
            cctAwardsHeaderVo.setNamedWhereClauseParam("b_awardHeaderId", awdHeaderId);
            // cctAwardsHeaderVo.setNamedWhereClauseParam("b_opptyId", opptyid);
            cctAwardsHeaderVo.applyViewCriteria(criteria1);
           
            cctAwardsHeaderVo.executeQuery();
            RowSetIterator createRowSetIterator1 = cctAwardsHeaderVo.createRowSetIterator(null);


            while (createRowSetIterator1.hasNext()) {
                if (j == 0) {
                    ViewRowImpl firstRow = (ViewRowImpl) createRowSetIterator1.next();

                } else {

                    ViewRowImpl currentAwardsRow = (ViewRowImpl) createRowSetIterator1.next();
                    BigDecimal deletingRowAwardId = (BigDecimal) currentAwardsRow.getAttribute("AwardHeaderId");
                  
                    EntityImpl entityImpl = (EntityImpl) currentAwardsRow.getEntity(0);
                    if (EntityImpl.STATUS_NEW != entityImpl.getEntityState()) {
                        currentAwardsRow.remove();
                       
                    }
                }
            }
            awdHeaderId = null;

            createRowSetIterator.closeRowSetIterator();
            createRowSetIterator1.closeRowSetIterator();
            /* try {
                ResultSet rscah = stmt2.executeQuery();
            } catch (SQLException e) {
            }*/
        }
       
    }

    public BigDecimal getMaxAwardHeaderId() {
        String query = "select CCT_AWARD_HDR_SEQ.nextval from dual";
        PreparedStatement stmt = getDBTransaction().createPreparedStatement(query, 0);
        ResultSet resultset = null;
        BigDecimal retoppty = null;
        try {
          
            resultset = stmt.executeQuery();
            
            while (resultset.next()) {
                retoppty = resultset.getBigDecimal(1);
            }
            //            if (retoppty == null){
            //            retoppty = new BigDecimal(0) ;}
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                resultset.close();
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return retoppty;
    }

    public BigDecimal getAwardHeaderId(Object opptyid) {
        String strVal = (String) opptyid.toString();
        BigDecimal opptyId = new BigDecimal(strVal);
        String s = "select max(AWARD_HEADER_ID) as MAX from CCT_AWARD_HEADERS where SOURCE_OPPORTUNITY_ID = " + opptyId;
        // String s="select 1 from dual";
        PreparedStatement stmt = getDBTransaction().createPreparedStatement(s, 0);
        // PreparedStatement stmt =getDBTransaction().createPreparedStatement("" + s, 0);
        ResultSet resultset = null;
        BigDecimal retoppty = null;
        try {
            // stmt.setBigDecimal(1, opptyId);
          
            resultset = stmt.executeQuery();
          
            while (resultset.next()) {
                retoppty = resultset.getBigDecimal(1);
            }
        } catch (SQLException ex) {
            //ex.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "getAwardHeaderId", ex.getMessage());
            throw new SystemException("CCT", "getAwardHeaderId", "CCT_SYS_EX10", ex);
        } finally {
            try {
                resultset.close();
                stmt.close();
            } catch (SQLException e) {
                //e.printStackTrace();
                logger.log(Level.SEVERE, getClass(), "getAwardHeaderId", e.getMessage());
                throw new SystemException("CCT", "getAwardHeaderId", "CCT_SYS_EX10", e);
            }
        }
        return retoppty;
    }

    public HashMap insertAwards(Sheet datatypeSheet, Object opptyid) {
        HashMap awardsMap = new HashMap();
        generateAwardsUploadTemplate(ModelConstants.AWARDS_FILE_UPLOAD);
        awardsMap.put(ModelConstants.UPLOAD_STATUS, "unsuccessful");
        ViewObject cctAwardsVO = getCctAwardsVO1();
        String uploadFailureReason = "";
        int rowcounter = 1;
        boolean dataTypeException = false; //for Invalid data on beacon File 
        try {
            startTime = System.currentTimeMillis();

            deleteAwardsRows(opptyid);
            logger.log(Level.INFO, getClass(), "insertAwards", "Delete Time Start= " + startTime);
            logger.log(Level.INFO, getClass(), "insertAwards", "Delete Time End= " + System.currentTimeMillis());
            BigDecimal awdHeaderId = getAwardHeaderId(opptyid);
            Iterator<org.apache.poi.ss.usermodel.Row> iterator = datatypeSheet.iterator();
            iterator.next(); //skip first row
            rowcounter++;
            while (iterator.hasNext()) { // loopone
                org.apache.poi.ss.usermodel.Row currentRow = iterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();
                oracle.jbo.Row createRow = cctAwardsVO.createRow();
                while (cellIterator.hasNext()) { //loop two
                    Cell currentCell = cellIterator.next();

                    org.apache.poi.ss.usermodel.Row headerRow = datatypeSheet.getRow(0);
                    Cell cell = headerRow.getCell(currentCell.getColumnIndex());
                    if (null != cell) {
                        String header = cell.getStringCellValue();
                        Object objHeadAndDataType = uploadAwardsMapping.get(header);

                        if (objHeadAndDataType != null) {
                            String voAttribute = awardsVoAttrMapping.get(uploadAwardsMapping.get(header)[0]);
                            String dataType = uploadAwardsMapping.get(header)[1];
                            String attributeDataType = uploadAwardsMapping.get(header)[1];
                            //logger.log(Level.INFO, getClass(), "insertAwards", "VoAttribute = " + voAttribute + ",Data Type = " + dataType);
                            Object value = null;
                            if (null != voAttribute) {
                                if(attributeDataType.equals("NUMBER")){
                                    BigDecimal cellTYpeIntValue = BigDecimal.ZERO;
                                    if(currentCell.getCellTypeEnum() == CellType.STRING){
                                        if(!currentCell.getStringCellValue().equals("")){
                                            try{
                                                cellTYpeIntValue = BigDecimal.valueOf(Double.valueOf(currentCell.getStringCellValue()));
                                            }
                                            catch(Exception ae){                                            
                                                uploadFailureReason = "Incorrect data types detected at: {Column: "+header+", Row No: "+rowcounter+"}";
                                                awardsMap.put("uploadStatus", "unsuccessful");
                                                awardsMap.put("uploadFailureReason", uploadFailureReason);
                                                //quoteMap.put("incorrectQueueName", incorrectQueueName);
                                                dataTypeException = true;
                                                break;
                                            }
                                        }
                                    }
                                    else if(currentCell.getCellTypeEnum() == CellType.FORMULA){
                                       // if(!header.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[0])){
                                            currentCell.getCellFormula();
                                            switch (currentCell.getCachedFormulaResultTypeEnum()) {
                                            case NUMERIC:
                                                cellTYpeIntValue = BigDecimal.valueOf(currentCell.getNumericCellValue());
                                                break;
                                            
                                            case STRING:
                                                if(!currentCell.getStringCellValue().equals("")){
                                                    uploadFailureReason = "Incorrect data types detected at: {Column: "+header+", Row No: "+rowcounter+"}";
                                                    awardsMap.put(ModelConstants.UPLOAD_STATUS, "unsuccessful");
                                                    awardsMap.put("uploadFailureReason", uploadFailureReason);
                                                    //awardsMap.put("incorrectQueueName", incorrectQueueName);
                                                    dataTypeException = true;                                            
                                                    break;
                                                }
                                            }
                                        //}
                                    }
                                    cellTYpeIntValue = null;
                                }                           
                                switch (currentCell.getCellTypeEnum()) {
                                case STRING:
                                    value = currentCell.getStringCellValue();
                                    break;
                                case NUMERIC:
                                    value = currentCell.getNumericCellValue();
                                    //API sometimes returns double value very long lik 98.999999999999999 to mitigate this issue the follwing code is written
                                    value = (Math.round((((Double) value).doubleValue()) * 100000.0)) / 100000.0;
                                    double d = ((Double) value).doubleValue();
                                    // double d1r= (Math.round(d*100000.0))/100000.0;
                                    //If datatype is TEXT then ensure that whole number goes into DB without decimal
                                    if ("TEXT".equals(dataType)) {
                                        if ((d % 1) == 0) {
                                            value = String.valueOf((int) d);
                                        }

                                    }
                                    break;
                                case FORMULA:
                                    switch (currentCell.getCachedFormulaResultTypeEnum()) {
                                    case NUMERIC:
                                        value = currentCell.getNumericCellValue();
                                        //API sometimes returns double value very long lik 98.999999999999999 to mitigate that issue the follwing code is written
                                        value = (Math.round((((Double) value).doubleValue()) * 100000.0)) / 100000.0;
                                        double d1 = currentCell.getNumericCellValue();
                                        //If datatype is TEXT then ensure that whole number goes into DB without decimal
                                        if ("TEXT".equals(dataType)) {
                                            if ((d1 % 1) == 0) {
                                                value = String.valueOf((int) d1);
                                            }

                                        }

                                        break;

                                    case STRING:
                                        value = currentCell.getStringCellValue();
                                        break;
                                    }
                                    break;

                                }

                                createRow.setAttribute(voAttribute, value);
                            }
                        }
                    }
                }
                if(dataTypeException)
                    break;
                createRow.setAttribute("AwardHeaderId", awdHeaderId);
                cctAwardsVO.insertRow(createRow);
                rowcounter++;
            } // end of while loop
            if(!dataTypeException){                
                awardsMap.put(ModelConstants.UPLOAD_STATUS, "successful");
                awardsMap.put("uploadFailureReason", uploadFailureReason);
            } 
            awardsMap.put("dataTypeException", dataTypeException);
        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "insertAwards", e.getMessage());
            throw new SystemException("CCT", "insertAwards", "CCT_SYS_EX01", e);
        }

        return awardsMap;
    }

    public HashMap insertAwardsFileInDB(InputStream inputStream, HashMap inputParamMap, Object opptyid) {
        HashMap hashMap = new HashMap();
        String strVal = (String) opptyid.toString();
        BigDecimal opptyId = new BigDecimal(strVal);
        String uploadType = (String) inputParamMap.get(ModelConstants.UPLOAD_TYPE);
       
        String sheetName = (String) inputParamMap.get("sheetName");
        try {
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

            Sheet datatypeSheet = null;
            if (null != sheetName)
                datatypeSheet = workbook.getSheet(sheetName);
            else
                datatypeSheet = workbook.getSheetAt(0);

            if ("UPDATE".equalsIgnoreCase(uploadType)) {
                hashMap = updateAwards(datatypeSheet, opptyid);

                String status = (String) hashMap.get(ModelConstants.UPLOAD_STATUS);
                boolean dataTypeException = Boolean.parseBoolean(hashMap.get("dataTypeException").toString());   
                if ("successful".equalsIgnoreCase(status)) {
                    getDBTransaction().commit();
                    updateAwardHeadersProc(opptyId);
                    logger.log(Level.INFO, getClass(), "updateAwardsFileInDB", "Commit success");
                }
                else if("unsuccessful".equalsIgnoreCase(status) && !dataTypeException)                   
                   clearAwardsCache();               
                else {
                    clearAwardsCache();
                }
            } else if ("CREATE".equalsIgnoreCase(uploadType)) {
                hashMap = insertAwards(datatypeSheet, opptyid);
                String status = (String) hashMap.get(ModelConstants.UPLOAD_STATUS);
                boolean dataTypeException = Boolean.parseBoolean(hashMap.get("dataTypeException").toString()); 
                if ("successful".equalsIgnoreCase(status)) {
                    getDBTransaction().commit();
                    BigDecimal awdHeaderId = getAwardHeaderId(opptyid);
                    //                   updateAwardsReviewProc(awdHeaderId);
                    updateAwardHeadersProc(opptyId);
                    logger.log(Level.INFO, getClass(), "insertAwardsFileInDB", "Commit success");

                }                
                else if("unsuccessful".equalsIgnoreCase(status) && !dataTypeException)                   
                   clearAwardsCache(); 
                else
                    clearAwardsCache();
            }

        } catch (FileNotFoundException fexcep) {
            fexcep.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "insertAwardsFileInDB", fexcep.getMessage());
            getDBTransaction().rollback();
            throw new SystemException("CCT", "insertAwardsFileInDB", "CCT_SYS_EX12", fexcep);
        } catch (IOException e) {
            e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "insertAwardsFileInDB", e.getMessage());
            throw new SystemException("CCT", "insertAwardsFileInDB", "CCT_SYS_EX12", e);
        }
        return hashMap;
    }

    public HashMap insertUploadFileInDB(String revisionChoice, InputStream inputStream, String quoteNumber,
                                        String quoteRevision, String quoteRevisionId, String opportunityNumber,
                                        HashMap inputParamMap, String partsType) {
//        public HashMap insertUploadFileInDB(String revisionChoice, String fileName, String quoteNumber,
//                                        String quoteRevision, String quoteRevisionId, String opportunityNumber,
//                                        HashMap inputParamMap, String partsType) {
        Quote quote = new Quote();
        HashMap hashMap = new HashMap();
        hashMap.put("validateStatus", "success");
        String uploadType = (String) inputParamMap.get("uploadType");
        String sheetName = (String) inputParamMap.get("sheetName");
        try {
//            OPCPackage pkg = OPCPackage.open(inputStream);
//            Workbook workbook = WorkbookFactory.create(pkg);
            //Workbook workbook = WorkbookFactory.create(new File("H:\\Beacon_Files\\"+fileName));
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
            //SXSSFWorkbook streamWorkbook = new SXSSFWorkbook(workbook);
            Sheet datatypeSheet = null;
            ViewObjectImpl cctQuotePartsEOView = getCctQuotePartsEOView();
            if (null != sheetName)                
                datatypeSheet = workbook.getSheet(sheetName);
            else               
                datatypeSheet = workbook.getSheetAt(0);
            //Below is commented as it is not implemented fully.
            /*     String validateStatus = validateUploadedFile(datatypeSheet);
            if("failure".equalsIgnoreCase(validateStatus)){
                hashMap.put("validateStatus", "failure");
                return hashMap;
            }  */
            workbook = null;
            inputStream = null;
            System.gc();
            if ("CREATE".equalsIgnoreCase(uploadType) && !revisionChoice.equalsIgnoreCase("UP")) {
                hashMap =
                    insertQuoteParts(revisionChoice, datatypeSheet, quoteNumber, quoteRevision, quoteRevisionId,
                                     opportunityNumber, inputParamMap, partsType);
                datatypeSheet = null;
                System.gc();
                quote = (Quote) hashMap.get("quote");
                String status = (String) hashMap.get("uploadStatus");
                String batchExecution = hashMap.get("batchExecution").toString();
                   Boolean executeBatch = Boolean.valueOf(batchExecution);
                boolean dataTypeException = Boolean.parseBoolean(hashMap.get("dataTypeException").toString());                
                   if(!executeBatch){
                       if ("successful".equalsIgnoreCase(status)) {
                           if ("RI".equalsIgnoreCase(revisionChoice)) {
                               if (partsType.equalsIgnoreCase("parts"))
                                   deleteQuoteParts(new BigDecimal(quoteRevisionId), inputParamMap);
                               else
                                   deleteFinalQuoteParts(new BigDecimal(quoteRevisionId), inputParamMap);
                           }
                           getDBTransaction().commit();
                           if (partsType.equalsIgnoreCase("parts"))
                               updateQueueProc(quote.getQuoteNumber(), quote.getQuoteRevision());
                           getPartsQueues(new BigDecimal(quote.getQuoteRevisionId()));
                           executeQuoteSummaryViews(new BigDecimal(quote.getQuoteRevisionId())); //Commented as it is giving exception
                       } else if("unsuccessful".equalsIgnoreCase(status) && !dataTypeException){
                           //ViewObjectImpl cctQuotePartsEOView = getCctQuotePartsEOView();
                          cctQuotePartsEOView.clearCache();
                          cctQuotePartsEOView.reset();
                          cctQuotePartsEOView.executeQuery();
                       }
                       else {                          
                           //ViewObjectImpl cctQuotePartsEOView = getCctQuotePartsEOView();
                           cctQuotePartsEOView.clearCache();
                           cctQuotePartsEOView.reset();
                           cctQuotePartsEOView.executeQuery();
                       }
                   }
                   else{
                       if ("successful".equalsIgnoreCase(status)) {
                           if ("RI".equalsIgnoreCase(revisionChoice)) {
                             if (partsType.equalsIgnoreCase("parts"))
                                 deleteQuoteParts(new BigDecimal(quoteRevisionId), inputParamMap);
                             else
                                 deleteFinalQuoteParts(new BigDecimal(quoteRevisionId), inputParamMap);
                         }
                           getDBTransaction().commit();
                           if (partsType.equalsIgnoreCase("parts"))
                               updateQueueProc(quote.getQuoteNumber(), quote.getQuoteRevision());
                           getPartsQueues(new BigDecimal(quote.getQuoteRevisionId()));
                           executeQuoteSummaryViews(new BigDecimal(quote.getQuoteRevisionId())); //Commented as it is giving exception
                       }
                           else if("unsuccessful".equalsIgnoreCase(status) && !dataTypeException){
                              //ViewObjectImpl cctQuotePartsEOView = getCctQuotePartsEOView();
                             cctQuotePartsEOView.clearCache();
                             cctQuotePartsEOView.reset();
                             cctQuotePartsEOView.executeQuery();
                          }
                          else {                              
                              //ViewObjectImpl cctQuotePartsEOView = getCctQuotePartsEOView();
                             cctQuotePartsEOView.clearCache();
                             cctQuotePartsEOView.reset();
                             cctQuotePartsEOView.executeQuery();  
                          }
                   }                
            } else {                
                hashMap =
                    updateQuoteParts(revisionChoice, datatypeSheet, quoteNumber, quoteRevision, quoteRevisionId,
                                     opportunityNumber, inputParamMap);
                datatypeSheet = null;
                System.gc();
                quote = (Quote) hashMap.get("quote");
                String status = (String) hashMap.get("uploadStatus");
                boolean dataTypeException = Boolean.parseBoolean(hashMap.get("dataTypeException").toString());
                if ("successful".equalsIgnoreCase(status)) {
                    //deleteQuoteParts(new BigDecimal(quoteRevisionId), inputParamMap);
                    getDBTransaction().commit();
                    //updateQueueProc(quote.getQuoteNumber(), quote.getQuoteRevision());
                    getPartsQueues(new BigDecimal(quote.getQuoteRevisionId()));
                    executeQuoteSummaryViews(new BigDecimal(quote.getQuoteRevisionId())); //Commented as it is giving exception
                } 
                else if("unsuccessful".equalsIgnoreCase(status) && !dataTypeException){
                   //ViewObjectImpl cctQuotePartsEOView = getCctQuotePartsEOView();
                  cctQuotePartsEOView.clearCache();
                  cctQuotePartsEOView.reset();
                  cctQuotePartsEOView.executeQuery();
                }
                else {                    
                    //ViewObjectImpl cctQuotePartsEOView = getCctQuotePartsEOView();
                    cctQuotePartsEOView.clearCache();
                    cctQuotePartsEOView.reset();
                    cctQuotePartsEOView.executeQuery();
                }
            }       
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new SystemException("CCT", "insertUploadFileInDB", "CCT_SYS_EX01", e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new SystemException("CCT", "insertUploadFileInDB", "CCT_SYS_EX01", e);
        } catch (Exception e) {
            logger.log(Level.INFO, getClass(), "insertUploadFileInDB", "IN Exception Block");
            e.printStackTrace();
            SimpleDateFormat sdtf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            String systemErrorTime = sdtf.format(timestamp).toString();
            CharSequence month = systemErrorTime.substring(5, 7);
            CharSequence day = systemErrorTime.substring(8, 10);
            CharSequence year = systemErrorTime.substring(0, 4);
            CharSequence hours = systemErrorTime.substring(11, 13);
            CharSequence minutes = systemErrorTime.substring(14, 16);
            systemErrorTime = month.toString().concat(day.toString().concat(year.toString().concat(hours.toString().concat(minutes.toString()))));
            //System.out.println("systemErrorTime ===="+systemErrorTime);
            hashMap.put("uploadStatus", "unsuccessfulforunknown");
            hashMap.put("uploadFailureReason", systemErrorTime);
            //quoteMap.put("incorrectQueueName", incorrectQueueName);
            ADFContext.getCurrent().getRequestScope().put("unknownErrorScope", hashMap);
            ViewObjectImpl cctQuotePartsEOView = getCctQuotePartsEOView();
            cctQuotePartsEOView.clearCache();
            cctQuotePartsEOView.reset();
            cctQuotePartsEOView.executeQuery();
            throw new SystemException("CCT", "insertUploadFileInDB", "CCT_SYS_EX01", e);
        }
        return hashMap;
    }

    /**
     * Container's getter for CctQuotePartsEOView1.
     * @return CctQuotePartsEOView1
     */
    public ViewObjectImpl getCctQuotePartsEOView() {
        return (ViewObjectImpl) findViewObject("CctQuotePartsEOView");
    }

    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public HashMap insertQuoteParts(String revisionChoice, Sheet datatypeSheet, String quoteNumber,
                                    String quoteRevision, String quoteRevisionId, String opportunityNumber,
                                    HashMap inputParamMap, String partsType) {
        logger.log(Level.INFO, getClass(), "insertQuoteParts", "Entering inside the method  insertQuoteParts");
        logger.log(Level.INFO, getClass(), "insertQuoteParts",
                   "Passing parameters to insertQuoteParts \n 1)revisionChoice=" + revisionChoice +
                   "\n 2)datatypeSheet=" + datatypeSheet + "\n 3)quoteNumber=" + quoteNumber + "\n 4)quoteRevision=" +
                   quoteRevision + "\n 5)quoteRevisionId=" + quoteRevisionId + "\n 6)opportunityNumber=" +
                   opportunityNumber + "\n 7)inputParamMap=" + getHashMapString(inputParamMap));
        String queueType = (String) inputParamMap.get("queueType");
        String lineSequence = (String) inputParamMap.get("lineNumber");
        //Quote quote = new Quote();
        int lineNumber = Integer.parseInt(lineSequence);
        String queueid = (String) inputParamMap.get("queueid");
        //int queueID = Integer.parseInt(queueid);
        //BigDecimal queueId = new BigDecimal(queueid);
        generateTemplate(queueType);
        String queueID_InCorrect = "N";
        HashMap quoteMap = new HashMap();
        BigDecimal quoteRevisionIdFromUI = new BigDecimal(quoteRevisionId);
        //boolean templateRows = true;
        Quote newQuoteRevision = new Quote();
        newQuoteRevision.setQuoteNumber(quoteNumber);
        newQuoteRevision.setQuoteRevisionId(quoteRevisionId);
        newQuoteRevision.setQuoteRevision(quoteRevision);
        quoteMap.put("quote", newQuoteRevision);
        quoteMap.put("uploadStatus", "unsuccessful");
        BigDecimal quotePartsRevisionID = quoteRevisionIdFromUI;
        int rowcounter = 1;
        int headerRowCount = 1;
        String uploadFailureReason = "";
        String incorrectQueueName = "";
        boolean mandatoryRecordMissing = false;
        ViewObject cctQuotePartsView1 = null;
        StringBuffer missingHeadersSB = new StringBuffer();
        StringBuffer duplicateHeadersSB = new StringBuffer();
        if (partsType.equalsIgnoreCase("parts"))
            cctQuotePartsView1 = getCctQuotePartsEOView();
        else if (partsType.equalsIgnoreCase("finalparts"))
            cctQuotePartsView1 = getCctQuotePartsFinalEOVO1(); 
        
        AttributeDef[] quotePartsAttributes = cctQuotePartsView1.getAttributeDefs();
        try {
            Iterator<org.apache.poi.ss.usermodel.Row> iterator = datatypeSheet.iterator();
            for (int lineCounter = 0; lineCounter <= lineNumber; lineCounter++) {
                iterator.next(); //skip required rows upto the start of parts record
                rowcounter++;
                headerRowCount++;
            }
            HashMap quotePartsMap = getQuotePartsMap();
            List missingParameters = new ArrayList();
            List missingHeaders = new ArrayList();
            List<String> columnHeaders = new ArrayList<String>();
            List duplicateHeaders = new ArrayList();
            List mandatoryFields = new ArrayList();
            List mandatoryHeaders = new ArrayList();
            String attributeDataType2 = null;
            long startTime = System.currentTimeMillis();   
           int batch = 0;
           boolean uploadExecution = true;
           int batchCounter = 1;
           boolean batchExecution = false;
            boolean dataTypeException = false; //for Invalid data on beacon File 
            boolean dataPrecisionException = false; //for invalid precision of data on beacon File
            int ProductRowCounter = 1;
            boolean insertFlag = true;
            missingHeaders.add(ModelConstants.queueTemplateHeaders[3]);
            missingHeaders.add(ModelConstants.queueTemplateHeaders[4]);
            missingHeaders.add(ModelConstants.queueTemplateHeaders[9]);
            missingHeaders.add(ModelConstants.queueTemplateHeaders[10]);
            missingHeaders.add(ModelConstants.queueTemplateHeaders[11]);
            missingHeaders.add(ModelConstants.queueTemplateHeaders[12]);
            missingHeaders.add(ModelConstants.queueTemplateHeaders[13]);
            mandatoryFields.add(ModelConstants.queueTemplateHeaders[3]);
            mandatoryFields.add(ModelConstants.queueTemplateHeaders[4]);
            mandatoryFields.add(ModelConstants.queueTemplateHeaders[9]);
            mandatoryFields.add(ModelConstants.queueTemplateHeaders[10]);
            while (iterator.hasNext()) { // this loop is for Template row
                org.apache.poi.ss.usermodel.Row currentRow = iterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();
                Iterator<Cell> cellIteratorInternal = currentRow.iterator();
                Cell newCell = null;
                Cell headerNewCell = null;
                String header2 = "";    
                String firstHeader = "";
                Object columnValue;                
                queueID_InCorrect = "N";    
                //if(rowcounter == 4389)
                    //System.out.println("row counter ==="+rowcounter);
                org.apache.poi.ss.usermodel.Row headerRow2 = datatypeSheet.getRow(lineNumber);
                Iterator<Cell> headerCellIterator = headerRow2.iterator();
               
                    while(headerCellIterator.hasNext()){
                        headerNewCell = headerCellIterator.next();
                        firstHeader = headerNewCell.getStringCellValue();                                            
                        //Condition for missing headers   
                        if(rowcounter == headerRowCount){
                            if((firstHeader.equals(ModelConstants.queueTemplateHeaders[9]) || 
                               firstHeader.equals(ModelConstants.queueTemplateHeaders[10]) || firstHeader.equals(ModelConstants.queueTemplateHeaders[11]) || 
                               firstHeader.equals(ModelConstants.queueTemplateHeaders[12])) || firstHeader.equals(ModelConstants.queueTemplateHeaders[13]) || 
                               firstHeader.equals(ModelConstants.queueTemplateHeaders[4]) ||  firstHeader.equals(ModelConstants.queueTemplateHeaders[3])){
                                if(firstHeader.equals(ModelConstants.queueTemplateHeaders[4]) ||  firstHeader.equals(ModelConstants.queueTemplateHeaders[3])){
                                    missingHeaders.remove(ModelConstants.queueTemplateHeaders[3]);
                                    missingHeaders.remove(ModelConstants.queueTemplateHeaders[4]);
                                }
                                else
                                    missingHeaders.remove(firstHeader);     
                                if(firstHeader.equals(ModelConstants.queueTemplateHeaders[3]))
                                    mandatoryFields.remove(ModelConstants.queueTemplateHeaders[4]);
                                if(firstHeader.equals(ModelConstants.queueTemplateHeaders[4]))
                                    mandatoryFields.remove(ModelConstants.queueTemplateHeaders[3]);
                            }
                            if(!firstHeader.equals(ModelConstants.queueTemplateHeaders[4]) &&  !firstHeader.equals(ModelConstants.queueTemplateHeaders[3])){
                                missingHeaders.remove(ModelConstants.queueTemplateHeaders[4]);
                            }
                        }
                        
                        //Condition for duplicate headers 
                        if(rowcounter == headerRowCount){
                            if(headerNewCell.getColumnIndex() > 0){
                                if(!columnHeaders.isEmpty()){
                                    if(columnHeaders.contains(firstHeader)){
                                        duplicateHeaders.add(firstHeader);                               
                                    }
                                }
                            
                                if(!columnHeaders.contains(firstHeader))
                                    columnHeaders.add(firstHeader);                    
                                //System.out.println("size of columnHeaders ==="+columnHeaders.size());
                            }
                        }
                    }
                
                // If mandatory headers of the beacon file is missing
                if(missingHeaders.size() > 0){
                    int missingHeaderCounter = 1;
                    missingHeadersSB.append("{");
                    for (Object headerMissed : missingHeaders) {     
                        if(missingHeaders.size() > 1){
                                if(missingHeaderCounter == missingHeaders.size())
                                    missingHeadersSB.append("" + headerMissed + "");
                                else
                                    missingHeadersSB.append("" + headerMissed + ", ");
                        }
                        else if(missingHeaders.size() == 1)
                            missingHeadersSB.append("" + headerMissed + "");
                        
                        missingHeaderCounter++;
                    }
                    missingHeadersSB.append("}");                    
                    uploadFailureReason = "It is missing the required headers: "+ missingHeadersSB.toString();
                    mandatoryRecordMissing = true;                    
                    missingHeaders.clear();
                    break;
                }
                //If duplicate headers exists in Beacon File
                if(duplicateHeaders.size() > 0){
                    int duplicateHeaderCounter = 1;
                    duplicateHeadersSB.append("{");
                    for (Object duplicateHeader : duplicateHeaders) {     
                        if(duplicateHeaders.size() > 1){
                            if(duplicateHeaderCounter == duplicateHeaders.size())
                                duplicateHeadersSB.append("" + duplicateHeader + "");
                            else
                                duplicateHeadersSB.append("" + duplicateHeader + ", ");
                        }
                        else if(duplicateHeaders.size() == 1)
                            duplicateHeadersSB.append("" + duplicateHeader + "");
                        
                        duplicateHeaderCounter++;
                    }
                    duplicateHeadersSB.append("}");                    
                    uploadFailureReason = "There are duplicate headers of: " + duplicateHeadersSB.toString();
                    mandatoryRecordMissing = true; 
                    duplicateHeaders.clear();
                    break;
                }
                org.apache.poi.ss.usermodel.Row headerRow = datatypeSheet.getRow(lineNumber);
                while (cellIteratorInternal.hasNext()) { //loop two
                    newCell = cellIteratorInternal.next();                    
                    Cell cell = headerRow.getCell(newCell.getColumnIndex());
                    //System.out.println("column index ==="+newCell.getColumnIndex());
                    if(null != cell){
                        header2 = cell.getStringCellValue(); 
                        mandatoryHeaders.add(header2);
                        attributeDataType2 = quoteVoAttrDataMapping.get(templateMapping.get(header2));
                        columnValue = null;
                        if (header2.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[9]) ||
                            header2.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[10]) ||
                            header2.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[4]) ||                       
                            manadotyFieldMap.containsKey(header2)) {
                            missingParameters.add(header2);
                            ProductRowCounter = rowcounter;
                            if (newCell.getCellTypeEnum() == CellType.STRING) {
                                if (!newCell.getStringCellValue()
                                            .trim()
                                            .equals("")) {
                                    columnValue = newCell.getStringCellValue().trim();
    
                                }
                            } else if (newCell.getCellTypeEnum() == CellType.NUMERIC)
                                columnValue = newCell.getNumericCellValue();
                            else if (newCell.getCellTypeEnum() == CellType.FORMULA) {
                                //if (formulaFiledMap.containsKey(header2)) {
                                if(!header2.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[0]) && !header2.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[14])){
                                    columnValue = newCell.getCellFormula();
                                    switch (newCell.getCachedFormulaResultTypeEnum()) {
                                    case NUMERIC:
                                        columnValue = newCell.getNumericCellValue();
                                        break;
    
                                    case STRING:
                                        columnValue = newCell.getStringCellValue();
                                        break;
                                    }
                                }
                                //}
                            }
                            if (header2.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[0])) {
                                if (quotePartsMap.get(newCell.getStringCellValue()) == null) {
                                    queueID_InCorrect = "Y";
                                }
                            }
                            if (columnValue != null)
                                missingParameters.remove(header2);

                        }
                    }
                    cell = null;
                    //headerRow = null;                    
                }                            
                //If L/I, Customer PN and Quoted Part Number are blank then we have reached upto the end of the template file
                if (missingParameters.size() > 0) {
                    if (missingParameters.contains(ModelConstants.queueTemplateHeaders[9]) &&
                        missingParameters.contains(ModelConstants.queueTemplateHeaders[10]) &&
                        missingParameters.contains(ModelConstants.queueTemplateHeaders[4])) {
                        missingParameters.clear();
                        break;
                    } else if (queueID_InCorrect.contentEquals("Y")) {
                        uploadFailureReason = "The File is missing required data: Column Name: {" + Arrays.asList(missingParameters).toString().replace("[", "").replace("]", "") + 
                                                                            " at Row Number:" +rowcounter+"}";
                        incorrectQueueName = "Queue Name is incorrect at Line No. " + rowcounter;
                        mandatoryRecordMissing = true;
                        break;
                    } else {                        
                        uploadFailureReason = "The File is missing required data: Column Name: {" + Arrays.asList(missingParameters).toString().replace("[", "").replace("]", "") + 
                                                                            " at Row Number:" +rowcounter+"}";
                        mandatoryRecordMissing = true;
                        missingParameters.clear();
                        break;
                    }
                }
                if (queueID_InCorrect.contentEquals("Y")) {
                    incorrectQueueName = "Queue Name is incorrect at Line No. " + rowcounter;
                    mandatoryRecordMissing = true;
                    break;
                }
                //if any unnecessary data are below the last product line
                if(!mandatoryHeaders.containsAll(mandatoryFields)){
                    uploadFailureReason = "There is unsupported data below the product data. Please delete all rows below line "+ProductRowCounter+" and retry";
                    mandatoryRecordMissing = true; 
                    mandatoryHeaders.clear();
                    break;
                }
                mandatoryHeaders.clear();
              
                oracle.jbo.Row createRow = cctQuotePartsView1.createRow();
                createRow.setAttribute("QuoteRevisionId", quotePartsRevisionID);
                org.apache.poi.ss.usermodel.Row headerRow3 = datatypeSheet.getRow(lineNumber);            
                boolean partsAttributeStringType = false;
                while (cellIterator.hasNext()) { //loop two
                    Cell currentCell = cellIterator.next();
                    int attributePrecision = 0;
                    int attributeCounter = 0; 
                    Cell cell = headerRow3.getCell(currentCell.getColumnIndex());
                    //String header = cell.getStringCellValue();
                    if (null != cell) {
                        String header = cell.getStringCellValue();
                        String voAttribute = quoteVoAttrMapping.get(templateMapping.get(header));
                        String attributeDataType = quoteVoAttrDataMapping.get(templateMapping.get(header));

                        Object value = null;                       
                        if (null != voAttribute) {
                            if(attributeDataType.equals("java.math.BigDecimal")){
                                BigDecimal cellTYpeIntValue = BigDecimal.ZERO;
                                if(currentCell.getCellTypeEnum() == CellType.STRING){
                                    if(!header.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[0]) && !header.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[14]) && !currentCell.getStringCellValue().equals("")){
                                        try{
                                            cellTYpeIntValue = BigDecimal.valueOf(Double.valueOf(currentCell.getStringCellValue()));
                                        }
                                        catch(Exception ae){                                            
                                            uploadFailureReason = "Incorrect data types detected at: {Column: "+header+", Row No: "+rowcounter+"}";
                                            quoteMap.put("uploadStatus", "unsuccessful");
                                            quoteMap.put("uploadFailureReason", uploadFailureReason);
                                            quoteMap.put("incorrectQueueName", incorrectQueueName);
                                            dataTypeException = true;
                                            break;
                                        }
                                    }
                                }
                                else if(currentCell.getCellTypeEnum() == CellType.FORMULA){
                                    if(!header.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[0])){
                                        currentCell.getCellFormula();
                                        switch (currentCell.getCachedFormulaResultTypeEnum()) {
                                        case NUMERIC:
                                            cellTYpeIntValue = BigDecimal.valueOf(currentCell.getNumericCellValue());
                                            break;
                                        
                                        case STRING:
                                            if(!currentCell.getStringCellValue().equals("")){
                                                uploadFailureReason = "Incorrect data types detected at: {Column: "+header+", Row No: "+rowcounter+"}";
                                                quoteMap.put("uploadStatus", "unsuccessful");
                                                quoteMap.put("uploadFailureReason", uploadFailureReason);
                                                quoteMap.put("incorrectQueueName", incorrectQueueName);
                                                dataTypeException = true;                                            
                                                break;
                                            }
                                        }
                                    }
                                }
                                cellTYpeIntValue = null;
                            }                           
                            if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                                if (attributeDataType.equals("java.lang.String")) {
                                    value = getIntValue(currentCell.getNumericCellValue());
                                } else
                                    value = currentCell.getNumericCellValue();

                            } else if (currentCell.getCellTypeEnum() == CellType.STRING) {
                                //value = ((String) (currentCell.getStringCellValue())).trim();
                                value = currentCell.getStringCellValue();
                                if (ModelConstants.quotePartsVOAttributes[0].equalsIgnoreCase(voAttribute)) {

                                    if (null != value && !"".equals(value)) {
                                        value = quotePartsMap.get(currentCell.getStringCellValue());

                                    }
                                }
                            } else if (currentCell.getCellTypeEnum() == CellType.FORMULA) {
//                                if(currentCell.getCellFormula().equals("VLOOKUP(B2,acid,7,FALSE)"))
//                                    System.out.println("rowcounter value ==="+rowcounter+"and the attribute is =="+voAttribute);
                                //if (formulaFiledMap.containsKey(header)) {
                                if(!header.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[14]) && !header.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[0])){
                                    value = currentCell.getCellFormula();
                                    switch (currentCell.getCachedFormulaResultTypeEnum()) {
                                    case NUMERIC:
                                        value = currentCell.getNumericCellValue();
                                        value = (Math.round((((Double) value).doubleValue()) * 100000.0)) / 100000.0;
                                        if (attributeDataType.equals("java.lang.String")) {
                                            value = getIntValue(value);
                                        }

                                        break;

                                    case STRING:
                                        value = currentCell.getStringCellValue();
                                        break;
                                    }
                                }
                                //}
                            }
                            if ("FirstPlanPurday".equalsIgnoreCase(voAttribute)) {
                                switch (currentCell.getCellTypeEnum()) {
                                case NUMERIC:
                                    value = currentCell.getDateCellValue();
                                    break;

                                case STRING:
                                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                                    value = sdf.parse(currentCell.getStringCellValue());
                                    break;
                                }
                            }
                            //}
//                            if(value == null || value.toString().equals("")){
//                                if(voAttribute.equals("QuotedPartNumber"))
//                                    System.out.println("rowcounter where value is null ==="+rowcounter);
//                            }
                            for(int i=0; i<quotePartsAttributes.length; i++){                                
                                if(quotePartsAttributes[attributeCounter].getName().equalsIgnoreCase(voAttribute)){                                   
                                    if(quotePartsAttributes[attributeCounter].getSQLType() == Types.VARCHAR){
                                        attributePrecision = quotePartsAttributes[attributeCounter].getPrecision(); 
                                        partsAttributeStringType = true;                                        
                                    }                                    
                                        break;
                                }      
                                attributeCounter++;
                            }
                            if(partsAttributeStringType){
                                if(null != value){                                   
                                    if(value.toString().length() > attributePrecision){
                                        uploadFailureReason = "Data size exceeds limits. See: {Column: "+header+", Row No: "+rowcounter+", size limit: "+attributePrecision+"}";
                                        quoteMap.put("uploadStatus", "unsuccessful");
                                        quoteMap.put("uploadFailureReason", uploadFailureReason);
                                        quoteMap.put("incorrectQueueName", incorrectQueueName);
                                        dataPrecisionException = true;
                                        break;
                                    }
                                    else{                                        
                                        createRow.setAttribute(voAttribute, value);
                                    }
                                }
                                else
                                    createRow.setAttribute(voAttribute, value);                                
                            }
                            else
                                createRow.setAttribute(voAttribute, value);
                            
                            //logger.log(Level.INFO, getClass(), "insertQuoteParts", "value inserted  "+value+" with corresponding voAttribute"+voAttribute);
                            partsAttributeStringType = false;
                        } /*else if (header.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[14]) || header.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[16])) {
                            newCell.setCellType(CellType.STRING);

                            value = ((String) (currentCell.getStringCellValue())).trim();
                            if (null != value && !"".equals(value)) {
                                value = quotePartsMap.get(currentCell.getStringCellValue());
                            }

                            createRow.setAttribute(ModelConstants.quotePartsVOAttributes[0], value);
                        }*/ else if (header.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[15])){
                            newCell.setCellType(CellType.STRING);
                            try{
                                value = ((String) (currentCell.getStringCellValue())).trim();
                            }catch(Exception ex){     
                                try{
                                    value = currentCell.getNumericCellValue();                                
                                    //value = value.toString(); 
                                }
                                catch(Exception ex2){
                                    value = currentCell.getCellFormula();
                                    switch (currentCell.getCachedFormulaResultTypeEnum()) {
                                    case NUMERIC:
                                        value = currentCell.getNumericCellValue();
                                        value = (Math.round((((Double) value).doubleValue()) * 100000.0)) / 100000.0;
                                        if (attributeDataType.equals("java.lang.String")) {
                                            value = getIntValue(value);
                                        }                                        
                                        break;

                                    case STRING:
                                        value = currentCell.getStringCellValue();
                                        break;
                                    }
                                }
                            }
                            value = String.valueOf(value);
                            createRow.setAttribute(ModelConstants.quotePartsVOAttributes[6], value);
                        }
                        attributeDataType = null;
                        voAttribute = null;
                        header = null;
                    }                                    
                    cell = null;                   
                    currentCell = null;                       
                }
                if(dataTypeException || dataPrecisionException)
                    break;
                if (insertFlag) {
                    //logger.log(Level.INFO, getClass(), "insertQuoteParts", "Total record value: "+createRow.toString());
                    cctQuotePartsView1.insertRow(createRow);
                }
                                
               if(batch == 10000){
                   batchExecution = true;
                   if ("N".equalsIgnoreCase(queueID_InCorrect) && !mandatoryRecordMissing) {
                       quoteMap.put("uploadStatus", "successful");
                       quoteMap.put("uploadFailureReason", "");
                       quoteMap.put("incorrectQueueName", "");
                   } else if (mandatoryRecordMissing) {
                       quoteMap.put("uploadStatus", "unsuccessful");
                       quoteMap.put("uploadFailureReason", uploadFailureReason);
                       quoteMap.put("incorrectQueueName", incorrectQueueName);
                       uploadExecution = false;
                       break;
                   }
                   batch = 0;                 
                   batchCounter++;
                   System.gc(); 
               }
                rowcounter++;
                batch++;
                createRow = null;
                headerCellIterator = null;
                headerRow3 = null;
                headerRow = null;
                headerRow2 = null;
                cellIteratorInternal = null;
                cellIterator = null;                
                currentRow = null;                  
            } //loop two            
            //logger.log(Level.INFO, getClass(), "insertQuoteParts", "Total records for upload: "+rowcounter);
            if(batchExecution){
                if(uploadExecution){
                    if(batch < 10000){
                        if ("N".equalsIgnoreCase(queueID_InCorrect) && !mandatoryRecordMissing && (!dataTypeException && !dataPrecisionException)) {
                            quoteMap.put("uploadStatus", "successful");
                            quoteMap.put("uploadFailureReason", "");
                            quoteMap.put("incorrectQueueName", "");
                        } else if (mandatoryRecordMissing) {
                            quoteMap.put("uploadStatus", "unsuccessful");
                            quoteMap.put("uploadFailureReason", uploadFailureReason);
                            quoteMap.put("incorrectQueueName", incorrectQueueName);
                        }  
                    }
                }
            }
            long endTime = System.currentTimeMillis();            
            logger.log(Level.INFO, getClass(), "insertQuoteParts", "Total time for upload: "+((endTime - startTime) / 1000)+" secs");
            if(!batchExecution){
                if ("N".equalsIgnoreCase(queueID_InCorrect) && !mandatoryRecordMissing && (!dataTypeException && !dataPrecisionException)) {
                    quoteMap.put("uploadStatus", "successful");
                    quoteMap.put("uploadFailureReason", "");
                    quoteMap.put("incorrectQueueName", "");
                } else if (mandatoryRecordMissing) {
                    quoteMap.put("uploadStatus", "unsuccessful");
                    quoteMap.put("uploadFailureReason", uploadFailureReason);
                    quoteMap.put("incorrectQueueName", incorrectQueueName);
                }                
            }
            quoteMap.put("batchExecution", batchExecution);
            quoteMap.put("dataTypeException", dataTypeException);
        } //loopOne
        catch (Exception ex) {
            ex.printStackTrace();            
            throw new SystemException("CCT", "insertQuoteParts", "CCT_SYS_EX01", ex);
        }
        return quoteMap;
    }

    @SuppressWarnings("oracle.jdeveloper.java.unused-method")
    private Object getIntValue(Object value) {
        double d = ((Double) value).doubleValue();
        BigDecimal dt = new BigDecimal(d);
        if ((d % 1) == 0) {            
            if(!String.valueOf((int)d).equals(String.valueOf(dt.intValue())))   
                value = dt;
            else
                value = String.valueOf(dt.intValue());                
        }

        return value;
    }

    public HashMap generateTemplate(String pMappingKey) {
        logger.log(Level.INFO, getClass(), "generateTemplate", "Entering generateTemplate method");
        HashMap tempHashMap = new HashMap();
        HashMap dataMaping = new HashMap();
        getQuotePartsVOAttr();
        getQuotePartsVOAttributeDataTypes();
        ViewObjectImpl cctMappingsROVO = getCctMappingsROVO();
        cctMappingsROVO.setNamedWhereClauseParam("pMappingKey", pMappingKey);
        cctMappingsROVO.executeQuery();
        StringBuilder stb = new StringBuilder();
        //TODO
        int index = 0;
        stb.append("------Key , Values pairs in DB---");
        RowSetIterator createRowSetIterator = cctMappingsROVO.createRowSetIterator(null);
        while (createRowSetIterator.hasNext()) {
            Row row = createRowSetIterator.next();
            dataMaping.put((String) row.getAttribute("SourceField"), (String) row.getAttribute("DataFormat"));
            templateMapping.put((String) row.getAttribute("SourceField"),
                                (String) row.getAttribute("DestinationField"));
            if ("Y".equalsIgnoreCase((String) row.getAttribute("Mandatory"))) {
                manadotyFieldMap.put((String) row.getAttribute("SourceField"), (String) row.getAttribute("Mandatory"));
            } else
                manadotyFieldMap.remove((String) row.getAttribute("SourceField"));
            if ("Y".equalsIgnoreCase((String) row.getAttribute("FormulaField"))) {
                formulaFiledMap.put((String) row.getAttribute("SourceField"),
                                    (String) row.getAttribute("FormulaField"));
            }
            index = index + 1;
            stb.append("\n" + index + ") " + "Source Field :-- " + row.getAttribute("SourceField") +
                       " Destination Field :-- " + row.getAttribute("DestinationField") + "--Formula Field :--" +
                       (String) row.getAttribute("FormulaField"));
        }
        logger.log(Level.INFO, getClass(), "generateTemplate", stb.toString());
        createRowSetIterator.closeRowSetIterator();
        tempHashMap.put("templateMapping", templateMapping);
        tempHashMap.put("quoteVoAttrMapping", quoteVoAttrMapping);
        tempHashMap.put("tasksVoAttrMapping", tasksVoAttrMapping);
        tempHashMap.put("dataMapping", dataMaping);
        logger.log(Level.INFO, getClass(), "generateTemplate", "Exiting generateTemplate method");
        return tempHashMap;
    }

    public String[] getAwardTagsLOV(String pMappingKey) {
        ViewObjectImpl cctMappingsROVO = getCctMappingsROVO();
        cctMappingsROVO.setNamedWhereClauseParam("pMappingKey", pMappingKey);
        cctMappingsROVO.executeQuery();
        int size = cctMappingsROVO.getRowCount();
        String[] awardTags = new String[size];
        ArrayList<String> awardTagList = new ArrayList<String>();

        RowSetIterator createRowSetIterator = cctMappingsROVO.createRowSetIterator(null);
        while (createRowSetIterator.hasNext()) {
            Row row = createRowSetIterator.next();
            String tag = (String) row.getAttribute(ModelConstants.SOURCE_FIELD);
            logger.log(Level.INFO, getClass(), "getAwardTagsLOV", "Tags= " + tag);

            awardTagList.add(tag);

        }
        String[] tempArray = new String[awardTagList.size()];
        awardTags = awardTagList.toArray(tempArray);
        return awardTags;
    }

    public HashMap generateAwardsUploadTemplate(String pMappingKey) {
        HashMap tempHashMap = new HashMap();
        getCctAwardsVOAttr();
      
        ViewObjectImpl cctMappingsROVO = getCctMappingsROVO();
        cctMappingsROVO.setNamedWhereClauseParam("pMappingKey", pMappingKey);
        cctMappingsROVO.executeQuery();
        RowSetIterator createRowSetIterator = cctMappingsROVO.createRowSetIterator(null);
        String[] arrStr = null;
        while (createRowSetIterator.hasNext()) {
            Row row = createRowSetIterator.next();
            arrStr = new String[2];
            arrStr[0] = (String) row.getAttribute("DestinationField");
            arrStr[1] = (String) row.getAttribute("DataFormat");
            if (arrStr[1] == null) {
                arrStr[1] = "";
            }
          
            uploadAwardsMapping.put((String) row.getAttribute(ModelConstants.SOURCE_FIELD), arrStr);
            logger.log(Level.INFO, getClass(), "generateAwardsUploadTemplate",
                       "Source Field :--" + row.getAttribute(ModelConstants.SOURCE_FIELD));

        }
        createRowSetIterator.closeRowSetIterator();
        tempHashMap.put("uploadAwardsMapping", uploadAwardsMapping);
        tempHashMap.put("awardsVoAttrMapping", awardsVoAttrMapping);
        return tempHashMap;
    }


    public HashMap generateDownloadTemplate(String pMappingKey) {
        HashMap tempHashMap = new HashMap();
        getAllQuotePartsVOAttr();
        ViewObjectImpl cctMappingsROVO = getCctMappingsROVO();
        cctMappingsROVO.setNamedWhereClauseParam("pMappingKey", pMappingKey);
        cctMappingsROVO.executeQuery();
        RowSetIterator createRowSetIterator = cctMappingsROVO.createRowSetIterator(null);
        String[] arrStr = null;
        while (createRowSetIterator.hasNext()) {
            Row row = createRowSetIterator.next();
            arrStr = new String[2];
            arrStr[0] = (String) row.getAttribute("DestinationField");

            arrStr[1] = (String) row.getAttribute("DataFormat");
            if (arrStr[1] == null) {
                arrStr[1] = "";
            }
            downloadBeaconMapping.put((String) row.getAttribute("SourceField"), arrStr);
            logger.log(Level.INFO, getClass(), "generateDownloadTemplate",
                       "Source Field :--" + row.getAttribute("SourceField"));

        }
        createRowSetIterator.closeRowSetIterator();
        tempHashMap.put("downloadBeaconMapping", downloadBeaconMapping);
        tempHashMap.put("allQuoteVoAttrMapping", allQuoteVoAttrMapping);
        return tempHashMap;
    }
    
    public HashMap generateFinalTop50DownloadTemplate(String pMappingKey) {
        HashMap tempHashMap = new HashMap();
        getCctFinalTop50PartsVOAttr();
        ViewObjectImpl cctMappingsROVO = getCctMappingsROVO();
        cctMappingsROVO.setNamedWhereClauseParam("pMappingKey", pMappingKey);
        cctMappingsROVO.executeQuery();
        RowSetIterator createRowSetIterator = cctMappingsROVO.createRowSetIterator(null);
        String[] arrStr = null;
        while (createRowSetIterator.hasNext()) {
            Row row = createRowSetIterator.next();
            arrStr = new String[2];
            arrStr[0] = (String) row.getAttribute("DestinationField");

            arrStr[1] = (String) row.getAttribute("DataFormat");
            if (arrStr[1] == null) {
                arrStr[1] = "";
            }
            downloadCctFinalTOP50Mapping.put((String) row.getAttribute("SourceField"), arrStr);
            logger.log(Level.INFO, getClass(), "generateDownloadTemplate",
                       "Source Field :--" + row.getAttribute("SourceField"));

        }
        createRowSetIterator.closeRowSetIterator();
        tempHashMap.put("downloadCctFinalTOP50Mapping", downloadCctFinalTOP50Mapping);
        tempHashMap.put("cctFinalTOP50AttrMapping", cctFinalTOP50AttrMapping);
        return tempHashMap;
    }

    public HashMap generateFinalDownloadTemplate(String pMappingKey) {
        HashMap tempHashMap = new HashMap();
        getQuotePartsFinalVOAttr();
        ViewObjectImpl cctMappingsROVO = getCctMappingsROVO();
        cctMappingsROVO.setNamedWhereClauseParam("pMappingKey", pMappingKey);
        cctMappingsROVO.executeQuery();
        RowSetIterator createRowSetIterator = cctMappingsROVO.createRowSetIterator(null);
        String[] arrStr = null;
        while (createRowSetIterator.hasNext()) {
            Row row = createRowSetIterator.next();
            arrStr = new String[2];
            arrStr[0] = (String) row.getAttribute("DestinationField");

            arrStr[1] = (String) row.getAttribute("DataFormat");
            if (arrStr[1] == null) {
                arrStr[1] = "";
            }
            downloadBeaconMapping.put((String) row.getAttribute("SourceField"), arrStr);
            logger.log(Level.INFO, getClass(), "generateDownloadTemplate",
                       "Source Field :--" + row.getAttribute("SourceField"));

        }
        createRowSetIterator.closeRowSetIterator();
        tempHashMap.put("downloadBeaconMapping", downloadBeaconMapping);
        tempHashMap.put("allQuoteVoAttrMapping", allQuoteVoAttrMapping);
        return tempHashMap;
    }

    public HashMap generateOpptyTemplate(String pMappingKey) {
        HashMap tempHashMap = new HashMap();
        getOpportunityVOAttr();
        ViewObjectImpl cctMappingsROVO = getCctMappingsROVO();
        cctMappingsROVO.setNamedWhereClauseParam("pMappingKey", pMappingKey);

        cctMappingsROVO.executeQuery();
        RowSetIterator createRowSetIterator = cctMappingsROVO.createRowSetIterator(null);
        while (createRowSetIterator.hasNext()) {
            Row row = createRowSetIterator.next();
            opptyMapping.put((String) row.getAttribute("SourceField"), (String) row.getAttribute("DestinationField"));
        }
        createRowSetIterator.closeRowSetIterator();
        tempHashMap.put("opptyMapping", opptyMapping);
        tempHashMap.put("opptyVoAttrMapping", opptyVoAttrMapping);
        return tempHashMap;
    }


    public Quote getNewQuoteRevision(String opportunityNumber) {
        CallableStatement cst = null;
        Quote quote = new Quote();
        BigDecimal quoteRevisonID;
        String stmt = " call CCT_QUOTE_CREATION.create_quote (?,?,?,?)";
        try {
            cst = this.getDBTransaction().createCallableStatement(stmt, 0);
            cst.setString(1, opportunityNumber);
            cst.registerOutParameter(2, Types.VARCHAR);
            cst.registerOutParameter(3, Types.VARCHAR);
            cst.registerOutParameter(4, Types.INTEGER);
            cst.executeUpdate();
            quoteRevisonID = cst.getBigDecimal(4);
            quote.setQuoteNumber(cst.getString(2));
            quote.setQuoteRevision(cst.getString(3));
            quote.setQuoteRevisionId(cst.getBigDecimal(4).toString());
        } catch (SQLException e) {
            //throw new JboException(e.getMessage());
            throw new SystemException("CCT", "getNewQuoteRevision", "CCT_SYS_EX01", e);
        } finally {
            if (cst != null) {
                try {
                    cst.close();
                } catch (SQLException e) {
                    // e.printStackTrace();
                    throw new SystemException("CCT", "getNewQuoteRevision", "CCT_SYS_EX01", e);
                }
            }
        }
        return quote;
    }

    public Quote getOpportunityQuoteRevison(String opportunityNumber) {
        LatestRevisonROVOImpl latestRevisonROVO = getLatestRevisonROVO();
        latestRevisonROVO.setpOpportunityNumber(opportunityNumber);
        latestRevisonROVO.executeQuery();
        Quote quote = new Quote();
        BigDecimal quoteRevisonID = BigDecimal.ZERO;
        RowSetIterator createRowSetIterator = null;
        try {
            createRowSetIterator = latestRevisonROVO.createRowSetIterator(null);
            while (createRowSetIterator.hasNext()) {
                Row row = createRowSetIterator.next();
                quoteRevisonID = (BigDecimal) row.getAttribute("RevisionId");
                quote.setQuoteRevisionId(quoteRevisonID.toString());
                quote.setQuoteNumber((String) row.getAttribute("QuoteNumber"));
                quote.setQuoteRevision((String) row.getAttribute("QuoteRevision"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException("CCT_BUS_EX01", e);
        } finally {
            if (null != createRowSetIterator)
                createRowSetIterator.closeRowSetIterator();

        }
        return quote;
    }


    /**
     * Container's getter for QuotePartsVO1.
     * @return QuotePartsVO1
     */
    public QuotePartsVOImpl getLightingQuotePartsVO1() {
        return (QuotePartsVOImpl) findViewObject("LightingQuotePartsVO1");
    }

    /**
     * Container's getter for QuotePartsVO2.
     * @return QuotePartsVO2
     */
    public QuotePartsVOImpl getHPPQuotePartsVO1() {
        return (QuotePartsVOImpl) findViewObject("HPPQuotePartsVO1");
    }

    /**
     * Container's getter for QuotePartsVO1.
     * @return QuotePartsVO1
     */
    public QuotePartsVOImpl getKittingQuotePartsVO1() {
        return (QuotePartsVOImpl) findViewObject("KittingQuotePartsVO1");
    }

    /**
     * Container's getter for QuotePartsVO2.
     * @return QuotePartsVO2
     */
    public QuotePartsVOImpl getAdvancedSourcingQuotePartsVO1() {
        return (QuotePartsVOImpl) findViewObject("AdvancedSourcingQuotePartsVO1");
    }

    /**
     * Container's getter for QuotePartsVO1.
     * @return QuotePartsVO1
     */
    public QuotePartsVOImpl getUnknownQuotePartsVO1() {
        return (QuotePartsVOImpl) findViewObject("UnknownQuotePartsVO1");
    }

    /**
     * Container's getter for QuotePartsVO2.
     * @return QuotePartsVO2
     */
    public QuotePartsVOImpl getStrategicPricingQuotePartsVO1() {
        return (QuotePartsVOImpl) findViewObject("StrategicPricingQuotePartsVO1");
    }

    /**
     * Container's getter for QuotePartsVO1.
     * @return QuotePartsVO1
     */
    public QuotePartsVOImpl getCommodityQuotePartsVO1() {
        return (QuotePartsVOImpl) findViewObject("CommodityQuotePartsVO1");
    }


    /**
     * Container's getter for QuotePartsVO1.
     * @return QuotePartsVO1
     */
    public ViewObjectImpl getAllQuotePartsVO1() {
        return (ViewObjectImpl) findViewObject("AllQuotePartsVO1");
    }

    /**
     * Container's getter for LatestRevisonROVO1.
     * @return LatestRevisonROVO1
     */
    public LatestRevisonROVOImpl getLatestRevisonROVO() {
        return (LatestRevisonROVOImpl) findViewObject("LatestRevisonROVO");
    }

    public void updateAwardsReviewProc(BigDecimal awdHeaderId) {
        CallableStatement cst = null;
        String stmt = " call  CCT_AWARD.map_part_with_opportunity (?)"; //awdHeaderId passed in the procedure
        try {
            cst = this.getDBTransaction().createCallableStatement(stmt, 0);
            cst.setBigDecimal(1, awdHeaderId);
            cst.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (cst != null) {
                try {
                    cst.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    public void updateAwardHeadersProc(BigDecimal opportunityId) {
        CallableStatement cst = null;
        String stmt = " call  CCT_AWARD.update_award_headers (?)"; //opportunityId passed in the procedure
        try {
            cst = this.getDBTransaction().createCallableStatement(stmt, 0);
            cst.setBigDecimal(1, opportunityId);
            cst.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (cst != null) {
                try {
                    cst.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void updateQueueProc(String quoteNumber, String quoteRevision) {
        CallableStatement cst = null;
        String stmt =
            " call  CCT_CREATE_QUEUE_TASKS.create_parts_queue (?,?,?)"; //  p_quote_number     IN cct_quote.quote_number%TYPE,
        try { // p_quote_revision   IN cct_quote_revisions.quote_revision%TYPE,
            cst = this.getDBTransaction().createCallableStatement(stmt, 0); //  p_return_status    OUT NOCOPY VARCHAR2
            cst.setString(1, quoteNumber);
            cst.setString(2, quoteRevision);
            cst.registerOutParameter(3, Types.VARCHAR);
            cst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SystemException("CCT", "updateQueueProc", "CCT_SYS_EX01", e);
        } finally {
            if (cst != null) {
                try {
                    cst.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    throw new SystemException("CCT", "updateQueueProc", "CCT_SYS_EX01", e);
                }
            }
        }
    }

    public QuotePartsVOImpl getNoBidQuotePartsVO1() {
        return (QuotePartsVOImpl) findViewObject("NoBidQuotePartsVO1");
    }

    public XSSFWorkbook downloadFileFromDB(XSSFWorkbook workbook, String queueId, String quoteRevisionId,
                                           String opportunityNumber) {
        //        XSSFWorkbook workbook=null;
        try {
            generateChemicalTemplate();
            //            workbook = new XSSFWorkbook(inputStream);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<org.apache.poi.ss.usermodel.Row> rowIterator = sheet.iterator();
            QuotePartsVOImpl acidReportVO = getAcidReportVO();
            //            fetchPartsQueues(acidReportVO, BigDecimal.ZERO, new BigDecimal(quoteRevisionId));
            RowSetIterator iterator = acidReportVO.createRowSetIterator(null);
            int i = 8;
            while (iterator.hasNext()) { //Row iteration
                Row currentRow = iterator.next();
                XSSFRow sheetRow = sheet.getRow(i);
                i++;
                int index = 0; //Column
                for (index = 0; index < currentRow.getAttributeCount(); index++) {
                    org.apache.poi.ss.usermodel.Row headerRow = sheet.getRow(7);
                    Cell currentCell = sheetRow.getCell(index);
                    // Cell newCell = headerRow.getCell(currentCell.getColumnIndex());
                    Cell newCell = headerRow.getCell(index);
                    if (null != newCell && newCell.getCellTypeEnum() != CellType.BLANK) {
                        String newHeader = newCell.getStringCellValue();
                        String voAttribute = templateMapping.get(newHeader);
                        if (null != currentRow && voAttribute != null && currentRow.getAttribute(voAttribute) != null) {
                            currentCell.setCellValue(currentRow.getAttribute(voAttribute).toString());
                            // currentCell.setCellStyle(cellStyle);


                        } else {
                            currentCell.setCellValue("");
                        }
                    }
                }
            }
            iterator.closeRowSetIterator();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SystemException("CCT", "updateQueueProc", "CCT_SYS_EX01", e);
        }
        return workbook;
    }

    public void generateChemicalTemplate() {
        templateMapping.put("L/N", "LineIdentifier");
        templateMapping.put("Customer Part Number", "CustomerPn");
        templateMapping.put("Annual Qty", "AnnualQty");
        templateMapping.put("UM(provided by customer)", "Uom");
        templateMapping.put("Item Description", "PartDesc");
        templateMapping.put("Total Qty", "TotalAltQtyOnhand");
        templateMapping.put("Product Class", "ProdClass");
    }


    /**
     * Container's getter for QuotePartsVO1.
     * @return QuotePartsVO1
     */
    public QuotePartsVOImpl getAcidReportVO() {
        return (QuotePartsVOImpl) findViewObject("AcidReportVO");
    }

    public long getQuotePartsCount(String opptunityNumber) {
        BigDecimal revisonId = BigDecimal.ZERO;
        ViewObjectImpl latestRevisonROVO = getLatestRevisonROVO();
        latestRevisonROVO.setNamedWhereClauseParam("pOpportunityNumber", opptunityNumber);
        latestRevisonROVO.executeQuery();
        RowSetIterator createRowSetIterator = latestRevisonROVO.createRowSetIterator(null);
        while (createRowSetIterator.hasNext()) {
            Row next = createRowSetIterator.next();
            revisonId = (BigDecimal) next.getAttribute("RevisionId");
        }
        createRowSetIterator.closeRowSetIterator();
        ViewObjectImpl cctQuotePartsEOView = getCctQuotePartsEOView();
        ViewCriteria criteria = cctQuotePartsEOView.getViewCriteria("CctQuotePartsEOViewCriteria");
        cctQuotePartsEOView.setNamedWhereClauseParam("pQuoteRevisionID", revisonId);
        cctQuotePartsEOView.applyViewCriteria(criteria);
        cctQuotePartsEOView.executeQuery();
        return cctQuotePartsEOView.getEstimatedRowCount();
    }

    public void deleteQuoteParts(BigDecimal revisonId, HashMap inputParamMap) {
        String queueid = (String) inputParamMap.get("queueid");
        int queueID = Integer.parseInt(queueid);
        BigDecimal queueId = new BigDecimal(queueid);
        ViewObjectImpl cctQuotePartsEOView = getCctQuotePartsEOView();
        if (queueID == 0) {
            ViewCriteria criteria = cctQuotePartsEOView.getViewCriteria("CctQuotePartsEOViewCriteria");
            cctQuotePartsEOView.setNamedWhereClauseParam("pQuoteRevisionID", revisonId);
            cctQuotePartsEOView.applyViewCriteria(criteria);
        } else {
            ViewCriteria criteria = cctQuotePartsEOView.getViewCriteria("CctQuotePartsEOViewCriteria1");
            cctQuotePartsEOView.setNamedWhereClauseParam("bind_QuoteRevId", revisonId);
            cctQuotePartsEOView.setNamedWhereClauseParam("bind_queueId", queueId);
            cctQuotePartsEOView.applyViewCriteria(criteria);
        }
        cctQuotePartsEOView.executeQuery();
        RowSetIterator createRowSetIterator = cctQuotePartsEOView.createRowSetIterator(null);
        while (createRowSetIterator.hasNext()) {
            ViewRowImpl currentRow = (ViewRowImpl) createRowSetIterator.next();
            EntityImpl entityImpl = (EntityImpl) currentRow.getEntity(0);
            if (EntityImpl.STATUS_NEW != entityImpl.getEntityState()) {
                currentRow.remove();
            }
        }
        createRowSetIterator.closeRowSetIterator();
        // getDBTransaction().commit();
    }

    public void deleteFinalQuoteParts(BigDecimal revisonId, HashMap inputParamMap) {
        String queueid = (String) inputParamMap.get("queueid");
        int queueID = Integer.parseInt(queueid);
        BigDecimal queueId = new BigDecimal(queueid);
        ViewObjectImpl cctQuotePartsFinalEOVO = getCctQuotePartsFinalEOVO1();
        ViewCriteria criteria = cctQuotePartsFinalEOVO.getViewCriteria("CctQuotePartsFinalEOVOCriteria");
        cctQuotePartsFinalEOVO.setNamedWhereClauseParam("bind_quoteRevisionID", revisonId);
        cctQuotePartsFinalEOVO.applyViewCriteria(criteria);

        cctQuotePartsFinalEOVO.executeQuery();
        RowSetIterator createRowSetIterator = cctQuotePartsFinalEOVO.createRowSetIterator(null);
        while (createRowSetIterator.hasNext()) {
            ViewRowImpl currentRow = (ViewRowImpl) createRowSetIterator.next();
            EntityImpl entityImpl = (EntityImpl) currentRow.getEntity(0);
            if (EntityImpl.STATUS_NEW != entityImpl.getEntityState()) {
                currentRow.remove();
            }
        }
        createRowSetIterator.closeRowSetIterator();
        // getDBTransaction().commit();
    }

    public Quote getNewQuoteRevisionID(String opportunityNumber) {
        CallableStatement cst = null;
        Quote quote = new Quote();
        BigDecimal quoteRevisonID = BigDecimal.ZERO;
        String stmt = " call CCT_QUOTE_CREATION.create_quote_revisions  (?,?,?)";
        try {
            cst = this.getDBTransaction().createCallableStatement(stmt, 0);
            cst.setString(1, opportunityNumber);
            cst.registerOutParameter(2, Types.INTEGER);
            cst.registerOutParameter(3, Types.VARCHAR);
            cst.executeUpdate();
            quoteRevisonID = cst.getBigDecimal(2);
            quote.setQuoteRevisionId(quoteRevisonID.toString());
            quote.setQuoteRevision(cst.getString(3));
        } catch (SQLException e) {
            throw new SystemException("CCT", "getNewQuoteRevisionID", "CCT_SYS_EX01", e);
            //throw new JboException(e.getMessage());
        } finally {
            if (cst != null) {
                try {
                    cst.close();
                } catch (SQLException e) {
                    //e.printStackTrace();
                    throw new SystemException("CCT", "getNewQuoteRevisionID", "CCT_SYS_EX01", e);
                }
            }
        }
        return quote;
    }

    public void executeFinalPartsSummaryViews(Object quoteRevisonId) {

        try {

            QuotePricingFinalVVOImpl pricingFinalVVO1 = getQuotePricingFinalVVO1();
            ViewCriteria vc2 = pricingFinalVVO1.getViewCriteria("QuotePricingFinalVVO_RevIDCriteria");
            pricingFinalVVO1.applyViewCriteria(vc2);
            pricingFinalVVO1.setNamedWhereClauseParam("b_QuoteRevId", quoteRevisonId);
            pricingFinalVVO1.executeQuery();
            logger.log(Level.INFO, getClass(), "executeFinalPartsSummaryViews",
                       "QuotePricingFinalVVO Executed=====" + pricingFinalVVO1.getEstimatedRowCount());


            QuoteClassFinalVVOImpl classFinalVVO1 = getQuoteClassFinalVVO1();
            classFinalVVO1.setNamedWhereClauseParam("b_QuoteRevisionId", quoteRevisonId);
            classFinalVVO1.executeQuery();

            logger.log(Level.INFO, getClass(), "executeFinalPartsSummaryViews",
                       "QuoteClassFinalVVO Executed====" + classFinalVVO1.getEstimatedRowCount());


        } catch (Exception e) {
            // TODO: Add catch code
            //e.printStackTrace();
            throw new SystemException("CCT", "executeQuoteSummaryViews", "CCT_SYS_EX01", e);
        }
    }

    public void finalQuoteRevisionID(String opportunityNumber) {
        logger.log(Level.INFO, getClass(), "finalQuoteRevisionID", "Entering finalQuoteRevisionID method");
        try {

            LatestRevisonROVOImpl latestRevisonROVO = getLatestRevisonROVO();
            latestRevisonROVO.setpOpportunityNumber(opportunityNumber);
            latestRevisonROVO.executeQuery();
            logger.log(Level.INFO, getClass(), "finalQuoteRevisionID",
                       "quote no in finalQuoteRevisionID" + latestRevisonROVO.getEstimatedRowCount());

            RowSetIterator createRowSetIterator = latestRevisonROVO.createRowSetIterator(null);
            Object revisionId = null;
            BigDecimal quoteID = BigDecimal.ZERO;
            while (createRowSetIterator.hasNext()) {
                Row next = createRowSetIterator.next();
                revisionId = next.getAttribute("RevisionId");
                quoteID = new BigDecimal(next.getAttribute("QuoteId").toString());
                logger.log(Level.INFO, getClass(), "finalQuoteRevisionID", "Quote revisionId ----- " + revisionId);
            }
            createRowSetIterator.closeRowSetIterator();
            quoteRevisionLOV(quoteID);
            executeFinalPartsSummaryViews(revisionId);
            logger.log(Level.INFO, getClass(), "finalQuoteRevisionID", "quote rev id in finalQuoteRevisionID");
        } catch (Exception e) {
            // TODO: Add catch code
            //e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "finalQuoteRevisionID", e.getMessage());
            throw new SystemException("CCT", "finalQuoteRevisionID", "CCT_SYS_EX01", e);
        }
        logger.log(Level.INFO, getClass(), "finalQuoteRevisionID", "Exiting finalQuoteRevisionID method");
    }

    /**
     * Container's getter for CctNotesVO1.
     * @return CctNotesVO1
     */
    public ViewObjectImpl getCctNotesVO1() {
        return (ViewObjectImpl) findViewObject("CctNotesVO1");
    }

    /**
     * Container's getter for NotesVO1.
     * @return NotesVO1
     */
    public ViewObjectImpl getTaskNotesVO() {
        return (ViewObjectImpl) findViewObject("TaskNotesVO");
    }

    /**
     * Container's getter for NotesVO1.
     * @return NotesVO1
     */
    public ViewObjectImpl getPartsNotesVO() {
        return (ViewObjectImpl) findViewObject("PartsNotesVO");
    }

    /**
     * Container's getter for NotesVO1.
     * @return NotesVO1
     */
    public ViewObjectImpl getOpportunityNotesVO() {
        return (ViewObjectImpl) findViewObject("OpportunityNotesVO");
    }

    public void addNotes(String taskNo, String partId, String opportunityNo, String noteComments, String noteLevel,
                         String userId, String username, String noteType) {
        logger.log(Level.INFO, getClass(), "addNotes", "Entering addNotes method");

        ViewObject vo = getCctNotesVO1();
        oracle.jbo.Row row = vo.createRow();
        row.setAttribute("NoteTaskNo", taskNo);
        row.setAttribute("NoteQuotePartId", partId);
        row.setAttribute("NoteOppNo", opportunityNo);
        row.setAttribute("NoteDesc", noteComments);
        row.setAttribute("NoteLevel", noteLevel);
        row.setAttribute("CreatedBy", userId);
        row.setAttribute("UserName", username);
        row.setAttribute("NoteType", noteType);
        vo.insertRow(row);
        getDBTransaction().commit();
    }

    public void addNotesData(String notesDesc, String noteType, String crfNo) {
        ActiveDirectoryManagerImpl adImpl = new ActiveDirectoryManagerImpl();
        String username = adImpl.fetchUserDetailsById(ADFContext.getCurrent().getSecurityContext().getUserName()).getName();                                       
        ViewObject vo = getCctNotesUpdateOppVO();
        oracle.jbo.Row row = vo.createRow();
        row.setAttribute("NoteOppNo", crfNo);
        row.setAttribute("NoteType", noteType);
        row.setAttribute("NoteDesc", notesDesc);
        row.setAttribute("NoteLevel", "OPPORTUNITY");
        row.setAttribute("UserName", username);
        row.setAttribute("CreatedBy", ADFContext.getCurrent()
                                                .getSecurityContext()
                                                .getUserName());

        vo.insertRow(row);
        getDBTransaction().commit();

    }

    public void setTaskNotesBindVar(String taskNumber) {
        logger.log(Level.INFO, getClass(), "setTaskNotesBindVar", "Entering setTaskNotesBindVar method");

        ViewObject vo = getTaskNotesVO();
        vo.setNamedWhereClauseParam(ModelConstants.BIND_TASK_NO, taskNumber);
        vo.setNamedWhereClauseParam(ModelConstants.BIND_LEVEL, ModelConstants.TASK);
        vo.executeQuery();
    }

    public void setPartsNotesBindVar(String quotePartId) {
        ViewObject vo = getPartsNotesVO();
        vo.setNamedWhereClauseParam("bind_quotePartId", quotePartId);
        vo.setNamedWhereClauseParam("bind_level", "PART");
        vo.executeQuery();
    }

    public void setOppNotesBindVar(String oppNo) {
       /* ViewObject vo = getOpportunityNotesVO();
        vo.setNamedWhereClauseParam("bind_OppNo", oppNo);
        vo.setNamedWhereClauseParam("bind_level", "OPPORTUNITY");
        vo.executeQuery();*/
        
        ViewObjectImpl cctNotesUpdateOppVO = getCctNotesUpdateOppVO();
        ViewCriteria criteria = cctNotesUpdateOppVO.getViewCriteria("CctNotesUpdateOppVOCriteria");
        //AwardHeaderVO.setNamedWhereClauseParam("b_sourceOppty", opptyid);
        cctNotesUpdateOppVO.setNamedWhereClauseParam("b_crfNo", oppNo);
        cctNotesUpdateOppVO.applyViewCriteria(criteria);
        cctNotesUpdateOppVO.executeQuery();
        //System.out.println("Notes rows========"+cctNotesUpdateOppVO.getEstimatedRowCount());
        
    }

    public void getOpportunityDetails(String opportunityNumber) {
        logger.log(Level.INFO, getClass(), "getOpportunityDetails", "Entering getOpportunityDetails method");

        ViewObject vo = getOpportunityVO1();
        vo.setNamedWhereClauseParam("bind_oppNo", opportunityNumber);
        vo.executeQuery();
    }

    public String getCrfID(String CrfNo) {
        logger.log(Level.INFO, getClass(), "getCrfID", "Entering getCrfID method");
        Object oppID = "";
        ViewObject oppVO = getOpportunityVO1();
        oppVO.setNamedWhereClauseParam("bind_oppNo", CrfNo);
        oppVO.executeQuery();
        RowSetIterator createRowSetIterator = oppVO.createRowSetIterator(null);
        while (createRowSetIterator.hasNext()) {
            Row next = createRowSetIterator.next();
            oppID = next.getAttribute("OpportunityId");
            //quoteID = new BigDecimal(next.getAttribute("QuoteId").toString());
        }
        return oppID.toString();
    }

    public void getQuotePartsVOAttr() {
        ViewAttributeDefImpl[] attributeDefImpl = getCctQuotePartsEOView().getViewAttributeDefImpls();
        for (ViewAttributeDefImpl viewAttrDef : attributeDefImpl) {
            quoteVoAttrMapping.put(viewAttrDef.getColumnName(), viewAttrDef.getName());
        }
        quoteVoAttrMapping.put("EXT_COST_VALUE_RANK", "ExtCostValueRank");
    }

    public void getQuotePartsVOAttributeDataTypes() {
        ViewAttributeDefImpl[] attributeDefImpl = getCctQuotePartsEOView().getViewAttributeDefImpls();
        for (ViewAttributeDefImpl viewAttrDef : attributeDefImpl) {
            quoteVoAttrDataMapping.put(viewAttrDef.getColumnName(),
                                       String.valueOf(viewAttrDef.getJavaType().getTypeName()));
        }
        quoteVoAttrDataMapping.put("EXT_COST_VALUE_RANK", "java.math.BigDecimal");
    }

    public void getCctAwardsVOAttr() {
        ViewAttributeDefImpl[] attributeDefImpl = getCctAwardsVO1().getViewAttributeDefImpls();
        for (ViewAttributeDefImpl viewAttrDef : attributeDefImpl) {
            awardsVoAttrMapping.put(viewAttrDef.getColumnName(), viewAttrDef.getName());
            logger.log(Level.INFO, getClass(), "getCctAwardsVOAttr",
                       "Col:-" + viewAttrDef.getColumnName() + "Attr:- " + viewAttrDef.getName());
        }
    }
    public void getCctFinalTop50PartsVOAttr() {
        logger.log(Level.INFO, getClass(), "getCctFinalTop50PartsVOAttr", "Entering getCctFinalTop50PartsVOAttr method");

        ViewAttributeDefImpl[] attributeDefImpl = getCctFinalTop50PartsView().getViewAttributeDefImpls();
        for (ViewAttributeDefImpl viewAttrDef : attributeDefImpl) {
            cctFinalTOP50AttrMapping.put(viewAttrDef.getColumnName(), viewAttrDef.getName());
            logger.log(Level.INFO, getClass(), "getCctFinalTop50PartsVOAttr",
                       "Col:-" + viewAttrDef.getColumnName() + "Attr:- " + viewAttrDef.getName());

        }
    }
   
    public void getAllQuotePartsVOAttr() {
        logger.log(Level.INFO, getClass(), "getAllQuotePartsVOAttr", "Entering getAllQuotePartsVOAttr method");

        ViewAttributeDefImpl[] attributeDefImpl = getQuotePartsVO1().getViewAttributeDefImpls();
        for (ViewAttributeDefImpl viewAttrDef : attributeDefImpl) {
            allQuoteVoAttrMapping.put(viewAttrDef.getColumnName(), viewAttrDef.getName());
            tasksVoAttrMapping.put(viewAttrDef.getColumnName(), viewAttrDef.getName());
            logger.log(Level.INFO, getClass(), "getAllQuotePartsVOAttr",
                       "Col:-" + viewAttrDef.getColumnName() + "Attr:- " + viewAttrDef.getName());

        }
    }

    public void getAllQuotePartsSnapshotFinalVOAttr() {
        logger.log(Level.INFO, getClass(), "getAllQuotePartsSnapshotFinalVOAttr",
                   "Entering getAllQuotePartsSnapshotFinalVOAttr method");

        ViewAttributeDefImpl[] attributeDefImpl = getAllQuotePartsSnapshotFinalVO1().getViewAttributeDefImpls();
        for (ViewAttributeDefImpl viewAttrDef : attributeDefImpl) {
            allQuoteVoAttrMapping.put(viewAttrDef.getColumnName(), viewAttrDef.getName());
            tasksVoAttrMapping.put(viewAttrDef.getColumnName(), viewAttrDef.getName());
            logger.log(Level.INFO, getClass(), "getAllQuotePartsSnapshotFinalVOAttr",
                       "Col:-" + viewAttrDef.getColumnName() + "Attr:- " + viewAttrDef.getName());

        }
    }

    public void getQuotePartsFinalVOAttr() {
        logger.log(Level.INFO, getClass(), "getCctQuotePartsFinalVO1", "Entering getAllQuotePartsVOAttr method");

        ViewAttributeDefImpl[] attributeDefImpl = getCctQuotePartsFinalVO1().getViewAttributeDefImpls();
        for (ViewAttributeDefImpl viewAttrDef : attributeDefImpl) {
            allQuoteVoAttrMapping.put(viewAttrDef.getColumnName(), viewAttrDef.getName());
            tasksVoAttrMapping.put(viewAttrDef.getColumnName(), viewAttrDef.getName());
            logger.log(Level.INFO, getClass(), "getCctQuotePartsFinalVO1",
                       "Col:-" + viewAttrDef.getColumnName() + "Attr:- " + viewAttrDef.getName());

        }
    }

    public void getOpportunityVOAttr() {
        ViewAttributeDefImpl[] attributeDefImpl = getOpportunityVO1().getViewAttributeDefImpls();
        for (ViewAttributeDefImpl viewAttrDef : attributeDefImpl) {
            opptyVoAttrMapping.put(viewAttrDef.getColumnName(), viewAttrDef.getName());
        }
    }

    public void currentQuoteRevisionID(String opportunityNumber) {
        logger.log(Level.INFO, getClass(), "currentQuoteRevisionID", "Entering currentQuoteRevisionID method");
        try {

            LatestRevisonROVOImpl latestRevisonROVO = getLatestRevisonROVO();
            latestRevisonROVO.setpOpportunityNumber(opportunityNumber);
            latestRevisonROVO.executeQuery();
            RowSetIterator createRowSetIterator = latestRevisonROVO.createRowSetIterator(null);
            Object revisionId = null;
            BigDecimal quoteID = BigDecimal.ZERO;
            while (createRowSetIterator.hasNext()) {
                Row next = createRowSetIterator.next();
                revisionId = next.getAttribute("RevisionId");
                quoteID = new BigDecimal(next.getAttribute("QuoteId").toString());
                logger.log(Level.INFO, getClass(), "currentQuoteRevisionID", "Quote revisionId ----- " + revisionId);
            }
            createRowSetIterator.closeRowSetIterator();
            quoteRevisionLOV(quoteID);
            executeQuoteSummaryViews(revisionId);
        } catch (Exception e) {
            // TODO: Add catch code
            //e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "currentQuoteRevisionID", e.getMessage());
            throw new SystemException("CCT", "currentQuoteRevisionID", "CCT_SYS_EX01", e);
        }
        logger.log(Level.INFO, getClass(), "currentQuoteRevisionID", "eXITING currentQuoteRevisionID method");
    }

    public void fetchQuoteDetails(String revisionID, String quoteVersion) {
        logger.log(Level.INFO, getClass(), "fetchQuoteDetails", "Entering fetchQuoteDetails method");

        ViewObject QuoteRevVO1 = getDistinctQuoteRevVO1();
        QuoteRevVO1.executeQuery();
        RowSetIterator QuoteRevision = QuoteRevVO1.createRowSetIterator(null);
        Object revisionId = null;
        while (QuoteRevision.hasNext()) {
            Row next = QuoteRevision.next();
            //revisionId = next.getAttribute("RevisionId");
            //quoteID = new BigDecimal(next.getAttribute("QuoteId").toString());
            if (next.getAttribute("QuoteRevision")
                    .toString()
                    .equals(quoteVersion)) {
                logger.log(Level.INFO, getClass(), "fetchQuoteDetails",
                           "Quote revisionId 3  -" + next.getAttribute("RevisionId"));

                revisionId = next.getAttribute("RevisionId");
                break;
            }
        }
        executeQuoteSummaryViews(revisionId); //for final pricing tab quote version LOV
        //executeFinalPartsSummaryViews(revisionId); //for final pricing tab quote version LOV
        logger.log(Level.INFO, getClass(), "fetchQuoteDetails", "Exiting fetchQuoteDetails method");

    }

    public void quoteRevisionLOV(BigDecimal quoteID) {
        try {
            ViewObject QuoteRevVO1 = getDistinctQuoteRevVO1();
            QuoteRevVO1.setNamedWhereClauseParam("bind_QuoteID", quoteID);
            QuoteRevVO1.executeQuery();
            //                ViewObjectImpl quoteRevisionVO = getQuoteRevisionVO1();
            //                ViewCriteria vc = quoteRevisionVO.getViewCriteria("QuoteRevisionVOCriteria");
            //                quoteRevisionVO.applyViewCriteria(vc);
            //                quoteRevisionVO.setNamedWhereClauseParam("bind_QuoteID", quoteID);
            //                quoteRevisionVO.executeQuery();
            RowSetIterator QuoteRevision = QuoteRevVO1.createRowSetIterator(null);
            while (QuoteRevision.hasNext()) {
                Row next = QuoteRevision.next();
                //revisionId = next.getAttribute("RevisionId");
                quoteID = new BigDecimal(next.getAttribute("QuoteId").toString());
            }

        } catch (Exception e) {
            // TODO: Add catch code
            //e.printStackTrace();
            throw new SystemException("CCT", "quoteRevisionLOV", "CCT_SYS_EX01", e);
        }
    }

    /**
     * Container's getter for CctMappingsROVO1.
     * @return CctMappingsROVO1
     */
    public ViewObjectImpl getCctMappingsROVO() {
        return (ViewObjectImpl) findViewObject("CctMappingsROVO");
    }

    public void executeQuoteSummaryViews(Object quoteRevisonId) {

        try {
            QuoteClassVVOImpl classVVO1 = getQuoteClassVVO1();
            //ViewCriteria vc = classVVO1.getViewCriteria("QuoteClassVVO_RevIDCriteria");
            //classVVO1.applyViewCriteria(vc);
            //classVVO1.setNamedWhereClauseParam("b_QuoteRevisionID", quoteRevisonId);
            classVVO1.setNamedWhereClauseParam("bind_RevisionID", quoteRevisonId);
            classVVO1.executeQuery();

            QuoteQueueVVOImpl queueVVO1 = getQuoteQueueVVO1();
            //ViewCriteria vc1 = queueVVO1.getViewCriteria("QuoteQueueVVO_RevIDCriteria");
            //queueVVO1.applyViewCriteria(vc1);
            //queueVVO1.setNamedWhereClauseParam("b_QuoteRevID", quoteRevisonId);
            queueVVO1.setNamedWhereClauseParam("bind_RevisionID", quoteRevisonId);
            queueVVO1.executeQuery();

            QuotePricingVVOImpl pricingVVO1 = getQuotePricingVVO1();
            ViewCriteria vc2 = pricingVVO1.getViewCriteria("QuotePricingVVO_RevIDCriteria");
            pricingVVO1.applyViewCriteria(vc2);
            pricingVVO1.setNamedWhereClauseParam("b_QuoteRevId", quoteRevisonId);
            pricingVVO1.executeQuery();
        } catch (Exception e) {
            // TODO: Add catch code
            //e.printStackTrace();
            throw new SystemException("CCT", "executeQuoteSummaryViews", "CCT_SYS_EX01", e);
        }
    }

    public void clearQuotePartsCache() {
        ViewObjectImpl cctQuotePartsEOView = getCctQuotePartsEOView();
        RowSetIterator createRowSetIterator = cctQuotePartsEOView.createRowSetIterator(null);
        while (createRowSetIterator.hasNext()) {
            ViewRowImpl currentRow = (ViewRowImpl) createRowSetIterator.next();
            EntityImpl entityImpl = (EntityImpl) currentRow.getEntity(0);
            if (EntityImpl.STATUS_NEW == entityImpl.getEntityState()) {
                currentRow.removeFromCollection();
            }
        }
        createRowSetIterator.closeRowSetIterator();
    }

    public void clearAwardsCache() {
        ViewObjectImpl CctAwardsVO1 = getCctAwardsVO1();
        RowSetIterator createRowSetIterator = CctAwardsVO1.createRowSetIterator(null);
        while (createRowSetIterator.hasNext()) {
            ViewRowImpl currentRow = (ViewRowImpl) createRowSetIterator.next();
            EntityImpl entityImpl = (EntityImpl) currentRow.getEntity(0);
            if (EntityImpl.STATUS_NEW == entityImpl.getEntityState()) {
                currentRow.removeFromCollection();
            }
        }
        createRowSetIterator.closeRowSetIterator();
    }

    public void awardReports(String criteria) {
        AwardsQuotePartsVOImpl awardsVVO1 = getAwardsQuotePartsVO1();
        ViewCriteria vc;
        vc = awardsVVO1.getViewCriteria(criteria.toString());
        awardsVVO1.applyViewCriteria(vc);
        //classVVO1.setNamedWhereClauseParam("b_QuoteRevisionID", quoteRevisonId);
        //           classVVO1.setNamedWhereClauseParam("bind_RevisionID", quoteRevisonId);
        awardsVVO1.executeQuery();
    }


    public HashMap getQuotePartsMap() {
        HashMap quotePartsMap = new HashMap();
        QuotePartsQueueVOImpl quotePartsQueueMapVO = getQuotePartsQueueMapVO();
        quotePartsQueueMapVO.executeQuery();
        RowSetIterator quotePartsIterator = quotePartsQueueMapVO.createRowSetIterator(null);
        while (quotePartsIterator.hasNext()) {
            Row currentRow = quotePartsIterator.next();
            quotePartsMap.put(currentRow.getAttribute("QueueName"), currentRow.getAttribute("QueueId"));
        }
        quotePartsIterator.closeRowSetIterator();
        return quotePartsMap;
    }

    /**
     * Container's getter for QuotePartsQueueVO3.
     * @return QuotePartsQueueVO3
     */
    public QuotePartsQueueVOImpl getQuotePartsQueueMapVO() {
        return (QuotePartsQueueVOImpl) findViewObject("QuotePartsQueueMapVO");
    }

    /**
     * Container's getter for DistinctQuoteRevVO1.
     * @return DistinctQuoteRevVO1
     */
    public DistinctQuoteRevVOImpl getDistinctQuoteRevVO1() {
        return (DistinctQuoteRevVOImpl) findViewObject("DistinctQuoteRevVO1");
    }

    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public HashMap updateQuoteParts(String revisionChoice, Sheet datatypeSheet, String quoteNumber,
                                    String quoteRevision, String quoteRevisionId, String opportunityNumber,
                                    HashMap inputParamMap) {
        logger.log(Level.INFO, getClass(), "updateQuoteParts", "Entering inside the method  updateQuoteParts");
        logger.log(Level.INFO, getClass(), "updateQuoteParts",
                   "Passing parameters to updateQuoteParts \n 1)revisionChoice=" + revisionChoice +
                   "\n 2)datatypeSheet=" + datatypeSheet + "\n 3)quoteNumber=" + quoteNumber + "\n 4)quoteRevision=" +
                   quoteRevision + "\n 5)quoteRevisionId=" + quoteRevisionId + "\n 6)opportunityNumber=" +
                   opportunityNumber + "\n 7)inputParamMap=" + getHashMapString(inputParamMap));
        HashMap quoteMap = new HashMap();
        String queueType = (String) inputParamMap.get("queueType");
        String lineSequence = (String) inputParamMap.get("lineNumber");
        int lineNumber = Integer.parseInt(lineSequence);
        String queueid = (String) inputParamMap.get("queueid");
        int queueID = Integer.parseInt(queueid);
        BigDecimal queueId = new BigDecimal(queueid);
        //generateTemplate("BEACON_FILE");
        generateTemplate(queueType);
        String queueID_InCorrect = "N";

        BigDecimal quoteRevisionIdFromUI = new BigDecimal(quoteRevisionId);
        Quote newQuoteRevision = new Quote();
        newQuoteRevision.setQuoteNumber(quoteNumber);
        newQuoteRevision.setQuoteRevisionId(quoteRevisionId);
        newQuoteRevision.setQuoteRevision(quoteRevision);
        quoteMap.put("quote", newQuoteRevision);
        quoteMap.put("uploadStatus", "unsuccessful");
        BigDecimal quotePartsRevisionID = quoteRevisionIdFromUI;
        String uploadFailureReason = "";
        String incorrectQueueName = "";
        boolean mandatoryRecordMissing = false;
        int rowcounter = 1;
        ViewObjectImpl cctQuotePartsEOView = null;
        if (queueType.equals(ModelConstants.templateMapping)) 
            cctQuotePartsEOView = getAllQuotePartsEOView1();
        else
            cctQuotePartsEOView = getCctQuotePartsEOView();
        
        AttributeDef[] quotePartsAttributes = cctQuotePartsEOView.getAttributeDefs();
        try {
            Iterator<org.apache.poi.ss.usermodel.Row> iterator = datatypeSheet.iterator();
            for (int lineCounter = 0; lineCounter <= lineNumber; lineCounter++) {
                iterator.next();
                rowcounter++;
            } //skip required rows upto the start of parts record
            HashMap quotePartsMap = getQuotePartsMap();
            startTime = System.currentTimeMillis();
            int cnt = 0;
            List missingParameters = new ArrayList();
            boolean dataTypeException = false; //for Invalid data on beacon File  
            boolean dataPrecisionException = false; //for invalid precision of data on beacon File
            String attributeDataType2 = null;
            while (iterator.hasNext()) { // loopone
                cnt++;
                
                org.apache.poi.ss.usermodel.Row currentRow = iterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();
                Iterator<Cell> cellIteratorInternal = currentRow.iterator();
                Iterator<Cell> cellIteratorInternal2 = currentRow.iterator();
                Cell newCell = null;
                String header2 = "";
                Object columnValue;
                queueID_InCorrect = "N";
                while (cellIteratorInternal2.hasNext()) { //loop two
                    newCell = cellIteratorInternal2.next();
                    org.apache.poi.ss.usermodel.Row headerRow = datatypeSheet.getRow(lineNumber);
                    Cell cell = headerRow.getCell(newCell.getColumnIndex());
                    if(null != cell){
                    header2 = cell.getStringCellValue();
                    attributeDataType2 = quoteVoAttrDataMapping.get(templateMapping.get(header2));
                    columnValue = null;

                    if (header2.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[1]) ||
                        header2.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[2]) ||
                        header2.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[9]) ||
                        header2.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[3]) ||
                        header2.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[4]) ||
                        header2.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[5]) ||
                        header2.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[10]) ||
                        header2.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[0]) ||
                        header2.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[14])) {
                        missingParameters.add(header2);

                        if (newCell.getCellTypeEnum() == CellType.STRING) {
                            if (!newCell.getStringCellValue()
                                        .trim()
                                        .equals("")) {
                                columnValue = newCell.getStringCellValue().trim();
                                if (header2.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[0])) {
                                    if (quotePartsMap.get(newCell.getStringCellValue()) == null) {
                                        queueID_InCorrect = "Y";
                                    }
                                }
                            }
                        } else if (newCell.getCellTypeEnum() == CellType.NUMERIC)
                            columnValue = newCell.getNumericCellValue();
                        else if (newCell.getCellTypeEnum() == CellType.FORMULA) {
                            if (formulaFiledMap.containsKey(header2)) {
                                columnValue = newCell.getCellFormula();
                                switch (newCell.getCachedFormulaResultTypeEnum()) {
                                case NUMERIC:
                                    columnValue = newCell.getNumericCellValue();
                                    break;

                                case STRING:
                                    columnValue = newCell.getStringCellValue();
                                    break;
                                }
                            }
                        }
                        if (columnValue != null)
                            missingParameters.remove(header2);
                    }
                 }
                }

                if (missingParameters.size() > 0) {
                    logger.log(Level.INFO, getClass(), "updateQuoteParts",
                               "Missing Params----" + Arrays.asList(missingParameters));
                    if (!queueid.equals(ModelConstants.queueIds[0])) {
                        if (missingParameters.contains(ModelConstants.queueTemplateHeaders[9]) &&
                            missingParameters.contains(ModelConstants.queueTemplateHeaders[10]) &&
                            missingParameters.contains(ModelConstants.queueTemplateHeaders[4])) {
                            missingParameters.clear();
                            break;
                        }

                    }
                    if (queueID_InCorrect.contentEquals("Y")) {                      
                        uploadFailureReason = "The File is missing required data: Column Name: {" + Arrays.asList(missingParameters).toString().replace("[", "").replace("]", "") + 
                                                    " at Row Number:" +rowcounter+"}";
                        incorrectQueueName = "Queue Name is incorrect at Line No. " + rowcounter;
                        mandatoryRecordMissing = true;
                        break;
                    } else {
                        uploadFailureReason = "The File is missing required data: Column Name: {" + Arrays.asList(missingParameters).toString().replace("[", "").replace("]", "") + 
                                                    " at Row Number:" +rowcounter+"}";
                        mandatoryRecordMissing = true;
                        missingParameters.clear();
                        break;
                    }
                }
                if (queueID_InCorrect.equalsIgnoreCase("Y")) {
                    incorrectQueueName = "Queue Name is incorrect at Line No. " + rowcounter;
                    mandatoryRecordMissing = true;
                    break;
                }

                org.apache.poi.ss.usermodel.Row headerRow = datatypeSheet.getRow(lineNumber);
                Object linecell = null; // Need to get Values dynamically for LineIdentifier
                Object customerPin = null; // Need to get Values dynamically for CustomerPin
                Object quotedPartNo = null;
                boolean partsAttributeStringType = false;
                while (cellIteratorInternal.hasNext()) { //loop two
                    Cell currentCell = cellIteratorInternal.next();                    
                    Cell cell = headerRow.getCell(currentCell.getColumnIndex());
                    if(null != cell){
                    String header = cell.getStringCellValue();
                    String voAttribute = quoteVoAttrMapping.get(templateMapping.get(header));
                    if (ModelConstants.queueTemplateHeaders[1].equalsIgnoreCase(header) ||
                        ModelConstants.queueTemplateHeaders[2].equalsIgnoreCase(header) ||
                        ModelConstants.queueTemplateHeaders[9].equalsIgnoreCase(header)) {
                        if (currentCell.getCellTypeEnum() == CellType.STRING) {
                            String linecell_str = (String) currentCell.getStringCellValue();
                            try {
                                linecell = new BigDecimal(linecell_str);
                            } catch (Exception ex) {
                                linecell = linecell_str;
                            }
                        } else if (currentCell.getCellTypeEnum() == CellType.NUMERIC)
                            linecell = currentCell.getNumericCellValue();
                    }
                    if (ModelConstants.queueTemplateHeaders[3].equalsIgnoreCase(header) ||
                        ModelConstants.queueTemplateHeaders[4].equalsIgnoreCase(header)){                           
                            try{
                                customerPin = currentCell.getStringCellValue();
                            }catch(Exception ex){                               
                                customerPin = getIntValue(currentCell.getNumericCellValue());                                
                                customerPin = customerPin.toString();
                                if(customerPin.toString().contains(".")){
                                    customerPin = customerPin.toString().replace(".0", "");                                                                    
                                }
                            }
                            customerPin.toString().trim();
                            customerPin = String.valueOf(customerPin);
                        }
                  /*  if (!queueid.equals(ModelConstants.queueIds[9])) {
                        if (queueid.equals(ModelConstants.queueIds[3]) &&
                            ModelConstants.queueTemplateHeaders[10].equalsIgnoreCase(header)) {                            
                            try{
                                quotedPartNo = currentCell.getStringCellValue();
                            }
                            catch(Exception ex){
                                quotedPartNo = currentCell.getNumericCellValue();
                                quotedPartNo = quotedPartNo.toString();
                                if(quotedPartNo.toString().contains(".")){
                                    quotedPartNo = quotedPartNo.toString().replace(".0", "");                                                                    
                                }
                            }
                            quotedPartNo = String.valueOf(quotedPartNo);
                        } else if (ModelConstants.queueTemplateHeaders[5].equalsIgnoreCase(header)) {
                            try{
                                quotedPartNo = currentCell.getStringCellValue();
                            }
                            catch(Exception ae){
                                quotedPartNo = currentCell.getNumericCellValue();
                                quotedPartNo = quotedPartNo.toString();
                                if(quotedPartNo.toString().contains(".")){
                                    quotedPartNo = quotedPartNo.toString().replace(".0", "");                                                                    
                                }
                            }
                            quotedPartNo = String.valueOf(quotedPartNo);
                            logger.log(Level.INFO, getClass(), "updateQuoteParts",
                                       "QuotedPartNumber value --" + quotedPartNo + "-- for the header --" + header);
                        }
                    } else if (queueid.equals(ModelConstants.queueIds[9])) {
                        if (ModelConstants.queueTemplateHeaders[10].equalsIgnoreCase(header)) {
                            try{
                                quotedPartNo = currentCell.getStringCellValue();
                            }
                            catch(Exception ae){
                                quotedPartNo = currentCell.getNumericCellValue();
                                quotedPartNo = quotedPartNo.toString();
                                if(quotedPartNo.toString().contains(".")){
                                    quotedPartNo = quotedPartNo.toString().replace(".0", "");                                                                    
                                }
                            }
                            quotedPartNo = String.valueOf(quotedPartNo);
                        }
                    }*/
                 }
                }
                /*logger.log(Level.INFO, getClass(), "insertQuoteParts",
                           "BindParameters passed to CctQuotePartsEOUpdateVC viewCriteria \n 1)lineIdentifier=--" +
                           linecell + "--" + "\n 2)customerPin=--" + customerPin + "--" + "\n 3)quotedPartNo=--" +
                           quotedPartNo + "--");*/               
                cctQuotePartsEOView.reset();
                cctQuotePartsEOView.clearCache();                
                ViewCriteria cctQuotePartsEOUpdateVC = null;
                if (queueType.equals(ModelConstants.templateMapping)) {
                    cctQuotePartsEOUpdateVC = cctQuotePartsEOView.getViewCriteria("AllQuotePartsEOViewCriteria");
                    cctQuotePartsEOView.setNamedWhereClauseParam("bind_LineIdentity", linecell);
                    cctQuotePartsEOView.setNamedWhereClauseParam("bind_CustPN", customerPin);
                    //cctQuotePartsEOView.setNamedWhereClauseParam("bind_QuotedPN", quotedPartNo);
                    cctQuotePartsEOView.setNamedWhereClauseParam("bind_QuoteRevId", quotePartsRevisionID);
                    cctQuotePartsEOView.applyViewCriteria(cctQuotePartsEOUpdateVC);
                } else {
                    cctQuotePartsEOUpdateVC = cctQuotePartsEOView.getViewCriteria("CctQuotePartsEOUpdateVC");
                    cctQuotePartsEOView.setNamedWhereClauseParam("pLineIdentifier", linecell);
                    cctQuotePartsEOView.setNamedWhereClauseParam("pCustomerPin", customerPin);
                    //cctQuotePartsEOView.setNamedWhereClauseParam("pQuotedPartNo", quotedPartNo);
                    cctQuotePartsEOView.setNamedWhereClauseParam("pQuoteRevID", quotePartsRevisionID);
                    cctQuotePartsEOView.applyViewCriteria(cctQuotePartsEOUpdateVC);
                }
//                logger.log(Level.INFO, getClass(), "updateQuoteParts",
//                           "cctQuotePartsEOView query is --\n" + cctQuotePartsEOView.getQuery());
                cctQuotePartsEOView.executeQuery();
                logger.log(Level.INFO, getClass(), "updateQuoteParts",
                           "QuotePartsView estimated row count----------------" +
                           cctQuotePartsEOView.getEstimatedRowCount());
                if (cctQuotePartsEOView.getEstimatedRowCount() == 0) {
                    uploadFailureReason = "Mandatory fields has invalid data at Line No. " + rowcounter;
                    mandatoryRecordMissing = true;
                    quoteMap.put("uploadFailureReason", uploadFailureReason);
                }
                RowSetIterator cctQuotePartsIterator = cctQuotePartsEOView.createRowSetIterator(null);

                //todo
                while (cctQuotePartsIterator.hasNext()) {
                    Row currentUpdateRow = cctQuotePartsIterator.next();
                    String currentRowQueueId = currentUpdateRow.getAttribute(ModelConstants.quotePartsVOAttributes[0]).toString();
                    logger.log(Level.INFO, getClass(), "updateQuoteParts",
                               "Queue ID of the updated part----------------" +currentRowQueueId);
                    boolean queueMoved = false;
                    while (cellIterator.hasNext()) { //loop two
                        Cell currentCell = cellIterator.next();  
                        int attributePrecision = 0;
                        int attributeCounter = 0;
                        Cell cell = headerRow.getCell(currentCell.getColumnIndex());                        
                        if (null != cell) {
                            String header = cell.getStringCellValue();
                            String voAttribute = quoteVoAttrMapping.get(templateMapping.get(header));
                            String attributeDataType = quoteVoAttrDataMapping.get(templateMapping.get(header));
                            Object value = null;
                            if (null != voAttribute) {
                                if(attributeDataType.equals("java.math.BigDecimal")){
                                    BigDecimal cellTYpeIntValue = BigDecimal.ZERO;
                                    if(currentCell.getCellTypeEnum() == CellType.STRING){
                                        if(!header.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[0]) && !currentCell.getStringCellValue().equals("")){
                                            try{
                                                cellTYpeIntValue = BigDecimal.valueOf(Double.valueOf(currentCell.getStringCellValue()));
                                            }
                                            catch(Exception ae){
                                                uploadFailureReason = "Incorrect data types detected at: {Column: "+header+", Row No: "+rowcounter+"}";
                                                quoteMap.put("uploadStatus", "unsuccessful");
                                                quoteMap.put("uploadFailureReason", uploadFailureReason);
                                                quoteMap.put("incorrectQueueName", incorrectQueueName);
                                                dataTypeException = true;
                                                break;
                                            }
                                        }
                                    }
                                    else if(currentCell.getCellTypeEnum() == CellType.FORMULA){
                                        if(!header.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[0])){
                                            currentCell.getCellFormula();
                                            if(currentCell.getCachedFormulaResultTypeEnum() == CellType.ERROR){
                                                //if(!currentCell.getStringCellValue().equals("")){
                                                    uploadFailureReason = "Error in the data value detected at: {Column: "+header+", Row No: "+rowcounter+"}";
                                                    quoteMap.put("uploadStatus", "unsuccessful");
                                                    quoteMap.put("uploadFailureReason", uploadFailureReason);
                                                    quoteMap.put("incorrectQueueName", incorrectQueueName);
                                                    dataTypeException = true;                                            
                                                    break;
                                                //}
                                            }
                                            else{
                                                switch (currentCell.getCachedFormulaResultTypeEnum()) {
                                                case NUMERIC:
                                                    cellTYpeIntValue = BigDecimal.valueOf(currentCell.getNumericCellValue());
                                                    break;
                                                
                                                case STRING:
                                                    if(!currentCell.getStringCellValue().equals("")){
                                                        uploadFailureReason = "Incorrect data types detected at: {Column: "+header+", Row No: "+rowcounter+"}";
                                                        quoteMap.put("uploadStatus", "unsuccessful");
                                                        quoteMap.put("uploadFailureReason", uploadFailureReason);
                                                        quoteMap.put("incorrectQueueName", incorrectQueueName);
                                                        dataTypeException = true;                                            
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    cellTYpeIntValue = null;
                                }
                                if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                                    if (attributeDataType.equals("java.lang.String")) {
                                        value = getIntValue(currentCell.getNumericCellValue());
                                    } else
                                        value = currentCell.getNumericCellValue();
                                } else if (currentCell.getCellTypeEnum() == CellType.STRING) {
                                    value = ((String) (currentCell.getStringCellValue())).trim();
                                    if (ModelConstants.quotePartsVOAttributes[0].equalsIgnoreCase(voAttribute)) {

                                        if (null != value && !"".equals(value)) {
                                            value = quotePartsMap.get(currentCell.getStringCellValue());
                                        }

                                    }
                                } else if (currentCell.getCellTypeEnum() == CellType.FORMULA) {
                                    if (formulaFiledMap.containsKey(header)) {
                                        value = currentCell.getCellFormula();
                                        switch (currentCell.getCachedFormulaResultTypeEnum()) {
                                        case NUMERIC:
                                            value = currentCell.getNumericCellValue();
                                            value =
                                                (Math.round((((Double) value).doubleValue()) * 100000.0)) / 100000.0;
                                            if (attributeDataType.equals("java.lang.String")) {
                                                value = getIntValue(value);
                                            }
                                            break;

                                        case STRING:
                                            value = currentCell.getStringCellValue();
                                            break;
                                        }
                                    }
                                }

                                if (ModelConstants.quotePartsVOAttributes[1].equalsIgnoreCase(voAttribute) ||
                                    ModelConstants.quotePartsVOAttributes[2].equalsIgnoreCase(voAttribute)) {
                                    if (currentCell.getCellTypeEnum() != CellType.BLANK) {
                                        if (!currentCell.getStringCellValue().equals("")) {
                                            try {
                                                value =
                                                    new SimpleDateFormat("MM/dd/yyyy")
                                                    .parse(currentCell.getStringCellValue());
                                            } catch (Exception ae) {
                                                value = currentCell.getStringCellValue();
                                            }
                                        }
                                    }
                                }
                               /* logger.log(Level.INFO, getClass(), "updateQuoteParts",
                                           "Row index=--" + (currentCell.getRowIndex() + 1) + "--Column header=--" +
                                           header + "--VoAttribute=--" + voAttribute + "--Current cell value=--" +
                                           value + "--" + "--CellTypeEnum=--" + currentCell.getCellTypeEnum() + "--");*/
                               for(int i=0; i<quotePartsAttributes.length; i++){                                
                               if(quotePartsAttributes[attributeCounter].getName().equalsIgnoreCase(voAttribute)){                                   
                                   if(quotePartsAttributes[attributeCounter].getSQLType() == Types.VARCHAR){
                                       attributePrecision = quotePartsAttributes[attributeCounter].getPrecision(); 
                                       partsAttributeStringType = true;                                        
                                   }                                    
                                       break;
                               }      
                               attributeCounter++;
                           }
                           if(partsAttributeStringType){
                               if(null != value){                                   
                                   if(value.toString().length() > attributePrecision){
                                       uploadFailureReason = "Data size exceeds limits. See: {Column: "+header+", Row No: "+rowcounter+", size limit: "+attributePrecision+"}";
                                       quoteMap.put("uploadStatus", "unsuccessful");
                                       quoteMap.put("uploadFailureReason", uploadFailureReason);
                                       quoteMap.put("incorrectQueueName", incorrectQueueName);
                                       dataPrecisionException = true;
                                       break;
                                   }
                                   else
                                       currentUpdateRow.setAttribute(voAttribute, value);
                               }
                               else
                                   currentUpdateRow.setAttribute(voAttribute, value);                                
                           }
                           else
                              currentUpdateRow.setAttribute(voAttribute, value);
                           
                                partsAttributeStringType = false;
                            } /*else if (header.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[14]) || header.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[16])) {
                                newCell.setCellType(CellType.STRING);

                                value = ((String) (currentCell.getStringCellValue())).trim();
                                if (null != value && !"".equals(value)) {
                                    value = quotePartsMap.get(currentCell.getStringCellValue());
                                }

                                currentUpdateRow.setAttribute(ModelConstants.quotePartsVOAttributes[0], value);
                            }*/
                            if (cctQuotePartsEOView.getEstimatedRowCount() != 0 && null != queueid &&
                                !queueid.equals(ModelConstants.queueIds[9])) {
                                //header.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[14]) || header.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[16])
//                                if ((null != voAttribute &&
//                                     ModelConstants.quotePartsVOAttributes[0].equalsIgnoreCase(voAttribute)))
//                                {
                                    if (!(queueid.toString()
                                                 .trim()
                                                 .equals(currentRowQueueId.trim()))) {
                                        uploadFailureReason =
                                            "Some of the parts does not belong to current queue . Please review all the parts for successful upload. Refer to Line No " +
                                            rowcounter + " for one specific dataset";
                                        mandatoryRecordMissing = true;
                                        quoteMap.put("uploadFailureReason", uploadFailureReason);
                                    }
                                //}
                            }
                            if (cctQuotePartsEOView.getEstimatedRowCount() != 0) {
                                if ((null != voAttribute &&
                                     ModelConstants.quotePartsVOAttributes[0].equalsIgnoreCase(voAttribute)) ||
                                    header.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[14]) || 
                                    header.equalsIgnoreCase(ModelConstants.queueTemplateHeaders[16])) {
                                    if (null != value && !value.equals("") &&
                                        !(value.toString()
                                                                                     .trim()
                                                                                     .equals(currentRowQueueId.trim()))) {
                                        queueMoved = true;
                                    }
                                }
                            }                            
                        } 
                        cell = null;
                        currentCell = null;
                    } //loop two
                    if (dataTypeException || dataPrecisionException)                         
                        break;                    
                    if (queueMoved) {
                        logger.log(Level.INFO, getClass(), "updateQuoteParts", "QueueMoved through upload");
                        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
                        SimpleDateFormat sdtf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        java.util.Date date = new java.util.Date();
                        String datetime = ModelConstants.PART_MOVED_ON + sdf.format(date);
                        currentUpdateRow.setAttribute(ModelConstants.quotePartsVOAttributes[4], datetime);
                        currentUpdateRow.setAttribute(ModelConstants.quotePartsVOAttributes[5], sdtf.format(date));

                    }
                    currentUpdateRow = null;
                }
                if (dataTypeException || dataPrecisionException) {
                    cctQuotePartsIterator.closeRowSetIterator();
                    break;
                }
                cctQuotePartsIterator.closeRowSetIterator();
                if (mandatoryRecordMissing)
                    break;
                rowcounter++;                                        
                cellIteratorInternal2 = null;
                cellIteratorInternal = null;
                cellIterator = null;
                currentRow = null;
            }
            //getDBTransaction().commit();
            if ("N".equalsIgnoreCase(queueID_InCorrect) && !mandatoryRecordMissing && (!dataTypeException && !dataPrecisionException)) {
                quoteMap.put("uploadStatus", "successful");
                quoteMap.put("uploadFailureReason", "");
                quoteMap.put("incorrectQueueName", "");
            } else if (mandatoryRecordMissing) {
                quoteMap.put("uploadFailureReason", uploadFailureReason);
                quoteMap.put("incorrectQueueName", incorrectQueueName);
            }
            quoteMap.put("dataTypeException", dataTypeException);
        } //loopOne
        catch (Exception ex) {
            ex.printStackTrace();
            throw new SystemException("CCT", "updateQuoteParts", "CCT_SYS_EX01", ex);
        }
        //getDBTransaction().commit();
        return quoteMap;
    }

    public BigDecimal getAwdHdrIdByOpptyId(Object opptyid) {
        ViewObjectImpl AwardHeaderVO = getCctAwardHeaderVO1();
        ViewCriteria criteria = AwardHeaderVO.getViewCriteria("CctAwardHeaderVOCriteria");
        AwardHeaderVO.setNamedWhereClauseParam("b_opptyId", opptyid);
        AwardHeaderVO.applyViewCriteria(criteria);
        AwardHeaderVO.executeQuery();
        BigDecimal awdHeaderId = null;
        RowSetIterator awdHeaderIterator = AwardHeaderVO.createRowSetIterator(null);
        while (awdHeaderIterator.hasNext()) {
            Row awardHeaderRow = awdHeaderIterator.next();
            awdHeaderId = new BigDecimal(awardHeaderRow.getAttribute("AwardHeaderId").toString());
        }
        return awdHeaderId;
    }

    public BigDecimal getOpptyIdByOpptyNum(String opptyNum) {
        String s = "select OPPORTUNITY_ID from CCT_OPPORTUNITY where OPPORTUNITY_NUMBER like ?";

        PreparedStatement stmt = getDBTransaction().createPreparedStatement(s, 0);

        ResultSet resultset = null;
        BigDecimal retoppty = null;
        try {
            stmt.setString(1, opptyNum);
          
            resultset = stmt.executeQuery();
           
            while (resultset.next()) {
                retoppty = resultset.getBigDecimal("OPPORTUNITY_ID");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "getOpptyIdByOpptyNum", ex.getMessage());
            throw new SystemException("CCT", "getOpptyIdByOpptyNum", "CCT_SYS_EX10", ex);
        } finally {
            try {
                resultset.close();
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.log(Level.SEVERE, getClass(), "getOpptyIdByOpptyNum", e.getMessage());
                throw new SystemException("CCT", "getOpptyIdByOpptyNum", "CCT_SYS_EX10", e);
            }
        }
        return retoppty;
    }

    public HashMap updateAwards(Sheet datatypeSheet, Object opptyid) {
        HashMap awardsMap = new HashMap();
        generateAwardsUploadTemplate(ModelConstants.AWARDS_REVIEW_UPLOAD);
        awardsMap.put(ModelConstants.UPLOAD_STATUS, "unsuccessful");
        try {
            Iterator<org.apache.poi.ss.usermodel.Row> iterator = datatypeSheet.iterator();
            iterator.next(); //skip first row
            BigDecimal awdHeaderId = getAwdHdrIdByOpptyId(opptyid);

            while (iterator.hasNext()) { // loopone

                org.apache.poi.ss.usermodel.Row currentRow = iterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();
                Iterator<Cell> cellIteratorInternal = currentRow.iterator();
                org.apache.poi.ss.usermodel.Row headerRow = datatypeSheet.getRow(0);
                Object awardedPn = null;

                while (cellIteratorInternal.hasNext()) { //loop two
                    Cell currentCell = cellIteratorInternal.next();
                    Cell cell = headerRow.getCell(currentCell.getColumnIndex());
                    logger.log(Level.INFO, getClass(), "updateAwards", "Cell type= " + cell.getCellTypeEnum());

                    String header = cell.getStringCellValue();
                    String voAttribute = awardsVoAttrMapping.get(uploadAwardsMapping.get(header));
                    if ("AwardedPartNumber".equalsIgnoreCase(voAttribute))
                        awardedPn = currentCell.getStringCellValue();

                }
                ViewObjectImpl cctAwardsVO = getCctAwardsVO1();
                ViewCriteria CctAwardsVOCriteria = cctAwardsVO.getViewCriteria("CctAwardsVOCriteria");
                cctAwardsVO.setNamedWhereClauseParam("b_originalAwardedPartNumber", awardedPn);
                cctAwardsVO.setNamedWhereClauseParam("b_awdHeaderId", awdHeaderId);
                cctAwardsVO.applyViewCriteria(CctAwardsVOCriteria);
                cctAwardsVO.executeQuery();
                logger.log(Level.INFO, getClass(), "updateAwards",
                           "View criteria rows----------" + cctAwardsVO.getEstimatedRowCount());

                RowSetIterator cctAwardsIterator = cctAwardsVO.createRowSetIterator(null);

                //todo
                while (cctAwardsIterator.hasNext()) {
                    Row currentUpdateRow = cctAwardsIterator.next();
                    while (cellIterator.hasNext()) { //loop two
                        Cell currentCell = cellIterator.next();
                        Cell cell = headerRow.getCell(currentCell.getColumnIndex());
                        String header = cell.getStringCellValue();
                        if (null != cell) {
                            String voAttribute = awardsVoAttrMapping.get(uploadAwardsMapping.get(header));
                            Object value = null;
                            if (null != voAttribute) {

                                if (currentCell.getCellTypeEnum() == CellType.STRING)
                                    value = currentCell.getStringCellValue();
                                else if (currentCell.getCellTypeEnum() == CellType.NUMERIC)
                                    value = currentCell.getNumericCellValue();

                                if (header.equalsIgnoreCase("Opportunity #")) {
                                    if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                                        Object opptynum = currentCell.getNumericCellValue();
                                        Double oppty1 = (Double) opptynum;
                                        int oty = oppty1.intValue();
                                       
                                        value = String.valueOf(oty);
                                        BigDecimal opportunityId = getOpptyIdByOpptyNum(String.valueOf(oty));
                                        currentUpdateRow.setAttribute("OpportunityId", opportunityId);
                                    }
                                }
                                if (header.equalsIgnoreCase("Loaded CP#")) {
                                    if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                                        Object cp = currentCell.getNumericCellValue();
                                        Double cpart = (Double) cp;
                                        int certPart = cpart.intValue();
                                        value = String.valueOf(certPart);
                                    }
                                }
                                if ("OpportunityNumber".equalsIgnoreCase(voAttribute)) {
                                    BigDecimal opportunityId = getOpptyIdByOpptyNum((String) value);
                                    currentUpdateRow.setAttribute("OpportunityId", opportunityId);
                                }

                                currentUpdateRow.setAttribute(voAttribute, value);


                            }
                        }
                    }

                }

                cctAwardsIterator.closeRowSetIterator();
            }
            awardsMap.put(ModelConstants.UPLOAD_STATUS, "successful");
            getDBTransaction().commit();
        }

        catch (Exception ex) {
            ex.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "updateAwards", ex.getMessage());
            getDBTransaction().rollback();
            throw new SystemException("CCT", "updateAwards", "CCT_SYS_EX23", ex);
        }
        return awardsMap;
    }


    /**
     * Container's getter for CctAwardsViewVO1.
     * @return CctAwardsViewVO1
     */
    public ViewObjectImpl getCctAwardsViewVO1() {
        return (ViewObjectImpl) findViewObject("CctAwardsViewVO1");
    }


    /**
     * Container's getter for CctProgramCostCatSummaryEOVO1.
     * @return CctProgramCostCatSummaryEOVO1
     */
    public ViewObjectImpl getCctProgramCostCatSummaryEOVO() {
        return (ViewObjectImpl) findViewObject("CctProgramCostCatSummaryEOVO");
    }

    /**
     * Container's getter for CctProgramCostCatTypesEOVO1.
     * @return CctProgramCostCatTypesEOVO1
     */
    public ViewObjectImpl getCctProgramCostCatTypesEOVO() {
        return (ViewObjectImpl) findViewObject("CctProgramCostCatTypesEOVO");
    }

    /**
     * Container's getter for CctProgramCostHeaderEOVO2.
     * @return CctProgramCostHeaderEOVO2
     */
    public CctProgramCostHeaderEOVOImpl getCctProgramCostHeaderEOVO2() {
        return (CctProgramCostHeaderEOVOImpl) findViewObject("CctProgramCostHeaderEOVO2");
    }

    /**
     * Container's getter for CctProgramCostLinesEOVO1.
     * @return CctProgramCostLinesEOVO1
     */
    public ViewObjectImpl getProgramLinesEHS() {
        return (ViewObjectImpl) findViewObject("ProgramLinesEHS");
    }

    /**
     * Container's getter for CctProgramCostLinesEOVO2.
     * @return CctProgramCostLinesEOVO2
     */
    public ViewObjectImpl getProgramLinesImps() {
        return (ViewObjectImpl) findViewObject("ProgramLinesImps");
    }

    /**
     * Container's getter for CctProgramCostLinesEOVO1.
     * @return CctProgramCostLinesEOVO1
     */
    public ViewObjectImpl getProgramLineFct() {
        return (ViewObjectImpl) findViewObject("ProgramLineFct");
    }

    /**
     * Container's getter for CctProgramCostLinesEOVO2.
     * @return CctProgramCostLinesEOVO2
     */
    public ViewObjectImpl getProgramCostLinesOtherService() {
        return (ViewObjectImpl) findViewObject("ProgramCostLinesOtherService");
    }

    /**
     * Container's getter for CctProgramCostLinesEOVO1.
     * @return CctProgramCostLinesEOVO1
     */
    public ViewObjectImpl getProgramCostLinesPar() {
        return (ViewObjectImpl) findViewObject("ProgramCostLinesPar");
    }


    /**
     * Container's getter for AwardsQuotePartsVO1.
     * @return AwardsQuotePartsVO1
     */
    public AwardsQuotePartsVOImpl getAwardsQuotePartsVO1() {
        return (AwardsQuotePartsVOImpl) findViewObject("AwardsQuotePartsVO1");
    }

    /**
     * Container's getter for AwardsReviewVO1.
     * @return AwardsReviewVO1
     */
    public ViewObjectImpl getAwardsReviewVO1() {
        return (ViewObjectImpl) findViewObject("AwardsReviewVO1");
    }

    public HashMap getWSRoleMap() {
        HashMap hashMap = new HashMap();
        hashMap.put((String) ParamCache.userRoleMap.get("CCT_ACCOUNTING_DIRECTOR"), "Accounting Vice President");
        hashMap.put((String) ParamCache.userRoleMap.get("CCT_OPERATIONS_VP"), "Operations Vice President");
        hashMap.put((String) ParamCache.userRoleMap.get("CCT_SALES_VP"), "Sales Level 2");
        hashMap.put((String) ParamCache.userRoleMap.get("CCT_SALES_DIRECTOR"), "Sales Level 1");
        hashMap.put((String) ParamCache.userRoleMap.get("CCT_FINANCE_VP"), "Financial Vice President");
        hashMap.put((String) ParamCache.userRoleMap.get("CCT_CORPORATE_CFO"), "Chief Financial Officer");
        hashMap.put((String) ParamCache.userRoleMap.get("CCT_GROUP_VP_AND_GM"), "Group VP and General Manager");
        hashMap.put((String) ParamCache.userRoleMap.get("CCT_SUPPLY_CHAIN_VP"), "Supply Chain Vice President");
        hashMap.put((String) ParamCache.userRoleMap.get("CCT_SR_CONTRACTS_DIRECTOR"), "Sr Director of Contracts");
        return hashMap;
    }

    public void fetchSnapshots(Integer opportunityId, String opportunityNumber) {

        logger.log(Level.INFO, getClass(), "fetchSnapshots", "Entering fetchSnapshots method");
        logger.log(Level.INFO, getClass(), "fetchSnapshots",
                   "Input parameters are --opportunityId=" + opportunityId + "--opportunityNumber=" +
                   opportunityNumber);
        BigDecimal oppId = BigDecimal.valueOf(opportunityId);
        getValidateApprovalSnapshotStatus(oppId);
        HashMap hashMap = getWSRoleMap();
        logger.log(Level.INFO, getClass(), "fetchSnapshots", "Web Servie Role Map--" + getHashMapString(hashMap));
        //                String userRole=null;
        //                List<String> userRoles = Arrays.asList(ADFContext.getCurrent().getSecurityContext().getUserRoles());
        //                for(String roles:userRoles){
        //                    userRole=roles;
        //                }
                 
        CallableStatement cst = null;
        ResultSet rs = null;
        String stmt = " BEGIN  CCT_APPROVAL_PROCESS.FETCH_SNAPSHOTS (?,?,?); end;";
        logger.log(Level.INFO, getClass(), "fetchSnapshots",
                   "Calling FETCH_SNAPSHOTS procedure using the stmt --" + stmt);
        try {
            cst = this.getDBTransaction().createCallableStatement(stmt, 0);
            cst.setInt(1, null != opportunityId ? opportunityId.intValue() : 0);
            cst.setString(2, (String) ParamCache.userRoleMap.get("CCT_PROPOSAL_MANAGER"));
            cst.registerOutParameter(3, OracleTypes.CURSOR);
            boolean result = cst.execute();
            logger.log(Level.INFO, getClass(), "fetchSnapshots",
                       "Executed FETCH_SNAPSHOTS procedure with resu --" + result);
            rs = ((OracleCallableStatement) cst).getCursor(3);
            //            if(null==rs){
            //                    cst.execute();
            //                    rs = ((OracleCallableStatement) cst).getCursor(3);
            //             }
            ViewObjectImpl viewObjectImpl = null;
            int columnCount = 0;
            if (null != rs) {
                ResultSetMetaData data = rs.getMetaData();
                viewObjectImpl = createDynamicAttDef(data);
                while (rs.next()) {
                    Row createRow = viewObjectImpl.createRow();
                    columnCount = data.getColumnCount();
                    for (int i = 1; i < columnCount + 1; i++) {
                        createRow.setAttribute(data.getColumnName(i), rs.getString(i));
                    }
                    viewObjectImpl.insertRow(createRow);
                }
                try {
                    TaskQueryManagerImpl taskQueryManagerImpl = new TaskQueryManagerImpl();
                    ActiveDirectoryManagerImpl adImpl = new ActiveDirectoryManagerImpl();
                    for (int j = 2; j < columnCount + 1; j++) {
                        int comntIndex=0;
                        StringBuilder comments=new StringBuilder();
                        StringBuilder notecomments=new StringBuilder();
                        String columnName = data.getColumnName(j);
                        String version = columnName.substring(7, columnName.length());
                        BigDecimal snapShotID = getSnapIDonVersion(new BigDecimal(opportunityId), version + ".0");
                        List<Approval> approvalTasksForSnapshot =
                            taskQueryManagerImpl.fetchApprovalTasksForSnapshot(null != opportunityNumber ?
                                                                               opportunityNumber : "",
                                                                               snapShotID.toString());
                        for (Approval approval : approvalTasksForSnapshot) {
                            String approverRole = approval.getApproverRole();
                            String value = null;
                            try {
                                value = (String) hashMap.get(approverRole);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            logger.log(Level.INFO, getClass(), "fetchSnapshots",
                                       "Passing ApproverId to ACtiveDirectoryService method fetchUserDetailsById--ApproverId=" +
                                       approval.getApproverId());
                            String approverName = adImpl.fetchUserDetailsById(approval.getApproverId()).getName();
                            String status = approval.getApprovalStatus(); //
                            String note = approval.getApprovalNote();
                            String approvalDate = approval.getApprovalDate();
                            logger.log(Level.INFO, getClass(), "fetchSnapshots",
                                       "Values returnedfrom Webservie \n 1)ApproverRole=" + approverRole +
                                       "\n 2)Value from hashMap=" + value + "\n 3)approverName from Active Directory=" +
                                       approverName + "\n 4)Status=" + status);
                            logger.log(Level.INFO, getClass(), "fetchSnapshots",
                                       "SuerName from securityContext" +
                                                                                ADFContext.getCurrent()
                                                                                                                             .getSecurityContext()
                                                                                                                             .getUserName()
                                                                                                                             .equalsIgnoreCase(approval.getApproverId()));


                            if ("APPROVE".equalsIgnoreCase(status)) {
                                status = "Approved";
                            }
                            if ("REJECT".equalsIgnoreCase(status))
                                status = "Rejected";
                            if ("CANCEL".equalsIgnoreCase(status))
                                status = "Cancelled by Proposal Manager";
                            if ("PASS".equalsIgnoreCase(status))
                                status = "Approval Passed by Proposal Manager";
                            if ("".equalsIgnoreCase(status))
                                status = "Pending";

                            RowSetIterator approvalSetIterator = getApprovalDynamicVO().createRowSetIterator(null);
                            approvalSetIterator.reset();
                            while (approvalSetIterator.hasNext()) {
                                Row next = approvalSetIterator.next();
                                String snapShots = (String) next.getAttribute(0);
                                if (value.equalsIgnoreCase(snapShots)) {
                                    next.setAttribute(j - 1,
                                                      status + " - " + approverName + " ,Approvals " + "Comments: " +
                                                      note + "<br/>Approved Date: " + approvalDate);
                                    if(null!=note && !"".equalsIgnoreCase(note)){
                                        comntIndex=comntIndex+1;
                                       comments.append(approverName+" - "+note+".\n");
                                        notecomments.append(approverName+" - "+note+".<br/>");
                                    }
                                }
                            }
                            approvalSetIterator.closeRowSetIterator();
                            RowSetIterator commentsRowSet = getApprovalDynamicVO().createRowSetIterator(null);
                            commentsRowSet.reset();
                           // System.out.println("Approval commetns"+comments.toString());
                            while (commentsRowSet.hasNext()) {
                                Row next = commentsRowSet.next();
                                String lable = (String) next.getAttribute(0);
                                if ("Approvers Comments".equalsIgnoreCase(lable)) {
                                    next.setAttribute(j - 1,comments.toString()+ " ,Approvals " +notecomments.toString());
                                }
                            }
                            commentsRowSet.closeRowSetIterator();
                        }
                        if (j == 2) {
                            String approvalStatus = getLatestApprovalStatus(opportunityNumber, version + ".0");
                            disableApproveButtons(approvalStatus);
                        }
                    }
                } catch (Exception e) {
                    //e.printStackTrace();
                    new SystemException("CCT", "updateAwards", "CCT_SYS_EX01", e);
                }
            }
            /*** Don't remove the below iterator code as it will refresh the vo data . Removing this will cause
                 * ORA-00936: missing expression
                ***/
            RowSetIterator createRowSetIterator = getApprovalDynamicVO().createRowSetIterator(null);
            createRowSetIterator.reset();
            while (createRowSetIterator.hasNext()) {
                Row next = createRowSetIterator.next();
            }
            createRowSetIterator.closeRowSetIterator();
        } catch (Exception e) {
            //e.printStackTrace();
            new SystemException("CCT", "updateAwards", "CCT_SYS_EX01", e);
        } finally {
            if (cst != null) {
                try {
                    cst.close();
                } catch (SQLException e) {
                    //e.printStackTrace();
                    new SystemException("CCT", "updateAwards", "CCT_SYS_EX01", e);
                }
            }

        }
        logger.log(Level.INFO, getClass(), "fetchSnapshots", "Exiting fetchSnapshots method");
    }

    /**
     * Container's getter for ApprovalDynamicVO1.
     * @return ApprovalDynamicVO1
     */
    public ViewObjectImpl getApprovalDynamicVO() {
        return (ViewObjectImpl) findViewObject("ApprovalDynamicVO");
    }


    public ViewObjectImpl createDynamicAttDef(ResultSetMetaData metaData) {
        ViewDefImpl viewDefImpl = new ViewDefImpl("com.klx.model.viewObjects.rovo.ApprovalDynamicVO");
        int columnCount = 0;
        try {
            columnCount = metaData.getColumnCount();
            for (int i = 1; i < columnCount + 1; i++) {
                AttributeDefImpl myOne =
                    viewDefImpl.addViewAttribute(metaData.getColumnLabel(i), metaData.getColumnName(i), String.class);
                myOne.setProperty(AttributeHints.ATTRIBUTE_LABEL, metaData.getColumnLabel(i));

                if (i == 2)
                    myOne.setProperty(AttributeHints.ATTRIBUTE_LABEL, "Current Version");


                logger.log(Level.INFO, getClass(), "createDynamicAttDef",
                           "attribute def cl--" + metaData.getColumnLabel(i) + "--CN--" + metaData.getColumnName(i));

            }
        } catch (SQLException e) {
            //e.printStackTrace();
            new SystemException("CCT", "createDynamicAttDef", "CCT_SYS_EX01", e);
        }

        viewDefImpl.resolveDefObject();
        viewDefImpl.registerDefObject();
        ViewObjectImpl approvalDynamic = getApprovalDynamicVO();
        approvalDynamic.remove();
        approvalDynamic = (ViewObjectImpl) createViewObject("ApprovalDynamicVO", viewDefImpl);
        return approvalDynamic;
    }


    public void readyForApproval(String opportunityNumber) {
        CallableStatement cst = null;
        String stmt = ModelConstants.APPROVAL_STMT; //  p_quote_number     IN cct_quote.quote_number%TYPE,
        try { // p_quote_revision   IN cct_quote_revisions.quote_revision%TYPE,
            logger.log(Level.INFO, getClass(), "readyForApproval", "Entering readyForApproval method");

            cst = this.getDBTransaction().createCallableStatement(stmt, 0); //  p_return_status    OUT NOCOPY VARCHAR2
            cst.setString(1, opportunityNumber);
            cst.setString(2, "Approval");
            cst.executeUpdate();
            ReadOnlyOpportunityRVOImpl impl = getReadOnlyOpportunityRVO();
            ViewCriteria criteria = impl.getViewCriteria("OpportunityNumberVC");
            impl.setNamedWhereClauseParam("pOppNum", opportunityNumber);
            impl.applyViewCriteria(criteria);
            impl.executeQuery();
            RowSetIterator createRowSetIterator = impl.createRowSetIterator(null);
            ReadOnlyOpportunityRVORowImpl currentRow = null;
            while (createRowSetIterator.hasNext()) {
                currentRow = (ReadOnlyOpportunityRVORowImpl) createRowSetIterator.next();
            }
            fetchSnapshots(currentRow.getOpportunityId().intValue(), opportunityNumber);
        } catch (Exception e) {
            //   e.printStackTrace();
            new SystemException("CCT", "readyForApproval", "CCT_SYS_EX01", e);
            logger.log(Level.SEVERE, getClass(), "readyForApproval", e.getMessage());
        } finally {
            if (cst != null) {
                try {
                    cst.close();
                } catch (SQLException e) {
                    //    e.printStackTrace();
                    new SystemException("CCT", "readyForApproval", "CCT_SYS_EX01", e);
                    logger.log(Level.SEVERE, getClass(), "readyForApproval", e.getMessage());
                }
            }
        }
        logger.log(Level.INFO, getClass(), "readyForApproval", "Exiting readyForApproval method");

    }


    /**
     * Container's getter for AwardsExceptionVO1.
     * @return AwardsExceptionVO1
     */
    public ViewObjectImpl getAwardsExceptionVO1() {
        return (ViewObjectImpl) findViewObject("AwardsExceptionVO1");
    }

    /**
     * Container's getter for CctAwardsVO1.
     * @return CctAwardsVO1
     */
    public ViewObjectImpl getCctAwardsVO1() {
        return (ViewObjectImpl) findViewObject("CctAwardsVO1");
    }

    /**
     * Container's getter for ProgramCostEOVO1.
     * @return ProgramCostEOVO1
     */
    public ProgramCostEOVOImpl getProgramCostEOVO() {
        return (ProgramCostEOVOImpl) findViewObject("ProgramCostEOVO");
    }

    /**
     * Container's getter for OpportunityToProgramCostVL1.
     * @return OpportunityToProgramCostVL1
     */
    public ViewLinkImpl getOpportunityToProgramCostVL1() {
        return (ViewLinkImpl) findViewLink("OpportunityToProgramCostVL1");
    }

    public String getValidateApprovalSnapshotStatus(BigDecimal oppID) {
        String approval = ModelConstants.SUBMITOSC_VALIDATE_STATUS;
        try {
            ValidateSnapshotRVOImpl validateSnapshotRVO = getValidateSnapshotRVO1();
            validateSnapshotRVO.setNamedWhereClauseParam("P_OPP_ID", oppID);
            validateSnapshotRVO.executeQuery();
            RowSetIterator createRowSetIterator = validateSnapshotRVO.createRowSetIterator(null);
            while (createRowSetIterator.hasNext()) {
                Row next = createRowSetIterator.next();
                approval = (String) next.getAttribute("Status");
            }
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
        return approval;
    }

    public String submitOSC(BigDecimal oppID) {

        String status = ModelConstants.SUBMITOSC_STATUS_SUCCESS;
        //commrnted to test
        try {
            logger.log(Level.INFO, getClass(), "submitOSC", "Entering submitOSC method");
           
            CallableStatement cst = null;
            String stmt = ModelConstants.SUBMITOSC_STMT;
            try {
                cst = this.getDBTransaction().createCallableStatement(stmt, 0);
                cst.setInt(1, null != oppID ? oppID.intValue() : 0);
                cst.executeUpdate();


            } catch (Exception e) {
                status = ModelConstants.SUBMITOSC_STATUS_FAIL;
                e.printStackTrace();
                logger.log(Level.SEVERE, getClass(), "submitOSC", e.getMessage());
                new SystemException("CCT", "submitOSC", "CCT_SYS_EX01", e);
            } finally {
                if (cst != null) {
                    try {
                        cst.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                        logger.log(Level.SEVERE, getClass(), "submitOSC", e.getMessage());
                        new SystemException("CCT", "submitOSC", "CCT_SYS_EX01", e);
                    }
                }
            }

            ReadOnlyOpportunityRVOImpl impl = getReadOnlyOpportunityRVO();
            ViewCriteria criteria = impl.getViewCriteria("OpportunityIdVC");
            impl.setNamedWhereClauseParam("pOppId", oppID);
            impl.applyViewCriteria(criteria);
            impl.executeQuery();
            RowSetIterator createRowSetIterator = impl.createRowSetIterator(null);
            ReadOnlyOpportunityRVORowImpl currentRow = null;
            while (createRowSetIterator.hasNext()) {
                currentRow = (ReadOnlyOpportunityRVORowImpl) createRowSetIterator.next();
            }
           
            createRowSetIterator.closeRowSetIterator();
            UpdateOpportunityManagerClient updateOpportunityManagerClient = new UpdateOpportunityManagerClient();
            BigDecimal netmargin = currentRow.getProjNetMargin();
            BigDecimal divisor = new BigDecimal("100");
            BigDecimal finalNetMargin = new BigDecimal("0");
            if (null != currentRow.getProjNetMargin())
                finalNetMargin = netmargin.divide(divisor, 2, RoundingMode.CEILING);
            /*String data =
                updateOpportunityManagerClient.updateOpportunityData(currentRow.getOscOpportunityId().toString(),
                                                                     currentRow.getAmount(), finalNetMargin, true);
        */
        } catch (Exception e) {
            status = ModelConstants.SUBMITOSC_STATUS_FAIL;
            e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "submitOSC", e.getMessage());
        }

        return status;
    }
    // for Submit to Sales Button in Approval Tab same as SubmitOSC
    public String submitOSCApprovalButton(BigDecimal oppID) {

        String status = ModelConstants.SUBMITOSC_STATUS_SUCCESS;

        try {
            logger.log(Level.INFO, getClass(), "submitOSC", "Entering submitOSCApprovalButton method");

            CallableStatement cst = null;
            String stmt = ModelConstants.SUBMITOSC_STMT_BUTTON;
            try {
                cst = this.getDBTransaction().createCallableStatement(stmt, 0);
                cst.setInt(1, null != oppID ? oppID.intValue() : 0);
                cst.executeUpdate();


            } catch (Exception e) {
                status = ModelConstants.SUBMITOSC_STATUS_FAIL;
                // e.printStackTrace();
                logger.log(Level.SEVERE, getClass(), "submitOSCApprovalButton", e.getMessage());
                new SystemException("CCT", "submitOSC", "CCT_SYS_EX01", e);
            } finally {
                if (cst != null) {
                    try {
                        cst.close();
                    } catch (SQLException e) {
                        //   e.printStackTrace();
                        logger.log(Level.SEVERE, getClass(), "submitOSCApprovalButton", e.getMessage());
                        new SystemException("CCT", "submitOSC", "CCT_SYS_EX01", e);
                    }
                }
            }

            ReadOnlyOpportunityRVOImpl impl = getReadOnlyOpportunityRVO();
            ViewCriteria criteria = impl.getViewCriteria("OpportunityIdVC");
            impl.setNamedWhereClauseParam("pOppId", oppID);
            impl.applyViewCriteria(criteria);
            impl.executeQuery();
            RowSetIterator createRowSetIterator = impl.createRowSetIterator(null);
            ReadOnlyOpportunityRVORowImpl currentRow = null;
            while (createRowSetIterator.hasNext()) {
                currentRow = (ReadOnlyOpportunityRVORowImpl) createRowSetIterator.next();
            }
            createRowSetIterator.closeRowSetIterator();
            String opptyNum = currentRow.getOpportunityNumber();
            EmailUtilityClient emailUtilityClient = new EmailUtilityClient();
            String data =  emailUtilityClient.sendEmailNotification("Submit to Sales Notification", opptyNum, oppID.toString(), "17");
           // UpdateOpportunityManagerClient updateOpportunityManagerClient = new UpdateOpportunityManagerClient();
//            BigDecimal netmargin = currentRow.getProjNetMargin();
//            BigDecimal divisor = new BigDecimal("100");
//            BigDecimal finalNetMargin = new BigDecimal("0");
//            if (null != currentRow.getProjNetMargin())
//                finalNetMargin = netmargin.divide(divisor, 2, RoundingMode.CEILING);
//            String data =
//                updateOpportunityManagerClient.updateOpportunityData(currentRow.getOscOpportunityId().toString(),
//                                                                     currentRow.getAmount(), finalNetMargin, true);
        } catch (Exception e) {
            status = ModelConstants.SUBMITOSC_STATUS_FAIL;
            //   e.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "submitOSCApprovalButton", e.getMessage());
        }

        return status;
    }

    /**
     * Container's getter for ReadOnlyOpportunityRVO1.
     * @return ReadOnlyOpportunityRVO1
     */
    public ReadOnlyOpportunityRVOImpl getReadOnlyOpportunityRVO() {
        return (ReadOnlyOpportunityRVOImpl) findViewObject("ReadOnlyOpportunityRVO");
    }

    /**
     * Container's getter for SnapShotIDROVO1.
     * @return SnapShotIDROVO1
     */
    public ViewObjectImpl getSnapShotIDROVO() {
        return (ViewObjectImpl) findViewObject("SnapShotIDROVO");
    }

    public BigDecimal getLatestSnapID(BigDecimal oppID) {
        BigDecimal snapID = BigDecimal.ZERO;
        ViewObjectImpl dROVO = getSnapShotIDROVO();
        dROVO.setNamedWhereClauseParam("pOpportunityId", oppID);
        dROVO.executeQuery();
        RowSetIterator dROVOIterator = dROVO.createRowSetIterator(null);
        while (dROVOIterator.hasNext()) {
            Row next = dROVOIterator.next();
            snapID = (BigDecimal) next.getAttribute("Snapid");
        }
        return snapID;
    }

    /**
     * Container's getter for BothQuoteAwardRVO1.
     * @return BothQuoteAwardRVO1
     */
    public ViewObjectImpl getBothQuoteAwardRVO() {
        return (ViewObjectImpl) findViewObject("BothQuoteAwardRVO");
    }

    /**
     * Container's getter for AwardedNotQuotedRVO1.
     * @return AwardedNotQuotedRVO1
     */
    public ViewObjectImpl getAwardedNotQuotedRVO() {
        return (ViewObjectImpl) findViewObject("AwardedNotQuotedRVO");
    }

    /**
     * Container's getter for QuotedNotAwardedRVO1.
     * @return QuotedNotAwardedRVO1
     */
    public ViewObjectImpl getQuotedNotAwardedRVO() {
        return (ViewObjectImpl) findViewObject("QuotedNotAwardedRVO");
    }

    public void selectDropDownValues(BigDecimal opptyId) {

        try {

            ViewObject bothQuotedAwarded = getBothQuoteAwardRVO();
            bothQuotedAwarded.setNamedWhereClauseParam("b_opptyid", opptyId);
            bothQuotedAwarded.executeQuery();

            ViewObject quotedNotAwarded = getQuotedNotAwardedRVO();
            quotedNotAwarded.setNamedWhereClauseParam("b_opptyid", opptyId);
            quotedNotAwarded.executeQuery();

            ViewObject awardedNotQuoted = getAwardedNotQuotedRVO();
            awardedNotQuoted.setNamedWhereClauseParam("b_opptyid", opptyId);
            awardedNotQuoted.executeQuery();


        } catch (Exception e) {
            //e.printStackTrace();
            new SystemException("CCT", "submitOSC", "CCT_SYS_EX01", e);
        }

    }


    /**
     * Container's getter for SnapShotROVO1.
     * @return SnapShotROVO1
     */
    public ViewObjectImpl getSnapShotROVO() {
        return (ViewObjectImpl) findViewObject("SnapShotROVO");
    }

    public BigDecimal getSnapIDonVersion(BigDecimal oppID, String versionId) {
        logger.log(Level.INFO, getClass(), "getSnapIDonVersion", "Entering method getSnapIDonVersion");
        logger.log(Level.INFO, getClass(), "getSnapIDonVersion",
                   "Input Parameters are oppID=" + oppID + "--versionId=" + versionId);
        BigDecimal snapshotId = BigDecimal.ZERO;
        ViewObjectImpl snapShotROVO = getSnapShotROVO();
        snapShotROVO.setNamedWhereClauseParam("pOppID", oppID);
        snapShotROVO.setNamedWhereClauseParam("pSnapVersion", versionId);
        snapShotROVO.executeQuery();
        RowSetIterator snapShotIterator = snapShotROVO.createRowSetIterator(null);
        while (snapShotIterator.hasNext()) {
            Row next = snapShotIterator.next();
            snapshotId = (BigDecimal) next.getAttribute("SnapshotId");
        }
        snapShotIterator.closeRowSetIterator();
        logger.log(Level.INFO, getClass(), "getSnapIDonVersion",
                   "Returned value  from getSnapIDonVersion --returning snapshotId=" + snapshotId);
        logger.log(Level.INFO, getClass(), "getSnapIDonVersion", "Exiting method getSnapIDonVersion");
        return snapshotId;
    }


    /**
     * Container's getter for UserAccess1.
     * @return UserAccess1
     */
    public ViewObjectImpl getUserAccess() {
        return (ViewObjectImpl) findViewObject("UserAccess");
    }    

    public void programHeaderCostData(String opportunityId) {
        logger.log(Level.INFO, getClass(), "programHeaderCostData", "Entering programHeaderCostData method");
        long startTime = System.currentTimeMillis();
        long endTime = System.currentTimeMillis();;
       
        ViewObjectImpl programHeaderVO = getCctProgramCostHeaderEOVO2();
        programHeaderVO.reset();
        //programHeaderVO.clearCache();
        ViewCriteria criteria = programHeaderVO.getViewCriteria("CctProgramCostHeaderEOVOCriteria");
        programHeaderVO.setNamedWhereClauseParam("bind_opportunityID", opportunityId);
        programHeaderVO.applyViewCriteria(criteria);
        programHeaderVO.executeQuery();
        Row[] headerRow = programHeaderVO.getAllRowsInRange();
        BigDecimal costHeaderID = BigDecimal.ZERO;
        if (programHeaderVO.getEstimatedRowCount() == 0)
            ADFContext.getCurrent()
                      .getSessionScope()
                      .put("programHeaderId", 0);
        else {
            ADFContext.getCurrent()
                      .getSessionScope()
                      .put("programHeaderId", headerRow[0].getAttribute("ProgramCostHeaderId").toString());
            costHeaderID = new BigDecimal(headerRow[0].getAttribute("ProgramCostHeaderId").toString());
        }
        
        programHeaderVO.reset();
       //programHeaderVO.clearCache();
        ViewObjectImpl programLinesEHS = getProgramLinesEHS();        
        ViewCriteria EHScriteria = programLinesEHS.getViewCriteria("CctProgramCostLinesEOVOCriteria");
        programLinesEHS.setNamedWhereClauseParam(ModelConstants.BIND_CATEGORY, ModelConstants.categoryID[0]);
        programLinesEHS.setNamedWhereClauseParam(ModelConstants.BIND_COST_HEADER, costHeaderID);
        programLinesEHS.applyViewCriteria(EHScriteria);
        programLinesEHS.executeQuery();
        
    //For Notes of EHS Category
       ViewObjectImpl programLinesEHSNotes = getCctProgramCostSummNotesEHSVO();
       programLinesEHSNotes.reset();
       //programLinesEHSNotes.clearCache();
       ViewCriteria EHSNotescriteria = programLinesEHSNotes.getViewCriteria("CctProgramCostSummNotesVOCriteria");
       programLinesEHSNotes.setNamedWhereClauseParam(ModelConstants.BIND_CATEGORY, ModelConstants.categoryID[0]);
       programLinesEHSNotes.setNamedWhereClauseParam(ModelConstants.BIND_COST_HEADER, costHeaderID);
       programLinesEHSNotes.applyViewCriteria(EHSNotescriteria);
       programLinesEHSNotes.executeQuery();        
            
        ViewObjectImpl programCostSubCatVO = getCctProgramCostCatSubtypesEOVO1();
        programCostSubCatVO.reset();
        //programCostSubCatVO.clearCache();
        ViewCriteria programCostCatVC = programCostSubCatVO.getViewCriteria("CctProgramCostCatSubtypesEOVOCriteria");
        programCostSubCatVO.setNamedWhereClauseParam("b_CatId", ModelConstants.categoryID[0]);
        programCostSubCatVO.applyViewCriteria(programCostCatVC);
        programCostSubCatVO.executeQuery();
        //Row[] catTypesRow = programCostSubCatVO.getAllRowsInRange();
        
        RowSetIterator programLinesEHSIterator = programLinesEHS.createRowSetIterator(null);
        RowSetIterator programCostSubCatEHSIterator = programCostSubCatVO.createRowSetIterator(null);

        if (programLinesEHSIterator.next().getAttribute("Category") == null) {
            //int countEHS = 0;
            programLinesEHSIterator = programLinesEHS.createRowSetIterator(null);
            while (programLinesEHSIterator.hasNext()) {
                Row nextEHS = programLinesEHSIterator.next();
                
                if (programCostSubCatEHSIterator.hasNext()) {
                    Row nextEHSSubType = programCostSubCatEHSIterator.next();
                    nextEHS.setAttribute("Category", nextEHSSubType.getAttribute("CategorySubtypeName"));
                    nextEHS.setAttribute("FixedOrRecurring", nextEHSSubType.getAttribute("FixedRecurring"));
                   
                } else {
                    Row nextEHSSubType = programCostSubCatEHSIterator.next();
                    nextEHS.setAttribute("Category", nextEHSSubType.getAttribute("CategorySubtypeName"));
                    nextEHS.setAttribute("FixedOrRecurring", nextEHSSubType.getAttribute("FixedRecurring"));
                   
                    break;
                }

            }
        }
        programLinesEHSIterator.closeRowSetIterator();
        programCostSubCatEHSIterator.closeRowSetIterator();

        ViewObjectImpl programLinesImplementation = getProgramLinesImps();
        programLinesImplementation.reset();
        //programLinesImplementation.clearCache();
        ViewCriteria implementCriteria = programLinesImplementation.getViewCriteria("CctProgramCostLinesEOVOCriteria");
        programLinesImplementation.setNamedWhereClauseParam(ModelConstants.BIND_CATEGORY, ModelConstants.categoryID[1]);
        programLinesImplementation.setNamedWhereClauseParam(ModelConstants.BIND_COST_HEADER, costHeaderID);
        programLinesImplementation.applyViewCriteria(implementCriteria);
        programLinesImplementation.executeQuery();
        
        //For Notes of Imp Category
        ViewObjectImpl programLineImpNotes = getCctProgramCostSummNotesImpVO();
        programLineImpNotes.reset();
        //programLineImpNotes.clearCache();
        ViewCriteria ImpNotescriteria = programLineImpNotes.getViewCriteria("CctProgramCostSummNotesVOCriteria");
        programLineImpNotes.setNamedWhereClauseParam(ModelConstants.BIND_CATEGORY, ModelConstants.categoryID[1]);
        programLineImpNotes.setNamedWhereClauseParam(ModelConstants.BIND_COST_HEADER, costHeaderID);
        programLineImpNotes.applyViewCriteria(ImpNotescriteria);
        programLineImpNotes.executeQuery();
 
        programCostSubCatVO.reset();
        //programCostSubCatVO.clearCache();
        programCostCatVC = programCostSubCatVO.getViewCriteria("CctProgramCostCatSubtypesEOVOCriteria");
        programCostSubCatVO.setNamedWhereClauseParam("b_CatId", ModelConstants.categoryID[1]);
        programCostSubCatVO.applyViewCriteria(programCostCatVC);
        programCostSubCatVO.executeQuery();
        //catTypesRow = programCostSubCatVO.getAllRowsInRange();

        RowSetIterator programLinesImplementationIterator = programLinesImplementation.createRowSetIterator(null);
        RowSetIterator programCostSubCatImplIterator = programCostSubCatVO.createRowSetIterator(null);

        if (null != programLinesImplementationIterator.next() &&
            programLinesImplementationIterator.next().getAttribute("Category") == null) {
            //int countImps = 0;
            programLinesImplementationIterator = programLinesImplementation.createRowSetIterator(null);
            while (programLinesImplementationIterator.hasNext()) {
                Row nextImpl = programLinesImplementationIterator.next();
                if (programCostSubCatImplIterator.hasNext()) {
                    Row nextImplSubType = programCostSubCatImplIterator.next();
                    nextImpl.setAttribute("Category", nextImplSubType.getAttribute("CategorySubtypeName"));
                    nextImpl.setAttribute("FixedOrRecurring", nextImplSubType.getAttribute("FixedRecurring"));

                    
                } else {
                    Row nextImplSubType = programCostSubCatImplIterator.next();
                    nextImpl.setAttribute("Category", nextImplSubType.getAttribute("CategorySubtypeName"));
                    nextImpl.setAttribute("FixedOrRecurring", nextImplSubType.getAttribute("FixedRecurring"));
                    //                                countImps++;
                    break;
                }
            }

        }
        programLinesImplementationIterator.closeRowSetIterator();
        programCostSubCatImplIterator.closeRowSetIterator();

        ViewObjectImpl programLinesServices = getProgramCostLinesOtherService();
        programLinesServices.reset();
        //programLinesServices.clearCache();
        ViewCriteria servicesCriteria = programLinesServices.getViewCriteria("CctProgramCostLinesEOVOCriteria");
        programLinesServices.setNamedWhereClauseParam(ModelConstants.BIND_CATEGORY, ModelConstants.categoryID[2]);
        programLinesServices.setNamedWhereClauseParam(ModelConstants.BIND_COST_HEADER, costHeaderID);
        programLinesServices.applyViewCriteria(servicesCriteria);
        programLinesServices.executeQuery();        
        
        //For Notes of Other Services Category
        ViewObjectImpl programCostOthNotes = getCctProgramCostSummNotesOthVO();
        programCostOthNotes.reset();
        //programCostOthNotes.clearCache();
        ViewCriteria OthNotescriteria = programCostOthNotes.getViewCriteria("CctProgramCostSummNotesVOCriteria");
        programCostOthNotes.setNamedWhereClauseParam(ModelConstants.BIND_CATEGORY, ModelConstants.categoryID[2]);
        programCostOthNotes.setNamedWhereClauseParam(ModelConstants.BIND_COST_HEADER, costHeaderID);
        programCostOthNotes.applyViewCriteria(OthNotescriteria);
        programCostOthNotes.executeQuery();
        
        programCostSubCatVO.reset();
        //programCostSubCatVO.clearCache();
        programCostCatVC = programCostSubCatVO.getViewCriteria("CctProgramCostCatSubtypesEOVOCriteria");
        programCostSubCatVO.setNamedWhereClauseParam("b_CatId", ModelConstants.categoryID[2]);
        programCostSubCatVO.applyViewCriteria(programCostCatVC);
        programCostSubCatVO.executeQuery();
        //catTypesRow = programCostSubCatVO.getAllRowsInRange();

        RowSetIterator programLinesServicesIterator = programLinesServices.createRowSetIterator(null);
        RowSetIterator programCostSubCatServicesIterator = programCostSubCatVO.createRowSetIterator(null);

        if (null != programLinesServicesIterator.next() &&
            programLinesServicesIterator.next().getAttribute("Category") == null) {
            programLinesServicesIterator = programLinesServices.createRowSetIterator(null);
            while (programLinesServicesIterator.hasNext()) {
                Row nextServices = programLinesServicesIterator.next();
                if (programCostSubCatServicesIterator.hasNext()) {
                    Row nextServicesSubType = programCostSubCatServicesIterator.next();
                    nextServices.setAttribute("Category", nextServicesSubType.getAttribute("CategorySubtypeName"));
                    nextServices.setAttribute("FixedOrRecurring", nextServicesSubType.getAttribute("FixedRecurring"));
                } else {
                    Row nextServicesSubType = programCostSubCatServicesIterator.next();
                    nextServices.setAttribute("Category", nextServicesSubType.getAttribute("CategorySubtypeName"));
                    nextServices.setAttribute("FixedOrRecurring", nextServicesSubType.getAttribute("FixedRecurring"));
                    break;
                }
            }
        }
        programLinesServicesIterator.closeRowSetIterator();
        programCostSubCatServicesIterator.closeRowSetIterator();
        
        ViewObjectImpl programLinesFacility = getProgramLineFct();
        programLinesFacility.reset();
        //programLinesFacility.clearCache();
        ViewCriteria facilityCriteria = programLinesFacility.getViewCriteria("CctProgramCostLinesEOVOCriteria");
        programLinesFacility.setNamedWhereClauseParam(ModelConstants.BIND_CATEGORY, ModelConstants.categoryID[3]);
        programLinesFacility.setNamedWhereClauseParam(ModelConstants.BIND_COST_HEADER, costHeaderID);
        programLinesFacility.applyViewCriteria(facilityCriteria);
        programLinesFacility.executeQuery();        
        
        //For Notes of Facility Services Category
       ViewObjectImpl programCostFacNotes = getCctProgramCostSummNotesFacVO();
       programCostFacNotes.reset();
       //programCostFacNotes.clearCache();
       ViewCriteria FacNotescriteria = programCostFacNotes.getViewCriteria("CctProgramCostSummNotesVOCriteria");
       programCostFacNotes.setNamedWhereClauseParam(ModelConstants.BIND_CATEGORY, ModelConstants.categoryID[3]);
       programCostFacNotes.setNamedWhereClauseParam(ModelConstants.BIND_COST_HEADER, costHeaderID);
       programCostFacNotes.applyViewCriteria(FacNotescriteria);
       programCostFacNotes.executeQuery();
        
        programCostSubCatVO.reset();
        //programCostSubCatVO.clearCache();
        programCostCatVC = programCostSubCatVO.getViewCriteria("CctProgramCostCatSubtypesEOVOCriteria");
        programCostSubCatVO.setNamedWhereClauseParam("b_CatId", ModelConstants.categoryID[3]);
        programCostSubCatVO.applyViewCriteria(programCostCatVC);
        programCostSubCatVO.executeQuery();
        // catTypesRow = programCostSubCatVO.getAllRowsInRange();

        RowSetIterator programLinesFacilityIterator = programLinesFacility.createRowSetIterator(null);
        RowSetIterator programCostSubCatFacilityIterator = programCostSubCatVO.createRowSetIterator(null);

        if (null != programLinesFacilityIterator.next() &&
            programLinesFacilityIterator.next().getAttribute("Category") == null) {
            programLinesFacilityIterator = programLinesFacility.createRowSetIterator(null);
            while (programLinesFacilityIterator.hasNext()) {
                Row nextFacility = programLinesFacilityIterator.next();
                if (programCostSubCatFacilityIterator.hasNext()) {
                    Row nextFacilitySubType = programCostSubCatFacilityIterator.next();
                    nextFacility.setAttribute("Category", nextFacilitySubType.getAttribute("CategorySubtypeName"));
                    nextFacility.setAttribute("FixedOrRecurring", nextFacilitySubType.getAttribute("FixedRecurring"));
                } else {
                    Row nextFacilitySubType = programCostSubCatFacilityIterator.next();
                    nextFacility.setAttribute("Category", nextFacilitySubType.getAttribute("CategorySubtypeName"));
                    nextFacility.setAttribute("FixedOrRecurring", nextFacilitySubType.getAttribute("FixedRecurring"));
                    break;
                }
            }
        }
        programLinesFacilityIterator.closeRowSetIterator();
        programCostSubCatFacilityIterator.closeRowSetIterator();

        ViewObjectImpl programLinesPersonnel = getProgramCostLinesPar();
        programLinesPersonnel.reset();
        //programLinesPersonnel.clearCache();
        ViewCriteria personnelCriteria = programLinesPersonnel.getViewCriteria("CctProgramCostLinesEOVOCriteria");
        programLinesPersonnel.setNamedWhereClauseParam(ModelConstants.BIND_CATEGORY, ModelConstants.categoryID[4]);
        programLinesPersonnel.setNamedWhereClauseParam(ModelConstants.BIND_COST_HEADER, costHeaderID);
        programLinesPersonnel.applyViewCriteria(personnelCriteria);
        programLinesPersonnel.executeQuery();        
        
        //For Notes of Personnel Category
       ViewObjectImpl programCostPerNotes = getCctProgramCostSummNotesPerVO();
       programCostPerNotes.reset();
       //programCostPerNotes.clearCache();
       ViewCriteria PerNotescriteria = programCostPerNotes.getViewCriteria("CctProgramCostSummNotesVOCriteria");
       programCostPerNotes.setNamedWhereClauseParam(ModelConstants.BIND_CATEGORY, ModelConstants.categoryID[4]);
       programCostPerNotes.setNamedWhereClauseParam(ModelConstants.BIND_COST_HEADER, costHeaderID);
       programCostPerNotes.applyViewCriteria(PerNotescriteria);
       programCostPerNotes.executeQuery();
       
        programCostSubCatVO.reset();
        //programCostSubCatVO.clearCache();
        programCostCatVC = programCostSubCatVO.getViewCriteria("CctProgramCostCatSubtypesEOVOCriteria");
        programCostSubCatVO.setNamedWhereClauseParam("b_CatId", ModelConstants.categoryID[4]);
        programCostSubCatVO.applyViewCriteria(programCostCatVC);
        programCostSubCatVO.executeQuery();
        //catTypesRow = programCostSubCatVO.getAllRowsInRange();

        RowSetIterator programLinesPersonnelIterator = programLinesPersonnel.createRowSetIterator(null);
        RowSetIterator programCostSubCatPersonnelIterator = programCostSubCatVO.createRowSetIterator(null);

        if (null != programLinesPersonnelIterator.next() &&
            programLinesPersonnelIterator.next().getAttribute("Category") == null) {
            programLinesPersonnelIterator = programLinesPersonnel.createRowSetIterator(null);
            while (programLinesPersonnelIterator.hasNext()) {
                Row nextPersonnel = programLinesPersonnelIterator.next();
                if (programCostSubCatPersonnelIterator.hasNext()) {
                    Row nextPersonnelSubType = programCostSubCatPersonnelIterator.next();
                    nextPersonnel.setAttribute("Category", nextPersonnelSubType.getAttribute("CategorySubtypeName"));
                    nextPersonnel.setAttribute("FixedOrRecurring", nextPersonnelSubType.getAttribute("FixedRecurring"));
                } else {
                    Row nextPersonnelSubType = programCostSubCatPersonnelIterator.next();
                    nextPersonnel.setAttribute("Category", nextPersonnelSubType.getAttribute("CategorySubtypeName"));
                    nextPersonnel.setAttribute("FixedOrRecurring", nextPersonnelSubType.getAttribute("FixedRecurring"));

                    break;
                }
            }
        }
        programLinesPersonnelIterator.closeRowSetIterator();
        programCostSubCatPersonnelIterator.closeRowSetIterator();       

        //getDBTransaction().commit();
        //programLinesEHS.clearCache();
        //programLinesImplementation.clearCache();
        endTime = System.currentTimeMillis();
        logger.log(Level.INFO, getClass(), "programHeaderCostData", "Total time to execute: "+((endTime - startTime) / 1000)+" secs");
        logger.log(Level.INFO, getClass(), "programHeaderCostData", "Exiting programHeaderCostData method");
    }    

    public boolean setValueToNotesSumm(BigDecimal costHeaderID, String EhsNotes, String ImpNotes, String OthNotes,
                                    String FacNotes, String PerNotes, BigDecimal opportunityID, BigDecimal totalProgramCosts) {
        logger.log(Level.INFO, getClass(), "setValueToNotesSumm", "Entering setValueToNotesSumm method");
        boolean dataException = false;
        try{
        ViewObjectImpl programHeaderVO = getCctProgramCostHeaderEOVO2();
        programHeaderVO.reset();        
        programHeaderVO.executeQuery();
        /*ViewObject programCostVO = getProgramCostEOVO1();
        programCostVO.reset();        
        programCostVO.setNamedWhereClauseParam("b_opp_id", opportunityID);        
        programCostVO.executeQuery();       
        RowSetIterator createRowSetIterator = programCostVO.createRowSetIterator(null);
        ProgramCostEOVORowImpl currentRow = null;
        while (createRowSetIterator.hasNext()) {
            currentRow = (ProgramCostEOVORowImpl) createRowSetIterator.next();
        }
        if(null != currentRow)
            currentRow.setTotalProgramCosts(totalProgramCosts);
        createRowSetIterator.closeRowSetIterator();*/
        //currentRow.
        ViewObjectImpl programLinesEHSNotes = getCctProgramCostSummNotesEHSVO();
        programLinesEHSNotes.reset();
        //programLinesEHSNotes.clearCache();
        ViewCriteria EHSNotescriteria = programLinesEHSNotes.getViewCriteria("CctProgramCostSummNotesVOCriteria");
        programLinesEHSNotes.setNamedWhereClauseParam(ModelConstants.BIND_CATEGORY, ModelConstants.categoryID[0]);
        programLinesEHSNotes.setNamedWhereClauseParam(ModelConstants.BIND_COST_HEADER, costHeaderID);
        programLinesEHSNotes.applyViewCriteria(EHSNotescriteria);
        programLinesEHSNotes.executeQuery();

        RowSetIterator programLinesEHSNotesIterator = programLinesEHSNotes.createRowSetIterator(null);
        while (programLinesEHSNotesIterator.hasNext()) {
            Row r = programLinesEHSNotesIterator.next();
            if (EhsNotes != null) {
                r.setAttribute("Notes", EhsNotes);
                logger.log(Level.INFO, getClass(), "setValueToNotesSumm method",
                           "EHS Notes are set ----- " + r.getAttribute("Notes"));
                break;
            }
        }
        programLinesEHSNotesIterator.closeRowSetIterator();

        ViewObjectImpl programLineImpNotes = getCctProgramCostSummNotesImpVO();
        programLineImpNotes.reset();
       // programLineImpNotes.clearCache();
        ViewCriteria ImpNotescriteria = programLineImpNotes.getViewCriteria("CctProgramCostSummNotesVOCriteria");
        programLineImpNotes.setNamedWhereClauseParam(ModelConstants.BIND_CATEGORY, ModelConstants.categoryID[1]);
        programLineImpNotes.setNamedWhereClauseParam(ModelConstants.BIND_COST_HEADER, costHeaderID);
        programLineImpNotes.applyViewCriteria(ImpNotescriteria);
        programLineImpNotes.executeQuery();

        RowSetIterator programLineImpNotesIterator = programLineImpNotes.createRowSetIterator(null);
        while (programLineImpNotesIterator.hasNext()) {
            Row r = programLineImpNotesIterator.next();
            if (ImpNotes != null) {
                r.setAttribute("Notes", ImpNotes);
                logger.log(Level.INFO, getClass(), "setValueToNotesSumm method",
                           "Imp Notes are set ----- " + r.getAttribute("Notes"));
                break;
            }
        }
        programLineImpNotesIterator.closeRowSetIterator();

        ViewObjectImpl programCostOthNotes = getCctProgramCostSummNotesOthVO();
        programCostOthNotes.reset();
        //programCostOthNotes.clearCache();
        ViewCriteria OthNotescriteria = programCostOthNotes.getViewCriteria("CctProgramCostSummNotesVOCriteria");
        programCostOthNotes.setNamedWhereClauseParam(ModelConstants.BIND_CATEGORY, ModelConstants.categoryID[2]);
        programCostOthNotes.setNamedWhereClauseParam(ModelConstants.BIND_COST_HEADER, costHeaderID);
        programCostOthNotes.applyViewCriteria(OthNotescriteria);
        programCostOthNotes.executeQuery();

        RowSetIterator programCostOthNotesIterator = programCostOthNotes.createRowSetIterator(null);
        while (programCostOthNotesIterator.hasNext()) {
            Row r = programCostOthNotesIterator.next();
            if (OthNotes != null) {
                r.setAttribute("Notes", OthNotes);
                logger.log(Level.INFO, getClass(), "setValueToNotesSumm method",
                           "Oth Notes are set ----- " + r.getAttribute("Notes"));
                break;
            }
        }
        programCostOthNotesIterator.closeRowSetIterator();

        ViewObjectImpl programCostFacNotes = getCctProgramCostSummNotesFacVO();
        programCostFacNotes.reset();
        //programCostFacNotes.clearCache();
        ViewCriteria FacNotescriteria = programCostFacNotes.getViewCriteria("CctProgramCostSummNotesVOCriteria");
        programCostFacNotes.setNamedWhereClauseParam(ModelConstants.BIND_CATEGORY, ModelConstants.categoryID[3]);
        programCostFacNotes.setNamedWhereClauseParam(ModelConstants.BIND_COST_HEADER, costHeaderID);
        programCostFacNotes.applyViewCriteria(FacNotescriteria);
        programCostFacNotes.executeQuery();

        RowSetIterator programCostFacNotesIterator = programCostFacNotes.createRowSetIterator(null);
        while (programCostFacNotesIterator.hasNext()) {
            Row r = programCostFacNotesIterator.next();
            if (FacNotes != null) {
                r.setAttribute("Notes", FacNotes);
                logger.log(Level.INFO, getClass(), "setValueToNotesSumm method",
                           "Fac Notes are set ----- " + r.getAttribute("Notes"));
                break;
            }
        }
        programCostFacNotesIterator.closeRowSetIterator();

        ViewObjectImpl programCostPerNotes = getCctProgramCostSummNotesPerVO();
        programCostPerNotes.reset();
        //programCostPerNotes.clearCache();
        ViewCriteria PerNotescriteria = programCostPerNotes.getViewCriteria("CctProgramCostSummNotesVOCriteria");
        programCostPerNotes.setNamedWhereClauseParam(ModelConstants.BIND_CATEGORY, ModelConstants.categoryID[4]);
        programCostPerNotes.setNamedWhereClauseParam(ModelConstants.BIND_COST_HEADER, costHeaderID);
        programCostPerNotes.applyViewCriteria(PerNotescriteria);
        programCostPerNotes.executeQuery();

        RowSetIterator programCostPerNotesIterator = programCostPerNotes.createRowSetIterator(null);
        while (programCostPerNotesIterator.hasNext()) {
            Row r = programCostPerNotesIterator.next();
            if (PerNotes != null) {
                r.setAttribute("Notes", PerNotes);
                logger.log(Level.INFO, getClass(), "setValueToNotesSumm method",
                           "Per Notes are set ----- " + r.getAttribute("Notes"));
                break;
            }
        }
        programCostPerNotesIterator.closeRowSetIterator();

        getDBTransaction().commit();
        
       // programCostVO.reset();
        //programCostVO.executeQuery();
        ViewObjectImpl programLinesEHS = getProgramLinesEHS();        
        programLinesEHS.executeQuery();
        ViewObjectImpl programLinesImplementation = getProgramLinesImps();
        programLinesImplementation.executeQuery();
        ViewObjectImpl programLinesServices = getProgramCostLinesOtherService();
        programLinesServices.executeQuery();
        ViewObjectImpl programLinesFacility = getProgramLineFct();
        programLinesFacility.executeQuery();
        ViewObjectImpl programLinesPersonnel = getProgramCostLinesPar();
        programLinesPersonnel.executeQuery();
        }catch(Exception ae){
            ae.printStackTrace();
            dataException = true;
        }
        logger.log(Level.INFO, getClass(), "setValueToNotesSumm", "Exiting setValueToNotesSumm method");
        return dataException;
    }

    /**
     * Container's getter for ApprovalButtonsROVO1.
     * @return ApprovalButtonsROVO1
     */
    public ViewObjectImpl getApprovalButtonsROVO() {
        return (ViewObjectImpl) findViewObject("ApprovalButtonsROVO");
    }

    public void getTop50FinalPricingSummary(String opportunityNumber){
        LatestRevisonROVOImpl latestRevisonROVO = getLatestRevisonROVO();
        latestRevisonROVO.setpOpportunityNumber(opportunityNumber);
        latestRevisonROVO.executeQuery();
        logger.log(Level.INFO, getClass(), "finalQuoteRevisionID",
                   "quote no in finalQuoteRevisionID" + latestRevisonROVO.getEstimatedRowCount());

        RowSetIterator createRowSetIterator = latestRevisonROVO.createRowSetIterator(null);
        Object revisionId = null;
        BigDecimal quoteID = BigDecimal.ZERO;
        while (createRowSetIterator.hasNext()) {
            Row next = createRowSetIterator.next();
            revisionId = next.getAttribute("RevisionId");
            quoteID = new BigDecimal(next.getAttribute("QuoteId").toString());
            logger.log(Level.INFO, getClass(), "finalQuoteRevisionID", "Quote revisionId ----- " + revisionId);
        }
        createRowSetIterator.closeRowSetIterator();
        CctTop50FinalPricingSummaryImpl top50FinalPricingROVO = getCctTop50FinalPricingSummary1();
        top50FinalPricingROVO.setbind_QuoteRevisionId(revisionId.toString());        
        top50FinalPricingROVO.executeQuery();
//        RowSetIterator createRowSetIterator2 = top50FinalPricingROVO.createRowSetIterator(null);
//       
//        while (createRowSetIterator2.hasNext()) {
//            Row next = createRowSetIterator2.next();
//            System.out.println("Top 50 Resale value ==="+next.getAttribute("Top50ResaleValue").toString());
//            System.out.println("Top %0 Cost value ==="+next.getAttribute("Top50CostValue").toString());
//            
//        }
    }
    public void disableApproveButtons(String approveStatus) {
        //    NOT INITIATED,REJECTED,IN PROGRESS,PASSED BY PROPOSAL MANAGER
        ViewObjectImpl errorPageVO = getApprovalButtonsROVO();
        RowSetIterator iterator = errorPageVO.createRowSetIterator(null);
        while (iterator.hasNext()) {
            Row currentRow = iterator.next();
            try {
                if ("NOT INITIATED".equalsIgnoreCase(approveStatus)) {
                    currentRow.setAttribute("TriggerButton", "false");
                    currentRow.setAttribute("PassButton", "false");
                    currentRow.setAttribute("CancelButton", "true");
                    currentRow.setAttribute("EditButton", "false");
                } else if ("NO DATA".equalsIgnoreCase(approveStatus)) {
                    currentRow.setAttribute("TriggerButton", "false");
                    currentRow.setAttribute("PassButton", "true");
                    currentRow.setAttribute("CancelButton", "true");
                    currentRow.setAttribute("EditButton", "true");
                } else if ("IN PROGRESS".equalsIgnoreCase(approveStatus) ||
                           "REJECTED".equalsIgnoreCase(approveStatus)) {
                    currentRow.setAttribute("TriggerButton", "true");
                    currentRow.setAttribute("PassButton", "true");
                    currentRow.setAttribute("CancelButton", "false");
                    currentRow.setAttribute("EditButton", "true");
                } else {
                    currentRow.setAttribute("TriggerButton", "true");
                    currentRow.setAttribute("PassButton", "true");
                    currentRow.setAttribute("CancelButton", "true");
                    currentRow.setAttribute("EditButton", "true");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        iterator.closeRowSetIterator();
    }

    /**
     * Container's getter for ApprovalLDOAAuditVO1.
     * @return ApprovalLDOAAuditVO1
     */
    public ViewObjectImpl getApprovalLDOAAuditVO1() {
        return (ViewObjectImpl) findViewObject("ApprovalLDOAAuditVO1");
    }

    /**
     * Container's getter for ApprovalRulesVO1.
     * @return ApprovalRulesVO1
     */
    public ViewObjectImpl getApprovalRulesVO1() {
        return (ViewObjectImpl) findViewObject("ApprovalRulesVO1");
    }


    /**
     * Container's getter for CustomerPNHistoryVO1.
     * @return CustomerPNHistoryVO1
     */
    public ViewObjectImpl getCustomerPNHistoryVO1() {
        return (ViewObjectImpl) findViewObject("CustomerPNHistoryVO1");
    }

    /**
     * Container's getter for showOpptyVO1.
     * @return showOpptyVO1
     */
    public ViewObjectImpl getshowOpptyVO1() {
        return (ViewObjectImpl) findViewObject("showOpptyVO1");
    }

    /**
     * Container's getter for CctAwardHeaderVO1.
     * @return CctAwardHeaderVO1
     */
    public ViewObjectImpl getCctAwardHeaderVO1() {
        return (ViewObjectImpl) findViewObject("CctAwardHeaderVO1");
    }


    /**
     * Container's getter for UploadErrorsTR2.
     * @return UploadErrorsTR2
     */
    public UploadErrorsTRVOImpl getUploadErrorsTrVo() {
        return (UploadErrorsTRVOImpl) findViewObject("UploadErrorsTrVo");
    }

    public void createErroTRVO() {
        ViewObjectImpl errorsROVOImpl = createErrorDynamicVO();
        Row createRow = errorsROVOImpl.createRow();
        createRow.setAttribute(ModelConstants.UEA_SI_NO, 1);
        createRow.setAttribute(ModelConstants.UEA_COLUMN_HEADER, "col 1");
        createRow.setAttribute(ModelConstants.UEA_DESCRIPTION, "desc ");
        createRow.setAttribute(ModelConstants.UEA_CELL_VALUE, "weerr string instead of number");
        errorsROVOImpl.insertRow(createRow);
        RowSetIterator createRowSetIterator = getUploadErrorROVO().createRowSetIterator(null); //
        createRowSetIterator.reset();
        while (createRowSetIterator.hasNext()) {
            Row next = createRowSetIterator.next();
        }
        createRowSetIterator.closeRowSetIterator();
       

    }

    public String validateUploadedFile(Sheet datatypeSheet) {
        String status = "success";
        generateTemplate("BEACON_FILE");
        ViewObject cctQuotePartsView1 = getCctQuotePartsEOView();
        ViewObjectImpl errorsROVOImpl = createErrorDynamicVO();
        BigDecimal sINO = BigDecimal.ZERO;
        try {
            Iterator<org.apache.poi.ss.usermodel.Row> iterator = datatypeSheet.iterator();
            iterator.next(); //skip first row
            while (iterator.hasNext()) { // loopone
                org.apache.poi.ss.usermodel.Row currentRow = iterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();
                while (cellIterator.hasNext()) { //loop two
                    Cell currentCell = cellIterator.next();
                    org.apache.poi.ss.usermodel.Row headerRow = datatypeSheet.getRow(0);
                    Cell cell = headerRow.getCell(currentCell.getColumnIndex());
                    String header = cell.getStringCellValue();
                    boolean isMandatory = false;
                    if (null != mandatoryFiledMap.get(header) && "Y".equalsIgnoreCase(mandatoryFiledMap.get(header)))
                        isMandatory = true;
                    if (null != cell) {
                        String voAttribute = quoteVoAttrMapping.get(templateMapping.get(header));
                        Object value = null;
                        if (null != voAttribute) {
                            AttributeDef attributeDef =
                                cctQuotePartsView1.getAttributeDef(cctQuotePartsView1.getAttributeIndexOf(voAttribute));
                            Object attributeDataType = attributeDef.getJavaType();
                            if (currentCell.getCellTypeEnum() == CellType.BLANK && isMandatory) {
                                status = "failure";
                                Row createRow = errorsROVOImpl.createRow();
                                createRow.setAttribute(ModelConstants.UEA_SI_NO, sINO.add(BigDecimal.ONE));
                                createRow.setAttribute(ModelConstants.UEA_COLUMN_HEADER, header);
                                createRow.setAttribute(ModelConstants.UEA_DESCRIPTION, "desc");
                                createRow.setAttribute(ModelConstants.UEA_CELL_VALUE, "It is mandtory");
                                errorsROVOImpl.insertRow(createRow);
                            } else if ("class java.math.BigDecimal".equalsIgnoreCase(attributeDataType.toString())) {
                                if (currentCell.getCellTypeEnum() != CellType.NUMERIC) {
                                    //    value = currentCell.getString();
                                    status = "failure";
                                    Row createRow = errorsROVOImpl.createRow();
                                    createRow.setAttribute(ModelConstants.UEA_SI_NO, sINO.add(BigDecimal.ONE));
                                    createRow.setAttribute(ModelConstants.UEA_COLUMN_HEADER, header);
                                    createRow.setAttribute(ModelConstants.UEA_DESCRIPTION, "new desc");
                                    createRow.setAttribute(ModelConstants.UEA_CELL_VALUE,
                                                           "Give string instead of number");
                                    errorsROVOImpl.insertRow(createRow);
                                }
                            }
                            if ("FirstPlanPurday".equalsIgnoreCase(voAttribute) ||
                                "LtaExpDate".equalsIgnoreCase(voAttribute)) {
                                if (currentCell.getCellTypeEnum() != CellType.BLANK) {
                                    value = new SimpleDateFormat("MM/dd/yyyy").parse(currentCell.getStringCellValue());
                                }
                            }
                        }
                    }
                } //loop two
            }
        } //loopOne

        catch (Exception ex) {
            ex.printStackTrace();
        }
        RowSetIterator createRowSetIterator = getUploadErrorROVO().createRowSetIterator(null); //
        createRowSetIterator.reset();
        while (createRowSetIterator.hasNext()) {
            Row next = createRowSetIterator.next();
        }
        createRowSetIterator.closeRowSetIterator();
        return status;
    }


    public List<String> getApprovalUsers(HashMap hashMap) {
       // updatePaymentTerm(new BigDecimal((Integer) hashMap.get("snapShotId")), (Boolean) hashMap.get("flag")); //As Incoterm flag is not being used in approval tab
        Map<Integer, String> userMap = new TreeMap<Integer, String>();
        ArrayList<String> userList=new ArrayList<String>();
        CallableStatement cst = null;
        ResultSet rs = null;
        String stmt = " BEGIN CCT_APPROVAL_PROCESS.get_approval_user(?,?,?); END;";
        CctApprovalType cctApprovalType;
        try {
            cctApprovalType = new CctApprovalType();
            cctApprovalType.setOpportunityNumber((String) hashMap.get("opportunityNumber")); //"66997"
            cctApprovalType.setSnapshotId(new BigDecimal((Integer) hashMap.get("snapShotId"))); //41
            cctApprovalType.setSource("BPM");
            cctApprovalType.setAction("CREATE");
            cst = this.getDBTransaction().createCallableStatement(stmt, 0);
            cst.setObject(1, cctApprovalType);
            cst.registerOutParameter(2, OracleTypes.CURSOR);
            cst.registerOutParameter(3, Types.NUMERIC);
            cst.execute();
            rs = ((OracleCallableStatement) cst).getCursor(2);
            if (null != rs) {
                ActiveDirectoryManagerImpl adImpl = new ActiveDirectoryManagerImpl();
                while (rs.next()) {
                    String approvalRole = rs.getString(1);
                    String approvalUser = rs.getString(2);
                    String approverName = adImpl.fetchUserDetailsById(approvalUser).getName();
                    HashMap wsMap = getWSRoleMap();
                    if (null != approvalUser && null!=approvalRole) {
                        String roleGroup=(String)getKeyFromValue(wsMap, approvalRole);
                        if(((String)ParamCache.userRoleMap.get(ModelConstants.CCT_SALES_DIRECTOR)).equalsIgnoreCase(roleGroup))
                        userMap.put(1,approvalRole + "-" + approverName);
                        if(((String)ParamCache.userRoleMap.get(ModelConstants.CCT_SALES_VP)).equalsIgnoreCase(roleGroup))
                        userMap.put(2,approvalRole + "-" + approverName);
                        if(((String)ParamCache.userRoleMap.get(ModelConstants.CCT_GROUP_VP_AND_GM)).equalsIgnoreCase(roleGroup))
                        userMap.put(3,approvalRole + "-" + approverName);
                        if(((String)ParamCache.userRoleMap.get(ModelConstants.CCT_ACCOUNTING_DIRECTOR)).equalsIgnoreCase(roleGroup))
                        userMap.put(4,approvalRole + "-" + approverName);
                        if(((String)ParamCache.userRoleMap.get(ModelConstants.CCT_FINANCE_VP)).equalsIgnoreCase(roleGroup))
                        userMap.put(5,approvalRole + "-" + approverName);
                        if(((String)ParamCache.userRoleMap.get(ModelConstants.CCT_OPERATIONS_VP)).equalsIgnoreCase(roleGroup))
                        userMap.put(6,approvalRole + "-" + approverName);
                        if(((String)ParamCache.userRoleMap.get(ModelConstants.CCT_CORPORATE_CFO)).equalsIgnoreCase(roleGroup))
                        userMap.put(7,approvalRole + "-" + approverName);
                        if(((String)ParamCache.userRoleMap.get(ModelConstants.CCT_SUPPLY_CHAIN_VP)).equalsIgnoreCase(roleGroup))
                        userMap.put(8,approvalRole + "-" + approverName);
                        if(((String)ParamCache.userRoleMap.get(ModelConstants.CCT_SR_CONTRACTS_DIRECTOR)).equalsIgnoreCase(roleGroup))
                        userMap.put(9,approvalRole + "-" + approverName);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (Map.Entry<Integer, String> entry : userMap.entrySet()) {
             userList.add(entry.getValue());
        }
        return userList;
    }

    public ViewObjectImpl createErrorDynamicVO() {
        ViewDefImpl viewDefImpl = new ViewDefImpl("com.klx.model.viewObjects.rovo.UploadErrorROVO");
        AttributeDefImpl siNo =
            viewDefImpl.addViewAttribute(ModelConstants.UEA_SI_NO, ModelConstants.UEA_SI_NO, String.class);
        siNo.setProperty(AttributeHints.ATTRIBUTE_LABEL, ModelConstants.UEA_SI_NO);
        AttributeDefImpl columnHeader =
            viewDefImpl.addViewAttribute(ModelConstants.UEA_COLUMN_HEADER, ModelConstants.UEA_COLUMN_HEADER,
                                         String.class);
        columnHeader.setProperty(AttributeHints.ATTRIBUTE_LABEL, ModelConstants.UEA_COLUMN_HEADER);
        AttributeDefImpl description =
            viewDefImpl.addViewAttribute(ModelConstants.UEA_DESCRIPTION, ModelConstants.UEA_DESCRIPTION, String.class);
        description.setProperty(AttributeHints.ATTRIBUTE_LABEL, ModelConstants.UEA_DESCRIPTION);
        AttributeDefImpl cellValue =
            viewDefImpl.addViewAttribute(ModelConstants.UEA_CELL_VALUE, ModelConstants.UEA_CELL_VALUE, String.class);
        cellValue.setProperty(AttributeHints.ATTRIBUTE_LABEL, ModelConstants.UEA_CELL_VALUE);
        viewDefImpl.resolveDefObject();
        viewDefImpl.registerDefObject();
        ViewObjectImpl uploadErrorROVO = getUploadErrorROVO();
        uploadErrorROVO.remove();
        uploadErrorROVO = (ViewObjectImpl) createViewObject("UploadErrorROVO", viewDefImpl);
        return uploadErrorROVO;
    }


    /**
     * Container's getter for AwardHeadersVVO1.
     * @return AwardHeadersVVO1
     */
    public AwardHeadersVVOImpl getAwardHeadersVVO1() {
        return (AwardHeadersVVOImpl) findViewObject("AwardHeadersVVO1");
    }

    /**
     * Container's getter for UploadErrorRO1.
     * @return UploadErrorRO1
     */

    public ViewObjectImpl getUploadErrorROVO() {
        return (ViewObjectImpl) findViewObject("UploadErrorROVO");
    }

    public void updatePaymentTerm(BigDecimal sanpShotID, boolean flag) {
        String paymentValue = "NO";
        if (flag)
            paymentValue = "YES";
        String query =
            "UPDATE CCT_SNAPSHOT SET INCLUDE_PMT_TERM='" + paymentValue + "' WHERE SNAPSHOT_ID=" + sanpShotID;
        PreparedStatement stmt = getDBTransaction().createPreparedStatement(query, 0);
        try {
            stmt.executeUpdate();
            getDBTransaction().commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Container's getter for CctAwardHeaderVO2.
     * @return CctAwardHeaderVO2
     */
    public ViewObjectImpl getCctAwardHeaderVO2() {
        return (ViewObjectImpl) findViewObject("CctAwardHeaderVO2");
    }


    public String getOpportunityID(String opportunityNumber) {
        CallableStatement cst = null;
        String stmt1 = " call  CCT_FETCH_FIELDS.get_opty_id(?,?)";
        String opportunityID = null;
        try {
            cst = this.getDBTransaction().createCallableStatement(stmt1, 0);
            cst.setString(1, opportunityNumber);
            cst.registerOutParameter(2, Types.VARCHAR);
            cst.executeUpdate();
            opportunityID = cst.getString(2);
        } catch (SQLException e) {
            throw new JboException(e.getMessage());
        } finally {
            if (cst != null) {
                try {
                    cst.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return opportunityID;
    }

    public void createSnapshot(String opportunityNumber, String snapshotType) {
        CallableStatement cst = null;


        String stmt = " call CCT_APPROVAL_PROCESS.create_snapshot (?,?)";
     


        try {
            cst = this.getDBTransaction().createCallableStatement(stmt, 0);
            cst.setString(1, opportunityNumber);
            cst.setString(2, snapshotType);
            cst.executeUpdate();
        } catch (SQLException e) {
            throw new JboException(e.getMessage());
        } finally {
            if (cst != null) {
                try {
                    cst.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void fetchCostComparisonDetails(String opportunityNumber) {
        CallableStatement cst = null;
        String opptyID = getOpportunityID(opportunityNumber);
        String stmt = " call CCT_COST_COMPARISON.insert_cost_header (?)";

        try {
            cst = this.getDBTransaction().createCallableStatement(stmt, 0);
            cst.setString(1, opportunityNumber);
            cst.executeUpdate();
            ViewObjectImpl CctCostCompareHeaderVO = getCostCompareHeaderVO1();

            CctCostCompareHeaderVO.setNamedWhereClauseParam("pOppID", opptyID);

            CctCostCompareHeaderVO.executeQuery();
            String changeType = "Additional Change";
            ViewObjectImpl cctCostCompareLinesVO = getCostCompareLinesVO1();
            ViewCriteria vc = cctCostCompareLinesVO.getViewCriteria("AdditionalChangeNotEqualToVOCriteria");
            cctCostCompareLinesVO.setNamedWhereClauseParam("pOppID", opptyID);
            cctCostCompareLinesVO.setNamedWhereClauseParam("type", changeType);
            cctCostCompareLinesVO.applyViewCriteria(vc);
            cctCostCompareLinesVO.executeQuery();


        } catch (SQLException e) {
            throw new JboException(e.getMessage());
        } finally {
            if (cst != null) {
                try {
                    cst.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public BigDecimal getAdditionalChangesValue(String opportunityID) {
        BigDecimal deltaAddChangeVal = null;

        BigDecimal opptyId = new BigDecimal(opportunityID);
        int dataType = 3;

        String s =
            "select ADDITIONAL_CHANGES from CCT_COST_COMPARE_HEADER  where OPPORTUNITY_ID = " + opptyId +
            " and TYPE_ID =" + dataType;

        PreparedStatement stmt = getDBTransaction().createPreparedStatement(s, 0);

        ResultSet resultset = null;

        try {

            resultset = stmt.executeQuery();

            while (resultset.next()) {
                deltaAddChangeVal = resultset.getBigDecimal(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                resultset.close();
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
       
        return deltaAddChangeVal;
    }

    public HashMap generateCostComparisonDownloadTemplate(String pMappingKey) {
        HashMap tempHashMap = new HashMap();
        getCostComparisonLinesVOAttr();
        ViewObjectImpl cctMappingsROVO = getCctMappingsROVO();
        cctMappingsROVO.setNamedWhereClauseParam("pMappingKey", pMappingKey);
        cctMappingsROVO.executeQuery();
        Row[] rows = cctMappingsROVO.getAllRowsInRange();
      
        RowSetIterator createRowSetIterator = cctMappingsROVO.createRowSetIterator(null);
        String[] arrStr = null;
        while (createRowSetIterator.hasNext()) {
            Row row = createRowSetIterator.next();
            arrStr = new String[2];
            arrStr[0] = (String) row.getAttribute("DestinationField");
            arrStr[1] = (String) row.getAttribute("DataFormat");
            if (arrStr[1] == null) {
                arrStr[1] = "";
            }
            downloadCostComparisonMapping.put((String) row.getAttribute("SourceField"), arrStr);
            logger.log(Level.INFO, getClass(), "generateCostComparisonDownloadTemplate",
                       "Source Field :-- " + row.getAttribute("SourceField"));
        }
        createRowSetIterator.closeRowSetIterator();
        tempHashMap.put("downloadCostComparisonMapping", downloadCostComparisonMapping);
        tempHashMap.put("costComparisonLinesAttrMapping", costComparisonLinesAttrMapping);

        return tempHashMap;
    }


    /* Container's getter for CostCompareLinesQuotePartsVO1.
 * @return CostCompareLinesQuotePartsVO1
     */
    public CostCompareLinesQuotePartsVOImpl getCostCompareLinesQuotePartsVO1() {
        return (CostCompareLinesQuotePartsVOImpl) findViewObject("CostCompareLinesQuotePartsVO1");
    }

    /**
     * Container's getter for SysdateVO1.
     * @return SysdateVO1
     */
    public SysdateVOImpl getSysdateVO1() {
        return (SysdateVOImpl) findViewObject("SysdateVO1");
    }

    /**
     * Container's getter for CostCompareHeaderVO1.
     * @return CostCompareHeaderVO1
     */

    public CostCompareHeaderVOImpl getCostCompareHeaderVO1() {
        return (CostCompareHeaderVOImpl) findViewObject("CostCompareHeaderVO1");
    }

    public String getHashMapString(HashMap hashMap) {
        StringBuilder stb = new StringBuilder();
        int index = 0;
        stb.append("------Passed hashMap Values---");
        if (null != hashMap) {
            Iterator iter = hashMap.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry pair = (Map.Entry) iter.next();
                index = index + 1;
                stb.append("\n" + index + ")" + pair.getKey() + "=" + pair.getValue());
            }

        }
        return stb.toString();
    }

    /**
     * Container's getter for CostCompareLinesVO1.
     * @return CostCompareLinesVO1
     */
    public CostCompareLinesVOImpl getCostCompareLinesVO1() {
        return (CostCompareLinesVOImpl) findViewObject("CostCompareLinesVO1");
    }

    /**
     * Container's getter for CctProgramCostCatSubtypesEO1.
     * @return CctProgramCostCatSubtypesEO1
     */
    public ViewObjectImpl getCctProgramCostCatSubtypesEOVO1() {
        return (ViewObjectImpl) findViewObject("CctProgramCostCatSubtypesEOVO1");
    }

    /**
     * Container's getter for QuotePartsVO1.
     * @return QuotePartsVO1
     */
    public QuotePartsVOImpl getQuotePartsVO1() {
        return (QuotePartsVOImpl) findViewObject("QuotePartsVO1");
    }

    public void getOppDetailsApprovalTab(String opportunityNumber) {
        logger.log(Level.INFO, getClass(), "Entering getOppDetailsApprovalTab",
                   "Parameters are opportunityNumber=--" + opportunityNumber + "--");
        ViewObject opportunityForApproval = getOpportunityForApproval();
        opportunityForApproval.setNamedWhereClauseParam("bind_oppNo", opportunityNumber);
        opportunityForApproval.executeQuery();
        logger.log(Level.INFO, getClass(), "Count for ",
                   "opportunityForApproval :--" +
                   (null != opportunityForApproval ? opportunityForApproval.getEstimatedRowCount() :
                    opportunityForApproval));
        logger.log(Level.INFO, getClass(), "Exiting getOppDetailsApprovalTab", "");

    }

    /**
     * Container's getter for Opportunity1.
     * @return Opportunity1
     */
    public ViewObjectImpl getOpportunityForApproval() {
        return (ViewObjectImpl) findViewObject("OpportunityForApproval");
    }


    /**
     * Container's getter for OppROVO1.
     * @return OppROVO1
     */
    public ViewObjectImpl getOppROVO() {
        return (ViewObjectImpl) findViewObject("OppROVO");
    }

    /**
     * Container's getter for CreateTaskRolesVO1.
     * @return CreateTaskRolesVO1
     */
    public CreateTaskRolesVOImpl getCreateTaskRolesVO1() {
        return (CreateTaskRolesVOImpl) findViewObject("CreateTaskRolesVO1");
    }


    /**
     * Container's getter for CctQuotePartsFinalVO3.
     * @return CctQuotePartsFinalVO3
     */
    public CctQuotePartsFinalVOImpl getCctQuotePartsFinalVO1() {
        return (CctQuotePartsFinalVOImpl) findViewObject("CctQuotePartsFinalVO1");
    }


    /**
     * Container's getter for CctQuotePartsFinalEOVO1.
     * @return CctQuotePartsFinalEOVO1
     */
    public ViewObjectImpl getCctQuotePartsFinalEOVO1() {
        return (ViewObjectImpl) findViewObject("CctQuotePartsFinalEOVO1");
    }

    /**
     * Container's getter for QuoteClassFinalVVO1.
     * @return QuoteClassFinalVVO1
     */
    public QuoteClassFinalVVOImpl getQuoteClassFinalVVO1() {
        return (QuoteClassFinalVVOImpl) findViewObject("QuoteClassFinalVVO1");
    }

    /**
     * Container's getter for QuotePricingFinalVVO1.
     * @return QuotePricingFinalVVO1
     */
    public QuotePricingFinalVVOImpl getQuotePricingFinalVVO1() {
        return (QuotePricingFinalVVOImpl) findViewObject("QuotePricingFinalVVO1");
    }

    /**
     * Container's getter for AllQuotePartsSnapshotFinalVO1.
     * @return AllQuotePartsSnapshotFinalVO1
     */
    public AllQuotePartsSnapshotFinalVOImpl getAllQuotePartsSnapshotFinalVO1() {
        return (AllQuotePartsSnapshotFinalVOImpl) findViewObject("AllQuotePartsSnapshotFinalVO1");
    }

    /**
     * Container's getter for CctApprovalCompareHeaderVO1.
     * @return CctApprovalCompareHeaderVO1
     */
    public CctApprovalCompareHeaderVOImpl getCctApprovalCompareHeaderVO1() {
        return (CctApprovalCompareHeaderVOImpl) findViewObject("CctApprovalCompareHeaderVO1");
    }

    /**
     * Container's getter for CctApprovedComparedHeaderVO1.
     * @return CctApprovedComparedHeaderVO1
     */
    public CctApprovedComparedHeaderVOImpl getCctApprovedComparedHeaderVO1() {
        return (CctApprovedComparedHeaderVOImpl) findViewObject("CctApprovedComparedHeaderVO1");
    }

    public void fetchFinalAreaComparisonDetails(String opportunityNumber) {
        CallableStatement cst = null;
        String opptyID = getOpportunityID(opportunityNumber);
        String stmt = " call CCT_COST_COMPARISON.insert_approvals_header (?)";

        try {
            cst = this.getDBTransaction().createCallableStatement(stmt, 0);
            cst.setString(1, opportunityNumber);
            cst.executeUpdate();
            ViewObjectImpl CctApprovalCompareHeaderVO = getCctApprovalCompareHeaderVO1();
            CctApprovalCompareHeaderVO.setNamedWhereClauseParam("pOppID", opptyID);
            CctApprovalCompareHeaderVO.executeQuery();
            ViewObjectImpl cctApprovedCompareHeaderVO = getCctApprovedComparedHeaderVO1();
            cctApprovedCompareHeaderVO.setNamedWhereClauseParam("pOppID", opptyID);
            cctApprovedCompareHeaderVO.executeQuery();


        } catch (SQLException e) {
            throw new JboException(e.getMessage());
        } finally {
            if (cst != null) {
                try {
                    cst.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * Container's getter for AwardsDownloadVO1.
     * @return AwardsDownloadVO1
     */
    public ViewObjectImpl getAwardsDownloadVO1() {
        return (ViewObjectImpl) findViewObject("AwardsDownloadVO1");
    }

    /**
     * Container's getter for AllQuotePartsEOView1.
     * @return AllQuotePartsEOView1
     */
    public ViewObjectImpl getAllQuotePartsEOView1() {
        return (ViewObjectImpl) findViewObject("AllQuotePartsEOView1");
    }

    /**
     * Container's getter for QuotePartsFinalDownloadVO1.
     * @return QuotePartsFinalDownloadVO1
     */
    public QuotePartsFinalDownloadVOImpl getQuotePartsFinalDownloadVO1() {
        return (QuotePartsFinalDownloadVOImpl) findViewObject("QuotePartsFinalDownloadVO1");
    }

    /**
     * Container's getter for AllPartsDownloadVO1.
     * @return AllPartsDownloadVO1
     */
    public ViewObjectImpl getAllPartsDownloadVO1() {
        return (ViewObjectImpl) findViewObject("AllPartsDownloadVO1");
    }

    public BigDecimal getAwardHeaderIdByOpptyId(BigDecimal opptyId) {
        ViewObjectImpl AwardHeaderVO = getCctAwardHeaderVO1();
        ViewCriteria criteria = AwardHeaderVO.getViewCriteria("CctAwardHeaderVOCriteria");
        AwardHeaderVO.setNamedWhereClauseParam("b_opptyId", opptyId);
        AwardHeaderVO.applyViewCriteria(criteria);
        AwardHeaderVO.executeQuery();
        BigDecimal awdHeaderId = null;
        RowSetIterator awdHeaderIterator = AwardHeaderVO.createRowSetIterator(null);
        while (awdHeaderIterator.hasNext()) {
            Row awardHeaderRow = awdHeaderIterator.next();
            awdHeaderId = new BigDecimal(awardHeaderRow.getAttribute("AwardHeaderId").toString());
        }
        return awdHeaderId;
    }

    /**
     * Container's getter for ApprovalStatusROVO1.
     * @return ApprovalStatusROVO1
     */
    public ApprovalStatusROVOImpl getApprovalStatusROVO() {
        return (ApprovalStatusROVOImpl) findViewObject("ApprovalStatusROVO");
    }

    public String getLatestApprovalStatus(String oppNumber, String snapVer) {
        String approval = "NO DATA";
        try {
            ApprovalStatusROVOImpl approvalStatusROVO = getApprovalStatusROVO();
            approvalStatusROVO.setNamedWhereClauseParam("pOppNum", oppNumber);
            approvalStatusROVO.setNamedWhereClauseParam("pSnapVer", snapVer);
            approvalStatusROVO.executeQuery();
            RowSetIterator createRowSetIterator = approvalStatusROVO.createRowSetIterator(null);
            while (createRowSetIterator.hasNext()) {
                Row next = createRowSetIterator.next();
                approval = (String) next.getAttribute("ApprovalStatus");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return approval;
    }

    /**
     * Container's getter for ValidateSnapshotRVO1.
     * @return ValidateSnapshotRVO1
     */
    public ValidateSnapshotRVOImpl getValidateSnapshotRVO1() {
        return (ValidateSnapshotRVOImpl) findViewObject("ValidateSnapshotRVO1");
    }

    /**
     * Container's getter for ProgramCostEOVO1.
     * @return ProgramCostEOVO1
     */
    public ProgramCostEOVOImpl getProgramCostEOVO1() {
        return (ProgramCostEOVOImpl) findViewObject("ProgramCostEOVO1");
    }

    /**
     * Container's getter for CctProgramCostSummNotesVO1.
     * @return CctProgramCostSummNotesVO1
     */
    public CctProgramCostSummNotesVOImpl getCctProgramCostSummNotesEHSVO() {
        return (CctProgramCostSummNotesVOImpl) findViewObject("CctProgramCostSummNotesEHSVO");
    }

    /**
     * Container's getter for CctProgramCostSummNotesVO2.
     * @return CctProgramCostSummNotesVO2
     */
    public CctProgramCostSummNotesVOImpl getCctProgramCostSummNotesImpVO() {
        return (CctProgramCostSummNotesVOImpl) findViewObject("CctProgramCostSummNotesImpVO");
    }

    /**
     * Container's getter for CctProgramCostSummNotesVO1.
     * @return CctProgramCostSummNotesVO1
     */
    public CctProgramCostSummNotesVOImpl getCctProgramCostSummNotesOthVO() {
        return (CctProgramCostSummNotesVOImpl) findViewObject("CctProgramCostSummNotesOthVO");
    }

    /**
     * Container's getter for CctProgramCostSummNotesVO2.
     * @return CctProgramCostSummNotesVO2
     */
    public CctProgramCostSummNotesVOImpl getCctProgramCostSummNotesFacVO() {
        return (CctProgramCostSummNotesVOImpl) findViewObject("CctProgramCostSummNotesFacVO");
    }

    /**
     * Container's getter for CctProgramCostSummNotesVO1.
     * @return CctProgramCostSummNotesVO1
     */
    public CctProgramCostSummNotesVOImpl getCctProgramCostSummNotesPerVO() {
        return (CctProgramCostSummNotesVOImpl) findViewObject("CctProgramCostSummNotesPerVO");
    }
    public void initializeCctCrfRevisionData(String crfNo){
        BigDecimal crfID = null;
        if (null != crfNo) {
            CctCrfEoVOImpl cctCrfEOVO = getUpdateCcctCrfEOVO();
            cctCrfEOVO.setpCrfNo(crfNo);
            cctCrfEOVO.executeQuery();
            RowSetIterator cctCrfRowSetIterator = cctCrfEOVO.createRowSetIterator(null);
            while (cctCrfRowSetIterator.hasNext()) {
                CctCrfEoVORowImpl currentRow = (CctCrfEoVORowImpl) cctCrfRowSetIterator.next();
                crfID = currentRow.getCrfId();
            }
        }
        //CCT_CRF_REVISION
        CctCrfRevisionVOImpl cctCrfRevisionVO = getCctCrfRevisionVO();
        cctCrfRevisionVO.reset();
        cctCrfRevisionVO.clearCache();
        CctCrfRevisionVORowImpl cctCrfRevisionVORow = (CctCrfRevisionVORowImpl)cctCrfRevisionVO.createRow();
        //cctCrfRevisionVORow.refresh(Row.REFRESH_UNDO_CHANGES | Row.REFRESH_WITH_DB_FORGET_CHANGES);
        cctCrfRevisionVORow.setCrfId(crfID);
        cctCrfRevisionVORow.setNotifyLegal("N");
        cctCrfRevisionVORow.setNotifyParts("N");
        cctCrfRevisionVORow.setNotifyProgram("N");
        cctCrfRevisionVO.insertRow(cctCrfRevisionVORow);
       
    }
    
    public void rollbackCurrentRevisionRow(){
        CctCrfRevisionVOImpl cctCrfRevisionVO = getCctCrfRevisionVO();
        RowSetIterator cctCrfRevisionVOIterator =cctCrfRevisionVO.createRowSetIterator(null);
        try {
            while ( cctCrfRevisionVOIterator.hasNext()) { 
            CctCrfRevisionVORowImpl nextRow = (CctCrfRevisionVORowImpl)cctCrfRevisionVOIterator.next();
            Integer rowStatus = (Integer) nextRow.getRowStatusTrans();
                if(rowStatus == 0){
                    nextRow.remove();
                }
                else if(rowStatus == 2){
                    nextRow.refresh(Row.REFRESH_UNDO_CHANGES | Row.REFRESH_WITH_DB_FORGET_CHANGES);
                }
            }
//            if (null != cctCrfRevisionVORow) {
//                System.out.println("Current CrfId-----"+cctCrfRevisionVORow.getAttribute("CrfId"));
//                System.out.println("Current Parts val-----"+cctCrfRevisionVORow.getAttribute("NotifyParts"));
//                cctCrfRevisionVORow.refresh(Row.REFRESH_UNDO_CHANGES | Row.REFRESH_WITH_DB_FORGET_CHANGES);
//                System.out.println("After Refresh CrfId-----"+cctCrfRevisionVORow.getAttribute("CrfId"));
//                System.out.println("After Refresh Parts val-----"+cctCrfRevisionVORow.getAttribute("NotifyParts"));
//                //cctCrfRevisionVORow.remove();
//                cctCrfRevisionVO.removeCurrentRow();
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
       
    }

    public void initializeCreateOpp() {
        logger.log(Level.INFO, getClass(), "initializeCreateOpp", "Entering initializeCreateOpp method");
        try {
            getCctSalesHierarchyFilter().executeEmptyRowSet();
            
            //CCT_CRF
            CctCrfEoVOImpl cctCrfEoVO = getCctCrfEoVO();
            removeUnUsedRows(cctCrfEoVO);
            CctCrfEoVORowImpl cctCrfEoVORow = (CctCrfEoVORowImpl) cctCrfEoVO.createRow();
            BigDecimal crfId = cctCrfEoVORow.getCrfId();
            cctCrfEoVO.insertRow(cctCrfEoVORow);
            //CCT_CRF_CUSTOMER_INFO
            CctCrfCustomerInfoEOVOImpl cctCrfCustomerInfoEOVO = getCctCrfCustomerInfoEOVO();
            removeUnUsedRows(cctCrfCustomerInfoEOVO);
            CctCrfCustomerInfoEOVORowImpl customerInfoRow =
                (CctCrfCustomerInfoEOVORowImpl) cctCrfCustomerInfoEOVO.createRow();
            customerInfoRow.setCrfId(crfId);
            cctCrfCustomerInfoEOVO.insertRow(customerInfoRow);
            //CCT_CRF_CUSTOMER_REQUEST
            CctCrfCustomerRequestEOVOImpl cctCrfCustomerRequestEOVO = getCctCrfCustomerRequestEOVO();
            removeUnUsedRows(cctCrfCustomerRequestEOVO);
            CctCrfCustomerRequestEOVORowImpl CctCrfCustomerRequestEOVORow =
                (CctCrfCustomerRequestEOVORowImpl) cctCrfCustomerRequestEOVO.createRow();
            CctCrfCustomerRequestEOVORow.setCrfId(crfId);
            CctCrfCustomerRequestEOVORow.setCustReqRevisionNo(BigDecimal.ZERO);
            cctCrfCustomerRequestEOVO.insertRow(CctCrfCustomerRequestEOVORow);
            //CCT_CRF_KLX_OFFER
            CctCrfKlxOfferEOVOImpl cctCrfKlxOfferEOVO = getCctCrfKlxOfferEOVO();
            removeUnUsedRows(cctCrfKlxOfferEOVO);
            CctCrfKlxOfferEOVORowImpl cctCrfKlxOfferEOVORow =
                (CctCrfKlxOfferEOVORowImpl) cctCrfKlxOfferEOVO.createRow();
            cctCrfKlxOfferEOVORow.setCrfId(crfId);
            cctCrfKlxOfferEOVORow.setRevision(BigDecimal.ZERO);
            cctCrfKlxOfferEOVO.insertRow(cctCrfKlxOfferEOVORow);
            //CCT_KLX_AWARDS
            CctCrfKlxAwardVOImpl cctCrfKlxAwardVO = getCctCrfKlxAwardVO1();
            removeUnUsedRows(cctCrfKlxAwardVO);
            CctCrfKlxAwardVORowImpl cctCrfKlxAwardVORow = (CctCrfKlxAwardVORowImpl) cctCrfKlxAwardVO.createRow();
            cctCrfKlxAwardVORow.setCrfId(crfId);
            cctCrfKlxAwardVORow.setRevision(BigDecimal.ZERO);
            cctCrfKlxAwardVO.insertRow(cctCrfKlxAwardVORow);

            //            CctCrfDateMgmtVOImpl cctCrfDateMgmtVO = getCctCrfDateMgmtVO();
            //            CctCrfDateMgmtVORowImpl cctCrfDateMgmtVORow = (CctCrfDateMgmtVORowImpl) cctCrfDateMgmtVO.createRow();
            //            cctCrfDateMgmtVORow.setCrfId(crfId);
            //            cctCrfDateMgmtVO.insertRow(cctCrfDateMgmtVORow);
            //
            //            CctCrfAwardDatesVOImpl cctCrfAwardDatesVO = getCctCrfAwardDatesVO();
            //            CctCrfAwardDatesVORowImpl cctCrfAwardDatesVORow =
            //                (CctCrfAwardDatesVORowImpl) cctCrfAwardDatesVO.createRow();
            //            cctCrfAwardDatesVORow.setCrfId(crfId);
            //            cctCrfAwardDatesVO.insertRow(cctCrfAwardDatesVORow);
//            CctSalesHierarchyNewVOImpl cctSalesHierarchyVO = getCctSalesHierarchyFilter();
//            CctSalesHierarchyNewVORowImpl createRow = (CctSalesHierarchyNewVORowImpl) cctSalesHierarchyVO.createRow();
//            cctSalesHierarchyVO.insertRow(createRow);
            

        } catch (Exception e) {
            e.printStackTrace();
            throw new SystemException("CCT", "initializeCreateOpp", "CCT_SYS_EX01", e); 
        }
        logger.log(Level.INFO, getClass(), "initializeCreateOpp", "Exiting initializeCreateOpp method");

    }

    /**
     * Container's getter for CctCrfEoVO1.
     * @return CctCrfEoVO1
     */
    public CctCrfEoVOImpl getCctCrfEoVO() {
        return (CctCrfEoVOImpl) findViewObject("CctCrfEoVO");
    }

    public String executeCctCrfVO(String customerNumber, String executeFlag) {
        String result = null;
        Boolean containsMoreThanOneRow = false;
        customerNumber = null != customerNumber ? customerNumber.trim() : null;
        ViewObjectImpl crfCustomerMasterEOVO = getCrfCustomerMasterEOVO();
        ViewObjectImpl cctCrfKlxMasterContractVO = getCctCrfKlxMasterContractVO1();
        CrfCustomerMasterEOVORowImpl masterRow = null;
        CctCrfKlxMasterContractVORowImpl contractMasterRow = null;
        if (null != customerNumber && !"".equalsIgnoreCase(customerNumber)) {
            crfCustomerMasterEOVO.setNamedWhereClauseParam("pCustomerNo", customerNumber);
            crfCustomerMasterEOVO.executeQuery();
            RowSetIterator createRowSetIterator = crfCustomerMasterEOVO.createRowSetIterator(null);
            while (createRowSetIterator.hasNext()) {
                masterRow = (CrfCustomerMasterEOVORowImpl) createRowSetIterator.next();
            }
            createRowSetIterator.closeRowSetIterator();

            cctCrfKlxMasterContractVO.setNamedWhereClauseParam("pCustomerNo", customerNumber);
            //System.out.println("cctCrfKlxMasterContractVO Query: " + cctCrfKlxMasterContractVO.getQuery());
            cctCrfKlxMasterContractVO.executeQuery();
            RowSetIterator createContractRowSetIterator = cctCrfKlxMasterContractVO.createRowSetIterator(null);
            while (createContractRowSetIterator.hasNext()) {
                contractMasterRow = (CctCrfKlxMasterContractVORowImpl) createContractRowSetIterator.next();
                if (createContractRowSetIterator.hasNext())
                    containsMoreThanOneRow = true;
                /* if(null != contractMasterRow.getExistingContract())
                    existingContract.add(contractMasterRow.getExistingContract());
                if(null != contractMasterRow.getExistingContractType())
                    existingContractType.add(contractMasterRow.getExistingContractType());  */
            }
            createContractRowSetIterator.closeRowSetIterator();
        }
        ViewObjectImpl cctSalesHierarchyFilter = getCctSalesHierarchyFilter();
        CctCrfCustomerInfoEOVOImpl cctCrfCustomerInfoEOVO = null;
        if("updateScreen".equalsIgnoreCase(executeFlag)){
            cctCrfCustomerInfoEOVO = getUpdateCustomerInfoEOVO();   
        }
        else{
            cctCrfCustomerInfoEOVO = getCctCrfCustomerInfoEOVO();
        }
         
        CctCrfCustomerInfoEOVORowImpl infoRow = (CctCrfCustomerInfoEOVORowImpl) cctCrfCustomerInfoEOVO.getCurrentRow();
        if (null != masterRow) {
            infoRow.setCustomerNo(masterRow.getCustomerNo());
            infoRow.setCustomerName(masterRow.getCustomerName());
            infoRow.setAccountType(masterRow.getAccountType());
            infoRow.setIsGt25(masterRow.getIsGt25());
            infoRow.setIsEcomm(masterRow.getIsEcomm());
            infoRow.setCurrencyCode(masterRow.getCurrencyCode());
            infoRow.setPaymentTerms(masterRow.getPaymentTerms());
            infoRow.setIncoTerms(masterRow.getIncoTerms());
            infoRow.setUltimtateDestination(masterRow.getUltimtateDestination());
            infoRow.setIsDfar(masterRow.getIsDfar());
            infoRow.setIsAslApplies(masterRow.getIsAslApplicable());
            infoRow.setWarehouseInfo(masterRow.getWarehouseInfo());
            infoRow.setLineMin(masterRow.getLineMin());
            infoRow.setOrderMin(masterRow.getOrderMin());
            infoRow.setSalesOwner(masterRow.getSalesOwner());
            infoRow.setSalesTeam(masterRow.getSalesTeamNumber() + " - " + masterRow.getSalesTeam());
            infoRow.setSalesOutside(masterRow.getSalesOutside());
            if (containsMoreThanOneRow) {
                //infoRow.setExistingContractNumber(new BigDecimal(9999));
                infoRow.setExistingContract("Mutiple");
                infoRow.setExistingContractType("Multiple");
            } else {
                if (null != contractMasterRow.getExistingContract()) {
                    //infoRow.setExistingContractNumber(new BigDecimal(Integer.parseInt(contractMasterRow.getExistingContract())));
                    infoRow.setExistingContract(contractMasterRow.getExistingContract());
                } else {
                    //infoRow.setExistingContractNumber(null);
                    infoRow.setExistingContract(null);
                }
                if (null != contractMasterRow.getExistingContractType())
                    infoRow.setExistingContractType(contractMasterRow.getExistingContractType());
                else
                    infoRow.setExistingContractType(null);
            }
            /* if (null != contractMasterRow.getExistingContract())
                infoRow.setExistingContractNumber(new BigDecimal(Integer.parseInt(contractMasterRow.getExistingContract())));
            if (null != contractMasterRow.getExistingContractType())
                infoRow.setExistingContractType(contractMasterRow.getExistingContractType());*/
            try {
                cctSalesHierarchyFilter.setNamedWhereClauseParam("pTeamNo", masterRow.getSalesTeamNumber());
            } catch (Exception e) {
                e.printStackTrace();
            }
            cctSalesHierarchyFilter.executeQuery();
            RowSetIterator salesRowSetIter = cctSalesHierarchyFilter.createRowSetIterator(null);
            while (salesRowSetIter.hasNext()) {
                CctSalesHierarchyNewVORowImpl salesRow = (CctSalesHierarchyNewVORowImpl) salesRowSetIter.next();
                try {
                    infoRow.setSalesTeamNumber(new BigDecimal((salesRow.getTeamNo())));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            salesRowSetIter.closeRowSetIterator();

        } else {
            cctSalesHierarchyFilter.executeEmptyRowSet();
            infoRow.setCustomerNo(null);
            infoRow.setCustomerName(null);
            infoRow.setAccountType(null);
            infoRow.setIsGt25(null);
            infoRow.setIsEcomm(null);
            infoRow.setCurrencyCode(null);
            infoRow.setPaymentTerms(null);
            infoRow.setIncoTerms(null);
            infoRow.setUltimtateDestination(null);
            //            infoRow.setExistingContractNumber(null);
            //            infoRow.setExistingContractType(null);
            infoRow.setIsDfar(null);
            infoRow.setIsAslApplies(null);
            //      infoRow.setAslDetails(null);
            infoRow.setWarehouseInfo(null);
            infoRow.setLineMin(null);
            infoRow.setOrderMin(null);
            infoRow.setSalesOwner(null);
            infoRow.setSalesTeam(null);
            infoRow.setSalesOutside(null);
        }

        return result;
    }

    /**
     * Container's getter for CctSalesHierarchyVO1.
     * @return CctSalesHierarchyVO1
     */
    public CctSalesHierarchyVOImpl getCctSalesHierarchyVO() {
        return (CctSalesHierarchyVOImpl) findViewObject("CctSalesHierarchyVO");
    }


    /**
     * Container's getter for CctCrfCustomerInfoEOVO1.
     * @return CctCrfCustomerInfoEOVO1
     */
    public CctCrfCustomerInfoEOVOImpl getCctCrfCustomerInfoEOVO() {
        return (CctCrfCustomerInfoEOVOImpl) findViewObject("CctCrfCustomerInfoEOVO");
    }

    public String saveCreatedOpportunity() {
        logger.log(Level.INFO, getClass(), "saveCreatedOpportunity", "Entering saveCreatedOpportunity method");
        String status = ModelConstants.STATUS_FAILURE;
        String qualificationStage="No";
        try {
            BigDecimal crfId = getDBSeq(ModelConstants.CCT_CRF_SEQ);
            if (BigDecimal.ZERO == crfId)
                return status;
            CctCrfEoVOImpl cctCrfEoVO = getCctCrfEoVO();
            CctCrfEoVORowImpl cctCRFcurrentRow = (CctCrfEoVORowImpl) cctCrfEoVO.getCurrentRow();
            if(null==cctCRFcurrentRow.getCrfId()){
            qualificationStage=cctCRFcurrentRow.getQualificationStage();
            if("Yes".equalsIgnoreCase(qualificationStage)){
                cctCRFcurrentRow.setCrfStatus("Qualification");
                Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
                cctCRFcurrentRow.setQualificationCreationDate(currentTimestamp);
            }
            cctCRFcurrentRow.setCrfId(crfId);
            cctCRFcurrentRow.setCrfNo("CRF" + crfId);
            logger.log(Level.INFO, getClass(), "saveCreatedOpportunity", "CCT_CRF sequence :--"+crfId+"--");
            
            BigDecimal cctCrfCustomerRequestId = getDBSeq(ModelConstants.CCT_CRF_CUSTOMER_REQUEST_SEQ);
            CctCrfCustomerRequestEOVOImpl cctCrfCustomerRequestEOVO = getCctCrfCustomerRequestEOVO();
            CctCrfCustomerRequestEOVORowImpl cctCrfCustomerRequestEOVORow =
                (CctCrfCustomerRequestEOVORowImpl) cctCrfCustomerRequestEOVO.getCurrentRow();
            cctCrfCustomerRequestEOVORow.setCctCrfCustomerRequestId(cctCrfCustomerRequestId);
            cctCrfCustomerRequestEOVORow.setCrfId(crfId);
            logger.log(Level.INFO, getClass(), "saveCreatedOpportunity", "CCT_CRF_CUSTOMER_REQUEST sequence :--"+cctCrfCustomerRequestId+"--");

            BigDecimal cctCrfCustomerInfoId = getDBSeq(ModelConstants.CCT_CRF_CUSTOMER_INFO_SEQ);
            CctCrfCustomerInfoEOVOImpl cctCrfCustomerInfoEOVO = getCctCrfCustomerInfoEOVO();
            CctCrfCustomerInfoEOVORowImpl customerInfoRow =
                (CctCrfCustomerInfoEOVORowImpl) cctCrfCustomerInfoEOVO.getCurrentRow();
            customerInfoRow.setCctCrfCustomerInfoId(cctCrfCustomerInfoId);
            customerInfoRow.setCrfId(crfId);
            
            logger.log(Level.INFO, getClass(), "saveCreatedOpportunity", "CCT_CRF_CUSTOMER_INFO sequence :--"+cctCrfCustomerInfoId+"--");

            BigDecimal klxOfferId = getDBSeq(ModelConstants.CCT_CRF_KLX_OFFER_SEQ);
            CctCrfKlxOfferEOVOImpl cctCrfKlxOfferEOVO = getCctCrfKlxOfferEOVO();
            CctCrfKlxOfferEOVORowImpl cctCrfKlxOfferEOVORow =
                (CctCrfKlxOfferEOVORowImpl) cctCrfKlxOfferEOVO.getCurrentRow();
            cctCrfKlxOfferEOVORow.setKlxOfferId(klxOfferId);
            cctCrfKlxOfferEOVORow.setCrfId(crfId);
            logger.log(Level.INFO, getClass(), "saveCreatedOpportunity", "CCT_CRF_KLX_OFFER sequence :--"+klxOfferId+"--");

            BigDecimal klxAwardId = getDBSeq(ModelConstants.CCT_CRF_KLX_AWARD_SEQ);
            CctCrfKlxAwardVOImpl cctCrfKlxAwardVO = getCctCrfKlxAwardVO1();
            CctCrfKlxAwardVORowImpl cctCrfKlxAwardVORow = (CctCrfKlxAwardVORowImpl) cctCrfKlxAwardVO.getCurrentRow();
            cctCrfKlxAwardVORow.setKlxAwardId(klxAwardId);
            cctCrfKlxAwardVORow.setCrfId(crfId);
            logger.log(Level.INFO, getClass(), "saveCreatedOpportunity", "CCT_CRF_KLX_AWARD sequence :--"+klxAwardId+"--");
            
            }
            logger.log(Level.INFO, getClass(), "saveCreatedOpportunity", "Before opportunity commit to DB");
            getDBTransaction().commit();
            logger.log(Level.INFO, getClass(), "saveCreatedOpportunity", "Afer opportunity commit to DB");
            
            
            String crfNo = cctCRFcurrentRow.getCrfNo();
            status = copyToOpportunity(crfNo);
            
            if (ModelConstants.STATUS_SUCCESS.equalsIgnoreCase(status)&& "No".equalsIgnoreCase(qualificationStage))
                status = createOpportunity(crfNo);
            if (ModelConstants.STATUS_SUCCESS.equalsIgnoreCase(status)&& "Yes".equalsIgnoreCase(qualificationStage)){
                BigDecimal oppID = null;
                ReadOnlyOpportunityRVOImpl createVO = getFilterOppOnCreateVO();
                    createVO.setpOppNum(crfNo);
                    createVO.executeQuery();
                    RowSetIterator createRowSetIterator = createVO.createRowSetIterator(null);
                    while (createRowSetIterator.hasNext()) {
                        ReadOnlyOpportunityRVORowImpl currentRow = (ReadOnlyOpportunityRVORowImpl) createRowSetIterator.next();
                        oppID = currentRow.getOpportunityId();
                    }
                    createRowSetIterator.closeRowSetIterator();
                EmailUtilityClient emailUtilityClient = new EmailUtilityClient();
                String notfnStatus =  emailUtilityClient.sendEmailNotification("Qualification Email Notification", crfNo, oppID.toString(), "24");
            }
             
            logger.log(Level.INFO, getClass(), "saveCreatedOpportunity", "Initialize Date Management VO");
            
            CctCrfDateMgmtVOImpl cctCrfDateMgmtVO = getCctCrfDateMgmtVO();
            cctCrfDateMgmtVO.setNamedWhereClauseParam("b_crfId", crfId);
            cctCrfDateMgmtVO.executeQuery();
            RowSetIterator cctCrfDateMgmtIterator = cctCrfDateMgmtVO.createRowSetIterator(null);
            while(cctCrfDateMgmtIterator.hasNext()){
                CctCrfDateMgmtVORowImpl cctCrfDateMgmtRow = (CctCrfDateMgmtVORowImpl)cctCrfDateMgmtIterator.next();
                if(qualificationStage.equalsIgnoreCase("Yes")){
                    cctCrfDateMgmtRow.setOpportunityCreationDate(null);
                }
            }
            logger.log(Level.INFO, getClass(), "saveCreatedOpportunity", "Committing DateMgmt values to DB");
            getDBTransaction().commit();    
            
        } catch (Exception e) {
            status = ModelConstants.STATUS_FAILURE;
            e.printStackTrace();
//            getDBTransaction().rollback(); //CHG0042518
        }
        logger.log(Level.INFO, getClass(), "saveCreatedOpportunity", "Exiting saveCreatedOpportunity method");
        return status;
    }
        
            public BigDecimal getDBSeq(String query) {
            BigDecimal crfId = BigDecimal.ZERO;
            PreparedStatement stmt = getDBTransaction().createPreparedStatement(query, 0);
            ResultSet resultset = null;
            try {
                resultset = stmt.executeQuery();
                while (resultset.next()) {
                    crfId = resultset.getBigDecimal(1);
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    resultset.close();
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return crfId;
        }
                
        public String copyToOpportunity(String crfNumber) {
            logger.log(Level.INFO, getClass(), "copyToOpportunity", "Entering copyToOpportunity method");
            String status = ModelConstants.STATUS_FAILURE;
            CallableStatement cst = null;
            String stmt = " call  cct_create_opportunity.copy_to_opportunity (?)"; //crfNumber passed in the procedure
            try {
                cst = this.getDBTransaction().createCallableStatement(stmt, 0);
                cst.setString(1, crfNumber);
                cst.executeUpdate();
                status = ModelConstants.STATUS_SUCCESS;
            } catch (Exception ex) {
                status = ModelConstants.STATUS_FAILURE;
                ex.printStackTrace();
            } finally {
                if (cst != null) {
                    try {
                        cst.close();
                    } catch (Exception e) {
                        logger.log(Level.INFO, getClass(), "copyToOpportunity", "Failed in copying the data by db proc  --cct_create_opportunity.copy_to_opportunity (?)--");
                        status = ModelConstants.STATUS_FAILURE;
                        e.printStackTrace();
                    }
                }
            }
            logger.log(Level.INFO, getClass(), "copyToOpportunity", "Exiting copyToOpportunity method");
            return status;
        }
                
                
                
        public String createOpportunity(String oppNumber) {
            logger.log(Level.INFO, getClass(), "createOpportunity", "Entering createOpportunity method");
            String status = ModelConstants.STATUS_FAILURE;
            BigDecimal oppID = null;String contractType = null;
            ReadOnlyOpportunityRVOImpl createVO = getFilterOppOnCreateVO();
            try {
                createVO.setpOppNum(oppNumber);
                createVO.executeQuery();
                RowSetIterator createRowSetIterator = createVO.createRowSetIterator(null);
                while (createRowSetIterator.hasNext()) {
                    ReadOnlyOpportunityRVORowImpl currentRow = (ReadOnlyOpportunityRVORowImpl) createRowSetIterator.next();
                    oppID = currentRow.getOpportunityId();
                    contractType = currentRow.getContractType();
                }
                createRowSetIterator.closeRowSetIterator();
                if (null != oppID) {
                    OpportunityPortType_Service opportunityPortType_Service = new OpportunityPortType_Service();
                    OpportunityPortType opportunityPortType = opportunityPortType_Service.getOpportunityPortTypePort();
                    Opportunity opportunity = new Opportunity();
                    OpportunityHeaderInfoType headerInfo = new OpportunityHeaderInfoType();
                    headerInfo.setOpportunityId(oppID.toString());
                    headerInfo.setOpportunityNumber(oppNumber);
                    headerInfo.setSource("ADFUI");
                    headerInfo.setOpportunityType("NEWCRF");
                    headerInfo.setContractType(contractType);
                    opportunity.setOpportunityHeaderInfo(headerInfo);
                    Response createOpportunity = opportunityPortType.createOpportunity(opportunity);
                    status = createOpportunity.getStatus();
                }
                if (ModelConstants.STATUS_SUCCESS.equalsIgnoreCase(status))
                    status = ModelConstants.STATUS_SUCCESS;
                else{
                    logger.log(Level.INFO, getClass(), "createOpportunity", "Failed due to WebService call");
                    status = ModelConstants.STATUS_FAILURE;
                }
            } catch (Exception e) {
                logger.log(Level.INFO, getClass(), "createOpportunity", "Failed due to WebService call");
                status = ModelConstants.STATUS_FAILURE;
                e.printStackTrace();
            }
            logger.log(Level.INFO, getClass(), "createOpportunity", "Exiting createOpportunity method");
            return status;
        }

    public void createSalesHierarchyRow() {
        CctSalesHierarchyVOImpl cctSalesHierarchyVO = getCctSalesHierarchyVO();
        CctSalesHierarchyVORowImpl createRow = (CctSalesHierarchyVORowImpl) cctSalesHierarchyVO.createRow();
        cctSalesHierarchyVO.insertRow(createRow);
    }

    public String saveSalesHierarchy() {
        try {
            getDBTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "success";
    }


    /**
     * Container's getter for CctCrfCustomerRequestEOVO1.
     * @return CctCrfCustomerRequestEOVO1
     */
    public CctCrfCustomerRequestEOVOImpl getCctCrfCustomerRequestEOVO() {
        return (CctCrfCustomerRequestEOVOImpl) findViewObject("CctCrfCustomerRequestEOVO");
    }

    /**
     * Container's getter for CctSalesHierarchyNewVO1.
     * @return CctSalesHierarchyNewVO1
     */
    public CctSalesHierarchyNewVOImpl getCctSalesHierarchyNewVO() {
        return (CctSalesHierarchyNewVOImpl) findViewObject("CctSalesHierarchyNewVO");
    }

    /**
     * Container's getter for CrfCustomerMasterEOVO1.
     * @return CrfCustomerMasterEOVO1
     */
    public CrfCustomerMasterEOVOImpl getCrfCustomerMasterEOVO() {
        return (CrfCustomerMasterEOVOImpl) findViewObject("CrfCustomerMasterEOVO");
    }

    /**
     * Container's getter for CctSalesHierarchyNewVO1.
     * @return CctSalesHierarchyNewVO1
     */
    public CctSalesHierarchyNewVOImpl getCctSalesHierarchyFilter() {
        return (CctSalesHierarchyNewVOImpl) findViewObject("CctSalesHierarchyFilter");
    }

    

    /**
     * Container's getter for CCTOpptySearchROVO1.
     * @return CCTOpptySearchROVO1
     */
    public ViewObjectImpl getMyOpptySearchROVO() {
        return (ViewObjectImpl) findViewObject("MyOpptySearchROVO");
    }

    /**
     * Container's getter for CCTOpptySearchROVO2.
     * @return CCTOpptySearchROVO2
     */
    public ViewObjectImpl getAssignedOpptySearchROVO() {
        return (ViewObjectImpl) findViewObject("AssignedOpptySearchROVO");
    }

    public void executeMyOpptySearchROVO() {
        ViewObjectImpl MyOpptySearchROVO = getMyOpptySearchROVO();
        ViewCriteria cCTOpptySearchROVOCriteria = MyOpptySearchROVO.getViewCriteria("CCTOpptySearchROVOCriteria");

        MyOpptySearchROVO.setNamedWhereClauseParam("b_crfStatus1", "Active");
        MyOpptySearchROVO.setNamedWhereClauseParam("b_crfStatus2", "Pending");
        MyOpptySearchROVO.setNamedWhereClauseParam("b_crfStatus3", "Qualification");
        MyOpptySearchROVO.setNamedWhereClauseParam("b_assgn", ADFContext.getCurrent()
                                                                        .getSecurityContext()
                                                                        .getUserName());
        MyOpptySearchROVO.applyViewCriteria(cCTOpptySearchROVOCriteria);

        MyOpptySearchROVO.executeQuery();
       
    }

    /**
     * Container's getter for CCTOpptySearchROVO1.
     * @return CCTOpptySearchROVO1
     */
    public ViewObjectImpl getClosedOpptySearchROVO() {
        return (ViewObjectImpl) findViewObject("ClosedOpptySearchROVO");
    }


    /**
     * Container's getter for ReadOnlyOpportunityRVO1.
     * @return ReadOnlyOpportunityRVO1
     */
    public ReadOnlyOpportunityRVOImpl getFilterOppOnCreateVO() {
        return (ReadOnlyOpportunityRVOImpl) findViewObject("FilterOppOnCreateVO");
    }

    
    
    public String getUsernameByUserId(String userid){
        logger.log(Level.INFO, getClass(), "getUsernameByUserId", "Entering getUsernameByUserId method");
        String username = "";
        ActiveDirectoryManagerClient activeDirectoryManagerClient = new ActiveDirectoryManagerClient();
        User user = activeDirectoryManagerClient.fetchUserDetailsById(userid);
        username = user.getName();
        logger.log(Level.INFO, getClass(), "getUsernameByUserId", "Username in Update Opportunity:"+username);
        if(null == username || username.equalsIgnoreCase("") ){
            user = activeDirectoryManagerClient.fetchUserDetailsById(userid);
            username = user.getName();
            logger.log(Level.INFO, getClass(), "getUsernameByUserId", "Username in Update Opportunity after re-trying:"+username);
        }        
        logger.log(Level.INFO, getClass(), "getUsernameByUserId", "Exiting getUsernameByUserId method");
        return username;
    }

    public String initializeUpdateOpp(String crfNumber) {
        logger.log(Level.INFO, getClass(), "initializeUpdateOpp", "Entering initializeUpdateOpp method");
        try{
        getCctSalesHierarchyFilter().executeEmptyRowSet();
        getUpdateCcctCrfEOVO().executeEmptyRowSet();
        getUpdateCustomerInfoEOVO().executeEmptyRowSet();
        getUpdateCrfCustReqEOVO().executeEmptyRowSet();
        getCctCrfKlxOfferEOVO().executeEmptyRowSet();
        getCctCrfKlxAwardVO1().executeEmptyRowSet();
        getCctCrfDateMgmtVO().executeEmptyRowSet();
        getCctCrfAwardDatesVO().executeEmptyRowSet();
        logger.log(Level.INFO, getClass(), "initializeUpdateOpp", "Cleared all VO's sucessfully");
        BigDecimal crfID = null;
        String customerNumber = null, qualificationStage = null;
        List<String> existingContractType = new ArrayList<String>();
        List<String> existingContract = new ArrayList<String>();
        Boolean containsMoreThanOneRow = false;
      
        if (null != crfNumber) {
            CctCrfEoVOImpl ccctCrfEOVO = getUpdateCcctCrfEOVO();
            ccctCrfEOVO.setpCrfNo(crfNumber);
            ccctCrfEOVO.executeQuery();
            RowSetIterator ccctCrfRowSetIterator = ccctCrfEOVO.createRowSetIterator(null);
            while (ccctCrfRowSetIterator.hasNext()) {
                CctCrfEoVORowImpl currentRow = (CctCrfEoVORowImpl) ccctCrfRowSetIterator.next();
                crfID = currentRow.getCrfId();
            }
            ccctCrfRowSetIterator.closeRowSetIterator();
            logger.log(Level.INFO, getClass(), "initializeUpdateOpp","crfid value:--"+crfID+"---  <--this value shouldn't be null or empty");
            CctCrfCustomerInfoEOVOImpl updateCustomerInfoEOVO = getUpdateCustomerInfoEOVO();
            updateCustomerInfoEOVO.setb_crfId(crfID);
            updateCustomerInfoEOVO.executeQuery();
            CctCrfCustomerInfoEOVORowImpl infoRow = null;
            RowSetIterator updateCustomerInfoRowSetIterator = updateCustomerInfoEOVO.createRowSetIterator(null);
            String createdByUsername = "";
            while (updateCustomerInfoRowSetIterator.hasNext()) {
                infoRow = (CctCrfCustomerInfoEOVORowImpl) updateCustomerInfoRowSetIterator.next();
                customerNumber = infoRow.getCustomerNo();
                createdByUsername = getUsernameByUserId(infoRow.getCreatedBy());
                infoRow.setcreatedByUsername(createdByUsername);
            }
            updateCustomerInfoRowSetIterator.closeRowSetIterator();
            

            if (null != crfID) {
                CctCrfCustomerRequestEOVOImpl cctCrfCustomerRequestEOVOImpl = getUpdateCrfCustReqEOVO();
                cctCrfCustomerRequestEOVOImpl.setNamedWhereClauseParam("pcrfID", crfID);
                cctCrfCustomerRequestEOVOImpl.executeQuery();
                CctCrfCustomerRequestEOVORowImpl updateCustomerRequestRow = null;
                RowSetIterator updateCustomerRequestRowSetIterator =
                    cctCrfCustomerRequestEOVOImpl.createRowSetIterator(null);
                while (updateCustomerRequestRowSetIterator.hasNext()) {
                    updateCustomerRequestRow =
                        (CctCrfCustomerRequestEOVORowImpl) updateCustomerRequestRowSetIterator.next();
                }
                //System.out.println("Return part list value ==="+updateCustomerRequestRow.getReturnPartListFormat());
                //System.out.println("Legal Format value ==="+updateCustomerRequestRow.getLegalFormatType());
                if(null != updateCustomerRequestRow.getLegalFormatType()){
                    if(!updateCustomerRequestRow.getLegalFormatType().equalsIgnoreCase("Boeing Distribution Standard Terms") && 
                        !updateCustomerRequestRow.getLegalFormatType().equalsIgnoreCase("Customer Document"))
                        ADFContext.getCurrent().getRequestScope().put("KLXLegalFormatValue", "KLX Standard Terms");
                    else
                        ADFContext.getCurrent().getRequestScope().put("KLXLegalFormatValue", updateCustomerRequestRow.getLegalFormatType());
                }
                if(null != updateCustomerRequestRow.getReturnPartListFormat()){
                    if(updateCustomerRequestRow.getReturnPartListFormat().equalsIgnoreCase("Both KLX and Customer format") || 
                        updateCustomerRequestRow.getReturnPartListFormat().equalsIgnoreCase("KLX format"))
                        ADFContext.getCurrent().getRequestScope().put("KLXReturnFormatValue", updateCustomerRequestRow.getReturnPartListFormat());
                    else
                        ADFContext.getCurrent().getRequestScope().put("KLXReturnFormatValue", updateCustomerRequestRow.getReturnPartListFormat());
                }
                //ADFContext.getCurrent().getSessionScope().put("LegalFormat", updateCustomerRequestRow.getLegalFormatType());
                updateCustomerRequestRowSetIterator.closeRowSetIterator();
                CctCrfKlxOfferEOVORowImpl klxOfferRow = null;
                CctCrfKlxOfferEOVOImpl cctCrfKlxOfferEOVOImpl = getCctCrfKlxOfferEOVO();
                cctCrfKlxOfferEOVOImpl.setNamedWhereClauseParam("pCrfId", crfID);
                cctCrfKlxOfferEOVOImpl.executeQuery();
                RowSetIterator klxOfferRowSetIterator = cctCrfKlxOfferEOVOImpl.createRowSetIterator(null);
                while (klxOfferRowSetIterator.hasNext()) {
                    klxOfferRow = (CctCrfKlxOfferEOVORowImpl) klxOfferRowSetIterator.next();
                   
                }
                klxOfferRowSetIterator.closeRowSetIterator();

                ViewObjectImpl crfCustomerMasterEOVO = getCrfCustomerMasterEOVO();
                ViewObjectImpl cctCrfKlxMasterContractVO = getCctCrfKlxMasterContractVO1();
                CrfCustomerMasterEOVORowImpl masterRow = null;
                CctCrfKlxMasterContractVORowImpl contractMasterRow = null;
                if (null != customerNumber && !"".equalsIgnoreCase(customerNumber)) {
                    crfCustomerMasterEOVO.setNamedWhereClauseParam("pCustomerNo", customerNumber);
                    crfCustomerMasterEOVO.executeQuery();
                    RowSetIterator createRowSetIterator = crfCustomerMasterEOVO.createRowSetIterator(null);
                    while (createRowSetIterator.hasNext()) {
                        masterRow = (CrfCustomerMasterEOVORowImpl) createRowSetIterator.next();
                    }
                    createRowSetIterator.closeRowSetIterator();

                    cctCrfKlxMasterContractVO.setNamedWhereClauseParam("pCustomerNo", customerNumber);
                 
                    cctCrfKlxMasterContractVO.executeQuery();
                    RowSetIterator createContractRowSetIterator = cctCrfKlxMasterContractVO.createRowSetIterator(null);
                    while (createContractRowSetIterator.hasNext()) {
                        contractMasterRow = (CctCrfKlxMasterContractVORowImpl) createContractRowSetIterator.next();
                        if (createContractRowSetIterator.hasNext())
                            containsMoreThanOneRow = true;
                        /* if(null != contractMasterRow.getExistingContract())
                            existingContract.add(contractMasterRow.getExistingContract());
                        if(null != contractMasterRow.getExistingContractType())
                            existingContractType.add(contractMasterRow.getExistingContractType());*/
                    }
                    /*
                    while (createContractRowSetIterator.hasNext()) {
                        contractMasterRow = (CctCrfKlxMasterContractVORowImpl) createContractRowSetIterator.next();
                    }*/
                    createContractRowSetIterator.closeRowSetIterator();
                }
                ViewObjectImpl cctSalesHierarchyFilter = getCctSalesHierarchyFilter();


                if (null != masterRow && null!=infoRow) {
                    infoRow.setCustomerNo(masterRow.getCustomerNo());
                    infoRow.setCustomerName(masterRow.getCustomerName());
                    infoRow.setAccountType(masterRow.getAccountType());
                    infoRow.setIsGt25(masterRow.getIsGt25());
                    infoRow.setIsEcomm(masterRow.getIsEcomm());
                    infoRow.setCurrencyCode(masterRow.getCurrencyCode());
                    infoRow.setPaymentTerms(masterRow.getPaymentTerms());
                    infoRow.setIncoTerms(masterRow.getIncoTerms());
                    infoRow.setUltimtateDestination(masterRow.getUltimtateDestination());
                    infoRow.setIsDfar(masterRow.getIsDfar());
                    infoRow.setIsAslApplies(masterRow.getIsAslApplicable());
                    infoRow.setWarehouseInfo(masterRow.getWarehouseInfo());
                    infoRow.setLineMin(masterRow.getLineMin());
                    infoRow.setOrderMin(masterRow.getOrderMin());
                    infoRow.setSalesOwner(masterRow.getSalesOwner());
                   
                    infoRow.setSalesTeam(masterRow.getSalesTeamNumber() + " - " + masterRow.getSalesTeam());
                    infoRow.setSalesOutside(masterRow.getSalesOutside());
                    if (null!=infoRow) {
                        if (containsMoreThanOneRow) {
                            //infoRow.setExistingContractNumber(new BigDecimal(9999));
                            infoRow.setExistingContract("Multiple");
                            infoRow.setExistingContractType("Multiple");
                        } else {
                            if (null != contractMasterRow.getExistingContract()) {
                                //infoRow.setExistingContractNumber(new BigDecimal(Integer.parseInt(contractMasterRow.getExistingContract())));
                                infoRow.setExistingContract(contractMasterRow.getExistingContract());
                            } else {
                                //infoRow.setExistingContractNumber(null);
                                infoRow.setExistingContract(null);
                            }
                            if (null != contractMasterRow.getExistingContractType())
                                infoRow.setExistingContractType(contractMasterRow.getExistingContractType());
                            else
                                infoRow.setExistingContractType(null);
                        }
                    }
                    //infoRow.setExistingContract(existingContract.toString().replace("[", "").replace("]", ""));
                    //infoRow.setExistingContractNumber(new BigDecimal(Integer.parseInt(contractMasterRow.getExistingContract())));
                    //infoRow.setExistingContractType(existingContractType.toString().replace("[", "").replace("]", ""));
                    //infoRow.setExistingContractType(contractMasterRow.getExistingContractType());

                }
                try {
                    cctSalesHierarchyFilter.setNamedWhereClauseParam("pTeamNo", masterRow.getSalesTeamNumber());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                cctSalesHierarchyFilter.executeQuery();
                RowSetIterator salesRowSetIter = cctSalesHierarchyFilter.createRowSetIterator(null);
                while (salesRowSetIter.hasNext()) {
                    CctSalesHierarchyNewVORowImpl salesRow = (CctSalesHierarchyNewVORowImpl) salesRowSetIter.next();
                   
                }
                salesRowSetIter.closeRowSetIterator();

                CctCrfKlxAwardVOImpl cctCrfKlxAwardVOImpl = getCctCrfKlxAwardVO1();
                cctCrfKlxAwardVOImpl.setNamedWhereClauseParam("pCrfId", crfID);
                cctCrfKlxAwardVOImpl.executeQuery();

                RowSetIterator klxAwardRowSetIterator = cctCrfKlxAwardVOImpl.createRowSetIterator(null);
                while (klxAwardRowSetIterator.hasNext()) {
                    CctCrfKlxAwardVORowImpl klxAwardRow = (CctCrfKlxAwardVORowImpl) klxAwardRowSetIterator.next();
                  
                }
                klxAwardRowSetIterator.closeRowSetIterator();

                CctCrfDateMgmtVOImpl cctCrfDateMgmtVO = getCctCrfDateMgmtVO();
                cctCrfDateMgmtVO.setNamedWhereClauseParam("b_crfId", crfID);
                cctCrfDateMgmtVO.executeQuery();
                       
                CctCrfAwardDatesVOImpl cctCrfAwardDatesVO = getCctCrfAwardDatesVO();
                cctCrfAwardDatesVO.setNamedWhereClauseParam("b_crfId", crfID);
                cctCrfAwardDatesVO.executeQuery();

            }
            ViewObjectImpl cctNotesUpdateOppVO = getCctNotesUpdateOppVO();
            ViewCriteria criteria = cctNotesUpdateOppVO.getViewCriteria("CctNotesUpdateOppVOCriteria");
            cctNotesUpdateOppVO.setNamedWhereClauseParam("b_crfNo", crfNumber);
            cctNotesUpdateOppVO.applyViewCriteria(criteria);
            cctNotesUpdateOppVO.executeQuery();
           
        }
        updatesupportTeam(crfNumber);
        } catch (Exception e) {
            e.printStackTrace();
            throw new SystemException("CCT", "initializeCreateOpp", "CCT_SYS_EX01", e); 
        }
        logger.log(Level.INFO, getClass(), "initializeUpdateOpp", "Exiting initializeUpdateOpp method");
        return null;
    }

    public void changeCRFStatus(String crfNumber) {
        BigDecimal crfID = null;
        if (null != crfNumber) {
            CctCrfEoVOImpl ccctCrfEOVO = getUpdateCcctCrfEOVO();
            ccctCrfEOVO.setpCrfNo(crfNumber);
            ccctCrfEOVO.executeQuery();
            OpportunityEVOImpl oppImpl = getOpportunityEVO1();
            ViewCriteria vc = oppImpl.getViewCriteria("OpportunityEVOCriteria");
            oppImpl.setNamedWhereClauseParam("bind_oppNum", crfNumber);
            oppImpl.applyViewCriteria(vc);
            oppImpl.executeQuery();
            RowSetIterator ccctCrfRowSetIterator = ccctCrfEOVO.createRowSetIterator(null);
            RowSetIterator oppRowSetIterator = oppImpl.createRowSetIterator(null);
            while (ccctCrfRowSetIterator.hasNext()) {
                CctCrfEoVORowImpl currentRow = (CctCrfEoVORowImpl) ccctCrfRowSetIterator.next();
                crfID = currentRow.getCrfId();
                if((currentRow.getAttribute("CrfStatus").toString()).equalsIgnoreCase("Active"))
                    currentRow.setAttribute("CrfStatus", "Pending");                                
            }
            while(oppRowSetIterator.hasNext()){
                OpportunityEVORowImpl currentOppRow = (OpportunityEVORowImpl) oppRowSetIterator.next();
                //crfID = currentRow.getCrfId();
                if((currentOppRow.getAttribute("Status").toString()).equalsIgnoreCase("Active"))
                    currentOppRow.setAttribute("Status", "Pending");                
                
            }
            getDBTransaction().commit();
            ccctCrfRowSetIterator.closeRowSetIterator();
            oppRowSetIterator.closeRowSetIterator();
        }
    }

    /**
     * Container's getter for CctCrfEoVO1.
     * @return CctCrfEoVO1
     */
    public CctCrfEoVOImpl getUpdateCcctCrfEOVO() {
        return (CctCrfEoVOImpl) findViewObject("UpdateCcctCrfEOVO");
    }


    /**
     * Container's getter for CctCrfCustomerRequestEOVO1.
     * @return CctCrfCustomerRequestEOVO1
     */
    public CctCrfCustomerRequestEOVOImpl getUpdateCrfCustReqEOVO() {
        return (CctCrfCustomerRequestEOVOImpl) findViewObject("UpdateCrfCustReqEOVO");
    }

    /**
     * Container's getter for CctCrfKlxOfferEOVO1.
     * @return CctCrfKlxOfferEOVO1
     */
    public CctCrfKlxOfferEOVOImpl getCctCrfKlxOfferEOVO() {
        return (CctCrfKlxOfferEOVOImpl) findViewObject("CctCrfKlxOfferEOVO");
    }

    public String assignOpptytoUsers(String toUser, BigDecimal crfId) {
        BigDecimal currentCrfId = null;
        int res = 0;
        CctCrfOpptyDashboardVOImpl cctCrfEoVO = getCctCrfOpptyDashboardVO();
        RowSetIterator cctCrfRow = cctCrfEoVO.createRowSetIterator(null);
        while (cctCrfRow.hasNext()) {
            Row currentRow = cctCrfRow.next();
            currentCrfId = (BigDecimal) currentRow.getAttribute("CrfId");
            res = currentCrfId.compareTo(crfId);
            if (res == 0) {
                currentRow.setAttribute("AssignedTo", toUser);
                getDBTransaction().commit();
                break;
            }
        }
        return "success";

    }

    /**
     * Container's getter for CctCrfOpptyDashboardVO1.
     * @return CctCrfOpptyDashboardVO1
     */
    public CctCrfOpptyDashboardVOImpl getCctCrfOpptyDashboardVO() {
        return (CctCrfOpptyDashboardVOImpl) findViewObject("CctCrfOpptyDashboardVO");
    }

    /**
     * Container's getter for CctCrfSalesTeamROVO1.
     * @return CctCrfSalesTeamROVO1
     */
    public ViewObjectImpl getCctCrfSalesTeamROVO() {
        return (ViewObjectImpl) findViewObject("CctCrfSalesTeamROVO");
    }


    /**
     * Container's getter for CctCrfAslCodesEOVO1.
     * @return CctCrfAslCodesEOVO1
     */
    public ViewObjectImpl getCctCrfAslCodesEOVO1() {
        return (ViewObjectImpl) findViewObject("CctCrfAslCodesEOVO1");
    }

    /**
     * Container's getter for CctCrfCustomerInfoEOVO1.
     * @return CctCrfCustomerInfoEOVO1
     */
    public CctCrfCustomerInfoEOVOImpl getUpdateCustomerInfoEOVO() {
        return (CctCrfCustomerInfoEOVOImpl) findViewObject("UpdateCustomerInfoEOVO");
    }

    /**
     * Container's getter for CCTOpptySearchROVO1.
     * @return CCTOpptySearchROVO1
     */
    public ViewObjectImpl getTeamOpptySearchROVO() {
        return (ViewObjectImpl) findViewObject("TeamOpptySearchROVO");
    }
    
    public List<BigDecimal> getSalesTeamNumbersOfUser(String loggedInUser){
        SalesTeamROVOImpl salesTeamROVO = getSalesTeamROVO1();
        salesTeamROVO.setbind_Owner_ID(loggedInUser);
        salesTeamROVO.executeQuery();
        RowSetIterator createRowSetIterator = null;
        BigDecimal[] salesTeamNunber = null;
        List<BigDecimal> salesTeam = new ArrayList<BigDecimal>();       
        int count = 0;
        createRowSetIterator = salesTeamROVO.createRowSetIterator(null);
        if(createRowSetIterator.hasNext()){
            while(createRowSetIterator.hasNext()){
                Row salesTeamRow = createRowSetIterator.next(); 
                //salesTeamNunber[count] = new BigDecimal(salesTeamRow.getAttribute("SalesTeamNumber").toString());
                //salesTeamNunber[count] = BigDecimal.valueOf(Double.valueOf(salesTeamRow.getAttribute("SalesTeamNumber").toString()));
                salesTeam.add(BigDecimal.valueOf(Double.valueOf(salesTeamRow.getAttribute("SalesTeamNumber").toString())));
                count++;
            }
        }
        else{
        String s = "select SALES_TEAM_NUMBER from CCT_CRF_SALES_TEAM where IS_ACTIVE = 'Y' and SALES_OWNER_ID like ?";

        PreparedStatement stmt = getDBTransaction().createPreparedStatement(s, 0);

        ResultSet resultset = null;
        //BigDecimal salesTeamNo = null;
        try {
            stmt.setString(1, loggedInUser.toLowerCase());

            resultset = stmt.executeQuery();
          
            //if((resultset.next())){
            while (resultset.next()) {
                //salesTeamNo = resultset.getBigDecimal("SALES_TEAM_NUMBER");
               
                //salesTeamNunber[count] = new BigDecimal(Double.valueOf(resultset.getBigDecimal("SALES_TEAM_NUMBER").toString()));
                if(null != resultset.getBigDecimal("SALES_TEAM_NUMBER"))
                    salesTeam.add(BigDecimal.valueOf(Double.valueOf(resultset.getBigDecimal("SALES_TEAM_NUMBER").toString())));
               // count++;
            }
        }catch (SQLException ex) {
            ex.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "getSalesTeamNumbersOfUser", ex.getMessage());
            throw new SystemException("CCT", "getSalesTeamNumbersOfUser", "CCT_SYS_EX10", ex);
        } finally {
            try {
                resultset.close();
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.log(Level.SEVERE, getClass(), "getSalesTeamNumbersOfUser", e.getMessage());
                throw new SystemException("CCT", "getSalesTeamNumbersOfUser", "CCT_SYS_EX10", e);
            }
        }
        }
        //return salesTeamNo;
        return salesTeam;
    }

    public List<BigDecimal> getSalesTeamNoOfUser(String loggedInUser) {
        SalesTeamROVOImpl salesTeamROVO = getSalesTeamROVO1();
        salesTeamROVO.setbind_Owner_ID(loggedInUser);
        salesTeamROVO.executeQuery();
        RowSetIterator createRowSetIterator = null;
        BigDecimal[] salesTeamNunber = null;
        List<BigDecimal> salesTeam = new ArrayList<BigDecimal>();
        //String quoteRevisonID = "null";
        int count = 0;
        createRowSetIterator = salesTeamROVO.createRowSetIterator(null);
        if(createRowSetIterator.hasNext()){
            while(createRowSetIterator.hasNext()){
                Row salesTeamRow = createRowSetIterator.next();                
                salesTeam.add(BigDecimal.valueOf(Double.valueOf(salesTeamRow.getAttribute("SalesTeamNumber").toString())));
                count++;
            }
        }
        else{
            String s = "select SALES_TEAM_NUMBER from CCT_CRF_SALES_TEAM where IS_ACTIVE = 'Y' and SALES_OWNER_ID like ?";
    
            PreparedStatement stmt = getDBTransaction().createPreparedStatement(s, 0);
    
            ResultSet resultset = null;
            BigDecimal salesTeamNo = BigDecimal.ZERO;
            try {
                stmt.setString(1, loggedInUser.toLowerCase());
    
                resultset = stmt.executeQuery();
              
                //while (resultset.next()) {
                if(resultset.next()){
                    if(null != resultset.getBigDecimal("SALES_TEAM_NUMBER"))
                        //salesTeamNo = resultset.getBigDecimal("SALES_TEAM_NUMBER");
                        salesTeam.add(BigDecimal.valueOf(Double.valueOf(resultset.getBigDecimal("SALES_TEAM_NUMBER").toString())));
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
                logger.log(Level.SEVERE, getClass(), "getSalesTeamNoOfUser", ex.getMessage());
                throw new SystemException("CCT", "getSalesTeamNoOfUser", "CCT_SYS_EX10", ex);
            } finally {
                try {
                    resultset.close();
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    logger.log(Level.SEVERE, getClass(), "getSalesTeamNoOfUser", e.getMessage());
                    throw new SystemException("CCT", "getSalesTeamNoOfUser", "CCT_SYS_EX10", e);
                }
            }
        }
        //return salesTeamNo;
        return salesTeam;
    }


    /**
     * Container's getter for CctCrfDateMgmtVO1.
     * @return CctCrfDateMgmtVO1
     */
    public CctCrfDateMgmtVOImpl getCctCrfDateMgmtVO() {
        return (CctCrfDateMgmtVOImpl) findViewObject("CctCrfDateMgmtVO");
    }

    /**
     * Container's getter for CctCrfAwardDatesVO1.
     * @return CctCrfAwardDatesVO1
     */
    public CctCrfAwardDatesVOImpl getCctCrfAwardDatesVO() {
        return (CctCrfAwardDatesVOImpl) findViewObject("CctCrfAwardDatesVO");
    }


    /**
     * Container's getter for CctCrfKlxAwardVO1.
     * @return CctCrfKlxAwardVO1
     */
    public CctCrfKlxAwardVOImpl getCctCrfKlxAwardVO1() {
        return (CctCrfKlxAwardVOImpl) findViewObject("CctCrfKlxAwardVO1");
    }

    /**
     * Container's getter for CctCrfKlxMasterContractVO1.
     * @return CctCrfKlxMasterContractVO1
     */
    public CctCrfKlxMasterContractVOImpl getCctCrfKlxMasterContractVO1() {
        return (CctCrfKlxMasterContractVOImpl) findViewObject("CctCrfKlxMasterContractVO1");
    }

    /**
     * Container's getter for CctNotesUpdateOppVO1.
     * @return CctNotesUpdateOppVO1
     */
    public ViewObjectImpl getCctNotesUpdateOppVO() {
        return (ViewObjectImpl) findViewObject("CctNotesUpdateOppVO");
    }

    /**
     * Container's getter for CctLookUpEOVO2.
     * @return CctLookUpEOVO2
     */
    public ViewObjectImpl getCctLookUpNotesEOVO() {
        return (ViewObjectImpl) findViewObject("CctLookUpNotesEOVO");
    }

    public void updateToOpportunity(String crfNo) {
        CallableStatement cst = null;
        String stmt = " call  cct_create_opportunity.update_to_opportunity (?)"; //crfNo passed in the procedure
        try {
            cst = this.getDBTransaction().createCallableStatement(stmt, 0);
            cst.setString(1, crfNo);
            cst.executeUpdate();
        } catch (SQLException ex) {
        
            ex.printStackTrace();
        } finally {
            if (cst != null) {
                try {
                    cst.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        ViewObject vo = getOpportunityVO1();
        vo.reset();
        vo.clearCache();
        vo.setNamedWhereClauseParam("bind_oppNo", crfNo);
        vo.executeQuery();

    }

    public void updateOpportunity() {
        getDBTransaction().commit();
    }


    /**
     * Container's getter for CctNotesVO2.
     * @return CctNotesVO2
     */
    public ViewObjectImpl getCctNotesSearchByNoteTypeVO() {
        return (ViewObjectImpl) findViewObject("CctNotesSearchByNoteTypeVO");
    }

    public void createRevisionForOppty(String opptyNo) {
            logger.log(Level.INFO, getClass(), "createRevisionForOppty", "Entering createRevisionForOppty method");
            BigDecimal crfRevisionId = getDBSeq(ModelConstants.CCT_CRF_REVISION_SEQ);
            CctCrfRevisionVOImpl cctCrfRevisionVO = getCctCrfRevisionVO();
            cctCrfRevisionVO.reset();
            cctCrfRevisionVO.clearCache();
            cctCrfRevisionVO.executeQuery();
            CctCrfRevisionVORowImpl cctCrfRevisionVORow = (CctCrfRevisionVORowImpl)cctCrfRevisionVO.getCurrentRow();
            cctCrfRevisionVORow.setCrfRevisionId(crfRevisionId);
            if(null == cctCrfRevisionVORow.getAttribute("NotifyLegal")){
                cctCrfRevisionVORow.setNotifyLegal("N");
            }
            if(null == cctCrfRevisionVORow.getAttribute("NotifyParts")){
                cctCrfRevisionVORow.setNotifyParts("N");
            }
            if(null == cctCrfRevisionVORow.getAttribute("NotifyProgram")){
                cctCrfRevisionVORow.setNotifyProgram("N");
            }
            
            getDBTransaction().commit();
        logger.log(Level.INFO, getClass(), "createRevisionForOppty", "Successful transaction with cctCrfRevisionVO");
        BigDecimal quoteRevisionId = null;
        CallableStatement cst = null;
        BigDecimal oppID = null;
        String stmt = " call  cct_quote_creation.create_quote_revisions (?,?,?)"; //opptyNo passed in the procedure
        try {
            cst = this.getDBTransaction().createCallableStatement(stmt, 0);
            cst.setString(1, opptyNo);
            cst.registerOutParameter(2, Types.INTEGER);
            cst.registerOutParameter(3, Types.VARCHAR);
            cst.executeUpdate();

            quoteRevisionId = cst.getBigDecimal(2);
            ReadOnlyOpportunityRVOImpl createVO = getFilterOppOnCreateVO();
            createVO.setpOppNum(opptyNo);
            createVO.executeQuery();
            RowSetIterator createRowSetIterator = createVO.createRowSetIterator(null);
            while (createRowSetIterator.hasNext()) {
                ReadOnlyOpportunityRVORowImpl currentRow = (ReadOnlyOpportunityRVORowImpl) createRowSetIterator.next();
                oppID = currentRow.getOpportunityId();
            }
            createRowSetIterator.closeRowSetIterator();
            UpdateOpportunityManagerImpl impl = new UpdateOpportunityManagerImpl();
            impl.createOpportunityRevision(opptyNo, oppID.toString());
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, getClass(), "createRevisionForOppty", ex.getMessage());
            throw new SystemException("CCT", "createRevisionForOppty", "CCT_BUS_EX01", ex);
        } catch (Exception ae) {
            logger.log(Level.SEVERE, getClass(), "createRevisionForOppty", ae.getMessage());            
            throw new SystemException("CCT", "createRevisionForOppty", "CCT_BUS_EX01", ae);
        } finally {
            if (cst != null) {
                try {
                    cst.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        logger.log(Level.INFO, getClass(), "createRevisionForOppty", "Exiting createRevisionForOppty method");
    }

    public void changeCRFstatusForRevision(String crfNumber) {
        BigDecimal crfID = null;
        if (null != crfNumber) {
            CctCrfEoVOImpl ccctCrfEOVO = getUpdateCcctCrfEOVO();
            ccctCrfEOVO.setpCrfNo(crfNumber);
            ccctCrfEOVO.executeQuery();
            OpportunityEVOImpl oppImpl = getOpportunityEVO1();
            ViewCriteria vc = oppImpl.getViewCriteria("OpportunityEVOCriteria");
            oppImpl.setNamedWhereClauseParam("bind_oppNum", crfNumber);
            oppImpl.applyViewCriteria(vc);
            oppImpl.executeQuery();

            RowSetIterator ccctCrfRowSetIterator = ccctCrfEOVO.createRowSetIterator(null);
            RowSetIterator oppRowSetIterator = oppImpl.createRowSetIterator(null);
            while (ccctCrfRowSetIterator.hasNext()) {
                CctCrfEoVORowImpl currentRow = (CctCrfEoVORowImpl) ccctCrfRowSetIterator.next();
                crfID = currentRow.getCrfId();
                currentRow.setAttribute("CrfStatus", "Active");
                currentRow.setAttribute("Revision", "Yes");                
            }
            while(oppRowSetIterator.hasNext()){
                OpportunityEVORowImpl currentOppRow = (OpportunityEVORowImpl) oppRowSetIterator.next();
                //crfID = currentRow.getCrfId();
                //if((currentOppRow.getAttribute("Status").toString()).equalsIgnoreCase("Active"))
                    currentOppRow.setAttribute("Status", "Active");                
                
            }
            getDBTransaction().commit();
            ccctCrfRowSetIterator.closeRowSetIterator();
            oppRowSetIterator.closeRowSetIterator();
        }
    }

    /**
     * Container's getter for supportTeamROVO1.
     * @return supportTeamROVO1
     */
    public supportTeamROVOImpl getsupportTeamROVO() {
        return (supportTeamROVOImpl) findViewObject("supportTeamROVO");
    }

    public void updatesupportTeam(String opportunityNumber) {
        logger.log(Level.INFO, getClass(), "updatesupportTeam", "Entering updatesupportTeam method");
        logger.log(Level.INFO, getClass(), "updatesupportTeam", "Input parameters passing  opportunityNumber:--"+opportunityNumber+"--");
        try
        {
            LatestRevisonROVOImpl latestRevisonROVO = getLatestRevisonROVO();
            latestRevisonROVO.setpOpportunityNumber(opportunityNumber);
            latestRevisonROVO.executeQuery();
            RowSetIterator createRowSetIteratorLat = null;
            String quoteRevisonID = "null";
            createRowSetIteratorLat = latestRevisonROVO.createRowSetIterator(null);
            while (createRowSetIteratorLat.hasNext()) {
                Row row = createRowSetIteratorLat.next();
                quoteRevisonID = row.getAttribute("RevisionId").toString();
            }
            try {
//                CctTop50ProductQueueViewImpl cctTop50ProductQueueView = getCctTop50ProductQueueView();
//                cctTop50ProductQueueView.setNamedWhereClauseParam("b_quoteRevisionId", quoteRevisonID);
//                cctTop50ProductQueueView.executeQuery(); 
//
                CctFinalTop50ProdQueueViewImpl cctFinalTop50ProdQueueView = getCctFinalTop50ProdQueueView();
                cctFinalTop50ProdQueueView.setNamedWhereClauseParam("b_quoteRevisionId", quoteRevisonID);
                cctFinalTop50ProdQueueView.executeQuery();
                
                CctTop50ProdQueueVOImpl cctTop50ProdQueueVO = getCctTop50ProdQueueVO();
                cctTop50ProdQueueVO.setNamedWhereClauseParam("b_quoteRevisionId", quoteRevisonID);
                cctTop50ProdQueueVO.executeQuery();
                
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            supportTeamROVOImpl supportTeamROVOImpl = getsupportTeamROVO();
        RowSetIterator createRowSetIterator = supportTeamROVOImpl.createRowSetIterator(null);
        supportTeamROVORowImpl supportTeamRow = null;
       // try{
        while (createRowSetIterator.hasNext()) {
            supportTeamRow = (supportTeamROVORowImpl) createRowSetIterator.next();
        }
        if (null != supportTeamRow) {
            supportTeamRow.setProposal(null);
            supportTeamRow.setPricing(null);
            supportTeamRow.setAdvancedSourcing(null);
            supportTeamRow.setHpp(null);
            supportTeamRow.setLighting(null);
            supportTeamRow.setElectrical(null);
            supportTeamRow.setChemical(null);
            supportTeamRow.setLegal(null);
            supportTeamRow.setOperations(null);
            supportTeamRow.setProgramSolutions(null);
            supportTeamRow.setQuality(null);
            supportTeamRow.setContractsAdministrator(null);
            
            TaskQueryManagerImpl impl = new TaskQueryManagerImpl();
            Map supportTeamMap = new HashMap();
            List<Tasks> taskList = impl.fetchTasksListByOppNo(opportunityNumber, null);
            for (Tasks task : taskList) {
                String group = task.getGroup();
                String assignee = task.getAssigneeName();
                logger.log(Level.INFO, getClass(), "updatesupportTeam", "group :--"+group+"--assignee:--"+assignee+"--");
                if (null != assignee) {
                    if (((String)ParamCache.userRoleMap.get("CCT_PROPOSAL_MANAGER")).equalsIgnoreCase(group)) {
                        supportTeamRow.setProposal(assignee);
                     //   updateTop50ProdQueue("Chemical", assignee);
                    }
                    if (((String)ParamCache.userRoleMap.get("CCT_STRATEGIC_PRICING")).equalsIgnoreCase(group)) {
                        supportTeamRow.setPricing(assignee);
                        updateTop50ProdQueue("Strategic Pricing", assignee);
                        updateFinalTop50ProdQueue("Strategic Pricing", assignee);
                    }
                    if (((String)ParamCache.userRoleMap.get("CCT_ADVANCED_SOURCING")).equalsIgnoreCase(group)) {
                        supportTeamRow.setAdvancedSourcing(assignee);
                        updateTop50ProdQueue("Advance Sourcing", assignee);
                        updateFinalTop50ProdQueue("Advance Sourcing", assignee);
                    }
                    if (((String)ParamCache.userRoleMap.get("CCT_HONEYWELL_PRODUCT_LINE")).equalsIgnoreCase(group)) {
                        supportTeamRow.setHpp(assignee);
                        updateTop50ProdQueue("HPP", assignee);
                        updateFinalTop50ProdQueue("HPP", assignee);
                    }
                    if (((String)ParamCache.userRoleMap.get("CCT_LIGHTING_PRODUCT_LINE")).equalsIgnoreCase(group)) {
                        supportTeamRow.setLighting(assignee);
                        updateTop50ProdQueue("Lighting", assignee);
                        updateFinalTop50ProdQueue("Lighting", assignee);
                    }                    
                    if (((String)ParamCache.userRoleMap.get("CCT_ELECTRICAL_PRODUCT_LINE")).equalsIgnoreCase(group)) {
                        supportTeamRow.setElectrical(assignee);
                        updateTop50ProdQueue("Electrical", assignee);
                        updateFinalTop50ProdQueue("Electrical", assignee);
                    }
                    if (((String)ParamCache.userRoleMap.get("CCT_CSM")).equalsIgnoreCase(group)) {
                        supportTeamRow.setChemical(assignee);
                        updateTop50ProdQueue("Chemical", assignee);
                        updateFinalTop50ProdQueue("Chemical", assignee);
                    }
                    if (((String)ParamCache.userRoleMap.get("CCT_LEGAL")).equalsIgnoreCase(group)) {
                        supportTeamRow.setLegal(assignee);
                     //   updateTop50ProdQueue("", assignee);
                    }
                    if (((String)ParamCache.userRoleMap.get("CCT_OPERATIONS")).equalsIgnoreCase(group)) {
                        supportTeamRow.setOperations(assignee);
                    //    updateTop50ProdQueue("", assignee);
                    }
                    if (((String)ParamCache.userRoleMap.get("CCT_PROGRAM_SOLUTIONS")).equalsIgnoreCase(group)) {
                        supportTeamRow.setProgramSolutions(assignee);
                    //    updateTop50ProdQueue("", assignee);
                    }
                    if (((String)ParamCache.userRoleMap.get("CCT_QUALITY")).equalsIgnoreCase(group)) {
                        supportTeamRow.setQuality(assignee);
                    //    updateTop50ProdQueue("", assignee);
                    }
                    if (((String)ParamCache.userRoleMap.get("CCT_CONTRACTS_ADMINISTRATOR")).equalsIgnoreCase(group)) {
                        supportTeamRow.setContractsAdministrator(assignee);
                     //   updateTop50ProdQueue("", assignee);
                    }
                }
            }
        }
        } catch (Exception e) {
            e.printStackTrace();     
        }
        logger.log(Level.INFO, getClass(), "updatesupportTeam", "Exiting updatesupportTeam method");
    }

    /**
     * Container's getter for CctCrfSalesTeamROVO1.
     * @return CctCrfSalesTeamROVO1
     */
    public ViewObjectImpl getCctCrfSalesTeamAllOpptyROVO() {
        return (ViewObjectImpl) findViewObject("CctCrfSalesTeamAllOpptyROVO");
    }

    public String passApprovalProc(int snapshotId) {
        CallableStatement cst = null;
        String stmt = " call CCT_APPROVAL_PROCESS.update_approval_status (?,?,?)";
        try {
            cst = this.getDBTransaction().createCallableStatement(stmt, 0);
            cst.setNull(1, Types.INTEGER);
            //cst.setInt(1, null);
            cst.setInt(2, snapshotId);
            cst.setString(3, "PASSED BY PROPOSAL MANAGER");
            cst.executeUpdate();
        } catch (SQLException e) {
            throw new JboException(e.getMessage());
        } finally {
            if (cst != null) {
                try {
                    cst.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return "success";

    }


    /**
     * Container's getter for CctApprovalUsersEOVO1.
     * @return CctApprovalUsersEOVO1
     */
    public CctApprovalUsersEOVOImpl getCctApprovalUsersEOVO() {
        return (CctApprovalUsersEOVOImpl) findViewObject("CctApprovalUsersEOVO");
    }


    /**
     * Container's getter for ApprovalUsersTRVO1.
     * @return ApprovalUsersTRVO1
     */
    public ApprovalUsersTRVOImpl getApprovalUsersTRVO() {
        return (ApprovalUsersTRVOImpl) findViewObject("ApprovalUsersTRVO");
    }

    public String insertApprovalUsers(HashMap hashMap) {
        String status="Failure";
        try {
            ApprovalUserROVOImpl approvalUserROVO = getApprovalUserROVO();
            RowSetIterator createRowSetIterator = approvalUserROVO.createRowSetIterator(null);
            while (createRowSetIterator.hasNext()) {
                ApprovalUserROVORowImpl currentRow = (ApprovalUserROVORowImpl) createRowSetIterator.next();
                String lvlOne = currentRow.getSaleslvlone();
                if (null != lvlOne && lvlOne.contains("-")) {
                    hashMap.put("userInfo", lvlOne);
                    hashMap.put("role", "CCT_SALES_DIRECTOR");
                    createApprovalUsers(hashMap);
                } else {
                    hashMap.put("role", "CCT_SALES_DIRECTOR");
                    deleteApproval(hashMap);
                }
                String lvlTwo = currentRow.getSaleslvltwo();
                if (null != lvlTwo && lvlTwo.contains("-")) {
                    hashMap.put("userInfo", lvlTwo);
                    hashMap.put("role", "CCT_SALES_VP");
                    createApprovalUsers(hashMap);
                } else {

                    hashMap.put("role", "CCT_SALES_VP");
                    deleteApproval(hashMap);
                }
                String gM = currentRow.getGvandgm();
                if (null != gM && gM.contains("-")) {
                    hashMap.put("userInfo", gM);
                    hashMap.put("role", "CCT_GROUP_VP_AND_GM");
                    createApprovalUsers(hashMap);
                } else {
                    hashMap.put("role", "CCT_GROUP_VP_AND_GM");
                    deleteApproval(hashMap);
                }
                String accountVicePresident = currentRow.getAccountvicepresident();
                if (null != accountVicePresident && accountVicePresident.contains("-")) {
                    hashMap.put("userInfo", accountVicePresident);
                    hashMap.put("role", "CCT_ACCOUNTING_DIRECTOR");
                    createApprovalUsers(hashMap);
                } else {
                    hashMap.put("role", "CCT_ACCOUNTING_DIRECTOR");
                    deleteApproval(hashMap);
                }
                String chiefFinanceOfficer = currentRow.getChieffinanceofficer();
                if (null != chiefFinanceOfficer && chiefFinanceOfficer.contains("-")) {
                    hashMap.put("userInfo", chiefFinanceOfficer);
                    hashMap.put("role", "CCT_CORPORATE_CFO");
                    createApprovalUsers(hashMap);
                } else {
                    hashMap.put("role", "CCT_CORPORATE_CFO");
                    deleteApproval(hashMap);
                }
                String operationsVicePres = currentRow.getOperationsvicepres();
                if (null != operationsVicePres && operationsVicePres.contains("-")) {
                    hashMap.put("userInfo", operationsVicePres);
                    hashMap.put("role", "CCT_OPERATIONS_VP");
                    createApprovalUsers(hashMap);
                } else {
                    hashMap.put("role", "CCT_OPERATIONS_VP");
                    deleteApproval(hashMap);
                }
                String financeVicePresident = currentRow.getFinancevicepresident();
                if (null != financeVicePresident && financeVicePresident.contains("-")) {
                    hashMap.put("userInfo", financeVicePresident);
                    hashMap.put("role", "CCT_FINANCE_VP");
                    createApprovalUsers(hashMap);
                } else {
                    hashMap.put("role", "CCT_FINANCE_VP");
                    deleteApproval(hashMap);
                }
                String supplyChainVicePresident = currentRow.getSupplychainvicepresident();
                if (null != supplyChainVicePresident && supplyChainVicePresident.contains("-")) {
                    hashMap.put("userInfo", supplyChainVicePresident);
                    hashMap.put("role", "CCT_SUPPLY_CHAIN_VP");
                    createApprovalUsers(hashMap);
                } else {
                    hashMap.put("role", "CCT_SUPPLY_CHAIN_VP");
                    deleteApproval(hashMap);
                }
                
                String srContDirec = currentRow.getSrcontratdirector();
                if (null != srContDirec && srContDirec.contains("-")) {
                    hashMap.put("userInfo", srContDirec);
                    hashMap.put("role", "CCT_SR_CONTRACTS_DIRECTOR");
                    createApprovalUsers(hashMap);
                } else {
                    hashMap.put("role", "CCT_SR_CONTRACTS_DIRECTOR");
                    deleteApproval(hashMap);
                }
            }
            getDBTransaction().commit();
            createRowSetIterator.closeRowSetIterator();
            status="success";
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
        return status;
    }


    public void deleteApproval(HashMap hashMap) {
        try {
            CctApprovalUsersEOVOImpl approvalUsersEOVO = getCctApprovalUsersEOVO();
            approvalUsersEOVO.setpOpptyID((BigDecimal) hashMap.get("opportunityId"));
            approvalUsersEOVO.setpSnapShotID((BigDecimal) hashMap.get("snapShotId"));
            approvalUsersEOVO.setpRoleId((String) hashMap.get("role"));
            approvalUsersEOVO.executeQuery();
            RowSetIterator createRowSetIterator = approvalUsersEOVO.createRowSetIterator(null);
            CctApprovalUsersEOVORowImpl currentRow = null;
            while (createRowSetIterator.hasNext()) {
                currentRow = (CctApprovalUsersEOVORowImpl) createRowSetIterator.next();
            }
            if (null != currentRow) {
                currentRow.remove();
                getDBTransaction().commit();
            }
            createRowSetIterator.closeRowSetIterator();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String createApprovalUsers(HashMap hashMap) {
        try {
            HashMap userMap = getUserIdAndName((String) hashMap.get("userInfo"));
            HashMap approvalUser = validateApprovalUser((String) userMap.get("userId"), (String) hashMap.get("role"));
            if("No".equalsIgnoreCase((String)approvalUser.get("validUser")))
                return null;
            CctApprovalUsersEOVOImpl approvalUsersEOVO = getCctApprovalUsersEOVO();
            approvalUsersEOVO.setpOpptyID((BigDecimal) hashMap.get("opportunityId"));
            approvalUsersEOVO.setpSnapShotID((BigDecimal) hashMap.get("snapShotId"));
            approvalUsersEOVO.setpRoleId((String) hashMap.get("role"));
            approvalUsersEOVO.executeQuery();
            RowSetIterator createRowSetIterator = approvalUsersEOVO.createRowSetIterator(null);
            CctApprovalUsersEOVORowImpl currentRow = null;
            while (createRowSetIterator.hasNext()) {
                currentRow = (CctApprovalUsersEOVORowImpl) createRowSetIterator.next();
                currentRow.setApprovalUserId((String) userMap.get("userId"));
                currentRow.setApprovalUserName((String) approvalUser.get("userName"));
            }
            createRowSetIterator.closeRowSetIterator();

            if (null == currentRow) {
                CctApprovalUsersEOVORowImpl createRow = (CctApprovalUsersEOVORowImpl) approvalUsersEOVO.createRow();
                try {
                    createRow.setSnapshotId((BigDecimal) hashMap.get("snapShotId"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    createRow.setOpportunityId((BigDecimal) hashMap.get("opportunityId"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                createRow.setApprovalRoleId((String) hashMap.get("role"));
                createRow.setApprovalUserId((String) userMap.get("userId"));
                createRow.setApprovalUserName((String) userMap.get("userName"));
                approvalUsersEOVO.insertRow(createRow);
            }
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
        return null;
    }

    public HashMap getUserIdAndName(String userInfo) {
        HashMap hashMap = new HashMap();
        if (null != userInfo) {
            int splitlength = userInfo.indexOf("-");
            hashMap.put("userId", (userInfo.substring(0, splitlength)).trim());
            hashMap.put("userName", userInfo.substring(splitlength + 1, userInfo.length()));
        }
        return hashMap;
    }

    /**
     * Container's getter for ApprovalUserROVO1.
     * @return ApprovalUserROVO1
     */
    public ApprovalUserROVOImpl getApprovalUserROVO() {
        return (ApprovalUserROVOImpl) findViewObject("ApprovalUserROVO");
    }

    public void executeApprovalUsers(HashMap hashMap) {
        ApprovalUserROVOImpl approvalUserROVO = getApprovalUserROVO();
        approvalUserROVO.clearCache();
        RowSetIterator approvalRowSetIterator = approvalUserROVO.createRowSetIterator(null);
        ApprovalUserROVORowImpl approvalUserRow = null;
        while (approvalRowSetIterator.hasNext()) {
            approvalUserRow = (ApprovalUserROVORowImpl) approvalRowSetIterator.next();
        }
        approvalRowSetIterator.closeRowSetIterator();
        CctApprovalUsersEOVOImpl approvalUsersEOVO = getFilterCctApprovalUsersEOVO();
        approvalUsersEOVO.setpOpptyID((BigDecimal) hashMap.get("opportunityId"));
        approvalUsersEOVO.setpSnapShotID((BigDecimal) hashMap.get("snapShotId"));
        approvalUsersEOVO.executeQuery();

        RowSetIterator createRowSetIterator = approvalUsersEOVO.createRowSetIterator(null);
        while (createRowSetIterator.hasNext()) {
            CctApprovalUsersEOVORowImpl currentRow = (CctApprovalUsersEOVORowImpl) createRowSetIterator.next();
            if (null != currentRow && null != approvalUserRow) {
                String approvalRoleId = currentRow.getApprovalRoleId();
                String approvalUserId = currentRow.getApprovalUserId();
                String approvalUserName = currentRow.getApprovalUserName();
                
                logger.log(Level.INFO, getClass(), "executeApprovalUsers", "approvalRoleId :--"+approvalRoleId+"--approvalUserId:--"+approvalUserId+"--approvalUserName--->"+approvalUserName);
                
                if ("CCT_SALES_DIRECTOR".equalsIgnoreCase(approvalRoleId))
                    approvalUserRow.setSaleslvlone(approvalUserId + "-" + approvalUserName);
                if ("CCT_SALES_VP".equalsIgnoreCase(approvalRoleId))
                    approvalUserRow.setSaleslvltwo(approvalUserId + "-" + approvalUserName);
                if ("CCT_ACCOUNTING_DIRECTOR".equalsIgnoreCase(approvalRoleId))
                    approvalUserRow.setAccountvicepresident(approvalUserId + "-" + approvalUserName);
                if ("CCT_GROUP_VP_AND_GM".equalsIgnoreCase(approvalRoleId))
                    approvalUserRow.setGvandgm(approvalUserId + "-" + approvalUserName);
                if ("CCT_CORPORATE_CFO".equalsIgnoreCase(approvalRoleId))
                    approvalUserRow.setChieffinanceofficer(approvalUserId + "-" + approvalUserName);
                if ("CCT_FINANCE_VP".equalsIgnoreCase(approvalRoleId))
                    approvalUserRow.setFinancevicepresident(approvalUserId + "-" + approvalUserName);
                if ("CCT_OPERATIONS_VP".equalsIgnoreCase(approvalRoleId))
                    approvalUserRow.setOperationsvicepres(approvalUserId + "-" + approvalUserName);
                if ("CCT_SR_CONTRACTS_DIRECTOR".equalsIgnoreCase(approvalRoleId))
                    approvalUserRow.setSrcontratdirector(approvalUserId + "-" + approvalUserName);
            }
        }
        createRowSetIterator.closeRowSetIterator();
    }

    /**
     * Container's getter for CctApprovalUsersEOVO1.
     * @return CctApprovalUsersEOVO1
     */
    public CctApprovalUsersEOVOImpl getFilterCctApprovalUsersEOVO() {
        return (CctApprovalUsersEOVOImpl) findViewObject("FilterCctApprovalUsersEOVO");
    }

    public HashMap validateApprovalUser(String userid, String userGroup) {
        String validUser = "No";
        String userName = "";
        HashMap validateMap = new HashMap();
        ActiveDirectoryManagerImpl adImpl = new ActiveDirectoryManagerImpl();
        User user = adImpl.fetchUserDetailsById(userid);
        if (null != user && null != user.getName()) {
            validUser = "Yes";
            userName = user.getName();
            Roles roles = user.getRoles();
            ArrayList<String> list = (ArrayList<String>) roles.getRole();
            String roleGroup=(String) ParamCache.userRoleMap.get(userGroup);
            if (null != list && list.contains(roleGroup))
                validUser = "Yes";
            else
                validUser = "No";
        }
        validateMap.put("validUser", validUser);
        validateMap.put("userName", userName);
        return validateMap;
    }
    
    public Object getKeyFromValue(Map map, Object value) {
        for (Object obj : map.keySet()) {
          if (map.get(obj).equals(value)) {
            return obj;
          }
        }
        return null;
      }

    /**
     * Container's getter for CrfCustomerMasterEOVO1.
     * @return CrfCustomerMasterEOVO1
     */
    public CrfCustomerMasterEOVOImpl getCrfCustomerMasterUpdateOpptyVO() {
        return (CrfCustomerMasterEOVOImpl) findViewObject("CrfCustomerMasterUpdateOpptyVO");
    }
    
    public void removeUnUsedRows(ViewObjectImpl viewObjectImpl){
            RowSetIterator createRowSetIterator = viewObjectImpl.createRowSetIterator(null);
            while (createRowSetIterator.hasNext()) {
                ViewRowImpl currentRow = (ViewRowImpl) createRowSetIterator.next();
                EntityImpl entityImpl = (EntityImpl) currentRow.getEntity(0);
                //System.out.println("view object :"+viewObjectImpl+"--Entirty state:--"+entityImpl.getEntityState()+"--post state:--"+entityImpl.getPostState());
                if (EntityImpl.STATUS_NEW == entityImpl.getEntityState()) {
                    currentRow.remove();
                }
            }
    }
    
    public boolean customerNoValidate(String custNo) {
        String s = "select * from CCT_CRF_CUSTOMER_MASTER where CUSTOMER_NO like ?";
        PreparedStatement stmt = getDBTransaction().createPreparedStatement(s, 0);
        ResultSet resultset = null;
        boolean row = false;
        try {
            stmt.setString(1, custNo);
            resultset = stmt.executeQuery();
            while (resultset.next()) {
                row = true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "customerNoValidate", ex.getMessage());
            throw new SystemException("CCT", "customerNoValidate", "CCT_SYS_EX10", ex);
        } finally {
            try {
                resultset.close();
                stmt.close();
            } catch (SQLException e) {
                //e.printStackTrace();
                logger.log(Level.SEVERE, getClass(), "customerNoValidate", e.getMessage());
                throw new SystemException("CCT", "customerNoValidate", "CCT_SYS_EX10", e);
            }
        }
        return row;
    }

    /**
     * Container's getter for CctCrfRevisionVO1.
     * @return CctCrfRevisionVO1
     */
    public CctCrfRevisionVOImpl getCctCrfRevisionVO() {
        return (CctCrfRevisionVOImpl) findViewObject("CctCrfRevisionVO");
    }

    /**
     * Container's getter for CctCrfRevisionVO1.
     * @return CctCrfRevisionVO1
     */
    public CctCrfRevisionVOImpl getCctCrfRevisionDateMgmtVO() {
        return (CctCrfRevisionVOImpl) findViewObject("CctCrfRevisionDateMgmtVO");
    }
    
    public String getRevisionDetails(String crfNumber){
        logger.log(Level.INFO, getClass(), "getRevisionDetails", "Entering getRevisionDetails method");
        String resvisionCreated = "";
        try{
            
        ViewObjectImpl cctCrfEoVO = getCctCrfEoVO();
        ViewCriteria vc = cctCrfEoVO.getViewCriteria("CctCrfEoVOCriteria");
        cctCrfEoVO.setNamedWhereClauseParam("pCrfNo", crfNumber);
        cctCrfEoVO.applyViewCriteria(vc);
        cctCrfEoVO.executeQuery();
        
        RowSetIterator cctCrfEoVOIterator = cctCrfEoVO.createRowSetIterator(null);
        if(cctCrfEoVOIterator.hasNext()){
        Row cctCrfEoVORow = cctCrfEoVOIterator.next();
        if(null != cctCrfEoVORow.getAttribute("Revision")){
        resvisionCreated = cctCrfEoVORow.getAttribute("Revision").toString(); 
        logger.log(Level.INFO, getClass(), "getRevisionDetails", "resvisionCreated="+resvisionCreated);
        }
        }
        cctCrfEoVOIterator.closeRowSetIterator();
        }catch(Exception e){
            e.printStackTrace();
        }
        logger.log(Level.INFO, getClass(), "getRevisionDetails", "Exiting getRevisionDetails method");
        return resvisionCreated;
    }

    /**
     * Container's getter for CctFinalTop50PartsView1.
     * @return CctFinalTop50PartsView1
     */
    public CctFinalTop50PartsViewImpl getCctFinalTop50PartsView() {
        return (CctFinalTop50PartsViewImpl) findViewObject("CctFinalTop50PartsView");
    }

    /**
     * Container's getter for CctFinalTop50ProdQueueView1.
     * @return CctFinalTop50ProdQueueView1
     */
    public CctFinalTop50ProdQueueViewImpl getCctFinalTop50ProdQueueView() {
        return (CctFinalTop50ProdQueueViewImpl) findViewObject("CctFinalTop50ProdQueueView");
    }


    public void updateTop50ProdQueue(String queueName,String poc){
        
        CctTop50ProdQueueVOImpl cctTop50ProdQueueVO = getCctTop50ProdQueueVO();
        RowSetIterator createRowSetIterator = cctTop50ProdQueueVO.createRowSetIterator(null);
        while(createRowSetIterator.hasNext()){
            CctTop50ProdQueueVORowImpl next = (CctTop50ProdQueueVORowImpl) createRowSetIterator.next();
            if(queueName.equalsIgnoreCase(next.getQueueName()))
            {
                next.setPoc(poc);
                }
        }
        createRowSetIterator.closeRowSetIterator();
        
    }
    
    public void updateFinalTop50ProdQueue(String queueName,String poc){
        CctFinalTop50ProdQueueViewImpl cctFinalTop50ProdQueueView = getCctFinalTop50ProdQueueView();
        RowSetIterator createRowSetIterator = cctFinalTop50ProdQueueView.createRowSetIterator(null);
        while(createRowSetIterator.hasNext()){
            CctFinalTop50ProdQueueViewRowImpl next = (CctFinalTop50ProdQueueViewRowImpl) createRowSetIterator.next();
            if(queueName.equalsIgnoreCase(next.getQueueName()))
            {
                next.setPoc(poc);
                }
        }
        createRowSetIterator.closeRowSetIterator();
    }

    /**
     * Container's getter for SalesTeamROVO1.
     * @return SalesTeamROVO1
     */
    public SalesTeamROVOImpl getSalesTeamROVO1() {
        return (SalesTeamROVOImpl) findViewObject("SalesTeamROVO1");
    }

    /**
     * Container's getter for CctTop50FinalPricingSummary1.
     * @return CctTop50FinalPricingSummary1
     */
    public CctTop50FinalPricingSummaryImpl getCctTop50FinalPricingSummary1() {
        return (CctTop50FinalPricingSummaryImpl) findViewObject("CctTop50FinalPricingSummary1");
    }

    /**
     * Container's getter for CctTop50ProdQueueVO1.
     * @return CctTop50ProdQueueVO1
     */
    public CctTop50ProdQueueVOImpl getCctTop50ProdQueueVO() {
        return (CctTop50ProdQueueVOImpl) findViewObject("CctTop50ProdQueueVO");
    }
    
    public BigDecimal getTop50Parts(BigDecimal quoteRevisionId) {
        logger.log(Level.INFO, getClass(), "gettop50parts", "Entering gettop50parts method");

        logger.log(Level.INFO, getClass(), "gettop50parts", "quoteRevisionId---"+ quoteRevisionId);
        BigDecimal ret_code = null;
        CallableStatement cst = null;
        String stmt = " call  cct_top50_parts_pkg.get_top50_parts (?,?,?)"; //quoteRevisionId passed in the procedure
        try {
            cst = this.getDBTransaction().createCallableStatement(stmt, 0);
            cst.setBigDecimal(1, quoteRevisionId);
            cst.registerOutParameter(2, Types.INTEGER);
            cst.registerOutParameter(3, Types.VARCHAR);
            cst.executeUpdate();
            ret_code = cst.getBigDecimal(2);           
        } catch (SQLException ex) {
            //ex.printStackTrace();
			logger.log(Level.SEVERE, getClass(), "gettop50parts", ex.getMessage());
            throw new SystemException("CCT", "gettop50parts", "CCT_BUS_EX01", ex);
       } catch (Exception ae) {
            logger.log(Level.SEVERE, getClass(), "gettop50parts", ae.getMessage());            
            throw new SystemException("CCT", "gettop50parts", "CCT_BUS_EX01", ae);			
        } finally {
            if (cst != null) {
                try {
                    cst.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }       
        logger.log(Level.INFO, getClass(), "gettop50parts", "Exiting gettop50parts method");
        return ret_code;
    }	

    /**
     * Container's getter for CctTop50PartsVO1.
     * @return CctTop50PartsVO1
     */
    public ViewObjectImpl getCctTop50PartsVO() {
        return (ViewObjectImpl) findViewObject("CctTop50PartsVO");
    }
    public String getOpptyOutcomeinDB(String crfNo) {
        String s = "select OPPTY_OUTCOME from cct_crf where CRF_NO like ?";

        PreparedStatement stmt = getDBTransaction().createPreparedStatement(s, 0);

        ResultSet resultset = null;
        String retopptyOutcome = null;
        try {
            stmt.setString(1, crfNo);
          
            resultset = stmt.executeQuery();
           
            while (resultset.next()) {
                retopptyOutcome = resultset.getString("OPPTY_OUTCOME");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            logger.log(Level.SEVERE, getClass(), "getOpptyOutcomeinDB", ex.getMessage());
            throw new SystemException("CCT", "getOpptyOutcomeinDB", "CCT_SYS_EX10", ex);
        } finally {
            try {
                resultset.close();
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.log(Level.SEVERE, getClass(), "getOpptyOutcomeinDB", e.getMessage());
                throw new SystemException("CCT", "getOpptyOutcomeinDB", "CCT_SYS_EX10", e);
            }
        }
        return retopptyOutcome;
    }

    /**
     * Container's getter for ElectricalQuotePartsVO1.
     * @return ElectricalQuotePartsVO1
     */
    public QuotePartsVOImpl getElectricalQuotePartsVO1() {
        return (QuotePartsVOImpl) findViewObject("ElectricalQuotePartsVO1");
    }

    /**
     * Container's getter for OpportunityEVO1.
     * @return OpportunityEVO1
     */
    public OpportunityEVOImpl getOpportunityEVO1() {
        return (OpportunityEVOImpl) findViewObject("OpportunityEVO1");
    }

	/**
     * Container's getter for CctcrfSalesOwnerROVO1.
     * @return CctcrfSalesOwnerROVO1
     */
    public CctcrfSalesOwnerROVOImpl getCctcrfSalesOwnerROVO1() {
        return (CctcrfSalesOwnerROVOImpl) findViewObject("CctcrfSalesOwnerROVO1");
    }

    /**
     * Container's getter for CctCustomerDueDateHistoryEOView1.
     * @return CctCustomerDueDateHistoryEOView1
     */
    public ViewObjectImpl getCctCustomerDueDateHistoryEOView() {
        return (ViewObjectImpl) findViewObject("CctCustomerDueDateHistoryEOView");
    }
}

