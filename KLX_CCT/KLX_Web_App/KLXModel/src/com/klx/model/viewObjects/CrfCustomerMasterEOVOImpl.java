package com.klx.model.viewObjects;

import oracle.jbo.server.ViewObjectImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Jan 02 15:11:43 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class CrfCustomerMasterEOVOImpl extends ViewObjectImpl {
    /**
     * This is the default constructor (do not remove).
     */
    public CrfCustomerMasterEOVOImpl() {
    }

    /**
     * Returns the variable value for pCustomerNo.
     * @return variable value for pCustomerNo
     */
    public String getpCustomerNo() {
        return (String) ensureVariableManager().getVariableValue("pCustomerNo");
    }

    /**
     * Sets <code>value</code> for variable pCustomerNo.
     * @param value value to bind as pCustomerNo
     */
    public void setpCustomerNo(String value) {
        ensureVariableManager().setVariableValue("pCustomerNo", value);
    }
}

