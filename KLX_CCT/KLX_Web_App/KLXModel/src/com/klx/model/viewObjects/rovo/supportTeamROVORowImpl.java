package com.klx.model.viewObjects.rovo;

import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Apr 02 19:05:10 IST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class supportTeamROVORowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    protected enum AttributesEnum {
        Proposal,
        Pricing,
        PricingTeam,
        AdvancedSourcing,
        Hpp,
        Lighting,
        Electrical,
        Chemical,
        Legal,
        Operations,
        ProgramSolutions,
        Quality,
        ContractsAdministrator;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        protected int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        protected static final int firstIndex() {
            return firstIndex;
        }

        protected static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        protected static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int PROPOSAL = AttributesEnum.Proposal.index();
    public static final int PRICING = AttributesEnum.Pricing.index();
    public static final int PRICINGTEAM = AttributesEnum.PricingTeam.index();
    public static final int ADVANCEDSOURCING = AttributesEnum.AdvancedSourcing.index();
    public static final int HPP = AttributesEnum.Hpp.index();
    public static final int LIGHTING = AttributesEnum.Lighting.index();
    public static final int ELECTRICAL = AttributesEnum.Electrical.index();
    public static final int CHEMICAL = AttributesEnum.Chemical.index();
    public static final int LEGAL = AttributesEnum.Legal.index();
    public static final int OPERATIONS = AttributesEnum.Operations.index();
    public static final int PROGRAMSOLUTIONS = AttributesEnum.ProgramSolutions.index();
    public static final int QUALITY = AttributesEnum.Quality.index();
    public static final int CONTRACTSADMINISTRATOR = AttributesEnum.ContractsAdministrator.index();

    /**
     * This is the default constructor (do not remove).
     */
    public supportTeamROVORowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute Proposal.
     * @return the Proposal
     */
    public String getProposal() {
        return (String) getAttributeInternal(PROPOSAL);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Proposal.
     * @param value value to set the  Proposal
     */
    public void setProposal(String value) {
        setAttributeInternal(PROPOSAL, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Pricing.
     * @return the Pricing
     */
    public String getPricing() {
        return (String) getAttributeInternal(PRICING);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Pricing.
     * @param value value to set the  Pricing
     */
    public void setPricing(String value) {
        setAttributeInternal(PRICING, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PricingTeam.
     * @return the PricingTeam
     */
    public String getPricingTeam() {
        return (String) getAttributeInternal(PRICINGTEAM);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PricingTeam.
     * @param value value to set the  PricingTeam
     */
    public void setPricingTeam(String value) {
        setAttributeInternal(PRICINGTEAM, value);
    }

    /**
     * Gets the attribute value for the calculated attribute AdvancedSourcing.
     * @return the AdvancedSourcing
     */
    public String getAdvancedSourcing() {
        return (String) getAttributeInternal(ADVANCEDSOURCING);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute AdvancedSourcing.
     * @param value value to set the  AdvancedSourcing
     */
    public void setAdvancedSourcing(String value) {
        setAttributeInternal(ADVANCEDSOURCING, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Hpp.
     * @return the Hpp
     */
    public String getHpp() {
        return (String) getAttributeInternal(HPP);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Hpp.
     * @param value value to set the  Hpp
     */
    public void setHpp(String value) {
        setAttributeInternal(HPP, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Lighting.
     * @return the Lighting
     */
    public String getLighting() {
        return (String) getAttributeInternal(LIGHTING);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Lighting.
     * @param value value to set the  Lighting
     */
    public void setLighting(String value) {
        setAttributeInternal(LIGHTING, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Electrical.
     * @return the Electrical
     */
    public String getElectrical() {
        return (String) getAttributeInternal(ELECTRICAL);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Electrical.
     * @param value value to set the  Electrical
     */
    public void setElectrical(String value) {
        setAttributeInternal(ELECTRICAL, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Chemical.
     * @return the Chemical
     */
    public String getChemical() {
        return (String) getAttributeInternal(CHEMICAL);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Chemical.
     * @param value value to set the  Chemical
     */
    public void setChemical(String value) {
        setAttributeInternal(CHEMICAL, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Legal.
     * @return the Legal
     */
    public String getLegal() {
        return (String) getAttributeInternal(LEGAL);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Legal.
     * @param value value to set the  Legal
     */
    public void setLegal(String value) {
        setAttributeInternal(LEGAL, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Operations.
     * @return the Operations
     */
    public String getOperations() {
        return (String) getAttributeInternal(OPERATIONS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Operations.
     * @param value value to set the  Operations
     */
    public void setOperations(String value) {
        setAttributeInternal(OPERATIONS, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ProgramSolutions.
     * @return the ProgramSolutions
     */
    public String getProgramSolutions() {
        return (String) getAttributeInternal(PROGRAMSOLUTIONS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ProgramSolutions.
     * @param value value to set the  ProgramSolutions
     */
    public void setProgramSolutions(String value) {
        setAttributeInternal(PROGRAMSOLUTIONS, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Quality.
     * @return the Quality
     */
    public String getQuality() {
        return (String) getAttributeInternal(QUALITY);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Quality.
     * @param value value to set the  Quality
     */
    public void setQuality(String value) {
        setAttributeInternal(QUALITY, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ContractsAdministrator.
     * @return the ContractsAdministrator
     */
    public String getContractsAdministrator() {
        return (String) getAttributeInternal(CONTRACTSADMINISTRATOR);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ContractsAdministrator.
     * @param value value to set the  ContractsAdministrator
     */
    public void setContractsAdministrator(String value) {
        setAttributeInternal(CONTRACTSADMINISTRATOR, value);
    }
}

