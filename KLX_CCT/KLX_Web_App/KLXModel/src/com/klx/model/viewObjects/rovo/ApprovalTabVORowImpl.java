package com.klx.model.viewObjects.rovo;

import java.math.BigDecimal;

import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Aug 22 09:54:57 EDT 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ApprovalTabVORowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    protected enum AttributesEnum {
        OpportunityId,
        ProjectNumber,
        CustomerName,
        Gt25,
        DealType,
        TotalProductCostLoc,
        TotalProductResaleLoc,
        ProductGrossMargin,
        Rebate,
        ProjectNetMargin,
        BitemLiability,
        PaymentTerms,
        Incoterms,
        TotalPartsRequested,
        TotalPartsQuoted,
        ProductClassAItemCount,
        ProductClassACost,
        ProductClassAResale,
        ProductClassAMargin,
        ProductClassAGrossProfit,
        ProductClassA1aItemCount,
        ProductClassA1aCost,
        ProductClassA1aResale,
        ProductClassA1aMargin,
        ProductClassA1aGrossProfit,
        ProductClassA1bItemCount,
        ProductClassA1bCost,
        ProductClassA1bResale,
        ProductClassA1bMargin,
        ProductClassA1bGrossProfit,
        ProductClassBItemCount,
        ProductClassBCost,
        ProductClassBResale,
        ProductClassBMargin,
        ProductClassBGrossProfit,
        ProductClassCItemCount,
        ProductClassCCost,
        ProductClassCResale,
        ProductClassCMargin,
        ProductClassCGrossProfit,
        ProductClassDItemCount,
        ProductClassDCost,
        ProductClassDResale,
        ProductClassDMargin,
        ProductClassDGrossProfit,
        AgentCommissions,
        LineMinProvision,
        LolProtection,
        CustLiabProtection,
        StockingStrategy,
        Penalties,
        TitleTransfer,
        RightsOfReturn,
        CancelPrivileges,
        BillHold,
        BuyBack,
        Currency,
        Incentives;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        protected int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        protected static final int firstIndex() {
            return firstIndex;
        }

        protected static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        protected static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int OPPORTUNITYID = AttributesEnum.OpportunityId.index();
    public static final int PROJECTNUMBER = AttributesEnum.ProjectNumber.index();
    public static final int CUSTOMERNAME = AttributesEnum.CustomerName.index();
    public static final int GT25 = AttributesEnum.Gt25.index();
    public static final int DEALTYPE = AttributesEnum.DealType.index();
    public static final int TOTALPRODUCTCOSTLOC = AttributesEnum.TotalProductCostLoc.index();
    public static final int TOTALPRODUCTRESALELOC = AttributesEnum.TotalProductResaleLoc.index();
    public static final int PRODUCTGROSSMARGIN = AttributesEnum.ProductGrossMargin.index();
    public static final int REBATE = AttributesEnum.Rebate.index();
    public static final int PROJECTNETMARGIN = AttributesEnum.ProjectNetMargin.index();
    public static final int BITEMLIABILITY = AttributesEnum.BitemLiability.index();
    public static final int PAYMENTTERMS = AttributesEnum.PaymentTerms.index();
    public static final int INCOTERMS = AttributesEnum.Incoterms.index();
    public static final int TOTALPARTSREQUESTED = AttributesEnum.TotalPartsRequested.index();
    public static final int TOTALPARTSQUOTED = AttributesEnum.TotalPartsQuoted.index();
    public static final int PRODUCTCLASSAITEMCOUNT = AttributesEnum.ProductClassAItemCount.index();
    public static final int PRODUCTCLASSACOST = AttributesEnum.ProductClassACost.index();
    public static final int PRODUCTCLASSARESALE = AttributesEnum.ProductClassAResale.index();
    public static final int PRODUCTCLASSAMARGIN = AttributesEnum.ProductClassAMargin.index();
    public static final int PRODUCTCLASSAGROSSPROFIT = AttributesEnum.ProductClassAGrossProfit.index();
    public static final int PRODUCTCLASSA1AITEMCOUNT = AttributesEnum.ProductClassA1aItemCount.index();
    public static final int PRODUCTCLASSA1ACOST = AttributesEnum.ProductClassA1aCost.index();
    public static final int PRODUCTCLASSA1ARESALE = AttributesEnum.ProductClassA1aResale.index();
    public static final int PRODUCTCLASSA1AMARGIN = AttributesEnum.ProductClassA1aMargin.index();
    public static final int PRODUCTCLASSA1AGROSSPROFIT = AttributesEnum.ProductClassA1aGrossProfit.index();
    public static final int PRODUCTCLASSA1BITEMCOUNT = AttributesEnum.ProductClassA1bItemCount.index();
    public static final int PRODUCTCLASSA1BCOST = AttributesEnum.ProductClassA1bCost.index();
    public static final int PRODUCTCLASSA1BRESALE = AttributesEnum.ProductClassA1bResale.index();
    public static final int PRODUCTCLASSA1BMARGIN = AttributesEnum.ProductClassA1bMargin.index();
    public static final int PRODUCTCLASSA1BGROSSPROFIT = AttributesEnum.ProductClassA1bGrossProfit.index();
    public static final int PRODUCTCLASSBITEMCOUNT = AttributesEnum.ProductClassBItemCount.index();
    public static final int PRODUCTCLASSBCOST = AttributesEnum.ProductClassBCost.index();
    public static final int PRODUCTCLASSBRESALE = AttributesEnum.ProductClassBResale.index();
    public static final int PRODUCTCLASSBMARGIN = AttributesEnum.ProductClassBMargin.index();
    public static final int PRODUCTCLASSBGROSSPROFIT = AttributesEnum.ProductClassBGrossProfit.index();
    public static final int PRODUCTCLASSCITEMCOUNT = AttributesEnum.ProductClassCItemCount.index();
    public static final int PRODUCTCLASSCCOST = AttributesEnum.ProductClassCCost.index();
    public static final int PRODUCTCLASSCRESALE = AttributesEnum.ProductClassCResale.index();
    public static final int PRODUCTCLASSCMARGIN = AttributesEnum.ProductClassCMargin.index();
    public static final int PRODUCTCLASSCGROSSPROFIT = AttributesEnum.ProductClassCGrossProfit.index();
    public static final int PRODUCTCLASSDITEMCOUNT = AttributesEnum.ProductClassDItemCount.index();
    public static final int PRODUCTCLASSDCOST = AttributesEnum.ProductClassDCost.index();
    public static final int PRODUCTCLASSDRESALE = AttributesEnum.ProductClassDResale.index();
    public static final int PRODUCTCLASSDMARGIN = AttributesEnum.ProductClassDMargin.index();
    public static final int PRODUCTCLASSDGROSSPROFIT = AttributesEnum.ProductClassDGrossProfit.index();
    public static final int AGENTCOMMISSIONS = AttributesEnum.AgentCommissions.index();
    public static final int LINEMINPROVISION = AttributesEnum.LineMinProvision.index();
    public static final int LOLPROTECTION = AttributesEnum.LolProtection.index();
    public static final int CUSTLIABPROTECTION = AttributesEnum.CustLiabProtection.index();
    public static final int STOCKINGSTRATEGY = AttributesEnum.StockingStrategy.index();
    public static final int PENALTIES = AttributesEnum.Penalties.index();
    public static final int TITLETRANSFER = AttributesEnum.TitleTransfer.index();
    public static final int RIGHTSOFRETURN = AttributesEnum.RightsOfReturn.index();
    public static final int CANCELPRIVILEGES = AttributesEnum.CancelPrivileges.index();
    public static final int BILLHOLD = AttributesEnum.BillHold.index();
    public static final int BUYBACK = AttributesEnum.BuyBack.index();
    public static final int CURRENCY = AttributesEnum.Currency.index();
    public static final int INCENTIVES = AttributesEnum.Incentives.index();

    /**
     * This is the default constructor (do not remove).
     */
    public ApprovalTabVORowImpl() {
    }
}

