package com.klx.model.viewObjects.rovo;

import java.math.BigDecimal;

import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Mar 27 16:24:47 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class CctFinalTop50ProdQueueViewRowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    protected enum AttributesEnum {
        QuoteRevisionId,
        QueueName,
        Poc,
        CountPnWithCost,
        TotalCost,
        TotalResale,
        PartsMargin,
        PctOfTotalCost;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        protected int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        protected static final int firstIndex() {
            return firstIndex;
        }

        protected static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        protected static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int QUOTEREVISIONID = AttributesEnum.QuoteRevisionId.index();
    public static final int QUEUENAME = AttributesEnum.QueueName.index();
    public static final int POC = AttributesEnum.Poc.index();
    public static final int COUNTPNWITHCOST = AttributesEnum.CountPnWithCost.index();
    public static final int TOTALCOST = AttributesEnum.TotalCost.index();
    public static final int TOTALRESALE = AttributesEnum.TotalResale.index();
    public static final int PARTSMARGIN = AttributesEnum.PartsMargin.index();
    public static final int PCTOFTOTALCOST = AttributesEnum.PctOfTotalCost.index();

    /**
     * This is the default constructor (do not remove).
     */
    public CctFinalTop50ProdQueueViewRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute QuoteRevisionId.
     * @return the QuoteRevisionId
     */
    public BigDecimal getQuoteRevisionId() {
        return (BigDecimal) getAttributeInternal(QUOTEREVISIONID);
    }

    /**
     * Gets the attribute value for the calculated attribute QueueName.
     * @return the QueueName
     */
    public String getQueueName() {
        return (String) getAttributeInternal(QUEUENAME);
    }

    /**
     * Gets the attribute value for the calculated attribute Poc.
     * @return the Poc
     */
    public String getPoc() {
        return (String) getAttributeInternal(POC);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Poc.
     * @param value value to set the  Poc
     */
    public void setPoc(String value) {
        setAttributeInternal(POC, value);
    }

    /**
     * Gets the attribute value for the calculated attribute CountPnWithCost.
     * @return the CountPnWithCost
     */
    public BigDecimal getCountPnWithCost() {
        return (BigDecimal) getAttributeInternal(COUNTPNWITHCOST);
    }

    /**
     * Gets the attribute value for the calculated attribute TotalCost.
     * @return the TotalCost
     */
    public BigDecimal getTotalCost() {
        return (BigDecimal) getAttributeInternal(TOTALCOST);
    }

    /**
     * Gets the attribute value for the calculated attribute TotalResale.
     * @return the TotalResale
     */
    public BigDecimal getTotalResale() {
        return (BigDecimal) getAttributeInternal(TOTALRESALE);
    }

    /**
     * Gets the attribute value for the calculated attribute PartsMargin.
     * @return the PartsMargin
     */
    public BigDecimal getPartsMargin() {
        return (BigDecimal) getAttributeInternal(PARTSMARGIN);
    }

    /**
     * Gets the attribute value for the calculated attribute PctOfTotalCost.
     * @return the PctOfTotalCost
     */
    public BigDecimal getPctOfTotalCost() {
        return (BigDecimal) getAttributeInternal(PCTOFTOTALCOST);
    }
}

