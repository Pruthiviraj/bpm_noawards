package com.klx.model.viewObjects.rovo;

import java.math.BigDecimal;

import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Oct 16 03:50:59 EDT 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class CostCompareLinesVORowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    protected enum AttributesEnum {
        OpportunityId,
        QuotedPartNumber,
        PreviousExtCost,
        NewExtCost,
        Delta,
        PercDifference,
        PercCost,
        WorkQueue,
        ChangeType;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        protected int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        protected static final int firstIndex() {
            return firstIndex;
        }

        protected static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        protected static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }

    public static final int OPPORTUNITYID = AttributesEnum.OpportunityId.index();
    public static final int QUOTEDPARTNUMBER = AttributesEnum.QuotedPartNumber.index();
    public static final int PREVIOUSEXTCOST = AttributesEnum.PreviousExtCost.index();
    public static final int NEWEXTCOST = AttributesEnum.NewExtCost.index();
    public static final int DELTA = AttributesEnum.Delta.index();
    public static final int PERCDIFFERENCE = AttributesEnum.PercDifference.index();
    public static final int PERCCOST = AttributesEnum.PercCost.index();
    public static final int WORKQUEUE = AttributesEnum.WorkQueue.index();
    public static final int CHANGETYPE = AttributesEnum.ChangeType.index();

    /**
     * This is the default constructor (do not remove).
     */
    public CostCompareLinesVORowImpl() {
    }
}

