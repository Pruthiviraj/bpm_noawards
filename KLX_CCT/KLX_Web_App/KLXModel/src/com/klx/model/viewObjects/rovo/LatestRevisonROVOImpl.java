package com.klx.model.viewObjects.rovo;

import oracle.jbo.server.ViewObjectImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Jul 20 05:55:47 EDT 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class LatestRevisonROVOImpl extends ViewObjectImpl {
    /**
     * This is the default constructor (do not remove).
     */
    public LatestRevisonROVOImpl() {
    }

    /**
     * Returns the bind variable value for pOpportunityNumber.
     * @return bind variable value for pOpportunityNumber
     */
    public String getpOpportunityNumber() {
        return (String) getNamedWhereClauseParam("pOpportunityNumber");
    }

    /**
     * Sets <code>value</code> for bind variable pOpportunityNumber.
     * @param value value to bind as pOpportunityNumber
     */
    public void setpOpportunityNumber(String value) {
        setNamedWhereClauseParam("pOpportunityNumber", value);
    }
    
}

