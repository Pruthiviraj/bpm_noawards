package com.klx.model.viewObjects;

import java.math.BigDecimal;

import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Dec 20 16:24:20 IST 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class CctSalesHierarchyVORowImpl extends ViewRowImpl {

    public static final int ENTITY_CCTSALESHIERARCHYEO = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    protected enum AttributesEnum {
        Description,
        SalesDirector,
        SalesManager,
        SalesTeamRegion,
        SalesVp,
        TeamId,
        TeamNo;
        private static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        protected int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        protected static final int firstIndex() {
            return firstIndex;
        }

        protected static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        protected static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }

    public static final int DESCRIPTION = AttributesEnum.Description.index();
    public static final int SALESDIRECTOR = AttributesEnum.SalesDirector.index();
    public static final int SALESMANAGER = AttributesEnum.SalesManager.index();
    public static final int SALESTEAMREGION = AttributesEnum.SalesTeamRegion.index();
    public static final int SALESVP = AttributesEnum.SalesVp.index();
    public static final int TEAMID = AttributesEnum.TeamId.index();
    public static final int TEAMNO = AttributesEnum.TeamNo.index();

    /**
     * This is the default constructor (do not remove).
     */
    public CctSalesHierarchyVORowImpl() {
    }

    /**
     * Gets CctSalesHierarchyEO entity object.
     * @return the CctSalesHierarchyEO
     */
    public EntityImpl getCctSalesHierarchyEO() {
        return (EntityImpl) getEntity(ENTITY_CCTSALESHIERARCHYEO);
    }

    /**
     * Gets the attribute value for DESCRIPTION using the alias name Description.
     * @return the DESCRIPTION
     */
    public String getDescription() {
        return (String) getAttributeInternal(DESCRIPTION);
    }

    /**
     * Sets <code>value</code> as attribute value for DESCRIPTION using the alias name Description.
     * @param value value to set the DESCRIPTION
     */
    public void setDescription(String value) {
        setAttributeInternal(DESCRIPTION, value);
    }

    /**
     * Gets the attribute value for SALES_DIRECTOR using the alias name SalesDirector.
     * @return the SALES_DIRECTOR
     */
    public String getSalesDirector() {
        return (String) getAttributeInternal(SALESDIRECTOR);
    }

    /**
     * Sets <code>value</code> as attribute value for SALES_DIRECTOR using the alias name SalesDirector.
     * @param value value to set the SALES_DIRECTOR
     */
    public void setSalesDirector(String value) {
        setAttributeInternal(SALESDIRECTOR, value);
    }

    /**
     * Gets the attribute value for SALES_MANAGER using the alias name SalesManager.
     * @return the SALES_MANAGER
     */
    public String getSalesManager() {
        return (String) getAttributeInternal(SALESMANAGER);
    }

    /**
     * Sets <code>value</code> as attribute value for SALES_MANAGER using the alias name SalesManager.
     * @param value value to set the SALES_MANAGER
     */
    public void setSalesManager(String value) {
        setAttributeInternal(SALESMANAGER, value);
    }

    /**
     * Gets the attribute value for SALES_TEAM_REGION using the alias name SalesTeamRegion.
     * @return the SALES_TEAM_REGION
     */
    public String getSalesTeamRegion() {
        return (String) getAttributeInternal(SALESTEAMREGION);
    }

    /**
     * Sets <code>value</code> as attribute value for SALES_TEAM_REGION using the alias name SalesTeamRegion.
     * @param value value to set the SALES_TEAM_REGION
     */
    public void setSalesTeamRegion(String value) {
        setAttributeInternal(SALESTEAMREGION, value);
    }

    /**
     * Gets the attribute value for SALES_VP using the alias name SalesVp.
     * @return the SALES_VP
     */
    public String getSalesVp() {
        return (String) getAttributeInternal(SALESVP);
    }

    /**
     * Sets <code>value</code> as attribute value for SALES_VP using the alias name SalesVp.
     * @param value value to set the SALES_VP
     */
    public void setSalesVp(String value) {
        setAttributeInternal(SALESVP, value);
    }

    /**
     * Gets the attribute value for TEAM_ID using the alias name TeamId.
     * @return the TEAM_ID
     */
    public BigDecimal getTeamId() {
        return (BigDecimal) getAttributeInternal(TEAMID);
    }

    /**
     * Sets <code>value</code> as attribute value for TEAM_ID using the alias name TeamId.
     * @param value value to set the TEAM_ID
     */
    public void setTeamId(BigDecimal value) {
        setAttributeInternal(TEAMID, value);
    }

    /**
     * Gets the attribute value for TEAM_NO using the alias name TeamNo.
     * @return the TEAM_NO
     */
    public String getTeamNo() {
        return (String) getAttributeInternal(TEAMNO);
    }

    /**
     * Sets <code>value</code> as attribute value for TEAM_NO using the alias name TeamNo.
     * @param value value to set the TEAM_NO
     */
    public void setTeamNo(String value) {
        setAttributeInternal(TEAMNO, value);
    }
}

