package com.klx.model.constants;

import java.math.BigDecimal;


public class ModelConstants {
    public ModelConstants() {
        super();
    }
    public static final String PROGRAM_COST_HEADER_CRITERIA = "CctProgramCostHeaderEOVOCriteria";
    public static final String SALES_METHOD = "SalesMethod";
    public static final String PRICING_MANAGER = "PricingManager";
    public static final String SALES_REPRESENTATIVE = "SalesRep";
    public static final String PROPOSAL_MANAGER = "ProposalManager";
    public static final String CUSTOMER_NAME = "CustomerName";
    public static final String CUSTOMER_DUE_DATE = "CustomerDueDate";
    public static final String CUSTOMER_NUMBER = "CustomerNumber";
    public static final String DEAL_TYPE = "DealType";
    public static final String OSC_OPPORTUNITY_ID = "OscOpportunityId";
    public static final String DEAL_STRUCTURE = "DealStructure";
    public static final String BIND_TASK_NO = "bind_taskNo";
    public static final String TASK = "TASK";
    public static final String BIND_LEVEL = "bind_level";
    public static final String BIND_OPPORTUNITY_NO = "bind_OppNo";
    public static final String OPPORTUNITY = "OPPORTUNITY";
    public static final String TEAM_NUMBER = "TeamNumber";
    public static final String PROJECT_TYPE = "ProjectType";
    public static final String CONTRACT_TYPE = "ContractType";
    public static final String CUSTOMER_CRF_NO = "CustomerNoCrf";
    public static final String BIND_CATEGORY = "pCategory";
    public static final String BIND_COST_HEADER = "bind_costHeaderId";
    public static final String UPLOAD_STATUS = "uploadStatus";
    public static final String UPLOAD_TYPE = "uploadType";
    public static final String AWARDS_REVIEW_UPLOAD = "AWARDS_REVIEW_UPLOAD";
    public static final String AWARDS_FILE_UPLOAD = "AWARDS_FILE_UPLOAD";
    public static final String SOURCE_FIELD = "SourceField";
    public static final String DESTINATION_FIELD = "DestinationField";
    public static final String SUBMITOSC_STATUS_SUCCESS = "success";
    public static final String SUBMITOSC_VALIDATE_STATUS = "NotValid";
    public static final String SUBMITOSC_STATUS_FAIL = "unsuccessful";
    public static final String SUBMITOSC_STMT_BUTTON = " call  cct_create_opportunity.submit_to_sales (?)";
    public static final String SUBMITOSC_STMT = " call  cct_create_opportunity.update_formula_fields (?)";
    public static final String APPROVAL_STMT = " call  CCT_APPROVAL_PROCESS.create_snapshot (?,?)";
    public static final String STATUS_SUCCESS = "success";
    public static final String STATUS_FAILURE = "Failure";
    //UploadError Attributes
    public static final String UEA_SI_NO = "SINO";
    public static final String UEA_COLUMN_HEADER = "COLUMN_HEADER";
    public static final String UEA_DESCRIPTION = "DESCRIPTION";
    public static final String UEA_CELL_VALUE = "CELL_VALUE";
    public static final String PART_MOVED_ON = "Part moved on ";
    public static final BigDecimal[] categoryID = {
        new BigDecimal(1), new BigDecimal(2), new BigDecimal(3), new BigDecimal(4), new BigDecimal(5)
    };
    public static final BigDecimal[] queueID = {
        new BigDecimal(1), new BigDecimal(2), new BigDecimal(3), new BigDecimal(4), new BigDecimal(5),
        new BigDecimal(6), new BigDecimal(7), new BigDecimal(8), new BigDecimal(9), new BigDecimal(10)
    };
    public static final String[] queueTemplateHeaders = {
        "Work Queue", "L/N", "Line Item", "Customer Part Number", "Customer PN", "Part Number Requested",
        "Cost  Used for  Quote", "Resale", "Calc ABC", "L/I", "Quoted  Part Number", "Annual  Qty", "UOM",
        "Total  Qty Length  Contract", "Group", "Margin_Validation", "Specialty Products"
    };
    public static final String[] quotePartsVOAttributes = {
        "QueueId", "FirstPlanPurday", "LtaExpDate", "ItemTag", "Status", "QueueAssignmentDate", "MarginValidation"
    };
    public static final String[] queueIds = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "10"};
    public static final String templateMapping = "BEACON_FILE_UPDATE";

    //roleid's
    public static final String CCT_COMMODITY = "CCT_COMMODITY";
    public static final String CCT_ACCOUNTING_DIRECTOR = "CCT_ACCOUNTING_DIRECTOR";
    public static final String CCT_AWARD_MANAGER = "CCT_AWARD_MANAGER";
    public static final String CCT_FINANCE_VP = "CCT_FINANCE_VP";
    public static final String CCT_GROUP_VP_AND_GM = "CCT_GROUP_VP_AND_GM";
    public static final String CCT_OPERATIONS_VP = "CCT_OPERATIONS_VP";
    public static final String CCT_SUPPLY_CHAIN_VP = "CCT_SUPPLY_CHAIN_VP";
    
    public static final String CCT_SR_CONTRACTS_DIRECTOR = "CCT_SR_CONTRACTS_DIRECTOR";
    public static final String CCT_CORPORATE_CFO = "CCT_CORPORATE_CFO";
    public static final String CCT_SALES_VP = "CCT_SALES_VP";
    public static final String CCT_SALES_DIRECTOR = "CCT_SALES_DIRECTOR";
    public static final String CCT_STRATEGIC_PRICING = "CCT_STRATEGIC_PRICING";
    public static final String CCT_CSM = "CCT_CSM";
    public static final String CCT_LIGHTING_PRODUCT_LINE = "CCT_LIGHTING_PRODUCT_LINE";
    public static final String CCT_HONEYWELL_PRODUCT_LINE = "CCT_HONEYWELL_PRODUCT_LINE";
    public static final String CCT_ADVANCED_SOURCING = "CCT_ADVANCED_SOURCING";
    public static final String CCT_TECHNICAL_SUPPORT_TEAM = "CCT_TECHNICAL_SUPPORT_TEAM";
    public static final String CCT_PROPOSAL_MANAGER = "CCT_PROPOSAL_MANAGER";
    public static final String CCT_PROGRAM_SOLUTIONS = "CCT_PROGRAM_SOLUTIONS";
    public static final String CCT_CONTRACTS = "CCT_CONTRACTS";
    public static final String CCT_UNKNOWN = "CCT_UNKNOWN";
    public static final String CCT_SALES = "CCT_SALES";
    public static final String CCT_APPROVERS = "CCT_APPROVERS";
    public static final String CCT_KITTING = "CCT_KITTING";
    public static final String CCT_OPERATIONS = "CCT_OPERATIONS";
    public static final String CCT_PROGRAM_SOLUTION_SPECIALIST = "CCT_PROGRAM_SOLUTION_SPECIALIST";
    public static final String CCT_LEGAL = "CCT_LEGAL";
    public static final String CCT_QUALITY = "CCT_QUALITY";

    //Query statements for create opportunity .
    public static final String CCT_CRF_SEQ = "select CCT_CRF_SEQ.nextval from dual";
    public static final String CCT_CRF_CUSTOMER_REQUEST_SEQ = "select CCT_CRF_CUSTOMER_REQUEST_SEQ.nextval from dual";
    public static final String CCT_CRF_CUSTOMER_INFO_SEQ = "select CCT_CRF_CUSTOMER_INFO_SEQ.nextval from dual";
    public static final String CCT_CRF_KLX_OFFER_SEQ = "select CCT_CRF_KLX_OFFER_SEQ.nextval from dual";
    public static final String CCT_CRF_KLX_AWARD_SEQ = "select CCT_CRF_KLX_AWARD_SEQ.nextval from dual";
     public static final String CCT_CRF_REVISION_SEQ = "select CCT_CRF_REVISION_SEQ.nextval from dual";

}
