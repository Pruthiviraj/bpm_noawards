xquery version "1.0" encoding "utf-8";
(:: pragma  parameter="$anyType1" type="anyType" ::)
(:: pragma  type="anyType" ::)


declare namespace xf = "http://www.klx.com/xquery/bpm/task/wrapper/V1.0";
declare namespace ts="http://xmlns.oracle.com/bpel/workflow/task";


declare function xf:fetchTaskIds($Output as element(*))
    as element(*) {
       <TaskDetails>
		{
		for $task in $Output/ts:task
                return	       
	          <TaskId>{$task/ts:systemAttributes/ts:taskId/text()}</TaskId>   
                }
          </TaskDetails>    
 };    

declare variable $Output as element(*) external;

xf:fetchTaskIds($Output)