xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://xmlns.oracle.com/schema/common/services";
(:: import schema at "../../../../CommonDataModel/Canonicals/UtilityArtifacts/xsd/ResubmissionProcessor.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/DB_OUT_GetResubmissionPayloads";
(:: import schema at "../../business/xsd/DB_OUT_GetResubmissionPayloads_sp.xsd" ::)

declare variable $ResubmitDetails as element() (:: schema-element(ns1:ResubmitDetails) ::) external;

declare function local:func($ResubmitDetails as element() (:: schema-element(ns1:ResubmitDetails) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
       <ns2:I_RESUBMISSION_IDS>
          {
              for $MessageFlowId in $ResubmitDetails/ns1:MessageFlowIds/ns1:MessageFlowId
              return 
              <ns2:I_RESUBMISSION_IDS_ITEM>{$MessageFlowId/text()}</ns2:I_RESUBMISSION_IDS_ITEM>
          }
      </ns2:I_RESUBMISSION_IDS>
    </ns2:InputParameters>
};

local:func($ResubmitDetails)