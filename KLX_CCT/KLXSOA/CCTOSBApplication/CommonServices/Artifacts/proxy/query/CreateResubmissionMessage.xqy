xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://xmlns.oracle.com/schema/common/services";
(:: import schema at "../../../../CommonDataModel/Canonicals/UtilityArtifacts/xsd/ResubmissionMessage.xsd" ::)

declare variable $timestamp as xs:string external;
declare variable $interfaceId as xs:string external;
declare variable $interfaceName as xs:string external;
declare variable $subProcessName as xs:string external;
declare variable $messageFlowId as xs:string external;
declare variable $operation as xs:string external;
declare variable $payload as xs:string external;

declare function local:func($timestamp as xs:string, 
                            $interfaceId as xs:string, 
                            $interfaceName as xs:string, 
                            $subProcessName as xs:string, 
                            $messageFlowId as xs:string, 
                            $operation as xs:string, 
                            $payload as xs:string) 
                            as element() (:: schema-element(ns1:ResubmissionMessage) ::) {
    <ns1:ResubmissionMessage>
        <ns1:MessageHeader>
            <ns1:TimeStamp>{$timestamp}</ns1:TimeStamp>
            <ns1:InterfaceId>{$interfaceId}</ns1:InterfaceId>
            <ns1:InterfaceName>{$interfaceName}</ns1:InterfaceName>
            <ns1:SubProcessName>{$subProcessName}</ns1:SubProcessName>
            <ns1:MessageFlowId>{$messageFlowId}</ns1:MessageFlowId>
            <ns1:Operation>{$operation}</ns1:Operation>
        </ns1:MessageHeader>
        <ns1:MessageBody>
            <ns1:Payload>{$payload}</ns1:Payload>
        </ns1:MessageBody>
    </ns1:ResubmissionMessage>
};

local:func($timestamp, $interfaceId, $interfaceName, $subProcessName, $messageFlowId, $operation, $payload)