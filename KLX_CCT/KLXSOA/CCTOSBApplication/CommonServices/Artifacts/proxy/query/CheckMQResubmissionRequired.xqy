xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare variable $errorMessage as xs:string external;

declare function local:func($errorMessage as xs:string) as xs:boolean {
    let $errorCode := fn:substring-before(fn:substring-after($errorMessage,"["),"]"),
    $resubmit := dvmtr:lookup('CommonServices/Artifacts/proxy/dvm/ResubmitMQErrorCodes', 'ErrorCode', $errorCode, 'Resubmit', 'N')
    return
    if ($resubmit = 'Y') then
        true()
    else
        if(fn:contains($errorMessage,"General runtime error:"))
        then true()
        else false()
};

local:func($errorMessage)