xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://xmlns.oracle.com/schema/common/services";
(:: import schema at "../../../../CommonDataModel/Canonicals/UtilityArtifacts/xsd/ResubmissionProcessor.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/DB_OUT_UpdateResubmissionStatus";
(:: import schema at "../../business/xsd/DB_OUT_UpdateResubmissionStatus_sp.xsd" ::)

declare variable $ResubmitTargetService as element() (:: schema-element(ns1:ResubmitTargetService) ::) external;

declare function local:func($ResubmitTargetService as element() (:: schema-element(ns1:ResubmitTargetService) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:I_RESUBMISSION_IDS>
            <ns2:I_RESUBMISSION_IDS_ITEM>{$ResubmitTargetService/ns1:FMWResubmissionId/text()}</ns2:I_RESUBMISSION_IDS_ITEM>
        </ns2:I_RESUBMISSION_IDS>
    </ns2:InputParameters>
};

local:func($ResubmitTargetService)