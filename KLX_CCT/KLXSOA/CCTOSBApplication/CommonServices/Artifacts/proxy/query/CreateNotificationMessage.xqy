xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://xmlns.oracle.com/schema/common/services";
(:: import schema at "../../../../CommonDataModel/Canonicals/UtilityArtifacts/xsd/CommonMessage.xsd" ::)


declare variable $NotificationPayload as xs:string external;
declare variable $To as xs:string external;
declare variable $ReplyTo as xs:string external;
declare variable $Cc as xs:string external;
declare variable $Bcc as xs:string external;
declare variable $Subject as xs:string external;
declare variable $ArrayofAttachment as element() (:: element(*, ns1:ArrayOfAttachmentType) ::) external;

declare function local:func($NotificationPayload as xs:string, $To as xs:string, $ReplyTo as xs:string, $Cc as xs:string, $Bcc as xs:string, $Subject as xs:string, $ArrayofAttachment as element() (:: element(*, ns1:ArrayOfAttachmentType) ::)) as element() (:: schema-element(ns1:CommonMessage) ::) {
    <ns1:CommonMessage>
        <ns1:MessageHeader>
            <ns1:ServiceInvoked>Notification</ns1:ServiceInvoked>
            <ns1:NotificationDetails>
                <ns1:To>{$To}</ns1:To>
                { if($ReplyTo != '')
                  then (<ns1:ReplyTo>{$ReplyTo}</ns1:ReplyTo>)
                  else ()
                }
                { if($Cc != '')
                  then (<ns1:Cc>{$Cc}</ns1:Cc>)
                  else ()
                }
                { if($Bcc != '')
                  then (<ns1:Bcc>{$Bcc}</ns1:Bcc>)
                  else ()
                }
                <ns1:Subject>{$Subject}</ns1:Subject>
                <ns1:ArrayOfAttachment>{$ArrayofAttachment/*}</ns1:ArrayOfAttachment>
            </ns1:NotificationDetails>
        </ns1:MessageHeader>
        <ns1:MessageBody>
                <ns1:NotificationPayload>{$NotificationPayload}</ns1:NotificationPayload>
        </ns1:MessageBody>
    </ns1:CommonMessage>
};

local:func($NotificationPayload, $To, $ReplyTo, $Cc, $Bcc, $Subject, $ArrayofAttachment)