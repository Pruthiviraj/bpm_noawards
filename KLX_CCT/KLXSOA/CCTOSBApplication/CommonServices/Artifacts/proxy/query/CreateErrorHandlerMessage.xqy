xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://xmlns.oracle.com/schema/common/services";
(:: import schema at "../../../../CommonDataModel/Canonicals/UtilityArtifacts/xsd/CommonMessage.xsd" ::)

declare variable $Source as xs:string external;
declare variable $Target as xs:string external;
declare variable $Timestamp as xs:string external;
declare variable $InterfaceId as xs:string external;
declare variable $InterfaceName as xs:string external;
declare variable $SubProcessName as xs:string external;
declare variable $Operation as xs:string external;
declare variable $MessageFlowId as xs:string external;
declare variable $CompositeInstanceId as xs:string external;
declare variable $Severity as xs:string external;
declare variable $Node as xs:string external;
declare variable $Location as xs:string external;
declare variable $Direction as xs:string external;
declare variable $BusinessId as xs:string external;
declare variable $ErrorCode as xs:string external;
declare variable $ErrorMessage as xs:string external;
declare variable $ErrorStackTrace as xs:string external;
declare variable $InputRequest as xs:string external;

declare function local:func($Source as xs:string, $Target as xs:string, $Timestamp as xs:string, $InterfaceId as xs:string, $InterfaceName as xs:string, $SubProcessName as xs:string,$Operation as xs:string,$MessageFlowId as xs:string,$CompositeInstanceId as xs:string,$Severity as xs:string,$Node as xs:string,$Location as xs:string,$Direction as xs:string,$BusinessId as xs:string,$ErrorCode as xs:string, $ErrorMessage as xs:string, $ErrorStackTrace as xs:string, $InputRequest as xs:string) as element() (:: schema-element(ns1:CommonMessage) ::) {
    <ns1:CommonMessage>
       <ns1:MessageHeader>
          <ns1:Source>{$Source}</ns1:Source>
          <ns1:Target>{$Target}</ns1:Target>
          <ns1:TimeStamp>{$Timestamp}</ns1:TimeStamp>
          <ns1:ServiceInvoked>ErrorHandler</ns1:ServiceInvoked>
          <ns1:InterfaceId>{$InterfaceId}</ns1:InterfaceId>
          <ns1:InterfaceName>{$InterfaceName}</ns1:InterfaceName>
          <ns1:SubProcessName>{$SubProcessName}</ns1:SubProcessName>
          <ns1:Operation>{$Operation}</ns1:Operation>
          <ns1:MessageFlowId>{$MessageFlowId}</ns1:MessageFlowId>
          { if($CompositeInstanceId != '')
            then (<ns1:CompositeInstanceId>{$CompositeInstanceId}</ns1:CompositeInstanceId>)
            else (<ns1:CompositeInstanceId>N/A</ns1:CompositeInstanceId>)
          }
          <ns1:ErrorDetails>
              <ns1:ErrorCode>{$ErrorCode}</ns1:ErrorCode>
              <ns1:ErrorMessage>{$ErrorMessage}</ns1:ErrorMessage>
              { if ($ErrorStackTrace != '')
                then (<ns1:ErrorStackTrace>{$ErrorStackTrace}</ns1:ErrorStackTrace>)
                else (<ns1:ErrorStackTrace>N/A</ns1:ErrorStackTrace>)
              }
              { if ($InputRequest != '')
                then (<ns1:InputRequest>{$InputRequest}</ns1:InputRequest>)
                else (<ns1:InputRequest>N/A</ns1:InputRequest>)
              }
          </ns1:ErrorDetails>
          <ns1:LoggingDetails>
              <ns1:Severity>{$Severity}</ns1:Severity>
              <ns1:Node>{$Node}</ns1:Node>
              <ns1:Location>{$Location}</ns1:Location>
              <ns1:Direction>{$Direction}</ns1:Direction>
              <ns1:BusinessId>{$BusinessId}</ns1:BusinessId>
          </ns1:LoggingDetails>
          <ns1:NotificationDetails>
          </ns1:NotificationDetails>
       </ns1:MessageHeader>
       <ns1:MessageBody>
       </ns1:MessageBody>
    </ns1:CommonMessage>
};

local:func($Source,$Target,$Timestamp,$InterfaceId,$InterfaceName,$SubProcessName,$Operation,$MessageFlowId,$CompositeInstanceId,$Severity,$Node,$Location,$Direction,$BusinessId,$ErrorCode,$ErrorMessage,$ErrorStackTrace,$InputRequest)