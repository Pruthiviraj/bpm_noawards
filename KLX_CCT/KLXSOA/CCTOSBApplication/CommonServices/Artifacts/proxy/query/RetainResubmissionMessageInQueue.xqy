xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare variable $errorMessage as xs:string external;
declare variable $errorPatternMetadata as element(*) external;

declare function local:func($errorMessage as xs:string,
                            $errorPatternMetadata as element(*)) 
                            as xs:boolean {
    let $fault := $errorPatternMetadata/Fault[Retain = 'Y']
    return
    if (fn:count($fault) > 0) 
    then
        let $pattern := $fault/Pattern[fn:contains($errorMessage, .)]
        return
          if(fn:count($pattern) > 0)
          then true()
          else false()
    else
        false()
};

local:func($errorMessage, $errorPatternMetadata)