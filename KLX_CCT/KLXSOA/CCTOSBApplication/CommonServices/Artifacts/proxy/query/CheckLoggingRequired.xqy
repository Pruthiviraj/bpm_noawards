xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare variable $RequestLogLevel as xs:string external;
declare variable $FilterLogLevel as xs:string external;

declare function local:func($RequestLogLevel as xs:string, 
                            $FilterLogLevel as xs:string) 
                            as xs:boolean {
    local:GetLevelValue($RequestLogLevel) >= local:GetLevelValue($FilterLogLevel)
};

declare function local:GetLevelValue($logLevel as xs:string)
    as xs:int {
        
        let $logFilter := fn:upper-case($logLevel)
        return
        if($logFilter = "INFO")
        then xs:int('3')
        else if($logFilter = "ERROR")
        then xs:int('5')
        else if($logFilter = "WARN")
        then xs:int('4')
        else if($logFilter = "OFF")
        then xs:int('7')
        else if($logFilter = "TRACE")
        then xs:int('1')
        else if($logFilter = "DEBUG")
        then xs:int('2')
        else if($logFilter = "FATAL")
        then xs:int('6')
        else xs:int('0')      
        
};

local:func($RequestLogLevel, $FilterLogLevel)