xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://xmlns.oracle.com/schema/common/services";
(:: import schema at "../../../../CommonDataModel/Canonicals/UtilityArtifacts/xsd/CommonMessage.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/top/DB_OUT_FMWAuditing";
(:: import schema at "../../business/xsd/DB_OUT_FMWAuditing_table.xsd" ::)

declare variable $CommonMessage as element() (:: schema-element(ns1:CommonMessage) ::) external;

declare function local:func($CommonMessage as element() (:: schema-element(ns1:CommonMessage) ::)) as element() (:: schema-element(ns2:FmwAuditingCollection) ::) {
    <ns2:FmwAuditingCollection>
    {   let $messageHeader := $CommonMessage/ns1:MessageHeader
        return
        <ns2:FmwAuditing>
            <ns2:source>{$messageHeader/ns1:Source/text()}</ns2:source>
            <ns2:target>{$messageHeader/ns1:Target/text()}</ns2:target>
            <ns2:timestamp>{$messageHeader/ns1:TimeStamp/text()}</ns2:timestamp>
            <ns2:interfaceId>{$messageHeader/ns1:InterfaceId/text()}</ns2:interfaceId>
            <ns2:interfaceName>{$messageHeader/ns1:InterfaceName/text()}</ns2:interfaceName>
            <ns2:subProcessName>{$messageHeader/ns1:SubProcessName/text()}</ns2:subProcessName>
            <ns2:operation>{$messageHeader/ns1:Operation/text()}</ns2:operation>
            <ns2:messageFlowId>{$messageHeader/ns1:MessageFlowId/text()}</ns2:messageFlowId>
            <ns2:compositeInstanceId>{$messageHeader/ns1:CompositeInstanceId/text()}</ns2:compositeInstanceId>
            <ns2:businessId>{$messageHeader/ns1:LoggingDetails/ns1:BusinessId/text()}</ns2:businessId>
            <ns2:severity>{$messageHeader/ns1:LoggingDetails/ns1:Severity/text()}</ns2:severity>
            <ns2:node>{$messageHeader/ns1:LoggingDetails/ns1:Node/text()}</ns2:node>
            <ns2:location>{$messageHeader/ns1:LoggingDetails/ns1:Location/text()}</ns2:location>
            <ns2:direction>{$messageHeader/ns1:LoggingDetails/ns1:Direction/text()}</ns2:direction>
            <ns2:errorCode>{$messageHeader/ns1:ErrorDetails/ns1:ErrorCode/text()}</ns2:errorCode>)
            <ns2:messagePayload>{$CommonMessage/ns1:MessageBody/ns1:LoggingPayload/text()}</ns2:messagePayload>   
         </ns2:FmwAuditing>
    }     
    </ns2:FmwAuditingCollection>
};

local:func($CommonMessage)