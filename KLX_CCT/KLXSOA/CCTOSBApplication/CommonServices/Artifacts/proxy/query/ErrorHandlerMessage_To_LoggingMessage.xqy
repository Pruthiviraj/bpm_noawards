xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://xmlns.oracle.com/schema/common/services";
(:: import schema at "../../../../CommonDataModel/Canonicals/UtilityArtifacts/xsd/CommonMessage.xsd" ::)

declare variable $ErrorHandlerMessage as element() (:: schema-element(ns1:CommonMessage) ::) external;

declare function local:func($ErrorHandlerMessage as element() (:: schema-element(ns1:CommonMessage) ::)) as element() (:: schema-element(ns1:CommonMessage) ::) {
    let $messageHeader := $ErrorHandlerMessage/ns1:MessageHeader
    return
    <ns1:CommonMessage>
        <ns1:MessageHeader>
            <ns1:Source>{$messageHeader/ns1:Source/text()}</ns1:Source>
            <ns1:Target>{$messageHeader/ns1:Target/text()}</ns1:Target>
            <ns1:TimeStamp>{$messageHeader/ns1:TimeStamp/text()}</ns1:TimeStamp>
            <ns1:InterfaceId>{$messageHeader/ns1:InterfaceId/text()}</ns1:InterfaceId>
            <ns1:InterfaceName>{$messageHeader/ns1:InterfaceName/text()}</ns1:InterfaceName>
            <ns1:SubProcessName>{$messageHeader/ns1:SubProcessName/text()}</ns1:SubProcessName>
            <ns1:Operation>{$messageHeader/ns1:Operation/text()}</ns1:Operation>
            <ns1:MessageFlowId>{$messageHeader/ns1:MessageFlowId/text()}</ns1:MessageFlowId>
            <ns1:CompositeInstanceId>{$messageHeader/ns1:CompositeInstanceId/text()}</ns1:CompositeInstanceId>
            <ns1:ErrorDetails>
                <ns1:ErrorCode>{$messageHeader/ns1:ErrorDetails/ns1:ErrorCode/text()}</ns1:ErrorCode>
            </ns1:ErrorDetails>
            <ns1:LoggingDetails>{$messageHeader/ns1:LoggingDetails/*}</ns1:LoggingDetails>
        </ns1:MessageHeader>  
        <ns1:MessageBody>
                <ns1:LoggingPayload>{$messageHeader/ns1:ErrorDetails/ns1:ErrorMessage/text()}</ns1:LoggingPayload>
        </ns1:MessageBody> 
    </ns1:CommonMessage>
};

local:func($ErrorHandlerMessage)