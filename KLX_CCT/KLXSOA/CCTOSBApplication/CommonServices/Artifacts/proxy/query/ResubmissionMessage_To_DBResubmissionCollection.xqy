xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://xmlns.oracle.com/schema/common/services";
(:: import schema at "../../../../CommonDataModel/Canonicals/UtilityArtifacts/xsd/ResubmissionMessage.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/top/DB_OUT_FMWResubmission";
(:: import schema at "../../business/xsd/DB_OUT_FMWResubmission_table.xsd" ::)

declare variable $FMWResubmissionRequest as element() (:: schema-element(ns2:ResubmissionMessage) ::) external;

declare function local:func($FMWResubmissionRequest as element() (:: schema-element(ns2:ResubmissionMessage) ::)) as element() (:: schema-element(ns1:FmwResubmissionTblCollection) ::) {
    <ns1:FmwResubmissionTblCollection>
        <ns1:FmwResubmissionTbl>
            <ns1:timestamp>{$FMWResubmissionRequest/ns2:MessageHeader/ns2:TimeStamp/text()}</ns1:timestamp>
            <ns1:interfaceId>{$FMWResubmissionRequest/ns2:MessageHeader/ns2:InterfaceId/text()}</ns1:interfaceId>
            <ns1:interfaceName>{$FMWResubmissionRequest/ns2:MessageHeader/ns2:InterfaceName/text()}</ns1:interfaceName>
            <ns1:subProcessName>{$FMWResubmissionRequest/ns2:MessageHeader/ns2:SubProcessName/text()}</ns1:subProcessName>
            <ns1:messageFlowId>{$FMWResubmissionRequest/ns2:MessageHeader/ns2:MessageFlowId/text()}</ns1:messageFlowId>
            <ns1:operation>{$FMWResubmissionRequest/ns2:MessageHeader/ns2:Operation/text()}</ns1:operation>
            <ns1:isProcessed>N</ns1:isProcessed>
            <ns1:messagePayload>{$FMWResubmissionRequest/ns2:MessageBody/ns2:Payload/text()}</ns1:messagePayload>
        </ns1:FmwResubmissionTbl>
    </ns1:FmwResubmissionTblCollection>
};

local:func($FMWResubmissionRequest)