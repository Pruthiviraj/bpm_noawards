<Properties name="ResubmissionErrorPatternMetadata">
   <!--Editable from sbconsole, please consult the fmw dev team before making any changes to this-->
   <description>Metadata for Detecting Resubmittable Error Patterns</description>
   <Fault>
      <ErrorCode>OSB-380001</ErrorCode>
      <Pattern>java.lang.OutOfMemoryError: GC overhead limit exceeded</Pattern>
      <Description>Out Of Memory Error in JCA Transport</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380001</ErrorCode>
      <Pattern>java.lang.OutOfMemoryError: Java heap space</Pattern>
      <Description>Out Of Memory Error in JCA Transport</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380001</ErrorCode>
      <Pattern>PL/SQL: could not find program unit being called</Pattern>
      <Description>PL/SQL: could not find program unit being called</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380002</ErrorCode>
      <Pattern>PL/SQL: could not find program unit being called</Pattern>
      <Description>PL/SQL: could not find program unit being called</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380001</ErrorCode>
      <Pattern>ORA-04068: existing state of packages has been discarded</Pattern>
      <Description>Package Body Invalidated in Cache due to a Recent Code Change</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380001</ErrorCode>
      <Pattern>ORA-04063: package body</Pattern>
      <Description>Package Body has Errors after a Change to a Dependent Object referred from within the Package</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380001</ErrorCode>
      <Pattern>ORA-00942: table or view does not exist</Pattern>
      <Description>Table Accidentally Dropped or not Present in the Schema</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380002</ErrorCode>
      <Pattern>java.sql.SQLTimeoutException: ORA-01013: user requested cancel of current operation</Pattern>
      <Description>SQL Timeout Exception</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>ERR_PO1_IN_PR_009</ErrorCode>
      <Pattern>ERR_PO1_IN_PR_009:Receive transaction is not present for the particular PO, so putaway could not be completed , this transaction will be processed automatically after some time</Pattern>
      <Description>Business Error , RPA PO1 tranx still in process</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380001</ErrorCode>
      <Pattern>weblogic.transaction.RollbackException: Transaction timed out</Pattern>
      <Description>Transaction timed out</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380001</ErrorCode>
      <Pattern>JCA Binding Component was unable to establish an outbound JCA CCI connection</Pattern>
      <Description>unable to establish an outbound JCA CCI connection</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>DBAPI_ERROR_STATUS</ErrorCode>
      <Pattern>could not find program unit being called</Pattern>
      <Description>Uncompiled Sub-Package, Noticed in Customer Inbound</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>DBAPI_ERROR_STATUS</ErrorCode>
      <Pattern>Unexpected error</Pattern>
      <Description>deadlock detected while waiting for resource</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380002</ErrorCode>
      <Pattern>PoolDisabledSQLException: weblogic.common.resourcepool.ResourceDisabledException</Pattern>
      <Description>Data Source is Suspended or Disabled</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380001</ErrorCode>
      <Pattern>ConnectionDeadSQLException: weblogic.common.resourcepool.ResourceDeadException</Pattern>
      <Description>DBMS driver exception</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380002</ErrorCode>
      <Pattern>ConnectionDeadSQLException: weblogic.common.resourcepool.ResourceDeadException</Pattern>
      <Description>DBMS driver exception</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380002</ErrorCode>
      <Pattern>UniversalConnectionPoolException: weblogic.common.resourcepool.ResourceDisabledException</Pattern>
      <Description>DBMS driver exception</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380002</ErrorCode>
      <Pattern>end-of-file on communication channel for database link</Pattern>
      <Description>EBS_To_SPP Issue</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380002</ErrorCode>
      <Pattern>java.sql.SQLRecoverableException</Pattern>
      <Description>java.sql.SQLRecoverableException: Failed to setAutoCommit to true for pool connection</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380002</ErrorCode>
      <Pattern>ORA-00600: internal error code</Pattern>
      <Description>Environment Issue</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380002</ErrorCode>
      <Pattern>ORA-03111: break received on communication channel</Pattern>
      <Description>Environment Issue</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380000</ErrorCode>
      <Pattern>General runtime error</Pattern>
      <Description>Environment Issue</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380002</ErrorCode>
      <Pattern>preceding line from EBS_TO_SPP</Pattern>
      <Description>Environment Issue</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380002</ErrorCode>
      <Pattern>error occurred at recursive SQL level 1</Pattern>
      <Description>Environment Issue</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380001</ErrorCode>
      <Pattern>error occurred at recursive SQL level 1</Pattern>
      <Description>Environment Issue</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380001</ErrorCode>
      <Pattern>Problem parsing endpoint properties</Pattern>
      <Description>Environment Issue</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>DBAPI_ERROR_STATUS</ErrorCode>
      <Pattern>Unexpected Error During Cancelation</Pattern>
      <Description>DPAL Shipment DB-API Error</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380002</ErrorCode>
      <Pattern>Connection has been administratively destroyed</Pattern>
      <Description>The pool got too many consecutive failed connection tests, and failures to obtain replacement connections from the DBMS</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380001</ErrorCode>
      <Pattern>Connection has been administratively destroyed</Pattern>
      <Description>The pool got too many consecutive failed connection tests, and failures to obtain replacement connections from the DBMS</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
   <Fault>
      <ErrorCode>OSB-380002</ErrorCode>
      <Pattern>Fail to construct descriptor</Pattern>
      <Description>java.sql.SQLException: Fail to construct descriptor: Unable to resolve type</Description>
      <Resubmit>Y</Resubmit>
   </Fault>
</Properties>