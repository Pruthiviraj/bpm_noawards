<Properties name="RetainResubmissionMessageMetadata">
  <!-- Editable from sbconsole, please consult the fmw dev team before making any changes to this --> 
    <Description>Metadata for Retaining\Retrying Resubmission Message in case FMW DB is Down</Description>
    <JMSServers>KLXCCTJMSServer</JMSServers>
    <AreDestinationsDistributed>false</AreDestinationsDistributed>
    <MainDestinationJNDI>jms.klx.cct.resubmission.q</MainDestinationJNDI>
    <ErrorDestinationJNDI>jms.klx.cct.resubmission.error.q</ErrorDestinationJNDI>
</Properties>