<Properties name="InterfaceResubmissionMetadata">
   <!--Editable from sbconsole, please consult the fmw dev team before making any changes to this-->
   <description>Metadata for Resubmission Service</description>
   <Interface id="XXCCT-I-001">
      <Name>UpdateOpportunity</Name>
      <!--Repeat for mutiple services within same interface-->
      <ResubmissionPoint>
         <SubProcessName>PLP_UpdateOpportunity</SubProcessName>
         <ServiceImplementation>OSB</ServiceImplementation>
         <TypeofService>Proxy</TypeofService>
         <URL>UpdateOpportunity/ProxyServices/UpdateOpportunity</URL>
         <RetryCount>-1</RetryCount>
      </ResubmissionPoint>
   </Interface>
</Properties>