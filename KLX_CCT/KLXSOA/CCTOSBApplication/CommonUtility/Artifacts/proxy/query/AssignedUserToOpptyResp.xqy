xquery version "1.0" encoding "utf-8";
(:: pragma  parameter="$anyType1" type="anyType" ::)
(:: pragma  type="anyType" ::)


declare namespace xf = "http://www.klx.com/xquery/bpm/task/wrapper/V1.0";
declare namespace ns1="http://www.klx.com/cct/tqs/resp/wrapper/V1.0";
declare namespace bea-xf="http://www.bea.com/2002/xquery-functions";

declare function xf:getAssignedUserId($Input as element(*))
    as element(*) {
      <Users>
        {
          for $task in $Input/ns1:task
                return        
          <UserId>{$task/ns1:TaskAttributes/ns1:acquiredBy/text()}</UserId>
        }
      </Users>    
 }; 
 
declare variable $Input as element(*) external;
xf:getAssignedUserId($Input)