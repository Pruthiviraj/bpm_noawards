xquery version "1.0" encoding "utf-8";
(:: pragma  parameter="$anyType1" type="anyType" ::)
(:: pragma  type="anyType" ::)


declare namespace xf = "http://www.klx.com/xquery/bpm/task/wrapper/V1.0";
declare namespace ns2="http://xmlns.oracle.com/apps/sales/opptyMgmt/opportunities/opportunityService/";
declare namespace tns="http://www.klx.com/cct/osc/user/V1.0";


declare function xf:getOpptyDetailsOnId($Input as element(*))
    as element(*) {
    
<tns:GetOpptyResponse>
  <tns:OpportunityId>{$Input//*:result/ns2:OptyId/text()}</tns:OpportunityId>
  <tns:OptyNumber>{$Input//*:result/ns2:OptyNumber/text()}</tns:OptyNumber>
  <tns:Name>{$Input//*:result/ns2:Name/text()}</tns:Name>
</tns:GetOpptyResponse>
        
 };    

declare variable $Input as element(*) external;

xf:getOpptyDetailsOnId($Input)