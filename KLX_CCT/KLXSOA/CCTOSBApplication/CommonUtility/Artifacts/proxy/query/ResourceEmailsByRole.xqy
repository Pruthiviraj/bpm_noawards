xquery version "1.0" encoding "utf-8";
(:: pragma  parameter="$anyType1" type="anyType" ::)
(:: pragma  type="anyType" ::)


declare namespace xf = "http://www.klx.com/xquery/bpm/task/wrapper/V1.0";
declare namespace ns2="http://xmlns.oracle.com/apps/sales/opptyMgmt/opportunities/opportunityService/";
declare namespace ns1="http://www.klx.com/cct/osc/user/V1.0";
declare namespace bea-xf="http://www.bea.com/2002/xquery-functions";

declare function xf:getEmailsByRole($Input as element(*),$Response as element(*))
    as element(*) {
      <Emails>
        {
          for $roles in $Input//Role/text(),$opportunityResource in $Response//ns2:OpportunityResource[ns2:MemberFunctionCode=$roles]
                return        
          <Email>{$opportunityResource/ns2:EmailAddress/text()}</Email>
        }
      </Emails>    
 };    



declare variable $Input as element(*) external;
declare variable $Response as element(*) external;

xf:getEmailsByRole($Input,$Response)