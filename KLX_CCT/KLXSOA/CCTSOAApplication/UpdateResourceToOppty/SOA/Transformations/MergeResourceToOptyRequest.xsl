<?xml version = '1.0' encoding = 'UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:ns0="http://xmlns.klx.com/schema/cct/oppty/resource/V1.0"
                xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction"
                xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20"
                xmlns:ns1="http://xmlns.oracle.com/pcbpel/adapter/osc/ContractCollaborationSOAApplication/UpdateOpportunityToOSC/OscResourceReference/types"
                xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions"
                xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator"
                xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas"
                xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue"
                xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tns="http://xmlns.oracle.com/pcbpel/adapter/osc/ContractCollaborationSOAApplication/UpdateOpportunityToOSC/OscOpportunityReference/types"
                exclude-result-prefixes="xsd oracle-xsl-mapper xsi xsl ns0 ns1 tns mhdr oraext xp20 xref socket dvm oraxsl"
                xmlns:types="http://xmlns.oracle.com/apps/cdm/foundation/resources/resourceService/applicationModule/types/"
                xmlns:orafault="http://xmlns.oracle.com/oracleas/schema/oracle-fault-11_0"
                xmlns:plnk="http://schemas.xmlsoap.org/ws/2003/05/partner-link/"
                xmlns:ns4="http://xmlns.oracle.com/adf/svc/errors/" xmlns:ns7="http://xmlns.oracle.com/adf/svc/types/"
                xmlns:sdoJava="commonj.sdo/java"
                xmlns:ns5="http://xmlns.oracle.com/pcbpel/adapter/osc/ContractCollaborationSOAApplication/UpdateOpportunityToOSC/OscResourceReference"
                xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"
                xmlns:ns3="http://xmlns.klx.com/wsdl/cct/oppty/resource/V1.0/types"
                xmlns:ns6="http://xmlns.oracle.com/apps/cdm/foundation/resources/resourceService/"
                xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
                xmlns:ns2="http://xmlns.klx.com/wsdl/cct/oppty/resource/V1.0"
                xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/"
                xmlns:ns13="http://xmlns.oracle.com/oracle/apps/sales/opptyMgmt/revenues/revenueService/"
                xmlns:sdoXML="commonj.sdo/xml"
                xmlns:ns15="http://xmlns.oracle.com/apps/crmCommon/activities/activitiesService/"
                xmlns:ns11="http://xmlns.oracle.com/apps/sales/opptyMgmt/revenues/revenueService/"
                xmlns:ns12="http://xmlns.oracle.com/apps/crmCommon/notes/flex/noteDff/"
                xmlns:ns9="http://xmlns.oracle.com/apps/sales/opptyMgmt/opportunities/opportunityService/types/"
                xmlns:ns8="http://xmlns.oracle.com/apps/sales/opptyMgmt/opportunities/opportunityService/"
                xmlns:sdo="commonj.sdo"
                xmlns:ns10="http://xmlns.oracle.com/pcbpel/adapter/osc/ContractCollaborationSOAApplication/UpdateOpportunityToOSC/OscOpportunityReference"
                xmlns:ns14="http://xmlns.oracle.com/apps/crmCommon/notes/noteService"
                xmlns:cct="http://www.oracle.com/XSL/Transform/java/com.cct.xpathutility.XpathUtility"
                xmlns:wkcdfv="http://www.klx.com/db/fault/V1.0" xmlns:weo="http://www.example.org">
  <oracle-xsl-mapper:schema>
    <!--SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY.-->
    <oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:source type="WSDL">
        <oracle-xsl-mapper:schema location="../WSDLs/OpportunityResource.wsdl"/>
        <oracle-xsl-mapper:rootElement name="OpptyResource"
                                       namespace="http://xmlns.klx.com/schema/cct/oppty/resource/V1.0"/>
      </oracle-xsl-mapper:source>
      <oracle-xsl-mapper:source type="WSDL">
        <oracle-xsl-mapper:schema location="../WSDLs/OscResourceReference.wsdl"/>
        <oracle-xsl-mapper:rootElement name="findResourceResponse"
                                       namespace="http://xmlns.oracle.com/pcbpel/adapter/osc/ContractCollaborationSOAApplication/UpdateOpportunityToOSC/OscResourceReference/types"/>
        <oracle-xsl-mapper:param name="Invoke_OSC_Resource_findResource_OutputVariable.parameters"/>
      </oracle-xsl-mapper:source>
    </oracle-xsl-mapper:mapSources>
    <oracle-xsl-mapper:mapTargets>
      <oracle-xsl-mapper:target type="WSDL">
        <oracle-xsl-mapper:schema location="../WSDLs/OscOpportunityReference.wsdl"/>
        <oracle-xsl-mapper:rootElement name="mergeOpportunity"
                                       namespace="http://xmlns.oracle.com/pcbpel/adapter/osc/ContractCollaborationSOAApplication/UpdateOpportunityToOSC/OscOpportunityReference/types"/>
      </oracle-xsl-mapper:target>
    </oracle-xsl-mapper:mapTargets>
    <!--GENERATED BY ORACLE XSL MAPPER 12.2.1.2.0(XSLT Build 161003.0739.0018) AT [MON JUL 03 11:04:55 EDT 2017].-->
  </oracle-xsl-mapper:schema>
  <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
  <xsl:param name="Invoke_OSC_Resource_findResource_OutputVariable.parameters"/>
  <xsl:template match="/">
    <tns:mergeOpportunity>
      <tns:opportunity>
        <ns8:OptyId>
          <xsl:value-of select="/ns0:OpptyResource/ns0:Resource/ns0:OpptyId"/>
        </ns8:OptyId>
        <ns8:OpportunityResource>
          <ns8:OptyId>
            <xsl:value-of select="/ns0:OpptyResource/ns0:Resource/ns0:OpptyId"/>
          </ns8:OptyId>
          <ns8:ResourceId>
            <xsl:value-of select="$Invoke_OSC_Resource_findResource_OutputVariable.parameters/ns1:findResourceResponse/ns1:result/ns6:Value/ns6:PartyId"/>
          </ns8:ResourceId>
          <ns8:MemberFunctionCode>
            <xsl:value-of select="oraext:query-database(concat (&quot;select OSC_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP ='&quot;, /ns0:OpptyResource/ns0:Resource/ns0:RoleName, &quot;'&quot; ),false(),false(),'jdbc/XxKLXDataSource')"/>
          </ns8:MemberFunctionCode>
        </ns8:OpportunityResource>
        <ns8:AwardFileUpload_c>IDW</ns8:AwardFileUpload_c>
      </tns:opportunity>
    </tns:mergeOpportunity>
  </xsl:template>
</xsl:stylesheet>