<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:tns="http://xmlns.oracle.com/schema/common/services" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:ns0="http://xmlns.oracle.com/pcbpel/adapter/db/sp/GetContractParts" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes=" xsd oracle-xsl-mapper xsi xsl ns0 tns mhdr oraext xp20 xref socket dvm oraxsl"
                xmlns:plt="http://schemas.xmlsoap.org/ws/2003/05/partner-link/"
                xmlns:ns1="http://xmlns.oracle.com/pcbpel/adapter/db/CCTSOAApplication/SyncContractPartsToCDX/GetContractParts"
                xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:jca="http://xmlns.oracle.com/pcbpel/wsdl/jca/"
                xmlns:plnk="http://docs.oasis-open.org/wsbpel/2.0/plnktype"
                xmlns:client="http://xmlns.oracle.com/CCT/SyncContractPartsToCDX/SyncContractPartsToCDX">
   <oracle-xsl-mapper:schema>
      <!--SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY.-->
      <oracle-xsl-mapper:mapSources>
         <oracle-xsl-mapper:source type="WSDL">
            <oracle-xsl-mapper:schema location="../WSDLs/GetContractParts.wsdl"/>
            <oracle-xsl-mapper:rootElement name="OutputParameters" namespace="http://xmlns.oracle.com/pcbpel/adapter/db/sp/GetContractParts"/>
         </oracle-xsl-mapper:source>
      </oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:mapTargets>
         <oracle-xsl-mapper:target type="WSDL">
            <oracle-xsl-mapper:schema location="../WSDLs/SyncContractPartsToCDX.wsdl"/>
            <oracle-xsl-mapper:rootElement name="CONTRACT_PARTS" namespace="http://xmlns.oracle.com/schema/common/services"/>
         </oracle-xsl-mapper:target>
      </oracle-xsl-mapper:mapTargets>
      <!--GENERATED BY ORACLE XSL MAPPER 12.2.1.3.0(XSLT Build 170820.1700.2557) AT [WED JUN 12 15:01:08 EDT 2019].-->
   </oracle-xsl-mapper:schema>
   <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
   <xsl:template match="/">
      <tns:CONTRACT_PARTS>
         <tns:CONTRACT_PARTS_HEADER>
            <tns:CONTRACT_NO>
               <xsl:value-of select="/ns0:OutputParameters/ns0:X_HDR_REC/ns0:CONTRACT_NO"/>
            </tns:CONTRACT_NO>
            <tns:CUSTOMER_NO>
               <xsl:value-of select="/ns0:OutputParameters/ns0:X_HDR_REC/ns0:CUSTOMER_NO"/>
            </tns:CUSTOMER_NO>
            <tns:CONTRACT_NAME>
               <xsl:value-of select="/ns0:OutputParameters/ns0:X_HDR_REC/ns0:CONTRACT_NAME"/>
            </tns:CONTRACT_NAME>
            <tns:CONTRACT_CODE>
               <xsl:value-of select="/ns0:OutputParameters/ns0:X_HDR_REC/ns0:CONTRACT_CODE"/>
            </tns:CONTRACT_CODE>
            <tns:INITIATION_DATE>
               <xsl:value-of select="/ns0:OutputParameters/ns0:X_HDR_REC/ns0:INITIATION_DATE"/>
            </tns:INITIATION_DATE>
            <tns:AWARD_DATE>
               <xsl:value-of select="/ns0:OutputParameters/ns0:X_HDR_REC/ns0:AWARD_DATE"/>
            </tns:AWARD_DATE>
            <tns:EFFECTIVE_DATE>
               <xsl:value-of select="/ns0:OutputParameters/ns0:X_HDR_REC/ns0:EFFECTIVE_DATE"/>
            </tns:EFFECTIVE_DATE>
            <tns:EXPIRATION_DATE>
               <xsl:value-of select="/ns0:OutputParameters/ns0:X_HDR_REC/ns0:EXPIRATION_DATE"/>
            </tns:EXPIRATION_DATE>
            <tns:DFAR>
               <xsl:value-of select="/ns0:OutputParameters/ns0:X_HDR_REC/ns0:DFAR"/>
            </tns:DFAR>
            <tns:C_ADMIN>
               <xsl:value-of select="/ns0:OutputParameters/ns0:X_HDR_REC/ns0:C_ADMIN"/>
            </tns:C_ADMIN>
            <tns:PRIORITY>
               <xsl:value-of select="/ns0:OutputParameters/ns0:X_HDR_REC/ns0:PRIORITY"/>
            </tns:PRIORITY>
            <tns:SALESMAN>
               <xsl:value-of select="/ns0:OutputParameters/ns0:X_HDR_REC/ns0:SALESMAN"/>
            </tns:SALESMAN>
            <tns:SALESDIRECTOR>
               <xsl:value-of select="/ns0:OutputParameters/ns0:X_HDR_REC/ns0:SALESDIRECTOR"/>
            </tns:SALESDIRECTOR>
            <tns:ATTRIBUTE1>
               <xsl:value-of select="/ns0:OutputParameters/ns0:X_HDR_REC/ns0:ATTRIBUTE1"/>
            </tns:ATTRIBUTE1>
            <tns:ATTRIBUTE2>
               <xsl:value-of select="/ns0:OutputParameters/ns0:X_HDR_REC/ns0:ATTRIBUTE2"/>
            </tns:ATTRIBUTE2>
            <tns:ATTRIBUTE3>
               <xsl:value-of select="/ns0:OutputParameters/ns0:X_HDR_REC/ns0:ATTRIBUTE3"/>
            </tns:ATTRIBUTE3>
            <tns:ATTRIBUTE4>
               <xsl:value-of select="/ns0:OutputParameters/ns0:X_HDR_REC/ns0:ATTRIBUTE4"/>
            </tns:ATTRIBUTE4>
            <tns:ATTRIBUTE5>
               <xsl:value-of select="/ns0:OutputParameters/ns0:X_HDR_REC/ns0:ATTRIBUTE5"/>
            </tns:ATTRIBUTE5>
         </tns:CONTRACT_PARTS_HEADER>
         <tns:CONTRACT_PARTS_LINES>
            <xsl:for-each select="/ns0:OutputParameters/ns0:X_LINE_TYPE/ns0:X_LINE_TYPE_ITEM">
               <tns:X_LINE_TYPE_ITEM>
                  <tns:LINE_NO>
                     <xsl:value-of select="ns0:LINE_NO"/>
                  </tns:LINE_NO>
                  <tns:INVOICED_PART>
                     <xsl:value-of select="ns0:INVOICED_PART"/>
                  </tns:INVOICED_PART>
                  <tns:CERTIFIED_PART>
                     <xsl:value-of select="ns0:CERTIFIED_PART"/>
                  </tns:CERTIFIED_PART>
                  <tns:ANNUAL_QTY>
                     <xsl:value-of select="ns0:ANNUAL_QTY"/>
                  </tns:ANNUAL_QTY>
                  <tns:PRICE>
                     <xsl:value-of select="ns0:PRICE"/>
                  </tns:PRICE>
                  <tns:UOM>
                     <xsl:value-of select="ns0:UOM"/>
                  </tns:UOM>
                  <tns:ITEM_CLASSIFICATION>
                     <xsl:value-of select="ns0:ITEM_CLASSIFICATION"/>
                  </tns:ITEM_CLASSIFICATION>
                  <tns:LEADTIME>
                     <xsl:value-of select="ns0:LEADTIME"/>
                  </tns:LEADTIME>
                  <tns:EFFECTIVE_DATE>
                     <xsl:value-of select="ns0:EFFECTIVE_DATE"/>
                  </tns:EFFECTIVE_DATE>
                  <tns:STATUS>
                     <xsl:value-of select="ns0:STATUS"/>
                  </tns:STATUS>
                  <tns:COMMENTS>
                     <xsl:value-of select="ns0:COMMENTS"/>
                  </tns:COMMENTS>
                  <tns:APPROVED_MANUF>
                     <xsl:value-of select="ns0:APPROVED_MANUF"/>
                  </tns:APPROVED_MANUF>
                  <tns:DISAPPROVED_MANUF>
                     <xsl:value-of select="ns0:DISAPPROVED_MANUF"/>
                  </tns:DISAPPROVED_MANUF>
                  <tns:CUSTOMER_REFERENCE>
                     <xsl:value-of select="ns0:CUSTOMER_REFERENCE"/>
                  </tns:CUSTOMER_REFERENCE>
                  <tns:ASL_CODES>
                     <xsl:value-of select="ns0:ASL_CODES"/>
                  </tns:ASL_CODES>
                  <tns:PRIME_NUMBER>
                     <xsl:value-of select="ns0:PRIME_NUMBER"/>
                  </tns:PRIME_NUMBER>
                  <tns:CONTRACT_NUMBER>
                     <xsl:value-of select="ns0:CONTRACT_NUMBER"/>
                  </tns:CONTRACT_NUMBER>
                  <tns:FUTURE_PRICE>
                     <xsl:value-of select="ns0:FUTURE_PRICE"/>
                  </tns:FUTURE_PRICE>
                  <tns:FUTURE_PRICE_EFFECTIVE_DATE>
                     <xsl:value-of select="ns0:FUTURE_PRICE_EFFECTIVE_DATE"/>
                  </tns:FUTURE_PRICE_EFFECTIVE_DATE>
                  <tns:EXPIRATION_DATE>
                     <xsl:value-of select="ns0:EXPIRATION_DATE"/>
                  </tns:EXPIRATION_DATE>
                  <tns:REVISION_LEVEL>
                     <xsl:value-of select="ns0:REVISION_LEVEL"/>
                  </tns:REVISION_LEVEL>
                  <tns:MOQ>
                     <xsl:value-of select="ns0:MOQ"/>
                  </tns:MOQ>
                  <tns:SAP_NUMBER>
                     <xsl:value-of select="ns0:SAP_NUMBER"/>
                  </tns:SAP_NUMBER>
                  <tns:CURRENCY>
                     <xsl:value-of select="ns0:CURRENCY"/>
                  </tns:CURRENCY>
                  <tns:TOTAL_CONTRACT_QUANTITY>
                     <xsl:value-of select="ns0:TOTAL_CONTRACT_QUANTITY"/>
                  </tns:TOTAL_CONTRACT_QUANTITY>
                  <tns:ORIGINAL_QUOTE_COST>
                     <xsl:value-of select="ns0:ORIGINAL_QUOTE_COST"/>
                  </tns:ORIGINAL_QUOTE_COST>
                  <tns:COST_SOURCE>
                     <xsl:value-of select="ns0:COST_SOURCE"/>
                  </tns:COST_SOURCE>
                  <tns:CONTRACTS_COMMENTS>
                     <xsl:value-of select="ns0:CONTRACTS_COMMENTS"/>
                  </tns:CONTRACTS_COMMENTS>
                  <tns:PURCHASING_COMMENTS>
                     <xsl:value-of select="ns0:PURCHASING_COMMENTS"/>
                  </tns:PURCHASING_COMMENTS>
                  <tns:HPP_ESCALATION_NOTES>
                     <xsl:value-of select="ns0:HPP_ESCALATION_NOTES"/>
                  </tns:HPP_ESCALATION_NOTES>
                  <tns:SALES_COMMENTS>
                     <xsl:value-of select="ns0:SALES_COMMENTS"/>
                  </tns:SALES_COMMENTS>
                  <tns:CUSTOMER_COMMENTS>
                     <xsl:value-of select="ns0:CUSTOMER_COMMENTS"/>
                  </tns:CUSTOMER_COMMENTS>
                  <tns:VENDOR_4PL>
                     <xsl:value-of select="ns0:VENDOR_4PL"/>
                  </tns:VENDOR_4PL>
                  <tns:APPLICABLE_CONTRACT>
                     <xsl:value-of select="ns0:APPLICABLE_CONTRACT"/>
                  </tns:APPLICABLE_CONTRACT>
                  <tns:CUST_EXCLUDE_PN>
                     <xsl:value-of select="ns0:CUST_EXCLUDE_PN"/>
                  </tns:CUST_EXCLUDE_PN>
                  <tns:PLANNING_COMMENTS>
                     <xsl:value-of select="ns0:PLANNING_COMMENTS"/>
                  </tns:PLANNING_COMMENTS>
                  <tns:CUSTOMER_PO>
                     <xsl:value-of select="ns0:CUSTOMER_PO"/>
                  </tns:CUSTOMER_PO>
                  <tns:ATTRIBUTE1>
                     <xsl:value-of select="ns0:ATTRIBUTE1"/>
                  </tns:ATTRIBUTE1>
                  <tns:ATTRIBUTE2>
                     <xsl:value-of select="ns0:ATTRIBUTE2"/>
                  </tns:ATTRIBUTE2>
                  <tns:ATTRIBUTE3>
                     <xsl:value-of select="ns0:ATTRIBUTE3"/>
                  </tns:ATTRIBUTE3>
                  <tns:ATTRIBUTE4>
                     <xsl:value-of select="ns0:ATTRIBUTE4"/>
                  </tns:ATTRIBUTE4>
                  <tns:ATTRIBUTE5>
                     <xsl:value-of select="ns0:ATTRIBUTE5"/>
                  </tns:ATTRIBUTE5>
                  <tns:L_ATTRIBUTE1>
                     <xsl:value-of select="ns0:L_ATTRIBUTE1"/>
                  </tns:L_ATTRIBUTE1>
                  <tns:L_ATTRIBUTE2>
                     <xsl:value-of select="ns0:L_ATTRIBUTE2"/>
                  </tns:L_ATTRIBUTE2>
                  <tns:L_ATTRIBUTE3>
                     <xsl:value-of select="ns0:L_ATTRIBUTE3"/>
                  </tns:L_ATTRIBUTE3>
                  <tns:L_ATTRIBUTE4>
                     <xsl:value-of select="ns0:L_ATTRIBUTE4"/>
                  </tns:L_ATTRIBUTE4>
                  <tns:L_ATTRIBUTE5>
                     <xsl:value-of select="ns0:L_ATTRIBUTE5"/>
                  </tns:L_ATTRIBUTE5>
               </tns:X_LINE_TYPE_ITEM>
            </xsl:for-each>
         </tns:CONTRACT_PARTS_LINES>
      </tns:CONTRACT_PARTS>
   </xsl:template>
</xsl:stylesheet>
