<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:ns0="http://xmlns.oracle.com/schema/common/services" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:nsd1="http://www.klx.com/db/fault/V1.0" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes=" xsd oracle-xsl-mapper xsi xsl ns0 nsd1 mhdr oraext xp20 xref socket dvm oraxsl"
                xmlns:ns1="http://xmlns.klx.com/schema/cct/notification/V1.0"
                xmlns:tns="http://xmlns.oracle.com/pcbpel/adapter/jms/CCTSSOAApplication/UpdateResourceToOppty/ErrorHandler_JMSAdapter"
                xmlns:plt="http://schemas.xmlsoap.org/ws/2003/05/partner-link/"
                xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:jca="http://xmlns.oracle.com/pcbpel/wsdl/jca/">
   <oracle-xsl-mapper:schema>
      <!--SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY.-->
      <oracle-xsl-mapper:mapSources>
         <oracle-xsl-mapper:source type="WSDL">
            <oracle-xsl-mapper:schema location="../WSDLs/ErrorHandler_JMSAdapter.wsdl"/>
            <oracle-xsl-mapper:rootElement name="CommonMessage" namespace="http://xmlns.oracle.com/schema/common/services"/>
         </oracle-xsl-mapper:source>
         <oracle-xsl-mapper:source type="WSDL">
            <oracle-xsl-mapper:schema location="../WSDLs/CCTNotificationWrapper.wsdl"/>
            <oracle-xsl-mapper:rootElement name="dbFaultMessagePart" namespace=""/>
            <oracle-xsl-mapper:rootElementDatatype name="DBFaultType" namespace="http://www.klx.com/db/fault/V1.0"/>
            <oracle-xsl-mapper:param name="FaultVariable.dbFaultMessagePart"/>
         </oracle-xsl-mapper:source>
      </oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:mapTargets>
         <oracle-xsl-mapper:target type="WSDL">
            <oracle-xsl-mapper:schema location="../WSDLs/ErrorHandler_JMSAdapter.wsdl"/>
            <oracle-xsl-mapper:rootElement name="CommonMessage" namespace="http://xmlns.oracle.com/schema/common/services"/>
         </oracle-xsl-mapper:target>
      </oracle-xsl-mapper:mapTargets>
      <!--GENERATED BY ORACLE XSL MAPPER 12.2.1.2.0(XSLT Build 161003.0739.0018) AT [THU JAN 11 16:21:44 IST 2018].-->
   </oracle-xsl-mapper:schema>
   <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
   <xsl:param name="FaultVariable.dbFaultMessagePart"/>
   <xsl:template match="/">
      <ns0:CommonMessage>
         <ns0:MessageHeader>
            <ns0:Source>
               <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:Source"/>
            </ns0:Source>
            <ns0:Target>
               <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:Target"/>
            </ns0:Target>
            <ns0:TimeStamp>
               <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:TimeStamp"/>
            </ns0:TimeStamp>
            <ns0:ServiceInvoked>
               <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:ServiceInvoked"/>
            </ns0:ServiceInvoked>
            <ns0:InterfaceId>
               <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:InterfaceId"/>
            </ns0:InterfaceId>
            <ns0:InterfaceName>
               <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:InterfaceName"/>
            </ns0:InterfaceName>
            <ns0:SubProcessName>
               <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:SubProcessName"/>
            </ns0:SubProcessName>
            <xsl:if test="/ns0:CommonMessage/ns0:MessageHeader/ns0:Operation">
               <ns0:Operation>
                  <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:Operation"/>
               </ns0:Operation>
            </xsl:if>
            <ns0:MessageFlowId>
               <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:MessageFlowId"/>
            </ns0:MessageFlowId>
            <xsl:if test="/ns0:CommonMessage/ns0:MessageHeader/ns0:CompositeInstanceId">
               <ns0:CompositeInstanceId>
                  <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:CompositeInstanceId"/>
               </ns0:CompositeInstanceId>
            </xsl:if>
            <xsl:if test="/ns0:CommonMessage/ns0:MessageHeader/ns0:ErrorDetails">
               <ns0:ErrorDetails>
                  <ns0:ErrorCode>
                     <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:ErrorDetails/ns0:ErrorCode"/>
                  </ns0:ErrorCode>
                  <ns0:ErrorMessage>
                     <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:ErrorDetails/ns0:ErrorMessage"/>
                  </ns0:ErrorMessage>
                  <xsl:if test="/ns0:CommonMessage/ns0:MessageHeader/ns0:ErrorDetails/ns0:ErrorStackTrace">
                     <ns0:ErrorStackTrace>
                        <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:ErrorDetails/ns0:ErrorStackTrace"/>
                     </ns0:ErrorStackTrace>
                  </xsl:if>
                  <xsl:if test="/ns0:CommonMessage/ns0:MessageHeader/ns0:ErrorDetails/ns0:InputRequest">
                     <ns0:InputRequest>
                        <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:ErrorDetails/ns0:InputRequest"/>
                     </ns0:InputRequest>
                  </xsl:if>
               </ns0:ErrorDetails>
            </xsl:if>
            <xsl:if test="/ns0:CommonMessage/ns0:MessageHeader/ns0:LoggingDetails">
               <ns0:LoggingDetails>
                  <ns0:Severity>
                     <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:LoggingDetails/ns0:Severity"/>
                  </ns0:Severity>
                  <ns0:Node>
                     <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:LoggingDetails/ns0:Node"/>
                  </ns0:Node>
                  <ns0:Location>
                     <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:LoggingDetails/ns0:Location"/>
                  </ns0:Location>
                  <ns0:Direction>
                     <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:LoggingDetails/ns0:Direction"/>
                  </ns0:Direction>
                  <xsl:if test="/ns0:CommonMessage/ns0:MessageHeader/ns0:LoggingDetails/ns0:BusinessId">
                     <ns0:BusinessId>
                        <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:LoggingDetails/ns0:BusinessId"/>
                     </ns0:BusinessId>
                  </xsl:if>
                  <xsl:if test="/ns0:CommonMessage/ns0:MessageHeader/ns0:LoggingDetails/ns0:DVMLookup">
                     <ns0:DVMLookup>
                        <ns0:FilterLogLevel>
                           <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:LoggingDetails/ns0:DVMLookup/ns0:FilterLogLevel"/>
                        </ns0:FilterLogLevel>
                        <ns0:LogLocation>
                           <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:LoggingDetails/ns0:DVMLookup/ns0:LogLocation"/>
                        </ns0:LogLocation>
                     </ns0:DVMLookup>
                  </xsl:if>
               </ns0:LoggingDetails>
            </xsl:if>
            <xsl:if test="/ns0:CommonMessage/ns0:MessageHeader/ns0:NotificationDetails">
               <ns0:NotificationDetails>
                  <ns0:To>
                     <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:NotificationDetails/ns0:To"/>
                  </ns0:To>
                  <xsl:if test="/ns0:CommonMessage/ns0:MessageHeader/ns0:NotificationDetails/ns0:ReplyTo">
                     <ns0:ReplyTo>
                        <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:NotificationDetails/ns0:ReplyTo"/>
                     </ns0:ReplyTo>
                  </xsl:if>
                  <xsl:if test="/ns0:CommonMessage/ns0:MessageHeader/ns0:NotificationDetails/ns0:Cc">
                     <ns0:Cc>
                        <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:NotificationDetails/ns0:Cc"/>
                     </ns0:Cc>
                  </xsl:if>
                  <xsl:if test="/ns0:CommonMessage/ns0:MessageHeader/ns0:NotificationDetails/ns0:Bcc">
                     <ns0:Bcc>
                        <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:NotificationDetails/ns0:Bcc"/>
                     </ns0:Bcc>
                  </xsl:if>
                  <ns0:Subject>
                     <xsl:value-of select="/ns0:CommonMessage/ns0:MessageHeader/ns0:NotificationDetails/ns0:Subject"/>
                  </ns0:Subject>
                  <xsl:if test="/ns0:CommonMessage/ns0:MessageHeader/ns0:NotificationDetails/ns0:ArrayOfAttachment">
                     <ns0:ArrayOfAttachment>
                        <xsl:for-each select="/ns0:CommonMessage/ns0:MessageHeader/ns0:NotificationDetails/ns0:ArrayOfAttachment/ns0:Attachment">
                           <ns0:Attachment>
                              <ns0:Name>
                                 <xsl:value-of select="ns0:Name"/>
                              </ns0:Name>
                              <ns0:Type>
                                 <xsl:value-of select="ns0:Type"/>
                              </ns0:Type>
                              <ns0:Content>
                                 <xsl:value-of select="ns0:Content"/>
                              </ns0:Content>
                           </ns0:Attachment>
                        </xsl:for-each>
                     </ns0:ArrayOfAttachment>
                  </xsl:if>
               </ns0:NotificationDetails>
            </xsl:if>
         </ns0:MessageHeader>
         <ns0:MessageBody>
            <xsl:choose>
               <xsl:when test="$FaultVariable.dbFaultMessagePart/dbFaultMessagePart/nsd1:MessagePaylod !=''">
                  <ns0:LoggingPayload>
                     <xsl:value-of select="$FaultVariable.dbFaultMessagePart/dbFaultMessagePart/nsd1:MessagePaylod"/>
                  </ns0:LoggingPayload>
               </xsl:when>
               <xsl:otherwise>
                  <ns0:LoggingPayload>
                     <xsl:value-of select="/ns0:CommonMessage/ns0:MessageBody/ns0:LoggingPayload"/>
                  </ns0:LoggingPayload>
               </xsl:otherwise>
            </xsl:choose>
            <xsl:if test="/ns0:CommonMessage/ns0:MessageBody/ns0:NotificationPayload">
               <ns0:NotificationPayload>
                  <xsl:value-of select="/ns0:CommonMessage/ns0:MessageBody/ns0:NotificationPayload"/>
               </ns0:NotificationPayload>
            </xsl:if>
         </ns0:MessageBody>
      </ns0:CommonMessage>
   </xsl:template>
</xsl:stylesheet>
