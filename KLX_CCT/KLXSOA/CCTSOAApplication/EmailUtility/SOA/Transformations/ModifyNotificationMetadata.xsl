<?xml version = '1.0' encoding = 'UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java" xmlns:ns0="http://xmlns.klx.com/schema/cct/notification/metadata/V1.0" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" exclude-result-prefixes="oracle-xsl-mapper xsi xsd xsl ns0 socket dvm mhdr oraxsl oraext xp20 xref" xmlns:plnk="http://docs.oasis-open.org/wsbpel/2.0/plnktype" xmlns:tns="http://xmlns.klx.com/wsdl/cct/notification/V1.0" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:bpws="http://docs.oasis-open.org/wsbpel/2.0/process/executable">
    <oracle-xsl-mapper:schema>
        <!--SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY.-->
        <oracle-xsl-mapper:mapSources>
            <oracle-xsl-mapper:source type="XSD">
                <oracle-xsl-mapper:schema location="../Schemas/NotificationMetadata.xsd"/>
                <oracle-xsl-mapper:rootElement name="CCTNotificationMetadata" namespace="http://xmlns.klx.com/schema/cct/notification/metadata/V1.0"/>
            </oracle-xsl-mapper:source>
        </oracle-xsl-mapper:mapSources>
        <oracle-xsl-mapper:mapTargets>
            <oracle-xsl-mapper:target type="XSD">
                <oracle-xsl-mapper:schema location="../Schemas/NotificationMetadata.xsd"/>
                <oracle-xsl-mapper:rootElement name="CCTNotificationMetadata" namespace="http://xmlns.klx.com/schema/cct/notification/metadata/V1.0"/>
            </oracle-xsl-mapper:target>
        </oracle-xsl-mapper:mapTargets>
        <!--GENERATED BY ORACLE XSL MAPPER 12.2.1.2.0(XSLT Build 161003.0739.0018) AT [MON SEP 11 09:55:42 EDT 2017].-->
    </oracle-xsl-mapper:schema>
    <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
    <xsl:template match="/">
        <ns0:CCTNotificationMetadata>
            <ns0:MetadataAttributes>
                <ns0:OpportunityNumber>
                    <xsl:value-of select="/ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:OpportunityNumber"/>
                </ns0:OpportunityNumber>
                <ns0:OpportunityId>
                    <xsl:value-of select="/ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:OpportunityId"/>
                </ns0:OpportunityId>
                <ns0:NotificationTemplateId>
                    <xsl:value-of select="/ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:NotificationTemplateId"/>
                </ns0:NotificationTemplateId>
                <ns0:Category>
                    <xsl:value-of select="/ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:Category"/>
                </ns0:Category>
                <ns0:isMailTriggerRequired>
                    <xsl:value-of select="/ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:isMailTriggerRequired"/>
                </ns0:isMailTriggerRequired>
                <ns0:Source>
                    <xsl:value-of select="/ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:Source"/>
                </ns0:Source>
                <xsl:choose>
                    <xsl:when test="dvm:lookupValue(&quot;DVM/NotificationMetadata.dvm&quot;,&quot;TemplateId&quot;,/ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:NotificationTemplateId,&quot;TO&quot;,&quot;&quot;)!=&quot;&quot; and /ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:ApprovalUserEmail !=&quot;&quot;">
                        <ns0:To>
                            <xsl:value-of select="concat(dvm:lookupValue(&quot;DVM/NotificationMetadata.dvm&quot;,&quot;TemplateId&quot;,/ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:NotificationTemplateId,&quot;TO&quot;,&quot;&quot;),&quot;,&quot;,/ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:ApprovalUserEmail)"/>
                        </ns0:To>
                    </xsl:when>
                    <xsl:when test="dvm:lookupValue(&quot;DVM/NotificationMetadata.dvm&quot;,&quot;TemplateId&quot;,/ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:NotificationTemplateId,&quot;TO&quot;,&quot;&quot;)!=&quot;&quot; and /ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:ApprovalUserEmail =&quot;&quot;">
                        <ns0:To>
                            <xsl:value-of select="dvm:lookupValue(&quot;DVM/NotificationMetadata.dvm&quot;,&quot;TemplateId&quot;,/ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:NotificationTemplateId,&quot;TO&quot;,&quot;&quot;)"/>
                        </ns0:To>
                    </xsl:when>
                    <xsl:when test="dvm:lookupValue(&quot;DVM/NotificationMetadata.dvm&quot;,&quot;TemplateId&quot;,/ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:NotificationTemplateId,&quot;TO&quot;,&quot;&quot;)=&quot;&quot; and /ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:ApprovalUserEmail !=&quot;&quot;">
                        <ns0:To>
                            <xsl:value-of select="/ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:ApprovalUserEmail"/>
                        </ns0:To>
                    </xsl:when>
                    <xsl:otherwise>
                        <ns0:To>
                            <xsl:value-of select="/ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:ApprovalUserEmail"/>
                        </ns0:To>
                    </xsl:otherwise>
                </xsl:choose>
                <ns0:Cc>
                    <xsl:value-of select="dvm:lookupValue(&quot;DVM/NotificationMetadata.dvm&quot;,&quot;TemplateId&quot;,/ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:NotificationTemplateId,&quot;CC&quot;,&quot;&quot;)"/>
                </ns0:Cc>
                <ns0:EmailBody>
                    <xsl:value-of select="/ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:EmailBody"/>
                </ns0:EmailBody>
                <ns0:EmailSubject>
                    <xsl:value-of select="/ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:EmailSubject"/>
                </ns0:EmailSubject>
                <ns0:Role>
                    <xsl:value-of select="/ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:Role"/>
                </ns0:Role>
                <ns0:ApprovalUserName>
                    <xsl:value-of select="/ns0:CCTNotificationMetadata/ns0:MetadataAttributes/ns0:ApprovalUserName"/>
                </ns0:ApprovalUserName>
            </ns0:MetadataAttributes>
        </ns0:CCTNotificationMetadata>
    </xsl:template>
</xsl:stylesheet>