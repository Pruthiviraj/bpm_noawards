<?xml version = '1.0' encoding = 'UTF-8'?>
<xsl:stylesheet version="1.0"
                xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator"
                xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas"
                xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue"
                xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction"
                xmlns:ns0="http://xmlns.klx.com/schema/cct/notification/V1.0"
                xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java"
                xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions"
                exclude-result-prefixes="oracle-xsl-mapper xsi xsd xsl ns0 socket dvm mhdr oraxsl oraext xp20 xref"
                xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/"
                xmlns:tns="http://xmlns.klx.com/wsdl/cct/notification/V1.0"
                xmlns:ns1="http://xmlns.klx.com/wsdl/cct/notification/V1.0/types"
                xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/">
    <oracle-xsl-mapper:schema>
        <!--SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY.-->
        <oracle-xsl-mapper:mapSources>
            <oracle-xsl-mapper:source type="WSDL">
                <oracle-xsl-mapper:schema location="oramds:/apps/WSDLs/CCTNotification.wsdl"/>
                <oracle-xsl-mapper:rootElement name="CCTNotification"
                                               namespace="http://xmlns.klx.com/schema/cct/notification/V1.0"/>
            </oracle-xsl-mapper:source>
        </oracle-xsl-mapper:mapSources>
        <oracle-xsl-mapper:mapTargets>
            <oracle-xsl-mapper:target type="WSDL">
                <oracle-xsl-mapper:schema location="oramds:/apps/WSDLs/CCTNotification.wsdl"/>
                <oracle-xsl-mapper:rootElement name="CCTNotification"
                                               namespace="http://xmlns.klx.com/schema/cct/notification/V1.0"/>
            </oracle-xsl-mapper:target>
        </oracle-xsl-mapper:mapTargets>
        <!--GENERATED BY ORACLE XSL MAPPER 12.2.1.2.0(XSLT Build 161003.0739.0018) AT [THU MAR 08 10:05:18 IST 2018].-->
    </oracle-xsl-mapper:schema>
    <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
    <xsl:template match="/">
        <ns0:CCTNotification>
            <ns0:CCTNotificationHeader>
                <ns0:Source>
                    <xsl:value-of select="/ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:Source"/>
                </ns0:Source>
                <ns0:OpportunityId>
                    <xsl:value-of select="/ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityId"/>
                </ns0:OpportunityId>
                <ns0:OpportunityNumber>
                    <xsl:value-of select="/ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber"/>
                </ns0:OpportunityNumber>
                <ns0:NotificationTemplateId>
                    <xsl:value-of select="/ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:NotificationTemplateId"/>
                </ns0:NotificationTemplateId>
                <ns0:MessageId>
                    <xsl:value-of select="/ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:MessageId"/>
                </ns0:MessageId>
                <xsl:choose>
                    <xsl:when test="oraext:query-database (concat (&quot;select NOTIFY_PARTS from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='Y' 
 and oraext:query-database (concat (&quot;select NOTIFY_LEGAL from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='Y'
 and oraext:query-database (concat (&quot;select NOTIFY_PROGRAM from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='Y' ">
                        <ns0:Role>
                            <xsl:value-of select="concat(oraext:query-database(&quot;select USER_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP_ID = 'CCT_STRATEGIC_PRICING' &quot; ,false(),false(),'jdbc/XxKLXDataSource'),',',oraext:query-database(&quot;select USER_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP_ID = 'CCT_LEGAL' &quot; ,false(),false(),'jdbc/XxKLXDataSource'),',',oraext:query-database(&quot;select USER_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP_ID = 'CCT_PROGRAM_SOLUTIONS' &quot; ,false(),false(),'jdbc/XxKLXDataSource'),',',oraext:query-database(&quot;select USER_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP_ID = 'CCT_PROPOSAL_MANAGER' &quot; ,false(),false(),'jdbc/XxKLXDataSource'))"/>
                        </ns0:Role>
                    </xsl:when>
                    <xsl:when test="oraext:query-database (concat (&quot;select NOTIFY_PARTS from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='Y' 
 and oraext:query-database (concat (&quot;select NOTIFY_LEGAL from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='Y'
 and oraext:query-database (concat (&quot;select NOTIFY_PROGRAM from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='N' ">
                        <ns0:Role>
                            <xsl:value-of select="concat(oraext:query-database(&quot;select USER_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP_ID = 'CCT_STRATEGIC_PRICING' &quot; ,false(),false(),'jdbc/XxKLXDataSource'),',',oraext:query-database(&quot;select USER_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP_ID = 'CCT_LEGAL' &quot; ,false(),false(),'jdbc/XxKLXDataSource'),',',oraext:query-database(&quot;select USER_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP_ID = 'CCT_PROPOSAL_MANAGER' &quot; ,false(),false(),'jdbc/XxKLXDataSource'))"/>
                        </ns0:Role>
                    </xsl:when>
                    <xsl:when test="oraext:query-database (concat (&quot;select NOTIFY_PARTS from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='N' 
 and oraext:query-database (concat (&quot;select NOTIFY_LEGAL from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='Y'
 and oraext:query-database (concat (&quot;select NOTIFY_PROGRAM from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='Y' ">
                        <ns0:Role>
                            <xsl:value-of select="concat(oraext:query-database(&quot;select USER_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP_ID = 'CCT_LEGAL' &quot; ,false(),false(),'jdbc/XxKLXDataSource'),',',oraext:query-database(&quot;select USER_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP_ID = 'CCT_PROGRAM_SOLUTIONS' &quot; ,false(),false(),'jdbc/XxKLXDataSource'),',',oraext:query-database(&quot;select USER_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP_ID = 'CCT_PROPOSAL_MANAGER' &quot; ,false(),false(),'jdbc/XxKLXDataSource'))"/>
                        </ns0:Role>
                    </xsl:when>
                    <xsl:when test="oraext:query-database (concat (&quot;select NOTIFY_PARTS from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='Y' 
 and oraext:query-database (concat (&quot;select NOTIFY_LEGAL from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='N'
 and oraext:query-database (concat (&quot;select NOTIFY_PROGRAM from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='Y' ">
                        <ns0:Role>
                            <xsl:value-of select="concat(oraext:query-database(&quot;select USER_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP_ID = 'CCT_STRATEGIC_PRICING' &quot; ,false(),false(),'jdbc/XxKLXDataSource'),',',oraext:query-database(&quot;select USER_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP_ID = 'CCT_PROGRAM_SOLUTIONS' &quot; ,false(),false(),'jdbc/XxKLXDataSource'),',',oraext:query-database(&quot;select USER_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP_ID = 'CCT_PROPOSAL_MANAGER' &quot; ,false(),false(),'jdbc/XxKLXDataSource'))"/>
                        </ns0:Role>
                    </xsl:when>
                    <xsl:when test="oraext:query-database (concat (&quot;select NOTIFY_PARTS from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='Y' 
 and oraext:query-database (concat (&quot;select NOTIFY_LEGAL from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='N'
 and oraext:query-database (concat (&quot;select NOTIFY_PROGRAM from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='N' ">
                        <ns0:Role>
                            <xsl:value-of select="concat(oraext:query-database(&quot;select USER_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP_ID = 'CCT_STRATEGIC_PRICING' &quot; ,false(),false(),'jdbc/XxKLXDataSource'),',',oraext:query-database(&quot;select USER_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP_ID = 'CCT_PROPOSAL_MANAGER' &quot; ,false(),false(),'jdbc/XxKLXDataSource'))"/>
                        </ns0:Role>
                    </xsl:when>
                    <xsl:when test="oraext:query-database (concat (&quot;select NOTIFY_PARTS from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='N' 
 and oraext:query-database (concat (&quot;select NOTIFY_LEGAL from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='Y'
 and oraext:query-database (concat (&quot;select NOTIFY_PROGRAM from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='N' ">
                        <ns0:Role>
                            <xsl:value-of select="concat(oraext:query-database(&quot;select USER_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP_ID = 'CCT_LEGAL' &quot; ,false(),false(),'jdbc/XxKLXDataSource'),',',oraext:query-database(&quot;select USER_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP_ID = 'CCT_PROPOSAL_MANAGER' &quot; ,false(),false(),'jdbc/XxKLXDataSource'))"/>
                        </ns0:Role>
                    </xsl:when>
                    <xsl:when test="oraext:query-database (concat (&quot;select NOTIFY_PARTS from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='N' 
 and oraext:query-database (concat (&quot;select NOTIFY_LEGAL from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='N'
 and oraext:query-database (concat (&quot;select NOTIFY_PROGRAM from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='Y' ">
                        <ns0:Role>
                            <xsl:value-of select="concat(oraext:query-database(&quot;select USER_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP_ID = 'CCT_PROGRAM_SOLUTIONS' &quot; ,false(),false(),'jdbc/XxKLXDataSource'),',',oraext:query-database(&quot;select USER_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP_ID = 'CCT_PROPOSAL_MANAGER' &quot; ,false(),false(),'jdbc/XxKLXDataSource'))"/>
                        </ns0:Role>
                    </xsl:when>
                    <xsl:when test="oraext:query-database (concat (&quot;select NOTIFY_PARTS from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='N' 
 and oraext:query-database (concat (&quot;select NOTIFY_LEGAL from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='N'
 and oraext:query-database (concat (&quot;select NOTIFY_PROGRAM from cct_crf_revision where crf_revision_id=(select max(crf_revision_id) from cct_crf cc, cct_crf_revision ccr where cc.crf_id = ccr.crf_id and cc.crf_no ='&quot;, /ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:OpportunityNumber, &quot;')&quot; ), false(), false(), 'jdbc/XxKLXDataSource' ) ='N' ">
                        <ns0:Role>
                            <xsl:value-of select="oraext:query-database(&quot;select USER_GROUP from CCT_ROLE_PROPERTIES where USER_GROUP_ID = 'CCT_PROPOSAL_MANAGER' &quot; ,false(),false(),'jdbc/XxKLXDataSource')"/>
                        </ns0:Role>
                    </xsl:when>
                </xsl:choose>
                <ns0:ApprovalUserEmail/>
                <ns0:Attribute1>
                    <xsl:value-of select="/ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:Attribute1"/>
                </ns0:Attribute1>
                <ns0:Attribute2>
                    <xsl:value-of select="/ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:Attribute2"/>
                </ns0:Attribute2>
                <ns0:Attribute3>
                    <xsl:value-of select="/ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:Attribute3"/>
                </ns0:Attribute3>
                <ns0:Attribute4>
                    <xsl:value-of select="/ns0:CCTNotification/ns0:CCTNotificationHeader/ns0:Attribute4"/>
                </ns0:Attribute4>
            </ns0:CCTNotificationHeader>
        </ns0:CCTNotification>
    </xsl:template>
</xsl:stylesheet>