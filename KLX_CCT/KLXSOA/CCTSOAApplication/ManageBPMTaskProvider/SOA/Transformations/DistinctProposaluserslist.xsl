<?xml version = '1.0' encoding = 'UTF-8'?>
<xsl:stylesheet version="2.0" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:tns="http://www.klx.com/bpm/cct/tqs/utility/V1.0" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:ns0="http://www.klx.com/cct/tqs/user/emails" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="xsd oracle-xsl-mapper xsi xsl ns0 tns mhdr oraext xp20 xref socket dvm oraxsl" xmlns:plnk="http://docs.oasis-open.org/wsbpel/2.0/plnktype" xmlns:client="http://xmlns.oracle.com/CCTSSOAApplication/ManageBPMTaskProvider/ManageBPMTaskProviderService" xmlns:ns4="http://xmlns.oracle.com/CommonUtility/bpm/tqs/Utility" xmlns:ns3="http://xmlns.klx.com/wsdl/cct/notification/V1.0" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:ns2="http://www.klx.com/db/fault/V1.0" xmlns:ns1="http://xmlns.klx.com/schema/Opportunity/V1.0" xmlns:ns5="http://xmlns.oracle.com/CommonUtility/bpm/tqs/Utility/types" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/">
  <oracle-xsl-mapper:schema>
    <!--SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY.-->
    <oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:source type="WSDL">
        <oracle-xsl-mapper:schema location="../WSDLs/ManageBPMTaskProviderService.wsdl"/>
        <oracle-xsl-mapper:rootElement name="UserResponse" namespace="http://www.klx.com/cct/tqs/user/emails"/>
      </oracle-xsl-mapper:source>
    </oracle-xsl-mapper:mapSources>
    <oracle-xsl-mapper:mapTargets>
      <oracle-xsl-mapper:target type="WSDL">
        <oracle-xsl-mapper:schema location="oramds:/apps/WSDLs/BPMTaskQueryUtility.wsdl"/>
        <oracle-xsl-mapper:rootElement name="AssignedUsersForOpportunityRsp" namespace="http://www.klx.com/bpm/cct/tqs/utility/V1.0"/>
      </oracle-xsl-mapper:target>
    </oracle-xsl-mapper:mapTargets>
    <!--GENERATED BY ORACLE XSL MAPPER 12.2.1.2.0(XSLT Build 161003.0739.0018) AT [THU MAR 08 15:47:53 IST 2018].-->
  </oracle-xsl-mapper:schema>
  <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
  <xsl:template match="/">
    <tns:AssignedUsersForOpportunityRsp>
      <tns:AssignedUsers>
        <tns:Users>
          <xsl:variable name="UserIds" select="distinct-values(/ns0:UserResponse/ns0:UserIds)"/>
          <xsl:variable name="UserNames" select="distinct-values(/ns0:UserResponse/ns0:UserNames)"/>
          <xsl:variable name="UserEmails" select="distinct-values(/ns0:UserResponse/ns0:UserEmails)"/>
          <tns:UserId>
            <xsl:value-of select="$UserIds" separator=","/>
          </tns:UserId>
          <tns:Name>
            <xsl:value-of select="$UserNames" separator=","/>
          </tns:Name>
          <tns:Email>
            <xsl:value-of select="$UserEmails" separator=","/>
          </tns:Email>
        </tns:Users>
      </tns:AssignedUsers>
    </tns:AssignedUsersForOpportunityRsp>
  </xsl:template>
</xsl:stylesheet>