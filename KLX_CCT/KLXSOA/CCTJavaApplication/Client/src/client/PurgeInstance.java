package client;

// CompositesInstanceManager.java

 
/**
 * The following is a utility program that is used to get information about
 * deployed composites and also to purge composite instances in different states.
 *
 * Composite States can be one of the following:
 *        
 *        State  Description
 *       0   STATE_INITIATED
 *        1   STATE_OPEN_RUNNING
 *        2   STATE_OPEN_SUSPENDED
 *        3   STATE_OPEN_FAULTED
 *        4   STATE_CLOSED_PENDING_CANCEL
 *        5   STATE_CLOSED_COMPLETED
 *        6   STATE_CLOSED_FAULTED
 *        7   STATE_CLOSED_CANCELLED
 *        8   STATE_CLOSED_ABORTED
 *        9   STATE_CLOSED_STALE
 *        10   STATE_CLOSED_ROLLED_BACK
 *      
 * The code uses the api's available in the Oracle Fusion Middleware modules.
 * Include the following jars in the path
 *
 *  1. fabric-common.jar
 *  ($ORACLE_HOME\oracle_common\modules\oracle.fabriccommon_11.1.1)
 * 2. fabric-runtime.jar
 *  ($ORACLE_HOME\Oracle_SOA\soa\modules\oracle.soa.fabric_11.1.1)
 * 3. soa-infra-mgmt.jar
 *  ($ORACLE_HOME\oracle_common\soa\modules\oracle.soa.mgmt_11.1.1)
 * 4. jrf.jar
 *  ($ORACLE_HOME\oracle_common\modules\oracle.jrf_11.1.1)
 *  5. wlfullclient.jar
 *  ($ORACLE_HOME\wlserver_10.3\server\lib)
 *
 *
 * @author Kumar Matcha
 * @copyright 2011
 * @since 03/10/2011
 * @version 1.0
 */
 
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
 
import javax.naming.Context;
 
import oracle.soa.management.facade.Composite;
import oracle.soa.management.facade.CompositeInstance;
import oracle.soa.management.facade.Locator;
import oracle.soa.management.facade.LocatorFactory;
import oracle.soa.management.util.CompositeFilter;
import oracle.soa.management.util.CompositeInstanceFilter;
 
public class PurgeInstance {
 
 private Locator locator = getLocator();
 
 public Hashtable<String, String> getJNDIProps() {
  Hashtable<String, String> jndiProps = new Hashtable<String, String>();
  jndiProps.put(Context.PROVIDER_URL, "t3://10.98.171.13:7003/soa-infra");
  jndiProps.put(Context.INITIAL_CONTEXT_FACTORY,
    "weblogic.jndi.WLInitialContextFactory");
  jndiProps.put(Context.SECURITY_PRINCIPAL, "weblogic");
  jndiProps.put(Context.SECURITY_CREDENTIALS, "DevBpm123");
  jndiProps.put("dedicated.connection", "true");
  return jndiProps;
 }
 
 public Locator getLocator() {
  try {
   return LocatorFactory.createLocator(getJNDIProps());
  } catch (Exception e) {
   e.printStackTrace();
  }
  return null;
 }
 
 /**
  * This method is used to list all the composites deployed in the soa
  * environment
  *
  */
 public void getComposites() {
  CompositeFilter compositeFilter = new CompositeFilter();
  try {
   List<Composite> composites = locator.getComposites(compositeFilter);
 
   Iterator<Composite> compositeIterator = composites.iterator();
   while (compositeIterator.hasNext()) {
    Composite composite = compositeIterator.next();
    System.out.println("Composite Name:: "
      + composite.getCompositeDN().getCompositeName()
      + " Version:: "
      + composite.getCompositeDN().getRevision());
 
   }
  } catch (Exception e) {
   e.printStackTrace();
  }
 }
 
 /**
  * This method is used to purge all the composite instances based on the
  * state
  *
  * @param CompositeFilter
  *            compositeFilter
  * @param int state
  *
  */
 public void purgeStaleInstances(CompositeFilter compositeFilter, int state) {
  int purgedInstanceCount = 0;
 
  try {
   CompositeInstanceFilter instanceFilter = new CompositeInstanceFilter();
   System.out.println("------- Setting the process State --------");
   instanceFilter.setState(state);
   /**
    * Retrieve all the instances based on the filter criteria given
    */
   List<Composite> composites = locator.getComposites(compositeFilter);
   System.out.println("## Composite Instance Size: "
     + composites.size() + " matching composites.");
 
   Iterator<Composite> compositeIterator = composites.iterator();
   while (compositeIterator.hasNext()) {
    Composite composite = compositeIterator.next();
    int compositeInstanceSize = composite.getInstances(
      instanceFilter).size();
    if (compositeInstanceSize > 0) {
     purgedInstanceCount += compositeInstanceSize;
   //  composite.purgeInstances(instanceFilter);
     System.out.println("## Purged " + compositeInstanceSize
       + " instances for composite "
       + composite.getCompositeDN().getCompositeName());
    } else {
     System.out.println("## No instances found for composite "
       + composite.getCompositeDN().getCompositeName());
    }
   }
   System.out.println("## Cleaned a total of " + purgedInstanceCount
     + " instances.");
  } catch (Exception e) {
   e.printStackTrace();
  }
 }
 
 public static void main(String[] args) {
  PurgeInstance instanceManager = new PurgeInstance();
  
   
 
  int instanceState = CompositeInstance.STATE_STALE;
  CompositeFilter compositeFilter = new CompositeFilter();
     //compositeFilter.
  System.out.println("------ List of Composites Deployed -----"+instanceState);
  instanceManager.getComposites();
  instanceManager.purgeStaleInstances(compositeFilter, instanceState);
 
 }
 
}
