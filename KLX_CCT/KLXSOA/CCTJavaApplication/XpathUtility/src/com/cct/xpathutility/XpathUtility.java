package com.cct.xpathutility;

import java.io.IOException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;

import javax.naming.NamingException;

/**
 *
 * @author infosys
 */
public class XpathUtility {


    public static void main(String[] args) throws IOException {
        // String input = "A,B,D,C";
        XpathUtility obj = new XpathUtility();

        // System.out.println(createCommaSeperatedString("A,B,D,C,VMI_BIN_PROGRAM,WENCOR"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date startDate = sdf.parse("2018-11-15");

            Date endDate = sdf.parse("2017-11-02");

            taskDueDate(4, endDate);
        } catch (ParseException e) {
        }

    }


    static Properties prop;

    /**
     * @param args the command line arguments
     */

    public static String createCommaSeperatedString(String input) {


        String finalResult = "";

        try {
            if (input != null && !input.isEmpty()) {
                setProperty();

                List<String> list = new ArrayList<String>();
                for (String s : input.split(",")) {
                    if (fetchResult(s) != null) {
                        list.add(fetchResult(s));
                    } else {
                        list.add(s);
                    }
                    finalResult = String.join(",", list);
                }
            }
        } catch (Exception e) {


            return input;
        }

        return finalResult;

    }


    public static void setProperty() throws IOException {
        prop = new Properties();
        prop.load(XpathUtility.class.getClassLoader().getResourceAsStream("osckey.properties"));
    }

    public static String fetchResult(String input) {
        return prop.getProperty(input);
    }

    public static void pause(String input) {

        try {
            Thread.sleep(Long.valueOf(input)); //1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }

    }


    public static void taskDueDate(int days, Date end) {
        //Ignore argument check

        //        Calendar c1 = GregorianCalendar.getInstance();
        //        c1.setTime(start);
        //        int w1 = c1.get(Calendar.DAY_OF_WEEK);
        //        c1.add(Calendar.DAY_OF_WEEK, -w1 + 1);

        Calendar c2 = GregorianCalendar.getInstance();
        c2.setTime(end);
        c2.add(Calendar.DAY_OF_MONTH, -days);
        System.out.println("VVVV" + c2.getTime());
        int w2 = c2.get(Calendar.DAY_OF_WEEK);
        Date intermediateDate = c2.getTime();
        System.out.println("VVVVaaaa" + intermediateDate);
        System.out.println("VVVVaaaa" + end);
        int a = (int) days(intermediateDate, end);
        
        System.out.println(a);

        //        do {
        //
        //            c2.add(Calendar.DAY_OF_MONTH, -1);
        //            w2 = c2.get(Calendar.DAY_OF_WEEK);
        //            System.out.println("qqqqq" + c2.getTime());
        //        } while (w2 == Calendar.SUNDAY || w2 == Calendar.SATURDAY);

//        if (w2 == Calendar.SUNDAY ) {
//            c2.add(Calendar.DAY_OF_MONTH, -1);
//            w2 = c2.get(Calendar.DAY_OF_WEEK);
//            System.out.println("qqqqq" + c2.getTime());
//        }
//        else if (w2 == Calendar.SATURDAY){
//            c2.add(Calendar.DAY_OF_MONTH, -2);
//            w2 = c2.get(Calendar.DAY_OF_WEEK);
//            System.out.println("qqqqq" + c2.getTime());
//        }else {
//            
//        }
//
//
//        System.out.println("Final" + c2.getTime());


        //  c2.add(Calendar.DAY_OF_WEEK, -w2 + 1);

        //end Saturday to start Saturday
        //        long days = (c2.getTimeInMillis() - c1.getTimeInMillis()) / (1000 * 60 * 60 * 24);
        //        long daysWithoutSunday = days - (days * 2 / 7);
        //
        //        if (w1 == Calendar.SUNDAY) {
        //            w1 = Calendar.MONDAY;
        //        }
        //        if (w2 == Calendar.SUNDAY || w2 == Calendar.SUNDAY) {
        //            w2 = Calendar.MONDAY;
        //        }
        //
        //        while (w2 == Calendar.SUNDAY || w2 == Calendar.SUNDAY)
        //
        //        if(daysWithoutSunday - w1 + w2 < 0){
        //            return 0;
        //        }
        //        else
        //        {
        //        return daysWithoutSunday - w1 + w2;
        //        }
    }
    
   public static long days(Date start, Date end){
        //Ignore argument check

        var ndays = 1 + Math.round((end.getTime()-start.getTime())/(24*3600*1000));
         var nsaturdays = Math.floor( (start.getDay()+ndays) / 7 );
         return 2*nsaturdays + (start.getDay()==0) - (end.getDay()==6);

    }


}
