package com.cct.utility;

//import javax.xml.transform.Source;

import org.apache.commons.codec.binary.Base64;

public class SOAUtilityImpl implements SOAUtility {

    @Override
    public String generateBase64EncodedString(String htmlPayloadAsString, String attribute1, String attribute2) {

        htmlPayloadAsString = htmlPayloadAsString.replaceAll("&lt;", "<");
        htmlPayloadAsString = htmlPayloadAsString.replaceAll("&gt;", ">");
        htmlPayloadAsString = htmlPayloadAsString.replaceAll("&quot;", "\"");
        htmlPayloadAsString = htmlPayloadAsString.replaceAll("&amp;", "&");
        
        System.out.println(htmlPayloadAsString);

        byte[] encoded = Base64.encodeBase64(htmlPayloadAsString.getBytes());

        return new String(encoded);


    }

  

}
