package com.cct.utility;

public interface SOAUtility {

    /**
     * Check an account number
     * @param account the account number to check
     * @return true of a valid IBAN, false if not
     */
    public String generateBase64EncodedString(String htmlPayloadAsString, String attribute1, String attribute2);


}
