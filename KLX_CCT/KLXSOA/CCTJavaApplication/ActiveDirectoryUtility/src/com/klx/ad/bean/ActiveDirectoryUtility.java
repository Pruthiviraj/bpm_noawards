package com.klx.ad.bean;

import com.klx.ad.generated.GetMultipleUsersDetailsResponse;
import com.klx.ad.generated.GetUserDetailsResponse;
import com.klx.ad.generated.GetUsersByRoleResponse;
import com.klx.ad.generated.GetUsersEmailsResponse;
import com.klx.ad.generated.Roles;
import com.klx.ad.generated.User;
import com.klx.ad.generated.UsersList;
import java.io.PrintStream;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.sql.DataSource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlObject.Factory;

public class ActiveDirectoryUtility
{
  private static String searchBase = "DC=corp,DC=klx,DC=com";
  static XmlObject out;
  public static final String DS_NAME = "jdbc/XxKLXDataSource";
  private static DirContext ldapContext;
  private static final Object LDAP_GROUPS_CONSIDERED = "LDAP_GROUPS_CONSIDERED";
  private static final String NON_PROD = "Non_Prod";
  private static final String SC_CCT = "scCCT";
  private static HashMap propertiesMap = new HashMap();
  private static final String PROPERTIES_QUERY = "select * from cct_properties";
  
  public static Connection createDBConnection()
    throws Exception
  {
    Connection connection = null;
    try
    {
      System.out.println("In createDBConnection - start");
      InitialContext context = new InitialContext();
      Object obj = context.lookup("jdbc/XxKLXDataSource");
      System.out.println("In createDBConnection - after data source look up");
      DataSource ds = (DataSource)obj;
      connection = ds.getConnection();
      System.out.println("In createDBConnection - end");
    }
    catch (SQLException sqlException)
    {
      System.out.println("in sql exception - " + sqlException.getMessage());
      sqlException.printStackTrace();
    }
    return connection;
  }
  
  public static XmlObject getUserDetailsById(String userId)
  {
    GetUserDetailsResponse response = new GetUserDetailsResponse();
    try
    {
      ldapContext = getLDAPContext();
      
      SearchControls searchCtls = new SearchControls();
      String[] returnedAtts = { "samAccountName", "mail", "manager", "name", "memberof" };
      searchCtls.setReturningAttributes(returnedAtts);
      searchCtls.setSearchScope(2);
      String searchFilter = "(&(objectClass=user)(sAMAccountName=" + userId + "))";
      
      NamingEnumeration<SearchResult> results = ldapContext.search(searchBase, searchFilter, searchCtls);
      
      User user = new User();
      user.setUserId(userId.toLowerCase());
      user.setName("");
      while (results.hasMoreElements())
      {
        SearchResult sr = (SearchResult)results.next();
        Attributes attrs = sr.getAttributes();
        String name = attrs.get("name").toString().replace("name: ", "");
        String emailId = attrs.get("mail").toString().replace("mail: ", "");
        if (null != attrs.get("memberof"))
        {
          String memberOf = attrs.get("memberof").toString().replace("memberOf: CN=", "");
          List<String> rolesList = getRolesList(memberOf);
          Roles roles = new Roles();
          for (String role : rolesList) {
            roles.getRole().add(role);
          }
          user.setRoles(roles);
          user.setEmail(emailId);
        }
        user.setName(name);
      }
      response.setUser(user);
      
      JAXBContext jaxbContext1 = JAXBContext.newInstance(new Class[] { GetUserDetailsResponse.class });
      Marshaller jaxbMarshaller = jaxbContext1.createMarshaller();
      jaxbMarshaller.setProperty("jaxb.formatted.output", Boolean.valueOf(true));
      jaxbMarshaller.setProperty("jaxb.fragment", Boolean.valueOf(true));
      StringWriter sw = new StringWriter();
      jaxbMarshaller.marshal(response, System.out);
      jaxbMarshaller.marshal(response, sw);
      
      return XmlObject.Factory.parse(sw.toString());
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return out;
  }
  
  public static XmlObject getUsersByRole(String input)
  {
    GetUsersByRoleResponse response = new GetUsersByRoleResponse();
    try
    {
      ldapContext = getLDAPContext();
      
      SearchControls searchCtls = new SearchControls();
      String[] returnedAtts = { "samAccountName", "mail", "manager", "name", "memberof" };
      searchCtls.setReturningAttributes(returnedAtts);
      searchCtls.setSearchScope(2);
      String searchFilter = "(&(objectClass=user)(memberOf=CN=" + input + ",OU=Groups,OU=KLX Aerospace,DC=corp,DC=klx,DC=com))";
      
      NamingEnumeration<SearchResult> results = ldapContext.search(searchBase, searchFilter, searchCtls);
      
      UsersList usersList = new UsersList();
      while (results.hasMoreElements())
      {
        User user = new User();
        SearchResult sr = (SearchResult)results.next();
        Attributes attrs = sr.getAttributes();
        String name = attrs.get("name").toString().replace("name: ", "");
        String userId = attrs.get("sAMAccountName").toString().replace("sAMAccountName: ", "");
        String emailId = attrs.get("mail").toString().replace("mail: ", "");
        String memberOf = attrs.get("memberof").toString().replace("memberOf: CN=", "");
        List<String> rolesList = getRolesList(memberOf);
        Roles roles = new Roles();
        for (String role : rolesList) {
          roles.getRole().add(role);
        }
        user.setName(name);
        user.setUserId(userId.toLowerCase());
        user.setEmail(emailId);
        user.setRoles(roles);
        usersList.getUser().add(user);
      }
      response.setUsersList(usersList);
      
      JAXBContext jaxbContext1 = JAXBContext.newInstance(new Class[] { GetUsersByRoleResponse.class });
      Marshaller jaxbMarshaller = jaxbContext1.createMarshaller();
      jaxbMarshaller.setProperty("jaxb.formatted.output", Boolean.valueOf(true));
      jaxbMarshaller.setProperty("jaxb.fragment", Boolean.valueOf(true));
      StringWriter sw = new StringWriter();
      jaxbMarshaller.marshal(response, System.out);
      jaxbMarshaller.marshal(response, sw);
      return XmlObject.Factory.parse(sw.toString());
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return out;
  }
  
  public static DirContext getLDAPContext()
  {
    try
    {
      String provider_url = "ldap://ldap-dc.corp.klx.com:389";
      Hashtable<String, String> ldapEnv = new Hashtable(11);
      ldapEnv.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
      ldapEnv.put("java.naming.provider.url", provider_url);
      ldapEnv.put("java.naming.security.authentication", "simple");
      ldapEnv.put("java.naming.security.principal", "ruISBPMDev");
      ldapEnv.put("java.naming.security.credentials", "b4P9M#j2s$");
      ldapContext = new InitialDirContext(ldapEnv);
      return ldapContext;
    }
    catch (Exception e)
    {
      System.out.println(" Search error: " + e);
      e.printStackTrace();
    }
    return null;
  }
  
  public static List<String> getRolesList(String memberOf)
  {
    List<String> rolesList = new ArrayList();
    List filterRoleList = null;
    
    String roleList = memberOf;
    int index = memberOf.indexOf("scCCT_");
    int count = 0;
    while (index != -1)
    {
      count++;
      memberOf = memberOf.substring(index + 1);
      index = memberOf.indexOf("scCCT_");
    }
    for (int i = 1; i <= count; i++)
    {
      roleList = roleList.substring(roleList.indexOf("scCCT_"), roleList.length());
      String role = roleList.substring(0, roleList.indexOf(","));
      rolesList.add(role);
      roleList = roleList.substring(roleList.indexOf(","), roleList.length());
    }
    if ((null != roleList) && (roleList.length() > 0)) {
      filterRoleList = filterUserGroups(rolesList);
    } else {
      filterRoleList = rolesList;
    }
    return filterRoleList;
  }
  
  public static List filterUserGroups(List userGroups)
  {
    System.out.println("Entering filterUserGroups method, original user groups from LDAP -- " + userGroups);
    String ldap_groups_considered = null;
    if (propertiesMap.size() == 0)
    {
      populatePropertiesMap();
      ldap_groups_considered = (String)propertiesMap.get(LDAP_GROUPS_CONSIDERED);
    }
    else
    {
      ldap_groups_considered = (String)propertiesMap.get(LDAP_GROUPS_CONSIDERED);
    }
    System.out.println("ldap_groups_considered -- " + ldap_groups_considered);
    ArrayList filterUserGroups = new ArrayList();
    filterUserGroups.addAll(userGroups);
    for (int i = 0; i < userGroups.size(); i++)
    {
      Object groupName = userGroups.get(i);
      if ((null != ldap_groups_considered) && (ldap_groups_considered.toString().equalsIgnoreCase("Non_Prod")))
      {
        if ((!groupName.toString().contains("Non_Prod")) && 
          (groupName.toString().contains("scCCT"))) {
          filterUserGroups.remove(groupName);
        }
      }
      else if ((groupName.toString().contains("Non_Prod")) && 
        (groupName.toString().contains("scCCT"))) {
        filterUserGroups.remove(groupName);
      }
    }
    System.out.println("Exiting filterUserGroups method, filtered user groups -- " + filterUserGroups);
    return filterUserGroups;
  }
  
  private static void populatePropertiesMap()
  {
    ResultSet resultset = null;
    Connection connection = null;
    Statement statement = null;
    try
    {
      connection = createDBConnection();
      statement = connection.createStatement();
      
      resultset = statement.executeQuery("select * from cct_properties");
      while (resultset.next()) {
        propertiesMap.put(resultset.getString("KEY"), resultset
          .getString("VALUE"));
      }
      resultset.close();
      statement.close(); return;
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
    finally
    {
      try
      {
        if (resultset != null) {
          resultset.close();
        }
        if (statement != null) {
          statement.close();
        }
        if (connection != null) {
          connection.close();
        }
      }
      catch (SQLException sqlException)
      {
        sqlException.printStackTrace();
      }
    }
  }
  
  public static XmlObject getMultipleUsersDetails(String input)
  {
    GetMultipleUsersDetailsResponse response = new GetMultipleUsersDetailsResponse();
    try
    {
      List<String> userList = new ArrayList(Arrays.asList(input.split("\\s*,\\s*")));
      ldapContext = getLDAPContext();
      
      SearchControls searchCtls = new SearchControls();
      String[] returnedAtts = { "samAccountName", "mail", "manager", "name", "memberof" };
      searchCtls.setReturningAttributes(returnedAtts);
      searchCtls.setSearchScope(2);
      String searchFilter = "";
      
      UsersList usersList = new UsersList();
      for (String str : userList)
      {
        searchFilter = "(&(objectClass=user)(sAMAccountName=" + str + "))";
        NamingEnumeration<SearchResult> results = ldapContext.search(searchBase, searchFilter, searchCtls);
        while (results.hasMoreElements())
        {
          User user = new User();
          SearchResult sr = (SearchResult)results.next();
          Attributes attrs = sr.getAttributes();
          String name = attrs.get("name").toString().replace("name: ", "");
          String userId = attrs.get("sAMAccountName").toString().replace("sAMAccountName: ", "");
          String emailId = attrs.get("mail").toString().replace("mail: ", "");
          String memberOf = attrs.get("memberof").toString().replace("memberOf: CN=", "");
          List<String> rolesList = getRolesList(memberOf);
          Roles roles = new Roles();
          for (String role : rolesList) {
            roles.getRole().add(role);
          }
          user.setName(name);
          user.setUserId(userId.toLowerCase());
          user.setEmail(emailId);
          user.setRoles(roles);
          usersList.getUser().add(user);
        }
      }
      response.setUsersList(usersList);
      
      JAXBContext jaxbContext1 = JAXBContext.newInstance(new Class[] { GetMultipleUsersDetailsResponse.class });
      Marshaller jaxbMarshaller = jaxbContext1.createMarshaller();
      jaxbMarshaller.setProperty("jaxb.formatted.output", Boolean.valueOf(true));
      jaxbMarshaller.setProperty("jaxb.fragment", Boolean.valueOf(true));
      StringWriter sw = new StringWriter();
      jaxbMarshaller.marshal(response, System.out);
      jaxbMarshaller.marshal(response, sw);
      return XmlObject.Factory.parse(sw.toString());
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return out;
  }
  
  public static XmlObject getUsersEmails(String input)
  {
    GetUsersEmailsResponse response = new GetUsersEmailsResponse();
    try
    {
      ldapContext = getLDAPContext();
      List<String> userList = new ArrayList(Arrays.asList(input.split("\\s*,\\s*")));
      SearchControls searchCtls = new SearchControls();
      String[] returnedAtts = { "samAccountName", "mail" };
      searchCtls.setReturningAttributes(returnedAtts);
      searchCtls.setSearchScope(2);
      String searchFilter = "";
      
      String emails = "";
      int count = 1;
      for (String str : userList)
      {
        searchFilter = "(&(objectClass=user)(sAMAccountName=" + str + "))";
        NamingEnumeration<SearchResult> results = ldapContext.search(searchBase, searchFilter, searchCtls);
        while (results.hasMoreElements())
        {
          SearchResult sr = (SearchResult)results.next();
          Attributes attrs = sr.getAttributes();
          String userId = attrs.get("sAMAccountName").toString().replace("sAMAccountName: ", "");
          if ((null != userId) && (userId.equalsIgnoreCase(str)))
          {
            String emailId = attrs.get("mail").toString().replace("mail: ", "");
            if ((null != emailId) && (!"".equalsIgnoreCase(emailId))) {
              if (count > 1) {
                emails = emails + "," + emailId;
              } else {
                emails = emailId;
              }
                count++;
            }              
          }
        }
       //   count++;
      }
      response.setEmails(emails);
        System.out.println("Emails-------------"+emails);
      GetUsersEmailsResponse obj = new GetUsersEmailsResponse();
      JAXBContext jaxbContext1 = JAXBContext.newInstance(new Class[] { obj.getClass() });
      Marshaller jaxbMarshaller = jaxbContext1.createMarshaller();
      jaxbMarshaller.setProperty("jaxb.formatted.output", Boolean.valueOf(true));
      jaxbMarshaller.setProperty("jaxb.fragment", Boolean.valueOf(true));
      StringWriter sw = new StringWriter();
      jaxbMarshaller.marshal(response, System.out);
      jaxbMarshaller.marshal(response, sw);
      return XmlObject.Factory.parse(sw.toString());
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return out;
  }
  
  public static void main(String[] args)
    throws NamingException
  {
    String req = String.format("<v1:GetMultipleUsersDetailsRequest  xmlns:v1=\"http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0\"> \n\n <v1:userId>sqb,spal</v1:userId> \n\n </v1:GetMultipleUsersDetailsRequest> \n", new Object[0]);
    try
    {
      XmlObject userDetailEmailObj = getUsersEmailsByName("Sherry Hancock,Sureka Renugopal,Ashu Raj");
        XmlObject userDetailEmailObj1 = getUsersEmails("839,sfs,xxx,araj");
      System.out.println("getUsersEmailsByName--" + userDetailEmailObj);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  public static XmlObject CustomerAddress(String CustId)
  {
    String xmlString = String.format("<GetUserDetailsResponse xmlns=\"http://www.klx.com/cct/ad/ActiveDirectoryService/V1.0\" xmlns:ns0=\"http://www.klx.com/cct/ad/User/V1.0\">\n   <User>\n      <ns0:UserId>sqb</ns0:UserId>\n      <ns0:Name>Sachin Bhardwaj</ns0:Name>\n      <ns0:Roles>\n         <ns0:Role>scCCT_Operations_VP</ns0:Role>\n         <ns0:Role>scCCT_Advanced_Sourcing</ns0:Role>\n         <ns0:Role>scCCT_Quality</ns0:Role>\n      </ns0:Roles>\n      <ns0:Email>Sachin.Bhardwaj@KLX.com</ns0:Email>\n   </User>\n</GetUserDetailsResponse>", new Object[0]);
    try
    {
      return XmlObject.Factory.parse(xmlString);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return out;
  }
  
  public static XmlObject getUserDetailsByEmail(String emailId)
  {
    GetUserDetailsResponse response = new GetUserDetailsResponse();
    try
    {
      ldapContext = getLDAPContext();
      
      SearchControls searchCtls = new SearchControls();
      String[] returnedAtts = { "samAccountName", "mail", "manager", "name", "memberof" };
      searchCtls.setReturningAttributes(returnedAtts);
      searchCtls.setSearchScope(2);
      String searchFilter = "(&(objectClass=user)(mail=" + emailId + "))";
      
      NamingEnumeration<SearchResult> results = ldapContext.search(searchBase, searchFilter, searchCtls);
      
      User user = new User();
      while (results.hasMoreElements())
      {
        SearchResult sr = (SearchResult)results.next();
        Attributes attrs = sr.getAttributes();
        String name = attrs.get("name").toString().replace("name: ", "");
        String userEmailId = attrs.get("mail").toString().replace("mail: ", "");
        String userId = attrs.get("samAccountName").toString().replace("samAccountName", "");
        if (null != userId) {
          userId = userId.substring(16, userId.length());
        }
        String memberOf = attrs.get("memberof").toString().replace("memberOf: CN=", "");
        List<String> rolesList = getRolesList(memberOf);
        Roles roles = new Roles();
        for (String role : rolesList) {
          roles.getRole().add(role);
        }
        user.setName(name);
        user.setUserId(userId.toLowerCase());
        user.setEmail(userEmailId);
        user.setRoles(roles);
      }
      response.setUser(user);
      
      JAXBContext jaxbContext1 = JAXBContext.newInstance(new Class[] { GetUserDetailsResponse.class });
      Marshaller jaxbMarshaller = jaxbContext1.createMarshaller();
      jaxbMarshaller.setProperty("jaxb.formatted.output", Boolean.valueOf(true));
      jaxbMarshaller.setProperty("jaxb.fragment", Boolean.valueOf(true));
      StringWriter sw = new StringWriter();
      jaxbMarshaller.marshal(response, System.out);
      jaxbMarshaller.marshal(response, sw);
      
      return XmlObject.Factory.parse(sw.toString());
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return out;
  }
  
  public static XmlObject getUsersEmailsByName(String input)
  {
    String userIds = "";
    input = input.trim();
    if (input.charAt(0) == ',') {
      input = input.substring(1, input.length());
    }
    try
    {
      ldapContext = getLDAPContext();
      List<String> userNameList = new ArrayList(Arrays.asList(input.split("\\s*,\\s*")));
      SearchControls searchCtls = new SearchControls();
      String[] returnedAtts = { "samAccountName", "name" };
      searchCtls.setReturningAttributes(returnedAtts);
      searchCtls.setSearchScope(2);
      
      int count = 1;
      for (String str : userNameList)
      {
        String searchFilter = "(&(objectClass=user)(name=" + str + "))";
        NamingEnumeration<SearchResult> answer = ldapContext.search(searchBase, searchFilter, searchCtls);
        while (answer.hasMoreElements())
        {
          SearchResult sr = (SearchResult)answer.next();
          Attributes attrs = sr.getAttributes();
          String userName = attrs.get("name").toString().replace("name: ", "");
          if ((null != userName) && (userName.equalsIgnoreCase(str)))
          {
            String userId = attrs.get("sAMAccountName").toString().replace("sAMAccountName: ", "");
            if ((null != userId) && (!"".equalsIgnoreCase(userId))) {
              if (count > 1) {
                userIds = userIds + "," + userId;
              } else {
                userIds = userId;
              }
            }
          }
        }
        count++;
      }
    }
    catch (Exception E)
    {
      SearchControls searchCtls;
      int count;
      System.out.println(E);
      userIds = "";
      E.printStackTrace();
    }
    return getUsersEmails(userIds);
  }
}
