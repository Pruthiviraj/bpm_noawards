package com.klx.osb.utility;

import java.io.PrintStream;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import java.util.StringTokenizer;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import javax.sql.DataSource;


public class OSBCommonUtility {

    private static String searchBase = "DC=corp,DC=klx,DC=com";
    private static DirContext ldapContext;
    private static final Object LDAP_GROUPS_CONSIDERED = "LDAP_GROUPS_CONSIDERED";
    private static final String NON_PROD = "Non_Prod";
    private static final String SC_CCT = "scCCT";
    private static HashMap propertiesMap = new HashMap();
    private static final String PROPERTIES_QUERY = "select * from cct_properties";

    public static void main(String[] args) throws NamingException {
        //                System.out.println("Final list is " +
        //                                   OSBCommonUtility.checkGroupValidityForApprovals("ldap-dc.corp.klx.com", "389",
        //                                                                                   "CN=ruISBPMDev,OU=Ent Resource Accounts,DC=corp,DC=klx,DC=com",
        //              "b4P9M#j2s$",
        //            "scCCT_Legal,scCCT_Proposal_Manager"));
        try {
            //System.out.println(fetchExactCustomerDueDate("2018-02-17", 2));
            System.out.println("Final list is " +
                               OSBCommonUtility.getDetails("ldap-dc.corp.klx.com", "389",
                                                           "CN=ruISBPMDev,OU=Ent Resource Accounts,DC=corp,DC=klx,DC=com",
                                                           "b4P9M#j2s$", "psal"));
        } catch (Exception e) {
        }
    }



    public static void pause(String input) {

        try {
            Thread.sleep(Long.valueOf(input)); //1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }

    }

    public static Connection createDBConnection() throws Exception
    {
      Connection connection = null;
      try
      {
        System.out.println("In createDBConnection - start");
        InitialContext context = new InitialContext();
        Object obj = context.lookup("jdbc/XxKLXDataSource");
        System.out.println("In createDBConnection - after data source look up");
        DataSource ds = (DataSource)obj;
        connection = ds.getConnection();
        System.out.println("In createDBConnection - end");
      }
      catch (SQLException sqlException)
      {
        System.out.println("in sql exception - " + sqlException.getMessage());
        sqlException.printStackTrace();
      }
      return connection;
    }
    
    public static String getUserXMLObject(String host, String port, String principal, String credential,
                                          String input) throws NamingException {
        int flag = input.indexOf("scCCT_");
        String type = "", xmlOutput = "";
        if (flag < 0)
            type = "USERS";
        else
            type = "GROUP";

        if (type.equals("USERS")) {
            try {

                List<String> attributes = new ArrayList<String>();
                attributes.add("USERID");
                attributes.add("NAME");
                attributes.add("EMAILID");
                attributes.add("ROLES");
                xmlOutput =
                    "<AttributeIdDetailsResp xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                    "                        xsi:schemaLocation=\"http://www.klx.com/cct/ldap/user/V1.0 Artifacts/proxy/xsd/ADUtility.xsd\"\n" +
                    "                        xmlns=\"http://www.klx.com/cct/ldap/user/V1.0\">";
                List<String> userList = Arrays.asList(input.split("\\s*,\\s*"));
                for (String user : userList) {
                    String colonSeparatedoutput = "";
                    colonSeparatedoutput = OSBCommonUtility.getDetails(host, port, principal, credential, user);

                    List<String> userOutput = Arrays.asList(colonSeparatedoutput.split("\\s*;\\s*"));
                    Iterator it1 = attributes.iterator();
                    Iterator it2 = userOutput.iterator();
                    xmlOutput = xmlOutput + "<userobjects>";
                    String attribute = "", value = "";
                    while (it1.hasNext() || it2.hasNext()) {
                        if (it1.hasNext()) {
                            attribute = (String) it1.next();
                            value = "";
                        }
                        if (it2.hasNext()) {
                            value = (String) it2.next();
                        }
                        xmlOutput = xmlOutput + "<" + attribute + ">";
                        if (attribute.equals("ROLES")) {
                            List<String> roleList = Arrays.asList(value.split("\\s*,\\s*"));
                            for (String role : roleList) {
                                xmlOutput = xmlOutput + "<ROLE>";
                                xmlOutput = xmlOutput + role;
                                xmlOutput = xmlOutput + "</ROLE>";
                            }
                        } else {
                            xmlOutput = xmlOutput + value;
                        }
                        xmlOutput = xmlOutput + "</" + attribute + ">";
                    }
                    xmlOutput = xmlOutput + "</userobjects>";
                }
                xmlOutput = xmlOutput + "</AttributeIdDetailsResp>";
            } catch (Exception e) {
                e.printStackTrace();
                return xmlOutput;
            }
        } else {
            try {
                ldapContext = setContext(host, port, principal, credential);
                SearchControls searchCtls = new SearchControls();
                String returnedAtts[] = { "samAccountName" };
                searchCtls.setReturningAttributes(returnedAtts);
                searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
                String searchFilter =
                    "(&(objectClass=user)(memberOf=CN=" + input + ",OU=Groups,OU=KLX Aerospace,DC=corp,DC=klx,DC=com))";

                //                String searchBase = "OU=KLX Aerospace,DC=corp,DC=klx,DC=com";

                NamingEnumeration<SearchResult> answer =
                    ldapContext.search(OSBCommonUtility.searchBase, searchFilter, searchCtls);
                List<String> userList = new ArrayList<String>();

                while (answer.hasMoreElements()) {
                    SearchResult sr = (SearchResult) answer.next();
                    Attributes attrs = sr.getAttributes();
                    String userId = "";
                    try {
                        userId = attrs.get("samAccountName").toString();

                    } catch (Exception e) {
                        System.out.println(attrs.get("samAccountName"));
                    }
                    userList.add(userId.replace("sAMAccountName: ", ""));
                }
                xmlOutput =
                    OSBCommonUtility.getUserXMLObject("ldap-dc.corp.klx.com", "389",
                                                      "CN=ruISBPMDev,OU=Ent Resource Accounts,DC=corp,DC=klx,DC=com",
                                                      "b4P9M#j2s$", String.join(",", userList));
            } catch (Exception E) {
                System.out.println(E);
                return "";
            } finally {
                ldapContext.close();
            }
        }
        return xmlOutput;
    }

    public static String checkGroupValidityForApprovals(String host, String port, String principal, String credential,
                                                        String commaSeparatedGroups) throws NamingException {
        try {
            ldapContext = setContext(host, port, principal, credential);
            List<String> groupList = Arrays.asList(commaSeparatedGroups.split("\\s*,\\s*"));
            List<String> mailList = new ArrayList<String>();
            SearchControls searchCtls = new SearchControls();

            String[] returnedAtts = { "sAMAccountName" };
            searchCtls.setReturningAttributes(returnedAtts);
            searchCtls.setSearchScope(2);

            String searchFilter = "(&(objectClass=user)(|";
            for (String group : groupList) {
                searchFilter =
                    searchFilter + "(memberOf=CN=" + group + ",OU=Groups,OU=KLX Aerospace,DC=corp,DC=klx,DC=com)";
            }
            searchFilter = searchFilter + "))";
            System.out.println(searchFilter);

            // String searchBase = "OU=KLX Aerospace,DC=corp,DC=klx,DC=com";

            NamingEnumeration<SearchResult> answer =
                ldapContext.search(OSBCommonUtility.searchBase, searchFilter, searchCtls);
            SearchResult sr;
            while (answer.hasMoreElements()) {
                sr = (SearchResult) answer.next();
                Attributes attrs = sr.getAttributes();
                String sAMAccountName = "";
                try {
                    sAMAccountName = attrs.get("sAMAccountName").toString();
                    System.out.println(sAMAccountName);
                } catch (Exception e) {
                    System.out.println(sAMAccountName);
                }
                if ((sAMAccountName != null) || (!sAMAccountName.isEmpty())) {
                    mailList.add(sAMAccountName.replace("sAMAccountName: ", ""));
                }
            }
            return String.join(",", mailList);
        } catch (Exception E) {
            System.out.println(E);
            return "";
        } finally {
            ldapContext.close();
        }
    }

    public static String getEmailList(String host, String port, String principal, String credential,
                                      String commaSeparatedGroups) throws NamingException {
        try {
            ldapContext = setContext(host, port, principal, credential);
            List<String> groupList = Arrays.asList(commaSeparatedGroups.split("\\s*,\\s*"));
            List<String> mailList = new ArrayList<String>();
            SearchControls searchCtls = new SearchControls();

            String[] returnedAtts = { "mail" };
            searchCtls.setReturningAttributes(returnedAtts);
            searchCtls.setSearchScope(2);

            String searchFilter = "(&(objectClass=user)(|";
            for (String group : groupList) {
                searchFilter =
                    searchFilter + "(memberOf=CN=" + group + ",OU=Groups,OU=KLX Aerospace,DC=corp,DC=klx,DC=com)";
            }
            searchFilter = searchFilter + "))";
            System.out.println(searchFilter);

            //String searchBase = "OU=KLX Aerospace,DC=corp,DC=klx,DC=com";

            NamingEnumeration<SearchResult> answer =
                ldapContext.search(OSBCommonUtility.searchBase, searchFilter, searchCtls);
            SearchResult sr;
            while (answer.hasMoreElements()) {
                sr = (SearchResult) answer.next();
                Attributes attrs = sr.getAttributes();
                String mailId = "";
                try {
                    mailId = attrs.get("mail").toString();
                    System.out.println(attrs.get("mail").toString());
                } catch (Exception e) {
                    System.out.println(attrs.get("mail"));
                }
                mailList.add(mailId.replace("mail: ", ""));
            }
            return String.join(",", mailList);
        } catch (Exception E) {
            System.out.println(E);
            return "";
        } finally {
            ldapContext.close();
        }
    }


    public static String getDetails(String host, String port, String principal, String credential,
                                    String userId) throws NamingException {
        try {

            String xmlResult = "";
            String manager = "", roles = "", emailId = "", name = "", status = "";
            ldapContext = setContext(host, port, principal, credential);
            SearchControls searchCtls = new SearchControls();
            String returnedAtts[] = { "samAccountName", "mail", "manager", "name", "memberof" };
            searchCtls.setReturningAttributes(returnedAtts);
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            String searchFilter = "(&(objectClass=user)(sAMAccountName=" + userId + "))";

            //            String searchBase = "DC=corp,DC=klx,DC=com";


            NamingEnumeration<SearchResult> answer =
                ldapContext.search(OSBCommonUtility.searchBase, searchFilter, searchCtls);
            String memberOf = "";
            while (answer.hasMoreElements()) {
                SearchResult sr = (SearchResult) answer.next();
                Attributes attrs = sr.getAttributes();
                name = attrs.get("name")
                            .toString()
                            .replace("name: ", "");
                if (null != attrs.get("memberof")) {
                    emailId = attrs.get("mail")
                                   .toString()
                                   .replace("mail: ", "");
                    memberOf = attrs.get("memberof")
                                    .toString()
                                    .replace("memberOf: CN=", "");
                }
                String roleList = memberOf;
                int index = memberOf.indexOf("scCCT_");
                int count = 0;
                while (index != -1) {
                    count++;
                    memberOf = memberOf.substring(index + 1);
                    index = memberOf.indexOf("scCCT_");
                }
                for (int i = 1; i <= count; i++) {
                    roleList = roleList.substring(roleList.indexOf("scCCT_"), roleList.length());
                    String role = roleList.substring(0, roleList.indexOf(","));
                    if (i > 1)
                        roles = roles + "," + role;
                    else
                        roles = role;
                    roleList = roleList.substring(roleList.indexOf(","), roleList.length());
                }
            }

            if (roles != "") {
                List rolesList = new ArrayList();
                String filterRoles = "";
                StringTokenizer st = new StringTokenizer(roles, ",");
                while (st.hasMoreTokens()) {
                    rolesList.add((String) st.nextToken());
                }
                List filterRolesList = filterUserGroups(rolesList);
                for (int j = 0; j < filterRolesList.size(); j++) {
                    if (j == 0)
                        filterRoles = (String) filterRolesList.get(j);
                    else
                        filterRoles = filterRoles + "," + filterRolesList.get(j);
                }
                roles = filterRoles;
            }

            xmlResult =
                "<Details>" + "<userId>" + userId + "</userId>" + "<name>" + name + "</name>" + "<emailId>" + emailId +
                "</emailId>" + "<roles>" + roles + "</roles>" + "</Details>";
            return xmlResult;


        } catch (Exception E) {
            System.out.println(E);
            E.printStackTrace();
            return "";
        } finally {
            ldapContext.close();
        }
    }

    public static List filterUserGroups(List userGroups) {
        System.out.println("Entering filterUserGroups method, original user groups from LDAP -- " + userGroups);
        String ldap_groups_considered = null;
        if (propertiesMap.size() == 0) {
            populatePropertiesMap();
            ldap_groups_considered = (String) propertiesMap.get(LDAP_GROUPS_CONSIDERED);
        } else {
            ldap_groups_considered = (String) propertiesMap.get(LDAP_GROUPS_CONSIDERED);
        }
        System.out.println("ldap_groups_considered -- " + ldap_groups_considered);
        ArrayList filterUserGroups = new ArrayList();
        filterUserGroups.addAll(userGroups);
        for (int i = 0; i < userGroups.size(); i++) {
            Object groupName = userGroups.get(i);
            if (null != ldap_groups_considered && ldap_groups_considered.toString().equalsIgnoreCase(NON_PROD)) {
                if (!groupName.toString().contains(NON_PROD) && groupName.toString().contains(SC_CCT))
                    filterUserGroups.remove(groupName);
            } else {
                if (groupName.toString().contains(NON_PROD) && groupName.toString().contains(SC_CCT))
                    filterUserGroups.remove(groupName);
            }
        }


        System.out.println("Exiting filterUserGroups method, filtered user groups -- " + filterUserGroups);
        return filterUserGroups;
    }

    private static void populatePropertiesMap() {

        ResultSet resultset = null;
        Connection connection = null;
        Statement statement = null;

        try {

            connection = createDBConnection();
            statement = connection.createStatement();

            // fetch all external configuration properties from DB
            resultset = statement.executeQuery(PROPERTIES_QUERY);

            while (resultset.next()) {

                propertiesMap.put(resultset.getString("KEY"), resultset.getString("VALUE"));

            }

            resultset.close();
            statement.close();

        } catch (Exception exception) {

            exception.printStackTrace();

        } finally {
            try {
                if (resultset != null) {
                    resultset.close();
                }
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException sqlException) {

                sqlException.printStackTrace();

            }
        }


    }


    public static DirContext setContext(String host, String port, String principal, String credential) {
        try {
            String provider_url = "ldap://" + host + ":" + port;
            Hashtable<String, String> ldapEnv = new Hashtable<String, String>(11);
            ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            ldapEnv.put(Context.PROVIDER_URL, provider_url);
            ldapEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
            ldapEnv.put(Context.SECURITY_PRINCIPAL, principal);
            ldapEnv.put(Context.SECURITY_CREDENTIALS, credential);
            ldapContext = new InitialDirContext(ldapEnv);
            return ldapContext;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String fetchExactCustomerDueDate(String customerDueDate, int numOfDays) throws ParseException {
        Date originalCustomerDueDate = new SimpleDateFormat("yyyy-MM-dd").parse(customerDueDate);
        //new SimpleDateFormat("yyyy-MM-dd").parse(customerDueDate);
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(originalCustomerDueDate);
        //               System.out.println(cal.get(Calendar.DAY_OF_WEEK));
        cal.add(Calendar.DATE, -numOfDays);
        //               System.out.println(cal.get(Calendar.DAY_OF_WEEK));
        if (cal.get(Calendar.DAY_OF_WEEK) == 1) {
            cal.add(Calendar.DATE, -2);
        } else if (cal.get(Calendar.DAY_OF_WEEK) == 7) {
            cal.add(Calendar.DATE, -1);
        }
        SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
        return sdFormat.format(cal.getTime());
    }


}


